/*
$(function() {
	$("div.data").html("Tidak ada data.");
});
*/
function doReqCif() {  
	var name = $('#acc').val();
	if (name == "" || name == null){
        alert('Silahkan isi nomor Rekening.');
        return false;
	} 
		
	$.ajax({
		url: "/ReportMCB/eStatGetCif.do",  
		data: "acc=" + name,  
		dataType: 'json', 
		success: function(data){
			$("div.data").html(data.data);
		},
		error: function(e){  
		    alert('Error: ' + e);  
		}  
		}); 
} 

function valRegEStat(){
	var email_to = document.getElementById('email_to').value;
	var updtsts = document.getElementById('updtsts').value;
	
	if (email_to == ""){
        alert('Silahkan isikan email to');
        return false;
    }
	
	if (updtsts == "" || updtsts == null){
		alert('Silahkan pilih status.');
	    return false;
	}
	return true;
}

