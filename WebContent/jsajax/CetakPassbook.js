$("div.data").hide();

function checkDec(cif){
	cif.value = cif.value.replace(/[^ 0-9\n\r]+/g, '');
}

function doAjaxPost() {  
	var name = $('#acc').val();
	if (name == "" || name == null){
        alert('Silahkan isi nomor Rekening.');
        return false;
	} 
	$.blockUI({ overlayCSS: { backgroundColor: '#00f' } }); 
	  var teks = "<tr><td></td>" +
	  		"<td align='left'><button type='button' id='idbtnprntdata' onclick='printData()'>Print</button> &nbsp; <button type='button' id='idbtnnextdata' onclick='nextData()'>Next</button></td></tr>";
	  var name = $('#acc').val();   
	  $.ajax({ 
		  url: "/ReportMCB/getCountDataCtkPssbook.do",  
		  data: "acc=" + name,  
	      dataType: 'json', // Choosing a JSON datatype
	      success: function(data) // Variable data contains the data we get from serverside
	      {
		    setTimeout($.unblockUI, 100); 
		  	if (data.data1 == 0){
			  	$("div.data").html( "<br /><div style='width:490px; height:100%; background:#ccc; border: 1px solid #000; padding:5px;'> " +
			  			"<table border='0' width='490px'><tr><td colspan='2'><b>"+ data.header +"</b></td></tr></table></div>");		  		
		  	} else {
			  	$("div.data").html( "<br /><div style='width:490px; height:100%; background:#ccc; border: 1px solid #000; padding:5px;'> " +
			  			"<table border='0' width='490px'><tr><td colspan='2'>"+ data.header +"</td></tr>" +
			  			"<tr><td colspan='2'><b>"+ data.data1 +"</b></td></tr>" +
			  			"<tr><td><input type='radio' name='optprint' value='1' checked='checked' onclick='dis()'></td><td>"+ data.data2 +"</td></tr>" +
			  			"<tr><td width='10px'><input type='radio' name='optprint' value='2'  onclick='ena()'></td><td>Mulai Baris : <input type='text' name='lne' id='lne' maxlength='2' width='10px;' onKeyup='checkDec(this);' ></td></tr>" +			  			
			  			" " + teks + " </table></div>");		  		
		  	}
		  	$("div.data").show("fold", 5000);
			document.getElementById("idbtnnextdata").disabled=true;
			document.getElementById("lne").disabled=true;
		      /*var WindowObject = window.open('', '', 'height=1, width=1');
		      WindowObject.document.writeln(data[0].name);
		      WindowObject.document.close();
		      WindowObject.focus();
		      WindowObject.print();
		      WindowObject.close();
		      */
	      },
	      error: function(e){  
	    	  setTimeout($.unblockUI, 100); 
	    	  alert('Error: ' + e);  
	    }  
	  }); 
	}  

function nextData(){
	var teks = "<tr><td align='left'>" +
		"<button type='button' id='idbtnprntdata' onclick='printData()'>Print</button>&nbsp;" +
		"<button type='button' id='idbtnnextdata' onclick='nextData()'>Next</button></td></tr>";
	var tex = "";
	var name = $('#acc').val();  
	$.blockUI({ overlayCSS: { backgroundColor: '#00f' } });  
	$.ajax({ 
	  url: "/ReportMCB/nextCtkPssbook.do",  
	  data: "acc=" + name,  
	  dataType: 'json', // Choosing a JSON datatype
	  success: function(data) // Variable data contains the data we get from serverside
	  {		
	    setTimeout($.unblockUI, 100); 
		if (data.data1 == 0){
			$("div.data").html( "<br /><div style='width:490px; height:100%; background:#ccc; border: 1px solid #000; padding:5px;'> " +
		  			"<table border='0' width='490px'><tr><td colspan='2'><b>"+ data.header +"</b></td></tr></table></div>");	  		
	  	} else {
	  		tex = "<br /><div style='width:490px; height:100%; background:#ccc; border: 1px solid #000; padding:5px;'> " +
			"<table border='0' width='490px'><tr><td colspan='2'>"+ data.header +"</td></tr>" +
			"<tr><td colspan='2'><b>"+ data.data1 +"</b>" + "</td></tr>" +
		  	"<tr><td colspan='2'>"+ data.data2 +"</td></tr>";
	  		tex += teks;
			tex += "</table></div>";	
			$("div.data").html(tex);	  		
	  	}
	  	$("div.data").show("fold", 5000);
		$("div.datatrans").hide();
		document.getElementById("idbtnnextdata").disabled=true;
	  },
	  error: function(e){  
    	  setTimeout($.unblockUI, 100); 
    	  alert('Error: ' + e);  
	}  
	}); 
}
function printData(){
	var name = $('#acc').val();   
	var optlne = $('input[name=optprint]:checked').val(); 
	var strtlne = $('#lne').val();
	var brs = parseInt(strtlne);
	if (brs < 1 || brs > 26){
		alert('Mulai baris antara 1 sampai dengan 26');
		return false;
	}

	$.ajax({ 
	  url: "/ReportMCB/printDataPssbook.do",  
	  data: {lne : optlne, startlne:strtlne },  
	  dataType: 'json', // Choosing a JSON datatype
	  success: function(data) // Variable data contains the data we get from serverside
	  {	
		var text = "<html><head><style type='text/css'>body{margin:0; padding:42px 0px 0px 0px; display: block; position: fixed; left: 0;}</style></head><body><table border='0'>";
		var a = 0;
		for (a = 0; a<data.length; a++){
			text += "<tr style = 'color: black; font-family:Times New Roman; font-size: 8pt;'>" +
				"<td style='width:7px; padding:1px 3px 1px 1px;'>"+data[a].no+"</td>" +
				"<td style='width:30px; padding:1px 3px 1px 1px;'>"+data[a].dt+"</td>" +
				"<td style='width:27px; padding:1px 3px 1px 1px;'>"+data[a].trncd+"</td>" +
				"<td style='text-align: right; width:102px; padding:1px 3px 1px 1px;'>"+data[a].mutasi+ " " + data[a].drcr + "</td>" +
				"<td style='width:205px; padding:1px 3px 1px 1px;'>"+data[a].detail+"</td>" +
				"<td style='text-align: right; width:93px; padding:1px 3px 1px 1px;'>"+data[a].saldo+"</td>" +
				"<td style='width:72px; padding:1px 3px 1px 1px;'>"+data[a].auth+"</td></tr>";
		}
		text += "</table></body></html>";
		var WindowObject = window.open('', '', 'height=750, width=650');
	      WindowObject.document.writeln(text);
	      WindowObject.document.close();
	      WindowObject.focus();
	      WindowObject.print();
	      WindowObject.close();
		document.getElementById("idbtnnextdata").disabled=false;
		document.getElementById("idbtnprntdata").disabled=true;
		document.getElementById("lne").disabled=true;
	  },
	error: function(e){  
	  alert('Error: ' + e);  
	}  
	}); 
	
}

function reprint(){
	var no = $('input[name=rbnNumber]:checked').val();
    var ket = $('#lbl_ket').text();
	if ($('input[name=rbnNumber]:checked').length == 0) {
		alert('Silahkan pilih mulai cetak.');
		return false;
    }
	$.ajax({ 
		  url: "/ReportMCB/rePrintDataPssbook.do",  
		  data: { no: no, ket: ket }, 
		  dataType: 'json', // Choosing a JSON datatype
		  success: function(data) // Variable data contains the data we get from serverside
		  {	

			var text = "<html><head><style type='text/css'>body{margin:0;padding:42px 0px 0px 0px;}</style></head><body><table border='0'>";
			var a = 0;
			for (a = 0; a<data.length; a++){
				text += "<tr style = 'color: black; font-family:Times New Roman; font-size: 8pt;'>" +
					"<td style='width:7px; padding:1px 3px 1px 1px;'>"+data[a].no+"</td>" +
					"<td style='width:30px; padding:1px 3px 1px 1px;'>"+data[a].dt+"</td>" +
					"<td style='width:27px; padding:1px 3px 1px 1px;'>"+data[a].trncd+"</td>" +
					"<td style='text-align: right; width:102px; padding:1px 3px 1px 1px;'>"+data[a].mutasi+ " " + data[a].drcr + "</td>" +
					"<td style='width:205px; padding:1px 3px 1px 1px;'>"+data[a].detail+"</td>" +
					"<td style='text-align: right; width:93px; padding:1px 3px 1px 1px;'>"+data[a].saldo+"</td>" +
					"<td style='width:72px; padding:1px 3px 1px 1px;'>"+data[a].auth+"</td></tr>";
			}
			text += "</table></body></html>";
			var WindowObject = window.open('', '', 'height=750, width=650');
		      WindowObject.document.writeln(text);
		      WindowObject.document.close();
		      WindowObject.focus();
		      WindowObject.print();
		      WindowObject.close();
			//$("div.datatrans").html(text);
			//$("div.datatrans").show();
			document.getElementById("idbtnreprint").disabled=true;
		  },
		error: function(e){  
		  alert('Error: ' + e);  
		}  
		}); 
}

function dis(){
	document.getElementById("lne").disabled=true;
}
function ena(){
	document.getElementById("lne").disabled=false;
}

function valInput(){
    var acc = document.getElementById('acc').value;
    var ket = document.getElementById('ket').value;
    var ket1 = document.getElementById('ket1').value;
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;

    if (acc == ""){
        alert('Rekening tidak boleh kosong');
        return false;
    }
    if (ket == ""){
        alert('Silakan pilih keterangan');
        return false;
    }
    if (username == ""){
        alert('User name supervisor tidak boleh kosong');
        return false;
    }
    if (password == ""){
        alert('Password supervisor tidak boleh kosong');
        return false;
    }
    if (ket == "4"){
        if (ket1 == "" || ket1 == null || ket1 == "undefined") {
            alert('Silakan input keterangan');
            return false;
        }
    }
	return true;
}

function viewKet1(){
    var ket = document.getElementById('ket').value;
    if (ket == "4"){
    	document.getElementById("ket1").disabled=false;    	
    } else {
    	document.getElementById("ket1").disabled=true;
    }
}