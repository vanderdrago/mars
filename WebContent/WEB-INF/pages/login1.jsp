<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<link href="css/reset.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/modernizr.js"></script>
</head>
<body>
	
		<fieldset>
			<legend align="center">LOGIN</legend>
			<form action="LoginProcess.do" method="POST" class="cd-form floating-labels">	
			<div class="icon">
							<label class="cd-label" for="cd-name">Branch</label>
							<input class="company" type="text" name="kodecabang" id="kodecabang">
						</div>
						<div class="icon">
							<label class="cd-label" for="cd-name">User Id</label>
							<input class="user" type="text" name="user" id="user">
						</div>
		    
		    <div align="left">
		      	<input type="submit" value="Login">
		    </div>
		    </form>
		</fieldset>
		<script src="js/jquery-2.1.1.js"></script>
		<script src="js/main.js"></script> 
	
</body>
</html>