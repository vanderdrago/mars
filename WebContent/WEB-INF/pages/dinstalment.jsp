<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemrtgsint(){
      var cusid;
    var acno;
    var name;
    
    cusid = document.getElementById('cusid').value;
    acno = document.getElementById('acno').value;
    name = document.getElementById('name').value;
    
    if ((cusid == "" || "".equals(cusid)) && (acno == "" || "".equals(acno)) && (name == "" || "".equals(name))){
        alert('Silahkan isi salah satu fields yang disediakan');
        return false;
    }
    return true;
}
</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h2 align="center">Account Jadwal angsur</h2>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <form action="/ReportMCB/vJMonRTGS.do?fr=f" method="POST" name="mrtgs">
	    
        <table border="0" width="100%">
			<tr>
				<td style="width: 240px">Customer ID</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="cusid" id="cusid"></td>
			</tr>
			<tr>
				<td style="width: 240px">Account No</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="acno" id="acno"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td style="width: 240px">Account Name</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="name" id="name"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td><input onclick="return validatemrtgsint()" type="submit" value="Ok" name="ok" id="ok" /> <!-- onclick="return validatemrtgs()" -->
				</td>
			</tr>
	</table>
        </form>
            <br />
       <div style="font-size:20px; font-family:times new roman;">

           <table id="rounded-corner" style="width:150%">
               <tr >
                 <td>
		            <display:table name="ltrn" id="ltrn" class="wb" requestURI="MJRtgsForward.do" pagesize="20" sort="external" >
		                <display:column title="CUSTOMER ID" sortable="true" style="width:70px;">${ltrn.customer_id}</display:column>
						<display:column title="PROD CODE" sortable="true" style="width:100px;">${ltrn.prod_code}</display:column>
						<display:column title="PROD CATGORY" sortable="true" style="width:70px;">${ltrn.prod_ctgry}</display:column> 
						<display:column title="ACCOUNT NO" sortable="true" style="width:70px;">${ltrn.ac_branch}</display:column>
						<display:column title="BOOK DATE" sortable="true" style="width:100px;">${ltrn.book_date}</display:column>
						<display:column title="MATURITY DATE" sortable="true" style="width:70px;">${ltrn.matur_date}</display:column>
						<display:column title="APP ID" sortable="true" style="width:70px;">${ltrn.prim_app_id}</display:column>
						<display:column title="APP NAME" sortable="true" style="width:300px;">${ltrn.prim_app_name}</display:column>
						<display:column title="NO INSTALMENT" sortable="true" style="width:70px;">${ltrn.no_of_installments}</display:column>
						<display:column title="TTL SALE" sortable="true" style="width:70px;"><fmt:formatNumber type="number" maxFractionDigits="2" value = "${ltrn.total_sale_value}" /> </display:column>
						<display:column title="PROFIT" sortable="true" style="width:70px;"><fmt:formatNumber type="number" maxFractionDigits="2" value = "${ltrn.upfront_profit_booked}" /> </display:column>
						<display:column title="DISBURST" sortable="true" style="width:70px;"><fmt:formatNumber type="number" maxFractionDigits="2" value = "${ltrn.amount_disbursed}" /> </display:column>
						<display:column title="<h3>Rel TRN</h3>" sortable="true" style="width:70px; text-align:center;" >${ltrn.relTRN}</display:column>
			            <display:column title="<h3>Intrf TRN</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.trn}</display:column>
			            <display:column title="<h3>Ultim Benef Acc</h3>" sortable="true" style="width:110px; text-align:center;">${ltrn.ultimateBeneAcc}</display:column>          
			            <display:column title="<h3>Ultim Benef Name</h3>" sortable="true" style="width:170px;">${ltrn.ultimateBeneName}</display:column>
			            <display:column title="<h3>BOR</h3>" sortable="true" style="width:50px; text-align:center;">${ltrn.bor}</display:column>
			            <display:column title="<h3>Intrf Status</h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.status}</display:column>
		        	</display:table>
		        </td>
		       </tr>
		     </table>
		</div>
        <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
		</logic:equal>
</body>
</html>