<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Iterator" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Simulasi Angsuran KPR 5 Tahun</title>
</head>
<body>
	<table width="100%">
		<tr>
			<td align="center">
				<table width="1000px" border="0">
					<tr>
						<td align="center" colspan="7"><b>JADWAL ANGSURAN KPR Muamalat iB 5 Tahun - Angsuran Ringan</b></td>
					</tr>
					<tr>
						<td width="150px"> Nama </td>
						<td width="10px">:</td>
						<td width="300px">${nama}</td>
						<td width="150px">Harga Jual</td>
						<td width="10px">:</td>
						<td width="300px"><fmt:formatNumber type="number" value="${sumAngsuran}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td width="150px"> Plafond Pembiayaan </td>
						<td width="10px">:</td>
						<td colspan="1" width="300px"><fmt:formatNumber type="number" value="${plafond}" maxFractionDigits="2"/></td>
						<td>Total Margin</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${sumMargin}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td width="150px"> Tanggal Simulasi </td>
						<td width="10px">:</td>
						<td colspan="1" width="300px">${tgl}</td>
						<td width="150px">Angsuran Periode 1</td>
						<td width="10px">:</td>
						<td colspan="1" width="300px"><fmt:formatNumber type="number" value="${period1}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td>Jangka Waktu</td>
						<td width="10px">:</td>
						<td width="400px">${waktu} Bulan</td>
						<td width="200px">Angsuran Periode 2</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${period2}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td align="right"><a href="downloadPdf.do"><img src="/ReportMCB/images/red-circle-with-pdf-icon-png-20.png"/></a>
							<a href="downloadExcel.do"><img src="/ReportMCB/images/excel-xls-icon-2.png"/></a>
						</td>

						
						
					</tr>
				</table>
				<table id="rounded-corner" style="width: 1000px">
					<thead>
				        <tr align="center">
				            <th style="width:30px; font-size: 12px;"><b>Bulan</b></th>
				            <th style="width:60px; font-size: 12px;"><b>O/S Pokok</b></th>
				            <th style="width:60px; font-size: 12px;"><b>AngPokok</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Margin</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Angsuran</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Sisa Angsuran</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Sisa Margin</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Rate</b></th>
				        </tr>
				     </thead>
				     <tbody>
				     		<c:forEach var="kpr" items="${kpr}" varStatus="row">
							<tr style="font-size: 11px;">
								<td>${row.index + 1}</td>
								<td><c:if test="${row.index < 37}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.resultPlafond}" />
				     				</c:if>
				     			    <c:if test="${row.index > 35}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.sisa_plafond}" />
				     				</c:if>
				     			</td>
								<td><c:if test="${row.index < 37}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.Angsuran_pokok}" />
				     				</c:if>
				     			    <c:if test="${row.index > 35}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.sisa_pokok}" />
				     				</c:if>
				     			</td>
				     			<td><c:if test="${row.index < 37}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.TempHashMargin}" />
				     				</c:if>
				     			    <c:if test="${row.index > 35}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.sisa_margin}" />
				     				</c:if>
				     			</td>
				     			<td><c:if test="${row.index < 37}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.Angsuran}" />
				     				</c:if>
				     			    <c:if test="${row.index > 35}">
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.sisa_angsuran}" />
				     				</c:if>
				     			</td>
				     			<td>
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.sisa_ang}" />
				     			</td>
				     			<td>
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.sisa_mar}" />
				     			</td>
				     			<td>
				     				<c:if test="${row.index < 37}">
				     					${kpr.Rate}
				     				</c:if>
				     				<c:if test="${row.index > 35}">
				     					${kpr.sisaRate}
				     				</c:if>
				     			</td>
				     		</tr>
				     		</c:forEach>
				     </tbody>
				     <tfoot>
				     	<tr style="font-weight: bold">
				     		<td colspan="2" align="center"><b>TOTAL</b></td>
					    	<td style="text-align: left; " ><fmt:formatNumber type="number" maxFractionDigits="0" value="${sumAngPokok}" /></td>
					    	<td style="text-align: left; " ><fmt:formatNumber type="number" maxFractionDigits="0" value="${sumMargin}" /></td>
					    	<td style="text-align: left; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${sumAngsuran}" /></td>
		    			</tr>
				     </tfoot>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>