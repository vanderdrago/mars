<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Detail User</title>
</head>
<body>
	<table width="1075px" >
		<tr>
			<td>
				<h2>Detail User</h2>
				<p>Detail User terkait pending transaksi</p>
				<table class="table table-striped" >
					<thead>
						<tr>
							<th style="width:50px; font-size: 10px; font-style: family font sains"><b>#</b></th>
							<th style="width:70px; font-size: 10px;"><b>NAMA</b></th>
							<th style="width:250px; font-size: 10px;"><b>ALAMAT</b></th>
							<th style="width:60px; font-size: 10px;"><b>NO HP</b></th>
						</tr>
					<thead>
					<tbody>
				    	<c:set var="inc" value="0" />
						<c:forEach var="detail" items="${detail}" >
							<tr>
								<c:set var="inc" value="${inc + 1}" />
								<td style="font-size: 12px">${inc}</td>
								<td style="font-size: 12px">${detail.userName}</td>
								<td style="font-size: 12px">${detail.alamat}</td>
								<td style="font-size: 12px">${detail.NoHp}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
</body>

</html>