<%--
    Document   : selCab
    Created on : Apr 26, 2013, 5:42:21 PM
    Author     : Windows7
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<script type='text/javascript' src='/MuamalatFrontEndPayment/scripts/common.js'></script>
<script type="text/javascript">
    function validatePilCab(){
        var cabang;

        cabang = document.getElementById('cabang').value;

        if (cabang == ""){
            alert('Cabang tidak boleh kosong, silahkan pilih pada drop down list Cabang!');
            return false;
        }

        return true;
    }
</script>

<h2>Pilih Cabang</h2>
<form name="report" method="post" action="selCab.do">
    <table>
        <tr>
            <td>Kode Cabang :</td>
            <td colspan="2"><select name="cabang" id="cabang" >
                    <option value="">[--Select One--]</option>
                    <c:forEach items="${selCabList}" var="selCabList">
                        <c:choose>
                            <c:when test="${selCabList == cabang}"><option value=${selCabList} selected >${selCabList}</option></c:when>
                            <c:otherwise><option value=${selCabList}>${selCabList}</option></c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select></td>
        </tr>
        <tr align="left">
            <th></th>
            <td><input type="submit" value="Pilih" onclick="return validatePilCab()" ></td>
            <td></td>
        </tr>
    </table>
    <hr>
    <font color="purple">${message}</font>
</form>