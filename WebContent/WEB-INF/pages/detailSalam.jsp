<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.24.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link rel="stylesheet" href="/ReportMCB/css/report/table.css">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Financing SalamMuamalat</title>
</head>
<body>
	<div style="float: center; text-align: center;">
	<table width="100%" border="0">
		<tr >
			<td align="center">
			<table id="rounded-corner"  border="0.5" align="center" style=" width : 1075px;">
				<thead>
					<tr>
						<th style="width:75px; font-size: 12px;"><b>NO. CIF </b></th>
						<th style="width:150px; font-size: 12px;"><b>NAMA NASABAH</b></th>
						<th style="width:75px; font-size: 12px;"><b>NO. KARTU</b></th>
						<th style="width:100px; font-size: 12px;"><b>NO. REKENING ANGSURAN</b></th>
						<th style="width:100px; font-size: 12px;"><b>PRODUK KODE</b></th>
						<th style="width:100px; font-size: 12px;"><b>STATUS KARTU</b></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${salam.no_cif}</td>
						<td>${salam.nama_nasabah}</td>
						<td>${salam.no_kartu}</td>
						<td>${salam.no_rek_angsuran}</td>
						<td>${salam.produk_kode}</td>
						<td>${salam.status_aktif}</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</table>
	
		<table width="100%" border="0">
			<tr>
				<td align="center">
					<fieldset style="width:1050px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    			<legend> <b></b> </legend>
	    				<table width="1050px" border="0,5" id="rounded-corner" >
	    					<tr>
	    						<th style="font-size: 20px;align="center"; colspan=6 ;"><b><h3><center>DASHBOARD FINANCING SALAMUAMALAT</center></h3></b></th>
	    					</tr>
	    					<tr>
	    						<td width="225px">No.CIF</td>
	    						<td>:</td>
	    						<td width="225px">${salam.no_cif}</td>
	    						<td width="250px">Currency</td>
	    						<td>:</td>
	    						<td width="250px">${salam.currency}</td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Kategori Produk</td>
	    						<td>:</td>
	    						<td width="225px">${salam.produk_kategori}</td>
	    						<td width="250px">OS Pembiayaan Saat Ini</td>
	    						<td>:</td>
	    						<td width="250px" style="text-align: left;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${salam.pembiayaan}"/></td>
	    					</tr>
	    					<tr>
	    						<td width="225px">No. Kartu</td>
	    						<td>:</td>
	    						<td width="225px">${salam.no_kartu}</td>
	    						<td width="250px">Angsuran Bulanan</td>
	    						<td>:</td>
	    						<td width="250px" style="text-align: left;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${total.total_angsuran}"/></td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Nama Produk</td>
	    						<td>:</td>
	    						<td width="225px">${salam.nama_produk}</td>
	    						<td width="250px">Pembayaran Angsuran Terakhir</td>
	    						<td>:</td>
	    						<td width="250px" style="text-align: left;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${total.angsuran_terakhir}"/></td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Nama Nasabah</td>
	    						<td>:</td>
	    						<td width="225px">${salam.nama_nasabah}</td>
	    						<td width="250px">Tunggakan Angsuran</td>
	    						<td>:</td>
	    						<td width="250px" style="text-align: left;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${salam.tunggakan_angsuran}"/></td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Jangka Waktu Pembiayaan </td>
	    						<td>:</td>
	    						<td width="225px">${salam.jangka_waktu} Bulan</td>
	    						<td width="250px">Denda Bulanan (Jika ada)</td>
	    						<td>:</td>
	    						<c:if test="${empty salam.denda}">
	    							<td>0</td>
	    						</c:if>
	    						<c:if test="${not empty salam.denda}">
	    							<td width="250px" style="text-align: left;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${salam.denda}"/></td>
	    						</c:if>
	    					</tr>
	    					<tr>
	    						<td width="225px">Sisa Jangka Waktu Pembiayaan</td>
	    						<td>:</td>
	    						<td width="225px">${total.sisa_angsuran} Bulan</td>
	    						<td width="250px">Tunggakan Denda (Jika ada)</td>
	    						<td>:</td>
	    						<td width="250px" style="text-align: left;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${salam.tunggakan_denda}"/></td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Tanggal Pencairan</td>
	    						<td>:</td>
	    						<td width="225px">${salam.tanggal_pencairan}</td>
	    						<td width="250px">Cabang Pengelola Pembiayaan</td>
	    						<td>:</td>
	    						<td width="250px">${salam.branch}</td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Tanggal Jatuh Tempo Pembiayaan</td>
	    						<td>:</td>
	    						<td width="225px">${salam.tanggal_jatuh_tempo}</td>
	    						<td width="250px">Alamat Cabang Pengelola Pembiayaan</td>
	    						<td>:</td>
	    						<td width="250px">${salam.alamat}</td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Tanggal Jatuh Tempo Angsuran</td>
	    						<td>:</td>
	    						<td width="225px">Setiap Tanggal ${total.tanggal_jatuh_tempo_angsuran}</td>
	    						<td width="250px">No. Telp Cabang Pengelola Pembiayaan</td>
	    						<td>:</td>
	    						<td width="250px">${salam.telp}</td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Tanggal Angsuran Terakhir</td>
	    						<td>:</td>
<!-- 	    						<td></td> -->
	    						<td width="225px">${total.tanggal_angsuran_terakhir}</td>
	    						<td width="250px">Kode Account Manager</td>
	    						<td>:</td>
	    						<td width="300px">${salam.kode_marketing}</td>
	    					</tr>
	    					<tr>
	    						<td width="225px">Tanggal Angsuran Selanjutnya</td>
	    						<td>:</td>
<!-- 	    						<td></td> -->
	    						<td width="250px">${total.tanggal_angsur_selanjutnya}</td>
	    						<td width="250px">Nama Account Manager</td>
	    						<td>:</td>
	    						<td width="300px">${salam.nama_marketing}</td>
	    					</tr>
	    				</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>