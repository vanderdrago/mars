<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.24.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>


<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/ReportMCB/scripts/css3-mediaqueries.js"></script>

<link rel="stylesheet" href="/ReportMCB/css/report/jquery-ui.css">
<script type='text/javascript' src='/ReportMCB/scripts/smooth/jquery-1.9.1.js'></script>
<script type='text/javascript' src='/ReportMCB/scripts/smooth/jquery-ui.js'></script>


<script>
$(function() {
	 $( "#dialog" ).dialog({
	      autoOpen: false,
	      width:'auto',     
		  height:'auto',
	      show: {
	        effect: "blind",
	        duration: 1000
	      },
	      hide: {
	        effect: "explode",
	        duration: 1000
	      }
	    });
	 
	    $( "#detail_aum" ).click(function() {
	      $( "#dialog" ).dialog( "open" );
	    });
	  });

</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>Insert title here</title>
</head>
<body>
<table border="0" width="80%">
  <tr>
    <td align="center" colspan="2">
    	<fieldset style="color:black;font-family:Helvetica;width:450px; border:0px solid #000000; border-radius:8px; box-shadow:0 0 20px #999;">
		    <legend> <b>PERAGAAN KELOLAAN DANA</b> </legend>
		    <form action="peragaanDanaAct.do" method="POST" name="jurnal">
		    <table>
				<tr>
					<td><input type="text" name="acc" id="acc" class="inputs" placeholder="No Rekening" style=" width : 400px;"></td>
				</tr>
				<tr>
					<td><input type="text" name="nama" id="nama" class="inputs" placeholder="CIF" style=" width : 400px;"></td>
				</tr>
				<tr>
					<td align="right"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
				</tr>
				<tr>
	            	<td colspan="3" align = "center">
	            		<logic:equal name="confrm" value="err">
						<p style="color: red;"> ${respon} </p>
						</logic:equal>	
	            	</td>
	            </tr>
			</table>
		    </form>
		</fieldset>
    </td>
  </tr>
  <tr style="color:black">
  	  <td align="center">
  		  <table border="0" width="1075px">
  		  	  <tr>
  		  	  	  <td>
  		  	  	   	  <table border="0">
  		  	  	   	  	  <tr>
  		  	  	   	  	  	  <td>
  		  	  	   	  	  	  	<c:if test="${sawalSaldo >= 1}">Page ${sawalSaldo} Of ${noOfPagesSaldo}</c:if>
  		  	  	   	  	  	  </td>
  		  	  	   	  	  	  <td align="left">
  	  							  <c:if test="${sawalSaldo >= 1}"><a href="manageLphSaldoFwd.do?page=1" class="page">First</a></c:if>
  	  						  </td>	
  	  						  <td>
		       					  <c:if test="${currentPageSaldo != 1 && (currentPageSaldo - 1 > 0)}">
								  <a href="manageLphSaldoFwd.do?page=${currentPageSaldo - 1}" class="page">Previous</a>
							      </c:if>
		       			      </td>
		       			      <td>
		       			      	  <table>
		       			      	  	  <tr>
		       			      	  	  	  <td>
		       			      	  	  	  	  <c:if test="${sawalSaldo >= 1}">
		       			      	  	  	  	  	  <c:forEach begin="${sawalSaldo}" end="${sakhirSaldo}" var="i">
		       			      	  	  	  	  	  	 <c:choose><td><a href="manageLphSaldoFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
		       			      	  	  	  	  	  </c:forEach>
		       			      	  	  	  	  </c:if>
		       			      	  	  	  </td>
		       			      	  	  </tr>
		       			      	  </table>
		       			      </td>
		       			      <td>
					     		  <c:if test="${currentPageSaldo lt noOfPagesSaldo}">
						          	<a href="manageLphSaldoFwd.do?page=${currentPageSaldo + 1}" class="page">Next</a>
								  </c:if>
					    	  </td>
					    	  <td>
					    	  	  <c:if test="${noOfPagesSaldo - 9 > 0}">
					       			<a href="manageLphSaldoFwd.do?page=${currentPageSaldo - 9}"class="page">Last</a>
					       		  </c:if>
					    	  </td>
  		  	  	   	  	  </tr>
  		  	  	   	  </table>
  		  	  	    </td>
  		  	  	  	<td align="right">
  		  	  	  		<c:if test="${sawalSaldo >= 1}">Search Result - ${noOfRecordsSaldo} Result</c:if>
  		  	  	  	</td>
  		  	  </tr>
  		  	  <tr>
  		  	  	  <td colspan="2">
					<div style="font-size:13px; font-family:times new roman;">
					<table id="rounded-corner" style="width:1075px"> 
						<thead>
							<tr align="center">
								<th style="width:200px; font-size: 12px;"><b>NO REKENING</b></th>
								<th style="width:250x; font-size: 12px;"><b>NAMA</b></th>
								<th style="width:100px; font-size: 12px;"><b>NO CIF</b></th>
								<th style="width:200px; font-size: 12px;"><b>TOTAL KELOLAAN DANA</b></th>
								<th style="width:100px; font-size: 12px;"><b></b></th>
							
								</tr>
						</thead>
						<tbody>
							<c:set var="inc" value="0" />
							<c:forEach var="lphSaldo" items="${lphSaldo}" >
							<tr style="font-size: 11px;">
								<c:set var="inc" value="${inc + 1}" />
								<td>${lphSaldo.branch}</td>
								<td>${lphSaldo.branch_name}</td>
								<td>${lphSaldo.acc_no}</td>
								<td>${lphSaldo.acc_no}</td>
								<td><button id="detail_aum" name="detail_aum" >INFO DETAIL</button></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</td>
  		 </tr>
<!--   		</div> -->
  	</table>
  </td>
 </tr>
</table>

<div id="dialog" title="DETAIL">
	<div style="float: left; text-align: left;">
		<fieldset style="width:95%">
			<legend> <b>INFO DETAIL TOTAL KELOLAAN DANA</b> </legend>
				<div style="font-size: 12px; font-family: times new roman; float: left;">

					<table id="rounded-corner" style="width:100%" align="center">
						<tr>
							<td>On Balance Sheet</td>
							<td>:</td>
							<td>${DetailCustom.CUST_AC_NO}</td>
							<td>Off Balance Sheet</td>
							<td>:</td>
							<td>${DetailCustom.acDesc}</td>
						</tr>
						<tr>
							<td>Tabungan</td>
							<td>:</td>
							<td>${DetailCustom.custNo}</td>
							<td>Sukuk</td>
							<td>:</td>
							<td>${DetailCustom.accountClass}</td>
						</tr>
						<tr>
							<td>Giro</td>
							<td>:</td>
							<td style="text-align: left;"><fmt:formatNumber type="number"
 								maxFractionDigits="3" value="${DetailCustom.amount}"/></td> 
							<td>Bancasurance</td>
							<td>:</td>
 							<td>${DetailCustom.remarks}</td> 
						</tr>
						<tr>
							<td>Deposito</td>
							<td>:</td>
							<td>${DetailCustom.effectiveDate}</td>
									</tr>
						<tr>
							<td>Financing</td>
							<td>:</td>
							<td>${DetailCustom.modNo}</td>
						</tr>
					</table>
				</div>
		</fieldset>
	</div>
</div>


</body>
</html>