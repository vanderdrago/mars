<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Iterator" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Simulasi Angsuran KPR 15 Tahun</title>
</head>
<body>
	<table width="100%">
		<tr>
			<td align="center">
				<table width="1000px" border="0">
					<tr>
						<td align="center" colspan="7"><b>JADWAL ANGSURAN KPR Muamalat iB 15 Tahun - Angsuran Ringan</b></td>
					</tr>
					<tr>
						<td width="150px"> Nama </td>
						<td width="10px">:</td>
						<td width="300px">${nama}</td>
						<td width="150px">Harga Jual</td>
						<td width="10px">:</td>
						<td width="300px"><fmt:formatNumber type="number" value="${totalAngsuran}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td width="150px"> Plafond Pembiayaan </td>
						<td width="10px">:</td>
						<td colspan="1" width="300px"><fmt:formatNumber type="number" value="${plafond}" maxFractionDigits="2"/></td>
						<td>Total Margin</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${totalMargin}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td width="150px"> Tanggal Simulasi </td>
						<td width="10px">:</td>
						<td colspan="1" width="300px">${tanggal}</td>
						<td width="150px">Angsuran Periode 1</td>
						<td width="10px">:</td>
						<td colspan="1" width="300px"><fmt:formatNumber type="number" value="${angsuranPertama}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td>Jangka Waktu</td>
						<td width="10px">:</td>
						<td width="400px">${jangkaWaktu} Bulan</td>
						<td width="200px">Angsuran Periode 2</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${angsuranKedua}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="200px">Angsuran Periode 3</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${angsuranketiga}" maxFractionDigits="0"/></td>
					</tr>
					
				</table>
				<table id="rounded-corner" style="width: 1000px">
					<thead>
				        <tr align="center">
				            <th style="width:30px; font-size: 12px;"><b>Bulan</b></th>
				            <th style="width:60px; font-size: 12px;"><b>O/S Pokok</b></th>
				            <th style="width:60px; font-size: 12px;"><b>AngPokok</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Margin</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Angsuran</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Rate</b></th>
				        </tr>
				     </thead>
				     <tbody>
				     		<c:forEach var="kpr" items="${kpr}" varStatus="row">
							<tr style="font-size: 11px;">
								<td>${row.index + 1}</td>
								<td>
									<c:if test="${row.index < 72}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.osPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.osKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 83}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.osKetiga}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 131}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.osKeempat}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 72}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.pokokPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.pokokKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 83}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.pokokKetiga}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 131}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.pokokKeempat}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 72}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.marginPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.marginKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 83}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.marginKetiga}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 131}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.MarginKeempat}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 72}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.TempAngsuranPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 83}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 131}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranKetiga}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 72}"> 
				     					10.75%
				     				</c:if>
				     				<c:if test="${row.index > 71 && row.index < 132}"> 
				     					11.75%
				     				</c:if>
				     				<c:if test="${row.index > 131}"> 
				     					14.17%
				     				</c:if>
								</td>
				     		</tr>
				     		</c:forEach>
				     </tbody>
				     <tfoot>
				     	<tr style="font-weight: bold">
				     		<td colspan="2" align="center"><b>TOTAL</b></td>
					    	<td style="text-align: left; " ><fmt:formatNumber type="number" maxFractionDigits="0" value="${totalPokok}" /></td>
					    	<td style="text-align: left; " ><fmt:formatNumber type="number" maxFractionDigits="0" value="${totalMargin}" /></td>
					    	<td style="text-align: left; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${totalAngsuran}" /></td>
					    	<td></td>
		    			</tr>
				     </tfoot>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>