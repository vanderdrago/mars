<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
	function valdata(){
			var	user;
// 				branch;
				user = document.getElementById('user').value;
// 				branch = document.getElementById ('branch').value;
			if (user == "" && branch == ""){
					alert ("Silahkan Authorized Id terlebih dahulu");
					return false;
				}
			return true;
		}
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center">	
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Monitoring Aktivitas Supervisor Versi Teller</b></legend>
						<form action="LapSpvAct.do" method="POST" name="jurnal">
							<table>
								<tr>  
									<td><input type="text" name="user" id="user" class="inputs" placeholder="Authorized Id" style=" width : 400px;"></td>
<!-- 									<td> Authorized Id :</td> -->
<!-- 									<td> <input type="text" id="user" name="user"></td> -->
								</tr>
								<tr>
								   <td align="right">
										<input onclick="return valdata()" type="submit" value="Cari" name="btn" id="ok" class="blue btn" />
										<input onclick="return valdata()" type="submit" value="Cetak" name="btndwnld" id="dwnld" class="blue btn" />
								   </td>
<!-- 										<input onclick="return valdata()" type="submit" id="ok" name="btn" value="Cari" /> -->
<!-- 										<input onclick="return valdata()" type="submit" id="dwnld" name="btndwnld" value="Cetak"> -->
								</tr>
								<tr>
									<td colspan="3">
				            			<logic:equal name="confrm" value="err">
											<p style="color: red;"> ${respon} </p>
										</logic:equal>	
				            		</td>
								</tr>
							</table>
						</form>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td align="center">
				<div style="font-size:13px; font-family:times new roman;">
					<table id="rounded-corner" style="width:1075px"> 
						<thead>
							<tr align="center">
								<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
								<th style="width:75px; font-size: 12px;"><b>BRANCH</b></th>
								<th style="width:100px; font-size: 12px;"><b>TANGGAL</b></th>
								<th style="width:100px; font-size: 12px;"><b>JAM</b></th>
								<th style="width:150px; font-size: 12px;"><b>TRN REF NO</b></th>
								<th style="width:100px; font-size: 12px;"><b>TRN CODE</b></th>
								<th style="width:100px; font-size: 12px;"><b>AC NO</b></th>
								<th style="width:150px; font-size: 12px;"><b>NO WARKAT</b></th>
								<th style="width:500px; font-size: 12px;"><b>DESC</b></th>
								<th style="width:20px; font-size: 12px; text-align: center;"><b>D/C</b></th>
								<th style="width:150px; font-size: 12px;"><b>NOMINAL</b></th>
								<th style="width:100px; font-size: 12px;"><b>USER ID</b></th>
								<th style="width:100px; font-size: 12px;"><b>AUTH ID</b></th>
							</tr>
						</thead>
						<tbody>
							<c:set var="inc" value="0" />
							<c:forEach var="lphTeller" items="${lphTeller}" >
							<tr style="font-size: 11px;">
								<c:set var="inc" value="${inc + 1}" />
								<td>${inc}</td>
								<td>${lphTeller.branch}</td>
								<td>${lphTeller.tanggal}</td>
								<td>${lphTeller.jam}</td>
								<td>${lphTeller.trn_ref_no}</td>
								<td>${lphTeller.trn_code}</td>
								<td>${lphTeller.ac_no}</td>
								<td>${lphTeller.no_warkat}</td>
								<td>${lphTeller.function_desc}</td>
								<td style="text-align: center;">${lphTeller.drcr_ind}</td>
								<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lphTeller.nominal}" /></td>
								<td>${lphTeller.user_id}</td>
								<td>${lphTeller.auth_id}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<logic:equal name="konfirmasi" value="err">
					<p style="color: red;">${dataresponse}</p>
				</logic:equal>
			</td>
		</tr>
	</table>
</body>
</html>