<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/css/demos.css" /> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,400' rel='stylesheet' type='text/css'> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <script src="http://code.jquery.com/jquery-1.12.1.min.js"></script> -->
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Facility</title>
</head>
<body>
	<table width="1075px" >
		<tr>
			<td>
				<h2>Detail Fasilitas</h2>
				<p>Click Button Detail Untuk Mengetahui Detail Dari No Kartu.</p>
				<table class="table table-striped" >
					<thead>
						<tr>
							<th style="width:50px; font-size: 10px; font-style: family font sains"><b>#</b></th>
							<th style="width:100px; font-size: 10px;"><b>NO FASILITAS</b></th>
							<th style="width:100px; font-size: 10px;"><b>NO KARTU</b></th>
							<th style="width:70px; font-size: 10px;"><b>NO REK ANGSURAN</b></th>
							<th style="width:250px; font-size: 10px;"><b>NAMA NASABAH</b></th>
							<th style="width:60px; font-size: 10px;"><b>PRODUCT CODE</b></th>
							<th style="width:70px; font-size: 10px;"><b>PRODUCT CATEGORY</b></th>
							<th style="width:70px; font-size: 10px;"><b>TANGGAL PENCAIRAN</b></th>
							<th style="width:100px; font-size: 10px;"><b>TANGGAL JATUH TEMPO</b></th>
							<th style="width:60px; font-size: 10px;"><b>STATUS KARTU</b></th>
						</tr>
					<thead>
					<tbody>
				    	<c:set var="inc" value="0" />
						<c:forEach var="lph" items="${lph}" >
							<tr>
								<c:set var="inc" value="${inc + 1}" />
								<td style="font-size: 12px">${inc}</td>
								<td style="font-size: 12px">${lph.fasilitasKe}</td>
								<td style="font-size: 12px">${lph.noKartu}</td>
								<td style="font-size: 12px">${lph.noRekAngsuran}</td>
								<td style="font-size: 12px">${lph.namaNasabah}</td>
								<td style="font-size: 12px">${lph.prodCode}</td>
								<td style="font-size: 10px">${lph.prodCategory}</td>
								<td style="font-size: 12px">${lph.tglPencairan}</td>
								<td style="font-size: 12px">${lph.tglJatuhTempo}</td>
								<c:choose>
									<c:when test="${lph.statusKartu == 'ACTIVE'}">
										<td><span class="label label-info">${lph.statusKartu}</span></td>
									</c:when>
									<c:when test="${lph.statusKartu == 'LIQUIDATE'}">
										<td><span class="label label-danger">${lph.statusKartu}</span></td>
									</c:when>
									<c:when test="${lph.statusKartu == 'REVESAL'}">
										<td><span class="label label-warning">${lph.statusKartu}</span></td>
									</c:when>
									<c:when test="${lph.statusKartu == 'HOLD'}">
										<td><span class="label label-danger">${lph.statusKartu}</span></td>
									</c:when>
								</c:choose>
								<td style="font-size: 12px">	
		<!-- 							<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
									<a href="/ReportMCB/detailKartu.do?noKartu=${lph.noKartu}" target="_blank" class="btn btn-success" role="button"> Detail <span class="glyphicon glyphicon-list-alt "></span></a>	
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
</body>

</html>