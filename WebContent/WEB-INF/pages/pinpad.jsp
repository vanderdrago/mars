<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css"> -->
<!--     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> -->
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<script type="text/javascript">
function valtgl(){
    var tgl1;
    var tgl2;
    tgl1 = document.getElementById('tgl1').value;
    tgl2 = document.getElementById('tgl2').value;

    if (tgl1 == "" && tgl2 == ""){
        alert('Silahkan isi tanggal');
        return false;
    } else if (tgl1 == ""){
    	alert('Silahkan isi tanggal 1');
    } else if (tgl2 == "") {
    	alert('Silahkan isi tanggal 2');
    }
    return true;
}

$(function() {
    $( "#tgl1" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
    $( "#tgl2" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>




<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table border="0" width="100%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Transaksi EDC</b> </legend>
					<form action="pinpadAct.do" method="POST" name="jurnal">
					<table>
				    	<tr>
				    		<td>
					    		<table>
					    			<tr>
					    				<td><input type="text" name="termId" id="termId" class="inputs" placeholder="Input Terminal Id" style=" width : 400px;"></td>					    				
					    			</tr>
<!-- 					    			<tr> -->
<!-- 					    				<td>  -->
<!-- 					    					<select class="chosen" name="category" style=" width : 420px;"> -->
<%--                                            		<c:forEach items="${name}" var="category"> --%>
<%--                                            			<option value="${category.merchantName}"> ${category.merchantName} </option> --%>
<%--                                            		</c:forEach> --%>
<!-- 										      </select>         -->
<!-- 					    				</td> -->
<!-- 					    			</tr> -->
					    		</table>
					    	</td>
					    </tr>
					    <tr>
				            <td><input type="text" name="tgl1" id="tgl1" class="inputs" placeholder="Silahkan Pilih Mulai Tanggal (DD-MM-YYYY)" style=" width : 400px;"></td>
				        </tr>
				        <tr>
				            <td><input type="text" name="tgl2" id="tgl2" class="inputs" placeholder="Silahkan Pilih Hingga Tanggal (DD-MM-YYYY)" style=" width : 400px;"></td>
				        </tr>
				        <tr>
				            <td align="center">
				                <input onclick="return valtgl()" type="submit" value="Cari" name="btn" id="ok" class="blue btn" />
				                <input onclick="return valtgl()" type="submit" value="Cetak" name="btndwnld" id="dwnld" class="blue btn" />
				            </td>
				        </tr>
				        <tr>
							<td colspan="3">
		            			<logic:equal name="confrm" value="err">
									<p style="color: red;"> ${respon} </p>
								</logic:equal>	
		            		</td>
						</tr>
					</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color:black">
			<td align="center">
				<table border="0" width="1075px">
					<tr>
						<td align="center" colspan="2">
						<div style="font-size:13px; font-family:times new roman;">
							<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:80px; font-size: 12px;"><b>TRN DT</b></th>
										<th style="width:100px; font-size: 12px;"><b>TERMINAL ID</b></th>
										<th style="width:100px; font-size: 12px;"><b>AC NO</b></th>
										<th style="width:100px; font-size: 12px;"><b>MERCHANT ID</b></th>
										<th style="width:100px; font-size: 12px;"><b>TRN REF NO</b></th>
										<th style="width:100px; font-size: 12px;"><b>CARD NO</b></th>
										<th style="width:100px; font-size: 12px;"><b>STATUS</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="detail" items="${detail}" >
									<tr>
										<c:set var="inc" value="${inc + 1}" />
										<td style="font-size: 10px">${inc}</td>
										<td style="font-size: 10px">${detail.trnDt}</td>
										<td style="font-size: 10px">${detail.termId}</td>
										<td style="font-size: 10px">${detail.custAcNo}</td>
										<td style="font-size: 10px">${detail.idMerc}</td>
										<td style="font-size: 10px">${detail.trnRefNo}</td>
										<td style="font-size: 10px">${detail.accNo}</td>
										<td style="font-size: 10px">${detail.trnType}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<logic:equal name="konfirmasi" value="err">
							<p style="color: red;">${dataresponse}</p>
						</logic:equal>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>