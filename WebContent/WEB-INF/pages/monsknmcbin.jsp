<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemskncb(){
   var amont_a;
    var amont_b;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}
</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h2 align="center">Monitoring & Reporting SKN MCB Incoming</h2>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <form action="/ReportMCB/vMonSKNMcbIn.do" method="POST" name="mskn">
	    
 <table border="0" width="100%">
	<tr>
		<td style="width: 240px">No Referensi</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="rel_trn" id="rel_trn"></td>
	</tr>

	<!-- <tr>
		<td style="width: 240px">From Account</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="rek_f_ac" id="rek_f_ac"
			maxlength="16"><label style="color: red"></label></td>
	</tr> -->

	<tr>
		<td style="width: 240px">No Account</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="rek_t_ac" id="rek_t_ac"
			maxlength="16"><label style="color: red"></label></td>
	</tr>
	
	<tr>
		<td style="width: 240px">Start Amount</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="amont_a" id="amont_a"
			maxlength="16"><label style="color: red"></label></td>
	</tr>
	<tr>
		<td style="width: 240px">To Amount</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="amont_b" id="amont_b"
			maxlength="16"><label style="color: red"></label></td>
	</tr>

	<tr>
		<td colspan="2"></td>
		<td><input onclick="return validatemskncb()" type="submit" value="Ok" name="ok" id="ok" /> <!-- onclick="return validatemrtgs()" -->
		</td>
	</tr>
</table>
</form>
            <br />
       <div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:100%">
               <tr ><td>
					<display:table name="ltrn" id="ltrn" class="wb" requestURI="MonSknMcbInForward.do" pagesize="20" sort="external">
					<display:column title="<h3>Tanggal</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.trn_dt}</display:column>
					<display:column title="<h3>TRN Ref No</h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.trn_ref_no}</display:column>
					<display:column title="<h3>Event</h3>" sortable="true" style="width:50px; ">${ltrn.event}</display:column> 
					<display:column title="<h3>Module</h3>" sortable="true" style="width:50px; ">${ltrn.module}</display:column>         
					<display:column title="<h3>Acc Branch</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.ac_branch}</display:column>
					<display:column title="<h3>TRN Code</h3>" sortable="true" style="width:70px; ">${ltrn.trn_code}</display:column>
					<display:column title="<h3>Batch No</h3>" sortable="true" style="width:70px; ">${ltrn.batch_no}</display:column>
					<display:column title="<h3>User ID</h3>" sortable="true" style="width:70px; ">${ltrn.user_id}</display:column>
					<display:column title="<h3>Auth ID</h3>" sortable="true" style="width:70px; ">${ltrn.auth_id}</display:column>
					<display:column title="<h3>Auth Status</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.auth_stat}</display:column>
					<display:column title="<h3>Acc No</h3>" sortable="true" style="width:70px; ">${ltrn.ac_no}</display:column>
					<display:column title="<h3>Acc Desc</h3>" sortable="true" style="width:170px; ">${ltrn.ac_desc}</display:column>
					<display:column title="<h3>Db/ Cr</h3>" sortable="true" style="width:50px; text-align:center;">${ltrn.drcr_ind}</display:column>
					<display:column title="<h3>Amount</h3>" sortable="true" style="width:130px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.lcy_amount}" /></display:column>
					</display:table></td>
                </td>
                </tr>
           </table>
        </div>
     <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
	</logic:equal>
</body>
</html>