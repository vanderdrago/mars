<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.24.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link rel="stylesheet" href="/ReportMCB/css/report/table.css">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DETAIL PERAGAAN ACCOUNT</title>
</head>
<body>
	<div style="float: center; text-align: center;">
		<table width="100%" border="0">
			<tr>
				<td align="center">
					<table id="rounded-corner"  border="0.5" align="center" style=" width : 1075px;">
						<thead>
							<tr align="left" style="width: 70px; height: 25px">
								<th style=" font-size: 12px;"><b>NO. REKENING</b></th>
								<th style=" font-size: 12px;"><b>NAMA</b></th>
								<th style="font-size: 12px;"><b>NO. CIF</b></th>
								<th style=" font-size: 12px;"><b>CURRENCY</b></th>
								<th style="font-size: 12px;"><b>JENIS REKENING</b></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${detailAccount.noRek}</td>
								<td>${detailAccount.nama}</td>
								<td>${detailAccount.noCif}</td>
								<td>${detailAccount.ccy}</td>
								<td>${detailAccount.accountClass}</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		
		<table width="100%" border="0">
			<tr>
				<td align="center">
					<fieldset style="width:1075px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    				<legend> <b></b> </legend>
	    				
	    				<table width="1075px" border="0.5" id="rounded-corner">
	    					<tr>
								<th style="font-size: 20px;align="center"; colspan=7 ;"><b><h3><center>DETAIL DEPOSITO</center></h3></b></th>
							</tr>
							<tr >
								<td >NO. REKENING</td>
								<td >:</td>
								<td >${detailAccount.noRek}</td>
								<td >NAMA LENGKAP</td>
								<td >:</td>
								<td>${detailAccount.nama}</td>
							</tr>
	    				</table>
	    			</fieldset>
				</td>
			</tr>
		</table>
		
	</div>
</body>
</html>