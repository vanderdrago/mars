<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<script type='text/javascript' src='/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<script type='text/javascript' src='/ReportMCB/scripts/money/jquery.js' ></script>
<script type="text/javascript">
$(function() {
    $( "#bsnsdt" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'yymmdd'
    });
});


function valInput(){
    var bsnsdt = document.getElementById('bsnsdt').value;
    var kdcab = document.getElementById('kdcab').value;
    var rpt = document.getElementById('rpt').value;

    if (bsnsdt == ""){
        alert('Business date tidak boleh kosong');
        return false;
    }
    if (kdcab == ""){
        alert('Silahkan isi Kode Cabang');
        return false;
    }
    if (rpt == ""){
        alert('Silahkan pilih Report');
        return false;
    }
    return true;
}


function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div style="font-size: 11px;">
<table border="0" align="center">
	<tr>
		<td>			
			<fieldset style="width:300px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			<legend> <b>Report Retur Skn Incoming</b> </legend>
			<form action="sknInRepRetTtpnView.do" method="POST" name="jurnal">
				<table border="0">
					<tr>
						<td>Busines Date</td>
						<td>:</td>
						<td><input type="text" name="bsnsdt" id="bsnsdt" readonly="true"></td>
					</tr>
					<tr><td width="90px;">Branch</td>
						<td>:</td>
						<td width="180px;"><input type="text" name="kdcab" id="kdcab" maxlength="3"></td>	
					</tr>
					<tr><td>Report</td>
						<td>:</td>
						<td><select name="rpt" id="rpt">
							  <option value="">Pilih</option>
							  <option value="03">Retur</option>
							  <option value="05">Titipan</option>
							</select> </td>	
					</tr>
					<tr>
						<td colspan="2"></td>
						<td>
							<input onclick="return valInput()" type="submit" value="Lihat" name="btn" id="idbtn" />
							<input onclick="return valInput()" type="submit" value="Download" name="btn" id="download" />
						</td>						
					</tr>
				</table>
			</form>
			</fieldset>
		</td>
	</tr>
</table>
<logic:equal name="respon" value="teks">
<br />
${teks }
</logic:equal>

<logic:equal name ="data" value="out">
<table border="0" align="center" width="1075px;">
	<tr>
		<td>
			
	<div style="  float:left; text-align: left; ">
	<table border="0">
		<tr>
	    	<td><a href="mensknInRepRetTtpnFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="mensknInRepRetTtpnFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="mensknInRepRetTtpnFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="mensknInRepRetTtpnFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="mensknInRepRetTtpnFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style="width:500px;    float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr style="font-size: 11px;">
					            <th style="width:20px; "><b>NO</b></th>
					            <th style="width:45px; "><b>NO. REFF</b></th>
					            <th style="width:45px;"><b>SOR</b></th>
					            <th style="width:30px; "><b>SANDI BANK PENGIRIM</b></th>
					            <th style="width:120px; "><b>NAMA PENGIRIM</b></th>
					            <th style="width:120px; "><b>NAMA PENERIMA</b></th>					            
								<logic:equal name ="clm" value="05">
					            <th style="width:120px; "><b>NAMA PENERIMA CORE BMI</b></th>	
								</logic:equal>
					            <th style="width:45px; "><b>REKEING TUJUAN</b></th>	
					            <th style="width:100px; text-align:right; "><b>NOMINAL</b></th>
					            <th style="width:300px; "><b>KETERANGAN</b></th>
					            <th style="width:40px; "><b>STATUS</b></th>		            
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="ltrnsknin" items="${ltrnsknin}" >
						        <tr style="font-size: 11px; ">
						            <td >${ltrnsknin.no}</td>
						            <td >${ltrnsknin.noref}</td>
						            <td >${ltrnsknin.sor}</td>
						            <td >${ltrnsknin.sbp}</td>
						            <td >${ltrnsknin.nm_pengirim}</td>
						            <td >${ltrnsknin.nm_penerima}</td>
						            <logic:equal name ="clm" value="05">
						            <td style="width:120px; ">${ltrnsknin.nm_penerima_core}</td>	
									</logic:equal>
						            <td >${ltrnsknin.rek_tujuan}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrnsknin.nominal}" /></td>						            
						            <td >${ltrnsknin.ket}</td>
						            <td >${ltrnsknin.sts}</td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>			
		</td>
	</tr>
</table>
</logic:equal>

<logic:equal value="rekap" name="data">
	<br />
	<b>Report Titipan SKN Incoming</b> <br />
	<b><%=new java.text.SimpleDateFormat("dd MMM yyyy").format(new java.util.Date()) %></b>
	<table border="0" align="center" width="620px;">
		<tr>
			<td>
				<table id="rounded-corner" >
				    <thead>
				        <tr style="font-size: 11px;">
				            <th style="width:20px; "><b>NO</b></th>
				            <th style="width:50px; "><b>CABANG</b></th>
				            <th style="width:150px;"><b>REPORT</b></th>
				            <th style="width:100px; text-align:center;"><b>TOTAL TRNS</b></th>
				            <th style="width:100px; text-align:right; "><b>NOMINAL</b></th>
				            <th style="width:100px; "><b>DOWLOAD</b></th>		            
				        </tr>
				    </thead>
			
				    <tbody>
				    	<c:forEach var="lrkpskn" items="${lrkpskn}" >
					        <tr style="font-size: 11px; ">
					            <td >${lrkpskn.no}</td>
					            <td >${lrkpskn.cab_ssl}</td>
					            <td >${lrkpskn.desc_rep}</td>
					            <td style="text-align:center;">${lrkpskn.tottrns}</td>
					            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lrkpskn.nominal}" /></td>						            
					            <td >
					            	<a href="/ReportMCB/sknInRepRetTtpnView.do?btn=xx&cab=${lrkpskn.cab_ssl}&rpt=${lrkpskn.p_code}">Download</a>
								</td>
					        </tr>
					    </c:forEach>
				    </tbody>
			      </table>		
			</td>
		</tr>
	</table>
</logic:equal>

</div>

</body>
</html>