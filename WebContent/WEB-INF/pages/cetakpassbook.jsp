<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<script type='text/javascript' src='/ReportMCB/jsajax/CetakPassbook.js'></script>
<script type='text/javascript' src='/ReportMCB/jsajax/jquery.isloading.min.js'></script>
<script type='text/javascript' src='/ReportMCB/jsajax/jquery.blockUI.js'></script>


<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<html>
<head>
<style type="text/css">
.datatrans {
  color: black;
  font-family:Times New Roman;
  font-size: 8pt;
  
} 
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cetak Statement</title>
</head>
<body>

<table border="0" align="center">
	<tr>
		<td> 
			<fieldset style="width:350px;  border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			    <legend> <b>Cetak Passbook</b> </legend>
			    <!-- form action="cetakPassbookAct.do" method="POST" name="lsp" -->
			    <table>
			        <tr>
			            <td>No. Rekening</td>
			            <td>:</td>
			            <td colspan="2" ><input type="text" name="acc" id="acc" maxlength="10" onKeyup="checkDec(this);" ><label style="color:red">*</label></td>
			        </tr>
			        <tr>
			            <td colspan="2"> </td>
			            <td colspan="2">
			                <input type="submit" value="Lihat" name="btn" id="print" onclick="doAjaxPost()"/>			                			                
			            </td>
				   </tr>
			    </table>
			    <!-- /form -->
			</fieldset>
		</td>
	</tr>
</table>

<table border="0" width="100%">
	<tr>
		<td align="center">
			<div class="data" ></div>	<br />
			<div class="datatrans"></div>			
		</td>
	</tr>
</table>

<logic:equal name="data" value="konfrm">
	<b>${teks}</b>
</logic:equal>

<logic:equal name="data" value="viewdata">
<table border="0" align="center" width="790px;">
	<tr>
		<td>
			<div style="  float:left; text-align: left; ">
			<table border="0">
				<tr>
			       	<td>
			       		<c:if test="${currentPage lt noOfPages}"><a href="manageCtkPssbookFwd.do?page=${currentPage + 1}">Next</a></c:if>
			       	</td>
			    </tr>
			 </table>
			</div>
			<div style="width:500px;    float:right; text-align: right"><br />
				<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
			</div>
			<br />
			<div style="font-size:6px; float:left;">   
			
			<table id="rounded-corner" style="width: 100%;">
				<thead>
					<tr align="center">
						<th style="width:20px; font-size: 12px;" ><b>No</b></th>
						<th style="width:60px; font-size: 12px;"><b>Tanggal</b></th>
						<th style="width:40px; font-size: 12px;"><b>Sandi</b></th>
						<th style="width:200px; font-size: 12px; text-align: right;"><b>Mutasi</b></th>
						<th style="width:300px; font-size: 12px;"><b>Keterangan</b></th>
						<th style="width:100px; font-size: 12px; text-align: right;"><b>Saldo</b></th>
						<th style="width:60px; font-size: 12px;"><b>Pengesah</b></th> 
					</tr>
				</thead>
				<tbody>
					<c:forEach var="lhist" items="${lhist}" >
					<tr>						
						<td >${lhist.no_s }</td>
						<td >${lhist.trn_dt_s}</td>
						<td >${lhist.trn_code}</td>
						<td style="text-align: right;">${lhist.mutasi_s} &nbsp; ${lhist.drcr_ind}</td>
						<td >${lhist.detail}</td>
						<td style="text-align: right;">${lhist.saldo_s}</td>
						<td >${lhist.auth_id}</td>
					</tr>
					</c:forEach>					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="7"><a href="manageCtkPssbookFwd.do?cetak=${sawal}">Cetak</a></td>
					</tr>
				</tfoot>
			</table>
							    
			</div>
		</td>
	</tr>
</table>   
</logic:equal> 
</body>
</html>