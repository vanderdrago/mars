<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<script type='text/javascript' src='/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<script type='text/javascript' src='/ReportMCB/scripts/money/jquery.js' ></script>
<script type="text/javascript">
$(function() {
    $( "#bsnsdt" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'yymmdd'
    });
});


function valInput(){
    var bsnsdt;
    bsnsdt = document.getElementById('bsnsdt').value;

    if (bsnsdt == ""){
        alert('Business date tidak boleh kosong');
        return false;
    }
    return true;
}


function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}

function dis(){
	document.getElementById("kdcab").disabled=true;
	document.getElementById("idtmmbr").disabled=true;
	document.getElementById("lss").disabled=false;
    document.getElementById('kdcab').value = '';
}
function ena(){
	document.getElementById("kdcab").disabled=false;
	document.getElementById("idtmmbr").disabled=false;
	document.getElementById("lss").disabled=true;
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div style="font-size: 11px;">
<table border="0" align="center">
	<tr>
		<td>			
			<fieldset style="width:1000px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			<legend> <b>Interface SKN</b> </legend>
			<form action="sknInterfaceView.do" method="POST" name="jurnal">
				<table border="0">
					<tr>
						<td colspan="10">
							<input type="radio" name="skn" value="out" onclick="ena()" checked="checked">Skn Outgoing &nbsp;
							<input type="radio" name="skn" value="in" onclick="dis()">Skn Incoming
						</td>
					</tr>
					<tr>
						<td>Busines Date</td>
						<td>:</td>
						<td><input type="text" name="bsnsdt" id="bsnsdt" readonly="true"></td>						
						<td>Business Type</td>
						<td>:</td>
						<td>
							<select name="trn" id="idtrn">
							  <option value="">Pilih</option>
							  <option value="1">1</option>
							  <option value="2">2</option>
							  <option value="3">3</option>
							</select> 
						</td>	
						<td>Amount From</td>
						<td>:</td>
						<td><input type="text" name="amtf" id="amtf" maxlength="17">						
							<script type="text/javascript">$("#amtf").maskMoney();</script>		                    
		                </td>
						<td rowspan="4" valign="top" align="left">
							<fieldset style="width: 199px;">									
									<table>
							        <tr>
							            <th ><b>STS</b></th>
							            <th ><b>TOTAL</b></th>
							            <th ><b>NOMINAL</b></th>				            
							        </tr>	
							    	<c:forEach var="lrkprtgs" items="${lrkprtgs}" >
								        <tr style="font-size: 10px; ">
								            <td >${lrkprtgs.status}</td>
								            <td >${lrkprtgs.tottrans}</td>
								            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lrkprtgs.totnominal}" /></td>
								        </tr>
								    </c:forEach>
						      		</table>
							</fieldset>
						</td>	
					</tr>
					<tr><td width="90px;">Branch</td>
						<td>:</td>
						<td width="180px;"><input type="text" name="kdcab" id="kdcab" maxlength="3"></td>						
						<td>No. Ref</td>
						<td>:</td>
						<td ><input type="text" name="reltrn" id="idreltrn" maxlength="16"></td>							
						<td>Amount To</td>
						<td>:</td>
						<td>
							<input type="text" name="amtto" id="amtto" maxlength="17">
							<script type="text/javascript">$("#amtto").maskMoney();</script>
						</td>	
					</tr>
					<tr><td>Account</td>
						<td>:</td>
						<td><input type="text" name="acc" id="acc" maxlength="24"></td>			
						<td>To Member</td>
						<td>:</td>
						<td>
							<select name="tmmbr" id="idtmmbr" >
		                        <option value="">Pilih</option>
		                        <c:forEach items="${lmmbrbnk}" var="lmmbrbnk">
		                             <option value=${lmmbrbnk.kode_member}>${lmmbrbnk.kode_member}</option>
		                        </c:forEach>
		                    </select>
						</td>	
						<td></td>
						<td></td>
						<td>
						</td>
					</tr>
					<tr>
						<td>Txn Status</td>
						<td>:</td>
						<td>
							<select name="txnsts" id="idtxnsts">
							  <option value="">Pilih</option>
							  <option value="82">Successfull</option>
							  <option value="21">Maintenance operator</option>
							  <option value="31">Maintenance supervisor)</option>
							  <option value="11">Ready to TPK</option>
							</select> 
						</td>
						<td>Sect Server</td>
						<td>:</td>
						<td>
							<select name="lss" id="lss" disabled="disabled">
		                        <option value="">Semua</option>
		                        <c:forEach items="${lss}" var="lss">
		                             <option value=${lss.pc_code}>${lss.pc_desc}</option>
		                        </c:forEach>
		                    </select>
						</td>
						<td colspan="3">
							<input onclick="return valInput()" type="submit" value="Lihat" name="btn" id="idbtn" />
							<input onclick="return valInput()" type="submit" value="Download" name="btn" id="download" />
						</td>
						
					</tr>
				</table>
			</form>
			</fieldset>
		</td>
	</tr>
</table>
<logic:equal name="respon" value="teks">
<br />
${teks }
</logic:equal>

</div>

</body>
</html>