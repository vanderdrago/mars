<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/toolTipText.css" rel="stylesheet" type="text/css"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Informasi Kurs</title>
</head>
<body>
<!-- 	<table border="0" width="80%"> -->
<!-- 		<tr> -->
<!-- 			<td align="center"> -->
<!-- 				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;"> -->
<!-- 					<legend align="center"><b>Informasi Kurs Hari Ini</b></legend> -->
<!-- 					<form action="KursAct.do" name="jurnal" method="POST"> -->
<!-- 						<table border="0"> -->
<!-- 							<tr> -->
<!-- 								<td> -->
<!-- 									<input type="text" name="branch" id="branch" class="inputs" placeholder="Silahkan Masukan Branch Code" style=" width : 400px;"> -->
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 							<tr> -->
<!-- 								<td align="center"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td> -->
<!-- 							</tr> -->
<!-- 						</table> -->
<!-- 					</form> -->
<!-- 				</fieldset> -->
<!-- 			</td> -->
<!-- 		</tr> -->
		
		
<!-- 		<tr style="color: black;">  		 -->
<!-- 			<td align="center"> -->
<!-- 				<table border="0" width="1075"> -->
<!-- 					<tr> -->
<!-- 						<td> -->
							
<!-- 						</td> -->
<!-- 						<td align="right"> -->
<%-- 							<c:if test="${sawal >= 1}">Search Result - ${noOfRecords} Result</c:if> --%>
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td colspan="2"> -->
<!-- 							<table id="rounded-corner" style="width: 1075px;"> -->
<!-- 							<thead> -->
<!-- 								<tr align="center"> -->
<!-- 									<th style="width:50px; font-size: 12px;" ><b>NO</b></th> -->
<!-- 									<th style="width:80px; font-size: 12px;"><b>BRANCH</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;"><b>RATE DATE</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;" ><b>FCY AMOUNT</b></th> -->
<!-- 									<th style="width:100px; font-size: 12px;"><b>LCY AMOUNT</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;"><b>RATE TYPE</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;"><b>BUY SPREAD</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;"><b>SALE SPREAD</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;"><b>BUY RATE</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;"><b>SALE RATE</b></th> -->
<!-- 								</tr> -->
<!-- 							</thead> -->
<!-- 							<tbody> -->
<%-- 								<c:set var="inc" value="0" /> --%>
<%-- 								<c:forEach var="lph" items="${lph}" > --%>
<!-- 									<tr> -->
<%-- 										<c:set var="inc" value="${inc + 1}" /> --%>
<%-- 										<td>${inc}</td> --%>
<%-- 										<td>${lph.branch}</td> --%>
<%-- 										<td>${lph.rateDate}</td> --%>
<%-- 										<td>${lph.fcyAmount}</td> --%>
<%-- 										<td>${lph.lcyAmount}</td> --%>
<%-- 										<td>${lph.rateType}</td> --%>
<%-- 										<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.buySpread}" /> --%>
<%-- 										<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.saleSpread}" /> --%>
<%-- 										<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.buyRate}" /> --%>
<%-- 										<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.saleRate}" /> --%>
<!-- 									</tr> -->
<%-- 								</c:forEach> --%>
<!-- 							</tbody> -->
<!-- 							</table> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 				</table> -->
<!-- 			</td> -->
<!-- 		  </tr> -->
	
<!-- 	</table> -->
	<table border = "0">
		<tr>	
			<td align="center">
				<fieldset style="width:1050px; border:0px solid #999; border-radius:8px; box-shadow:0 0 5px #999;">
	    			<legend> <b></b> </legend>
	    			<table id="rounded-corner"  >
	    				<tr>
	    					<th style="font-size: 20px;align="center"; colspan=7 ;"><b><h3><center>INFORMASI KURS</center></h3></b></th>
	    				</tr>
	    			</table>
	    		</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<table id="rounded-corner"  border="0.5" align="center" style=" width : 1070px;">
					<thead>
						<tr>
							<th style="width:50px; font-size: 12px;" ><b>NO</b></th>
<!-- 							<th style="width:80px; font-size: 12px;"><b>BRANCH</b></th> -->
							<th style="width:150px; font-size: 12px;"><b>RATE DATE</b></th>
							<th style="width:150px; font-size: 12px;" ><b>FCY AMOUNT</b></th>
							<th style="width:100px; font-size: 12px;"><b>LCY AMOUNT</b></th>
							<th style="width:150px; font-size: 12px;"><b>RATE TYPE</b></th>
							<th style="width:150px; font-size: 12px;"><b>BUY SPREAD</b></th>
							<th style="width:150px; font-size: 12px;"><b>SALE SPREAD</b></th>
							<th style="width:150px; font-size: 12px;"><b>BUY RATE</b></th>
 							<th style="width:150px; font-size: 12px;"><b>SALE RATE</b></th>
						</tr>
					</thead>
					<tbody> 
						<c:set var="inc" value="0" /> 
						<c:forEach var="lph" items="${lph}" > 
							<tr> 
								<c:set var="inc" value="${inc + 1}" />
								<td>${inc}</td>
<%-- 								<td>${lph.branch}</td> --%>
								<td>${lph.rateDate}</td>
								<td>${lph.fcyAmount}</td>
								<td>${lph.lcyAmount}</td>
								<td>${lph.rateType}</td>
								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.buySpread}" />
								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.saleSpread}" />
								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.buyRate}" />
								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.saleRate}" />
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</td>
		</tr>
<!-- 	</table> -->
<!-- 	<table width="100%" border="0"> -->
<!-- 		<tr> -->
<!-- 			<td align="center"> -->
<!-- 				<table id="rounded-corner"  border="0.5" align="center" style=" width : 1070px;"> -->
<!-- 					<thead>	 -->
<!-- 						<tr> -->
<!-- 							<th style="width:50px; font-size: 12px;" ><b>NO</b></th>  -->
<!-- 							<th style="width:80px; font-size: 12px;"><b>BRANCH</b></th> -->
<!-- 							<th style="width:150px; font-size: 12px;"><b>RATE DATE</b></th> -->
<!-- 							<th style="width:150px; font-size: 12px;" ><b>FCY AMOUNT</b></th> -->
<!-- 							<th style="width:100px; font-size: 12px;"><b>LCY AMOUNT</b></th> -->
<!-- 							<th style="width:150px; font-size: 12px;"><b>RATE TYPE</b></th> -->
<!-- 							<th style="width:150px; font-size: 12px;"><b>BUY SPREAD</b></th> -->
<!-- 							<th style="width:150px; font-size: 12px;"><b>SALE SPREAD</b></th> -->
<!-- 							<th style="width:150px; font-size: 12px;"><b>BUY RATE</b></th> -->
<!-- 							<th style="width:150px; font-size: 12px;"><b>SALE RATE</b></th> -->
<!-- 						</tr> -->
<!-- 					</thead> -->
<!-- 					<tbody> -->
<%-- 						<c:set var="inc" value="0" />  --%>
<%-- 						<c:forEach var="lph" items="${lph}" >  --%>
<!-- 							<tr>  -->
<%-- 								<c:set var="inc" value="${inc + 1}" /> --%>
<%-- 								<td>${inc}</td> --%>
<%-- 								<td>${lph.branch}</td> --%>
<%-- 								<td>${lph.rateDate}</td> --%>
<%-- 								<td>${lph.fcyAmount}</td> --%>
<%-- 								<td>${lph.lcyAmount}</td> --%>
<%-- 								<td>${lph.rateType}</td> --%>
<%-- 								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.buySpread}" /> --%>
<%-- 								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.saleSpread}" /> --%>
<%-- 								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.buyRate}" /> --%>
<%-- 								<td><fmt:formatNumber type="number"maxFractionDigits="2" value="${lph.saleRate}" /> --%>
<!-- 							</tr> -->
<%-- 						</c:forEach> --%>
<!-- 					</tbody> -->
<!--  				</table>	 -->
<!--  			</td> -->
<!--  		</tr> -->
	</table>
</body>
</html>