<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<a href="bukujurnal.do"><span>Batch Detail MCB</span></a>
        <table border="0" width="100%">
            <tr>
                <td align="center" colspan="3"><b>${title} </b></td>
            </tr>
          
          <tr>
          <td height="60px">  </td>
          </tr>  
            

          

 <div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:100%">
               <tr ><td>

<display:table name="lbd" id="lbd" class="wb" requestURI="manageLbbFwd2.do" pagesize="35" sort="external" >
<display:column title="VALUE DATE" sortable="true" style="width:70px;">${lbd.value_dt}</display:column>
<display:column title="NO REFERENSI" sortable="true" style="width:100px;">${lbd.trn_ref_no}</display:column>
<!--<display:column title="EVENT" sortable="true" style="width:70px;">${lbd.event}</display:column> -->
<display:column title="CABANG" sortable="true" style="width:70px;">${lbd.ac_branch}</display:column>
<display:column title="CURRENCY" sortable="true" style="width:100px;">${lbd.ac_ccy}</display:column>
<display:column title="AMOUNT" sortable="true" style="width:70px;"> <fmt:formatNumber type="number" maxFractionDigits="2" value = "${lbd.lcy_amount}" /> </display:column>
<display:column title="TRN CODE" sortable="true" style="width:70px;">${lbd.trn_code}</display:column>
<display:column title="NARRATIVE" sortable="true" style="width:300px;">${lbd.remarks}</display:column>
<display:column title="DR/CR" sortable="true" style="width:70px;">${lbd.drcr_ind}</display:column>


</display:table>
</td>
</tr>
</table>
</body>
</html>