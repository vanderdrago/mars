<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">

function valdata(){
    var kode;
    kode = document.getElementById('kode').value;
    if (kode == ""){
        alert('Silahkan masukkan KODE CABANG terlebih dahulu');
        return false;
    }
    return true;
}

</script> 

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<table border='0' width="100%">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
				<legend align="center"><b>Peragaan Akhir User Teller dan Back Office</b></legend>
				<form action = "ViewUserAktifCabang.do" method = "POST" name = "cabang">
				<table>
					<tr>
						<td>Kode Cabang :</td>
						<td><input type = "text" name = "kode" id = "kode"></td>
						<td>
						    <input onclick="return valdata()" type="submit" value="Cari" name="btn" id="ok" />
						</td>
					</tr>
				</table>
				</form>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td align="center">
			<div style="font-size:13px; font-family:times new roman;">
	           <table id="rounded-corner" style="width:650px;">
	           		<tr>
	           			<td align="center">
	           				<div style="font-size:18px; font-family:times new roman;">LIST USER AKTIF TODAY</div>
	           			</td>
	           		</tr>
	           		<tr>
	               		<td>
							<display:table name="lph" id="lph" requestURI="ManageUser.do" pagesize="30" sort="external" >
					            <display:column title="<b>KODE CABANG</b>" style="width:100px; ">${lph.kode_cabang}</display:column>          
					            <display:column title="<b>CABANG</b>" style="width:300px; ">${lph.cabang}</display:column> 
					            <display:column title="<b>TANGGAL</b>" style="width:100px;">${lph.tanggal}</display:column>
					            <display:column title="<b>USER ID</b>"  style="width:150px; ">${lph.user_id}</display:column>
			        		</display:table> 	
						</td>
	               </tr>
	            </table>
			</div>
				<logic:equal name="konfirmasi" value="err">
					<p style="color: red;">${dataresponse}</p>
				</logic:equal>
			</td>
		</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>