<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemskncb(){
   var amont_a;
    var amont_b;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}
</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h2 align="center">Monitoring & Reporting SKN MCB</h2>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <form action="/ReportMCB/vMonSKNMcb.do" method="POST" name="mskn">
	    
 <table border="0" width="100%">
	<tr>
		<td style="width: 240px">No Referensi</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="rel_trn" id="rel_trn"></td>
	</tr>

	<tr>
		<td style="width: 240px">From Account</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="rek_f_ac" id="rek_f_ac"
			maxlength="16"><label style="color: red"></label></td>
	</tr>

	<tr>
		<td style="width: 240px">To Account</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="rek_t_ac" id="rek_t_ac"
			maxlength="16"><label style="color: red"></label></td>
	</tr>
	
	<tr>
		<td style="width: 240px">Start Amount</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="amont_a" id="amont_a"
			maxlength="16"><label style="color: red"></label></td>
	</tr>
	<tr>
		<td style="width: 240px">To Amount</td>
		<td style="width: 21px">:</td>
		<td><input type="text" name="amont_b" id="amont_b"
			maxlength="16"><label style="color: red"></label></td>
	</tr>

	<tr>
		<td colspan="2"></td>
		<td><input onclick="return validatemskncb()" type="submit" value="Ok" name="ok" id="ok" /> <!-- onclick="return validatemrtgs()" -->
		</td>
	</tr>
</table>
</form>
            <br />
       <div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:100%">
               <tr ><td>
					<display:table name="ltrn" id="ltrn" class="wb" requestURI="MonSknMcbForward.do" pagesize="20" sort="external">               
					<display:column title="<h3>No Contract</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.contractRefNo}</display:column>
					<display:column title="<h3>MCB TRN </h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.udf2}</display:column>
					<display:column title="<h3>From Acc No</h3>" sortable="true" style="width:80px; ">${ltrn.custAcNo}</display:column> 
					<display:column title="<h3>From Acc Name</h3>" sortable="true" style="width:170px; ">${ltrn.custName}</display:column>         
					<display:column title="<h3>Bank Code</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.cptyBankcode}</display:column>
					<display:column title="<h3>Benef Acc No</h3>" sortable="true" style="width:110px; ">${ltrn.cptyAcNo}</display:column>
					<display:column title="<h3>Benef Acc Name</h3>" sortable="true" style="width:170px; ">${ltrn.cptyName}</display:column>
					<display:column title="<h3>Amount</h3>" sortable="true" style="width:120px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.txnAmount}" /></display:column>
					<display:column title="<h3>Maker Id</h3>" sortable="true" style="width:70px; ">${ltrn.makerId}</display:column>
					<display:column title="<h3>Checker Id</h3>" sortable="true" style="width:70px; ">${ltrn.checkerId}</display:column>
					<display:column title="<h3>Dispatch Ref</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.dispatchRefNo}</display:column>
					<display:column title="<h3>MCB Status</h3>" sortable="true" style="width:90px; text-align:center;">${ltrn.exceptionQueue}</display:column>
        		</display:table>
                </td>
                </tr>
           </table>
        </div>
     <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
	</logic:equal>
</body>
</html>