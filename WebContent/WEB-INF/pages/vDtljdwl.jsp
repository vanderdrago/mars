<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pembayaran Dipercepat</title>
</head>
<body>
<table width="100%" border="0">
	<tr >
		<td align="center">
			<logic:equal name="viewfal" value="udffal">
			<fieldset style="width:500px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    	<legend> <b>Detil Pembayaran Dipercepat</b> </legend>
			<table width="500px" border="1" >
					<tr>
		            	<td colspan="2">Account Number : ${account_no}</td>
		            </tr>
					<tr style=" background-color: #66CCCC;" align="center">
			        	<td width="30px" style="font-size: 15px;" ><b>BRANCH</b></td>
				        <td width="50px" style="font-size: 15px;" ><b>EXEC DATE</b></td>
				        <td width="50px" style="font-size: 15px;"><b>AMOUNT PAID</b></td>
			        </tr>
			    </thead>				
			    <tbody>
			    	<c:forEach var="jdwl" items="${jdwl}" >
				        <tr>
				            <td style="text-align: center" >${jdwl.branch_code}</td>
				            <td style="text-align: center">${jdwl.exec_date}</td>
				            <td style="text-align: right"><fmt:formatNumber type="number" maxFractionDigits="2" value="${jdwl.nominal}" /></td>
				        </tr>
				    </c:forEach>
			    </tbody>
			</table>
			</fieldset>
		</logic:equal>
		</td>
	</tr>
</table>
</body>
</html>