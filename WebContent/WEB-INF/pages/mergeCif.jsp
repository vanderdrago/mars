<html>
<head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<!-- <link rel="stylesheet" type="text/css" id="theme" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/> -->
<!-- <link rel="stylesheet" type="text/css" id="theme" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css"/> -->
<title>Report Merge Cif</title>
<script>
function valdata(){
    var branch;
    branch = document.getElementById('branch').value;
    if (branch ==""){
   	 alert('Silahkan isi Branch Code Terlebih Dahulu');
     return false;
    }
    return true;
}

function validateBranch(branch) 
{
    var maintainplus = '';
    var numval = branch.value
    if ( numval.charAt(0)=='+' )
    {
        var maintainplus = '';
    }
    curphonevar = numval.replace(/[\\A-Za-z!"£$%^&\,*+_={};:'@#~,.Š\/<>?|`¬\]\[]/g,'');
    branch.value = maintainplus + curphonevar;
    var maintainplus = '';
    branch.focus;
}


function check(){
	var branch = document.getElementById('branch');
	var x = document.getElementById('chkBranch');
	if (x.checked == true){
		branch.value = 'ALL BRANCH';
	} else if (x.checked == false){
		branch.value = '';
	}
}
</script>

</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:550px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<h4 class="c-grey-900"><b>Report Merge CIF </b></h4>
					<form action="mergeCifAct.do" method="POST" name="merge" onsubmit="return validateForm()">
						<table>
							<tr>
								<td><input type="text" name="branch" id="branch" maxlength="3" class="inputs" placeholder="Branch Code" style=" width : 400px;">
									<input type="checkbox" name="chkBranch" id="chkBranch" value="all" onclick="check()">ALL BRANCH 
								</td>
							</tr>
							<tr>
					            <td align="left"><a href="#" onclick="document.forms['merge'].submit(); return valdata();" class="blue btn">Search</a></td>
<!-- 					            <td> -->
<!-- 					            	<button class="btn cur-p btn-secondary" type="button" data-dismiss="modal" name="btn" id="ok" onclick="document.forms['merge'].submit(); return valdata();">Submit</button> -->
<!-- 					            	<button type="button" class="btn btn-info">Info</button> -->
<!-- 					            	<input onclick="return valdata()" type="submit" value="Cari" name="btn" id="ok" class="blue btn"/> -->
<!-- 					            </td> -->
					       </tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color: black;">
			<td align="center">
				<table width="1075px">
					<tr>
						<td align="center">
							<div style="font-size:13px; font-family:times new roman;">
<!-- 								<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%"> -->
								<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:70px; font-size: 12px;"><b>REFERENCE NO</b></th>
										<th style="width:70px; font-size: 12px;"><b>CUST NO</b></th>
										<th style="width:100px; font-size: 12px;"><b>CUST NAME</b></th>
										<th style="width:70px; font-size: 12px;"><b>BRANCH CODE</b></th>
										<th style="width:250px; font-size: 12px;"><b>BRANCH NAME</b></th>
										<th style="width:80px; font-size: 12px;"><b>MERGE DT</b></th>
										<th style="width:80px; font-size: 12px;"><b>MERGE MAKER</b></th>
										<th style="width:100px; font-size: 12px;"><b>MERGE CHECKER</b></th>
										<th style="width:100px; font-size: 12px;"><b>CLOSE MAKER</b></th>
										<th style="width:100px; font-size: 12px;"><b>CLOSE CHECKER</b></th>
										<th style="width:100px; font-size: 12px;"><b>LAST MAKER</b></th>
										<th style="width:100px; font-size: 12px;"><b>STATUS</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="merge" items="${merge}" >
									<tr>
										<c:set var="inc" value="${inc + 1}" />
										<td style="font-size: 10px">${inc}</td>
										<td style="font-size: 10px">${merge.refNo}</td>
										<td style="font-size: 10px">${merge.custNo}</td>
										<td style="font-size: 10px">${merge.custName}</td>
										<td style="font-size: 10px">${merge.branchCode}</td>
										<td style="font-size: 10px">${merge.branchName}</td>
										<td style="font-size: 10px">${merge.mergeMakerDt}</td>
										<td style="font-size: 10px">${merge.mergeMaker}</td>
										<td style="font-size: 10px">${merge.mergeChecker}</td>
										<td style="font-size: 10px">${merge.closeMaker}</td>
										<td style="font-size: 10px">${merge.closeChecker}</td>
										<td style="font-size: 10px">${merge.lastMakerDt}</td>
										<td style="font-size: 10px">${merge.status}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
							</div>
							<logic:equal name="confrm" value="err">
							    <b>${respon}</b>
							</logic:equal>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<!-- 	<script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<!-- 	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> -->
<!-- 	<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script> -->
<!-- 	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script> -->
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> -->
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script> -->
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> -->
<!-- 	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script> -->
<!-- 	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script> -->
	<script>
 		$(document).ready(function() {
 	    $('#example').DataTable( {
 	        dom: 'Bfrtip',
 	        buttons: [
 	            'excel', 'pdf', 'print'
 	        ]
 	    } );
 		} );
	</script>
</body>
</html>