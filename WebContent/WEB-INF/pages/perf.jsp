<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">

function validateoke(){ 
 	var accno;
 	var name;
 	accno = document.getElementById('accno').value;
 	name = document.getElementById('name').value; 
	if (accno == "" && name == ""){
	 	alert('Silahkan isi No Rekening atau nama'); 
	 	return false;
	} 
 	return true;
  }



</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alternate Account</title>
</head>
<body>
<table width="100%" border="0">
	<tr>
		<td align="center">
			<fieldset style="width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    	<legend> <b>Alternate Account</b> </legend>
	        <form action="/ReportMCB/vGetAltAct.do" method="POST" name="mgetinstl">	    
	        <table border="0" width="100%">
	            <tr>
	            	<td style="width: 150px">No Rekening</td>
	            	<td style="width: 21px">:</td>
	                <td><input type="text" name="accno" id="accno" ><label style="color:red">
	                </td>
	            </tr>
	             <tr>
	            	<td style="width: 150px">Nama</td>
	            	<td style="width: 21px">:</td>
	                <td><input type="text" name="name" id="name" ><label style="color:red">
	                </td>
	            </tr>
				<tr>
		            <td colspan="2"> </td>
		            <td>
		                <input onclick="return validateoke()" type="submit" value="Lihat" name="ok" id="ok" />
		            </td>
	            </tr>
	        </table>
	        </form>
	</fieldset>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br /><br />
			
		<div style="font-size:13px; font-family:times new roman;">
           <table id="rounded-corner" style="width:900px;">
           		<tr>
               		<td>
						<display:table name="alt" id="alt"  requestURI="managePAltNomrek.do" pagesize="30" sort="external" >	               		
			            	<display:column title="<b>No. Rekening</b>" sortable="true" style="width:150px; ">${alt.cust_ac_no}</display:column>
				            <display:column title="<b>Alt. No. Rekening</b>" sortable="true" style="width:150px; ">${alt.alt_ac_no}</display:column>
				            <display:column title="<b>Nama</b>" sortable="true" style="width:300px; ">${alt.ac_desc}</display:column>          
				            <display:column title="<b>Cabang</b>" sortable="true" style="width:100px;">${alt.branch_code}</display:column>
				            <display:column title="<b>Valuta</b>" sortable="true" style="width:100px; ">${alt.ccy}</display:column>
				            <display:column title="<b>Status</b>" sortable="true" style="width:100px; ">${alt.record_stat}</display:column>				        
		        		</display:table> 	
					</td>
               </tr>
            </table>
		</div>
			<logic:equal name="konfirmasi" value="err">
				<p style="color: red;">${dataresponse}</p>
			</logic:equal>
		</td>
	</tr>
</table>

</body>
</html>