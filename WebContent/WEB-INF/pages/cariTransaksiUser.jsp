<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
$(function() {
    $( "#tgl" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Cari Transaksi User Ref No</b></legend>
					<form action="cariTransaksiUserAct.do" name="jurnal" method="POST">
						<table>
<!-- 							<tr> -->
<!-- 								<td><input type="text" name="tgl" id="tgl" class="inputs" placeholder="Tanggal" style=" width : 400px;"></td> -->
<!-- 							</tr> -->
							<tr>
								<td><input type="text" name="userRef" id="userRef" class="inputs" placeholder="User Ref No" style=" width : 400px;"></td>
							</tr>
							<tr>
								<td align="left"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color:black">
			<td align="center">
				<table border="0" width="1075px">
					<tr>
						<td>
							<table border="0">
								<tr>
									<td>
										<c:if test="${sawalNorek >= 1}">Page ${sawalNorek} Of ${noOfPagesNorek}</c:if>
									</td>
									<td align="left">
										<c:if test="${sawalNorek >= 1}"><a href="manageLphCariFwd.do?page=1" class="page">First</a></c:if>
									</td>
									<td>
										<c:if test="${currentPageNorek != 1 && (currentPageNorek - 1 > 0)}">
											<a href="manageLphCariFwd.do?page=${currentPageNorek - 1}" class="page">Previous</a>
										</c:if>
									</td>
									<td>
										<table>
   			      	  						<tr>
					      	  	  	  			<td><c:if test="${sawalNorek >= 1}">
					      	  	  	  	  	    <c:forEach begin="${sawalNorek}" end="${sawalNorek}" var="i">
					      	  	  	  	  	  	<c:choose><td><a href="manageLphCariFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
					      	  	  	  	  	    </c:forEach></c:if></td>
			   			      	  			</tr>
   			     						</table>
									</td>
									<td> 
					     		  		<c:if test="${currentPageNorek lt noOfPagesNorek}">
						          			<a href="manageLphCariFwd.do?page=${currentPageNorek + 1}" class="page">Next</a>
								  		</c:if>
					    	  		</td>
		    	  					<td>
						    	  	  	<c:if test="${noOfPagesNorek - 9 > 0}">
						       				<a href="manageLphCariFwd.do?page=${currentPageNOrek - 9}"class="page">Last</a>
						       		  	</c:if>
					    	  		</td>
		    	  				</tr>
		    	  		</table>
		    	  		</td>
		    	  		<td align="right">
							<c:if test="${sawalNorek >= 1}">Search Result - ${noOfRecordsNorek} Result</c:if>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">
						<div style="font-size:13px; font-family:times new roman;">
							<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:20px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:70px; font-size: 12px;"><b>NO REFERENCE</b></th>
										<th style="width:50px; font-size: 12px;"><b>TRN DT</b></th>
										<th style="width:50px; font-size: 12px;"><b>SERIAL NO</b></th>
										<th style="width:70px; font-size: 12px;"><b>USER REF NO</b></th>
										<th style="width:50px; font-size: 12px;"><b>BRANCH</b></th>
										<th style="width:50px; font-size: 12px;"><b>AC NO</b></th>
										<th style="width:30px; font-size: 12px;"><b>TRN KODE</b></th>
										<th style="width:300px; font-size: 12px;"><b>DESC</b></th>
										<th style="width:30px; font-size: 12px;"><b>CCY</b></th>
										<th style="width:30px; font-size: 12px;"><b>DRCR</b></th>
										<th style="width:100px; font-size: 12px;"><b>AMOUNT</b></th>
										<th style="width:50px; font-size: 12px;"><b>EXCH RATE</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="lphUserRef" items="${lphUserRef}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td>${inc}</td>
										<td>${lphUserRef.refNo}</td>
										<td>${lphUserRef.trnDate}</td>
										<td>${lphUserRef.serialNo}</td>
										<td>${lphUserRef.userRefNo}</td>
										<td>${lphUserRef.branch}</td>
										<td>${lphUserRef.acNo}</td>
										<td>${lphUserRef.txnCode}</td>
										<td>${lphUserRef.desc}</td>
										<td>${lphUserRef.ccy}</td>
										<td>${lphUserRef.drcr}</td>
										<td>
											<fmt:formatNumber type="number" maxFractionDigits="2" value="${lphUserRef.amount}" /> 
										</td>
										<td>${lphUserRef.rate}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>