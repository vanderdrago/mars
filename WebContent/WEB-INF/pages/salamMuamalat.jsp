<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
$(function() {
    $( "#lahir" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        yearRange: "1945:2016",
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>SalaMuamalat</b></legend>
					<form action="salamMuamalatAct.do" name="jurnal" method="POST">
						<table>
							<tr>
								<td><input type="text" name="no_kartu" id="no_kartu" class="inputs" placeholder="No Kartu Angsuran" style=" width : 400px;"></td>
							</tr>
							<tr>
								<td><input type="text" name="no_rek" id="no_rek" class="inputs" placeholder="No Rekening Angsuran" style=" width : 400px;"></td>
							</tr>
							<tr>
								<td><input type="text" name="nama" id="nama" class="inputs" placeholder="Nama Nasabah" style=" width : 400px;"></td>
							</tr>
							<tr>
								<td><input type="text" name="lahir" id="lahir" class="inputs" placeholder="Tanggal Lahir (dd-MM-yyyy) " style=" width : 400px;"></td>
							</tr>
							<tr>
								<td align="left"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color:black">
			<td align="center">
				<table border="0" width="1075px">
					<tr>
						<td>
							<table border="0">
								<tr>
									<td>
										<c:if test="${sawalSalam >= 1}">Page ${sawalSalam} Of ${noOfPagesSalam}</c:if>
									</td>
									<td align="left">
										<c:if test="${sawalSalam >= 1}"><a href="manageLphSalamFwd.do?page=1" class="page">First</a></c:if>
									</td>
									<td>
										<c:if test="${currentPageSalam != 1 && (currentPageSalam - 1 > 0)}">
											<a href="manageLphSalamFwd.do?page=${currentPageSalam - 1}" class="page">Previous</a>
										</c:if>
									</td>
									<td>
										<table>
   			      	  						<tr>
					      	  	  	  			<td><c:if test="${sawalSalam >= 1}">
					      	  	  	  	  	    <c:forEach begin="${sawalSalam}" end="${sakhirSalam}" var="i">
					      	  	  	  	  	  	<c:choose><td><a href="manageLphSalamFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
					      	  	  	  	  	    </c:forEach></c:if></td>
			   			      	  			</tr>
   			     						</table>
									</td>
									<td> 
					     		  		<c:if test="${currentPageSalam lt noOfPagesSalam}">
						          			<a href="manageLphSalamFwd.do?page=${currentPageSalam + 1}" class="page">Next</a>
								  		</c:if>
					    	  		</td>
		    	  					<td>
						    	  	  	<c:if test="${noOfPagesSalam - 9 > 0}">
						       				<a href="manageLphSalamFwd.do?page=${currentPageSalam - 9}"class="page">Last</a>
						       		  	</c:if>
					    	  		</td>
		    	  				</tr>
		    	  		</table>
		    	  		</td>
		    	  		<td align="right">
							<c:if test="${sawalSalam >= 1}">Search Result - ${noOfRecordsSalam} Result</c:if>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">
						<div style="font-size:13px; font-family:times new roman;">
							<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:100px; font-size: 12px;"><b>NO KARTU</b></th>
										<th style="width:100px; font-size: 12px;"><b>NO REK ANGSURAN </b></th>
										<th style="width:100px; font-size: 12px;"><b>NO CIF</b></th>
										<th style="width:200px; font-size: 12px;"><b>NAMA NASABAH</b></th>
										<th style="width:100px; font-size: 12px;"><b>TGL LAHIR</b></th>
										<th style="width:20px; font-size: 12px;"><b>PRODUK KODE</b></th>
										<th style="width:100px; font-size: 12px;"><b>PRODUK KATEGORI</b></th>
										<th style="width:100px; font-size: 12px;"><b>TGL PENCAIRAN</b></th>
										<th style="width:100px; font-size: 12px;"><b>TGL JATUH TEMPO</b></th>
										<th style="width:100px; font-size: 12px;"><b>STATUS KARTU</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="lphSalamMuamalat" items="${lphSalamMuamalat}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td>${inc}</td>
										<td><a href="/ReportMCB/salamMuamalatPeriod.do?no_kartu=${lphSalamMuamalat.no_kartu}&nama=${lphSalamMuamalat.nama_nasabah}" target="_blank">${lphSalamMuamalat.no_kartu}</a></td>
										<td>${lphSalamMuamalat.no_rek_angsuran}</td>
										<td>${lphSalamMuamalat.no_cif}</td>
										<td>${lphSalamMuamalat.nama_nasabah}</td>
										<td>${lphSalamMuamalat.lahir}</td>
										<td>${lphSalamMuamalat.produk_kode}</td>
										<td>${lphSalamMuamalat.produk_kategori}</td>
										<td>${lphSalamMuamalat.tanggal_pencairan}</td>
										<td>${lphSalamMuamalat.tanggal_jatuh_tempo}</td>
										<td>${lphSalamMuamalat.status_aktif}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>