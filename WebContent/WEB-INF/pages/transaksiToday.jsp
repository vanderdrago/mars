<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function valdata(){
    var kodecabang;
    var userId;
    kodecabang = document.getElementById('kodecabang').value;
    userId = document.getElementById('userId').value;
    if (kodecabang =="" && userId ==""){
   	 alert('Silahkan isi User Id terlebih dahulu');
     return false;
    }
    return true;
}
</script> 

<script type="text/javascript">
 $(document).ready(function(){
	 $("#isCheck").click(function () {
		 $.ajax({
			 url : "/ReportMCB/saveTransaksi.do",
			 type : "POST",
			 data : {"noRek" : noRek},
			 
			 });
		
// 		alert($('input:checkbox[name=checkme]').is(':checked'));
		});
	});
</script>




<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align = "center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Laporan Mutasi Teller Dan Back Office</b></legend>
						<form action="transaksiToday.do" method="POST" name="jurnal">
						<table border="0">
							<tr>
								<td>
									<input type="text" name="userId" id="userId" class="inputs" placeholder="User Id" style=" width : 400px;">
								</td>
							</tr>
							<tr>
	            				<td align="center">
					                <input onclick="return valdata()" type="submit" value="Cari" name="btn" id="ok" class="blue btn"/>
					                <input onclick="return valdata()" type="submit" value="Cetak" name="btndwnld" id="dwnld" class="blue btn"/>
					            </td>
					        </tr>
<!-- 					        <tr> -->
<!-- 				            	<td colspan="3"> -->
<%-- 				            		<logic:equal name="confrm" value="err"> --%>
<%-- 									<p style="color: red;"> ${respon} </p> --%>
<%-- 									</logic:equal>	 --%>
<!-- 				            	</td> -->
<!-- 				            </tr> -->
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color: black">
			<td align="center" colspan="2">
				<div style="font-size:13px; font-family:times new roman;">
					<table id="rounded-corner" style="width:1075px"> 
						<thead>
							<tr align="center">
								<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
								<th style="width:75px; font-size: 12px;"><b>USER ID</b></th>
								<th style="width:50px; font-size: 12px;"><b>BRANCH</b></th>
								<th style="width:75px; font-size: 12px;"><b>TANGGAL</b></th>
								<th style="width:75px; font-size: 12px;"><b>JAM</b></th>
								<th style="width:50px; font-size: 12px;"><b>ACC CODE</b></th>
								<th style="width:100px; font-size: 12px;"><b>NO REKENING</b></th>
								 <th style="width:100px; font-size: 12px;"><b>NAMA REKENING</b></th>
								<th style="width:75px; font-size: 12px;"><b>NOMOR WARKAT</b></th>
								<th style="width:350px; font-size: 12px;"><b>KET TRANSAKSI</b></th>
								<th style="width:75px; font-size: 12px;"><b>DEBET</b></th>
								<th style="width:75px; font-size: 12px;"><b>KREDIT</b></th>
								<th style="width:100px; font-size: 12px;"><b>USER APPROVAL</b></th>
<!-- 								<th style="width:20px; font-size: 12px;"><b>DIPERIKSA</b></th> -->
<!-- 								<th style="width:20px; font-size: 12px;"><b>OPSI</b></th> -->
<!-- 								<th style="width:20px; font-size: 12px;"><b>OPSI</b></th> -->
							</tr>
						</thead>
						<tbody>
							<c:set var="inc" value="0" />
							<c:forEach var="lphtoday" items="${lphtoday}" >
							<tr style="font-size: 11px;">
								<c:set var="inc" value="${inc + 1}" />
								<td>${inc}</td>
								<td>${lphtoday.user_id}</td>
								<td>${lphtoday.kode_cabang}</td>
								<td>${lphtoday.tanggal}</td>
								<td>${lphtoday.jam}</td>
								<td>${lphtoday.kode_transaksi}</td>
								<td>${lphtoday.ac_no}</td>
								 <td>${lphtoday.ac_desc}</td>
								<td>${lphtoday.nomor_warkat}</td>
								<td>${lphtoday.ket_kode_transaksi}</td>
								<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lphtoday.debet}" /></td>
								<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lphtoday.kredit}" /></td>
								<td>${lphtoday.user_approval}</td>
<%-- 								<td>${lphtoday.periksa}<input type="checkbox" name="tes" id="tes" value = "belum"></td> --%>
<!-- 								<td><input type="checkbox" name="checkme" id="tes"></td> -->
<!-- 								<td><a id="isCheck" href="/ReportMCB/saveTransaksi.do">Save</a></td> -->
<!-- 								<td><input type="submit" name="btnsave" id="isCheck" class="blue btn" value="Save Tes"></td> -->
								
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</td>
		</tr>
	</table>
	
</body>
</html>