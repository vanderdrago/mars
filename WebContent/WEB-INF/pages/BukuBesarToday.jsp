<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
	
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">

function ValData(){
	var userId;
		userId = document.getElementById('userId').value;
	if (userId == ""){
		 alert('Silahkan isi User ID terlebih dahulu');
	     return false;
	}
	return true;
}

</script>
	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buku Besar Today</title>
</head>
<body>
	<table border='0' width="100%">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
				<legend><b>Buku Besar Today</b></legend>
				<form action = "ViewBukuJurnalToday.do" method = "post" name = "jurnal">
				<table>
					<tr>
						<td>User Id :</td>
						<td><input type = "text" name = "userId" id = "userId"></td>
						<td colspan = 2 ><input type = "submit" onclick="return valData" name = "btn" id = "ok" value = "Cari"/></td>
					</tr>
				</table>
				</form>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table border="0" width="1075">
					<tr>
						<td>
							<table border = 0>
								<tr>
									<td>
										<c:if test = "${sawal >= 1}">
											<a href="manageLphToday.do?page=1">First</a>
										</c:if>	
									</td>
									<td>
										<c:if test="${currentPage != 1 && (currentPage - 1 > 0)}">
											<a href="manageLphToday.do?page=${currentPage - 1}">Previous</a>
										</c:if>
									</td>
									<td>
										<table border="0" >
											<tr>
												<c:if test="${sawal >= 1}">
												<c:forEach begin="${sawal}" end="${sakhir}" var="i">
										    	<c:choose>
											    	<td><a href="manageLphToday.do?page=${i}">${i}</a></td>
										    	</c:choose>
											    </c:forEach>
				    						</tr>
				    						</c:if>
										</table>
									</td>
									<td>
								     	<c:if test="${currentPage lt noOfPages}">
									        <a href="manageLphToday.do?page=${currentPage + 1}">Next</a>
										</c:if>
								    </td>
								    <td>
										<c:if test="${noOfPages - 9 > 0}">
								       	<a href="manageLphToday.do?page=${noOfPages - 9}">Last</a>
								       	</c:if>
								    </td>
								</tr>
							</table>
						</td>
						<td align = "right">
							<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
						</td>
					</tr>
					<tr>
						<td colspan = 2 border = 0>
							<table id="rounded-corner" style="width: 1075px;">
								<thead>
									<tr align = "center">
										<th style="width:80px; font-size: 10px;"><b>User ID</b></th>
							            <th style="width:150px; font-size: 10px;"><b>KODE CABANG</b></th>
							            <th style="width:200px; font-size: 10px;"><b>CABANG</b></th>
							            <th style="width:100px; font-size: 10px;"><b>TANGGAL</b></th>
							            <th style="width:80px; font-size: 10px;"><b>JAM</b></th>
							            <th style="width:150px; font-size: 10px;"><b>KODE TRANSAKSI</b></th>
							            <th style="width:250px; font-size: 10px;"><b>KETERANGAN KODE TRANSAKSI</b></th>
							            <th style="width:150px; font-size: 10px;"><b>NOMOR WARKAT</b></th>
							            <th style="width:200px; font-size: 10px;"><b>KETERANGAN TRANSAKSI</b></th>
							            <th style="width:150px; font-size: 10px;"><b>DEBET</b></th>
							            <th style="width:150px; font-size: 10px;"><b>KREDIT</b></th>
							            <th style="width:100px; font-size: 10px;"><b>USER APPROVAL</b></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var = "lph" items = "${lph}">
										<tr>
											<!-- SESUAI DENGAN ISI FIELD DARI SQL FUNCTION -->
											<td>${lph.USER_ID}</td>
											<td>${lph.KODE_CABANG}</td>
											<td>${lph.CABANG}</td>
											<td>${lph.TANGGAL}</td>
											<td>${lph.JAM}</td>
											<td>${lph.KODE_TRANSAKSI}</td>
											<td>${lph.KET_KODE_TRANSAKSI}</td>
											<td>${lph.NOMOR_WARKAT}</td>
											<td>${lph.KETERANGAN_TRANSAKSI}</td>
											<td>${lph.DEBET}</td>
											<td>${lph.KREDIT}</td>
											<td>${lph.USER_APPROVAL}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>