<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/css/demos.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,400' rel='stylesheet' type='text/css'>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="http://code.jquery.com/jquery-1.12.1.min.js"></script>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<title>History Peragaan Rekening</title>
</head>
<body>
	<table width="1075px">
		<tr>
			<td align="center">
				<form action="vHistTrans.do" method="POST" name="jurnal">
					<fieldset>
						<legend>PERAGAAN HISTORY</legend>
						<div class="box-body">
							<div class="row">
					    		<div class="col-md-1">
									<label>Acc No :</label>
								</div>
								<div class="col-xs-6">
									<input class="form-control" type="text" value ="${norek}" disabled>
								</div>
					    	</div><br>
				    		<div class="row">
				    			<div class="col-md-1">
									<label>Nama :</label>
								</div>
								<div class="col-xs-6">
									<input class="form-control" type="text" value ="${nama}" disabled>
								</div>
				    		</div><br>
				    		<div class="row">
				    			<div class="col-md-1"></div>
								<div class="col-xs-6">
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" type="text" name="tgl1" placeholder = "Mulai Tanggal">								
									</div>
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-xs-6">
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="" type="text" name="tgl2" placeholder = "Sampai Tanggal">								
									</div>
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-xs-1" align="right">
									<button type="submit" class="btn btn-success" value="send" name="btnsend" id="ok"> Search <span class="glyphicon glyphicon-search"></span></button>															        		
					        	</div>
					        	<div class="col-xs-1" align="right">
									<button type="submit" class="btn btn-info" value="Cetak" name="btndwnld" id="ok"> Download PDF <span class="fa fa-file-pdf-o"></span></button>															        		
					        	</div>
							</div>
						</div>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr style="color: black;">
			<td align="center">
				<logic:equal name="vht" value="ht">
			<br />
				<table width="1075px">
					<tr>
						<td width="100">Periode</td>
						<td width="5">:</td>
						<td>${period}</td>
					</tr>
					<tr>
						<td>Saldo Awal</td>
						<td>:</td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${saldoawal}" /> 
						</td>
					</tr>
				</table>
				<table class="table table-striped" style="width: 1075px;">
					<thead>
						<tr align="center">
							<th style="width:20px; font-size: 12px;"><b>NO.</b></th>
							<th style="width:150px; font-size: 12px;"><b>NO. REF</b></th>
							<th style="width:60px; font-size: 12px;" ><b>TRN DATE</b></th>
<!-- 							<th style="width:50px; font-size: 12px;" ><b>JAM TRN</b></th> -->
							<th style="width:60px; font-size: 12px;"><b>VALUE DATE</b></th>
							<th style="width:50px; font-size: 12px;"><b>TRN CODE</b></th>
							<th style="width:700px; font-size: 12px;"><b>DESC</b></th>
							<th style="width:15px; font-size: 12px; text-align: center;"><b>D/C</b></th>
							<th style="width:150px; font-size: 12px; text-align: right;"><b>NOMINAL</b></th>
							<th style="width:150px; font-size: 12px; text-align: right;"><b>SALDO</b></th>
							<th style="width:100px; font-size: 12px; text-align: right;"><b>EKIVALEN</b></th>
							<th style="width:50px; font-size: 12px; text-align: center;"><b>STATUS</b></th>
<!-- 							<th style="width:50px; font-size: 12px; text-align: center;"><b>TXN_DT_TIME</b></th> -->
						</tr>
					</thead>
					<tbody>
						<c:set var="inc" value="0" />
						<c:forEach var="lhist" items="${lhist}" >
						<tr style="font-size: 11px;">
							<c:set var="inc" value="${inc + 1}" />
							<td >${inc}</td>
							<td >${lhist.trn_ref_no}</td>
							<td >${lhist.trn_dt}</td>
<%-- 							<td >${lhist.jam}</td> --%>
							<td >${lhist.val_dt}</td>
							<td >${lhist.trn_code}</td>
							<td >${lhist.desc}</td>
							<td style="text-align: center;">${lhist.drcr}</td>
							<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lhist.nominal}" /></td>
							<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lhist.saldo}" /></td>
							<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lhist.ekivalen}" /></td>
							<td style="text-align: center;">${lhist.auth_stat}</td>
<%-- 							<td style="text-align: center;">${lhist.txn_dt_time}</td> --%>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</logic:equal>
			</td>
		</tr>
	</table>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $(document).ready(function(){	
    	$('#example2').DataTable( {
    	        "scrollX": true
    	    } );
    	} );
	
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd-mm-yyyy", {"placeholder": "Tanggal Lahir"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm-dd-yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

  });
 </script>
</body>
</html>