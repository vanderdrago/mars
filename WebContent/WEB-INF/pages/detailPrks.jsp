<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div style="float: center; text-align: center;">
	<table border="0" width="100%">
		<tr style="color:black">
			<th style="font-size: 20px;align="center"; colspan=7 ;"><b><h3><center>DETAIL PRKS</center></h3></b></th>
		</tr>
		<tr>
			<td align="center">
				<table id="rounded-corner"  border="0.5" align="center" style=" width : 1075px;">
					<tr>
						<td>Customer Name :</td>
						<td>:</td>
						<td>${head.custName}</td>
						
						<td>Rate</td>
						<td>:</td>
						<td>${head.rate}%</td>
						
						<td>O/S</td>
						<td>:</td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${head.os}" /></td>
					</tr>
					<tr>
						<td>No Rek PRKS</td>
						<td>:</td>
						<td>${head.acc}</td>
						
						<td>Start Date</td>
						<td>:</td>
						<td>${head.startDt}</td>
						
						<td>FCU</td>
						<td>:</td>
						<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${head.fcu}"></fmt:formatNumber></td>
					</tr>
					<tr>
						<td>No Rek Bagi Hasil</td>
						<td>:</td>
						<td>${head.accBaghas}</td>
						
						<td>Expiry Date</td>
						<td>:</td>
						<td>${head.expiryDt}</td>
						
						<td>Account Class</td>
						<td>:</td>
						<td>${head.accClass}</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="color:black">
			<td align="center">
				<table border="0" width="1075px">
					<tr>
						<td align="center" colspan="2">
						<div style="font-size:13px; font-family:times new roman;">
							<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:100px; font-size: 12px;"><b>PERIODE</b></th>
										<th style="width:100px; font-size: 12px;"><b>BULAN TAGIHAN</b></th>
										<th style="width:100px; font-size: 12px;"><b>BULAN BAYAR</b></th>
										<th style="width:100px; font-size: 12px;"><b>SALDO RATA-RATA</b></th>
										<th style="width:100px; font-size: 12px;"><b>PROYEKSI BAGI HASIL</b></th>
										<th style="width:200px; font-size: 12px;"><b>REALISASI BAGI HASIL</b></th>
										<th style="width:100px; font-size: 12px;"><b>STATUS</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="detail" items="${detail}" >
									<tr>
										<c:set var="inc" value="${inc + 1}" />
										<td style="font-size: 10px">${inc}</td>
										<td style="font-size: 10px">${detail.periode}</td>
										<td style="font-size: 10px">${detail.bulanTagihan}</td>
										<td style="font-size: 10px">${detail.bulanBayar}</td>
										<td style="font-size: 10px"><fmt:formatNumber type="number" maxFractionDigits="2" value="${detail.saldoRata}" /></td>
										<td style="font-size: 10px"><fmt:formatNumber type="number" maxFractionDigits="2" value="${detail.pbh}" /></td>
										<td style="font-size: 10px"><fmt:formatNumber type="number" maxFractionDigits="2" value="${detail.rbh}" /></td>
										<td style="font-size: 10px">${detail.status}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<logic:equal name="konfirmasi" value="err">
							<p style="color: red;">${dataresponse}</p>
						</logic:equal>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
</body>
</html>