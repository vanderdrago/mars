<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function valdata(){
    var acount;
    var nama;
    var ktp;
    var lhr;
    acount = document.getElementById('acount').value;
    nama = document.getElementById('nama').value;
    ktp = document.getElementById('ktp').value;
    lhr = document.getElementById('lhr').value;
    if (acount == "" && nama == "" &&  ktp == "" && lhr == "" ){
        alert('Silahkan isi No. Rek atau Nama atau No KTP atau Tgl Lahir');
        return false;
    }
    return true;
}

$(function() {
    $( "#lhr" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
  		yearRange: "1925:2013",
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd',
        changeYear: true
    });


});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}

</script>
<table border="0" width="100%">
  <tr>
    <td align="center" colspan="2">
    	<fieldset style="color:black; width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
		    <legend> <b>PERAGAAN HISTORY</b> </legend>
		    <form action="viewRagaHistSalam.do" method="POST" name="jurnal">
		    <table>
		        <tr>
<!-- 		            <td><input type="text" name="acount" id="acount" maxlength="10" onKeyup="checkDec(this);" ></td> -->
		            <td><input type="text" name="acount" id="acount" maxlength="10" onKeyup="checkDec(this);" class="inputs" placeholder="No Rekening" style=" width : 400px;"></td>
		        </tr>
		        <tr>
<!-- 		            <td><input type="text" name="nama" id="nama" ></td> -->
		            <td><input type="text" name="nama" id="nama" class="inputs" placeholder="Nama Nasabah" style=" width : 400px;"></td>
		        </tr>
		         <tr>
<!-- 		            <td><input type="text" name="ktp" id="ktp" ></td> -->
		            <td><input type="text" name="ktp" id="ktp" class="inputs" placeholder="KTP" style=" width : 400px;"></td>
		         </tr>
		         <tr>
<!-- 		            <td><input type="text" name="lhr" id="lhr" ><label></label>&nbsp;dd-mm-yyyy</td> -->
		            <td><input type="text" name="lhr" id="lhr" class="inputs" placeholder="Tanggal (dd-mm-YYYY)" style=" width : 400px;"></td>
		        </tr>
		        
		        <tr>
<!-- 		            <td colspan="2"> </td> -->
		            <td align="left"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a>
<!-- 		                <input onclick="return valdata()" type="submit" value="Cari" name="btn" id="ok" /> -->
		            </td>
		         </tr>
		    </table>
		    </form>
		</fieldset>
		    	
    </td>
  </tr>
  <tr style="color: black;">  		
	<td align="center">
		<table border="0" width="1075">
			<tr>
				<td>
					<table border="0">
						<tr>
							<td>
								<c:if test="${sawal >= 1}">Page ${sawal} Of ${noOfPages}</c:if>
							</td>
							<td>
								<c:if test="${sawal >= 1}">
			       					<a href="manageLphFwd.do?page=1" class="page">First</a>
			       				</c:if>
			       			</td>
			       			<td>
			       				<c:if test="${currentPage != 1 && (currentPage - 1 > 0)}">
									<a href="manageLphFwd.do?page=${currentPage - 1}" class="page">Previous</a>
								</c:if>
			       			</td>
		       				<td>
							<table border="0" >
								<tr>
									<c:if test="${sawal >= 1}">
										<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							    		<c:choose>
								    		<td><a href="manageLphFwd.do?page=${i}" class="page">${i}</a></td>
							    	 	</c:choose>
								    	</c:forEach>
								    </c:if>
	    						</tr>
							</table>
					    	</td>
						    <td>
						     	<c:if test="${currentPage lt noOfPages}">
							        <a href="manageLphFwd.do?page=${currentPage + 1}" class="page">Next</a>
								</c:if>
						    </td>
						    <td>
								<c:if test="${noOfPages - 9 > 0}">
						       		<a href="manageLphFwd.do?page=${noOfPages - 9}" class="page">Last</a>
						       	</c:if>
						    </td>
						</tr>
					</table>
				</td>
				<td align="right">
					<c:if test="${sawal >= 1}">Search Result - ${noOfRecords} Result</c:if>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table id="rounded-corner" style="width: 1075px;">
					<thead>
						<tr align="center">
							<th style="width:150px; font-size: 12px;" ><b>ACCOUNT NO</b></th>
							<th style="width:500px; font-size: 12px;"><b>NAME</b></th>
							<th style="width:100px; font-size: 12px;"><b>CUST NO</b></th>
							<th style="width:50px; font-size: 12px;"><b>CCY</b></th>
							<th style="width:150px; font-size: 12px;"><b>ACCOUNT CLASS</b></th>
							<th style="width:300px; font-size: 12px;"><b>NO.KTP</b></th>
							<th style="width:300px; font-size: 12px;"><b>TANGGAL LAHIR</b></th>
							<th style="width:300px; font-size: 12px;"><b>PERAGAAN</b></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="lph" items="${lph}" >
						<tr>
							<td >${lph.cust_ac_no}</td>
							<td >${lph.ac_desc}</td>
							<td >${lph.cust_no}</td>
							<td >${lph.ccy}</td>
							<td >${lph.account_class}</td>
							<td >${lph.UNIQUE_ID_VALUE}</td>
							<td >${lph.DATE_OF_BIRTH}</td>
							<td >
							<a href="/ReportMCB/vPeragaanCustDSalam.do?norek=${lph.cust_ac_no}" target="_blank">Rekening</a> &nbsp;
							<a href="/ReportMCB/vPeragaanHistD.do?ac=${lph.cust_ac_no}&nm=${lph.ac_desc}" target="_blank">History </a>
							</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				</td>
			</tr>
		</table>
	</td>
  </tr>
</table>
