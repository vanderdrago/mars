<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function valtgl(){
    var tgl1;
    var tgl2;
    tgl1 = document.getElementById('tgl1').value;
    tgl2 = document.getElementById('tgl2').value;

    if (tgl1 == "" && tgl2 == ""){
        alert('Silahkan isi tanggal');
        return false;
    }
    return true;
}

$(function() {
    $( "#tgl1" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
    $( "#tgl2" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>
<script type="text/javascript">
$(function() {
    $( "#tgl1" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy'
    });
    $( "#tgl2" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy'
    });
});
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Salam Muamalat Financing</title>
</head>
<body>
	<table width="100%" border="0">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
				<legend><b>PERIOD PRKS</b></legend>
				<form action="detailPrks.do" method="POST" name="jurnal">
					<table border = "0">
						<tr>
							<td>
								<table>
									<tr>
										<td>Acc No PRKS</td>
										<td>:</td>
										<td><b>${acc}</b></td>
									</tr>
									<tr>
										<td>Customer Name</td>
										<td>:</td>
										<td><b>${nama}</b></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><input type="text" name="tgl1" id="tgl1" class="inputs" placeholder="Silahkan Pilih Periode (DD-MM-YYYY)" style=" width : 400px;"></td>
						</tr>
						<tr>
							<td><input type="text" name="tgl2" id="tgl2" class="inputs" placeholder="Silahkan Pilih Periode (DD-MM-YYYY)" style=" width : 400px;"></td>
						</tr>
						<tr>
							<td align="center"><a href="#" onclick="document.forms['jurnal'].submit(); return false;" class="blue btn">Search</a></td>
						</tr>
					</table>
				</form>
				</fieldset>
			</td>
		</tr>
	</table>
</body>
</html>