<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">
function check(f){
	if (f.checked){
	    document.getElementById('cab').value = 'Semua';
	}else {
		document.getElementById('cab').value = '';
	}
}

function checkval(f){
	if (f.checked){
	    document.getElementById('val').value = 'Semua';
	}else {
		document.getElementById('val').value = '';
	}
}
</script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Saldo Percobaan</title>
</head>
<body>

<table border="0" align="center">
	<tr>
		<td>
			<fieldset style="width:500px;  border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			    <legend> <b>Monitoring Saldo Percobaan</b> </legend>
			    <form action="searchLsp.do" method="POST" name="lsp">
			    <table>
			        <tr>
			            <td colspan="4">Saldo Percobaan</td>
			        </tr>
			        <tr>
			            <td>Mulai Rekening SSL</td>
			            <td>:</td>
			            <td colspan="2"><input type="text" name="ssl_a" id="ssl_a" maxlength="9" ><label style="color:red">*</label></td>
			        </tr>
			        <tr>
			            <td>Hingga Rekening SSL</td>
			            <td>:</td>
			            <td colspan="2"><input type="text" name="ssl_b" id="ssl_b" maxlength="9" ><label style="color:red">*</label></td>
			        </tr>
			        <tr>
			            <td>Kode Cabang</td>
			            <td>:</td>
			            <td><input type="text" name="cab" id="cab" maxlength="3" ><label style="color:red">*</label></td>
			            <td><input type="checkbox" name="allcab" id="allcab" value="1" onclick="check(this.form.allcab)"> Semua Kode Cabang</td>
			        </tr>
			        <tr>
			            <td>Kode Valuta</td>
			            <td>:</td>
			            <td ><input type="text" name="val" id="val" maxlength="3" ><label style="color:red">*</label></td>
			            <td><input type="checkbox" name="allval" id="allval" value="2" onclick="checkval(this.form.allval)"> Semua Kode Valuta</td>
			        </tr>
			        <tr>
			            <td colspan="4"><input type="checkbox" name="cbxspnoll" id="idcbxspnoll" value="1"> Termasuk saldo percobaan 0.0 <br/></td>
			        </tr>
			        <tr>
			            <td colspan="2"> </td>
			            <td colspan="2">
			                <input onclick="return validateLsp()" type="submit" value="Ok" name="ok" id="ok" />
			                <input onclick="return validateLsp()" type="submit" value="Download" name="btnsp" id="download" />
			            </td>
				   </tr>
			    </table>
			    </form>
			</fieldset>
		</td>
	</tr>
</table>

<logic:equal name="respon" value="data">
<b>${teks }</b>
</logic:equal>

<logic:equal name="viewrpt" value="R004">
    <table border="0" align="center" width="800px;">
            <tr>
                <td align="center" colspan="3"><b>MONITORING SALDO PERCOBAAN </b></td>
            </tr>
            <tr>
                <td width="150px;">No. SSL</td>
                <td width="2px;">:</td>
                <td>${ssl_ab}</td>
            </tr>
            <tr>
                <td>Kantor/Cabang</td>
                <td>:</td>
                <td>${cab}</td>
            </tr>
            <tr>
                <td>Valuta</td>
                <td>:</td>
                <td>${val}</td>
            </tr>
            
        </table>
            <br />
            
        <div style="font-size:13px; font-family:times new roman;">
        <table style="width:800px;" border="0">
        	<tr>
        		<td>        			
		           <table id="rounded-corner">
		               <tr ><td>
					        <display:table name="llsp" id="llsp" class="wb" requestURI="manageLspFwd.do" pagesize="30" sort="external" >
					            <display:column title="<h3>No SSL</h3>" sortable="true" style="width:100px;">${llsp.gl_code}</display:column>
					            <display:column title="<h3>Nama SSL</h3>" sortable="true" style="width:250px;">${llsp.gl_desc}</display:column>
					            <display:column title="<h3>Kode Cabang</h3>" sortable="true" style="width:120px;">${llsp.branch_code}</display:column>
					            <display:column title="<h3>Kode Valuta</h3>" sortable="true" style="width:120px;"> ${llsp.ccy}</display:column>
					            <display:column title="<h3>Saldo Percobaan</h3>" sortable="true" style="width:170px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${llsp.mov_lcy}" /></display:column>                        
					        </display:table>
		                       </td>
		                   </tr>
		          </table>
        		</td>
        	</tr>
        </table>
        </div>
</logic:equal>
            
</body>
</html>