<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/css/demos.css" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<!--  jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

<!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!--Font Awesome (added because you use icons in your prepend/append)-->
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />

<script>

function validatebranch(branch) 
{
    var maintainplus = '';
    var numval = branch.value
    if ( numval.charAt(0)=='+' )
    {
        var maintainplus = '';
    }
    curphonevar = numval.replace(/[\\A-Za-z!"£$%^&\,*+_={};:'@#~,.Š\/<>?|`¬\]\[]/g,'');
    branch.value = maintainplus + curphonevar;
    var maintainplus = '';
    branch.focus;
}


function validateForm() {
    var x = document.forms["quickCif"]["branch"].value;
    if (x == "") {
        alert("Branch Code Tidak Boleh Kosong");
        return false;
    }
}

function check(){
	var branch = document.getElementById('branch');
	var x = document.getElementById('chkBranch');
	if (x.checked == true){
		branch.value = 'ALL BRANCH';
	} else if (x.checked == false){
		branch.value = '';
	}
}

</script>
<title>Laporan Quick Cif</title>
</head>
<body>
	<table width="1075px">
		<tr>
			<td align="center">
				<form action="quickCifAct.do" method="POST" name="quickCif" onsubmit="return validateForm()">
					<fieldset>
						<legend>Laporan Quick Cif</legend>
						<div class="box-body">
							<div class="row">
					    		<div class="col-md-6">
									<input class="form-control" placeholder="Branch Code" type="text" name="branch" id = "branch" maxlength="3" onkeyup="validatebranch(this);">
								</div>
								<div class="col-xs-2">
									<input type="checkbox" name="chkBranch" id="chkBranch" value="all" onclick="check()">ALL BRANCH 
								</div>
					    	</div>
<!-- 					    	<br> -->
<!-- 					    	<div class="row"> -->
<!-- 					    		<div class="col-md-3"> -->
<!-- 									<div class="form-group"> -->
<!-- 										<div class='input-group date' id='datetimepicker1'> -->
<!-- 											<input class="form-control" id="StartDate" name="StartDate" placeholder="Start Date" type="text"/> -->
<!-- 											<span class="input-group-addon"> -->
<!-- 												<span class="glyphicon glyphicon-calendar"></span> -->
<!--                 							</span> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="col-md-3"> -->
<!-- 									<div class="form-group"> -->
<!-- 										<div class='input-group date' id='datetimepicker2'> -->
<!-- 											<input class="form-control" id="EndDate" name="EndDate" placeholder="End Date" type="text"/> -->
<!-- 											<span class="input-group-addon"> -->
<!-- 												<span class="glyphicon glyphicon-calendar"></span> -->
<!--                 							</span> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 					    	</div> -->
<!-- 				    		<br> -->
					    	<div class="row">
								<div class="col-xs-1" align="left">
									<button type="submit" class="btn btn-success" value="send" name="btnsend" id="ok"> Search <span class="glyphicon glyphicon-search"></span></button>															        		
					        	</div>
					        	<div class="col-xs-1" align="left">
									<button type="submit" class="btn btn-info" value="Cetak" name="btndwnld" id="ok"> Download PDF <span class="fa fa-file-pdf-o"></span></button>															        		
					        	</div>
							</div>
						</div>
					</fieldset>
				</form>
			</td>
		</tr>
		
	</table>
	<!-- Include jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript">

	var myDropdown=document.getElementsByName('status')[0];

	function myFunction(){
		 var textboxValue = document.getElementById("textbox").value;
	}

</script>

<script>
	$(document).ready(function(){
		var date_input=$('input[name="StartDate"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'dd-mm-yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>
<script>
	$(document).ready(function(){
		var date_input=$('input[name="EndDate"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'dd-mm-yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>

</body>
</html>