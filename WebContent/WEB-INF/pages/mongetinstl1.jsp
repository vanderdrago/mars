<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">

function validatemintsl(){   
    var accno;
    var cusid;
    var cusname;
        
    accno = document.getElementById('accno').value;
    cusid = document.getElementById('cusid').value;
	cusname = document.getElementById('cusname').value;  
    if ((accno == "") && (cusid == "") && (cusname == "")) {
        alert('Salah satu Fields wajib diisi');
        return false;
    }
    return true;
}


</script>
<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Installment</title>
</head>
<body>
<!-- <a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a> -->
<br />
<table width="500px">
	<tr>
		<td align="center">
		<fieldset style="width:350px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
    	<legend> <b>Fields Installment Query</b> </legend>
        <form action="/ReportMCB/vGetInstl1.do" method="POST" name="mgetinstl">	    
        <table border="0" width="300px">
            <tr>
            	<td style="width: 100px">Installment No</td>
            	<td style="width: 10px">:</td>
                <td><input type="text" name="accno" id="accno" ><label style="color:red">
                </td>
            </tr>
            <tr>
            	<td style="width: 100px">Customer ID</td>
            	<td style="width: 10px">:</td>
                <td><input type="text" name="cusid" id="cusid" ><label style="color:red"></label>
                </td>
            </tr>
            <tr>
				<td style="width: 100px">Customer Name</td>
				<td style="width: 10px">:</td>
				<td><input type="text" name="cusname" id="cusname"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
	            <td colspan="2"> </td>
	            <td>
	                <input onclick="return validatemintsl()" type="submit" value="Ok" name="ok" id="ok" />
	            </td>
            </tr>
        </table>
        </form>
</fieldset>
		</td>
	</tr>
</table>
		<br />
       <div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:1075px">
               <tr ><td>
		            <display:table name="ltrn" id="ltrn" class="wb" requestURI="MonGetInstlForward1.do" pagesize="30" sort="external" >
		            <display:column title="<h3>Customer ID</h3>" sortable="true" style="width:75px;">${ltrn.customer_id}</display:column>
		            <display:column title="<h3>Prd Code</h3>" sortable="true" style="width:60px;">${ltrn.prod_code}</display:column>
		            <display:column title="<h3>Prod Category</h3>" sortable="true" style="width:80px;">${ltrn.prod_ctgry}</display:column>
		            <display:column title="<h3>Book Date</h3>" sortable="true" style="width:60px;">${ltrn.book_date}</display:column>
		            <display:column title="<h3>Maturity Date</h3>" sortable="true" style="width:80px;">${ltrn.matur_date}</display:column>
		            <display:column title="<h3>Instalment No</h3>" sortable="true" style="width:60px;">
		            <a href="/ReportMCB/vRptInstl1.do?instNo=${ltrn.account_number}" target="_blank">${ltrn.account_number}</a>
		            </display:column>
		            <display:column title="<h3>Acc No</h3>" sortable="true" style="width:70px;">${ltrn.prim_app_id}</display:column>
		            <display:column title="<h3>Acc Name</h3>" sortable="true" style="width:280px;">${ltrn.prim_app_name}</display:column>
		            <display:column title="<h3>Period</h3>" sortable="true" style="width:50px;">${ltrn.no_of_installments}</display:column>
		            <display:column title="<h3>Acc Status</h3>" sortable="true" style="width:50px;">${ltrn.account_status}</display:column>
		            <display:column title="<h3>Amount Principal</h3>" sortable="true" style="width:170px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.amount_disbursed}" /></display:column>
			        <display:column title="<h3>Amount Margin</h3>" sortable="true" style="width:170px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.upfront_profit_booked}" /></display:column>
			        <display:column title="<h3>Amount Total</h3>" sortable="true" style="width:170px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.total_sale_value}" /></display:column>
			        </display:table>
                    </td>
               </tr>
            </table>
        </div>
     <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
	 </logic:equal>
</body>
</html>