<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function valtgl(){
    var StartDate;
    var EndDate;
    StartDate = document.getElementById('StartDate').value;
    EndDate = document.getElementById('EndDate').value;

    if (StartDate == "" && EndDate == ""){
        alert('Silahkan Pilih StartDate dan EndDate');
        return false;
    }
    return true;
}

$(function() {
    $( "#StartDate" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
    $( "#EndDate" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}

function check(){
	var branch = document.getElementById('branch');
	var x = document.getElementById('chkBranch');
	if (x.checked == true){
		branch.value = 'ALL BRANCH';
	} else if (x.checked == false){
		branch.value = '';
	}
}
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Laporan QuickCif Periode</title>
</head>
<body>
	<table border="0" width="100%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:500px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Laporan Quick Cif Periode</b></legend>
					<form action="quickCifPeriodeAct.do" method="POST" name="quickCifPeriode" onsubmit="return validateForm()">
						<table>
							<tr>
								<td>
									<input type="text" name="branch" id="branch" onKeyup="checkDec(this);" class="inputs" maxlength="3" placeholder="Branch Code" style=" width : 300px;">
									<input type="checkbox" name="chkBranch" id="chkBranch" value="all" onclick="check()">ALL BRANCH 
								</td>
							<tr>
								<td>
									<input type="text" name="StartDate" id="StartDate" class="inputs" placeholder="Start Date (DD-MM-YYYY)" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="EndDate" id="EndDate" class="inputs" placeholder="End Date (DD-MM-YYYY)" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td>
									<input onclick="return valtgl()" type="submit" value="Cari" name="btnsend" id="ok" class="blue btn" />
		                			<input onclick="return valtgl()" type="submit" value="Cetak" name="btndwnld" id="dwnld" class="blue btn" />		
		                		</td>				
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color: black;"> 
			<td align="center">
				<logic:equal name="vht" value="ht">
				<br />
					<table width="1075px">
						<tr>
							<td width="100">Branch</td>
							<td width="5">:</td>
							<td>${branch}</td>
						</tr>
						<tr>
							<td width="100">Periode</td>
							<td width="5">:</td>
							<td>${periode}</td>
						</tr>
					</table>
					<table id="rounded-corner" style="width: 1075px;">
						<thead>
							<tr align="center">
								<th style="width:15px; font-size: 10px; font-style: family font sains"><b>#</b></th>
								<th style="width:60px; font-size: 10px;"><b>CUST NO</b></th>
								<th style="width:100px; font-size: 10px;"><b>CUST NAME</b></th>
								<th style="width:50px; font-size: 10px;"><b>BRANCH CODE</b></th>
								<th style="width:130px; font-size: 10px;"><b>BRANCH NAME</b></th>
								<th style="width:80px; font-size: 10px;"><b>MAKER ID</b></th>
								<th style="width:70px; font-size: 10px;"><b>CIF CREATION DT</b>
								<th style="width:80px; font-size: 10px;"><b>MOD2 MAKER</b></th>
								<th style="width:80px; font-size: 10px;"><b>MOD2 MAKER DT</b></th>
								<th style="width:80px; font-size: 10px;"><b>MOD2 OTO</b></th>
								<th style="width:90px; font-size: 10px;"><b>MOD2 OTO DT</b></th>
								<th style="width:70px; font-size: 10px;"><b>STATUS</b></th>
							</tr>
						</thead>
						<tbody>
							<c:set var="inc" value="0" />
							<c:forEach var="listQuickCif" items="${listQuickCif}" >
							<tr>
								<c:set var="inc" value="${inc + 1}" />
								<td style="font-size: 10px">${inc}</td>
								<td style="font-size: 10px">${listQuickCif.customerNo}</td>
								<td style="font-size: 10px">${listQuickCif.customerName}</td>
								<td style="font-size: 10px">${listQuickCif.branchCode}</td>
								<td style="font-size: 10px">${listQuickCif.branchName}</td>
								<td style="font-size: 10px">${listQuickCif.firstMakerId}</td>
								<td style="font-size: 10px">${listQuickCif.cifCreationDate}</td>
								<td style="font-size: 10px">${listQuickCif.lastMakerId}</td>
								<td style="font-size: 10px">${listQuickCif.makerDate}</td>
								<td style="font-size: 10px">${listQuickCif.lastOtoId}</td>
								<td style="font-size: 10px">${listQuickCif.lastUpdate}</td>
								<td style="font-size: 10px">${listQuickCif.status}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</logic:equal>
			</td>
		</tr>
	</table>
</body>