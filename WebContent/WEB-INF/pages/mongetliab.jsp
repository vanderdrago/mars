<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">
function validatemrtgsint(){
    var amont_a;
    var amont_b;
    var liabid;
    var colcod;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
	
	if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Collateral Value Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Collateral Value Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Collateral Value harus lebih kecil dari To Collateral Value');
        return false;
    }
    return true;
}
</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Liability</title>
</head>
<body>
<!-- <h2 align="center">Liability Details</h2> -->
<!-- <a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>  -->
<br />
<table width="500px">
	<tr>
		<td align="center">
		<fieldset style="width:350px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
    	<legend> <b>Fields Facility Details</b> </legend>
        <form action="/ReportMCB/vGetliab.do" method="POST" name="mgetliab">
	    <table border="0" width="300px">
            <tr>
            	<td style="width: 100px">Liability No</td>
            	<td style="width: 10px">:</td>
                <td><input type="text" name="liabno" id="liabno" ><label style="color:red">
                </td>
            </tr>
            <tr>
            	<td style="width: 100px">Liability Name</td>
            	<td style="width: 10px">:</td>
                <td><input type="text" name="liabname" id="liabname" width= "250px" ><label style="color:red"></label>
                </td>
            </tr>
            <tr>
            <td colspan="2"> </td>
            <td>
                <input type="submit" value="Ok" name="ok" id="ok" />
                <!-- onclick="return validatemrtgsint()" -->
            </td>
            </tr>
        </table>
        </form>
        </fieldset>
        	</td>
	</tr>
</table>
	<br />
       <div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:1075px">
               <tr ><td>
		            <display:table name="ltrn" id="ltrn" class="wb" requestURI="MgetLiabpgg.do" pagesize="20" sort="external" >
		            <display:column title="<h3>Liability No</h3>" sortable="true" style="width:70px;">${ltrn.liab_no}</display:column>
		            <display:column title="<h3>Liability Name</h3>" sortable="true" style="width:330px;">${ltrn.liab_name}</display:column>
		            <display:column title="<h3>User Define Status</h3>" sortable="true" style="width:120px; text-align:center;">${ltrn.user_define_status}</display:column>
		            <display:column title="<h3>Branch</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.liab_branch}</display:column>
		            <display:column title="<h3>Currency</h3>" sortable="true" style="width:60px; text-align:center;">${ltrn.liab_ccy}</display:column>
		            <display:column title="<h3>Utilization Amount</h3>" sortable="true" style="width:130px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.util_amt}" /></display:column>
		            <display:column title="<h3>Overall Limit</h3>" sortable="true" style="width:130px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.overall_limit}" /></display:column>
		            <display:column title="<h3>Category</h3>" sortable="true" style="width:140px;">${ltrn.category}</display:column>
		            <display:column title="<h3>Maker ID</h3>" sortable="true" style="width:80px; ">${ltrn.maker_id}</display:column>
		            <display:column title="<h3>Checker ID</h3>" sortable="true" style="width:80px; ">${ltrn.checker_id}</display:column>
		            <display:column title="<h3>Status Rec</h3>" sortable="true" style="width:80px; text-align:center;">${ltrn.status_rcd}</display:column>
		            <display:column title="<h3>Status Auth</h3>" sortable="true" style="width:80px; text-align:center;">${ltrn.status_oto}</display:column>
		            </display:table>
                    </td>
               </tr>
            </table>
        </div>
     <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
	 </logic:equal>
	 
</body>
</html>