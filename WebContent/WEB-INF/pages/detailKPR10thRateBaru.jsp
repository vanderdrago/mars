<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Iterator" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Simulasi Angsuran KPR 10 Tahun</title>
</head>
<body>
	<table width="100%">
		<tr>
			<td align="center">
				<table width="1000px" border="0">
					<tr>
						<td align="center" colspan="7"><b>JADWAL ANGSURAN KPR Muamalat iB 10 Tahun - Angsuran Ringan</b></td>
					</tr>
					<tr>
						<td width="150px"> Nama </td>
						<td width="10px">:</td>
						<td width="300px">${nama}</td>
						<td width="150px">Harga Jual</td>
						<td width="10px">:</td>
						<td width="300px"><fmt:formatNumber type="number" value="${TotalAngsuran}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td width="150px"> Plafond Pembiayaan </td>
						<td width="10px">:</td>
						<td colspan="1" width="300px"><fmt:formatNumber type="number" value="${plafond}" maxFractionDigits="2"/></td>
						<td>Total Margin</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${TotalMargin}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td width="150px"> Tanggal Simulasi </td>
						<td width="10px">:</td>
						<td colspan="1" width="300px">${tanggal}</td>
						<td width="150px">Angsuran Periode 1</td>
						<td width="10px">:</td>
						<td colspan="1" width="300px"><fmt:formatNumber type="number" value="${AngsuranPertama}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td>Jangka Waktu</td>
						<td width="10px">:</td>
						<td width="400px">${jangkaWaktu} Bulan</td>
						<td width="200px">Angsuran Periode 2</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${AngsuranKedua}" maxFractionDigits="0"/></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td width="200px">Angsuran Periode 3</td>
						<td width="10px">:</td>
						<td colspan="2" width="300px"><fmt:formatNumber type="number" value="${AngsuranKetiga}" maxFractionDigits="0"/></td>
					</tr>
				</table>
				<table id="rounded-corner" style="width: 1000px">
					<thead>
				        <tr align="center">
				            <th style="width:30px; font-size: 12px;"><b>Bulan</b></th>
				            <th style="width:60px; font-size: 12px;"><b>O/S Pokok</b></th>
				            <th style="width:60px; font-size: 12px;"><b>AngPokok</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Margin</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Angsuran</b></th>
				            <th style="width:60px; font-size: 12px;"><b>Rate</b></th>
				        </tr>
				     </thead>
				     <tbody>
				     	<c:forEach var="kpr" items="${kpr}" varStatus="row">
							<tr style="font-size: 11px;">
								<td>${row.index + 1}</td>
								<td>
									<c:if test="${row.index < 12}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.OsPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 11 && row.index < 24}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.OsKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 23 && row.index < 73}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.OsKetiga}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71 && row.index < 96}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.OsKeempat}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 95}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.OsKelima}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 12}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.PokokPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 11 && row.index < 24}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.PokokKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 23 && row.index < 73}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.PokokKetiga}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71 && row.index < 96}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.PokokKeempat}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 95}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.PokokKelima}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 12}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.MarginPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 11 && row.index < 24}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.MarginKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 23 && row.index < 73}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.MarginKetiga}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71 && row.index < 96}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.MarginKeempat}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 95}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.MarginKelima}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 12}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranPertama}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 11 && row.index < 24}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranKedua}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 23 && row.index < 73}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranKetiga}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 71 && row.index < 96}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranKeempat}" /> 
				     				</c:if>
				     				<c:if test="${row.index > 95}"> 
				     					<fmt:formatNumber type="number" maxFractionDigits="0" value="${kpr.AngsuranKelima}" /> 
				     				</c:if>
								</td>
								<td>
									<c:if test="${row.index < 12}"> 
				     					11.50%
				     				</c:if>
				     				<c:if test="${row.index > 11 && row.index < 24}"> 
				     					10.50%
				     				</c:if>
				     				<c:if test="${row.index > 23 && row.index < 96}"> 
				     					10.00%
				     				</c:if>
				     				<c:if test="${row.index > 95}"> 
				     					10.84%
				     				</c:if>
								</td>
							</tr>
						</c:forEach>
				     </tbody>
				     <tfoot>
				     	<tr style="font-weight: bold">
				     		<td colspan="2" align="center"><b>TOTAL</b></td>
				     		<td style="text-align: left; " ><fmt:formatNumber type="number" maxFractionDigits="0" value="${TotalPokok}" /></td>
					    	<td style="text-align: left; " ><fmt:formatNumber type="number" maxFractionDigits="0" value="${TotalMargin}" /></td>
					    	<td style="text-align: left; "><fmt:formatNumber type="number" maxFractionDigits="0" value="${TotalAngsuran}" /></td>
					    	<td></td>
				     	</tr>
				     </tfoot>
				</table>
				
			</td>
		</tr>
	</table>
</body>
</html>