<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<script type="text/javascript">
function validateForm (){   

	var accno = document.forms["mgetinstl"]["accno"].value;
	var cusid = document.forms["mgetinstl"]["cusid"].value;
	var cusname = document.forms["mgetinstl"]["cusname"].value;
	var status = document.forms["mgetinstl"]["status"].value;

	if ((accno == "") && (cusid == "") && (cusname == "")){
		alert ("Account Number No Empty");
		return false;
	}
	if ((status == "Choose Account Status...")){
		alert ("Choose Account Status");
		return false;
	}
    return true;
}

</script>


<head>
<meta http-equiv="Content-Type:application/font-woff" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/bootstrap/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/ReportMCB/sorting/css/styles.css" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/ReportMCB/sorting/js/jquery-1.10.2.min.js"></script>
<script src="/ReportMCB/scripts/js/jquery.tablesorter.min.js"></script>

<title>Installment</title>

</head>
<body>
	<table width="1075px" border="0">
		<tr>
			<td align="center">
		        <form action="vGetInstl.do" method="POST" name="mgetinstl" onsubmit="return validateForm()">	
		        	<fieldset>
		        		<legend>Fields Report EDC</legend>
						    <div class="box-body">
						    	<div class="row">
						    		<div class="col-xs-5">
										<input class="form-control" placeholder="Please Input Terminal ID" type="text" name="term" id = "term" >
									</div>
						    	</div>
						    	<br>
						    	<div class="row">
						    		<div class="col-xs-5">
										<input class="form-control" placeholder="Please Input Merchant Name" type="text" name="name" id = "name" >
									</div>
						    	</div>
						    	<br>
						    	<div class="row">
						    		<div class="col-xs-5">
										<input class="form-control" placeholder="Please Input Parent Branch" type="text" name="parent" id = "parent" >
									</div>
						    	</div>
						    	<br>
						    	<div class="row">
						    		<div class="col-xs-5">
										<input class="form-control" placeholder="Please Input Branch Code" type="text" name="branch" id = "branch" >
									</div>
						    	</div>
						    	<br>
						        <div class="row">
						    		<div class="col-xs-5" align="left">
						    			<button type="submit" class="btn btn-success" onclick="return validateForm()">Submit</button>
									</div>
						    	</div>
							</div>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="1075px" border="0">
					<tr>
						<td>
							<table class="table table-striped" id="keywords">
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 10px; font-style: family font sains"><b>NO</b></th>
										<th style="width:80px; font-size: 10px;"><b>TERMINAL ID</b></th>
										<th style="width:300px; font-size: 10px;"><b>MERCHANT NAME</b></th>
										<th style="width:100px; font-size: 10px;"><b>PARENT CODE</b></th>
										<th style="width:100px; font-size: 10px;"><b>BRANCH CODE</b></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="edc" items="${edc}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td style="font-size: 10px">${inc}</td>
										<td>${edc.terminalId}</td>
										<td>${edc.merchantName}</td>
										<td>${edc.parent}</td>
										<td>${edc.branch}</td>
										<td>
											<a href="/Mars/vPeragaanCustD.do?norek=${lph.custAcNo}" target="_blank" class="label btn-info label-form">Update</a> 
											<a href="/Mars/vPeragaanHistD.do?ac=${lph.custAcNo}&nm=${lph.custName}" target="_blank" class="label btn-info label-form">Delete </a>
										</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
<script type="text/javascript">
$(function(){
  $('#keywords').tablesorter(); 
});
</script>
	
</body>
</html>