<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>Detail Kartu</title>
</head>
<body>
	<table width="1075px">
		<tr>
			<td>
				<div class="box">
					<div class="panel-group" id="accordion">
					    <div class="panel panel-default">
					      <div class="panel-heading">
					        <h4 class="panel-title">
					          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><span class="glyphicon glyphicon-th-list"></span> Detail Kartu</a>
					        </h4>
					      </div>
					      <div id="collapse1" class="panel-collapse collapse in">
					      	 <div class="panel-body">
					        	<div class="row">
					        		<div class="col-md-6">
				    					<div class="form-group">
				    						<label>NO CIF</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.no_cif}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>CURRENCY</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.currency}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>KATEGORI PRODUK</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.produk_kategori}">
				    					</div>
				    				</div>
				    				<c:forEach var="saldo" items="${saldo}">
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>OS PEMBIAYAAN SAAT INI</label><br>
				    						<c:choose>
				    							<c:when test="${salam.currency == 'IDR'}">
				    								<input class="form-control" type="text" disabled value = "Rp ${saldo.osSaatini}">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value = "$ ${saldo.osSaatini}">
				    							</c:otherwise>
				    						</c:choose>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>NO KARTU</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.no_kartu}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>ANGSURAN PER BULAN</label><br>
				    						<c:choose>
				    							<c:when test="${salam.currency == 'IDR'}">
				    								<input class="form-control" type="text" disabled value = "Rp ${saldo.angsuranBulanan}">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value = "$ ${saldo.angsuranBulanan}">
				    							</c:otherwise>
				    						</c:choose>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>NAMA PRODUK</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.nama_produk}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>PEMBAYARAN ANGSURAN TERAKHIR</label><br>
				    						<c:choose>
				    							<c:when test="${salam.currency == 'IDR'}">
				    								<input class="form-control" type="text" disabled value = "Rp ${saldo.angsuranTerakhir}">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value = "$ ${saldo.angsuranTerakhir}">
				    							</c:otherwise>
				    						</c:choose>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>NAMA NASABAH</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.nama_nasabah}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>TUNGGAKAN ANGSURAN</label><br>
				    						<c:choose>
				    							<c:when test="${salam.currency == 'IDR'}">
				    								<input class="form-control" type="text" disabled value = "Rp ${saldo.tunggakanAngsuran}">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value = "$ ${saldo.tunggakanAngsuran}">
				    							</c:otherwise>
				    						</c:choose>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>JANGKA WAKTU PEMBIAYAAN</label>
				    						<input class="form-control" type="text" disabled value = "${salam.jangka_waktu} Bulan" >
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>DENDA (JIKA ADA)</label><br>
				    						<c:choose>
				    							<c:when test="${salam.currency == 'IDR'}">
				    								<input class="form-control" type="text" disabled value = "Rp ${saldo.denda}">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value = "$ ${saldo.denda}">
				    							</c:otherwise>
				    						</c:choose>							
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>SISA JANGKA WAKTU PEMBIAYAAN</label>
				    						<c:choose>
										  		<c:when test="${total.sisa_angsuran == 'SUDAH LUNAS'}">
										  			<input class="form-control" type="text" disabled value ="${total.sisa_angsuran}">
										  		</c:when>
										  		<c:otherwise>
										  			<input class="form-control" type="text" disabled value ="${total.sisa_angsuran} Bulan">
										  		</c:otherwise>
										  	</c:choose>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>TUNGGAKAN DENDA (JIKA ADA)</label><br>
				    						<c:choose>
				    							<c:when test="${salam.currency == 'IDR'}">
				    								<input class="form-control" type="text" disabled value = "Rp ${saldo.tunggakanDenda}">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value = "$ ${saldo.tunggakanDenda}">
				    							</c:otherwise>
				    						</c:choose>	
				    					</div>
				    				</div>
				    				</c:forEach>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>TANGGAL PENCAIRAN</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.tanggal_pencairan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>CABANG PENGELOLA PEMBIAYAAN</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.branch}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>TANGGAL JATUH TEMPO PEMBIAYAAN</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.tanggal_jatuh_tempo}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>ALAMAT CABANG PENGELOLA PEMBIAYAAN</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.alamat}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>TANGGAL JATUH TEMPO ANGSURAN</label><br>
				    						<c:choose>
				    							<c:when test="${total.tanggal_jatuh_tempo_angsuran == 'SUDAH LUNAS'}">
				    								<input class="form-control" type="text" disabled value ="${total.tanggal_jatuh_tempo_angsuran}"> 
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value ="Setiap Tanggal ${total.tanggal_jatuh_tempo_angsuran}"> 
				    							</c:otherwise>
				    						</c:choose>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>NO TELP CABANG PENGELOLA PEMBIAYAAN</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.telp}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>TANGGAL ANGSURAN TERAKHIR</label><br>
				    						<input class="form-control" type="text" disabled value = "${total.tanggal_angsuran_terakhir}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>KODE ACCOUNT MANAGER</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.kode_marketing}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>TANGGAL ANGSURAN SELANJUTNYA</label><br>
				    						<input class="form-control" type="text" disabled value = "${total.tanggal_angsur_selanjutnya}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>NAMA ACCOUNT MANAGER</label><br>
				    						<input class="form-control" type="text" disabled value = "${salam.nama_marketing}">
				    					</div>
				    				</div>
					        	</div>
					         </div>
					       </div>
					     </div>
					 </div>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>