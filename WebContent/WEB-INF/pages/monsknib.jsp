<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript'
	src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemsknib(){
    var amont_a;
    var amont_b;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}
</script>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<div style="font-size: 11px;">
	<table border="0" align="center">
		<tr>
			<td align = "center">
				<fieldset style="width:700px; border:0px solid #999; border-radius:8px; box-shadow:0 0 5px #999;">
				<legend><b>Monitoring & Reporting SKN Internet Banking</b> </legend>
<!-- 				<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a> -->
				<form action="/ReportMCB/vMonMcbSknIB.do" method="POST" name="mib">
				<table border="0" width="600px">	
					<tr>
						<td style="width: 100px">Ref No</td>
						<td style="width: 10px">:</td>
						<td><input type="text" name="refno" id="refno"></td>
					</tr>
					<tr>
						<td style="width: 100px">From Account No</td>
						<td style="width: 10px">:</td>
						<td><input type="text" name="fromacc" id="fromacc"></td>
						<td style="width: 100px">To Account No</td>
						<td style="width: 10px">:</td>
						<td><input type="text" name="toacc" id="toacc"
							maxlength="16"><label style="color: red"></label></td>
					</tr>
					<tr>
						<td style="width: 100px">Start Amount</td>
						<td style="width: 10px">:</td>
						<td><input type="text" name="amont_a" id="amont_a"
							maxlength="16"><label style="color: red"></label></td>
						<td style="width: 100px">To Amount</td>
						<td style="width: 10px">:</td>
						<td><input type="text" name="amont_b" id="amont_b"
							maxlength="16"><label style="color: red"></label></td>
					</tr>
					<tr>
						<td colspan="2"></td>
						<td><input onclick="return validatemsknib()" type="submit" value="Ok" name="ok" id="ok" /> <!-- onclick="return validatemrtgs()" -->
						</td>
					</tr>
				</table>
				</form>
				</fieldset>
			</td>
		</tr>
				<br />
		<tr>
			<td align ="center">
			<div style="font-size: 13px; font-family: times new roman;">
			<table id="rounded-corner" style="width: 1075px">
				<tr>
					<td><display:table name="ltrn" id="ltrn" class="wb" requestURI="MonSknIbForward.do" pagesize="20" sort="external">
							<display:column title="<h3>Ref ID</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.refId}</display:column>
							<display:column title="<h3>Ref No</h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.refNo}</display:column>
							<display:column title="<h3>Transfer Type</h3>" sortable="true" style="width:80px; ">${ltrn.transferType}</display:column>
							<display:column title="<h3>Date Transaction</h3>" sortable="true" style="width:80px; ">${ltrn.dateTrx}</display:column>
							<display:column title="<h3>From Account No</h3>" sortable="true" style="width:170px; ">${ltrn.accountNo}</display:column>         
							<display:column title="<h3>To Account No</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.toAccountNo}</display:column>
							<display:column title="<h3>To Account Name</h3>" sortable="true" style="width:110px; ">${ltrn.toName}</display:column>
							<display:column title="<h3>To Bank Name</h3>" sortable="true" style="width:170px; ">${ltrn.toBankName}</display:column>
							<display:column title="<h3>Amount</h3>" sortable="true" style="width:120px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.amount}" /></display:column>
							<display:column title="<h3>Response Code</h3>" sortable="true" style="width:70px; ">${ltrn.responseCode}</display:column>
							<display:column title="<h3>Status</h3>" sortable="true" style="width:70px; ">${ltrn.status}</display:column>
						</display:table>
					</td>
				</tr>
			</table>
			</div>
			<logic:equal name="konfirmasi" value="err">
		    		<b>${dataresponse}</b>
			</logic:equal>
			</td>
		</tr>
	</table>
	</div>
</body>
</html>