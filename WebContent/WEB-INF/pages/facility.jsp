<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<script type="text/javascript">
	function cek(){
		var custid;
	    var custname;

	    custid = document.getElementById('custid').value;
	    custname = document.getElementById('custname').value;

	    if (custid == "" && custname == ""){
	    	alert('Customer Id Atau Customer Name Wajib Di Isi');
	    	return false;
		}
	    return true;
	}
</script>
<style type="text/css">
	h4{
    text-align: center;
    padding-top: 15px;
    padding-bottom: 15px;
}

.amount{
display: inline;
}

#pay{
  margin-top: 10px;
}

.form-control, .btn{
    border-radius: 0;

.standard-button{
 // background-color: #eebe25;
  color: rgba(0, 0, 0, 0.5);
  text-transform: uppercase;
  padding-top: 12px;
  padding-bottom: 12px;
  padding-left: 35px;
  padding-right: 35px;
  border-radius: 0;
  border:none;
  margin-top: 20px;
  margin-bottom: 20px;
  width: 16em;
  font-weight: 600;
  &:hover, &:active{
    // background-color: #616dab;
    background-color: #4b6a7e;
    color: white;
  }
}
.btn-anim{
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}
.btn-anim:after {
  content: '';
  position: absolute;
  z-index: -1;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/css/demos.css" /> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,400' rel='stylesheet' type='text/css'> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <script src="http://code.jquery.com/jquery-1.12.1.min.js"></script> -->
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
// $(function(){
// 	$("#dest").addSortWidget();
// });

function validateCif(cif) 
{
    var maintainplus = '';
    var numval = cif.value
    if ( numval.charAt(0)=='+' )
    {
        var maintainplus = '';
    }
    curphonevar = numval.replace(/[\\A-Za-z!"£$%^&\,*+_={};:'@#~,.Š\/<>?|`¬\]\[]/g,'');
    cif.value = maintainplus + curphonevar;
    var maintainplus = '';
    cif.focus;
}


function validateForm() {
    var cif = document.forms["facility"]["custId"].value;
    var name = document.forms["facility"]["name"].value;
    var status = document.forms["facility"]["status"].value;
    if ((cif == "") && (name == "")) {
        alert("Mohon Isi Customer ID atau Customer Name");
        return false;
    } 
    if ((status == "Choose Record Status...")){
		alert ("Record Status belum dipilih");
		return false;
	}
    return true;
}

</script>
<style type="text/css">
	h4{
    text-align: center;
    padding-top: 15px;
    padding-bottom: 15px;
}

.amount{
display: inline;
}

#pay{
  margin-top: 10px;
}

.form-control, .btn{
    border-radius: 0;

.standard-button{
 // background-color: #eebe25;
  color: rgba(0, 0, 0, 0.5);
  text-transform: uppercase;
  padding-top: 12px;
  padding-bottom: 12px;
  padding-left: 35px;
  padding-right: 35px;
  border-radius: 0;
  border:none;
  margin-top: 20px;
  margin-bottom: 20px;
  width: 16em;
  font-weight: 600;
  &:hover, &:active{
    // background-color: #616dab;
    background-color: #4b6a7e;
    color: white;
  }

}

.btn-anim{
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}
.btn-anim:after {
  content: '';
  position: absolute;
  z-index: -1;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}
  </style>
<title>Facility</title>
</head>
<body>
	<table width="1075px" border="0">
		<tr>
			<td align="center" >
		        <form action="facilityAct.do" method="POST" name="facility" onsubmit="returnCekValidation()">	
		        <fieldset>
		        	<legend>Detail Fasilitas</legend>
		        	<div class="box-body">
				    	<div class="row">
				    		<div class="col-xs-5">
								<input class="form-control" placeholder="Customer Id / No CIF" type="text" name="custId" id = "custId" maxlength="9" onkeyup="validateCif(this);">
							</div>
				    	</div>
				    	<br>
		        		<div class="row">
				    		<div class="col-xs-5">
								<input class="form-control" placeholder="Customer Name" type="text" name="name" id = "name" data-content="Default popover">
							</div>
				    	</div>
				    	<br>
				    	<div class="row">
				    		<div class="col-xs-5">
						        <select class="form-control" name="status" >
						        	<option selected>Choose Record Status...</option>
						        	<option value="All" name="All" id="All">All</option>
								    <option value="Open" name="Open" id="Open">Open</option>
								    <option value="Close" name="Close" id="Close">Close</option>
								</select>
							</div>
				    	</div>
				    	<br>
				    	<div class="row">
							<div class="col-xs-5" align="left">
								<button type="submit" class="btn btn-success" onclick="return validateForm()" value="send" name="btnsend" id="ok"> Send <span class="glyphicon glyphicon-send"></span></button>
				        	</div>
				        </div>
			        	<br>
					</div>
		        </fieldset> 
		        </form>
			</td>
		</tr>
		<tr>
			<td>
				<table class="table table-striped" id="dest" width="1075px" border="0">
				  <thead>
				        <tr align="center">
				        	<th style="font-size: 10px; font-style: family font sains"><b>#</b></th>
							<th style="font-size: 10px;"><b>NAMA NASABAH</b></th>
							<th style="font-size: 10px;"><b>NO FAS</b></th>
							<th style="font-size: 10px;"><b>FAS KE-</b></th>
							<th style="font-size: 10px;"><b>NAMA FASILITAS</b></th>
							<th style="font-size: 10px;"><b>CCY</b></th>
							<th style="font-size: 10px;"><b>LIMIT AMOUNT</b></th>
							<th style="font-size: 10px;"><b>AVAI AMOUNT</b></th>
							<th style="font-size: 10px;"><b>BLOCK AMOUNT</b></th>
							<th style="font-size: 10px;"><b>UTIL AMOUNT</b></th>
							<th style="font-size: 10px;"><b>START DATE</b></th>
							<th style="font-size: 10px;"><b>EXP DATE</b></th>
							<th style="font-size: 10px;"><b>RECORD STAT</b></th>
							<th style="font-size: 10px;"><b>ACTION</b></th>
						</tr>
				    </thead>
				    <tbody>
				    	<c:set var="inc" value="0" />
						<c:forEach var="lph" items="${lph}" >
						<tr>
							<c:set var="inc" value="${inc + 1}" />
							<td style="font-size: 10px">${inc}</td>
							<td style="font-size: 10px">${lph.namaNasabah}</td>
							<td style="font-size: 10px">${lph.noFasilitas}</td>
							<td style="font-size: 10px">${lph.lineSerial}</td>
							<td style="font-size: 10px">${lph.namaFasilitas}</td>
							<td style="font-size: 10px">${lph.ccy}</td>
							<td style="text-align: right; font-size: 10px;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lph.limitAmount}" /></td>
							<td style="text-align: right; font-size: 10px;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lph.availableAmount}" /></td>
							<td style="text-align: right; font-size: 10px;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lph.blockAmount}" /></td>
							<td style="text-align: right; font-size: 10px;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lph.utilization}" /></td>
							<td style="font-size: 10px ; ">${lph.startDate}</td>
							<td style="font-size: 10px">${lph.expiryDate}</td>
							<c:choose>
								<c:when test="${lph.recordStat == 'OPEN'}">
									<td><span class="label label-info">${lph.recordStat}</span></td>
								</c:when>
								<c:when test="${lph.recordStat == 'CLOSE'}">
									<td><span class="label label-danger">${lph.recordStat}</span></td>
								</c:when>
							</c:choose>
<!-- 							<td style="font-size: 10px"> -->
<%-- 								<c:if test="${lph.recordStat == 'O'}"> --%>
<%-- 									<c:out value="Open"></c:out> --%>
<%-- 								</c:if> --%>
<%-- 								<c:if test="${lph.recordStat == 'C'}"> --%>
<%-- 									<c:out value="Close"></c:out> --%>
<%-- 								</c:if> --%>
<!-- 							</td> -->
							<td style="font-size: 10px">
								<a href="/ReportMCB/detailFasilitas.do?fasilitasKe=${lph.fasilitasKe}" target="_blank" class="btn btn-success" role="button"> Detail <span class="glyphicon glyphicon-list-alt "></span></a>														  
							</td>
						</tr>
						</c:forEach>
				    </tbody>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>