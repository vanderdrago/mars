<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<script type='text/javascript' src='/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<script type='text/javascript' src='/ReportMCB/scripts/money/jquery.js' ></script>
<script type="text/javascript">
function valReject(){
    var ret = confirm("Reject data?");
    return ret;	
}
function valApprove(){
    var ret = confirm("Otorisasi data ?");
    return ret;
}
function valUpdatests(){
	var email_to = document.getElementById('email_to').value;
	var updtsts = document.getElementById('updtsts').value;
	if (email_to == ""){
        alert('Silahkan isikan email to');
        return false;
    }
	if (updtsts == "" || updtsts == null){
		alert('Silahkan pilih status.');
	    return false;
	}
    var ret = confirm("Update status data ?");
    return ret;
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table border="0" align="center">
	<tr>
		<td> 
			<fieldset style="width:330px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			    <legend> <b>Daftar Nasabah e-Statement</b> </legend>
			    <form action="eStateCustView.do" method="POST" name="jurnal">
			    <table border="0">
			    	<tr>
			            <td colspan="3"><a href="eStatReg.do">Registrasi</a></td>
			        </tr>
			        <tr>
			            <td >No. Rekening</td>
			            <td >:</td>
			            <td ><input type="text" name="acc" id="acc" maxlength="10"></td>
			        </tr>
			        <tr>
			            <td >Nama Nasabah</td>
			            <td >:</td>
			            <td ><input type="text" name="nsbh" id="nsbh" ></td>
			        </tr>
			        <tr>
			        	<td>Status</td>
						<td>:</td>
						<td>
							<select name="sts" id="sts">
							  <option value="">Semua</option>
							  <option value="I">Unauthorized</option>
							  <option value="H">Hold</option>
							  <option value="O">Active</option>
							  <option value="R">Reject</option>
							  <option value="C">Closed</option>
							</select> 
						</td>
			        </tr>			        
			        <tr>
			            <td colspan="2"></td>
			            <td>
			                <input type="submit" value="Lihat" name="btn" id="print" onclick="return valVestate()" />			                			                
			            </td>
			        </tr>
			    </table>
			    </form>
			</fieldset>
		</td>
	</tr>
</table>

<logic:equal name="data" value="konfirm">
	${ respon }
</logic:equal>


<logic:equal name="data" value="view">
<table border="0" width="1050px;">
<tr>
	<td>
	<div style="  float:left; text-align: left; ">	
	<table border="0">
		<tr>
	    	<td><a href="manageEstateFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="manageEstateFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="manageEstateFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="manageEstateFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageEstateFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style=" float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr align="center" >
					            <th style="width:100px; "><b>NO. REKENING</b></th>
					            <th style="width:300px; "><b>NAMA REKENING</b></th>
					            <th style="width:100px;" align="center"><b>JNS NASABAH</b></th>
					            <th style="width:100px; "><b>MAKER ID</b></th>
					            <th style="width:100px; "><b>CHECKER ID</b></th>
					            <th style="width:150px;"><b>STATUS</b></th>
					            <th style="width:150px;"><b>KETERANGAN</b></th>
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="lcust" items="${lcust}" >
						        <tr style="font-size: 11px; ">
						            <td ><a href="eStatdetail.do?acc=${lcust.account}">${lcust.account}</a></td>
						            <td >${lcust.cust_name}</td>
						            <td >${lcust.cust_type_desc}</td>
						            <td >${lcust.user_input}</td>
						            <td >${lcust.user_aut}</td>
						            <td >${lcust.sts_data_desc}</td>
						            <td >${lcust.option}</td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>
	</td>
</tr>
</table>
</logic:equal>

<logic:equal name="data" value="cust_oto_apprv">
<table border="0" width="700px;">
<tr>
	<td>
		<fieldset style="border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; padding-right: 15px;">
			<form action="eStateApprv.do" method="POST" name="jurnal">
			<table border="0">
				<tr>
					<td colspan="3" ><b>${jnsnsbh }</b></td>
				</tr>
				<tr><td>Status</td><td>:</td><td><b>${cust.sts_data_desc }</b></td></tr>
				<tr><td width="150px">Nomor Rekening</td><td width="5px">:</td><td width="500"> ${ cust.account }</td></tr>
				<tr><td>Cabang</td><td>:</td><td >${ cust.branch_code } - ${ cust.branch_name }</td></tr>
				<tr><td>No. CIF</td><td>:</td><td >${ cust.cust_no }</td></tr>
				<tr><td>Nama Rekening</td><td>:</td><td >${ cust.cust_name }</td></tr>
				<tr><td>Nama Produk</td><td>:</td><td >${ cust.acc_class } - ${ cust.acc_class_desc }</td></tr>
				<tr><td>Tanggal Lahir</td><td>:</td><td >${ cust.dt_birth }</td></tr>
				<tr><td>Nama Ibu Kandung</td><td>:</td><td >${ cust.ibu_kndng }</td></tr>
				<tr><td valign='top'>Alamat</td><td valign='top'>:</td><td colspan="4">${ cust.cust_addrs }</td></tr>
				<tr><td>Email (to)</td><td>:</td><td >${ cust.email }</td></tr>
				<tr><td>Email (cc)</td><td>:</td><td >${ cust.email_cc }</td></tr>
				<tr><td><br /></td><td></td><td></td></tr>
				<tr><td>Maker Id</td><td>:</td><td>${ cust.user_input }</td></tr>		
				<tr><td >Auth Id</td><td width="3px">:</td><td>${ cust.user_aut }</td></tr>
				<tr><td >Tgl Registrasi</td><td width="3px">:</td><td>${ cust.tgl_input }</td></tr>				
				<tr><td >Cabang Registrasi</td><td width="3px">:</td><td>${ cust.branch_reg } - ${ cust.branch_reg_name }</td></tr>
				<tr><td colspan="2"></td><td><input type="submit" value="Approve" name="btn" id="print" onclick="return valApprove()" /><input type="submit" value="Reject" name="btn" id="print" onclick="return valReject()" /></td></tr>			
			</table>
			</form>
		</fieldset>
	</td>
</tr>
</table>
</logic:equal>

<logic:equal name="data" value="cust">
<table border="0" width="700px;">
<tr>
	<td>
		<fieldset style="border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; padding-right: 15px;">
			<form action="eStateApprv.do" method="POST" name="jurnal">
			<table border="0">
				<tr>
					<td colspan="3" ><b>${jnsnsbh }</b></td>
				</tr>
				<tr><td width="150px">Nomor Rekening</td><td width="5px">:</td><td width="500"> ${ cust.account }</td></tr>
				<tr><td>Cabang</td><td>:</td><td >${ cust.branch_code } - ${ cust.branch_name }</td></tr>
				<tr><td>No. CIF</td><td>:</td><td >${ cust.cust_no }</td></tr>
				<tr><td>Nama Rekening</td><td>:</td><td >${ cust.cust_name }</td></tr>
				<tr><td>Nama Produk</td><td>:</td><td >${ cust.acc_class } - ${ cust.acc_class_desc }</td></tr>
				<tr><td>Tanggal Lahir</td><td>:</td><td >${ cust.dt_birth }</td></tr>
				<tr><td>Nama Ibu Kandung</td><td>:</td><td >${ cust.ibu_kndng }</td></tr>
				<tr><td valign='top'>Alamat</td><td valign='top'>:</td><td colspan="4">${ cust.cust_addrs }</td></tr>
				<tr><td>Email (to)</td><td>:</td><td >${ cust.email }</td></tr>
				<tr><td>Email (cc)</td><td>:</td><td >${ cust.email_cc }</td></tr>
				<tr><td><br /></td><td></td><td ></td></tr>
				<tr>
					<td>Maker Id</td><td>:</td><td>${ cust.user_input }</td>
				</tr>	
				<tr>
					<td >Auth Id</td><td width="3px">:</td><td>${ cust.user_aut }</td>
				</tr>
				<tr>
					<td >Tgl Registrasi</td><td width="3px">:</td><td>${ cust.tgl_input }</td>
				</tr>				
				<tr>
					<td >Cabang Registrasi</td><td width="3px">:</td><td>${ cust.branch_reg } - ${ cust.branch_reg_name }</td>
				</tr>
				<tr>
					<td >Status</td><td width="3px">:</td><td><b>${ cust.sts_data_desc }</b></td>
				</tr>						
			</table>
			</form>
		</fieldset>
	</td>
</tr>
</table>
</logic:equal>

<logic:equal name="data" value="cust_oto_updtsts">
<table border="0" width="700px;">
<tr>
	<td>
		<fieldset style="border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; padding-right: 15px;">
			<form action="eStateUpdtSts.do" method="POST" name="jurnal">
			<table border="0">
				<tr>
					<td colspan="3" ><b>${jnsnsbh }</b></td>
				</tr>
				<tr><td>Status</td><td>:</td><td><b>${cust.sts_data_desc }</b></td></tr>
				<tr><td width="150px">Nomor Rekening</td><td width="5px">:</td><td width="500"> ${ cust.account }</td></tr>
				<tr><td>Cabang</td><td>:</td><td >${ cust.branch_code } - ${ cust.branch_name }</td></tr>
				<tr><td>No. CIF</td><td>:</td><td >${ cust.cust_no }</td></tr>
				<tr><td>Nama Rekening</td><td>:</td><td >${ cust.cust_name }</td></tr>
				<tr><td>Nama Produk</td><td>:</td><td >${ cust.acc_class } - ${ cust.acc_class_desc }</td></tr>
				<tr><td>Tanggal Lahir</td><td>:</td><td >${ cust.dt_birth }</td></tr>
				<tr><td>Nama Ibu Kandung</td><td>:</td><td >${ cust.ibu_kndng }</td></tr>
				<tr><td valign='top'>Alamat</td><td valign='top'>:</td><td >${ cust.cust_addrs }</td></tr>
				<tr><td>Email (to)</td><td>:</td><td >${ cust.email }</td></tr>
				<tr><td>Email (cc)</td><td>:</td><td >${ cust.email_cc }</td></tr>				
				<tr>
					<td>Maker Id</td><td>:</td><td>${ cust.user_input }</td>
				</tr>	
				<tr>
					<td width="50px">Auth Id </td><td width="3px">:</td><td>${ cust.user_aut }</td>
				</tr>
				<tr>
					<td >Tgl Registrasi</td><td width="3px">:</td><td>${ cust.tgl_input }</td>
				</tr>				
				<tr>
					<td >Cabang Registrasi</td><td width="3px">:</td><td>${ cust.branch_reg } - ${ cust.branch_reg_name }</td>
				</tr>
				<tr><td>Status</td><td>:</td>
					<td colspan="4"><select name="updtsts" id="updtsts">
						<option value="">Pilih</option>
						  <option value="H">Hold</option>
						  <option value="O">Active</option>
						  <option value="C">Closed</option>
						</select> 
					</td>
				</tr>
				<tr><td colspan="2"></td><td colspan="4"><input type="submit" value="Update" name="btn" id="print" onclick="return valUpdatests()" /></td></tr>
					
			</table>
			</form>
		</fieldset>
	</td>
</tr>
</table>
</logic:equal>

<logic:equal name="data" value="cust_edit">
<table border="0" width="700px;">
<tr>
	<td>
		<fieldset style="border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; padding-right: 15px;">
			<form action="eStateEdt.do" method="POST" name="jurnal">
			<table border="0">
				<tr>
					<td colspan="6" ><b>${jnsnsbh }</b></td>
				</tr>
				<tr><td width="150px">Nomor Rekening</td><td width="5px">:</td><td colspan="4" width="500"> ${ cust.account }</td></tr>
				<tr><td>Cabang</td><td>:</td><td colspan="4">${ cust.branch_code } - ${ cust.branch_name }</td></tr>
				<tr><td>No. CIF</td><td>:</td><td colspan="4">${ cust.cust_no }</td></tr>
				<tr><td valign='top'>Nama Rekening</td><td valign='top'>:</td><td colspan="4">${ cust.cust_name }</td></tr>
				<tr><td valign='top'>Nama Produk</td><td valign='top'>:</td><td colspan="4">${ cust.acc_class } - ${ cust.acc_class_desc }</td></tr>
				<tr><td>Tanggal Lahir</td><td>:</td><td colspan="4">${ cust.dt_birth }</td></tr>
				<tr><td>Nama Ibu Kandung</td><td>:</td><td colspan="4">${ cust.ibu_kndng }</td></tr>
				<tr><td valign='top'>Alamat</td><td valign='top'>:</td><td colspan="4">${ cust.cust_addrs }</td></tr>
				<tr><td>Email to (sebelum)</td><td>:</td><td colspan="4">${ cust.email }</td></tr>
				<tr><td>Email cc (sebelum)</td><td>:</td><td colspan="4">${ cust.email_cc }</tr>
				<tr><td>Email (to)</td><td>:</td><td colspan="4"><input type='text' name='email_to' id='email_to' size='50'><label style='color:red'>*</label></td></tr>
				<tr><td>Email (cc)</td><td>:</td><td colspan="4"><input type='text' name='email_cc' id='email_cc' size='50'></td></tr>					
				<tr><td>Status</td><td>:</td>
					<td colspan="4"><select name="updtsts" id="updtsts">
						<option value="">Pilih</option>
						  <option value="H">Hold</option>
						  <option value="O">Active</option>
						</select> 
					</td>
				</tr>
				<tr><td colspan="6"> <br /></td></tr>
				<tr>
					<td>Maker Id</td><td>:</td><td>${ cust.user_input }</td>
				</tr>	
				<tr>
					<td >Auth Id</td><td width="3px">:</td><td>${ cust.user_aut }</td>
				</tr>
				<tr>
					<td >Tgl Registrasi</td><td width="3px">:</td><td>${ cust.tgl_input }</td>
				</tr>				
				<tr>
					<td >Cabang Registrasi</td><td width="3px">:</td><td>${ cust.branch_reg } - ${ cust.branch_reg_name }</td>
				</tr>
				<tr><td colspan="2"></td><td colspan="4"><input type="submit" value="Update" name="btn" id="print" onclick="return valUpdatests()" /></td></tr>
					
			</table>
			</form>
		</fieldset>
	</td>
</tr>
</table>
</logic:equal>


</body>
</html>