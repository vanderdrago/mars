<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Facility</title>
</head>
<body>
<table width="100%" border="0">
	<tr >
		<td align="center">
			<fieldset style="width:1075px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    	<legend> <b>Fields Facility Reports</b> </legend>
			<table width="1075px" border="0.5" >
				<tr style=" background-color: #66CCCC;">
					<td align="center" colspan="7"><b><h3>Facilities Details</h3></b></td>
				</tr>
				<tr >
					<td >Liab NO</td>
					<td >:</td>
					<td >${faclrn.liab_no}</td>
					<td >Limit Amount</td>
					<td >:</td>
					<td style=" text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.limit_amount}" /></td>
				</tr>
				<tr>
					<td >Liab Name</td>
					<td >:</td>
					<td >${faclrn.liab_name}</td>
					<td >Collateral Amount</td>
					<td >:</td>
					<td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.limit_amount}" /></td>
				</tr>
				<tr>
					<td >Line Code</td>
					<td >:</td>
					<td >${faclrn.line_code}</td>
					<td >Transfer Amount</td>
					<td >:</td>
					<td style=" text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.transfer_amount}" /></td>
				</tr>
				<tr>
					<td >Line Serial</td>
					<td >:</td>
					<td >${faclrn.line_serial}</td>
					<td >Effective Line Amount</td>
					<td >:</td>
					<td style=" text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.dsp_eff_line_amount}" /></td>
				</tr>
				<tr>
					<td >Branch</td>
					<td >:</td>
					<td >${faclrn.brn}</td>
					<td >Uncollected Amount</td>
					<td >:</td>
					<td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.uncollected_amount}" /></td>
				</tr>				
				<tr>
					<td >Curency</td>
					<td >:</td>
					<td >${faclrn.line_currency}</td>
					<td >Block Amount</td>
					<td >:</td>
					<td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.block_amount}" /></td>
				</tr>
				<tr>
					<td >Description</td>
					<td >:</td>
					<td >${faclrn.description}</td>
					<td >Approved Limit Amount</td>
					<td >:</td>
					<td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.approved_amt}" /></td>
				</tr>
				<tr>
					<td >Category</td>
					<td >:</td>
					<td >${faclrn.category}</td>
					<td >Available Amount</td>
					<td >:</td>
					<td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.available_amount}" /></td>
				</tr>
				<tr>
					<td >Status</td>
					<td >:</td>
					<td >${faclrn.status_av}</td>
					<td >Date Of First OD</td>
					<td >:</td>
					<td >${faclrn.date_of_first_od}</td>
				</tr>
				<tr>
					<td >Line Start Date</td>
					<td >:</td>
					<td >${faclrn.line_start_date}</td>
					<td >Date Of Last OD</td>
					<td >:</td>
					<td >${faclrn.date_of_last_od}</td>
				</tr>
				<tr>
					<td >Expiry Date</td>
					<td >:</td>
					<td >${faclrn.line_expiry_date}</td>
					<td >Amount Utilized Today</td>
					<td >:</td>
					<td style="text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.amount_utilised_today}" /></td>
				</tr>
				<tr>
					<td >Last New Utilization Date</td>
					<td >:</td>
					<td >${faclrn.last_new_util_date}</td>
					<td >Utilization Amount</td>
					<td >:</td>
					<td style="text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.utilisation}" /></td>
				</tr>
				<tr>
					<td >Status Record</td>
					<td >:</td>
					<td >${faclrn.status_rcd}</td>
					<td >Tanked Utilization</td>
					<td >:</td>
					<td style="text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.tanked_util}" /></td>
				</tr>
				<tr>
					<td >Status Auth</td>
					<td >:</td>
					<td >${faclrn.status_oto}</td>
					<td >Netting Contribution</td>
					<td >:</td>
					<td style="text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.netting_amount}" /></td>
				</tr>
				<tr>
					<td ><u><b>Pool Links</b></u></td>
				</tr>
				<tr>
					<td >Pool Code</td>
					<td >:</td>
					<td >${faclrn.pool_code}</td>
					<td >Pool Description</td>
					<td >:</td>
					<td >${faclrn.pool_description}</td></td>
				</tr>
				<tr>
					<td >Pool Currency</td>
					<td >:</td>
					<td >${faclrn.pool_ccy}</td>
					<td >Pool Amount</td>
					<td >:</td>
					<td style="text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.pool_amount}" /></td>
				</tr>
				<tr>
					<td >Pool Util</td>
					<td >:</td>
					<td >${faclrn.pool_util}</td>
					<td >Facility Branch</td>
					<td >:</td>
					<td >${faclrn.facility_branch_code}</td></td>
				</tr>
				<tr>
					<td >Percentage Pool</td>
					<td >:</td>
					<td >${faclrn.percentage_of_pool}</td>
					<td >Facility Amt Pool</td>
					<td >:</td>
					<td style="text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.facility_amount_pool_ccy}" /></td>
				</tr>
				<tr>
					<td >Facility Currency</td>
					<td >:</td>
					<td >${faclrn.facility_currency}</td>
					<td >Facility Amount</td>
					<td >:</td>
					<td style="text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${faclrn.facility_amount}" /></td>
				</tr>
				<tr>
					<td >Type</td>
					<td >:</td>
					<td >${faclrn.type}</td>
					<td >Percentage Contract</td>
					<td >:</td>
					<td >${faclrn.percentage_of_contract}</td></td>
				</tr>
			</table>
			</fieldset>
		</td>
	</tr>
</table>
</body>
</html>