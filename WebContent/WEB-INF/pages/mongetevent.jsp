<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function validatevent(){
    var accno;
    
    accno = document.getElementById('accno').value;
	
	if (accno == "" || "".equals(accno)){
    	alert('No Kartu Wajib Diisi');
    	return false;
    }
    return true;
}
</script>
<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Event Detail</title>
</head>
<body>
<table border="0">
	<tr>
		<td align="center">
			<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    	<legend> <b>Event Fields Detail</b> </legend>
	        <form action="/ReportMCB/vGetevent.do" method="POST" name="mgetev">	    
	        <table border="0">
	           <tr>
	                <td><input type="text" name="accno" id="accno" class="inputs" placeholder="No Kartu" style=" width : 400px;"></td>
	            </tr>
	            <tr>
	            	<td align="right"><a href="#" onclick="document.forms['mgetev'].submit(); return valdata();" class="blue btn">Search</a></td>
	            </tr>
	        </table>
	        </form>
			</fieldset>
		</td>
	</tr>
	<tr style="color:black;">
		<td align="center">
			<table border="0" width="1075px">
				<tr>
					<td>
						<table border="0">
							<tr>
								<td>
									<c:if test="${sawalEvent >= 1}">Page ${sawalEvent} Of ${noOfPagesEvent}</c:if>
								</td>
								<td align="left">
  	  							  <c:if test="${sawalEvent >= 1}"><a href="MgetEventpgg.do?page=1" class="page">First</a></c:if>
	  	  						</td>	
  	  						  	<td>
		       					  <c:if test="${currentPageEvent != 1 && (currentPageEvent - 1 > 0)}">
								  <a href="MgetEventpgg.do?page=${currentPageEvent - 1}" class="page">Previous</a>
							      </c:if>
		       			      	</td>
			       			    <td>
			       			      	<table>
			       			      	  	  <tr>
			       			      	  	  	  <td>
			       			      	  	  	  	  <c:if test="${sawalEvent >= 1}">
			       			      	  	  	  	  	  <c:forEach begin="${sawalEvent}" end="${sakhirEvent}" var="i">
			       			      	  	  	  	  	  	 <c:choose><td><a href="MgetEventpgg.do?page=${i}" class="page">${i}</a></td></c:choose>
			       			      	  	  	  	  	  </c:forEach>
			       			      	  	  	  	  </c:if>
			       			      	  	  	  </td>
			       			      	  	  </tr>
			       			      	  </table>
			       			      </td>
			       			      <td>
						     		  <c:if test="${currentPageEvent lt noOfPagesEvent}">
							          	<a href="MgetEventpgg.do?page=${currentPageEvent + 1}" class="page">Next</a>
									  </c:if>
						    	  </td>
						    	  <td>
						    	  	  <c:if test="${noOfPagesEvent - 9 > 0}">
						       			<a href="MgetEventpgg.do?page=${currentPageEvent - 9}"class="page">Last</a>
						       		  </c:if>
						    	  </td>
							</tr>
						</table>
					</td>
					<td align="right">
						Search Result - ${noOfRecordsEvent} Result 
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="color: black;">
		<td colspan="2">
			<div style="font-size:13px; font-family:times new roman;">
			<table id="rounded-corner" style="width:1075px"> 
				<thead>
					<tr align="center">
						<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
						<th style="width:50px; font-size: 12px;"><b>BRANCH</b></th>
						<th style="width:75px; font-size: 12px;"><b>PRODUCT CODE</b></th>
						<th style="width:100px; font-size: 12px;"><b>VALUE DATE</b></th>
						<th style="width:100px; font-size: 12px;"><b>SCHEDULE DUE DATE</b></th>
						<th style="width:100px; font-size: 12px;"><b>CCY</b></th>
						<th style="width:100px; font-size: 12px;"><b>ACCOUNT NO</b></th>
						<th style="width:50px; font-size: 12px;"><b>EVENT</b></th>
						<th style="width:100px; font-size: 12px;"><b>AMOUNT</b></th>
						<th style="width:50px; font-size: 12px;"><b>AMOUNT TAG</b></th>
						<th style="width:25px; font-size: 12px;"><b>DR/CR</b></th>
						<th style="width:100px; font-size: 12px;"><b>REF NO</b></th>
						<th style="width:100px; font-size: 12px;"><b>ACC</b></th>
						<th style="width:150px; font-size: 12px;"><b>ACC DESC</b></th>
						<th style="width:100px; font-size: 12px;"><b>TRN CODE</b></th>
						<th style="width:100px; font-size: 12px;"><b>TRN DESC</b></th>
					</tr>
				</thead>
				<tbody>
					<c:set var="inc" value="0" />
					<c:forEach var="ltrn" items="${ltrn}" >
					<tr style="font-size: 11px;">
						<c:set var="inc" value="${inc + 1}" />
						<td>${inc}</td>
						<td>${ltrn.branch_code}</td>
						<td>${ltrn.product_code}</td>
						<td>${ltrn.value_date}</td>
						<td>${ltrn.schedule_due_date}</td>
						<td>${ltrn.ccy}</td>
						<td>${ltrn.account_number}</td>
						<td>${ltrn.event_code}</td>
						<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.amount}" /></td>
						<td>${ltrn.amount_tag}</td>
						<td>${ltrn.drcr_ind}</td>
						<td>${ltrn.refno}</td>
						<td>${ltrn.acc}</td>
						<td>${ltrn.ac_desc}</td>
						<td>${ltrn.trn_code}</td>
						<td>${ltrn.trn_desc}</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
		</td>
	</tr>
</table>
</body>
</html>