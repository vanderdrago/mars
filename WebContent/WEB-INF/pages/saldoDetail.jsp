<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
$(function(){
	$("#prd1").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: "yymm",
		showButtonPanel: true,
		onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	});
	$("#prd2").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: "yymm",
		showButtonPanel: true,
		onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	});
});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>

<style type="text/css">
	.ui-datepicker-calendar {
	display: none;
}
</style>

<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Peragaan Saldo Rata-Rata</title>
</head>
<body>
	<table border="0" width="100%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
				<legend align="center"><b>PERAGAAN SALDO RATA-RATA</b></legend>
				<form action="vSaldoAct.do" name="jurnal" method="POST">
				<table border="0">
					<tr>
						<td>
							<table border="0" >
								<tr>
									<td>No. Rekening</td>
									<td>:</td>
									<td>${acc}</td>
								</tr>
								<tr>
									<td>Nama</td>
									<td>:</td>
									<td>${nama}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><input type="text" name="prd1" id="prd1" class="inputs" placeholder="Silahkan Pilih Mulai Periode (YYYY-MM)" style=" width : 400px;"></td>
					</tr>
					<tr>
						<td><input type="text" name="prd2" id="prd2" class="inputs" placeholder="Silahkan Pilih Sampai Periode (YYYY-MM)" style=" width : 400px;"></td>
					</tr>
					<tr>
						<td align="left"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
					</tr>
				</table>
				</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color:black">
  	 		 <td align="center">
  		  		<table border="0" width="1075px">
<!--   		    <div class="cd-pagination"> -->
  		  	  	  <tr>
	  		  	  	  <td>
	  		  	  	   	  <table border="0">
	  		  	  	   	  	  <tr>
	  		  	  	   	  	  	  <td>
	  		  	  	   	  	  	  	<c:if test="${sawalSaldoDetail >= 1}">Page ${sawalSaldoDetail} Of ${noOfPagesSaldoDetail}</c:if>
	  		  	  	   	  	  	  </td>
	  		  	  	   	  	  	  <td align="left">
	  	  							  <c:if test="${sawalSaldoDetail >= 1}"><a href="manageLphSaldoDetailFwd.do?page=1" class="page">First</a></c:if>
	  	  						  </td>	
	  	  						  <td>
			       					  <c:if test="${currentPageSaldoDetail != 1 && (currentPageSaldoDetail - 1 > 0)}">
									  <a href="manageLphSaldoDetailFwd.do?page=${currentPageSaldoDetail - 1}" class="page">Previous</a>
								      </c:if>
			       			      </td>
			       			      <td>
			       			      	  <table>
			       			      	  	  <tr>
			       			      	  	  	  <td>
			       			      	  	  	  	  <c:if test="${sawalSaldoDetail >= 1}">
			       			      	  	  	  	  	  <c:forEach begin="${sawalSaldoDetail}" end="${sakhirSaldoDetail}" var="i">
			       			      	  	  	  	  	  	 <c:choose><td><a href="manageLphSaldoDetailFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
			       			      	  	  	  	  	  </c:forEach>
			       			      	  	  	  	  </c:if>
			       			      	  	  	  </td>
			       			      	  	  </tr>
			       			      	  </table>
			       			      </td>
			       			      <td>
						     		  <c:if test="${currentPageSaldoDetail lt noOfPagesSaldoDetail}">
							          	<a href="manageLphSaldoDetailFwd.do?page=${currentPageSaldoDetail + 1}" class="page">Next</a>
									  </c:if>
						    	  </td>
						    	  <td>
						    	  	  <c:if test="${noOfPagesSaldoDetail - 9 > 0}">
						       			<a href="manageLphSaldoDetailFwd.do?page=${currentPageSaldoDetail - 9}"class="page">Last</a>
						       		  </c:if>
						    	  </td>
	  		  	  	   	  	  </tr>
	  		  	  	   	  </table>
	  		  	  	    </td>
  		  	  	  	<td align="right">
  		  	  	  		<c:if test="${sawalSaldoDetail >= 1}">Search Result - ${noOfRecordsSaldoDetail} Result</c:if>
  		  	  	  	</td>
  		  	  	</tr>
  		  	    <tr>
  		  	  	  <td colspan="2">
					<div style="font-size:13px; font-family:times new roman;">
					<table id="rounded-corner" style="width:1075px"> 
						<thead>
							<tr align="center">
								<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
								<th style="width:50px; font-size: 12px;"><b>BRANCH</b></th>
								<th style="width:250px; font-size: 12px;"><b>BRANCH NAME</b></th>
								<th style="width:100px; font-size: 12px;"><b>ACC NO</b></th>
								<th style="width:300px; font-size: 12px;"><b>NAMA</b></th>
								<th style="width:100px; font-size: 12px;"><b>PERIOD CODE</b></th>
								<th style="width:100px; font-size: 12px;"><b>CUST NO</b></th>
								<th style="width:50px; font-size: 12px;"><b>CCY</b></th>
								<th style="width:100px; font-size: 12px;"><b>NOMINAL EKIVALEN</b></th>
								<th style="width:100px; font-size: 12px;"><b>NOMINAL</b></th>
							</tr>
						</thead>
						<tbody>
							<c:set var="inc" value="0" />
							<c:forEach var="lphSaldoDetail" items="${lphSaldoDetail}" >
							<tr style="font-size: 11px;">
								<c:set var="inc" value="${inc + 1}" />
								<td>${inc}</td>
								<td>${lphSaldoDetail.branch}</td>
								<td>${lphSaldoDetail.branch_name}</td>
								<td>${lphSaldoDetail.acc_no}</td>
								<td>${lphSaldoDetail.acc_desc}</td>
								<td>${lphSaldoDetail.period}</td>
								<td>${lphSaldoDetail.cust_no}</td>
								<td>${lphSaldoDetail.ccy}</td>
								<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lphSaldoDetail.lcy_bal}" /></td>
								<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lphSaldoDetail.acy_bal}" /></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</td>
  		 </tr>
<!--   		</div> -->
  	</table>
  </td>
 </tr>
	</table>
</body>
</html>