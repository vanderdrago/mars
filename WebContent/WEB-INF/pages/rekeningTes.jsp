<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<fieldset color="black" style="color:black;width:1000px; border:1px solid black; border-radius:8px; padding: 10px;">
	<table width="100%">
		<tr>
			<td style="text-align: center;"><b>Laporan Mutasi Teller Dan Back Office</b></td>
		</tr>
	</table>
	<br />
	<div style="font-size: 11px;" align="left">	
		<table border="0">				
			<tr >
				<td style="width: 100px; ">User Id</td>
				<td>:</td>
				<td><input type="text" name="userid" id="userid" size="33"></td>
			</tr>
			<tr>
		        <td></td>
				<td></td>
				<td><input type="submit" value="Lihat" name="btn" id="btn" onclick="return getUserId('UR')"/></td>
			</tr>
		</table>
		<br />
		<div id="tbl_userid" class="tbl_userid"></div>
		<br />
		<logic:equal value="view" name="resp">
		${desc}
		</logic:equal>			
	</div>
</fieldset>
</body>
</html>