<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">
function validateUpload(){
	var theFile;
	theFile = document.getElementById('theFile').value; 
    if (theFile == ""){
        alert('File tidak ditemukan');
        return false;
	}
	return true;
}
</script>
<fieldset style="width:300px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	<legend> <b>Upload file Konversi Rekening</b> </legend>
	<html:form  method="POST" action="konvrekuploadFileAct.do" enctype="multipart/form-data">
	    <table border="0">
			<tr>
	        	<td>
	            	<input type="file" name="theFile" id="theFile" size="50">
				</td>
	        </tr>
	        <tr>
	        	<td>
	            	<input onclick=" return validateUpload()" type="submit" value="Upload">
				</td>
			</tr>
	    </table>    
	</html:form >
</fieldset>

<logic:equal name="conf" value="resp">
	<p style="color: black;">${respon}</p> 
	<form action="konvrekReadFile.do" method="POST" name="jurnal">
		<table border="0">
			<tr>
				<td colspan="6">
              		<input type="submit" value="Download" name="btn" id="download" />
					<input type="submit" value="Clear" name="btn" id="idbtn" />
				</td>
				
			</tr>
		</table>
	</form>
</logic:equal>

<logic:equal name="view" value="ok">
<p>${dataok}</p>
</logic:equal>

<logic:equal name="view" value="notok">
<p>${datanotok}</p>
</logic:equal>

<logic:equal name="view" value="reject">
	<table id="rounded-corner" style="width: 800px">
		<thead>
			<tr align="center">
				<th><b>Baris</b></th>
				<th><b>Penejelasan</b></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${rejectData}" var="rejectData">
				<tr>
					<td>${rejectData.row}</td>
					<td>${rejectData.desc}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</logic:equal>