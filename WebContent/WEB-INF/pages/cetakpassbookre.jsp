<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<html>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type='text/javascript' src='/ReportMCB/jsajax/CetakPassbook.js'></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cetak Statement</title>
</head>
<body>

<table border="0" align="center">
	<tr>
		<td>
			<fieldset style="width:400px;  border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			    <legend> <b>Cetak Ulang Passbook</b> </legend>
			    <form action="cetakPassbookAct.do" method="POST" name="lsp">
			    <table border="0">
			        <tr>
			            <td style="width: 120px;">No. Rekening</td>
			            <td>:</td>
			            <td><input type="text" name="acc" id="acc" maxlength="20" size="34" class="normalInput" onKeyup="checkDec(this);"><label style="color:red">*</label></td>			            
			        </tr>
			        <tr valign="top">
			            <td>Keterangan</td>
			            <td>:</td>
			            <td>
			            	<select name="ket" id="ket" style="width: 220px;" onchange="viewKet1()">
							  <option value="">Pilih</option>
							  <option value="1">Printer Rusak</option>
							  <option value="2">Sistem Offline</option>
							  <option value="3">User ID menggantung</option>
							  <option value="4">Lainnya</option>
							</select>
			            	<label style="color:red">*</label>
			            </td>
			        </tr>	
			        <tr valign="top">
			            <td></td>
			            <td></td>
			            <td><input type="text" name="ket1" id="ket1" size="34" value="" class="normalInput" disabled/>
			            </td>
			        </tr>		        
			        <tr>
			            <td>User Supervisor</td>
			            <td>:</td>
			            <td><input type="text" name="username" id="username" size="34" maxlength="20" value="" class="normalInput"/><label style="color:red">*</label></td>
			        </tr>
			        <tr>
			            <td>Password</td>
			            <td>:</td>
			            <td><input type="password" name="password" id="password" size="34" maxlength="20" value="" class="normalInput" /><label style="color:red">*</label></td>			            
			        </tr>
			        <tr>
			            <td colspan="2"> </td>
			            <td>
			                <input onclick="return valInput()" type="submit" value="Lihat" name="btn" id="reprint" />			                
			            </td>
				   </tr>
				   <logic:equal value="view" name="notif">				   
			        <tr>
			            <td colspan="3" style="vertical-align: text-top"><font style="color: red">${resp }</font> </td>			            
			        </tr>
				   </logic:equal>
			    </table>
			    </form>
			</fieldset>
		</td>
	</tr>
</table>


<logic:equal name="data" value="konfrm">
	<b>${teks}</b>
</logic:equal>

<logic:equal name="data" value="viewdata">
			<br />
			<br />
<table border="0" align="center" width="750px;">
	<tr>
		<td>
			<fieldset style="border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; height:730px;
			background-image:url('/ReportMCB/images/ctkpssbk/din.jpg'); background-repeat:no-repeat; 
			background-position: 50% 50%; ">
			<table border="0" style="padding:1px 1px 1px 1px; margin: 5px 10px 5px 10px; " width="730px;">
				<tbody>
					<tr >						
						<td width="130">Cabang</td>
						<td style="width: 5px;">:</td>
						<td width="160">${cust_acc.kdcab}</td>
						<td width="130">Nomor Rekening</td>
						<td style="width: 5px;">:</td>
						<td >${cust_acc.nomrek}</td>
					</tr>	
					<tr >						
						<td >Nama</td>
						<td >:</td>
						<td colspan="4">${cust_acc.rekname}</td>
					</tr>	
					<tr valign="baseline">						
						<td >Alamat</td>
						<td >:</td>
						<td colspan="4">${cust_acc.address}</td>
					</tr>	
					<tr >						
						<td >Nomor Passbook</td>
						<td >:</td>
						<td colspan="4"><b>${cust_acc.passbook_number}</b></td>
					</tr>	
					<tr valign="baseline">						
						<td >Keterangan</td>
						<td >:</td>
						<td colspan="4"><label class="lbl_ket" id="lbl_ket">${ket}</label></td>
					</tr>		
				</tbody>
			</table>
			<table border="0" style="padding:1px 1px 1px 1px; margin: 5px 10px 5px 10px; ">
				<thead>
					<tr style = "color: black; font-family:Times New Roman; font-size: 10pt;">
						<th style="width:13px; padding:1px 5px;"><b>No</b></th>
						<th style="width:25px; padding:1px 5px;"><b>Tanggal</b></th>
						<th style="width:20px; padding:1px 5px;"><b>Sandi</b></th>
						<th style="text-align: right; width:88px; padding:1px 5px;"><b>Mutasi</b></th>
						<th style="width:200px; padding:1px 5px;"><b>Keterangan</b></th>
						<th style="text-align: right; width:88px; padding:1px 5px;"><b>Saldo</b></th>
						<th style="width:70px; padding:1px 5px;"><b>Pengesah</b></th> 
						<th style="width:90px; padding:1px 5px;"><b>Mulai Cetak</b></th> 
					</tr>
				</thead>
				<tbody>
					<c:forEach var="lreprint" items="${lreprint}" >
					<tr style = "color: black; font-family:Times New Roman; font-size: 8pt;">						
						<td >${lreprint.no_s }</td>
						<td >${lreprint.trn_dt_s}</td>
						<td >${lreprint.trn_code}</td>
						<td style="text-align: right;">${lreprint.mutasi_s} &nbsp; ${lreprint.drcr_ind}</td>
						<td >${lreprint.detail}</td>
						<td style="text-align: right;">${lreprint.saldo_s}</td>
						<td >${lreprint.auth_id}</td>
						<td align="center">${lreprint.rbtn }</td>
					</tr>
					</c:forEach>					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="8"><button type='button' id='idbtnreprint' onclick='reprint()'>Cetak</button></td>
					</tr>
				</tfoot>
			</table>
		</fieldset>
		</td>
	</tr>
</table>   
</logic:equal> 
</body>
</html>