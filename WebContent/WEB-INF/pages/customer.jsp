<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.24.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link rel="stylesheet" href="/ReportMCB/css/report/table.css">

<html>
<head>
<meta charset="utf-8" />
<title>DETAIL REKENING MCB</title>

<link rel="stylesheet" href="/ReportMCB/css/report/jquery-ui.css">
<script type='text/javascript'
	src='/ReportMCB/scripts/smooth/jquery-1.9.1.js'></script>
<script type='text/javascript'
	src='/ReportMCB/scripts/smooth/jquery-ui.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/report/demos.css">

<script>
  $(function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      width:'auto',     
	  height:'auto',
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });
  });
 </script>

<style type="text/css">
body {
	font-family: times new roman;
}
.td1 {
	width: 150px;
}
.td2 {
	width: 5px;
}
</style>
</head>
<body>
<div style="float: center; text-align: center;">
<table width="100%" border="0">
	<tr >
		<td align="center">

<table id="rounded-corner"  border="0.5" align="center" style=" width : 1075px;">
	<thead>
			<tr align="left" style="width: 70px; height: 25px">
			<th style=" font-size: 12px;"><b>NO. REKENING</b></th>
			<th style=" font-size: 12px;"><b>NAMA</b></th>
			<th style="font-size: 12px;"><b>NO. CIF</b></th>
			<th style=" font-size: 12px;"><b>CURRENCY</b></th>
			<th style="font-size: 12px;"><b>JENIS REKENING</b></th>
			</tr>
	</thead>
	<tbody>
		<tr>
			<td>${DetailCustom.CUST_AC_NO}</td>
			<td>${DetailCustom.acDesc}</td>
			<td>${DetailCustom.custNo}</td>
			<td>${DetailCustom.ccy}</td>
			<td>${DetailCustom.accountClass}</td>
		</tr>
	</tbody>
 </table>	
 </td>
 </tr>
</table>

<table width="100%" border="0">
	<tr >
		<td align="center">
			<fieldset style="width:1075px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    	<legend> <b></b> </legend>

			<table width="1075px" border="0.5" id="rounded-corner"  >
				<tr>
					
					<th style="font-size: 20px;align="center"; colspan=7 ;"><b><h3><center>DETAIL REKENING</center></h3></b></th>
				</tr>
				<tr >
					<td >NO. REKENING</td>
					<td >:</td>
					<td >${DetailCustom.CUST_AC_NO}</td>
					<td >NAMA LENGKAP</td>
					<td >:</td>
					<td>${DetailCustom.acDesc}</td>
				</tr>
				<tr>
					<td >NO. CIF</td>
					<td >:</td>
					<td >${DetailCustom.custNo}</td>
					<td >JENIS IDENTITAS</td>
					<td>:</td>
					<td>${DetailCustom.p_nationalid}</td>
				</tr>
				<tr>
					<td >KODE PRODUK</td>
					<td >:</td>
					<td >${DetailCustom.accountClass}</td>
					<td >NOMOR IDENTITAS</td>
					<td >:</td>
					<td>${DetailCustom.UNIQUE_ID_VALUE}</td>
				</tr>
				<tr>
					<td >NAMA PRODUK</td>
					<td >:</td>
					<td >${DetailCustom.desc_acc}</td>
					<td >TGL LAHIR / TGL PENDIRIAN</td>
					<td >:</td>
					<td>${DetailCustom.DATE_OF_BIRTH}</td>
				</tr>
				<tr>
					<td >KODE VALUTA</td>
					<td >:</td>
					<td >${DetailCustom.ccy}</td>
					<td >IBU KANDUNG</td>
					<td>:</td>
					<td>${DetailCustom.ibu_kandung}</td>
				</tr>				
				<tr>
					<td >KODE CABANG</td>
					<td >:</td>
					<td >${DetailCustom.branch_code}</td>
					<td >ALAMAT</td>
					<td >:</td>
					<td> ${DetailCustom.address1}</td>
				</tr>
					<tr>
					<td >NAMA CABANG</td>
					<td >:</td>
					<td >${DetailCustom.branch_name}</td>
					<td ></td>
					<td ></td>
					<td >${DetailCustom.address2}</td>
				</tr>	
				<tr>
					<td >TGL BUKA REKENING</td>
					<td >:</td>
					<td >${DetailCustom.AC_OPEN_DATE}</td>
					<td ></td>
					<td ></td>
					<td >${DetailCustom.address3}</td>
				</tr>
				<tr>
					<td >RECORD STATUS</td>
					<td >:</td>
					<td >${DetailCustom.record_stat}</td>
					<td >NO. TELP</td>
					<td >:</td>
					<td >${DetailCustom.telephone}</td>
				</tr>
				<tr>
					<td >TGL TUTUP REKENING</td>
					<td >:</td>
					<td >${DetailCustom.AC_CLOSE_DATE}</td>
					<td >NO. HP</td>
					<td >:</td>
					<td >${DetailCustom.mobile_number}</td>
				</tr>
				<tr>
					<td >ALTERNATE ACCOUNT</td>
					<td >:</td>
					<td >${DetailCustom.alt_ac_no}</td>
					<td >EMAIL</td>
					<td >:</td>
					<td >${DetailCustom.email}</td>
				</tr>
				<tr>
					<td >KODE MARKETING</td>
					<td >:</td>
					<td >${DetailCustom.kode_marketing}</td>
					<td></td>
					<td ></td>
					<td></td>
				</tr>
				<tr>
					<td >SALDO</td>
					<td ></td>
					<td ></td>
					<td ></td>
					<td ></td>
					<td></td>
				</tr>
	
			<tr>
				<td>SALDO MEMO</td>
				<td>:</td>
				<td style="text-align: left;"><fmt:formatNumber type="number"
					maxFractionDigits="2" value="${DetailCustom.acyCurrBalance}" /></td>
				<td><button id="opener">DETAIL NASABAH</button></td>
				<td></td>
				<td colspan="2"></td>

			</tr>


			<tr>
				<td>SALDO HOLD</td>
				<td>:</td>
				<td style="text-align: left;"><fmt:formatNumber type="number"
					maxFractionDigits="3" value="${DetailCustom.acyBlockedAmount}" /></td>
				<td></td>
				<td></td>
				<td colspan="2"></td>
			</tr>


			<tr>
				<td>SALDO MINIMUM</td>
				<td>:</td>
				<td style="text-align: left;"><fmt:formatNumber type="number"
					maxFractionDigits="3" value="${DetailCustom.minBalance}" /></td>
				<td></td>
				<td></td>
				<td colspan="2"></td>
			</tr>



			<tr>
				<td>SALDO EFEKTIF</td>
				<td>:</td>
				<td style="text-align: left;"><fmt:formatNumber type="number"
					maxFractionDigits="3" value="${DetailCustom.acyAvlBal}" /></td>
				<td></td>
				<td></td>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td>SALDO RATA-RATA</td>
				<td>:</td>
				<td style="text-align: left;"><fmt:formatNumber type="number"
					maxFractionDigits="3" value="${DetailCustom.acy_bal}"/></td>
				<td></td>
				<td></td>
				<td colspan="2"></td>
			</tr>
		</table>
			</fieldset>
		</td>
	</tr>
</table>
</div>
<div id="dialog" title="DETAIL NASABAH">

<div style="float: left; text-align: left;">
<fieldset style="width:80%";><legend> <b>DETAIL
NASABAH</b> </legend>

<div style="font-size: 12px; font-family: times new roman; float: left;">
<table id="rounded-corner" style="width:100%" align="center">

	<tr></tr>
	<tr>

		<td>NO. CIF</td>
		<td>:</td>

		<td>${DetailCustom.custNo}</td>

		<td></td>

		<td>NATIONALITY</td>
		<td>:</td>
		<td>${DetailCustom.nationality}</td>
	</tr>

	<tr>
		<td>KODE CABANG</td>
		<td>:</td>

		<td>${DetailCustom.branch_code}</td>

		<td></td>

		<td>RESIDENT STATUS</td>
		<td>:</td> 
		
		<td>${DetailCustom.resident_status}</td>
		
	</tr>


	<tr>
		<td>NAMA CABANG</td>
		<td>:</td>

		<td>${DetailCustom.branch_name}</td>

		<td></td>

		<td>JENIS KELAMIN</td>
		<td>:</td>

		<td>${DetailCustom.jenis_kelamin}</td>
	</tr>

	<tr>
		<td>TGL BUKA CIF</td>
		<td>:</td>

		<td>${DetailCustom.CIF_CREATION_DATE}</td>

		<td></td>

		<td>AGAMA</td>
		<td>:</td>

		<td>${DetailCustom.agama}</td>


	</tr>
	<tr>
		<td>CUSTOMER TYPE</td>
		<td>:</td>
		<td>${DetailCustom.customer_type}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		
	</tr>
	<tr>
		<td>CUSTOMER CATEGORY</td>
		<td>:</td>
		<td>${DetailCustom.CUSTOMER_CATEGORY}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		
	</tr>
	
	<tr>
		<td>NAMA LENGKAP</td>
		<td>:</td>
		<td>${DetailCustom.acDesc}</td>
		<td></td>
		<td>TGL KADALUARSA PASSPORT</td>
		<td>:</td>
		<td>-</td>
	</tr>

	<tr>
		<td>JENIS IDENTITAS</td>
		<td>:</td>
		<td>${DetailCustom.p_nationalid}</td>
		<td></td>
		<td>PEKERJAAN</td>
		<td>:</td>
		<td>${DetailCustom.pekerjaan}</td>
	</tr>
	
	<tr>
		<td>NOMOR IDENTITAS</td>
		<td>:</td>
		<td>${DetailCustom.UNIQUE_ID_VALUE}</td>
		<td></td>
		<td>BIDANG PEKERJAAN</td>
		<td>:</td>
		<td>${DetailCustom.bidang_pekerjaan}</td>

	</tr>
	<tr>
		<td>TGL KADALUARSA ID</td>
		<td>:</td>
		<td>${DetailCustom.tgl_kadaluarsa_id}</td>
		<td></td>
		<td>STATUS PERKAWINAN</td>
		<td>:</td>
		<td>${DetailCustom.sts_perkawinan}</td>
	</tr>
	<tr>
		<td>TGL LAHIR / TGL PENDIRIAN</td>
		<td>:</td>
		<td>${DetailCustom.DATE_OF_BIRTH}</td>
		<td></td>
		<td>PENDIDIKAN</td>
		<td>:</td>
		<td>${DetailCustom.pendidikan}</td>
	</tr>
	
		<tr>
	
		<td>IBU KANDUNG</td>
		<td>:</td>
		<td>${DetailCustom.ibu_kandung}</td>
		<td></td>
		<td>PENGHASILAN TETAP /BULAN</td>
		<td>:</td>
		<td>${DetailCustom.penghsl_ttp}</td>

	</tr>
	
		<tr>
	
		<td>ALAMAT</td>
		<td>:</td>
		<td>${DetailCustom.address1}</td>
			 
			 
		<td></td>
		<td>PENGHASILAN TDK TETAP /BULAN</td>
		<td>:</td>
		<td>${DetailCustom.penghsl_tdk_ttp}</td>

	</tr>
		<tr>
	
		<td></td>
		<td></td>
		<td>${DetailCustom.address2}</td>
		<td></td>
		<td>PENGELUARAN TETAP /BULAN</td>
		<td>:</td>
		<td>${DetailCustom.pengeluaran_ttp}</td>

	</tr>
		<tr>
	
		<td></td>
		<td></td>
		<td>${DetailCustom.address3}</td>
		<td></td>
		<td>RATA2 PENGHASILAN TDK TETAP /BULAN</td>
		<td>:</td>
		<td>${DetailCustom.rtrt_penghsl_tdk_ttp}</td>

	</tr>
		<tr>
	
		<td>NO. TELP</td>
		<td>:</td>
		<td>${DetailCustom.telephone}</td>
		<td></td>
		<td>INFO BGMN MEMPEROLEH HASIL TAMBAHAN</td>
		<td>:</td>
		<td>${DetailCustom.info_mmprolh_hsl}</td>

	</tr>
	
	<tr>
		
		<td>EMAIL</td>
		<td>:</td>
		<td>${DetailCustom.email}</td>
		<td></td>
		<td>SUMBER DANA</td>
		<td>:</td>
		<td>${DetailCustom.sumber_dana}</td>

	</tr>
	
	<tr>
		<td>FAX</td>
		<td>:</td>
		<td>${DetailCustom.fax}</td>
		<td></td>
		<td>TUJUAN BUKA REKENING</td>
		<td>:</td>
		<td>${DetailCustom.tujuan_buka_rek}</td>
		
	</tr>
	
	<tr>
		
		<td>NO. HP</td>
		<td>:</td>
		<td>${DetailCustom.mobile_number}</td>
		
	</tr>
	
</table>
</div>
</fieldset>
</div>
</body>
</html>