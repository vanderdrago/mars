<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/toolTipText.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
$(function() {
    $( "#tgl" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        yearRange: "1945:2016",
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
});

function isNumber(evt){
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Simulasi Angsuran KPR 5 Tahun</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Simulasi Angsuran KPR 5 Tahun</b></legend>
					<form action="simulasiKPRactMarketing.do" name="jurnal" method="POST">
						<table border="0">
							<tr>
								<td>
									<input type="text" name="nama" id="nama" class="inputs" placeholder="Nama Nasabah" title="Isi Nama Nasabah" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="plafond" id="plafond" class="inputs" onkeypress="return isNumber(event)" placeholder="Plafond Pembiayaan" title="Isi Plafond Pembiayaan" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="tgl" id="tgl" class="inputs" placeholder="Tanggal Simulasi" title="Tanggal Simulasi" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td align="left"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
	</table>
</body>
</html>