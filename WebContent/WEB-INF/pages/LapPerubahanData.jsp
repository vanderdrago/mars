<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
// $(function() {
//     $( "#tgl" ).datepicker({
//         showOn: "button",
//         buttonImage: "images/calendar.gif",
//         buttonImageOnly: true,
//         yearRange: "1945:2016",
//         dateFormat: 'dd-mm-yy',
//         changeYear: true
//     });
// });

// function checkDec(el){
//     var ex = /^[0-9]+\.?[0-9]*$/;
//     if(ex.test(el.value)==false){
//         el.value = el.value.substring(0, el.value.length - 1);
//     }
// }

function valdata() {
	var norek;
	norek = document.getElementById('acc').value;
	if (acc == ""){
		alert('Silahkan isi No Rekening atau Nama terlebih dahulu');
		return false;
		}
	return true;
	};

</script>

<table border="0" width="80%">
  <tr>
      <td align="center" colspan="2">
      <fieldset style="color:black;width:320px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
		  <legend><b>LAPORAN PERUBAHAN DATA NO REKENING</b></legend>
		  <form action="LapPerubahanDataAct.do" method="POST" name="jurnal">
		  <table>
		  	   <tr>
		  	   	   <td><input type="text" name="acc" id="acc" class="inputs" placeholder="No Rekening" style=" width : 300px;"/>
		  	   </tr>
			   <tr>
				  <td><input type="text" name="branch" id="branch" class="inputs" placeholder="Cabang" style=" width : 300px;"/>
			  </tr>
			  <tr align="right">
				  <td><a href="#" onclick="document.forms['jurnal'].submit(); return false;" class="blue btn">Search</a> 
			  </tr>
		  </table>
		  </form>
	   </fieldset>
       </td>
  </tr>
  <tr style="color: black">
	  <td align="center">
	  <table border="0" width="1075px">
	  	 <tr>
	  	 	<td>
	  			<table border="0">
	  				<tr>
	  					<td>
	  						<c:if test="${sawalNorek >= 1}">Page ${sawalNorek} Of ${noOfPagesNorek}</c:if>
	  					</td>
	  					<td align="left">
							<c:if test="${sawalNorek >= 1}"><a href="manageLphNorekFwd.do?page=1" class="page">First</a></c:if>
						</td>
	  					<td>
							<c:if test="${currentPageNorek != 1 && (currentPageNorek - 1 > 0)}">
							<a href="manageLphNorekFwd.do?page=${currentPageNorek - 1}" class="page">Previous</a>
							</c:if>
				    	</td>
						<td>
							<table>
								<tr>
				 	  				<td><c:if test="${sawalNorek >= 1}">
				 	  	  	    	<c:forEach begin="${sawalNorek}" end="${sakhirNorek}" var="i">
				 	  	  	  		<c:choose><td><a href="manageLphNorekFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
				 	  	  	   	    </c:forEach></c:if></td>
		  						</tr>
							</table>
						</td>
						<td> 
		   		  			<c:if test="${currentPageNorek lt noOfPagesNorek}">
		         			<a href="manageLphNorekFwd.do?page=${currentPageNorek + 1}" class="page">Next</a>
				  			</c:if>
		   	  			</td>
	  					<td>
		    	  	  		<c:if test="${noOfPagesNorek - 9 > 0}">
		       				<a href="manageLphNorekFwd.do?page=${currentPageNorek - 9}"class="page">Last</a>
		       		  		</c:if>
		   	  			</td>
		   	  		</tr>
		   	  	</table>
		   	  	</td>
		   	  	<td align="right">
			  		<c:if test="${sawalNorek >= 1}">Search Result - ${noOfRecordsNorek} Result</c:if>
			  	</td>
	  		</tr>
	  		<tr>
	  			<td align="center" colspan="2"> 
	  			<div style="font-size:13px; font-family:times new roman;">
	  				<table id="rounded-corner" style="width:1075px"> 
	  					<thead>
						<tr align="center">
							<th style="width:1px; font-size: 12px;"><b>NO.</b></th>
							<th style="width:50px; font-size: 12px;"><b>BRANCH </b></th>
							<th style="width:125px; font-size: 12px;"><b>CUST ACC NO</b></th>
							<th style="width:150px; font-size: 12px;"><b>NAMA NASABAH</b></th>
							<th style="width:75px; font-size: 12px;"><b>FASTPATH</b></th>
							<th style="width:150px; font-size: 12px;"><b>DATA LAMA</b></th>
							<th style="width:150px; font-size: 12px;"><b>DATA BARU</b></th>
							<th style="width:25px; font-size: 12px;"><b>MODIF KE</b></th>
							<th style="width:200px; font-size: 12px;"><b>KETERANGAN</b></th>
							<th style="width:100px; font-size: 12px;"><b>MAKER DATE</b></th>
							<th style="width:100px; font-size: 12px;"><b>MAKER ID</b></th>
							<th style="width:100px; font-size: 12px;"><b>CHECKER ID</b></th>
						</tr>
					</thead>
					<tbody>
						<c:set var="inc" value="0" />
						<c:forEach var="lphRek" items="${lphRek}" >
						<tr style="font-size: 11px;">
							<c:set var="inc" value="${inc + 1}" />
							<td>${inc}</td>
							<td>${lphRek.branch}</td>
							<td>${lphRek.no_rek}</td>
							<td>${lphRek.nama_nasabah}</td>
							<td>${lphRek.fastpath}</td>
							<td>${lphRek.data_lama}</td>
							<td>${lphRek.data_baru}</td>
							<td>${lphRek.modif_ke}</td>
							<td>${lphRek.keterangan}</td>
							<td>${lphRek.maker_date}</td>
							<td>${lphRek.maker_id}</td>
							<td>${lphRek.checker_id}</td>
						</tr>
						</c:forEach>
					</tbody>
	  				</table>
	  			</div>	
	  			</td>
	  		</tr>
	  </table>
	</td>
  </tr>
</table>
