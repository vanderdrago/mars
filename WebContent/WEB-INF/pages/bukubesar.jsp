<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript'
	src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css" />
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet"
	type="text/css" />
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet"
	type="text/css" />
<link href="/ReportMCB/scripts/text.css" rel="stylesheet"
	type="text/css" />

<script type="text/javascript">
	$(function() {
		$("#tgl1").datepicker({
			showOn : "button",
			buttonImage : "images/calendar.gif",
			buttonImageOnly : true,
			dateFormat : 'dd-mm-yy'
		});
		$("#tgl2").datepicker({
			showOn : "button",
			buttonImage : "images/calendar.gif",
			buttonImageOnly : true,
			dateFormat : 'dd-mm-yy'
		});
		$("#tgl3").datepicker({
			showOn : "button",
			buttonImage : "images/calendar.gif",
			buttonImageOnly : true,
			dateFormat : 'ddmmyy'
		});
		$("#tgl").datepicker({
			showOn : "button",
			buttonImage : "images/calendar.gif",
			buttonImageOnly : true,
			dateFormat : 'dd-mm-yy'
		});
	});

	function validateInput() {
		var b = document.getElementById("report");
		var rpt = b.options[b.selectedIndex].value;
		if (rpt == "") {
			alert('Silahkan pilih report');
			return false;
		}
		return true;
	}

	function validateACD() {
		var tgl1;
		var tgl2;
		var acc;
		var brnch;
		var val;
		tgl1 = document.getElementById('tgl1').value;
		tgl2 = document.getElementById('tgl2').value;
		acc = document.getElementById('acount').value;
		brnch = document.getElementById('kdcab').value;
		val = document.getElementById('val').value;

		if (tgl1 == "") {
			alert('Silahkan isi tanggal awal');
			return false;
		}
		if (tgl2 == "") {
			alert('Silahkan isi tanggal akhir');
			return false;
		}
		if (val == "") {
			alert('Kode Valuta belum diisi');
			return false;
		}
		if (brnch == "") {
			alert('Kode Cabang belum diisi');
			return false;
		}
		return true;
	}

	function validatecrtrns() {
		var trnRefNo;
		trnRefNo = document.getElementById('trnRefNo').value;

		if (trnRefNo == "") {
			alert('Silahkan isi TRN REF NO');
			return false;
		}
		return true;
	}

	function checkDec(el) {
		var ex = /^[0-9]+\.?[0-9]*$/;
		if (ex.test(el.value) == false) {
			el.value = el.value.substring(0, el.value.length - 1);
		}
	}

	function validateLsp() {
		var ssl_a;
		var ssl_b;
		var cab;
		var val;
		ssl_a = document.getElementById('ssl_a').value;
		ssl_b = document.getElementById('ssl_b').value;
		cab = document.getElementById('cab').value;
		val = document.getElementById('val').value;

		if (ssl_a == "") {
			alert('Silahkan isi Mulai SSL');
			return false;
		}
		if (ssl_b == "") {
			alert('Silahkan isi Hingga SSL');
			return false;
		}
		if (cab == "") {
			alert('Kode Cabang belum diisi');
			return false;
		}
		if (val == "") {
			alert('Kode Valuta belum diisi');
			return false;
		}
		return true;
	}

	function validateACD2() {
		var tgl1;
		//var tgl2;
		var user;
		var brnch;
		var batch;
		tgl1 = document.getElementById('tgl1').value;
		//tgl2 = document.getElementById('tgl2').value;
		//user = document.getElementById('user').value;
	 
	        var today = new Date();
	        var dd = today.getDate();
	        var mm = today.getMonth(); //January is 0!

	        var yyyy = today.getFullYear();
	        if (dd < 10) {
	          dd = '0' + dd;
	        } 
	        if (mm < 10) {
	          mm = '0' + mm;
	        } 
	        var formattoday = yyyy + '/' + mm + '/' + dd;
	        
	       
	        var res = tgl1.split("-");
	        
	        var tgl2=res[2]+"/"+res[1]+"/"+res[0];
	 	     var b = new Date(tgl2);
	        
	       

	        var d = new Date(); // today date
	        d.setMonth(d.getMonth() - 1);  //subtract 6 month from current date 

	   /*    if (b < d)  {
	            alert("Tanggal input maksimal satu bulan lalu");
	            return false;
	        }
	 
	        if (tgl1 == "") {
			alert('Silahkan isi tanggal awal');
			return false;
		}*/
		
		return true;
	}

	function validateACD3() {
		var tgl1;
		var tgl2;
		var kdcab;
		var user;
		tgl1 = document.getElementById('tgl1').value;
		tgl2 = document.getElementById('tgl2').value;
		kdcab = document.getElementById('kdcab').value;
		user = document.getElementById('user').value;

		if (tgl1 == "") {
			alert('Silahkan isi tanggal awal');
			return false;
		}
		if (tgl2 == "") {
			alert('Silahkan isi tanggal akhir');
			return false;
		}
		if (kdcab == "") {
			alert('Kode Cabang belum diisi');
			return false;
		}

// 		if (user == "") {
// 			alert('User ID belum diisi');
// 			return false;
// 		}
		return true;
	}

	//penambahan allcab-allval BukuBesar, Agustus 2016
	function check(f) {
		if (f.checked) {
			document.getElementById('kdcab').value = 'Semua';
		} else {
			document.getElementById('kdcab').value = '';
		}
	}

	function checkval(f) {
		if (f.checked) {
			document.getElementById('val').value = 'Semua';
		} else {
			document.getElementById('val').value = '';
		}
	}

	//penambahan BukuBesarGL, Agustus 2016
	function validateACD4() {
		var tgl1;
		var tgl2;
		var acc, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, acc10;
		tgl1 = document.getElementById('tgl1').value;
		tgl2 = document.getElementById('tgl2').value;
		acc = document.getElementById('acount').value;
		acc2 = document.getElementById('acount2').value;
		acc3 = document.getElementById('acount3').value;
		acc4 = document.getElementById('acount4').value;
		acc5 = document.getElementById('acount5').value;
		acc6 = document.getElementById('acount6').value;
		acc7 = document.getElementById('acount7').value;
		acc8 = document.getElementById('acount8').value;
		acc9 = document.getElementById('acount9').value;
		acc10 = document.getElementById('acount10').value;

		if (tgl1 == "") {
			alert('Silahkan isi tanggal awal');
			return false;
		}
		if (tgl2 == "") {
			alert('Silahkan isi tanggal akhir');
			return false;
		}
		if (acc == "") {
			alert('Mohon isi minimal 2 nomor GL');
			return false;
		}
		if (acc6 == "") {
			alert('Mohon isi minimal 2 nomor GL');
			return false;
		}
		/*if(acc = "")
		{	document.getElementById('acount').value = '0'; }
		else */if (acc2 = "") {
			document.getElementById('acount2').value = '0';
		} else if (acc3 == "") {
			document.getElementById('acount3').value = '0';
		} else if (acc4 == "") {
			document.getElementById('acount4').value = '0';
		} else if (acc5 == "") {
			document.getElementById('acount5').value = '0';
		}
		/*else if(acc6 == "")
		{	document.getElementById('acount6').value = '0'; }
		 */else if (acc7 == "") {
			document.getElementById('acount7').value = '0';
		} else if (acc8 == "") {
			document.getElementById('acount8').value = '0';
		} else if (acc9 == "") {
			document.getElementById('acount9').value = '0';
		} else if (acc10 == "") {
			document.getElementById('acount10').value = '0';
		}
		return true;
	}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



	<logic:equal name="viewrpt" value="R001">
		<fieldset
			style="color: black; width: 620px; border: 0px solid #999; border-radius: 8px; box-shadow: 0 0 20px #999;">
			<legend align="center">
				<b>BUKU BESAR</b>
			</legend>
			<form action="viewjurnal.do" method="POST" name="jurnal">
				<table border="0">
					<!--         <tr> -->
					<!--             <td colspan="4">LAPORAN BUKU BESAR</td> -->
					<!--         </tr> -->
					<!--         <tr> -->
					<!--             <td>Mulai Tanggal</td> -->
					<!--             <td>:</td> -->
					<!--             <td colspan="2"><input type="text" name="tgl1" id="tgl1" readonly="true"><label style="color:red">*</label>&nbsp;dd-MM-yyyy</td> -->
					<!--         </tr> -->
					<tr>
						<td><input type="text" name="tgl1" id="tgl1" class="inputs"
							placeholder="Mulai Tanggal (dd-mm-YYYY)" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="tgl2" id="tgl2" class="inputs"
							placeholder="Hingga Tanggal (dd-mm-YYYY)" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="acount" id="acount"
							class="inputs" placeholder="No. GL" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="kdcab" id="kdcab" class="inputs"
							placeholder="Kode Cabang" style="width: 400px;maxlength="3"><label
							style="color: red">*</label></td>
						<td><input type="checkbox" name="allcab" id="allcab"
							value="1" onclick="check(this.form.allcab)"> Semua Kode
							Cabang</td>
					</tr>
					<tr>
						<td><input type="text" name="val" id="val" class="inputs"
							placeholder="Kode Valuta" style="width: 400px;maxlength="3"><label
							style="color: red">*</label></td>
						<td><input type="checkbox" name="allval" id="allval"
							value="1" onclick="check(this.form.allcab)"> Semua Kode
							Valuta</td>
					</tr>
					<!--         <tr> -->
					<!--             <td>Hingga Tanggal</td> -->
					<!--             <td>:</td> -->
					<!--             <td colspan="2"><input type="text" name="tgl2" id="tgl2" readonly="true" ><label style="color:red">*</label>&nbsp;dd-MM-yyyy</td> -->
					<!--         </tr> -->
					<!--         <tr> -->
					<!--             <td>No. GL</td> -->
					<!--             <td>:</td> -->
					<!--             <td colspan="2"><input type="text" name="acount" id="acount" maxlength="9" onKeyup="checkDec(this);" ><label style="color:red">*</label></td> -->
					<!--         </tr> -->
					<!--         <tr> -->
					<!--             <td>Kode Cabang</td> -->
					<!--             <td>:</td> -->
					<!--             <td><input type="text" name="kdcab" id="kdcab" maxlength="3" onKeyup="checkDec(this);"><label style="color:red">*</label></td> -->
					<!--             <td><input type="checkbox" name="allcab" id="allcab" value="1" onclick="check(this.form.allcab)"> Semua Kode Cabang</td> -->
					<!--         </tr> -->
					<!--         <tr> -->
					<!--             <td>Kode Valuta</td> -->
					<!--             <td>:</td> -->
					<!--             <td><input type="text" name="val" id="val" maxlength="3" ><label style="color:red">*</label></td> -->
					<!--             <td><input type="checkbox" name="allval" id="allval" value="2" onclick="checkval(this.form.allval)"> Semua Kode Valuta</td> -->
					<!--         </tr> -->
					<tr>
						<td colspan="3"><input type="checkbox" name="cbsrekoto"
							id="idcbsrekoto" value="1"> Tampilkan RAK Otomatis <br /></td>
					</tr>
					<tr>
						<!--             <td colspan="1"> </td> -->
						<!--             <td align="center" colspan="3"> -->
						<td align="center" colspan="3"><input
							onclick="return validateACD()" type="submit" value="Ok"
							name="btn" id="ok" class="blue btn" /> <input
							onclick="return validateACD()" type="submit" value="Download"
							name="btn" id="download" class="blue btn" /> <!--             		<a href="#" onclick="document.forms['jurnal'].submit(); return validateACD();" class="blue btn" value="Ok" name="btn" id="ok">Ok</a> -->
							<!--             		<a href="#" onclick="document.forms['jurnal'].submit(); return validateACD();" class="blue btn" value="Download" name="btn" id="download">Download</a>			 -->
							<!--                 <input onclick="return validateACD()" type="submit" value="Ok" name="btn" id="ok" /> -->
							<!--                 <input onclick="return validateACD()" type="submit" value="Download" name="btn" id="download" /> -->
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
	</logic:equal>


	<logic:equal name="viewrpt" value="R002">
		<fieldset
			style="color: black; width: 450px; border: 0px solid #999; border-radius: 8px; box-shadow: 0 0 20px #999;">
			<legend>
				<b>Today</b>
			</legend>
			<form action="viewbmijurnal.do" method="POST" name="jurnal">
				<table>
					<tr>
						<td colspan="2"></td>
						<td><input onclick="return validateACD()" type="submit"
							value="View" name="ok" id="ok" /></td>
					</tr>
				</table>
			</form>
		</fieldset>
	</logic:equal>
	<logic:equal name="viewrpt" value="R005">
		<fieldset
			style="color: black; width: 460px; border: 0px solid #999; border-radius: 8px; box-shadow: 0 0 20px #999;">
			<legend align="center">
				<b>History</b>
			</legend>
			<form action="viewhbmijurnal.do" method="POST" name="jurnal">
				<table border="0">
					<tr>
					<td><input type="text" name="tgl1" id="tgl1" class="inputs"
							placeholder="Tanggal" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<!--  <tr>
						<td><input type="text" name="tgl2" id="tgl2" class="inputs"
							placeholder="Hingga Tanggal" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="user" id="user" class="inputs"
							placeholder="User ID" maxlength="20" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>-->
					<tr>
						<td><input type="text" name="kdcab" id="kdcab" class="inputs"
							placeholder="Kode Cabang" maxlength="3" style="width: 400px;"></td>
					</tr>
					<!-- <tr>
						<td><input type="text" name="batch" id="batch" class="inputs"
							placeholder="Batch" maxlength="5" style="width: 400px;"></td>
					</tr> -->
					<tr>
						<td align="center"><input onclick="return validateACD2()"
							type="submit" value="Ok" name="btn" id="ok" class="blue btn" /></td>
					</tr>
					<!-- 		<tr> -->
					<!-- 			<td>Mulai Tanggal</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><label -->
					<!-- 				style="color: red"><input type="text" name="tgl1" id="tgl1" readonly="true">*</label></td> -->
					<!-- 		</tr> -->
					<!-- 		<tr> -->
					<!-- 			<td>Hingga Tanggal</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="tgl2" id="tgl2" readonly="true"><label -->
					<!-- 				style="color: red">*</label></td> -->
					<!-- 		</tr> -->
					<!-- 		<tr> -->
					<!-- 			<td>User ID</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="user" id="user" maxlength="20"><label -->
					<!-- 				style="color: red">*</label></td> -->
					<!-- 		</tr> -->
					<!-- 		<tr> -->
					<!-- 			<td>Kode Cabang</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="kdcab" id="kdcab" maxlength="3" -->
					<!-- 				onKeyup="checkDec(this);"><label style="color: red"></label></td> -->
					<!-- 		</tr> -->
					<!-- 		<tr> -->
					<!-- 			<td>Batch</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="batch" id="batch" maxlength="5"><label -->
					<!-- 				style="color: red"></label></td> -->
					<!-- 		</tr> -->
					<!-- 		<tr> -->
					<!-- 			<td colspan="2"></td> -->
					<!-- 			<td><input onclick="return validateACD2()" type="submit" -->
					<!-- 				value="Ok" name="ok" id="ok" /></td> -->
					<!-- 		</tr> -->
				</table>
			</form>
		</fieldset>
	</logic:equal>

	<logic:equal name="viewrpt" value="R006">
		<fieldset
			style="color: black; width: 470px; border: 0px solid #999; border-radius: 8px; box-shadow: 0 0 20px #999;">
			<legend align="center">
				<b>Transaksi</b>
			</legend>
			<form action="viewthbmijurnal.do" method="POST" name="jurnal">
				<table border="0">
					<tr>
						<td><input type="text" name="tgl1" id="tgl1" class="inputs"
							placeholder="Mulai Tanggal" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="tgl2" id="tgl2" class="inputs"
							placeholder="Hingga Tanggal" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="kdcab" id="kdcab" class="inputs"
							placeholder="Kode Cabang" maxlength="3"
							onKeyup="checkDec(this); " style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="user" id="user" class="inputs"
							placeholder="User ID" maxlength="20" style="width: 400px;"></td>
					</tr>
					<tr>
						<td><input type="text" name="mod" id="mod" class="inputs"
							placeholder="Module" maxlength="3" style="width: 400px;"></td>
					</tr>
					<tr>
						<!-- 			<td align="left"><input onclick="return validateACD3()" type="submit" value="Ok" name="btn" id="ok" class="blue btn" /></td> -->
						<td align="left"><a href="#"
							onclick="document.forms['jurnal'].submit(); return validateACD3();"
							class="blue btn">Ok</a>
					</tr>
					<!-- 		<tr> -->
					<!-- 			<td>Mulai Tanggal</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="tgl1" id="tgl1" readonly="true"><label -->
					<!-- 				style="color: red">*</label></td> -->
					<!-- 		</tr> -->
					<!-- 		<tr> -->
					<!-- 			<td>Hingga Tanggal</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="tgl2" id="tgl2" readonly="true"><label -->
					<!-- 				style="color: red">*</label></td> -->
					<!-- 		</tr> -->
					<!-- 		<tr> -->
					<!-- 			<td>Kode Cabang</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="kdcab" id="kdcab" maxlength="3" -->
					<!-- 				onKeyup="checkDec(this);"><label style="color: red">*</label></td> -->
					<!-- 		</tr> -->

					<!-- 		<tr> -->
					<!-- 			<td>User ID</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="user" id="user" maxlength="20" ><label style="color: red">*</label><label -->
					<!-- 				style="color: red"></label></td> -->
					<!-- 		</tr>	 -->
					<!-- 			<tr> -->
					<!-- 			<td>Module</td> -->
					<!-- 			<td>:</td> -->
					<!-- 			<td><input type="text" name="mod" id="mod" maxlength="3" -->
					<!-- 				onKeyup="checkDec(this);"><label style="color: red"></label></td> -->
					<!-- 		</tr> -->


					<!-- 		<tr> -->
					<!-- 			<td colspan="2"></td> -->
					<!-- 			<td><input onclick="return validateACD3()" type="submit" -->
					<!-- 				value="Ok" name="ok" id="ok" /></td> -->
					<!-- 		</tr> -->
				</table>
			</form>
		</fieldset>
	</logic:equal>


	<logic:equal name="viewrpt" value="R003">
		<fieldset
			style="color: black; width: 470px; border: 0px solid #999; border-radius: 8px; box-shadow: 0 0 20px #999;">
			<legend align="center">
				<b>Cari Transaksi</b>
			</legend>
			<form action="searchjurnal.do" method="POST" name="jurnal">
				<table border="0">
					<tr>
						<td colspan="3" align="center">LAPORAN BUKU BESAR</td>
					</tr>
					<tr>
						<td><input type="text" name="tgl" id="tgl" class="inputs"
							placeholder="Tanggal" style="width: 400px;"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td><input type="text" name="trnRefNo" id="trnRefNo" trnRefNo="mod"
							class="inputs" placeholder="TRN REF NO" maxlength="16"
							style="width: 400px;"></td>
					</tr>
					
					<tr>
						<td><input type="hidden" name="role" id="role" 
							class="inputs" value=${role_string} ></td>
					</tr>
					
					
					<tr>
						
						<td align="center"><input onclick="return validatecrtrns()"
							type="submit" value="Ok" name="btn" id="ok" class="blue btn" />
						<!--<td align="left"> <a href="#"
							onclick="document.forms['jurnal'].submit(); return validatecrtrns();"
							class="blue btn">Ok</a> -->
							
						
					</tr>
					<!--         <tr> -->
					<!--             <td>Tanggal</td> -->
					<!--             <td>:</td> -->
					<!--             <td><input type="text" name="tgl" id="tgl" readonly="true" >&nbsp;dd-MM-yyyy</td> -->
					<!--         </tr> -->
					<!--         <tr> -->
					<!--             <td>TRN REF NO</td> -->
					<!--             <td>:</td> -->
					<!--             <td><input type="text" name="trnRefNo" id="trnRefNo" maxlength="16" ><label style="color:red">*</label></td> -->
					<!--         </tr> -->
					<!--         <tr> -->
					<!--             <td colspan="1"> </td> -->
					<!--             <td> -->
					<!--                 <input onclick="return validatecrtrns()" type="submit" value="Ok" name="ok" id="ok" /> -->
					<!--             </td> -->
					<!-- 	   </tr> -->
				</table>
			</form>
		</fieldset>
	</logic:equal>

	<logic:equal name="viewrpt" value="R007">
		<fieldset
			style="color: black; width: 500px; border: 0px solid #999; border-radius: 8px; box-shadow: 0 0 20px #999;">
			<legend>
				<b>BUKU BESAR GL</b>
			</legend>
			<form action="viewjurnalGL.do" method="POST" name="jurnal">
				<table>
					<tr>
						<td colspan="4">LAPORAN BUKU BESAR GL</td>
					</tr>
					<tr>
						<td>Mulai Tanggal</td>
						<td>:</td>
						<td colspan="2"><input type="text" name="tgl1" id="tgl1"
							readonly="true"></td>
						<td><label style="color: red">*</label>&nbsp;dd-MM-yyyy</td>
					</tr>
					<tr>
						<td>Hingga Tanggal</td>
						<td>:</td>
						<td colspan="2"><input type="text" name="tgl2" id="tgl2"
							readonly="true"></td>
						<td><label style="color: red">*</label>&nbsp;dd-MM-yyyy</td>
					</tr>
					<tr>
						<td>No. GL</td>
						<td>:</td>
						<td colspan="1"><input type="text" name="acount" id="acount"
							maxlength="9" onKeyup="checkDec(this);"><label
							style="color: red">*</label></td>
						<td colspan="2"><input type="text" name="acount6"
							id="acount6" maxlength="9" onKeyup="checkDec(this);"><label
							style="color: red">*</label></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td colspan="1"><input type="text" name="acount2"
							id="acount2" maxlength="9" onKeyup="checkDec(this);"></td>
						<td colspan="2"><input type="text" name="acount7"
							id="acount7" maxlength="9" onKeyup="checkDec(this);"></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td colspan="1"><input type="text" name="acount3"
							id="acount3" maxlength="9" onKeyup="checkDec(this);"></td>
						<td colspan="2"><input type="text" name="acount8"
							id="acount8" maxlength="9" onKeyup="checkDec(this);"></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td colspan="1"><input type="text" name="acount4"
							id="acount4" maxlength="9" onKeyup="checkDec(this);"></td>
						<td colspan="2"><input type="text" name="acount9"
							id="acount9" maxlength="9" onKeyup="checkDec(this);"></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td colspan="1"><input type="text" name="acount5"
							id="acount5" maxlength="9" onKeyup="checkDec(this);"></td>
						<td colspan="2"><input type="text" name="acount10"
							id="acount10" maxlength="9" onKeyup="checkDec(this);"></td>
					</tr>
					<tr>
						<td colspan="2"></td>
						<td><input onclick="return validateACD4()" type="submit"
							value="Ok" name="btn" id="ok" /> <input
							onclick="return validateACD4()" type="submit" value="Download"
							name="btn" id="download" /></td>
					</tr>
				</table>
			</form>
		</fieldset>
	</logic:equal>

</body>
</html>