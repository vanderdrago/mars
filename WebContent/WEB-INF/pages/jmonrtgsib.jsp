<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
function validatemrtgsib(){
    var amont_a;
    var amont_b;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}

</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h2 align="center">Join Monitoring & Reporting RTGS MCB-IB</h2>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <form action="/ReportMCB/vJMonRTGSIB.do?fr=f" method="POST" name="mrtgsib">
	    
        <table border="0" width="100%">
			<tr>
				<td style="width: 240px">No Referensi</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="refno" id="refno"></td>
			</tr>
			<tr>
				<td style="width: 240px">From Account</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="fromacc" id="fromacc"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td style="width: 240px" >To Account</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="toacc" id="toacc"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td style="width: 240px">Start Amount</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="amont_a" id="amont_a"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td style="width: 240px" >To Amount</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="amont_b" id="amont_b"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td><input onclick="return validatemrtgsib()" type="submit" value="Ok" name="ok" id="ok" /> <!-- onclick="return validatemrtgs()" -->
				</td>
			</tr>
	</table>
        </form>
            <br />
       <div style="font-size:20px; font-family:times new roman;">

           <table id="rounded-corner" style="width:150%">
               <tr >
               <td><table><tr><td width="1040"><h4 align="center">INTERNET BANKING</h4></td><td style="width:10px;"></td><td width="1260"><h4 align="center">MCB</h4></td></tr>
               	<tr>
               <display:table name="ltrn" id="ltrn" class="wb" requestURI="MJRtgsIbForward.do" pagesize="20" sort="external" >
		            <td>
		            	<display:column title="<h3>Ref No</h3>" sortable="true" style="width:70px; ">${ltrn.refNo}</display:column>
			            <display:column title="<h3>Ref Id</h3>" sortable="true" style="width:70px; ">${ltrn.refId}</display:column>
			            <display:column title="<h3>Trf Type</h3>" sortable="true" style="width:110px; ">${ltrn.transferType}</display:column>          
			            <display:column title="<h3>Account No</h3>" sortable="true" style="width:100px;">${ltrn.accountNo}</display:column>
			            <display:column title="<h3>To Account No</h3>" sortable="true" style="width:100px; ">${ltrn.toAccountNo}</display:column>
			            <display:column title="<h3>To Name</h3>" sortable="true" style="width:180px; ">${ltrn.toName}</display:column>
			            <display:column title="<h3>To Bank Name</h3>" sortable="true" style="width:90px; ">${ltrn.toBankName}</display:column>
			            <display:column title="<h3>Amount</h3>" sortable="true" style="width:85px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.amount}" /></display:column>
			            <display:column title="<h3>Response Code</h3>" sortable="true" style="width:120px; text-align:center;">${ltrn.responseCode}</display:column>
			            <display:column title="<h3>IB Status</h3>" sortable="true" style="width:110px; text-align:center;">${ltrn.status}</display:column>
					</td>
					<td>  <display:column> </display:column></td>		        	
		        	<td>
		        		<display:column title="<h3>No Contract</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.contractRefNo}</display:column>
			            <display:column title="<h3>MCB TRN</h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.udf_2}</display:column>
			            <display:column title="<h3>From Acc No</h3>" sortable="true" style="width:100px; ">${ltrn.custAcNo}</display:column> 
			            <display:column title="<h3>From Acc Name</h3>" sortable="true" style="width:180px; ">${ltrn.custName}</display:column>         
			            <display:column title="<h3>Bank Code</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.custBankcode}</display:column>
			            <display:column title="<h3>Benef Acc No</h3>" sortable="true" style="width:100px; ">${ltrn.cptyAcNo}</display:column>
			            <display:column title="<h3>Benef Acc Name</h3>" sortable="true" style="width:180px; ">${ltrn.cptyName}</display:column>
			            <display:column title="<h3>Amount</h3>" sortable="true" style="width:120px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.txnAmount}" /></display:column>
			            <display:column title="<h3>Maker</h3>" sortable="true" style="width:80px; ">${ltrn.makerId}</display:column>
			            <display:column title="<h3>Checker</h3>" sortable="true" style="width:80px; ">${ltrn.checkerId}</display:column>
			            <display:column title="<h3>Dispatch Ref</h3>" sortable="true" style="width:80px; text-align:center;">${ltrn.dispatchRefNo}</display:column>
			            <display:column title="<h3>MCB Status</h3>" sortable="true" style="width:110px; text-align:center;">${ltrn.exceptionQueue}</display:column>
		        	</td>
		        	</display:table>
		        	</tr></table></td>
                </tr>
           </table>
        </div>
     <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
	 </logic:equal>
</body>
</html>