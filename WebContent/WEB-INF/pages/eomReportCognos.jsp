<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">

$(function() {
   $( "#prd" ).datepicker({
		showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
	    changeMonth: true,
	    changeYear: true,
	    dateFormat: 'yymm',
	    showButtonPanel: true,
	    onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	    beforeShow: function() {
	       if ((selDate = $(this).val()).length > 0)
	      {
	          iYear = selDate.substring(selDate.length - 4, selDate.length);
	          iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5),
	                   $(this).datepicker('option', 'monthNames'));
	          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
	          $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	       }
	    }
   });
	  
});
function validateLap(){
    var prd;
    var brnch;
    prd = document.getElementById('prd').value;
    brnch = document.getElementById('kdcab').value;
    
    if (prd == ""){
        alert('Silahkan isi Periode');
        return false;
    }
    if (brnch == ""){
        alert('Kode Cabang belum diisi');
        return false;
    }
    return true;
}

$(function() {
    $( "#lhr" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
  		yearRange: "1925:2013",
        buttonImageOnly: true,
        dateFormat: 'yy-mm',
        changeYear: true
    });


});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}

</script>
<style type="text/css">
  .ui-datepicker-calendar {
	    display: none;
	}
</style>
<table border="0" width="100%">
  <tr>
    <td align="center" colspan="2">
    	<fieldset style="width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
		    <legend> <b>EOM Report Cognos</b> </legend>
		     <form action="viewEOMReportCognos.do" method="POST" name="jurnal">
		    <table>
		        <tr>
            <td colspan="3">Field EoM Report</td>
        </tr>
        <tr>
            <td>Periode</td>
            <td>:</td>
            <td><input type="text" name="prd" id="prd" readonly="true"><label style="color:red">*</label>&nbsp;yyyyMM</td>
        </tr>
        <tr>
            <td>Kode Cabang</td>
            <td>:</td>
            <td><input type="text" name="kdcab" id="kdcab" maxlength="3" onKeyup="checkDec(this);"><label style="color:red">*</label></td>
        </tr>
        <tr>
            <td colspan="2"> </td>
            <td>
                <input onclick="return validateLap()" type="submit" value="View" name="btn" id="ok" />
            </td>
            </tr>
		    </table>
		    </form>
		</fieldset>		    	
    </td>
  </tr>
</table>

<logic:equal value="viewdata" name="eomReport"><br></br>
<table id="rounded-corner" style="width: 750px;">
					<thead>
						<tr align="center">
							<th style="width:20px; font-size: 12px;" ><b>No</b></th>
							<th style="width:270px; font-size: 12px;"><b>FileName</b></th>
							<th style="width:360px; font-size: 12px;"><b>Description</b></th>
							<th style="width:100px; font-size: 12px;"><b>Download</b></th> 
						</tr>
					</thead>
					<tbody>
						<c:set var="no_inc" value="1" />
						<c:forEach var="fn" items="${fn}" >
						<tr>
							<td >${no_inc }</td>
							<td >${fn.namefile}</td>
							<td >${fn.description}</td>
							<td ><a href="/ReportMCB/dwdRepeom.do?filename=${fn.namefile}">Download</a></td>
							<c:set var="no_inc" value="${no_inc + 1}" />
						</tr>
						</c:forEach>
					</tbody>
				</table>
</logic:equal>
<logic:equal value="notok" name="eomReport">
${respon}
</logic:equal>