<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="color:black;width:850px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Convert Your Base64 To Image</b></legend>
					<form action="imageUploadAct.do" name="jurnal" method="POST">
						<table align="left">
							<tr>
								<td >
									Enter Base64 String
								</td>
							</tr>
							<tr>
								<td>
									<textarea name="base64string" id="base64string" cols="80" rows="10">	</textarea>
								</td>
							</tr>
							<tr>
								<td>
								
								<input type="submit" value="Generate Image" name="btn" id="generate" class="btn span3 btn-large span11" /></td>
							</tr>
							<tr>
								<td>
									Output
								</td>
							</tr>
							<tr>
								<td>
									<textarea name="base64string1" id="base64string1" cols="80" rows="10">	</textarea>
								</td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
	</table>
</body>
</html>