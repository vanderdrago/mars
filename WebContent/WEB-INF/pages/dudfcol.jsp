<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemrtgsint(){
    var amont_a;
    var amont_b;
    var inputtrn;
    var stsrtgs;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
	inputtrn = document.getElementById('inputtrn').value;     
    stsrtgs = document.getElementById('stsrtgs').value;  
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}
</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h2 align="center">User Defined Fields Collateral</h2>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <form action="/ReportMCB/vMonRTGS.do" method="POST" name="mrtgs">
	    
        
        </form>
            <br />
       <div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:100%">
               <tr ><td>
		            <display:table name="ltrn" id="ltrn" class="wb" requestURI="MonRtgsForward.do" pagesize="20" sort="external" >
		            <display:column title="<h3>Value Date</h3>" sortable="true" style="width:65px;">${ltrn.valueDate}</display:column>
		            <display:column title="<h3>Rel TRN</h3>" sortable="true" style="width:70px;">${ltrn.relTRN}</display:column>
		            <display:column title="<h3>TRN</h3>" sortable="true" style="width:40px;">${ltrn.trn}</display:column>
		            <display:column title="<h3>To Member</h3>" sortable="true" style="width:50px;">${ltrn.toMember}</display:column>
		            <display:column title="<h3>To Acc Name</h3>" sortable="true" style="width:100px;">${ltrn.toAccName}</display:column>              
		            <display:column title="<h3>Ultimate Benef Acc</h3>" sortable="true" style="width:110px;">${ltrn.ultimateBeneAcc}</display:column>          
		            <display:column title="<h3>Ultimate Benef Name</h3>" sortable="true" style="width:140px;">${ltrn.ultimateBeneName}</display:column>
		            <display:column title="<h3>From Acc No</h3>" sortable="true" style="width:80px;">${ltrn.fromAccNum}</display:column>
		            <display:column title="<h3>From Acc Name</h3>" sortable="true" style="width:150px;">${ltrn.fromAccName}</display:column>
		            <display:column title="<h3>Amount</h3>" sortable="true" style="width:120px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.nominal}" /></display:column>
			        <display:column title="<h3>Pay Detail</h3>" sortable="true" style="width:190px;">${ltrn.payDetails}</display:column>
		            <display:column title="<h3>BOR</h3>" sortable="true" style="width:50px;">${ltrn.bor}</display:column>
		            <display:column title="<h3>Status</h3>" sortable="true" style="width:50px; text-align:center;">${ltrn.status}</display:column>
		        	</display:table>
                    </td>
               </tr>
            </table>
        </div>
     <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
	 </logic:equal>
	 
	
</body>
</html>