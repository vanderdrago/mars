<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">

function validatemrtgsint(){
    var amont_a;
    var amont_b;
    var liabid;
    var colcod;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
	
	if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Collateral Value Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Collateral Value Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Collateral Value harus lebih kecil dari To Collateral Value');
        return false;
    }
    return true;
}
</script>

<head>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Collateral</title>
</head>
<body>
	<table width="80%" border="0">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Fields Collateral Reports</b></legend>
					<form action="vGetcolt.do" name="mgetcol" method="POST">
						<table>
							<tr>
								<td>
									<input type="text" name="liabno" id="liabno" class="inputs" placeholder="Liability No" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="colcod" id="colcod" class="inputs" placeholder="Collateral Code" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td align="right"><a href="#" onclick="document.forms['mgetcol'].submit(); return valdata();" class="blue btn">Search</a></td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color:black">
			<td align="center">
				<table border="0" width="1075px">
					<tr>
						<td>
							<div style="font-size:13px; font-family:times new roman;">
								<table id="rounded-corner" style="width:1075px">
									<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:100px; font-size: 12px;"><b>LIABILITY NO</b></th>
										<th style="width:100px; font-size: 12px;"><b>LIABILITY NAME</b></th>
										<th style="width:100px; font-size: 12px;"><b>COLLAT CODE</b></th>
										<th style="width:200px; font-size: 12px;"><b>COLLAT DESC</b></th>
										<th style="width:100px; font-size: 12px;"><b>CURRENCY</b></th>
										<th style="width:20px; font-size: 12px;"><b>BRANCH</b></th>
										<th style="width:100px; font-size: 12px;"><b>COLLAT VALUEI</b></th>
										<th style="width:100px; font-size: 12px;"><b>LENDABLE MARGIN</b></th>
										<th style="width:100px; font-size: 12px;"><b>LIMIT CONTRIBUTION</b></th>
										<th style="width:100px; font-size: 12px;"><b>START DATE</b></th>
										<th style="width:100px; font-size: 12px;"><b>END DATE</b></th>
										<th style="width:100px; font-size: 12px;"><b>STATUS RECORD</b></th>
										<th style="width:100px; font-size: 12px;"><b>STATUS AUTH</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="ltrn" items="${ltrn}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td>${inc}</td>
										<td>${ltrn.liab_no}</td>
										<td>${ltrn.liab_name}</td>
										<td><a href="/ReportMCB/searchcol.do?collateral_code=${ltrn.collateral_code}" target="_blank">${ltrn.collateral_code}</a></td>									
										<td>${ltrn.collateral_description}</td>
										<td>${ltrn.collateral_currency}</td>
										<td>${ltrn.branch_code}</td>
										<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.collateral_value}"/></td>
										<td>${ltrn.lendable_margin}</td>
										<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.limit_contribution}"/></td>									
										<td>${ltrn.start_date}</td>
										<td>${ltrn.end_date}</td>
										<td>${ltrn.status_rcd}</td>
										<td>${ltrn.status_oto}</td>
									</tr>
									</c:forEach>
								</tbody>
								</table>
							</div>
							<logic:equal name="konfirmasi" value="err">
						    	<b>${dataresponse}</b>
							</logic:equal>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		
	</table>
	



</body>
</html>