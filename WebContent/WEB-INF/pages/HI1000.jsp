<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/toolTipText.css" rel="stylesheet" type="text/css"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HI1000</title>
</head>
<body>
	<table border="0" align="center">
		<tr style="color: black;">  		
			<td  >
				<fieldset style="width:1050px; border:0px solid #999; border-radius:8px; box-shadow:0 0 5px #999;">
					<legend> <b></b> </legend>
	    			<table id="rounded-corner"  >
	    				<tr>
	    					<th style="font-size: 20px;align="center"; colspan=7 ;"><b><h3><center>INFORMASI HI-1000</center></h3></b></th>
	    				</tr>
	    			</table>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<table id="rounded-corner"  border="0.5" align="center" style=" width : 1070px;">
					<thead>
						<tr>
							<th style="width:50px; font-size: 12px;" ><b>NO</b></th>
							<th style="width:150px; font-size: 12px;" ><b>GL CLASS</b></th>
							<th style="width:150px; font-size: 12px;"><b>RUN DATE</b></th>
							<th style="width:150px; font-size: 12px;" ><b>IDR</b></th>
							<th style="width:200px; font-size: 12px;" ><b>USD</b></th>
						</tr>
					</thead>
					<tbody>
						<c:set var="inc" value="0" />
						<c:forEach var="listHI" items="${listHI}" >
							<tr>
								<c:set var="inc" value="${inc + 1}" />
								<td>${inc}</td>
								<td>${listHI.glClass}</td>
								<td>${listHI.date}</td>
								<td>${listHI.idr}</td>
								<td>${listHI.usd}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</td>
		</tr>
<!-- 				<table> -->
<!-- 					<tr> -->
<!-- 						<td> -->
							
<!-- 						</td> -->
<!-- 						<td align="right"> -->
<%-- 							<c:if test="${sawal >= 1}">Search Result - ${noOfRecords} Result</c:if> --%>
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td colspan="2"> -->
<!-- 							<table  id="rounded-corner" align="center" style="width: 600px;"> -->
<!-- 							<thead> -->
<!-- 								<tr align="center"> -->
<!-- 									<td colspan = "4"> -->
<!-- 										Tabel H1000 -->
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<th style="width:50px; font-size: 12px;" ><b>NO</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;" ><b>KODE MATA UANG</b></th> -->
<!-- 									<th style="width:250px; font-size: 12px;" ><b>KODE MATA UANG ASING</b></th> -->
<!-- 									<th style="width:230px; font-size: 12px;"><b>AMOUNT DALAM NEGERI</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;" ><b>AMOUNT ASING</b></th> -->
<!-- 									<th style="width:150px; font-size: 12px;"><b>RUN DATE</b></th> -->
<!-- 								</tr> -->
<!-- 							</thead> -->
<!-- 							<tbody> -->
<%-- 								<c:set var="inc" value="0" /> --%>
<%-- 								<c:forEach var="listHI" items="${listHI}" > --%>
<!-- 									<tr> -->
<%-- 										<c:set var="inc" value="${inc + 1}" /> --%>
<%-- 										<td>${inc}</td> --%>
<%-- 										<td>${listHI.lccyCode}</td> --%>
<%-- 										<td>${listHI.fccyCode}</td> --%>
<%-- 										<td>${listHI.idr}</td> --%>
<%-- 										<td>${listHI.usd}</td> --%>
<%-- 										<td>${listHI.date}</td> --%>
<!-- 									</tr> -->
<%-- 								</c:forEach> --%>
<!-- 							</tbody> -->
<!-- 							</table> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 				</table> -->
<!-- 			</td> -->
<!-- 		  </tr> -->
	</table>
</body>
</html>