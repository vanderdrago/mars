<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/ReportMCB/scripts/css3-mediaqueries.js"></script>
<script type="text/javascript">
function valdata() {
	var acc ;
	var nama;
	acc = document.getElementById('acc').value;
	nama = document.getElementById('nama').value;
	acount = document.getElementById('acount').value;
    nama = document.getElementById('nama').value;
	if (acc == "" && nama == ""){
		alert('Silahkan isi No Rekening atau Nama terlebih dahulu');
		return false;
	}
	return true;
}
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>Insert title here</title>
</head>
<body>
<table border="0" width="80%">
  <tr>
    <td align="center" colspan="2">
    	<fieldset style="color:black;font-family:Helvetica;width:450px; border:0px solid #000000; border-radius:8px; box-shadow:0 0 20px #999;">
		    <legend> <b>PERAGAAN HISTORY SALDO RATA-RATA</b> </legend>
		    <form action="peragaanSaldoAct.do" method="POST" name="jurnal">
		    <table>
				<tr>
					<td><input type="text" name="acc" id="acc" class="inputs" placeholder="No Rekening" style=" width : 400px;"></td>
				</tr>
				<tr>
					<td><input type="text" name="nama" id="nama" class="inputs" placeholder="Nama Nasabah" style=" width : 400px;"></td>
				</tr>
				<tr>
					<td align="right"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
				</tr>
				<tr>
	            	<td colspan="3" align = "center">
	            		<logic:equal name="confrm" value="err">
						<p style="color: red;"> ${respon} </p>
						</logic:equal>	
	            	</td>
	            </tr>
			</table>
		    </form>
		</fieldset>
    </td>
  </tr>
  <tr style="color:black">
  	  <td align="center">
  		  <table border="0" width="1075px">
  		  	  <tr>
  		  	  	  <td>
  		  	  	   	  <table border="0">
  		  	  	   	  	  <tr>
  		  	  	   	  	  	  <td>
  		  	  	   	  	  	  	<c:if test="${sawalSaldo >= 1}">Page ${sawalSaldo} Of ${noOfPagesSaldo}</c:if>
  		  	  	   	  	  	  </td>
  		  	  	   	  	  	  <td align="left">
  	  							  <c:if test="${sawalSaldo >= 1}"><a href="manageLphSaldoFwd.do?page=1" class="page">First</a></c:if>
  	  						  </td>	
  	  						  <td>
		       					  <c:if test="${currentPageSaldo != 1 && (currentPageSaldo - 1 > 0)}">
								  <a href="manageLphSaldoFwd.do?page=${currentPageSaldo - 1}" class="page">Previous</a>
							      </c:if>
		       			      </td>
		       			      <td>
		       			      	  <table>
		       			      	  	  <tr>
		       			      	  	  	  <td>
		       			      	  	  	  	  <c:if test="${sawalSaldo >= 1}">
		       			      	  	  	  	  	  <c:forEach begin="${sawalSaldo}" end="${sakhirSaldo}" var="i">
		       			      	  	  	  	  	  	 <c:choose><td><a href="manageLphSaldoFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
		       			      	  	  	  	  	  </c:forEach>
		       			      	  	  	  	  </c:if>
		       			      	  	  	  </td>
		       			      	  	  </tr>
		       			      	  </table>
		       			      </td>
		       			      <td>
					     		  <c:if test="${currentPageSaldo lt noOfPagesSaldo}">
						          	<a href="manageLphSaldoFwd.do?page=${currentPageSaldo + 1}" class="page">Next</a>
								  </c:if>
					    	  </td>
					    	  <td>
					    	  	  <c:if test="${noOfPagesSaldo - 9 > 0}">
					       			<a href="manageLphSaldoFwd.do?page=${currentPageSaldo - 9}"class="page">Last</a>
					       		  </c:if>
					    	  </td>
  		  	  	   	  	  </tr>
  		  	  	   	  </table>
  		  	  	    </td>
  		  	  	  	<td align="right">
  		  	  	  		<c:if test="${sawalSaldo >= 1}">Search Result - ${noOfRecordsSaldo} Result</c:if>
  		  	  	  	</td>
  		  	  </tr>
  		  	  <tr>
  		  	  	  <td colspan="2">
					<div style="font-size:13px; font-family:times new roman;">
					<table id="rounded-corner" style="width:1075px"> 
						<thead>
							<tr align="center">
								<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
								<th style="width:75px; font-size: 12px;"><b>BRANCH</b></th>
								<th style="width:300px; font-size: 12px;"><b>BRANCH NAME</b></th>
								<th style="width:100px; font-size: 12px;"><b>NO REKENING</b></th>
								<th style="width:300px; font-size: 12px;"><b>NAMA</b></th>
								<th style="width:100px; font-size: 12px;"><b>NO CIF</b></th>
								<th style="width:100px; font-size: 12px;"><b>PRODUCT CODE</b></th>
								<th style="width:100px; font-size: 12px;"><b>OPENING DATE</b></th>
								<th style="width:100px; font-size: 12px;"><b>PERAGAAN</b></th>
							</tr>
						</thead>
						<tbody>
							<c:set var="inc" value="0" />
							<c:forEach var="lphSaldo" items="${lphSaldo}" >
							<tr style="font-size: 11px;">
								<c:set var="inc" value="${inc + 1}" />
								<td>${inc}</td>
								<td>${lphSaldo.branch}</td>
								<td>${lphSaldo.branch_name}</td>
								<td>${lphSaldo.acc_no}</td>
								<td>${lphSaldo.acc_desc}</td>
								<td>${lphSaldo.cust_no}</td>
								<td>${lphSaldo.product_code}</td>
								<td>${lphSaldo.opening_date}</td>
								<td><a href="/ReportMCB/vPeragaanDetail.do?acc=${lphSaldo.acc_no}&nama=${lphSaldo.acc_desc}" target="_blank">Detail </a></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</td>
  		 </tr>
<!--   		</div> -->
  	</table>
  </td>
 </tr>
</table>
</body>
</html>