<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
	function valdata(){
		var userId;
			userId = document.getElementById('userId').value;
		if (userId == ""){
			alert ('Silahkan isi User Id Terlebih Dahulu');
			return false;
			}
		return true;
		}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Monitoring Aktivitas Supervisor Versi CS</b></legend>
					<form action="LapSpvCSAct.do" method="POST" name="jurnal">
						<table border ="0">
							<tr>
								<td><input type="text" name="userId" id="userId" class="inputs" placeholder="User Id" style=" width : 400px;"></td>
<!-- 								<td><input type = "text" name = "userId" id = "userId"></td> -->
							</tr>
							<tr>
								<td align="right">
									<input onclick="return valdata()" type="submit" value="Cari" name="btn" id="ok" class="blue btn" />
									<input onclick="return valdata()" type="submit" value="Cetak" name="btndwnld" id="dwnld" class="blue btn" />
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<logic:equal value="err" name="confirm">
										<p style="color: red;"> ${respon} </p>
									</logic:equal>
								</td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color:black">
		  	<td align="center">
		  		<table border="0" width="1075px">
					<tr>
						<td>
							<table border = "0">
								<tr>
									<td>
										<c:if test="${sawalCs >= 1}">Page ${sawalCs} Of ${noOfPagesCs}</c:if>
									</td>
									<td align="left">
		  	  							 <c:if test="${sawalCs >= 1}"><a href="manageLphCsFwd.do?page=1" class="page">First</a></c:if>
		  	  					    </td>
		  	  					    <td>
			       					    <c:if test="${currentPageCs != 1 && (currentPageCs - 1 > 0)}">
									  	   <a href="manageLphCsFwd.do?page=${currentPageCs - 1}" class="page">Previous</a>
								        </c:if>
		       			      		</td>
		       			      		<td>
		       			      			<table>
				       			      	  	  <tr>
				       			      	  	  	  <td>
				       			      	  	  	  	  <c:if test="${sawalCs >= 1}">
				       			      	  	  	  	  	  <c:forEach begin="${sawalCs}" end="${sakhirCs}" var="i">
				       			      	  	  	  	  	  	 <c:choose><td><a href="manageLphCsFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
				       			      	  	  	  	  	  </c:forEach>
				       			      	  	  	  	  </c:if>
				       			      	  	  	  </td>
				       			      	  	  </tr>
		       			      	  		</table>
		       			      		</td>
		       			      		<td>
		       			      			<c:if test="${currentPageCs lt noOfPagesCs}">
						          			<a href="manageLphCsFwd.do?page=${currentPageCs + 1}" class="page">Next</a>
								 		</c:if>
		       			      		</td>
		       			      		<td>
		       			      			<c:if test="${noOfPagesCs - 9 > 0}">
							       			<a href="manageLphCsFwd.do?page=${currentPageCs - 9}"class="page">Last</a>
							       		</c:if>
		       			      		</td>
								</tr>
							</table>
						</td>
						<td align="right">
							<c:if test="${sawalCs >= 1}">Search Result - ${noOfRecordsCs} Result</c:if>
						</td>
					</tr>
					<tr>
						<td colspan="2"> 
							<div style="font-size:13px; font-family:times new roman;">
								<table id="rounded-corner" style="width:1075px"> 
									<thead>
										<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:75px; font-size: 12px;"><b>BRANCH</b></th>
										<th style="width:300px; font-size: 12px;"><b>BRANCH NAME</b></th>
										<th style="width:100px; font-size: 12px;"><b>KEY ID</b></th>
										<th style="width:100px; font-size: 12px;"><b>TANGGAL</b></th>
										<th style="width:75px; font-size: 12px;"><b>JAM</b></th>
										<th style="width:100px; font-size: 12px;"><b>FUNCTION ID</b></th>
										<th style="width:150px; font-size: 12px;"><b>MAKER ID</b></th>
										<th style="width:150px; font-size: 12px;"><b>CHECKER ID</b></th>
										<th style="width:100px; font-size: 12px;"><b>MODIFIKASI KE-</b></th>
										</tr>
									</thead>
									<tbody>
										<c:set var="inc" value="0" />
										<c:forEach var="lphCs" items="${lphCs}" >
										<tr style="font-size: 11px;">
											<c:set var="inc" value="${inc + 1}" />
											<td>${inc}</td>
											<td>${lphCs.branch}</td>
											<td>${lphCs.branch_name}</td>
											<td>${lphCs.key_id}</td>
											<td>${lphCs.tanggal}</td>
											<td>${lphCs.jam}</td>
											<td>${lphCs.function_id}</td>
											<td>${lphCs.maker_id}</td>
											<td>${lphCs.checker_id}</td>
											<td>${lphCs.modifikasi_ke}</td>
										</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>