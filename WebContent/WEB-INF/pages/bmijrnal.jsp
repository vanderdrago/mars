<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body> 

<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
<table border="0" width="100%">
	<tr>
    	<td align="center" colspan="3"><b>${title} </b></td>
     </tr>
</table>
<table id="rounded-corner" style="width: 900px">
    <thead>
        <tr>
            <th><b>TGL TRN</b></th>
            <th><b>BATCH NO</b></th>
            <th><b>DESKRIPSI</b></th>
            <th><b>MAKER ID</b></th>
            <th><b>DEBIT</b></th>
            <th><b>KREDIT</b></th>
        </tr>
    </thead>

    <tbody>
    	<c:forEach items="${lBatchM}" var="lBatchM">
        <tr>
            <td>${lBatchM.last_oper_dt_stamp}</td>
            <td><a href="/ReportMCB/detailBatchAct.do?batch_no=${lBatchM.batch_no}&userId=${lBatchM.last_oper_id}"> ${lBatchM.batch_no}</a></td>
            <td>${lBatchM.description}</td>
            <td>${lBatchM.last_oper_id}</td>
            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${lBatchM.dr_ent_total}" /></td>
            <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${lBatchM.cr_ent_total}" /></td>
        </tr>
    </c:forEach>
    </tbody>
    </table>

</body>
</html>