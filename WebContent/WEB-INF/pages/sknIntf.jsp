<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">
$(function() {
    $( "#bsnsdt" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'yymmdd'
    });
});


function valInput(){
    var bsnsdt;
    bsnsdt = document.getElementById('bsnsdt').value;

    if (bsnsdt == ""){
        alert('Business date tidak boleh kosong');
        return false;
    }
    return true;
}


function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}

function dis(){
	document.getElementById("kdcab").disabled=true;
	document.getElementById("idtmmbr").disabled=true;
}
function ena(){
	document.getElementById("kdcab").disabled=false;
	document.getElementById("idtmmbr").disabled=false;
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div style="font-size: 11px;">
<table border="0" align="center">
	<tr>
		<td>			
			<fieldset style="width:1000px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			<legend> <b>Interface SKN</b> </legend>
			<form action="rtgsIntfView.do" method="POST" name="jurnal">
				<table border="0">
					<tr>
						<td colspan="12">
							<input type="radio" name="rtgs" value="out" onclick="ena()" checked="checked">RTGS Outgoing &nbsp;
							<input type="radio" name="rtgs" value="in" onclick="dis()">RTGS Incoming
						</td>
					</tr>
					<tr>
						<td>Busines Date</td>
						<td>:</td>
						<td><input type="text" name="bsnsdt" id="bsnsdt" readonly="true"></td>						
						<td>Account</td>
						<td>:</td>
						<td><input type="text" name="acc" id="acc" maxlength="24"></td>	
						<td>To Member</td>
						<td>:</td>
						<td>
		                    <select name="tmmbr" id="idtmmbr" >
		                        <option value="">Pilih</option>
		                        <c:forEach items="${lmmbrbnk}" var="lmmbrbnk">
		                             <option value=${lmmbrbnk.codename}>${lmmbrbnk.codename}</option>
		                        </c:forEach>
		                    </select>
		                </td>	
						<td>Amount From</td>
						<td>:</td>
						<td><input type="text" name="amtf" id="amtf" maxlength="17"></td>
					</tr>
					<tr><td width="90px;">Cabang</td>
						<td>:</td>
						<td width="180px;"><input type="text" name="kdcab" id="kdcab" maxlength="3"></td>						
						<td>TRN</td>
						<td>:</td>
						<td><input type="text" name="trn" id="idtrn" maxlength="8"></td>							
						<td>From Member</td>
						<td>:</td>
						<td>
		                    <select name="fmmbr" id="idfmmbr" >
		                        <option value="">Pilih</option>
		                        <c:forEach items="${lmmbrbnk}" var="lmmbrbnk">
		                             <option value=${lmmbrbnk.codename}>${lmmbrbnk.codename}</option>
		                        </c:forEach>
		                    </select>
		                    <!-- input type="text" name="fmmbr" id="idfmmbr" maxlength="17" -->
		                </td>						
						<td>Amount To</td>
						<td>:</td>
						<td><input type="text" name="amtto" id="amtto" maxlength="17"></td>
					</tr>
					<tr><td>Txn Status</td>
						<td>:</td>
						<td><select name="txnsts" id="idtxnsts">
							  <option value="">Pilih</option>
							  <option value="CP">Complete (CP)</option>
							  <option value="OK">Converted (OK)</option>
							  <option value="RF">Rejected (RF)</option>
							  <option value="IC">Identic (IC)</option>
							  <option value="RP">Repaired (RP)</option>
							  <option value="DO">Dropped(DO)</option>
							  <option value="WA">Waiting (WA)</option>
							  <option value="RT">Retur (RT)</option>
							</select> 
						</td>			
						<td>Rel TRN</td>
						<td>:</td>
						<td><input type="text" name="reltrn" id="idreltrn" maxlength="16"></td>
						<td colspan="6"></td>
					</tr>
					<tr>
						<td colspan="12"><input onclick="return valInput()"  type="submit" value="Lihat" name="btn" id="idbtn" /></td>
					</tr>
				</table>
			</form>
			</fieldset>
		</td>
	</tr>
</table>
<logic:equal name="respon" value="teks">
<br />
${teks }
</logic:equal>

</div>

<table border="0" width="1075px;">
<tr>
<td>
	<logic:equal name ="data" value="out">
	<div style="  float:left; text-align: left; ">
	
	<table border="0">
		<tr>
	    	<td><a href="manageLtransRtgsFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="manageLtransRtgsFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="manageLtransRtgsFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="manageLtransRtgsFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageLtransRtgsFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style="width:500px;    float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr align="center" style="font-size: 10px;">
					            <th style="width:40px; "><b>MIS ID</b></th>
					            <th style="width:40px; "><b>BOR</b></th>
					            <th style="width:50px;"><b>VALUE DATE</b></th>
					            <th style="width:40px; "><b>RT STATUS</b></th>
					            <th style="width:40px; "><b>STATUS</b></th>
					            <th style="width:40px; "><b>RSNCDE</b></th>
					            <th style="width:50px; "><b>TO MEMBER</b></th>
					            <th style="width:60px; "><b>TRN</b></th>
					            <th style="width:100px; "><b>REL TRN</b></th>
					            <th style="width:900px; "><b>ORIGINATING NAME</b></th>
					            <th style="width:800px; "><b>BENEFICIARY NAME</b></th>
					            <th style="width:50px; "><b>A/C NO</b></th>
					            <th style="width:90px; "><b>AMOUNT</b></th>
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="ltrans" items="${ltrans}" >
						        <tr style="font-size: 10px; ">
						            <td >${ltrans.dataId}</td>
						            <td >${ltrans.bor}</td>
						            <td >${ltrans.valuedate}</td>
						            <td >${ltrans.rtstatus}</td>
						            <td >${ltrans.status}</td>
						            <td >${ltrans.rsncde}</td>
						            <td >${ltrans.tomember}</td>
						            <td >${ltrans.trn}</td>
						            <td >${ltrans.reltrn}</td>
						            <td >${ltrans.oriname}</td>
						            <td >${ltrans.ultimatebenename}</td>
						            <td >${ltrans.ultimatebeneacc}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrans.amount}" /></td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>
	</logic:equal>
	
	<logic:equal name ="data" value="in">
	<div style="  float:left; text-align: left; ">
	
	<table border="0">
		<tr>
	    	<td><a href="manageLtransRtgsFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="manageLtransRtgsFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="manageLtransRtgsFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="manageLtransRtgsFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageLtransRtgsFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style="width:500px;    float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr align="center" style="font-size: 10px;">
					            <th style="width:40px; "><b>MIS ID</b></th>
					            <th style="width:40px; "><b>BOR</b></th>
					            <th style="width:50px;"><b>VALUE DATE</b></th>
					            <th style="width:40px; "><b>STATUS</b></th>
					            <th style="width:40px; "><b>RSNCDE</b></th>
					            <th style="width:50px; "><b>FROM MEMBER</b></th>
					            <th style="width:60px; "><b>TRN</b></th>
					            <th style="width:100px; "><b>REL TRN</b></th>
					            <th style="width:900px; "><b>ORIGINATING NAME</b></th>
					            <th style="width:800px; "><b>BENEFICIARY NAME</b></th>
					            <th style="width:50px; "><b>A/C NO</b></th>
					            <th style="width:90px; "><b>AMOUNT</b></th>
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="ltrans" items="${ltrans}" >
						        <tr style="font-size: 10px; ">
						            <td >${ltrans.dataId}</td>
						            <td >${ltrans.bor}</td>
						            <td >${ltrans.valuedate}</td>
						            <td >${ltrans.status}</td>
						            <td >${ltrans.rsncde}</td>
						            <td >${ltrans.fomember}</td>
						            <td >${ltrans.trn}</td>
						            <td >${ltrans.reltrn}</td>
						            <td >${ltrans.oriname}</td>
						            <td >${ltrans.ultimatebenename}</td>
						            <td >${ltrans.ultimatebeneacc}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrans.amount}" /></td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>
	</logic:equal>
</td>
</tr>
</table>
</body>
</html>