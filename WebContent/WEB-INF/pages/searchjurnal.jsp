<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <table border="0" width="100%">
            <tr>
                <td align="center" ><b>${s_title} </b></td>
            </tr>
        </table>
            <br />
       <div style="font-size:13px; font-family:times new roman;">
     
           <table id="rounded-corner" style="width:100%">
               <tr valign="top"><td>
       
        <display:table name="s_lbb" id="s_lbb" class="wb" requestURI="manageSLbbFwd.do" pagesize="35" sort="external" >
            <display:column title="TGL TRN" sortable="true" style="width:70px;">${s_lbb.trn_dt}</display:column>
            <display:column title="VALUE DATE" sortable="true" style="width:70px;">${s_lbb.value_dt}</display:column>
            <display:column title="AC NO" sortable="true" style="width:70px;">${s_lbb.ac_no}</display:column>   
            <display:column title="BRANCH" sortable="true" style="width:70px;">${s_lbb.ac_branch}</display:column>
            <display:column title="VALUTA" sortable="true" style="width:70px;">${s_lbb.ac_ccy}</display:column>     
            <display:column title="NO REFERENSI" sortable="true" style="width:100px;">${s_lbb.trn_ref_no}</display:column>
            <display:column title="NO BATCH" sortable="true" style="width:70px;">${s_lbb.batch_no}</display:column>         
            <display:column title="EVENT" sortable="true" style="width:70px;">${s_lbb.event}</display:column>          
            <display:column title="MAKER ID" sortable="true" style="width:70px;">${s_lbb.maker_id}</display:column>
            <display:column title="AUTHORIZE ID" sortable="true" style="width:70px;">${s_lbb.auth_id}</display:column>
            <display:column title="TRN CODE" sortable="true" style="width:70px;">${s_lbb.trn_code}</display:column>
            <display:column title="DESKRIPSI" sortable="true" style="width:270px;">${s_lbb.ket}</display:column>
            <display:column title="KURS" sortable="true" style="width:70px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${s_lbb.exh_rate}" /></display:column>
            <display:column title="MUTASI DEBET" sortable="true" style="width:70px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${s_lbb.amt_db}" /></display:column>
            <display:column title="MUTASI KREDIT" sortable="true" style="width:70px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${s_lbb.amt_cr}" /></display:column>            
            <display:column title="NOMINAL EKUIVALEN" sortable="true" style="width:70px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${s_lbb.lcy_amount}" /></display:column>
        </display:table>
                       </td>
                   </tr>
                   <tr>
                       <td><b>TOTAL DB : <fmt:formatNumber type="number" maxFractionDigits="3" value="${s_totDB}" /></b></td>
                   </tr>
                   <tr>
                       <td><b>TOTAL CR : <fmt:formatNumber type="number" maxFractionDigits="3" value="${s_totCR}" /></b></td>
                   </tr>
               </table>
        </div>
</body>
</html>