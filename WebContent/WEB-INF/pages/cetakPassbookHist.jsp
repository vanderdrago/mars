<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<script type='text/javascript' src='/ReportMCB/jsajax/CetakPassbook.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">
$(function() {
	//document.getElementById("userid").disabled=true;
});
function valInput(){
	/*
	var optlne = $('input[name=rbn]:checked').val();
    var acc;
    var usr;
	if (optlne == "acc"){
	    acc = document.getElementById('acc').value;
	    if (acc == ""){
	        alert('Nomor Rekening tidak boleh kosong');
	        return false;
	    }	
	} else {
		usr = document.getElementById('userid').value;
	    if (usr == ""){
	        alert('User Id tidak boleh kosong');
	        return false;
	    }     
	}*/
	var acc = document.getElementById('acc').value;
	var user = document.getElementById('userid').value;
	var pengesah = document.getElementById('pengesah').value;
	if (acc == "" && user == "" && pengesah == ""){
        alert('Minimal satu field terisi.');
        return false;
    }
    return true;
}
/*
function dis(){
	document.getElementById("userid").disabled=false;
	document.getElementById("acc").disabled=true;
}
function ena(){
	document.getElementById("userid").disabled=true;
	document.getElementById("acc").disabled=false;
}
*/
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="0" align="center">
	<tr>
		<td> 
			<fieldset style="width:350px;  border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			    <legend> <b>History Cetak Passbook</b> </legend>
				<form action="viewCetakPassbookHist.do" method="POST" name="jurnal">
					<table border="0">
						
				        <tr>
				        	<!-- td><input type="radio" name="rbn" value="acc" onclick="ena()" checked="checked">No. Rekening</td -->
				        	<td>No. Rekening</td>
				            <td>:</td>
				            <td><input type="text" name="acc" id="acc" maxlength="10" onKeyup="checkDec(this);"></td>
				        </tr>
				        <tr>
				        	<td>User</td>
				            <td>:</td>
				            <td><input type="text" name="userid" id="userid" ></td>
				        </tr>
				        <tr>
				        	<td>Pengesah</td>
				            <td>:</td>
				            <td><input type="text" name="pengesah" id="pengesah" ></td>
				        </tr>
				        <tr>
				            <td > </td>
				            <td colspan="2">
				                <input type="submit" value="Lihat" name="btn" id="view" onclick="return valInput()"/>			                			                
				            </td>
					   </tr>
				    </table>
				</form>
			</fieldset>
		</td>
	</tr>
</table>

<logic:equal name="respon" value="teks">
<b>${teks }</b>
</logic:equal>

<table border="0" width="950px;">
<tr>
<td>
	<logic:equal name ="data" value="viewdata">
	<br />
	<fieldset style="border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; padding-right: 15px;">
		<logic:equal name="hdr" value="data">
			<table border="0" width="100%;">
				<tbody>
					<tr>
						<td colspan="3" align="center"><b>HISTORY CETAK PASSBOOK</b></td>
					</tr>
					<tr >						
						<td width="130">Cabang</td>
						<td width="10">:</td>
						<td >${cust_acc.kdcab}</td>
					</tr>	
					<tr >						
						<td >Nomor Rekening</td>
						<td >:</td>
						<td >${cust_acc.nomrek}</td>
					</tr>
					<tr >						
						<td >Nama</td>
						<td >:</td>
						<td >${cust_acc.rekname}</td>
					</tr>	
					<tr valign="baseline">						
						<td >Alamat</td>
						<td >:</td>
						<td >${cust_acc.address}</td>
					</tr>			
				</tbody>
			</table>
		</logic:equal>
		
		<div style="  float:left; text-align: left; ">	
		<table border="0">
			<tr>
		    	<td><a href="manageCetakHistFwd.do?page=1">First</a></td>
		    	<td>
		    		<c:if test="${currentPage != 1}">
						<a href="manageCetakHistFwd.do?page=${currentPage - 1}">Previous</a>
					</c:if>
		       	</td>
		       	<td>
			    	<%--For displaying Page numbers.
					The when condition does not display a link for the current page--%>
					<table border="0" cellpadding="5" cellspacing="5">
						<tr>
							<c:forEach begin="${sawal}" end="${sakhir}" var="i">
								<c:choose><td><a href="manageCetakHistFwd.do?page=${i}">${i}</a></td>
								</c:choose>
							</c:forEach>
						</tr>
					</table>
		       	</td>
		       	<td>
		       		<c:if test="${currentPage lt noOfPages}"><a href="manageCetakHistFwd.do?page=${currentPage + 1}">Next</a></c:if>
		       	</td>
		       	<td>
		       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageCetakHistFwd.do?page=${noOfPages - 9}">Last</a></c:if>
		       	</td>
		    </tr>
		 </table>
		 </div>
		<div style="width:500px;    float:right; text-align: right"><br />
			<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
		</div>
		<br />
		<div style="float:left;">   
		
		<table id="rounded-corner" >
		    <thead>
		        <tr align="left" >
		            <th style="width:13px; font-size:12px; "><b>No</b></th>
					<th style="width:110px; font-size:12px; "><b>Tanggal Cetak</b></th>
					<th style="width:80px; font-size:12px; "><b>No. Passbook</b></th>
					<th style="width:40px; font-size:12px; "><b>Mulai Baris</b></th> 
					<th style="width:40px; font-size:12px; "><b>Jumlah Transaksi</b></th>
					<th style="width:100px; font-size:12px; "><b>User</b></th> 
					<th style="width:100px; font-size:12px; "><b>Pengesah</b></th> 
					<th style="width:100px; font-size:12px; "><b>Print</b></th>
					<th style="width:170px; font-size:12px; "><b>Keterangan</b></th>
		        </tr>
		    </thead>
	
		    <tbody>
		    	<c:forEach var="lctkhst" items="${lctkhst}" >
			        <tr >
			            <td >${lctkhst.no}</td>
			            <td >${lctkhst.print_date_s}</td>
			            <td >${lctkhst.passbook_number}</td>
			            <td style="text-align: center;">${lctkhst.prev_line_no_start}</td>
			            <td style="text-align: center;">${lctkhst.total_trns}</td>
			            <td >${lctkhst.user_print}</td>
			            <td >${lctkhst.user_spv_print}</td>
			            <td >${lctkhst.sts}</td>
			            <td >${lctkhst.notes}</td>
			        </tr>
			    </c:forEach>
		    </tbody>
	      </table>						    
		</div>
	</fieldset>
	</logic:equal>
	
</td>
</tr>
</table>
</body>
</html>