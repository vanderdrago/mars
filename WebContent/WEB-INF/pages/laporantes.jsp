<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>

<script type="text/javascript" src="/ReportMCB/scripts/sweetalert-dev.js"></script>
<link href="/ReportMCB/scripts/sweetalert.css" rel="stylesheet" type="text/css"/>

<link href="css/reset.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript">
	function valdata(){
			var kodecabang;
			var	user;
				kodecabang = document.getElementById('kodecabang').value;
				user = document.getElementById('user').value;
			if (kodecabang == "" && user == ""){
					alert ("Silahkan isi Kode Cabang atau User Id terlebih dahulu");
					return false;
				}
			return true;
		}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td>	
					<fieldset>
						<legend align="center">Laporan Aktivitas Supervisor Versi Teller</legend>
						<form action="LapSpvAct.do" method="POST" name="jurnal" class="cd-form floating-labels">
						<div class="icon">
							<label class="cd-label" for="cd-name">Branch</label>
							<input class="company" type="text" name="kodecabang" id="kodecabang">
						</div>
						<div class="icon">
							<label class="cd-label" for="cd-name">User Id</label>
							<input class="user" type="text" name="user" id="user">
						</div>
						<div>
		      				<input onclick="return valdata()" type="submit" id="ok" name="btn" value="Cari" />
		    			</div>
		    			</form>
					</fieldset>
				<script src="js/jquery-2.1.1.js"></script>
				<script src="js/main.js"></script> 
			</td>
		</tr>
		<tr>
			<td align="center">
				<div style="font-size:13px; font-family:times new roman;">
					<table id="rounded-corner" style="width:1075px" align="center"> 
						<tr>
							<td align="center">
								<div style="font-size:18px; font-family:times new roman;">LIST AKTIVITAS TELLER</div>
							</td>
						</tr>
						<tr>
							<td>
								<display:table name="lph" id="lph" requestURI="ManageSpv.do" pagesize="30" sort="external">
									<display:column title="<b>BRANCH</b>"style="width:50px;">${lph.branch}</display:column>
									<display:column title="<b>TANGGAL</b>"style="width:75px;">${lph.tanggal}</display:column>
									<display:column title="<b>JAM</b>"style="width:75px;">${lph.jam}</display:column>
									<display:column title="<b>FUNCTION DESC</b>"style="width:150px;">${lph.function_desc}</display:column>
									<display:column title="<b>TRN CODE</b>"style="width:75px;">${lph.trn_code}</display:column>
									<display:column title="<b>AC NO</b>"style="width:75px;">${lph.ac_no}</display:column>
									<display:column title="<b>TRN REF NO</b>"style="width:100px;">${lph.trn_ref_no}</display:column>
									<display:column title="<b>NO WARKAT</b>"style="width:75px;">${lph.no_warkat}</display:column>
									<display:column title="<b>DEBET</b>"style="width:100px;">${lph.debet}</display:column>
									<display:column title="<b>KREDIT</b>"style="width:100px;">${lph.kredit}</display:column>
									<display:column title="<b>USER ID</b>"style="width:100px;">${lph.user_id}</display:column>
									<display:column title="<b>AUTH ID</b>"style="width:100px;">${lph.auth_id}</display:column>
								</display:table>
							</td>
						</tr>
					</table>
				</div>
				<logic:equal name="konfirmasi" value="err">
					<p style="color: red;">${dataresponse}</p>
				</logic:equal>
			</td>
		</tr>
	</table>
</body>
</html>