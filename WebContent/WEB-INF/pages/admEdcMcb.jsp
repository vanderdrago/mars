<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<head>
<meta http-equiv="Content-Type:application/font-woff" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
 <link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/bootstrap/bootstrap.css" />

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript'
	src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css" />

<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet"
	type="text/css" />
<link href="/ReportMCB/scripts/text.css" rel="stylesheet"
	type="text/css" />
	


<title>Installment</title>


<script type="text/javascript">


function validateForm (){   

	var tgl_awal = document.forms["admEdcMcb"]["tgl_awal"].value;
	var tgl_akhir = document.forms["admEdcMcb"]["tgl_akhir"].value;
	var branch = document.forms["admEdcMcb"]["branch_code"].value;
	
	if (tgl_awal == ""){
		alert ("Harap tanggal awal diisi");
		return false;
	}
	if  (tgl_akhir == "") {
		alert ("Harap tanggal akhir diisi");
		return false;
	}
	if (branch == ""){
		alert ("Harap kode cabang diisi");
		return false;
	}
	
    return true;
}




$(function() {
	$("#tgl_awal").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	$("#tgl_akhir").datepicker({
		dateFormat : 'yy-mm-dd'
	});
});
</script>




</head>
<body>
	<table width="1075px" border="0">
		<tr>
			<td align="center">
		        <form action="vGetEdcMcb.do" method="POST" name="admEdcMcb" onsubmit="return validateForm()">	
		        		<fieldset
		        		style="color: black; width: 620px; border: 0px solid #999; border-radius: 8px; box-shadow: 0 0 20px #999;">
				<legend align="center">
				<b>REPORT EDC MCB</b>
			</legend>
				<table border="0">
					<tr>
					<td ><input class="form-control" placeholder="Tanggal Awal (yyyy-mm-dd) *" type="text" name="tgl_awal" id = "tgl_awal" ><label
							style="color: red"></label></td>
					</tr>
					
					<tr>
					<td><input class="form-control" placeholder="Tanggal Akhir (yyyy-mm-dd) *" type="text" name="tgl_akhir" id = "tgl_akhir" style="width: 400px"><label
							style="color: red"></label></td>
					</tr>
					
					<tr>
						<td><input class="form-control" style="width: 400px" placeholder="Branch Code *" type="text" name="branch_code" id = "branch_code" maxlength="3"></td>
					</tr>
				
					<tr>
						<td align="center"><button type="submit" class="blue btn" >Search</button></td>
					</tr>
				
				</table>
			
		
					</fieldset>
				</form>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="1075px" border="0">
					<tr>
						<td>
						 <table border="0">
								<tr>
									<td>
										<c:if test="${sawalAngsuran >= 1}">Page ${sawalAngsuran} Of ${noOfPagesAngsuran}</c:if>
									</td>
									<td>
										<nav aria-label="...">
										  <ul class="pagination">
										  	<c:if test="${currentPageAngsuran != 1 && (currentPageAngsuran - 1 > 0)}">
											  <li class="page-item">
											  	<a href="ManageGetEdcMcbFwd.do?page=${currentPageAngsuran - 1}" class="page-link">
											  		<span class="page-link">Previous</span>
											  	</a>
											  </li>
										    </c:if>
										    <c:if test="${sawalAngsuran >= 1}">
										    	<li class="page-item">
										    		<a href="ManageGetEdcMcbFwd.do?page=1" class="page-link">First</a>
										    	 </li>	
										    </c:if>
										    <c:if test="${sawalAngsuran >= 1}"> 
			   			      	  	  	  	  	<c:forEach begin="${sawalAngsuran}" end="${sakhirAngsuran}" var="i">
			   			      	  	  	  	  	  	<c:choose><li><a href="ManageGetEdcMcbFwd.do?page=${i}" class="page-link">${i}</a></li></c:choose>
			   			      	  	  	  	  	</c:forEach>
		   			      	  	  	  	  	</c:if>
		   			      	  	  	  	  	<c:if test="${currentPageAngsuran lt noOfPagesAngsuran}">
		   			      	  	  	  	  	   <li class="page-item">
									          	  <a href="ManageGetEdcMcbFwd.do?page=${currentPageAngsuran + 1}" class="page-link">Next</a>
									         </li>
											 </c:if>
											 <c:if test="${noOfPagesAngsuran - 9 > 0}">
											 	<li class="page-item">
								       				<a href="ManageGetEdcMcbFwd.do?page=${currentPageAngsuran - 9}"class="page-link">Last</a>
								       			</li>
								       		 </c:if>
										  </ul>
										</nav>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<c:if test="${sawalAngsuran >= 1}">Search Result - ${noOfRecordsAngsuran} Result</c:if>
						</td>
					</tr>
					<tr>
						<td>
							<table class="table table-striped" id="keywords">
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 10px; font-style: family font sains"><b>NO</b></th>
										<th style="width:80px; font-size: 10px;"><b>BRANCH CODE</b></th>
										<th style="width:60px; font-size: 10px;"><b>TRN REF NO</b></th>
										<th style="width:100px; font-size: 10px;"><b>FID</b></th>
										<th style="width:60px; font-size: 10px;"><b>DATE</b></th>
										<th style="width:60px; font-size: 10px;"><b>MAKER ID</b></th>
										<th style="width:60px; font-size: 10px;"><b>CHECKER ID</b></th>
										<th style="width:70px; font-size: 10px;"><b>CHECKER DATE</b></th>
										<th style="width:50px; font-size: 10px;"><b>ACCOUNT</b></th>
										<th style="width:90px; font-size: 10px;"><b>CCY</b></th>
										<th style="width:90px; font-size: 10px;"><b>AMOUNT</b></th>
										<th style="width:90px; font-size: 10px;"><b>CHECKREMARK</b></th>
										<th style="width:90px; font-size: 10px;"><b>MAKERREMARK</b></th>
										<th style="width:100px; font-size: 10px;"><b>EDC STATUS</b></th>
										<th style="width:100px; font-size: 10px;"><b>ACCOUNT CLASS</b></th>
										<th style="width:100px; font-size: 10px;"><b>ATM FACILITY</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="RptEdcMcb" items="${RptEdcMcb}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td style="font-size: 10px">${inc}</td>
										<td>${RptEdcMcb.branch_code}</td>
										<td>${RptEdcMcb.trn_ref_no}</td>
										<td>${RptEdcMcb.functionid}</td>
										<td>${RptEdcMcb.wfinitdate}</td>
										<td>${RptEdcMcb.makerid}</td>
										<td>${RptEdcMcb.checkerid}</td>
										<td>${RptEdcMcb.checkerdatestamp}</td>
										<td>${RptEdcMcb.txnactdet}</td>
										<td>${RptEdcMcb.txnccydet}</td>
										<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${RptEdcMcb.txnamtdet}" /></td>
										<td>${RptEdcMcb.checkerremarks}</td>
										<td>${RptEdcMcb.makerremarks}</td>
										<td>${RptEdcMcb.edc_verify_status}</td>
										<td>${RptEdcMcb.account_class}</td>
										<td>${RptEdcMcb.atm_facility}</td>
											</tr>
									</c:forEach>
								</tbody>
							</table>
							<logic:equal name="konfirmasi" value="err">
					    		<center><b>${dataresponse}</b></center>
							</logic:equal>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
<script type="text/javascript">
$(function(){
  $('#keywords').tablesorter(); 
});
</script>
	
</body>
</html>