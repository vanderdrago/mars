<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script type="text/javascript">
function validateAccNo(accNo) 
	{
	    var maintainplus = '';
	    var numval = accNo.value
	    if ( numval.charAt(0)=='+' )
	    {
	        var maintainplus = '';
	    }
	    curphonevar = numval.replace(/[\\A-Za-z!"£$%^&\,*+_={};:'@#~,.Š\/<>?|`¬\]\[]/g,'');
	    accNo.value = maintainplus + curphonevar;
	    var maintainplus = '';
	    accNo.focus;
	}

function validateForm (){   

	var accno = document.forms["mgetinstl"]["accno"].value;
	var cusid = document.forms["mgetinstl"]["cusid"].value;
	var cusname = document.forms["mgetinstl"]["cusname"].value;
	var status = document.forms["mgetinstl"]["status"].value;

	if ((accno == "") && (cusid == "") && (cusname == "")){
		alert ("Account Number No Empty");
		return false;
	}
	if ((status == "Choose Account Status...")){
		alert ("Choose Account Status");
		return false;
	}
    return true;
}
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/css/demos.css" /> -->
<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,400' rel='stylesheet' type='text/css'> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <script src="http://code.jquery.com/jquery-1.12.1.min.js"></script> -->
<script src="/ReportMCB/scripts/js/sorttable.js"></script>
<!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<!-- <link rel="stylesheet" href="plugins/select2/select2.min.css"> -->
     
<title>Peragaan Rekening</title>
</head>
<body>
	<table width="1075px">
		<tr>
			<td align="center">
				<form action="viewRagaHist.do" method="POST" name="jurnal">
					<fieldset>
						<legend>PERAGAAN HISTORY</legend>
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<input class="form-control" placeholder="No Rekening" type="text" name="acount" id = "acount" maxlength="10" onkeyup="validateAccNo(this);">										
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group">
										<div class="input-group-addon">
											<i class="glyphicon glyphicon-user"></i>
										</div>
										<input class="form-control" placeholder="Nama Nasabah" type="text" name="nama" id = "nama">
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-6">
									<div class="input-group">
										<div class="input-group-addon">
											<i class="glyphicon glyphicon-credit-card"></i>
										</div>
										<input class="form-control" placeholder="No KTP" type="text" name="ktp" id = "ktp">
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-6">
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" type="text" name="lhr" placeholder = "Tanggal Lahir">								
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-1" align="left">
									<button type="submit" class="btn btn-success" value="send" name="btnsend" id="ok"> Search <span class="glyphicon glyphicon-search"></span></button>															        		
					        	</div>
							</div>
						</div>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="1075px" border="0">
					<tr>
						<td>
							<table border="0">
								<tr>
									<td>
										<c:if test="${sawal >= 1}">Page ${sawal} Of ${noOfPages}</c:if>
									</td>
									<td>
										<nav aria-label="...">
										  <ul class="pagination">
										  	<c:if test="${currentPage != 1 && (currentPage - 1 > 0)}">
											  <li class="page-item">
											  	<a href="manageLphFwd.do?page=${currentPage - 1}" class="page-link">
											  		<span class="page-link">Previous</span>
											  	</a>
											  </li>
										    </c:if>
										    <c:if test="${sawal >= 1}">
										    	<li class="page-item">
										    		<a href="manageLphFwd.do?page=1" class="page-link">First</a>
										    	 </li>	
										    </c:if>
										    <c:if test="${sawal >= 1}"> 
			   			      	  	  	  	  	<c:forEach begin="${sawal}" end="${sakhir}" var="i">
			   			      	  	  	  	  	  	<c:choose><li><a href="manageLphFwd.do?page=${i}" class="page-link">${i}</a></li></c:choose>
			   			      	  	  	  	  	</c:forEach>
		   			      	  	  	  	  	</c:if>
		   			      	  	  	  	  	<c:if test="${currentPage lt noOfPages}">
		   			      	  	  	  	  	   <li class="page-item">
									          	  <a href="manageLphFwd.do?page=${currentPage + 1}" class="page-link">Next</a>
									         </li>
											 </c:if>
											 <c:if test="${noOfPages - 9 > 0}">
											 	<li class="page-item">
								       				<a href="manageLphFwd.do?page=${currentPage - 9}"class="page-link">Last</a>
								       			</li>
								       		 </c:if>
										  </ul>
										</nav>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<c:if test="${sawal >= 1}">Search Result - ${noOfRecords} Result</c:if>
						</td>
					</tr>
					<tr>
						<td>
							<table class="table table-striped" style="width: 1075px;">
							<thead>
								<tr align="center">
									<th style="width:30px; font-size: 10px; font-style: family font sains"><b>#</b></th>
									<th style="width:120px; font-size: 12px;" ><b>ACCOUNT NO</b></th>
									<th style="width:500px; font-size: 12px;"><b>NAME</b></th>
									<th style="width:90px; font-size: 12px;"><b>CUST NO</b></th>
									<th style="width:50px; font-size: 12px;"><b>CCY</b></th>
									<th style="width:120px; font-size: 12px;"><b>ACC CLASS</b></th>
									<th style="width:150px; font-size: 12px;"><b>NO.KTP</b></th>
									<th style="width:200px; font-size: 12px;"><b>TANGGAL LAHIR</b></th>
									<th style="width:350px; font-size: 12px;"><b>PERAGAAN</b></th>
								</tr>
							</thead>
							<tbody>
								<c:set var="inc" value="0" />
								<c:forEach var="lph" items="${lph}" >
								<tr>
									<c:set var="inc" value="${inc + 1}" />
									<td style="font-size: 10px">${inc}</td>
									<td style="font-size: 10px">${lph.cust_ac_no}</td>
									<td style="font-size: 10px">${lph.ac_desc}</td>
									<td style="font-size: 10px">${lph.cust_no}</td>
									<td style="font-size: 10px">${lph.ccy}</td>
									<td style="font-size: 10px">${lph.account_class}</td>
									<td style="font-size: 10px">${lph.UNIQUE_ID_VALUE}</td>
									<td style="font-size: 10px">${lph.DATE_OF_BIRTH}</td>
									<td >
										<a href="/ReportMCB/vPeragaanCustD.do?norek=${lph.cust_ac_no}" class="btn btn-success" role="button"> Detail <span class="glyphicon glyphicon-list-alt "></span></a>
										<a href="/ReportMCB/vPeragaanHistD.do?ac=${lph.cust_ac_no}&nm=${lph.ac_desc}" class="btn btn-info" role="button"> History <span class="glyphicon glyphicon-user"></span></a>
<%-- 										<a href="/ReportMCB/vPeragaanCustD.do?norek=${lph.cust_ac_no}" target="_blank">Rekening</a> &nbsp; --%>
<%-- 										<a href="/ReportMCB/vPeragaanHistD.do?ac=${lph.cust_ac_no}&nm=${lph.ac_desc}" target="_blank">History </a> --%>
									</td>
		<!-- 							<td><input type="checkbox" id="singleOrMulti" onclick="tes()"> </td> -->
		<!-- 							<td><button class="w2ui-btn" onclick="openPopup()">Popup</button></td> -->
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	
<!-- <script src="plugins/jQuery/jquery-2.2.3.min.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> -->
<script src="plugins/fastclick/fastclick.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $(document).ready(function(){	
    	$('#example2').DataTable( {
    	        "scrollX": true
    	    } );
    	} );
	
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "Tanggal Lahir"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

  });
 </script>
</body>
</html>