<%-- 
    Document   : login
    Created on : Oct 13, 2010, 3:31:42 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<script type='text/javascript' src='/MuamalatFrontEndPayment/scripts/common.js'></script>
<script type="text/javascript" src="/ReportMCB/scripts/css3-mediaqueries.js"></script>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/ReportMCB/scripts/selectivizr.js"></script>
<fieldset style="width:300px; height:160px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; margin: 20px;">
    <legend style="background:#fff;" align="center"><b>LOGIN</b></legend>
	<form method="POST" action="LoginProcess.do" name="jurnal">
    	<table border="0" width="250" align="center">
    		<tbody>
        	<tr>
        		<td colspan="2">
        			<input type="text" name="username" maxlength="20" class="inputs" placeholder="username" onkeypress="return checkValidChar(event)" style=" width : 280px;"/> 
        		</td>		
<!-- 	            <td width="140">User</td> -->
<!-- 	            <td><input type="text" name="username" size="20" maxlength="20" value="" class="normalInput" onkeypress="return checkValidChar(event)" /></td> -->
        	</tr>
        	<tr>
        		<td colspan = "2">
        			<input type="password" name="password" maxlength="16" class="inputs"placeholder="Password" onkeypress="return checkValidChar(event)" style=" width : 280px;"/> 
        		</td>
<!-- 	            <td>Password</td> -->
<!-- 	            <td><input type="password" name="password"  size="20" maxlength="16" value="" class="normalInput" onkeypress="return checkValidChar(event)" /></td>             -->
       		 </tr>
       		 <tr>
	            <td>
	                &nbsp;
	                <input type="hidden" name="level" value="${level}">
	            </td>
	            <td align="right"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Login</a></td>
<!--             	<td align="right"><input type="submit" value="Login" /></td> -->
       	 	</tr>
    		</tbody>
		</table>
	</form>
</fieldset>
<br />
    <div align="center"><font color="red">${message}</font></div>
