<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
	<head>
		<style type="text/css">
		            body {font-family:times new roman;}
		            .td1 {width:150px;}
		            .td2 {width:5px;}
		</style>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title></title>
	</head>
	<body>
	<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <table border="0" width="100%">
            <tr>
                <td align="center" colspan="3"><b>${title} </b></td>
            </tr>
           
            <tr>
                <td></td>
                <td>:</td>
            </tr>
        </table>
            <br />    			
       			<div style="  float:left; text-align: left; ">
       				<table>
       				<tr>
       					<td>
       						<a href="manageLBatch.do?page=1">First</a>
       					</td>
       					<td>
       						<c:if test="${currentPage != 1}">
						        <a href="manageLBatch.do?page=${currentPage - 1}">Previous</a>
						    </c:if>
       					</td>
       					<td>
	       						<%--For displaying Page numbers.
						    The when condition does not display a link for the current page--%>
						    <table border="0" cellpadding="5" cellspacing="5">
						        <tr>
						            <c:forEach begin="${sawal}" end="${sakhir}" var="i">
						                <c:choose><td>
						                <a href="manageLBatch.do?page=${i}">${i}</a></td>
						                </c:choose>
						            </c:forEach>
						        </tr>
						    </table>						    
       					</td>
       					<td>
       						<c:if test="${currentPage lt noOfPages}">
						        <a href="manageLBatch.do?page=${currentPage + 1}">Next</a>
						    </c:if>
       					</td>
       					<td>
       						<c:if test="${noOfPages - 9 > 0}">
       						 <a href="manageLBatch.do?page=${noOfPages - 9}">Last</a>
       						</c:if>
       					</td>
       				</tr>
       			</table>
       			</div>
       			<div style="width:400px;    float:right; text-align: right">
       			<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
       			</div>
				    <br />
       			<div style="font-size:10px; font-family:times new roman; float:left;">   
				    <table id="rounded-corner" style="width: 100%">
					    <thead>
					        <tr align="center">
					            <th style="width:60px; font-size: 12px;" ><b>TGL TRN</b></th>
					            <th style="width:110px; font-size: 12px;"><b>NO REFERENSI</b></th>
					             <th style="width:60px; font-size: 12px;"><b>AC NO</b></th>
					            <th style="width:60px; font-size: 12px;"><b>SUB ACCOUNT</b></th>
					            <th style="width:60px; font-size: 12px;"><b>NO BATCH</b></th>
					            <th style="width:40px; font-size: 12px;"><b>CABANG</b></th>
					           <th style="width:70px; font-size: 12px;"><b>MAKER ID</b></th>
					            <th style="width:100px; font-size: 12px;"><b>AUTHORIZE ID</b></th>
					            <th style="width:50px; font-size: 12px;"><b>MODULE</b></th>
					            <th style="width:350px; font-size: 12px;"><b>STATUS</b></th>
					            <th style="width:100px; font-size: 12px;"><b>JUMLAH</b></th>
					            <th style="width:100px; font-size: 12px;"><b>MUTASI DRCR</b></th>
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="lBatchM" items="${lBatchM}" >
						        <tr>
						            <td style="width:70px;">${lBatchM.trn_dt}</td>
						            <td >${lBatchM.trn_ref_no}</td>						          
						            <td >${lBatchM.ac_no}</td> 
						             <td >${lBatchM.RELATED_ACCOUNT}</td>
						            <td >${lBatchM.batch_no}</td>
						            <td >${lBatchM.branch_code}</td>
						         	<td >${lBatchM.user_id}</td> 
						            <td >${lBatchM.auth_id}</td>
						            <td >${lBatchM.module}</td>
						           <td >${lBatchM.auth_stat}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lBatchM.cr_ent_total}" /></td>
	 									 <td >${lBatchM.drcr_ind}</td>
						        </tr>
						    </c:forEach>
					    </tbody>
					
				      </table>
				</div>
		</body>
</html>
