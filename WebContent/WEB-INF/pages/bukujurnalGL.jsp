<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
	<head>
		<style type="text/css">
		            body {font-family:times new roman;}
		            .td1 {width:150px;}
		            .td2 {width:5px;}
		</style>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title></title>
	
	</head>
	<body>
	<logic:equal name="view" value="data">
	
	<table border="0" width="100%">
		<tr>		
		<td><a href="viewBukuJurnal.do?idreport=R007"><span>Buku Besar GL</span></a></td>
        </tr>
    </table>
        <table border="0" width="100%">
            <tr>
                <td align="center" colspan="3"><b>${title} </b></td>
            </tr>
<!--            <tr>
                <td width="150px;">Rekening</td>
                <td width="2px;">:</td>
                <td>${saldo.account} / ${saldo.ac_name}</td>
            </tr>
            <tr>
                <td>Kantor/Cabang</td>
                <td>:</td>
                <td>${saldo.branch_code}</td>
            </tr>
            <tr>
                <td>Valuta</td>
                <td>:</td>
                <td>${saldo.acc_ccy}</td>
            </tr>
            <tr>
                <td>Saldo Awal Tanggal</td>
                <td>:</td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${saldo.acy_opening_bal}" /> &nbsp;Saldo Eq&nbsp;
                    <fmt:formatNumber type="number" maxFractionDigits="2" value="${saldo.lcy_opening_bal}" />
                </td>
            </tr>
            <tr>
                <td>Saldo Akhir Tanggal</td>
                <td>:</td>
                <td><fmt:formatNumber type="number" maxFractionDigits="2" value="${saldoakhir}" /></td>
            </tr>
-->            
        </table>
            <br />    			
       			<div style="  float:left; text-align: left; ">
       				<table border="0">
       				<tr>
       					<td>
       						<a href="manageLbbGLFwd.do?page=1">First</a>
       					</td>
       					<td>
       						<c:if test="${currentPage != 1}">
						        <a href="manageLbbGLFwd.do?page=${currentPage - 1}">Previous</a>
						    </c:if>
       					</td>
       					<td>
	       						<%--For displaying Page numbers.
						    The when condition does not display a link for the current page--%>
						    <table border="0" cellpadding="5" cellspacing="5">
						        <tr>
						        
						            <c:forEach begin="${sawal}" end="${sakhir}" var="i">
						                <c:choose><td>
						                <a href="manageLbbGLFwd.do?page=${i}">${i}</a></td>
						                </c:choose>
						            </c:forEach>
						        </tr>
						    </table>
       					</td>
       					<td>
       						<c:if test="${currentPage lt noOfPages}">
						        <a href="manageLbbGLFwd.do?page=${currentPage + 1}">Next</a>
						    </c:if>
       					</td>
       					<td>
       						<c:if test="${noOfPages - 9 > 0}">
       						 <a href="manageLbbGLFwd.do?page=${noOfPages - 9}">Last</a>
       						</c:if>
       					</td>
       				</tr>
       			</table>
       			</div>
       			<div style="width:400px;    float:right; text-align: right">
       			<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
       			</div>
				    <br />
       			<div style="font-size:8px; font-family:times new roman; float:left;">  
				    <table id="rounded-corner" style="width: 100%">
					    <thead>
					        <tr align="center">
					            <th style="width:60px; font-size: 12px;" ><b>TGL TRN</b></th>
					            <th style="width:65px; font-size: 12px;"><b>VALUE DATE</b></th>
					            <th style="width:60px; font-size: 12px;"><b>CABANG</b></th>
					            <th style="width:110px; font-size: 12px;"><b>NO REFERENSI</b></th>
					            <th style="width:60px; font-size: 12px;"><b>NO BATCH</b></th>
					            <th style="width:60px; font-size: 12px;"><b>RCC NO</b></th>
					            <th style="width:60px; font-size: 12px;"><b>AC NO</b></th>
					            <th style="width:40px; font-size: 12px;"><b>KURS</b></th>
					            <th style="width:50px; font-size: 12px;"><b>EVENT</b></th>
					            <th style="width:70px; font-size: 12px;"><b>MAKER ID</b></th>
					            <th style="width:100px; font-size: 12px;"><b>AUTHORIZE ID</b></th>
					            <th style="width:50px; font-size: 12px;"><b>TRN CODE</b></th>
					            <th style="width:500px; font-size: 12px;"><b>DESKRIPSI</b></th>
					            <th style="width:100px; font-size: 12px;"><b>MUTASI DR</b></th>
					            <th style="width:100px; font-size: 12px;"><b>MUTASI CR</b></th>
								<logic:notEqual name="viewccy" value="IDR">
						            <th style="width:300px; font-size: 12px;"><b>EKIVALEN DR</b></th>
						            <th style="width:300px; font-size: 12px;"><b>EKIVALEN CR</b></th>
								</logic:notEqual>					            
					            <th style="width:100px; font-size: 12px;"><b>SALDO</b></th>					            
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="lbukbes" items="${lbukbes}" >
						        <tr valign="top">
						            <td style="width:70px;">${lbukbes.trn_dt}</td>
						            <td >${lbukbes.value_dt}</td>
						            <td >${lbukbes.ac_branch}</td>
						            <td >${lbukbes.trn_ref_no}</td>
						            <td >${lbukbes.batch_no}</td>
						            <td >${lbukbes.rcc_no}</td>
						            <td >${lbukbes.ac_no}</td>
						            <td >${lbukbes.ac_ccy}</td>
						            <td >${lbukbes.event}</td>
						            <td >${lbukbes.maker_id}</td>
						            <td >${lbukbes.auth_id}</td>
						            <td >${lbukbes.trn_code}</td>
						            <td >${lbukbes.ket}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lbukbes.amt_db}" /></td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lbukbes.amt_cr}" /></td>
						            
									<logic:notEqual name="viewccy" value="IDR">
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lbukbes.amt_db_eq}" /></td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lbukbes.amt_cr_eq}" /></td>
									</logic:notEqual>
						            
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lbukbes.saldo}" /></td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
				</div>
	</logic:equal>
	<logic:equal name="view" value="nodata">
		${teks}
	</logic:equal>
	</body>
</html>
