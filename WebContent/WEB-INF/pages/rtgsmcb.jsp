<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type='text/javascript' src='/ReportMCB/scripts/money/jquery.js' ></script>
<script type="text/javascript">
$(function() {
    $( "#bsnsdt" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy'
    });
	document.getElementById("type_trns").disabled=true;
});

function valInput(){
    var bsnsdt;
    bsnsdt = document.getElementById('bsnsdt').value;

    if (bsnsdt == ""){
        alert('Tanggal tidak boleh kosong');
        return false;
    }
    return true;
}


function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
function dis(){
	document.getElementById("type_trns").disabled=false;
	document.getElementById("idtmmbr").disabled=true;
	document.getElementById("idtxnsts").disabled=true;
	document.getElementById("contractno").disabled=true;
	document.getElementById("acc").disabled=true;
}
function ena(){
	document.getElementById("type_trns").disabled=true;
	document.getElementById("idtmmbr").disabled=false;
	document.getElementById("idtxnsts").disabled=false;
	document.getElementById("contractno").disabled=false;
	document.getElementById("acc").disabled=false;
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div style="font-size: 11px;">
<table border="0" align="center">
	<tr>
		<td>			
			<fieldset style="width:780px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			<legend> <b>RTGS MCB</b> </legend>
			<form action="rtgsMcbView.do" method="POST" name="jurnal">
				<table border="0">
					<tr>
						<td colspan="10">
							<input type="radio" name="rtgsmcb" value="out" onclick="ena()" checked="checked">RTGS Outgoing &nbsp;
							<input type="radio" name="rtgsmcb" value="in" onclick="dis()">RTGS Incoming
						</td>
					</tr>
					<tr>
						<td>Bussiness Date</td>
						<td>:</td>
						<td><input type="text" name="bsnsdt" id="bsnsdt" readonly="true"></td>						
						<td>No. Contract</td>
						<td>:</td>
						<td><input type="text" name="contractno" id="contractno" maxlength="20"></td>
						<td>Type</td>
						<td>:</td>
						<td>
							<select name="type_trns" id="type_trns">
							  <option value="">All</option>
							  <option value="411">RTGS INCOMING</option>
							  <option value="4D1">RTGS RETUR</option>
							  <option value="4E1">RTGS TITIPAN</option>
							</select>
						</td>	
					</tr>
					<tr>
						<td width="90px;">Branch</td>
						<td>:</td>
						<td width="180px;"><input type="text" name="kdcab" id="kdcab" maxlength="3"></td>			
						<td>Cust Ac No</td>
						<td>:</td>
						<td><input type="text" name="cust_ac_no" id="cust_ac_no" maxlength="10"></td>	
						<td>Amount From</td>
						<td>:</td>
						<td>
							<input type="text" name="amtf" id="amtf" maxlength="17">
							<script type="text/javascript">$("#amtf").maskMoney();</script>
						</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>:</td>
						<td><select name="txnsts" id="idtxnsts">
							  <option value="">All</option>
							  <option value="C">PROCEED</option>
							  <option value="O">UNPROCEED</option>
							</select></td>							
						<td>To Account</td>
						<td>:</td>
						<td><input type="text" name="acc" id="acc" maxlength="24">
						</td>							
						<td>Amount To</td>
						<td>:</td>
						<td><input type="text" name="amtto" id="amtto" maxlength="17">
							<script type="text/javascript">$("#amtto").maskMoney();</script>
						</td>
					</tr>
					<tr>
						<td>No. Reference</td>
						<td>:</td>
						<td><input type="text" name="reltrn" id="idreltrn" maxlength="16"></td>
						<td>To Member</td>
						<td>:</td>
						<td>
							<select name="tmmbr" id="idtmmbr" >
		                        <option value="">Pilih</option>
		                        <c:forEach items="${lmmbrbnk}" var="lmmbrbnk">
		                             <option value=${lmmbrbnk.codename}>${lmmbrbnk.codename}</option>
		                        </c:forEach>
		                    </select>
						</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="9">
							<input onclick="return valInput()"  type="submit" value="Lihat" name="btn" id="idbtn" />
							<input onclick="return valInput()" type="submit" value="Download" name="btn" id="download" />
						</td>
						
					</tr>
				</table>
			</form>
			</fieldset>
		</td>
	</tr>
</table>
<logic:equal name="respon" value="teks">
${teks }
</logic:equal>

<logic:equal name ="data" value="out">
<table border="0" align="center" width="1075px;">
	<tr>
		<td>
			
	<div style="  float:left; text-align: left; ">
	<table border="0">
		<tr>
	    	<td><a href="manageRtgsMcbFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="manageRtgsMcbFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="manageRtgsMcbFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="manageRtgsMcbFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageRtgsMcbFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style="width:500px;    float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr align="center" style="font-size: 10px;">
					            <th style="width:15px; "><b>BRANCH</b></th>
					            <th style="width:20px; "><b>CONTRACT NO</b></th>
					            <th style="width:20px; "><b>NO. REFF</b></th>
					            <th style="width:30px;"><b>CUST AC NO</b></th>
					            <th style="width:300px; "><b>CUST NAME</b></th>
					            <th style="width:70px; "><b>CPTY AC NO</b></th>
					            <th style="width:250px; "><b>CPTY AC NAME</b></th>
					            <th style="width:10px; "><b>BANK CD</b></th>
					            <th style="width:500px; "><b>DETAILS</b></th>
					            <th style="width:40px; "><b>AMOUNT</b></th>
					            <th style="width:60px; "><b>DISPATCH REFF NO</b></th>	
					            <th style="width:15px; "><b>EXCEPTION QUEUE</b></th>
					            <th style="width:30px; "><b>EVENT STS</b></th>						            
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="ltrans" items="${ltrans}" >
						        <tr style="font-size: 10px; ">
						            <td >${ltrans.branch_code}</td>
						            <td >${ltrans.contract_ref_no}</td>
						            <td >${ltrans.ac_entry_ref_no}</td>
						            <td >${ltrans.cust_ac_no}</td>
						            <td >${ltrans.cust_name}</td>
						            <td >${ltrans.cpty_ac_no}</td>
						            <td >${ltrans.cpty_name}</td>
						            <td >${ltrans.cpty_bankcode}</td>
						            <td >${ltrans.remarks}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrans.txn_amount}" /></td>
						            <td >${ltrans.dispatch_ref_no}</td>
						            <td >${ltrans.exception_queue}</td>
						            <td >${ltrans.eventdesc}</td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>			
		</td>
	</tr>
</table>
</logic:equal>


<logic:equal name ="data" value="in">
<table border="0" align="center" width="1075px;">
	<tr>
		<td>
			
	<div style="  float:left; text-align: left; ">
	<table border="0">
		<tr>
	    	<td><a href="manageRtgsMcbFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="manageRtgsMcbFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="manageRtgsMcbFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="manageRtgsMcbFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageRtgsMcbFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style="width:500px;    float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr align="center" style="font-size: 10px;">
					            <th style="width:20px; "><b>NO</b></th>
					            <th style="width:40px; "><b>TRN DT</b></th>
					            <th style="width:30px; "><b>BRANCH</b></th>
					            <th style="width:40px; "><b>NO. REFF</b></th>
					            <th style="width:60px; "><b>CUST AC NO</b></th>
					            <th style="width:100px;"><b>TYPE</b></th>
					            <th style="width:40px; "><b>AMOUNT</b></th>
					            <th style="width:500px; "><b>DETAILS</b></th>	
					            <th style="width:40px; "><b>USER ID</b></th>
					            <th style="width:40px; "><b>AUTH ID</b></th>			            
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="ltrans" items="${ltrans}" >
						        <tr style="font-size: 10px; ">
						            <td >${ltrans.inc}</td>
						            <td >${ltrans.bookdt}</td>
						            <td >${ltrans.branch_code}</td>
						            <td >${ltrans.ac_entry_ref_no}</td>
						            <td >${ltrans.cust_ac_no}</td>
						            <td >${ltrans.trn_desc}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrans.txn_amount}" /></td>
						            <td >${ltrans.remarks}</td>
						            <td >${ltrans.maker_id}</td>
						            <td >${ltrans.checker_id}</td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>			
		</td>
	</tr>
</table>
</logic:equal>
</div>
</body>
</html>