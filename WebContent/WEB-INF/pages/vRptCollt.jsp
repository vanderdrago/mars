<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Collateral Detail</title>
</head>
<body>
<table width="100%" border="0">
	<tr >
		<td align="center">
			<logic:equal name="viewudf" value="udfcol">
			<fieldset style="width:1075px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    	<legend> <b>User Defined Fields Collateral</b> </legend>
			<table width="1075px" border="1" >
				<thead>
					<tr>
		            	<td colspan="2">Collateral Code : ${colcod}</td>
		            </tr>
					<tr style=" background-color: #66CCCC;" align="center">
			        	<td width="500px" style="font-size: 15px;" ><b>FIELD NAME</b></td>
				        <td width="350px" style="font-size: 15px;" ><b>VALUE </b></td>
			        </tr>
				</thead>	
			    <tbody>
			    	<c:forEach var="colrn" items="${colrn}" >
				        <tr>
				            <td >${colrn.field_name}</td>
				            <td >${colrn.value}</td>
				        </tr>
				    </c:forEach>
			    </tbody>
			</table>
			</fieldset>
		</logic:equal>
		</td>
	</tr>
</table>
</body>
</html>