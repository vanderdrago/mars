<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function valtgl(){
    var tgl1;
    var tgl2;
    tgl1 = document.getElementById('tgl1').value;
    tgl2 = document.getElementById('tgl2').value;

    if (tgl1 == "" && tgl2 == ""){
        alert('Silahkan isi tanggal');
        return false;
    }
    return true;
}

$(function() {
    $( "#tgl1" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
    $( "#tgl2" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy',
        changeYear: true
    });
});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>
<html>
<script type="text/javascript">
$(function() {
    $( "#tgl1" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy'
    });
    $( "#tgl2" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy'
    });
});
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Peragaan History</title>
</head>
<body>
<table border="0" width="100%">
	<tr>
		<td align="center">
		<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    <legend> <b>PERAGAAN HISTORY</b> </legend>
	    <form action="vHistTransMarketing.do" method="POST" name="jurnal">
	    <table border="0">
	    	<tr>
	    		<td>
		    		<table>
		    			<tr>
		    				<td>No. Rekening</td>
		    				<td>:</td>
		    				<td><b>${norek}</b></td>
		    			</tr>
		    			<tr>
		    				<td>Nama</td>
		    				<td>:</td>
		    				<td><b>${nama}</b></td>
		    			</tr>
		    		</table>
	    		</td>
	    	</tr>
	        <tr>
	            <td><input type="text" name="tgl1" id="tgl1" class="inputs" placeholder="Silahkan Pilih Mulai Tanggal (DD-MM-YYYY)" style=" width : 400px;"></td>
	        </tr>
	        <tr>
	            <td><input type="text" name="tgl2" id="tgl2" class="inputs" placeholder="Silahkan Pilih Hingga Tanggal (DD-MM-YYYY)" style=" width : 400px;"></td>
	        </tr>
	        <tr>
	            <td align="center">
<!-- 	                <input onclick="return valtgl()" type="submit" value="Search" name="btn" id="ok" class="blue btn" /> -->
	                <a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a>
	            </td>
	        </tr>
            <tr>
            	<td colspan="3">
            		<logic:equal name="confrm" value="err">
					<p style="color: red;"> ${respon} </p>
					</logic:equal>	
            	</td>
            </tr>
	    </table>
	    </form>
		</fieldset>
		</td>
	</tr>
	<tr style="color: black;">
		<td align="center">
			<logic:equal name="vht" value="ht">
		<br />
		<table width="1075px">
			<tr>
				<td width="100">Periode</td>
				<td width="5">:</td>
				<td>${period}</td>
			</tr>
			<tr>
				<td>Saldo Awal</td>
				<td>:</td>
				<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${saldoawal}" /> 
<!-- 				&nbsp; ~ &nbsp; -->
<%-- 				<fmt:formatNumber type="number" maxFractionDigits="2" value="${saldoawaleki}" /> --%>
				</td>
			</tr>
		</table>
		<table id="rounded-corner" style="width: 1075px;">
					<thead>
						<tr align="center">
							<th style="width:20px; font-size: 12px;"><b>NO.</b></th>
							<th style="width:150px; font-size: 12px;"><b>NO. REF</b></th>
							<th style="width:60px; font-size: 12px;" ><b>TRN DATE</b></th>
<!-- 							<th style="width:50px; font-size: 12px;" ><b>JAM TRN</b></th> -->
							<th style="width:60px; font-size: 12px;"><b>VALUE DATE</b></th>
							<th style="width:50px; font-size: 12px;"><b>TRN CODE</b></th>
							<th style="width:700px; font-size: 12px;"><b>DESC</b></th>
							<th style="width:15px; font-size: 12px; text-align: center;"><b>D/C</b></th>
							<th style="width:150px; font-size: 12px; text-align: right;"><b>NOMINAL</b></th>
							<th style="width:150px; font-size: 12px; text-align: right;"><b>SALDO</b></th>
							<th style="width:100px; font-size: 12px; text-align: right;"><b>EKIVALEN</b></th>
							<th style="width:50px; font-size: 12px; text-align: center;"><b>STATUS</b></th>
<!-- 							<th style="width:50px; font-size: 12px; text-align: center;"><b>TXN_DT_TIME</b></th> -->
						</tr>
					</thead>
					<tbody>
						<c:set var="inc" value="0" />
						<c:forEach var="lhist" items="${lhist}" >
						<tr style="font-size: 11px;">
							<c:set var="inc" value="${inc + 1}" />
							<td >${inc}</td>
							<td >${lhist.trn_ref_no}</td>
							<td >${lhist.trn_dt}</td>
<%-- 							<td >${lhist.jam}</td> --%>
							<td >${lhist.val_dt}</td>
							<td >${lhist.trn_code}</td>
							<td >${lhist.desc}</td>
							<td style="text-align: center;">${lhist.drcr}</td>
							<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lhist.nominal}" /></td>
							<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lhist.saldo}" /></td>
							<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lhist.ekivalen}" /></td>
							<td style="text-align: center;">${lhist.auth_stat}</td>
<%-- 							<td style="text-align: center;">${lhist.txn_dt_time}</td> --%>
						</tr>
						</c:forEach>
					</tbody>
				</table>
	</logic:equal>
		</td>
	</tr>
</table>
	
</body>
</html>