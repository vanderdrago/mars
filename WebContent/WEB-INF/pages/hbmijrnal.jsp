<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body> 

<a href="bukujurnal.do"><span>BACK</span></a>
<table border="0" width="100%">
	<tr>
    	<td align="center" colspan="3"><b>${title} </b></td>
     </tr>
</table>

<table>
    <tr>
          <td height="60px">  </td>
          </tr>  
</table>


<div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:100%">
               <tr ><td>

<display:table name="lBatchM" id="lBatchM" class="wb" requestURI="viewhbmijurnal.do" pagesize="35" sort="external" >
<display:column title="VALUE DATE" sortable="true" style="width:70px;">${lBatchM.last_oper_dt_stamp}</display:column>
<display:column title="USER ID" sortable="true" style="width:70px;"> ${lBatchM.last_oper_id}</display:column>

<c:if test="${lBatchM.auth_stat!='A'}">
<display:column title="NO BATCH" sortable="true" style="width:100px;">-</display:column>
</c:if> 
<c:if test="${lBatchM.auth_stat=='A'}">
<display:column title="NO BATCH" sortable="true" style="width:100px;"><a href="/ReportMCB/detailHBatchAct.do?batch_no=${lBatchM.batch_no}&userId=${lBatchM.last_oper_id}&kdcab=${lBatchM.branch_code}&trndt=${lBatchM.last_oper_dt_stamp}"> ${lBatchM.batch_no}</a></display:column>
</c:if> 

<c:if test="${lBatchM.auth_stat!='A'}">
<display:column title="JENIS TRANSAKSI" sortable="true" style="width:100px;">${lBatchM.auth_stat}</display:column>
</c:if> 
<c:if test="${lBatchM.auth_stat=='A'}">
<display:column title="JENIS TRANSAKSI" sortable="true" style="width:100px;">-</display:column>
</c:if> 

<c:if test="${lBatchM.auth_stat=='A'}">
<display:column title="DESKRIPSI" sortable="true" style="width:150px;">${lBatchM.description}</display:column> 
</c:if> 
<c:if test="${lBatchM.auth_stat!='A'}">
<display:column title="DESKRIPSI" sortable="true" style="width:100px;"> ${lBatchM.batch_no}</display:column>
</c:if> 

<display:column title="CABANG" sortable="true" style="width:100px;">${lBatchM.branch_code}</display:column>
<c:if test="${lBatchM.auth_stat!='A'}">
<display:column title="STATUS" sortable="true" style="width:100px;">-</display:column>
</c:if> 
<c:if test="${lBatchM.auth_stat=='A'}">
<display:column title="STATUS" sortable="true" style="width:100px;">${lBatchM.auth_stat}</display:column>
</c:if> 
<display:column title="DEBET" sortable="true" style="width:70px;"> <fmt:formatNumber type="number" maxFractionDigits="2" value = "${lBatchM.dr_ent_total}" /> </display:column>
<display:column title="CREDIT" sortable="true" style="width:70px;"> <fmt:formatNumber type="number" maxFractionDigits="2" value = "${lBatchM.cr_ent_total}" /> </display:column>
</display:table>
</td>
</tr>
</table>
</div>


</body>
</html>