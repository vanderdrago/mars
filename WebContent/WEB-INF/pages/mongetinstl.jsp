<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<script type="text/javascript">

// function validateForm() {
//     var accno = document.forms["mgetinstl"]["accno"].value;
// // 	var accno = document.getElementById('accno').value;
// // 	var cusid = document.getElementById('cusid').value;
// // 	var cusname = document.getElementById('cusname').value; 
// 	var status = document.getElementById('status').value;
// //     if ((accno == "") && (cusid == "") && (cusname == "")) {
// //         alert('Installment No Atau Customer Id Atau Customer Name Wajib Di Isi');
// //         return false;
// //     } 
//     if ((accno == "")){
//         return false;
//     } 
// //     if ((status == "")){
// // 		alert"status"
// // 		return false;
// //     }
//     return true;
// }

function validateForm (){   

	var accno = document.forms["mgetinstl"]["accno"].value;
	var cusid = document.forms["mgetinstl"]["cusid"].value;
	var cusname = document.forms["mgetinstl"]["cusname"].value;
	var status = document.forms["mgetinstl"]["status"].value;

	if ((accno == "") && (cusid == "") && (cusname == "")){
		alert ("Account Number No Empty");
		return false;
	}
	if ((status == "Choose Account Status...")){
		alert ("Choose Account Status");
		return false;
	}
    return true;
}


// $(function () {
//     $('.input-group-addon.beautiful').each(function () {
        
//         var $widget = $(this),
//             $input = $widget.find('input'),
//             type = $input.attr('type');
//             settings = {
//                 checkbox: {
//                     on: { icon: 'fa fa-check-circle-o' },
//                     off: { icon: 'fa fa-circle-o' }
//                 },
//                 radio: {
//                     on: { icon: 'fa fa-dot-circle-o' },
//                     off: { icon: 'fa fa-circle-o' }
//                 }
//             };
            
//         $widget.prepend('<span class="' + settings[type].off.icon + '"></span>');
            
//         $widget.on('click', function () {
//             $input.prop('checked', !$input.is(':checked'));
//             updateDisplay();
//         });
            
//         function updateDisplay() {
//             var isChecked = $input.is(':checked') ? 'on' : 'off';
                
//             $widget.find('.fa').attr('class', settings[type][isChecked].icon);
                
//             //Just for desplay
//             isChecked = $input.is(':checked') ? 'checked' : 'Liquidate';
//             $widget.closest('.input-group').find('input[type="text"]').val('' + isChecked)
//         }
        
//         updateDisplay();
//     });
// });
</script>


<head>
<meta http-equiv="Content-Type:application/font-woff" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/bootstrap/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/ReportMCB/sorting/css/styles.css" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/ReportMCB/sorting/js/jquery-1.10.2.min.js"></script>
<script src="/ReportMCB/scripts/js/jquery.tablesorter.min.js"></script>

<title>Installment</title>

</head>
<body>
	<table width="1075px" border="0">
		<tr>
			<td align="center">
		        <form action="vGetInstl.do" method="POST" name="mgetinstl" onsubmit="return validateForm()">	
		        	<fieldset>
		        		<legend>Fields Installment Query</legend>
						    <div class="box-body">
						    	<div class="row">
						    		<div class="col-xs-5">
										<input class="form-control" placeholder="Installment No" type="text" name="accno" id = "accno" >
									</div>
						    	</div>
						    	<br>
						    	<div class="row">
						    		<div class="col-xs-5">
										<input class="form-control" placeholder="Customer ID" type="text" name="cusid" id = "cusid" >
									</div>
						    	</div>
						    	<br>
						    	<div class="row">
						    		<div class="col-xs-5">
										<input class="form-control" placeholder="Customer Name" type="text" name="cusname" id = "cusname" >
									</div>
						    	</div>
						    	<br>
						    	<div class="row">
						    		<div class="col-xs-5">
								        <select class="form-control" name="status" >
								        	<option selected>Choose Account Status...</option>
								        	<option value="all" name="all" id="all">All</option>
										    <option value="active" name="active" id="active">Active</option>
										    <option value="liquidate" name="liquidate" id="liquidate">Liquidate</option>
										</select>
									</div>
						    	</div>
						    	<br>
						        <div class="row">
						    		<div class="col-xs-5" align="left">
						    			<button type="submit" class="btn btn-success" onclick="return validateForm()">Search</button>
									</div>
						    	</div>
							</div>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="1075px" border="0">
					<tr>
						<td>
							<table border="0">
								<tr>
									<td>
										<c:if test="${sawalAngsuran >= 1}">Page ${sawalAngsuran} Of ${noOfPagesAngsuran}</c:if>
									</td>
									<td>
										<nav aria-label="...">
										  <ul class="pagination">
										  	<c:if test="${currentPageAngsuran != 1 && (currentPageAngsuran - 1 > 0)}">
											  <li class="page-item">
											  	<a href="ManageGetInstlFwd.do?page=${currentPageAngsuran - 1}" class="page-link">
											  		<span class="page-link">Previous</span>
											  	</a>
											  </li>
										    </c:if>
										    <c:if test="${sawalAngsuran >= 1}">
										    	<li class="page-item">
										    		<a href="ManageGetInstlFwd.do?page=1" class="page-link">First</a>
										    	 </li>	
										    </c:if>
										    <c:if test="${sawalAngsuran >= 1}"> 
			   			      	  	  	  	  	<c:forEach begin="${sawalAngsuran}" end="${sakhirAngsuran}" var="i">
			   			      	  	  	  	  	  	<c:choose><li><a href="ManageGetInstlFwd.do?page=${i}" class="page-link">${i}</a></li></c:choose>
			   			      	  	  	  	  	</c:forEach>
		   			      	  	  	  	  	</c:if>
		   			      	  	  	  	  	<c:if test="${currentPageAngsuran lt noOfPagesAngsuran}">
		   			      	  	  	  	  	   <li class="page-item">
									          	  <a href="ManageGetInstlFwd.do?page=${currentPageAngsuran + 1}" class="page-link">Next</a>
									         </li>
											 </c:if>
											 <c:if test="${noOfPagesAngsuran - 9 > 0}">
											 	<li class="page-item">
								       				<a href="ManageGetInstlFwd.do?page=${currentPageAngsuran - 9}"class="page-link">Last</a>
								       			</li>
								       		 </c:if>
										  </ul>
										</nav>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<c:if test="${sawalAngsuran >= 1}">Search Result - ${noOfRecordsAngsuran} Result</c:if>
						</td>
					</tr>
					<tr>
						<td>
							<table class="table table-striped" id="keywords">
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 10px; font-style: family font sains"><b>NO</b></th>
										<th style="width:80px; font-size: 10px;"><b>CUSTOMER ID</b></th>
										<th style="width:60px; font-size: 10px;"><b>PROD CODE</b></th>
										<th style="width:100px; font-size: 10px;"><b>PROD CATEGORY</b></th>
										<th style="width:60px; font-size: 10px;"><b>BOOK DATE</b></th>
										<th style="width:60px; font-size: 10px;"><b>MATURITY DATE</b></th>
										<th style="width:60px; font-size: 10px;"><b>INSTALMENT NO</b></th>
										<th style="width:70px; font-size: 10px;"><b>ACC NO</b></th>
										<th style="width:50px; font-size: 10px;"><b>ACC NAME</b></th>
										<th style="width:90px; font-size: 10px;"><b>PERIODE</b></th>
										<th style="width:90px; font-size: 10px;"><b>ACC STATUS</b></th>
										<th style="width:90px; font-size: 10px;"><b>AMOUNT PRINCIPAL</b></th>
										<th style="width:100px; font-size: 10px;"><b>AMOUNT MARGIN</b></th>
										<th style="width:100px; font-size: 10px;"><b>AMOUNT TOTAL</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="ltrnAngsuran" items="${ltrnAngsuran}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td style="font-size: 10px">${inc}</td>
										<td>${ltrnAngsuran.customer_id}</td>
										<td>${ltrnAngsuran.prod_code}</td>
										<td>${ltrnAngsuran.prod_ctgry}</td>
										<td>${ltrnAngsuran.book_date}</td>
										<td>${ltrnAngsuran.matur_date}</td>
										<td><a href="/ReportMCB/vRptInstl.do?instNo=${ltrnAngsuran.account_number}" target="_blank">${ltrnAngsuran.account_number}</a></td>
										<td>${ltrnAngsuran.prim_app_id}</td>
										<td>${ltrnAngsuran.prim_app_name}</td>
										<td>${ltrnAngsuran.no_of_installments}</td>
										<td>${ltrnAngsuran.account_status}</td>
										<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrnAngsuran.amount_disbursed}" /></td>
										<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrnAngsuran.upfront_profit_booked}" /></td>
										<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrnAngsuran.total_sale_value}" /></td>										
									</tr>
									</c:forEach>
								</tbody>
							</table>
							<logic:equal name="konfirmasi" value="err">
					    		<center><b>${dataresponse}</b></center>
							</logic:equal>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
<script type="text/javascript">
$(function(){
  $('#keywords').tablesorter(); 
});
</script>
	
</body>
</html>