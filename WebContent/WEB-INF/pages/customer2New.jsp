<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/css/demos.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,400' rel='stylesheet' type='text/css'>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="http://code.jquery.com/jquery-1.12.1.min.js"></script>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<title>DETAIL REKENING MCB</title>
</head>
<body>
	<table width="1075px" border="0">
		<tr>
			<td>
				<h2>DETAIL REKENING</h2>
  				<div class="box box-default">
		    		<div class="box-body" style="display: block;">
		    			<div class="row">
		    				<div class="col-md-2">
		    					<div class="form-group">
		    						<label>No Rekening</label><br>
		    						<input class="form-control" type="text" disabled value ="${DetailCustom.CUST_AC_NO}">
		    					</div>
		    				</div>
		    				<div class="col-md-5">
		    					<div class="form-group">
		    						<label>Nama</label><br>
		    						<input class="form-control" type="text" disabled value ="${DetailCustom.acDesc}">
		    					</div>
		    				</div>
		    				<div class="col-md-2">
		    					<div class="form-group">
		    						<label>No CIF</label><br>
		    						<input class="form-control" type="text" disabled value ="${DetailCustom.custNo}">
		    					</div>
		    				</div>
		    				<div class="col-md-1">
		    					<div class="form-group">
		    						<label>Currency</label><br>
		    						<input class="form-control" type="text" disabled value ="${DetailCustom.ccy}">
		    					</div>
		    				</div>
		    				<div class="col-md-2">
		    					<div class="form-group">
		    						<label>Jenis Rekening</label><br>
		    						<input class="form-control" type="text" disabled value ="${DetailCustom.accountClass}">
		    					</div>
		    				</div>
		    			</div>
		    		</div>
		    	</div>	
				<div class="box">
					<div class="panel-group" id="accordion">
					    <div class="panel panel-default">
					      <div class="panel-heading">
					        <h4 class="panel-title">
					          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><span class="glyphicon glyphicon-th-list"></span> Detail Rekening</a>
					        </h4>
					      </div>
					      <div id="collapse1" class="panel-collapse collapse">
					        <div class="panel-body">
					        	<div class="row">
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No Rekening</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.CUST_AC_NO}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Nama</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.acDesc}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No CIF</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.custNo}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Jenis Identitas</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.p_nationalid}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kode Produk</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.accountClass}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No Identitas</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.UNIQUE_ID_VALUE}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Nama Produk</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.desc_acc}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tgl Lahir/Tgl Pendirian</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.DATE_OF_BIRTH}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kode Valuta</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.ccy}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Ibu Kandung</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.ibu_kandung}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kode Cabang</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.branch_code}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Jalan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.jalan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Nama Cabang</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.branch_name}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>RT/RW</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.rt_rw}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tgl Buka Rekening</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.AC_OPEN_DATE}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kelurahan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kelurahan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Record Status</label><br>
				    						<c:choose>
				    							<c:when test="${DetailCustom.record_stat == 'O'}">
				    								<input class="form-control" type="text" disabled value ="OPEN">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value ="CLOSE">
				    							</c:otherwise>
				    						</c:choose> 
<%-- 				    						<input class="form-control" type="text" disabled value ="${DetailCustom.record_stat}"> --%>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kecamatan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kecamatan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tgl Tutup Rekening</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.AC_CLOSE_DATE}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kota/Kabupaten</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kota_kabupaten}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Alternate Account</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.alt_ac_no}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Provinsi</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.provinsi}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kode Marketing Account</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kode_marketing}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kode Pos</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kode_pos}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Status Dormant</label><br>
<%-- 				    						<input class="form-control" type="text" disabled value ="${DetailCustom.statDormant}"> --%>
											<c:choose>
				    							<c:when test="${DetailCustom.statDormant == 'N'}">
				    								<input class="form-control" type="text" disabled value ="NO">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value ="YES">
				    							</c:otherwise>
				    						</c:choose> 
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No. Telp</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.telephone}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Status Frozen</label><br>
				    						<c:choose>
				    							<c:when test="${DetailCustom.statFrozen == 'N'}">
				    								<input class="form-control" type="text" disabled value ="NO">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value ="YES">
				    							</c:otherwise>
				    						</c:choose> 
<%-- 				    						<input class="form-control" type="text" disabled value ="${DetailCustom.statFrozen}"> --%>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No. Hp</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.mobile_number}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Status Account No Credit</label><br>
				    						<c:choose>
				    							<c:when test="${DetailCustom.statCredit == 'N'}">
				    								<input class="form-control" type="text" disabled value ="NO">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value ="YES">
				    							</c:otherwise>
				    						</c:choose> 
<%-- 				    						<input class="form-control" type="text" disabled value ="${DetailCustom.statCredit}"> --%>
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Email</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.email}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Status Account No Debet </label><br>
				    						<c:choose>
				    							<c:when test="${DetailCustom.statDebet == 'N'}">
				    								<input class="form-control" type="text" disabled value ="NO">
				    							</c:when>
				    							<c:otherwise>
				    								<input class="form-control" type="text" disabled value ="YES">
				    							</c:otherwise>
				    						</c:choose> 
<%-- 				    						<input class="form-control" type="text" disabled value ="${DetailCustom.statDebet}"> --%>
				    					</div>
				    				</div>
				    			</div>
					        </div>
					      </div>
					    </div>
					    <div class="panel panel-default">
					      <div class="panel-heading">
					        <h4 class="panel-title">
					          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><span class="glyphicon glyphicon-user"></span> Detail Nasabah </a>
					        </h4>
					      </div>
					      <div id="collapse2" class="panel-collapse collapse">
					        <div class="panel-body">
					        	<div class="row">
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No CIF</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.custNo}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Nationality</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.nationality}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kode Cabang</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.branch_code}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Resident Status</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.resident_status}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Nama Cabang</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.branch_name}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Jenis Kelamin</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.jenis_kelamin}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tanggal Buka CIF</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.CIF_CREATION_DATE}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Agama</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.agama}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Customer Type</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.customer_type}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tanggal Kadaluarsa ID</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.tgl_kadaluarsa_id}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Customer Category</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.CUSTOMER_CATEGORY}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tanggal Lahir/Pendirian</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.DATE_OF_BIRTH}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Nama Lengkap</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.acDesc}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tanggal Kadaluarsa Passport</label><br>
				    						<input class="form-control" type="text" disabled value ="-">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Jenis Identitas</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.p_nationalid}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Pekerjaan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.pekerjaan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No Identitas</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.UNIQUE_ID_VALUE}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Bidang Pekerjaan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.bidang_pekerjaan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Ibu Kandung</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.ibu_kandung}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Status Perkawinan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.sts_perkawinan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Jalan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.jalan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Pendidikan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.pendidikan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>RT RW</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.rt_rw}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Penghasilan Tetap /Bulan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.penghsl_ttp}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kelurahan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kelurahan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Penghasilan Tidak Tetap /Bulan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.penghsl_tdk_ttp}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kecamatan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kecamatan}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Pengeluaran Tetap /Bulan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.pengeluaran_ttp}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kota/Kabupaten</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kota_kabupaten}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Rata-rata Penghasilan Tidak Tetap /Bulan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.rtrt_penghsl_tdk_ttp}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Provinsi</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.provinsi}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Info Bagaimana Memperoleh Penghasilan Tambahan</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.info_mmprolh_hsl}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Kode Pos</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.kode_pos}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Sumber Dana</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.sumber_dana}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No Telp</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.telephone}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>Tujuan Buka Rekening</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.tujuan_buka_rek}">
				    					</div>
				    				</div>
				    				<div class="col-md-6">
				    					<div class="form-group">
				    						<label>No HP</label><br>
				    						<input class="form-control" type="text" disabled value ="${DetailCustom.mobile_number}">
				    					</div>
				    				</div>
				    			</div>
					        </div>
					      </div>
					    </div>
					    <div class="panel panel-default">
					      <div class="panel-heading">
					        <h4 class="panel-title">
					          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><span class="glyphicon glyphicon-credit-card"></span> Detail Saldo</a>
					        </h4>
					      </div>
					      <div id="collapse3" class="panel-collapse collapse">
					        <div class="panel-body">
<%-- 					        	<c:forEach var="saldo" items="${saldo}"> --%>
						        	<div class="row">
					    				<div class="col-md-6">
					    					<div class="form-group">
					    						<label>Saldo Memo</label><br>
					    						<input class="form-control" type="text" disabled value ="${DetailCustom.acyCurrBalance}">
					    					</div>
					    				</div>
					    				<div class="col-md-6">
					    					<div class="form-group">
					    						<label>Saldo Hold</label><br>
					    						<input class="form-control" type="text" disabled value ="${DetailCustom.acyBlockedAmount}">
					    					</div>
					    				</div>
					    				<div class="col-md-6">
					    					<div class="form-group">
					    						<label>Saldo Minimum</label><br>
					    						<input class="form-control" type="text" disabled value ="${DetailCustom.minBalance}">
					    					</div>
					    				</div>
					    				<div class="col-md-6">
					    					<div class="form-group">
					    						<label>Saldo Efektif</label><br>
					    						<input class="form-control" type="text" disabled value ="${DetailCustom.acyAvlBal}">
					    					</div>
					    				</div>
					    				<div class="col-md-6">
					    					<div class="form-group">
					    						<label>Saldo Rata-rata</label><br>
					    						<input class="form-control" type="text" disabled value ="${DetailCustom.acy_bal}">
					    					</div>
						    			</div>
					    				<div class="col-md-12"></div>
					    				<div class="col-md-12">
					    					<div class="panel panel-default">
										    <div class="panel-heading">
										      <h4 class="panel-title">
										        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><span class="glyphicon glyphicon-credit-card"></span> Detail Saldo Hold
										        </a>
										      </h4>
										    </div>
										    <div id="collapse4" class="panel-collapse collapse">
										      <div class="panel-body">
										      	<div class="row">
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>No Rekening</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.CUST_AC_NO}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>Nama</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.acDesc}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>No CIF</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.custNo}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>Kode Produk</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.accountClass}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>Amount</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.amount}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>Remarks</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.remarks}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>Effective Date</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.effectiveDate}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>Expiry Date</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.expiryDate}">
								    					</div>
								    				</div>
								    				<div class="col-md-6">
								    					<div class="form-group">
								    						<label>Mod No</label><br>
								    						<input class="form-control" type="text" disabled value ="${DetailCustom.modNo}">
								    					</div>
								    				</div>
								    			</div>
										      </div>
										    </div>
										  </div>
					    				</div>
					    				</div>
<%-- 					    			</c:forEach> --%>
<!-- 					    			<div class="panel panel-default"> -->
<!-- 									    <div class="panel-heading"> -->
<!-- 									      <h4 class="panel-title"> -->
<!-- 									        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> -->
<!-- 									        Collapsible Group 3</a> -->
<!-- 									      </h4> -->
<!-- 									    </div> -->
									   
					    			
<!-- 					    			 <div id="collapse4" class="panel-collapse collapse"> -->
<!-- 									      <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, -->
<!-- 									      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad -->
<!-- 									      minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea -->
<!-- 									      commodo consequat.</div> -->
<!-- 									    </div> -->
<!-- 									  </div> -->
					        </div>
					      </div>
					    </div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	
</body>
</html>