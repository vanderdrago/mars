<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<script type="text/javascript">
function ShowHide(obj){
var tbody = obj.parentNode.parentNode.parentNode.getElementsByTagName("tbody")[0];
var old = tbody.style.display;

tbody.style.display = (old == "none"?"":"none");
}

</script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Jadwal Angsuran</title>
</head>
<body>
<table width="100%" border="0">
	<tr>
		<td align="center">
			<table width="1000px" border="0">
				<tr>
					<td align="center" colspan="6"><b>JADWAL ANGSURAN</b></td>
				</tr>
				<tr>
					<td style="width: 120px;">CIF</td>
					<td style="width: 5px;">:</td>
					<td >${mstrInstl.customer_id}</td>
					<td style="width: 120px;">No Kartu</td>
					<td style="width: 5px;">:</td>
					<td style="width: 300px;">${mstrInstl.account_number}</td>
				</tr>
				<tr>
					<td style="width: 120px;">Nasabah</td>
					<td>:</td>
					<td >${mstrInstl.prim_app_name}</td>
					<td style="width: 120px;">Produk Kategori</td>
					<td>:</td>
					<td>${mstrInstl.prod_code} - ${mstrInstl.prod_ctgry}</td>
				</tr>
				<tr>
					<td style="width: 120px;">Harga Beli Awal</td>
					<td>:</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${mstrInstl.amount_disbursed}" /></td>
					<td style="width: 120px;">Produk Deskripsi</td>
					<td>:</td>
					<td>${mstrInstl.prod_desc}</td>
				</tr>
				<tr>
					<td style="width: 120px;">Harga Pokok</td>
					<td>:</td>
					<td ><fmt:formatNumber type="number" maxFractionDigits="2" value="${mstrInstl.tempTotPkh}" /></td>
					<td style="width: 120px;">Jk. Waktu</td>
					<td>:</td>
					<td>${mstrInstl.no_of_installments} Bln</td>
				</tr>
				<tr>
					<td style="width: 120px;">Margin</td>
					<td>:</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${mstrInstl.tempTotMrgnh}" /></td>
					<td style="width: 120px;">Periode</td>
					<td>:</td>
					<td>-</td>
				</tr>
				<tr>
					<td style="width: 120px;">Ujroh</td>
					<td>:</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${mstrInstl.tempTotUjroh}" /></td>
<!-- 					<td style="width: 120px;">Harga Jual</td> -->
<!-- 					<td>:</td> -->
<%-- 					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${mstrInstl.tempPaidh}" /></td> --%>
					<td style="width: 120px;">Dropping</td>
					<td>:</td>
					<td>-</td>
				</tr>
				<tr>
					<td style="width: 120px;">Harga Jual</td>
					<td>:</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${mstrInstl.tempPaidh}" /></td>
					 <td style="width: 120px;">Denda</td>
					<td>:</td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${mstrInstl.tempTotDenda}" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td ></td>
	</tr>
</table>

<h6>DETAIL ANGSURAN</h6>
    <table id="rounded-corner" style="width: 100%" >
		    <thead>
		        <tr align="center">
		            <th style="width:30px; font-size: 12px;" onclick="ShowHide(this)"><b>&#8711;</b></th>
		            <th style="width:60px; font-size: 12px;"><b>TGL MULAI</b></th>
		            <th style="width:60px; font-size: 12px;"><b>TGL JT TEMPO</b></th>
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>POKOK</b></th>
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>MARGIN</b></th>
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>TOT ANGSURAN</b></th>
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>UJROH</b></th>
		             <!-- <th style="width:80px; font-size: 12px; text-align: right;"><b>DENDA</b></th>-->
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>PEMBAYARAN NORMAL</b></th>
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>SISA POKOK</b></th>
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>SISA MARGIN</b></th>
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>SISA ANGSURAN</b></th>
		            <th style="width:40px; font-size: 12px; text-align: right;"><b>RATE</b></th>
		        </tr>
		    </thead>				
		    <tbody>
		    	<c:set var="totprincipal" value="0" />
		    	<c:set var="totmargin" value="0" />
		    	<c:set var="totangs" value="0" />
		    	<c:set var="inc" value="0" />
				<c:forEach var="angs" items="${angs}" >
			        <tr>
<%-- 			            <td >${angs.schedule_no}</td> --%>
						<c:set var="inc" value="${inc + 1}" />
						<td >${inc}</td>
			            <td >${angs.schedule_st_date}</td>
			            <td >${angs.schedule_due_date}</td>
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.principal}" /></td>
			        	<c:set var="totprincipal" value="${totprincipal + angs.principal}" />
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.profit}" /></td>
			            <c:set var="totmargin" value="${totmargin + angs.profit}" />
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.tot_angsuran}" /></td>
			            <c:set var="totangs" value="${totangs + angs.tot_angsuran}" />
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.ujroh}" /></td>
			            <c:set var="totujroh" value="${totujroh + angs.ujroh}" />
			              <!-- <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.denda}" /></td>
			            <c:set var="totdenda" value="${totdenda + angs.denda}" />-->
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.amount_settled}" /></td>
<!-- 						<td style="text-align: right;"> -->
<%-- 							<a href="/ReportMCB/vDtljdwl.do?account_no=${mstrInstl.account_number}&start_date=${angs.schedule_st_date}&end_date=${angs.schedule_due_date}" target="_blank"> --%>
<%-- 					                  <fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.nompaid}" /> --%>
<!-- 					        </a> -->
<!-- 						</td> -->
					    <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.sisa_principal}" /></td>
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.sisa_profit}" /></td>
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs.sisa_angsuran}" /></td>
			            <td style="text-align: right;">${angs.rate}</td>
			        </tr>
			    </c:forEach>
			</tbody>
		    <tfoot>
		    <tr style="font-weight: bold">
		    	<td colspan="3" align="center"><b>JUMLAH TOTAL</b></td>
		    	<td style="text-align: right; " ><fmt:formatNumber type="number" maxFractionDigits="2" value="${totprincipal}" /></td>
		    	<td style="text-align: right; " ><fmt:formatNumber type="number" maxFractionDigits="2" value="${totmargin}" /></td>
		    	<td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${totangs}" /></td>
		    	<td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${totujroh}" /></td>
		    	<!-- <td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${totdenda}" /></td>  -->
		    	<td align="right"></td>
		    	<td align="right"></td>
		    	<td align="right"></td>
		    	<td align="right"></td>
		    	<td align="right"></td>
		    </tr>
		    </tfoot>
		</table>
		
		</br>
		</br>
		
		
		<!-- tabel baru -->
		<h6>DETAIL DENDA</h6>
		  <table id="rounded-corner" style="width: 50%" >
		    <thead>
		        <tr align="center">
		            <th style="width:30px; font-size: 12px;" onclick="ShowHide(this)"><b>&#8711;</b></th>
		            <th style="width:60px; font-size: 12px;"><b>TGL MULAI</b></th>
		           <!--   <th style="width:60px; font-size: 12px;"><b>TGL JT TEMPO</b></th>-->
		               <th style="width:80px; font-size: 12px; text-align: right;"><b>DENDA</b></th>
		       
		            <th style="width:80px; font-size: 12px; text-align: right;"><b>PEMBAYARAN NORMAL</b></th>
		         
		        </tr>
		    </thead>				
		    <tbody>
		    	<c:set var="totprincipal" value="0" />
		    	<c:set var="totmargin" value="0" />
		    	<c:set var="totangs" value="0" />
		    	<c:set var="inc" value="0" />
				<c:forEach var="angs2" items="${angs2}" >
			        <tr>
<%-- 			            <td >${angs.schedule_no}</td> --%>
						<c:set var="inc" value="${inc + 1}" />
						<td >${inc}</td>
			            <td >${angs2.schedule_st_date_2}</td>
			          <!--   <td >${angs2.schedule_due_date_2}</td> -->
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs2.denda}" /></td>
	 					<c:set var="totdenda" value="${totdenda + angs2.denda}" />
			            <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${angs2.amount_settled_2}" /></td>
   		             	 <c:set var="totbayar" value="${totbayar + angs2.amount_settled_2}" />
			          </tr>
			    </c:forEach>
			</tbody>
		    <tfoot>
		    <tr style="font-weight: bold">
		    	<td colspan="2" align="center"><b>JUMLAH TOTAL</b></td>
		    	<td style="text-align: right; " ><fmt:formatNumber type="number" maxFractionDigits="2" value="${totdenda}" /></td>
		    	<td style="text-align: right; " ><fmt:formatNumber type="number" maxFractionDigits="2" value="${totbayar}" /></td>
		    	<!-- <td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${totdenda}" /></td>  -->
		    
		    </tr>
		    </tfoot>
		</table>
		
</body>
</html>