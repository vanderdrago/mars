<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">
$(function() {
    $( "#tgl1" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd-mm-yy'
    });
});

function validateInput(){
    var b = document.getElementById("report");
    var rpt = b.options[b.selectedIndex].value;
    if (rpt == ""){
        alert('Silahkan pilih report');
        return false;
    }
    return true;
}

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Monitoring SKN Outgoing/Incoming</title>
</head>
<body>
<fieldset style="width:450px;  border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
    <legend> <b>Monitoring SKN Outgoing/Incoming</b> </legend>
    <form action="viewMonSOut.do" method="POST" name="jurnal">
    <table>
    	<tr>
    		<td>
    		<input type="radio" name="skn" value="out" checked="checked">SKN Outgoing<br>
			<input type="radio" name="skn" value="in">SKN Incoming
    		</td>
    	</tr>
        <tr>
            <td>
                <input onclick="return validate()" type="submit" value="Refresh" name="ok" id="ok" />
            </td>
        </tr>
    </table>
    </form>
</fieldset>

<logic:equal name="viewlpcd" value="lpcd">
<table border="0">
	<tr>
		<th colspan="3" style="text-align: center;" >
		<logic:equal name="hdr" value="out"><h3>SKN OUTGOING</h3></logic:equal>
		<logic:equal name="hdr" value="in"><h3>SKN INCOMING</h3></logic:equal>
		</th>
	</tr>
	<tr >
		<th style="text-align: center;" >IB/CMS</th>
		<th style="text-align: center;" > MCB</th>
		<th style="text-align: center;" >INTERFACE SKN</th>
	</tr>
	<tr>
		<td>IB</td>
	</tr>
	<tr>
		<td valign="top" >
			<table id="rounded-corner" style="width:100%">
			<thead>
				<tr style="text-align: left;">
					<th width="150px;" style="font-weight: bold; font-size: 13px;">SERVER</th>
					<th style="font-weight: bold; font-size: 13px;">STATUS</th>
					<th style="text-align: right; font-size: 13px; font-weight: bold;"> TOTAL ITEM</th>
					<th width="150px;" style="text-align: right; font-size: 13px; font-weight: bold;">TOTAL NOMINAL</th>
				</tr>
			</thead>
			<tbody>
				<c:set var="prcd" value="0" />
			    <c:set var="unprcd" value="0" />
				<c:set var="nomprcd" value="0" />
			    <c:set var="nomunprcd" value="0" />
				<c:forEach items="${lpcdIb}" var="lpcdIb">
				<tr >
					<td >${lpcdIb.pcd}</td>
			        <td> ${lpcdIb.sts}</td>
			        <td style="text-align: right;">${lpcdIb.tot}</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lpcdIb.nom}" /></td>
			        <c:if test="${lpcdIb.sts == 'Failed'}">	
			        <c:set var="unprcd" value="${unprcd + lpcdIb.tot}" />
			        <c:set var="nomunprcd" value="${nomunprcd + lpcdIb.nom}" />
			        </c:if>
			        <c:if test="${lpcdIb.sts == 'Success'}">	
			        <c:set var="prcd" value="${prcd + lpcdIb.tot}" />
			        <c:set var="nomprcd" value="${nomprcd + lpcdIb.nom}" />
			        </c:if>
			        <c:if test="${lpcdIb.sts == '-'}">	
			        <c:set var="prcd" value="${prcd + lpcdIb.tot}" />
			        <c:set var="nomprcd" value="${nomprcd + lpcdIb.nom}" />
			        </c:if>
			        
				</tr>
			    </c:forEach>
		    </tbody>
		    <tfoot style="font-weight: bold;">
			    <tr>
			    	<td >TOT PROCEED </td>
					<td >:</td>
			        <td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${prcd}" /></td>
			        <td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomprcd}" /></td>
			    </tr>
			    <tr>
			    	<td>TOT UNPROCEED </td>
					<td >:</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${unprcd}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomunprcd}" /></td>
			    </tr>
			    <tr>
			    	<td>TOTAL </td>
					<td>:</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${prcd + unprcd}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomprcd + nomunprcd}" /></td>
			    </tr>
    		</tfoot>
			</table>
		</td>
		<td rowspan="4">
			<table id="rounded-corner" style="width:100%">
			<thead>
				<tr style="text-align: left;">
					<th width="150px;" style="font-weight: bold; font-size: 13px;">SERVER</th>
					<th style="font-weight: bold; font-size: 13px;">STATUS</th>
					<th style="text-align: right; font-size: 13px; font-weight: bold;"> TOTAL ITEM</th>
					<th width="150px;" style="text-align: right; font-size: 13px; font-weight: bold;">TOTAL NOMINAL</th>
				</tr>
			</thead>
			<tbody>
				<c:set var="prcd" value="0" />
			    <c:set var="unprcd" value="0" />
				<c:set var="nomprcd" value="0" />
			    <c:set var="nomunprcd" value="0" />
				<c:forEach items="${lpcd}" var="lpcd">
				<tr>
					<td >${lpcd.pcd}</td>
			        <td><a href="/ReportMCB/vJMonSKN.do?fr=u&sts=${lpcd.sts}&srv=${lpcd.pcd}" target="_blank"> ${lpcd.sts} </a></td>
			        <td style="text-align: right;">${lpcd.tot}</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lpcd.nom}" /></td>
			        <c:if test="${lpcd.sts == 'UNPROCEED'}">	
			        <c:set var="unprcd" value="${unprcd + lpcd.tot}" />
			        <c:set var="nomunprcd" value="${nomunprcd + lpcd.nom}" />
			        </c:if>
			        <c:if test="${lpcd.sts == 'PROCEED'}">	
			        <c:set var="prcd" value="${prcd + lpcd.tot}" />
			        <c:set var="nomprcd" value="${nomprcd + lpcd.nom}" />
			        </c:if>
			        <c:if test="${lpcd.sts == '-'}">	
			        <c:set var="prcd" value="${prcd + lpcd.tot}" />
			        <c:set var="nomprcd" value="${nomprcd + lpcd.nom}" />
			        </c:if>
			        
				</tr>
			    </c:forEach>
		    </tbody>
		    <tfoot style="font-weight: bold;">
			    <tr>
			    	<td>TOT PROCEED </td>
					<td>:</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${prcd}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomprcd}" /></td>
			    </tr>
			    <tr>
			    	<td>TOT UNPROCEED </td>
					<td >:</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${unprcd}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomunprcd}" /></td>
			    </tr>
			    <tr>
			    	<td>TOTAL </td>
					<td>:</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${prcd + unprcd}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomprcd + nomunprcd}" /></td>
			    </tr>
    		</tfoot>
			</table>
		</td>
		<td valign="middle" rowspan="4">
			<table id="rounded-corner" border="0">
			<thead>
				<tr style="text-align: left;">
					<th width="150px;" style="font-weight: bold; font-size: 13px;">CHANNEL</th>
					<th style="font-weight: bold; font-size: 13px;">STATUS</th>
					<th style="text-align: right; font-weight: bold; font-size: 13px;">TOTAL ITEM</th>
					<th width="150px;" style="text-align: right; font-weight: bold; font-size: 13px;">TOTAL NOMINAL</th>
				</tr>
				</thead>
			<tbody>
				<c:set var="nomprcdintf" value="0" />
			    <c:set var="nomunprcdintf" value="0" />
				<c:forEach items="${lpcdintf}" var="lpcdintf">
				<tr>
					<td >${lpcdintf.pcd}</td>
			        <td>${lpcdintf.sts}</td>
			        <td style="text-align: right;">${lpcdintf.tot}</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lpcdintf.nom}" /></td>
			        	
			        <c:set var="unprcdintf" value="${unprcdintf + lpcdintf.tot}" />
			        <c:set var="nomunprcdintf" value="${nomunprcdintf + lpcdintf.nom}" />
			        
			        
				</tr>
			    </c:forEach>
			</tbody>
		    <tfoot style="font-weight: bold;">
			    <tr >
			    	<td tyle="font-weight:bold">TOTAL</td>
					<td></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${unprcdintf}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomunprcdintf}" /></td>
			    </tr>
		    </tfoot>
			</table>
		</td>
	</tr>	
	<tr>
		<td>CMS</td>
	</tr>
	<tr>
		<td valign="top">
			<table id="rounded-corner" style="width:100%">
			<thead>
				<tr style="text-align: left;">
					<th width="150px;" style="font-weight: bold; font-size: 13px;">SERVER</th>
					<th style="font-weight: bold; font-size: 13px;">STATUS</th>
					<th style="text-align: right; font-size: 13px; font-weight: bold;"> TOTAL ITEM</th>
					<th width="150px;" style="text-align: right; font-size: 13px; font-weight: bold;">TOTAL NOMINAL</th>
				</tr>
			</thead>
			<tbody>
				<c:set var="prcd" value="0" />
			    <c:set var="unprcd" value="0" />
				<c:set var="nomprcd" value="0" />
			    <c:set var="nomunprcd" value="0" />
				<c:forEach items="${lpcdCms}" var="lpcdCms">
				<tr >
					<td >${lpcdCms.pcd}</td>
			        <td> ${lpcdCms.sts}</td>
			        <td style="text-align: right;">${lpcdCms.tot}</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lpcdCms.nom}" /></td>
			        <c:if test="${lpcdCms.sts == 'Failed'}">	
			        <c:set var="unprcd" value="${unprcd + lpcdCms.tot}" />
			        <c:set var="nomunprcd" value="${nomunprcd + lpcdCms.nom}" />
			        </c:if>
			        <c:if test="${lpcdCms.sts == 'Success'}">	
			        <c:set var="prcd" value="${prcd + lpcdCms.tot}" />
			        <c:set var="nomprcd" value="${nomprcd + lpcdCms.nom}" />
			        </c:if>
			        <c:if test="${lpcdCms.sts == '-'}">	
			        <c:set var="prcd" value="${prcd + lpcdCms.tot}" />
			        <c:set var="nomprcd" value="${nomprcd + lpcdCms.nom}" />
			        </c:if>
			        
				</tr>
			    </c:forEach>
		    </tbody>
		    <tfoot style="font-weight: bold;">
			    <tr>
			    	<td >TOT PROCEED </td>
					<td >:</td>
			        <td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${prcd}" /></td>
			        <td style="text-align: right; "><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomprcd}" /></td>
			    </tr>
			    <tr>
			    	<td>TOT UNPROCEED </td>
					<td >:</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${unprcd}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomunprcd}" /></td>
			    </tr>
			    <tr>
			    	<td>TOTAL </td>
					<td>:</td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${prcd + unprcd}" /></td>
			        <td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${nomprcd + nomunprcd}" /></td>
			    </tr>
    		</tfoot>
			</table>
		</td>
	</tr>
</table>
</logic:equal>
</body>
</html>