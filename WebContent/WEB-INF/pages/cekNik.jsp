<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

function validateoke(){ 
 	var nik;
 	nik = document.getElementById('nik').value;
	if (nik == ""){
	 	alert('Silahkan isi NIK'); 
	 	return false;
	} 
 	return true;
  }

</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cek NIK</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Cek User ID / NIK</b></legend>
					<form action="cekNIKAct.do" name="jurnal" method="POST">
						<table>
							<tr>
<!-- 								<td><input type="text" name="nik" id="nik" /></td> -->
								<td><input type="text" name="nik" id="nik" class="inputs" placeholder="User Id / NIK" style=" width : 400px;"></td>
							</tr>
							<tr>
<!-- 								<td><input onclick="return validateoke()" type="submit" name="ok" id="ok" value="VIEW"> -->
								<td align="right"><a href="#" onclick="document.forms['jurnal'].submit(); return validateoke();" class="blue btn">Search</a></td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color: black;">
			<td align="center">
				<table border="0" width="1075px">
					<tr>
						<td>
							<table border="0">
								<tr>
									<td>
										<c:if test="${sawalNik >= 1}">Page ${sawalNik} Of ${noOfPagesNik}</c:if>
									</td>
									<td align="left">
  	  							  		<c:if test="${sawalNik >= 1}"><a href="manageLphSaldoFwd.do?page=1" class="page">First</a></c:if>
  	  						  		</td>
  	  						  		<td>
			       					  	<c:if test="${currentPageNik != 1 && (currentPageNik - 1 > 0)}">
									  		<a href="manageLphSaldoFwd.do?page=${currentPageNik - 1}" class="page">Previous</a>
								     	</c:if>
		       			      		</td>	
		       			      		<td>
			       			      	   <table>
			       			      	  	   <tr>
			       			      	  	  	  <td>
			       			      	  	  	  	  <c:if test="${sawalNik >= 1}">
			       			      	  	  	  	  	  <c:forEach begin="${sawalNik}" end="${sakhirNik}" var="i">
			       			      	  	  	  	  	  	 <c:choose><td><a href="manageLphSaldoFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
			       			      	  	  	  	  	  </c:forEach>
			       			      	  	  	  	  </c:if>
			       			      	  	  	  </td>
			       			      	  	  </tr>
			       			      	   </table>
			       			        </td>
			       			        <td>
						     		   <c:if test="${currentPageNik lt noOfPagesNik}">
							          	  <a href="manageLphSaldoFwd.do?page=${currentPageNik + 1}" class="page">Next</a>
									   </c:if>
						    	    </td>
					    	  		<td>
					    	  	  		<c:if test="${noOfPagesNik - 9 > 0}">
					       					<a href="manageLphSaldoFwd.do?page=${currentPageNik - 9}"class="page">Last</a>
					       		  		</c:if>
					    	  		</td>
								</tr>
							</table>
						</td>
						<td align="right">
							<c:if test="${sawalNik >= 1}">Search Result - ${noOfRecordsNik} Result</c:if>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div style="font-size:13px; font-family:times new roman;">
							<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:100px; font-size: 12px;"><b>USER ID</b></th>
										<th style="width:150px; font-size: 12px;"><b>NAMA</b></th>
										<th style="width:100px; font-size: 12px;"><b>CABANG</b></th>
										<th style="width:200px; font-size: 12px;"><b>NAMA CABANG</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="NIK" items="${NIK}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td>${inc}</td>
										<td>${NIK.nik}</td>
										<td>${NIK.nama}</td>
										<td>${NIK.branch}</td>
										<td>${NIK.branch_name}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>