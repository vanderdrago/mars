<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://necolas.github.io/normalize.css/2.1.3/normalize.css">
<link href="/ReportMCB/scripts/css/jquery.idealforms.css" rel="stylesheet" type="text/css"/>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Detail Jadwal Angsuran FBTI</title>
<style>
body {
max-width: 78%;
margin:  1em auto;
font: normal 12px;
color: #353535;
overflow-y: scroll;
}

</style>
<!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
</head>
<body>
	<table border="0">
		<tr>
			<td align="center">
				<fieldset>
					<legend>JADWAL ANGSURAN</legend>
						<table width="1050px" border="0">
<!-- 						<tr> -->
<!-- 							<td align="center" colspan="6"><b>JADWAL ANGSURAN</b></td> -->
<!-- 						</tr> -->
						<tr>
							<td style="width: 120px;">No Installment</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.installmentNo}</td>
							<td style="width: 120px;">Customer Id</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.customerId}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Customer Name</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.customerName}</td>
							<td style="width: 120px;">Produk Code</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.productCode}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Branch</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.branch}</td>
							<td style="width: 120px;">Product Type</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.productType}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Due Date</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.dueDate}</td>
							<td style="width: 120px;">Product Category</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.productCategory}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Jangka Waktu</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.tenor} Bulan</td>
							<td style="width: 120px;">Rate</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.rate}%</td>
						</tr>
						<tr>
							<td style="width: 120px;">Currency</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.ccy}</td>
							<td style="width: 120px;">Plafond</td>
							<td style="width: 5px;">:</td>
							<td width="300px"><fmt:formatNumber type="number" maxFractionDigits="0" value="${headFbti.plafond}"/></td>
						</tr>
						<tr>
							<td style="width: 120px;">Harga Jual</td>
							<td style="width: 5px;">:</td>
							<td width="300px"><fmt:formatNumber type="number" maxFractionDigits="0" value="${headFbti.hargaJual}"/></td>
							<td style="width: 120px;">Margin</td>
							<td style="width: 5px;">:</td>
							<td width="300px"><fmt:formatNumber type="number" maxFractionDigits="0" value="${headFbti.marginAwal}"/></td>
						</tr>
						<tr>
							<td style="width: 120px;">Kategori Usaha</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.kategoriKonsumsiUsaha}</td>
							<td style="width: 200px;">Kategori Usaha Portfolio</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.kategoriPortofolioKonsumsiUsaha}</td>
						</tr>
						<tr>
							<td style="width: 150px;">Jenis Piutang Usaha</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.jenisPiutang}</td>
							<td style="width: 200px;">Sifat Piutang</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.sifatPiutang}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Orientasi Penggunaan</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.orientasiPenggunaan}</td>
							<td style="width: 200px;">Sektor Ekonomi Usaha</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.sektorEkonomiUsaha}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Lokasi Proyek</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.lokasiProyek}</td>
							<td style="width: 200px;">Financing Marketing Code</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.kodeMarketing}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Segmentasi Account</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.segmentAccount}</td>
							<td style="width: 200px;">Program Pembiayaan</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.programPembiayaan}</td>
						</tr>
						<tr>
							<td style="width: 180px;">Pembiayaan Back to Back</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.pembiayaanBackToBack}</td>
							<td style="width: 220px;">Nomor Rekening Back to Back</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.noRekBackToBack}</td>
						</tr>
						<tr>
							<td style="width: 120px;">Jenis Kartu</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.jenisKartu}</td>
							<td style="width: 200px;">Update Segmentasi Account</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.updateSegmentasi}</td>
						</tr>
						<tr>
							<td style="width: 200px;">Produk Segmentasi Account</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.produkSegmentasi}</td>
							<td style="width: 120px;">Jenis Penggunaan</td>
							<td style="width: 5px;">:</td>
							<td style="width: 300px;">${headFbti.modelKerja}</td>
						</tr>
					</table>
				</fieldset>
				
				
			</td>
		</tr>
		
		<tr>
			<td align="center">
				<table id="able table-striped" style="width: 1050px" border ="0" >
					<thead>
				        <tr align="center">
			<!-- 	            <th style="width:30px; font-size: 12px;"><b>NO</b></th> -->
				            <th style="width:50px; font-size: 12px;"><b>INSTALLMENT DATE</b></th>
				            <th style="width:80px; font-size: 12px;"><b>OUTSTANDING PRINCIPAL</b></th>
				            <th style="width:80px; font-size: 12px; text-align: right;"><b>PRINCIPAL REPAYMENT</b></th>
				            <th style="width:30px; font-size: 12px; text-align: right;"><b>TENOR</b></th>
				            <th style="width:80px; font-size: 12px; text-align: right;"><b>PROFIT MARGIN AMOUNT</b></th>
				            <th style="width:30px; font-size: 12px; text-align: right;"><b>RATE</b></th>
				            <th style="width:80px; font-size: 12px; text-align: right;"><b>TOTAL REPAYMENT</b></th>
				        </tr>
				    </thead>	
				    <tbody>
				    	<c:forEach var="detailJadang" items="${detailJadang}">
					    	<tr style="font-size: 11px;">
					    		<td style="text-align: center;">${detailJadang.tanggal}</td>
					    		<td style="text-align: center;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${detailJadang.amountPrincipal}"/></td>
					    		<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${detailJadang.principalRepayment}"/></td>
					    		<td style="text-align: right;">${detailJadang.tenor}</td>
					    		<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${detailJadang.profitMargin}"/></td>
					    		<td style="text-align: right;">${detailJadang.rate}%</td>
					    		<td style="text-align: right;"><fmt:formatNumber type="number" maxFractionDigits="0" value="${detailJadang.totalRepayment}"/></td>
					    	</tr>
				    	</c:forEach>
				    </tbody>
				</table>
			</td>
		</tr>
		
	</table>
	
	
</body>
</html>