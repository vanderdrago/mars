<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
$(function(){
	$("#prd1").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: "yymm",
		showButtonPanel: true,
		onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	});
});

</script>
<script type="text/javascript">
$(function(){
	$("#prd2").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: "yymm",
		showButtonPanel: true,
		onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	});
});

</script>
<style type="text/css">
	.ui-datepicker-calendar {
	display: none;
}
</style>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>PRKS</b></legend>
					<form action="prksAct.do" name="jurnal" method="POST">
						<table>
							<tr>
								<td><input type="text" name="acc" id="acc" class="inputs" placeholder="No Rekening PRKS" style=" width : 400px;"></td>
							</tr>
							<tr>
								<td align="left"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color:black">
			<td align="center">
				<table border="0" width="1075px">
					<tr>
						<td align="center" colspan="2">
						<div style="font-size:13px; font-family:times new roman;">
							<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
<!-- 										<th style="width:100px; font-size: 12px;"><b>PERIODE</b></th> -->
										<th style="width:100px; font-size: 12px;"><b>CUST NO</b></th>
										<th style="width:200px; font-size: 12px;"><b>CUST NAME</b></th>
										<th style="width:100px; font-size: 12px;"><b>ACCOUNT CLASS</b></th>
										<th style="width:200px; font-size: 12px;"><b>START DATE</b></th>
										<th style="width:100px; font-size: 12px;"><b>EXPIRY DATE</b></th>
										<th style="width:100px; font-size: 12px;"><b>PLAFOND</b></th>
										<th></th>
<!-- 										<th style="width:100px; font-size: 12px;"><b>PBH</b></th> -->
<!-- 										<th style="width:100px; font-size: 12px;"><b>RBH</b></th> -->
<!-- 										<th style="width:20px; font-size: 12px;"><b>OS</b></th> -->
<!-- 										<th style="width:100px; font-size: 12px;"><b>FCU</b></th> -->
<!-- 										<th style="width:100px; font-size: 12px;"><b>RATE</b></th> -->
<!-- 										<th style="width:100px; font-size: 12px;"><b>STATUS</b></th> -->
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="prks" items="${prks}" >
									<tr>
										<c:set var="inc" value="${inc + 1}" />
										<td style="font-size: 10px">${inc}</td>
										<td style="font-size: 10px">${prks.acc}</td>
										<td style="font-size: 10px">${prks.custName}</td>
										<td style="font-size: 10px">${prks.accClass}</td>
										<td style="font-size: 10px">${prks.startDt}</td>
										<td style="font-size: 10px">${prks.expiryDt}</td>
										<td style="font-size: 10px"><fmt:formatNumber type="number" maxFractionDigits="2" value="${prks.plafond}" /></td>
										<td>
											<a href="/ReportMCB/periodPrks.do?accNo=${prks.acc}&custName=${prks.custName}&brn=${prks.brn}" target="_blank" class="label btn-info label-form">Detail </a>
										</td>
<%-- 										<td style="font-size: 10px"><fmt:formatNumber type="number" maxFractionDigits="2" value="${prks.os}" /></td> --%>
<%-- 										<td style="font-size: 10px">${prks.fcu}</td> --%>
<%-- 										<td style="font-size: 10px">${prks.rate} %</td> --%>
<%-- 										<td style="font-size: 10px">${prks.status}</td> --%>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>