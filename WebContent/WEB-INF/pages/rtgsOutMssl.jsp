<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type='text/javascript' src='/ReportMCB/scripts/money/jquery.js' ></script>
<script type="text/javascript">
$(function() {
    $( "#bsnsdt" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'yymmdd'
    });
});

function valInput(){
    var bsnsdt;
    bsnsdt = document.getElementById('bsnsdt').value;

    if (bsnsdt == ""){
        alert('Tanggal tidak boleh kosong');
        return false;
    }
    return true;
}


function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div style="font-size: 11px;">
<table border="0" align="center">
	<tr>
		<td>			
			<fieldset style="width:650px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			<legend> <b>RTGS OUT MASSAL</b> </legend>
			<form action="rtgsOutMsslView.do" method="POST" name="jurnal">
				<table border="0">					
					<tr>
						<td width="90px;">Trns Dt</td>
						<td>:</td>
						<td><input type="text" name="bsnsdt" id="bsnsdt" readonly="true"></td>			
						<td>From Account</td>
						<td>:</td>
						<td><input type="text" name="fracc" id="fracc" maxlength="24"></td>
					</tr>
					<tr>
						<td>Branch</td>
						<td>:</td>
						<td width="180px;"><input type="text" name="kdcab" id="kdcab" maxlength="3"></td>							
						<td>To Account</td>
						<td>:</td>
						<td><input type="text" name="toacc" id="toacc" maxlength="24"></td>	
						
					</tr>
					<tr>
						<td>No. Reference</td>
						<td>:</td>
						<td><input type="text" name="reltrn" id="idreltrn" maxlength="16"></td>								
						<td>To Member</td>
						<td>:</td>
						<td>
		                    <select name="tmmbr" id="idtmmbr" >
		                        <option value="">Pilih</option>
		                        <c:forEach items="${lmmbrbnk}" var="lmmbrbnk">
		                             <option value=${lmmbrbnk.codename}>${lmmbrbnk.codename}</option>
		                        </c:forEach>
		                    </select> &nbsp; Member Bank
		                </td>				
						
					</tr>
					<tr>
						<td colspan="6">
							<input onclick="return valInput()"  type="submit" value="View" name="btn" id="idbtn" />
                			<input onclick="return valInput()" type="submit" value="Download" name="btn" id="download" />
						</td>
						
					</tr>
				</table>
			</form>
			</fieldset>
		</td>
	</tr>
</table>
<logic:equal name="respon" value="teks">
${teks }
</logic:equal>

<logic:equal name ="data" value="out">
<table border="0" align="center" width="1075px;">
	<tr>
		<td>
			
	<div style="  float:left; text-align: left; ">
	<table border="0">
		<tr>
	    	<td><a href="manageRtgsMsslFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="manageRtgsMsslFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="manageRtgsMsslFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="manageRtgsMsslFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageRtgsMsslFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style="width:500px;    float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr align="center" style="font-size: 11px;">
					            <th style="width:25px; "><b>BRNCH</b></th>
					            <th style="width:50px; "><b>NO. REFF</b></th>
					            <th style="width:50px;"><b>FROM ACC</b></th>
					            <th style="width:60px; "><b>TO ACC</b></th>
					            <th style="width:70px; "><b>TO MEMBER</b></th>
					            <th style="width:100px; text-align:right; "><b>AMOUNT</b></th>
					            <th style="width:500px; "><b>DETAIL</b></th>	
					            <th style="width:180px; "><b>FILENAME</b></th>	
					            <th style="width:50px; "><b>STS DATA</b></th>
					            <th style="width:50px;"><b>AUTHORIZED</b></th>			            
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="ltrnrtgsmsl" items="${ltrnrtgsmsl}" >
						        <tr style="font-size: 11px; ">
						            <td >${ltrnrtgsmsl.branch_cd}</td>
						            <td >${ltrnrtgsmsl.sender_ref_no}</td>
						            <td >${ltrnrtgsmsl.fr_acc}</td>
						            <td >${ltrnrtgsmsl.to_acc}</td>
						            <td >${ltrnrtgsmsl.to_member}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrnrtgsmsl.amount}" /></td>
						            <td >${ltrnrtgsmsl.pay_detail}</td>
						            <td >${ltrnrtgsmsl.filename}</td>
						            <td >${ltrnrtgsmsl.sts_data}</td>
						            <td >${ltrnrtgsmsl.sts_trns}</td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>			
		</td>
	</tr>
</table>
</logic:equal>
</div>
</body>
</html>