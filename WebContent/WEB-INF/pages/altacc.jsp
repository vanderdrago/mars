<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

function validateoke(){ 
 	var accno;
 	var name;
 	accno = document.getElementById('accno').value;
 	name = document.getElementById('name').value; 
	if (accno == "" && name == ""){
	 	alert('Silahkan isi No Rekening atau nama'); 
	 	return false;
	} 
 	return true;
  }

</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alternate Account</title>
</head>
<body>
<table width="100%" border="0">
	<tr>
		<td align="center">
			<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
	    	<legend> <b>Alternate Account</b> </legend>
	        <form action="/ReportMCB/vGetAltAct.do" method="POST" name="mgetinstl">	    
	        <table border="0">
	            <tr>
	            	<td><input type="text" name="accno" id="accno" class="inputs" placeholder="No. Rekening" style=" width : 400px;"></td>
	            </tr>
	             <tr>
	             	<td><input type="text" name="name" id="name" class="inputs" placeholder="Nama" style=" width : 400px;"></td>
	            </tr>
				<tr>
					<td align="right"><a href="#" onclick="document.forms['mgetinstl'].submit(); return valdata();" class="blue btn">Search</a></td>
	            </tr>
	        </table>
	        </form>
			</fieldset>
		</td>
	<tr style="color: black;">
		<td align="center">
			<table border="0" width="1075px">
				<tr>
					<td>
						<table>
							<tr>
								<td>
									<c:if test="${sawalAlt >= 1}">Page ${sawalAlt} Of ${noOfPagesAlt}</c:if>
								</td>
								<td align="left">
										<c:if test="${sawalAlt >= 1}"><a href="managePAltNomrek.do?page=1" class="page">First</a></c:if>
								</td>
								<td>
									<c:if test="${currentPageAlt != 1 && (currentPageAlt - 1 > 0)}">
										<a href="managePAltNomrek.do?page=${currentPageAlt - 1}" class="page">Previous</a>
									</c:if>
								</td>
								<td>
									<table>
  			      	  					<tr>
				      	  	  	  			<td><c:if test="${sawalAlt >= 1}">
				      	  	  	  	  	    <c:forEach begin="${sawalAlt}" end="${sakhirAlt}" var="i">
				      	  	  	  	  	  	<c:choose><td><a href="managePAltNomrek.do?page=${i}" class="page">${i}</a></td></c:choose>
				      	  	  	  	  	    </c:forEach></c:if></td>
		   			      	  			</tr>
  			     					</table>
								</td>
								<td> 
				     		  		<c:if test="${currentPageAlt lt noOfPagesAlt}">
					          			<a href="managePAltNomrek.do?page=${currentPageAlt + 1}" class="page">Next</a>
							  		</c:if>
				    	  		</td>
	    	  					<td>
					    	  	  	<c:if test="${noOfPagesAlt - 9 > 0}">
					       				<a href="managePAltNomrek.do?page=${currentPageAlt - 9}"class="page">Last</a>
					       		  	</c:if>
				    	  		</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<c:if test="${sawalAlt >= 1}">Search Result - ${noOfRecordsAlt} Result</c:if>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<div style="font-size:13px; font-family:times new roman;">
							<table id="rounded-corner" style="width:1075px"> 
								<thead>
									<tr align="center">
										<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
										<th style="width:100px; font-size: 12px;"><b>NO REKENING</b></th>
										<th style="width:100px; font-size: 12px;"><b>ALT NO REKENING</b></th>
										<th style="width:200px; font-size: 12px;"><b>NAMA</b></th>
										<th style="width:100px; font-size: 12px;"><b>CABANG</b></th>
										<th style="width:100px; font-size: 12px;"><b>VALUTA</b></th>
										<th style="width:100px; font-size: 12px;"><b>STATUS</b></th>
									</tr>
								</thead>
								<tbody>
									<c:set var="inc" value="0" />
									<c:forEach var="alt" items="${alt}" >
									<tr style="font-size: 11px;">
										<c:set var="inc" value="${inc + 1}" />
										<td>${inc}</td>
										<td>${alt.cust_ac_no}</td>
										<td>${alt.alt_ac_no}</td>
										<td>${alt.ac_desc}</td>
										<td>${alt.branch_code}</td>
										<td>${alt.ccy}</td>
										<td>${alt.record_stat}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
<%-- 						<logic:equal name="konfirmasi" value="err"> --%>
<%-- 							<p style="color: red;">${dataresponse}</p> --%>
<%-- 						</logic:equal> --%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>