<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.24.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link rel="stylesheet" href="/ReportMCB/css/report/table.css">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Standing Instruction</title>
</head>
<body>
	<div style="float: center; text-align: center;">
		<table width="100%">
			<tr>
				<td align="center">
					<fieldset style="width:1060px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
						<legend> <b></b> </legend>
							<table width="1075px" border="0" id="rounded-corner"  >
								<tr>
									<th style="font-size: 20px;align="center"; colspan=7 ;"><b><h3><center>DETAIL STANDING INSTRUCTION</center></h3></b></th>
								</tr>
								<tr>
									<td width="200px">NO. INSTRUCTION </td>
									<td>:</td>
									<td width="300px">${detail.instructionNo}
									<td width="200px">PRODUCT CODE</td>
									<td>:</td>
									<td width="300px">${detail.productCode}</td>
								</tr>
								<tr>
									<td>REFERENCE NO</td>
									<td>:</td>
									<td>${detail.contractRefNo}
									<td>PRODUCT REMARKS</td>
									<td>:</td>
									<td>${detail.productRemarks}</td>
								</tr>
								<tr>
									<td>USER REFERENCE </td>
									<td>:</td>
									<td>${detail.userRefNumber}
									<td>PRODUCT DESCRIPTION</td>
									<td>:</td>
									<td>${detail.productDesc}</td>
								</tr>
								<tr>
									<td>FIRST EXECUTION VALUE DATE</td>
									<td>:</td>
									<td>${detail.firstExecValueDate}</td>
									<td>SI CURRENCY</td>
									<td>:</td>
									<td>${detail.ccy}</td>
								</tr>
								<tr>
									<td>FIRST EXECUTION DUE DATE</td>
									<td>:</td>
									<td>${detail.firstExecDueDate}</td>
									<td>SI EXPIRY DATE</td>
									<td>:</td>
									<td>${detail.expiryDate}</td>
								</tr>
								<tr>
									<td>NEXT EXECUTION VALUE DATE</td>
									<td>:</td>
									<td>${detail.nextExecValueDate}</td>
									<td>SI AMOUNT</td>
									<td>:</td>
									<td>${detail.amount}</td>
								</tr>
								<tr>
									<td>NEXT EXECUTION DUE DATE</td>
									<td>:</td>
									<td>${detail.nextExecDueDate}</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td>DEBIT ACCOUNT BRANCH</td>
									<td>:</td>
									<td>${detail.drBranch}</td>
									<td>CREDIT ACCOUNT BRANCH</td>
									<td>:</td>
									<td>${detail.crBranch}</td>
								</tr>
								<tr>
									<td>DEBIT ACCOUNT</td>
									<td>:</td>
									<td>${detail.drAccount}</td>
									<td>CREDIT ACCOUNT</td>
									<td>:</td>
									<td>${detail.crAccount}</td>
								</tr>
								<tr>
									<td>REMARKS</td>
									<td>:</td>
									<td colspan="4">${detail.remarks}</td>
								</tr>
							</table>
					</fieldset>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>