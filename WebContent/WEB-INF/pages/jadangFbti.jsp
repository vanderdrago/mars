<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<script type="text/javascript">

function validateForm() {
    var instNo = document.forms["jadang"]["instNo"].value;
	var custId = document.forms["jadang"]["custId"].value;
	var name = document.forms["jadang"]["name"].value;
    if ((instNo == "") && (custId == "") && (name == "")) {
        alert("Installment No Or Customer Id Or Customer Name Not Empty");
        return false;
    }
    return true;
}

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Jadang Angsuran FBTI</title>
</head>
<body>
	<table width="1075px" border="0">
		<tr>
			<td>
				<form action="jadangFbtiAct.do" id="jadang" name="jadang" method="POST" onsubmit="return validateForm()">
					<fieldset>
						<legend>Fields Installment Query</legend>
						<div class="row">
							<div class="box-body">
								<div class="col-xs-5">
									<input class="form-control" placeholder="Installment No" type="text" name="instNo" id = "instNo" >
								</div>
							<br><br>
								<div class="col-xs-5">
									<input class="form-control" placeholder="Customer Id" type="text" name="custId" id = "custId" >
								</div>
							<br><br>
								<div class="col-xs-5">
									<input class="form-control" placeholder="Customer Name" type="text" name="name" id = "name" >
								</div>
							<br><br>
								<div class="col-xs-5">
									<button type="submit" class="btn btn-success" onclick="return validateForm()">Send <span class="glyphicon glyphicon-send"></span></button>
					        	</div>
							</div>
						</div>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr>
			<td>
				<table class="table table-striped" id="dev-table">
					<thead>
						<tr align="center">
							<th style="width:30px; font-size: 11px; font-style: family font sains"><b>#</b></th>
							<th style="width:120px; font-size: 11px;"><b>INSTALMENT NO</b></th>
							<th style="width:120px; font-size: 11px;"><b>CUSTOMER ID</b></th>
							<th style="width:250px; font-size: 11px;"><b>CUSTOMER NAME</b></th>
							<th style="width:80px; font-size: 11px;"><b>PROD CODE</b></th>
							<th style="width:100px; font-size: 11px;"><b>PROD CATEGORY</b></th>
							<th style="width:50px; font-size: 11px;"><b>TENOR</b></th>
							<th style="width:50px; font-size: 11px;"><b>CCY</b></th>
							<th style="width:50px; font-size: 11px;"><b>STATUS</b></th>
						</tr>
					</thead>
					<tbody>
						<c:set var="inc" value="0" />
						<c:forEach var="jadwalAngsuran" items="${jadwalAngsuran}" >
						<tr class="thead-inverse">
							<c:set var="inc" value="${inc + 1}" />
							<td style="font-size: 10px">${inc}</td>
							<td style="font-size: 10px"><a href="/ReportMCB/detailAngsuran.do?instNo=${jadwalAngsuran.installmentNo}" target="_blank">${jadwalAngsuran.installmentNo}</a></td>
							<td style="font-size: 10px">${jadwalAngsuran.customerId}</td>
							<td style="font-size: 10px">${jadwalAngsuran.customerName}</td>
							<td style="font-size: 10px">${jadwalAngsuran.productCode}</td>
							<td style="font-size: 10px">${jadwalAngsuran.productCategory}</td>
							<td style="font-size: 10px">${jadwalAngsuran.tenor} Bulan</td>
							<td style="font-size: 10px">${jadwalAngsuran.ccy}</td>
							<td style="font-size: 10px">${jadwalAngsuran.status}</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
							
			</td>
		</tr>
	</table>
	
</body>
</html>