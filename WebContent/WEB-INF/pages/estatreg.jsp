<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<script type='text/javascript' src='/ReportMCB/jsajax/e_statement.js'></script>
<script type='text/javascript' src='/ReportMCB/jsajax/jquery.isloading.min.js'></script>
<script type='text/javascript' src='/ReportMCB/jsajax/jquery.blockUI.js'></script>

<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table border="0" align="center">
	<tr>
		<td> 
			<fieldset style="width:330px; height:60px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			    <legend> <b>Registrasi e-Statement</b> </legend>
			    <table>
			        <tr>
			            <td >No. Rekening</td>
			            <td >:</td>
			            <td ><input type="text" name="acc" id="acc" maxlength="10"><label style="color:red">*</label></td>
			            <td >
			                <input type="submit" value="Lihat" name="btn" id="print" onclick="doReqCif()"/>			                			                
			            </td>
			        </tr>
			    </table>
			</fieldset>
		</td>
	</tr>
</table>

<table border="0" width="100%">
	<tr>
		<td align="center">
		<table width="600px;">
			<tr>
				<td>
					<fieldset style="border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999; padding-right: 15px;">
						<div class="data" ></div>
						<logic:equal name="respon" value="ret">
						${teks }
						</logic:equal>
					</fieldset>
				</td>
			</tr>
		</table>			
		</td>
	</tr>
</table>


</body>
</html>