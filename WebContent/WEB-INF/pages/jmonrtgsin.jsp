<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemrtgsint(){
    var amont_a;
    var amont_b;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}
</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h2 align="center">Join Monitoring & Reporting RTGS Interface-MCB Incoming</h2>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <form action="/ReportMCB/vJMonRTGSIn.do?fr=f" method="POST" name="mrtgs">
	    
        <table border="0" width="100%">
			<tr>
				<td style="width: 240px">Rel TRN</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="inputtrn" id="inputtrn"></td>
			</tr>
			<tr>
				<td style="width: 240px">Status</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="stsrtgs" id="stsrtgs"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<!-- <tr>
				<td style="width: 240px">To Account</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="rek_t_ac" id="rek_t_ac"
					maxlength="16"><label style="color: red"></label></td>
			</tr> -->
			<tr>
				<td style="width: 240px">Start Amount</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="amont_a" id="amont_a"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td style="width: 240px">To Amount</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="amont_b" id="amont_b"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td><input onclick="return validatemrtgsint()" type="submit" value="Ok" name="ok" id="ok" /> <!-- onclick="return validatemrtgs()" -->
				</td>
			</tr>
	</table>
        </form>
            <br />
       <div style="font-size:20px; font-family:times new roman;">

           <table id="rounded-corner" style="width:150%">
               <tr >
               <td><table><tr><td width="1260"><h4 align="center">INTERFACE</h4></td><td style="width:10px;"></td><td width="700"><h4 align="center">MCB</h4></td></tr>
               	<tr>
               <display:table name="ltrn" id="ltrn" class="wb" requestURI="MJRtgsInForward.do" pagesize="20" sort="external" >
		            <td>
			            <display:column title="<h3>Value Date</h3>" sortable="true" style="width:70px; text-align:center;" >${ltrn.valuedate}</display:column>
			            <display:column title="<h3>Rel TRN</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.relTRN}</display:column>
			            <display:column title="<h3>TRN</h3>" sortable="true" style="width:110px; text-align:center;">${ltrn.TRN}</display:column>          
			            <display:column title="<h3>Trans Code</h3>" sortable="true" style="width:170px;">${ltrn.transcode}</display:column>
			            <display:column title="<h3>BOR</h3>" sortable="true" style="width:50px; text-align:center;">${ltrn.bor}</display:column>
			            <display:column title="<h3>OSN</h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.osn}</display:column>
		        		<display:column title="<h3>From Acc No</h3>" sortable="true" style="width:85px; ">${ltrn.fromaccnum}</display:column>
		        		<display:column title="<h3>From Acc Name</h3>" sortable="true" style="width:85px; ">${ltrn.fromaccname}</display:column>
		        		<display:column title="<h3>To Acc Num</h3>" sortable="true" style="width:85px; ">${ltrn.toaccnum}</display:column>
		        		<display:column title="<h3>To Acc Name</h3>" sortable="true" style="width:85px; ">${ltrn.toaccname}</display:column>
		        		<display:column title="<h3>Benef Acc</h3>" sortable="true" style="width:85px; ">${ltrn.ultimatebeneacc}</display:column>
		        		<display:column title="<h3>Benef Name</h3>" sortable="true" style="width:85px; ">${ltrn.ultimatebenename}</display:column>
		        		<display:column title="<h3>Amount</h3>" sortable="true" style="width:120px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.nominal}" /></display:column>
			            <display:column title="<h3>Pay Details</h3>" sortable="true" style="width:85px; ">${ltrn.payDetails}</display:column>
		        		<display:column title="<h3>Status</h3>" sortable="true" style="width:85px; ">${ltrn.status}</display:column>
		        		<display:column title="<h3>Sakti No</h3>" sortable="true" style="width:85px; ">${ltrn.saktinum}</display:column>
		        	</td>
		            <td>  <display:column> </display:column></td>		        	
		        	<td>
		            	<display:column title="<h3>TRN Ref No</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.trn_ref_no}</display:column>
			            <display:column title="<h3>Event</h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.event}</display:column>
			            <display:column title="<h3>Module</h3>" sortable="true" style="width:80px; ">${ltrn.module}</display:column> 
			            <display:column title="<h3>Branch</h3>" sortable="true" style="width:170px; ">${ltrn.ac_branch}</display:column>         
			            <display:column title="<h3>TRN Code</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.trn_code}</display:column>
			            <display:column title="<h3>Batch No</h3>" sortable="true" style="width:110px; ">${ltrn.batch_no}</display:column>
			            <display:column title="<h3>Acc No</h3>" sortable="true" style="width:170px; ">${ltrn.ac_no}</display:column>
			            <display:column title="<h3>Acc Desc</h3>" sortable="true" style="width:170px; ">${ltrn.ac_desc}</display:column>
			            <display:column title="<h3>Db/ Cr</h3>" sortable="true" style="width:170px; ">${ltrn.drcr_ind}</display:column>
			            <display:column title="<h3>Amount</h3>" sortable="true" style="width:120px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.lcy_amount}" /></display:column>
			            <display:column title="<h3>Auth Stat</h3>" sortable="true" style="width:70px; ">${ltrn.auth_stat}</display:column>
			            <display:column title="<h3>User Id</h3>" sortable="true" style="width:70px; ">${ltrn.user_id}</display:column>
			            <display:column title="<h3>Auth Ref</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.auth_id}</display:column>
			        </td>
		        	</display:table>
		        	</tr></table></td>
                </tr>
           </table>
        </div>
        <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
		</logic:equal>
</body>
</html>