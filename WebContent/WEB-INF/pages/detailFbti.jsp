<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.24.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<link rel="stylesheet" href="/ReportMCB/css/report/table.css">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Detail Transaksi</title>
</head>
<body>
	<div style="float: center; text-align: center;">
		<table width="100%">
			<tr>
				
			</tr>
			<tr>
				<td>
					<table width="1075px">
						<tr>
							<td align="center" colspan="2">
								<div style="font-size:13px; font-family:times new roman;">
									<table id="rounded-corner" style="width:1075px"> 
										<thead>
											<tr align="center">
												<th style="width:30px; font-size: 12px;"><b>NO.</b></th>
												<th style="width:100px; font-size: 12px;"><b>INSTALMENT NO</b></th>
										</thead>
										<tbody>
											<c:set var="inc" value="0" />
											<c:forEach var="detail" items="${detail}" >
											<tr style="font-size: 11px;">
												<c:set var="inc" value="${inc + 1}" />
												<td>${inc}</td>
											<td>${detail.installmentNo}</td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>