<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemskn(){
    var amont_a;
    var amont_b;
    
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}
</script>

<head>
<!-- <style type="text/css"> -->
<!-- /*             body {font-family:times new roman;} */ -->
<!-- /*             .td1 {width:150px;} */ -->
<!-- /*             .td2 {width:5px;} */ -->
<!--         </style> -->

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<div style="font-size: 11px;">
	<table border="0" align="center">
		<tr>
			<td align = "center">
				<fieldset style="width:700px; border:0px solid #999; border-radius:8px; box-shadow:0 0 5px #999;">
				<legend><b>Monitoring & Reporting SKN Interface</b></legend>
<!-- 				<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a> -->
        		<form action="/ReportMCB/vMonSKN.do" method="POST" name="mskn">
		        <table border="0" width="600px">
		            <tr>
		            	<td style="width: 100px">Rel TRN</td>
		            	<td style="width: 10px">:</td>
		                <td><input type="text" name="inputstrn" id="inputstrn" ><label style="color:red">
		                </td>
		            	<td style="width: 100px">Status 1</td>
		            	<td style="width: 10px">:</td>
		                <td><input type="text" name="stsskn" id="stsskn" ><label style="color:red">
		                </td>
		            </tr>
		            <tr>
						<td style="width: 100px">Start Amount</td>
						<td style="width: 10px">:</td>
						<td><input type="text" name="amont_a" id="amont_a"
							maxlength="16"><label style="color: red"></label></td>
						<td style="width: 100px">To Amount</td>
						<td style="width: 10px">:</td>
						<td><input type="text" name="amont_b" id="amont_b"
							maxlength="16"><label style="color: red"></label></td>
					</tr>
		            <tr>
		            	<td colspan="2"> </td>
		            	<td><input onclick="return validatemskn()" type="submit" value="Ok" name="ok" id="ok" /><!-- onclick="return validatemrtgs()" --></td>
		            </tr>
		        </table>
       			</form>
       			</fieldset>
       		</td>
       	</tr>
        <br />
        
        <tr>
        	<td align ="center">
       		<div style="font-size:13px; font-family:times new roman;">
            <table id="rounded-corner" style="width:1075px">
               <tr>
               	   <td>
		            <display:table name="ltrn" id="ltrn" class="wb" requestURI="MonSknForward.do" pagesize="20" sort="external" >
		            <display:column title="<h3>Ref No</h3>" sortable="true" style="width:70px;">${ltrn.od_ref_no}</display:column>
		            <display:column title="<h3>Bank Receiver</h3>" sortable="true" style="width:100px;">${ltrn.od_bankno}</display:column>
		            <display:column title="<h3>Sender Name</h3>" sortable="true" style="width:200px;">${ltrn.od_op_name}</display:column>
		            <display:column title="<h3>Receiver Acc No</h3>" sortable="true" style="width:100px;">${ltrn.od_accno}</display:column>
		            <display:column title="<h3>Receiver Name</h3>" sortable="true" style="width:200px;">${ltrn.od_cu_name}</display:column>              
		            <display:column title="<h3>Description</h3>" sortable="true" style="width:250px;">${ltrn.od_desc}</display:column>          
		            <display:column title="<h3>Amount</h3>" sortable="true" style="width:120px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.od_amount}" /></display:column>
		            <display:column title="<h3>Status 1</h3>" sortable="true" style="width:60px; text-align:center;">${ltrn.od_status1}</display:column>
		            <display:column title="<h3>Status 2</h3>" sortable="true" style="width:60px; text-align:center;" >${ltrn.od_status2}</display:column>
        		</display:table>
                </td>
              </tr>
           </table>
         </div>
	     <logic:equal name="konfirmasi" value="err">
	    		<b>${dataresponse}</b>
		</logic:equal>
		</td>
	</tr>
	</table>
	</div>
</body>
</html>