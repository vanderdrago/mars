<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Date" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/toolTipText.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/ReportMCB/css/report/jquery-ui.css">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Informasi Standing Instruction</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<fieldset style="color:black;width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Informasi Standing Instruction</b></legend>
					<form action="standingInstructionAct.do" name="jurnal" method="POST">
						<table border="0">
							<tr>
								<td>
									<input type="text" name="no" id="no" class="inputs" placeholder="No Instruction" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="acc" id="no" class="inputs" placeholder="No Rekening Debet" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td>
									<input type="text" name="acc1" id="no" class="inputs" placeholder="No Rekening Credit" style=" width : 400px;">
								</td>
							</tr>
							<tr>
								<td align="center"><a href="#" onclick="document.forms['jurnal'].submit(); return valdata();" class="blue btn">Search</a></td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color: black;">  		
			<td align="center">
				<table border="0" width="1075">
					<tr>
						<td>
							
						</td>
						<td align="right">
							<c:if test="${sawal >= 1}">Search Result - ${noOfRecords} Result</c:if>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table id="rounded-corner" style="width: 1075px;">
							<thead>
								<tr align="center">
									<th style="width:30px; font-size: 12px;" ><b>NO</b></th>
									<th style="width:100px; font-size: 12px;"><b>BRANCH CODE</b></th>
									<th style="width:150px; font-size: 12px;" ><b>NO INSTRUCTION</b></th>
									<th style="width:100px; font-size: 12px;"><b>ACCOUNT DEBET</b></th>
									<th style="width:100px; font-size: 12px;"><b>ACCOUNT CREDIT</b></th>
									<th style="width:100px; font-size: 12px;"><b>PRODUCT CODE</b></th>
									<th style="width:300px; font-size: 12px;"><b>PRODUCT DESC</b></th>
									<th style="width:100px; font-size: 12px;"><b>CCY</b></th>
									<th style="width:100px; font-size: 12px;"><b>STATUS</b></th>
								</tr>
							</thead>
							<tbody>
								<c:set var="inc" value="0" />
								<c:forEach var="listStanding" items="${listStanding}" >
									<tr>
										<c:set var="inc" value="${inc + 1}" />
										<td>${inc}</td>
										<td>${listStanding.drBranch}</td>
										<td><a href="/ReportMCB/detailInstruction.do?instNo=${listStanding.instructionNo}" target="_blank">${listStanding.instructionNo}</a></td>
										<td>${listStanding.drAccount}</td>
										<td>${listStanding.crAccount}</td>
										<td>${listStanding.productCode}</td>
										<td>${listStanding.productDesc}</td>
										<td>${listStanding.ccy}</td>
										<td>${listStanding.eventCode}</td>
									</tr>
								</c:forEach>
							</tbody>
							</table>
						</td>
					</tr>
				</table>
			</td>
		  </tr>
	</table>
	
</body>
</html>