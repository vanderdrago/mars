<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type='text/javascript' src='/ReportMCB/scripts/money/jquery.js' ></script>
<script type="text/javascript">


function valInput(){
    var bsnsdt;
    bsnsdt = document.getElementById('tgl').value;

    if (bsnsdt == ""){
        alert('Tanggal tidak boleh kosong');
        return false;
    }
    return true;
}


function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div style="font-size: 11px;">
<table border="0" align="center">
	<tr>
		<td>			
			<fieldset style="width:1000px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			<legend> <b>RTGS Outgoing titipan</b> </legend>
			<form action="rtgsTtpnView.do" method="POST" name="jurnal">
				<table border="0">
					
					<tr>
						<td width="90px;">Branch</td>
						<td>:</td>
						<td width="180px;"><input type="text" name="kdcab" id="kdcab" maxlength="3"></td>			
						<td>To Account</td>
						<td>:</td>
						<td><input type="text" name="acc" id="acc" maxlength="24"></td>	
						<td>Amount From</td>
						<td>:</td>
						<td><input type="text" name="amtf" id="amtf" maxlength="17">
							<script type="text/javascript">$("#amtf").maskMoney();</script>
						</td>						
						<td rowspan="4" valign="top" align="left"> 						
							<fieldset style="width: 190px;">									
								<table>
						        <tr>
						            <th ><b>STS</b></th>
						            <th ><b>TOTAL</b></th>
						            <th ><b>NOMINAL</b></th>				            
						        </tr>	
						    	<c:forEach var="lttp" items="${lttp}" >
							        <tr style="font-size: 10px; ">
							            <td >${lttp.status}</td>
							            <td >${lttp.tottrans}</td>
							            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${lttp.totnominal}" /></td>
							        </tr>
							    </c:forEach>
					      </table><br />
								Desc : <br />
								C = Completed; O = Waiting 
							</fieldset>
						</td>	
					</tr>
					<tr>
						<td>Status</td>
						<td>:</td>
						<td><select name="txnsts" id="idtxnsts">
							  <option value="">All</option>
							  <option value="C">Completed</option>
							  <option value="O">Waiting</option>
							</select></td>							
						<td>TRN</td>
						<td>:</td>
						<td><input type="text" name="trn" id="idtrn" maxlength="8"></td>						
						<td>Amount To</td>
						<td>:</td>
						<td><input type="text" name="amtto" id="amtto" maxlength="17">
							<script type="text/javascript">$("#amtto").maskMoney();</script>
						</td>
						
					</tr>
					<tr>
						<td>No. Reference</td>
						<td>:</td>
						<td><input type="text" name="reltrn" id="idreltrn" maxlength="16"></td>								
						<td>To Member</td>
						<td>:</td>
						<td>
		                    <select name="tmmbr" id="idtmmbr" >
		                        <option value="">Pilih</option>
		                        <c:forEach items="${lmmbrbnk}" var="lmmbrbnk">
		                             <option value=${lmmbrbnk.kode_member}>${lmmbrbnk.codename}</option>
		                        </c:forEach>
		                    </select> &nbsp; Code Bank
		                </td>			
						<td colspan="3"></td>	
						
					</tr>
					<tr>
						<td colspan="9"><input onclick="return valInput()"  type="submit" value="Lihat" name="btn" id="idbtn" /></td>
						
					</tr>
				</table>
			</form>
			</fieldset>
		</td>
	</tr>
</table>
<logic:equal name="respon" value="teks">
${teks }
</logic:equal>

<logic:equal name ="data" value="out">
<table border="0" align="center" width="1075px;">
	<tr>
		<td>
			
	<div style="  float:left; text-align: left; ">
	<table border="0">
		<tr>
	    	<td><a href="manageRtgsTtpnFwd.do?page=1">First</a></td>
	    	<td>
	    		<c:if test="${currentPage != 1}">
					<a href="manageRtgsTtpnFwd.do?page=${currentPage - 1}">Previous</a>
				</c:if>
	       	</td>
	       	<td>
		    	<%--For displaying Page numbers.
				The when condition does not display a link for the current page--%>
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<c:forEach begin="${sawal}" end="${sakhir}" var="i">
							<c:choose><td><a href="manageRtgsTtpnFwd.do?page=${i}">${i}</a></td>
							</c:choose>
						</c:forEach>
					</tr>
				</table>
	       	</td>
	       	<td>
	       		<c:if test="${currentPage lt noOfPages}"><a href="manageRtgsTtpnFwd.do?page=${currentPage + 1}">Next</a></c:if>
	       	</td>
	       	<td>
	       		<c:if test="${noOfPages - 9 > 0}"> <a href="manageRtgsTtpnFwd.do?page=${noOfPages - 9}">Last</a></c:if>
	       	</td>
	    </tr>
	 </table>
	</div>
	<div style="width:500px;    float:right; text-align: right"><br />
		<b>Halaman ${sawal}</b>, &nbsp; <b>Ditemukan ${noOfRecords} baris</b>
	</div>
	<br />
	<div style="font-size:6px; float:left;">   
	
	<table id="rounded-corner" >
					    <thead>
					        <tr align="center" style="font-size: 10px;">
					            <th style="width:30px; "><b>NO. REFF</b></th>
					            <th style="width:30px; "><b>TRN</b></th>
					            <th style="width:30px;"><b>TO MEMBER</b></th>
					            <th style="width:30px; "><b>TA NAME</b></th>
					            <th style="width:70px; "><b>AMOUNT</b></th>
					            <th style="width:250px; "><b>DETAIL</b></th>
					            <th style="width:10px; "><b>STS</b></th>
					            <th style="width:60px; "><b>ULTIMATE BENF ACC</b></th>
					            <th style="width:200px; "><b>ULTIMATE BENF NAME</b></th>
					            <th style="width:90px; "><b>TRNS DT</b></th>					            
					        </tr>
					    </thead>
				
					    <tbody>
					    	<c:forEach var="ltrans" items="${ltrans}" >
						        <tr style="font-size: 10px; ">
						            <td >${ltrans.sender_ref_no}</td>
						            <td >${ltrans.trn}</td>
						            <td >${ltrans.to_member}</td>
						            <td >${ltrans.ta_name}</td>
						            <td style="text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrans.amount}" /></td>
						            <td >${ltrans.pay_detail}</td>
						            <td >${ltrans.rtgstmp_sts}</td>
						            <td >${ltrans.ultimate_benef_acc}</td>
						            <td >${ltrans.ultimate_benef_name}</td>
						            <td >${ltrans.insert_dt}</td>
						        </tr>
						    </c:forEach>
					    </tbody>
				      </table>
					    
	</div>			
		</td>
	</tr>
</table>
</logic:equal>

</div>
</body>
</html>