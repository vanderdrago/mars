<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
<meta http-equiv="Content-Type:application/font-woff" content="text/html; charset=ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="/ReportMCB/scripts/bootstrap/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/ReportMCB/sorting/css/styles.css" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/ReportMCB/sorting/js/jquery-1.10.2.min.js"></script>
<script src="/ReportMCB/scripts/js/jquery.tablesorter.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/jquery-ui.css">
<script type='text/javascript' src='/ReportMCB/scripts/smooth/jquery-1.9.1.js'></script>
<script type='text/javascript' src='/ReportMCB/scripts/smooth/jquery-ui.js'></script>
<!-- <link rel="stylesheet" href="/ReportMCB/css/report/demos.css"> -->
<script type="text/javascript">
var countryStateInfo = {
		"JAKARTA 1": {
			"303 - KCU Bumi Serpong Damai (BSD)": {}, "308 - KCU Serang": {}, "310 - KCU Mangga Dua": {}, "312 - KCU Roxy": {},
			"313 - KCU Gajah Mada": {}, "316 - KCU Pluit": {}, "319 - KCU Puri Indah": {}, "325 - KCU Cengkareng": {}, 
			"326 - KCU Tangerang City": {}, "327 - KCU Kedoya": {}, "330 - KCU Bintaro Jaya": {},"333 - KCU Cilegon": {}
		},
		"JAKARTA 2": {
			"121 - KCU Bogor": {}, "301 - KCU Muamalat Tower": {}, "302 - KCU Cipulir": {}, "304 - KCU Fatmawati": {},
			"307 - KCU Depok": {}, "314 - KCU Arthaloka": {}, "324 - KCU Panglima Polim": {}, "328 - KCU Ciledug": {}, 
			"340 - KCU Pondok Indah": {}
		},
		"JAKARTA 3": {
			"305 - KCU Kalimalang": {}, "309 - KCU Rawamangun": {}, "317 - KCU Kemayoran": {}, "318 - KCU Matraman": {}, 
			"320 - KCU Klender": {}, "321 - KCU Kelapa Gading": {}, "322 - KCU Wolter Monginsidi": {}, "323 - KCU Pancoran": {}, 
			"329 - KCU Kalimas Bekasi": {}, "341 - KCU Karawang": {}
		},
		"JABAR":{
			"101 - KCU Bandung": {}, "131 - KCU Cirebon": {},
			"141 - KCU Cianjur": {}, "151 - KCU Tasikmalaya": {}, "161 - KCU Sukabumi": {}
		}, 
		"JATENG & DIY" :{
			"501 - KCU Semarang": {}, "506 - KCU Kudus": {},
			"511 - KCU Pekalongan": {}, "512 - KCU Tegal": {}, "521 - KCU Solo": {},
			"531 - KCU Yogyakarta": {}, "541 - KCU Purwokerto": {}
		},
		"JATIMBALNUSRA" : {
			"701 - KCU Darmo": {}, "702 - KCU KH. Mas Mansyur": {},
			"711 - KCU Malang": {}, "721 - KCU Mataram": {}, "731 - KCU Jember": {},
			"741 - KCU Kediri": {}, "751 - KCU Denpasar": {}, "761 - KCU Kupang": {}, 
			"771 - KCU Surabaya Sungkono": {}
		},
		"KALIMANTAN" : {
			"601 - KCU Balikpapan": {}, "602 - KCU Samarinda": {},
			"611 - KCU Banjarmasin": {}, "621 - KCU Pontianak": {}, "631 - KCU Palangkaraya": {}
		},
		"SULAWESI" : {
			"801 - KCU Makasar": {}, "811 - KCU Gorontalo": {}, "821 - KCU Kendari": {}, "831 - KCU Palu": {},
			"841 - KCU Ternate": {}, "851 - KCU Sorong": {}, "861 - KCU Manado": {},
			"871 - KCU Ambon": {}, "881 - KCU Jayapura": {}, "891 - KCU Mamuju": {}
		},
		"SUMBANGSEL" : {
			"351 - KCU Lampung": {}, "361 - KCU Palembang": {}, "371 - KCU Pangkal Pinang": {}, 
			"421 - KCU Padang": {}, "431 - KCU Bengkulu": {}, "441 - KCU Jambi": {}
		},
		"SUMBAGUT" : {
			"211 - KCU Medan Balaikota": {}, "221 - KCU Pekanbaru": {}, "231 - KCU Padang Sidempuan": {}, 
			"241 - KCU Banda Aceh": {}, "251 - KCU Pematang Siantar": {}, "261 - KCU Langsa": {},
			"411 - KCU Batam": {}, "451 - KCU Tanjung Pinang": {}, "482 - KCU Medan Sudirman": {}
		},
		"KUALA LUMPUR" : {
			"901 - KCU Kuala Lumpur": {}
		}
	}


	window.onload = function () {
		
		//Get html elements
		var countySel = document.getElementById("countySel");
		var stateSel = document.getElementById("stateSel");	
		
		//Load countries
		for (var country in countryStateInfo) {
			countySel.options[countySel.options.length] = new Option(country, country);
		}
		//County Changed
		countySel.onchange = function () {
			 stateSel.length = 1; // remove all options bar first
			 if (this.selectedIndex < 1)
				 return; // done
			 for (var state in countryStateInfo[this.value]) {
				 stateSel.options[stateSel.options.length] = new Option(state, state);
			 }
		}
		
		//State Changed
		stateSel.onchange = function () {	
			 if (this.selectedIndex < 1)
				 return; // done
			 for (var city in countryStateInfo[countySel.value][this.value]) {
				 citySel.options[citySel.options.length] = new Option(city, city);
			 }
		}
	}
</script>


<head>
<title>Laporan Pending Transaksi</title>
</head>
<body>
	<table border="0" width="1075px">
		<tr>
			<td align="center">
		        <form action="pendingTransaksiAct.do" method="POST" name="mgetinstl">	
		        	<fieldset>
		        		<legend>Laporan Pending Transaksi EOD</legend>
						    <div class="box-body">
						    	<div class="col-xs-5">
									<select class="form-control" id="countySel" name="countySel">
										<option selected>Select Regional...</option>
									</select>
								</div>
								<br><br>
								<div class="col-xs-5">
						    		<select class="form-control" id="stateSel" name="stateSel">
										<option selected>Select Branch...</option>
									</select>
						    	</div>
						    	<br><br>
<!-- 						    	<div class="col-xs-5"> -->
<!-- 						    		<input type="text" class="form-control" id="tes" name="tes"> -->
<!-- 						    	</div> -->
<!-- 						    	<br><br> -->
						        <div class="row">
									<div class="col-xs-5" align="left">
										<button class="btn btn-info pull-right" value="Cari" name="btn" id="ok">Submit</button>
<!-- 						    			<button type="submit" class="btn btn-success" value="search" name="btn1" >Text box</button>  -->
									</div>
						    	</div>
						    	<logic:equal name="confrm" value="err"> 
 									<p style="color: red;"> ${respon} </p> 
								</logic:equal> 
							</div>
					</fieldset>
				</form>
			</td>
		</tr>
		<tr style="color: black;">
			<td align="center">
				<table width="1075px">
					<tr>
						<td align="center">
								<table class="table table-striped" id="pending">
									<thead>
										<tr>
											<th>No</th>
											<th>Area</th>
											<th>Branch Code</th>
											<th>Branch Name</th>
											<th>Module</th>
											<th>Ref No</th>
											<th>User Id</th>
											<th>User Name</th>
<!-- 											<th> </th> -->
										</tr>
									</thead>
									<tbody>
										<c:set var="inc" value="0" />
										<c:forEach var="area" items="${area}" >
											<tr>
												<c:set var="inc" value="${inc + 1}" />
												<td>${inc}</td>
												<td>${area.area} </td>
												<td>${area.branch}</td>
												<td>${area.branchName}</td>
												<td>${area.module} </td>
												<td>${area.refNo}</td>
												<td>${area.userId}</td>
												<td>${area.userName}</td>
<!-- 												<td> -->
<%-- 													<a href="/ReportMCB/detailUser.do?userId=${area.userId}" target="_blank" class="btn btn-success" role="button"> Detail <span class="glyphicon glyphicon-list-alt "></span></a> --%>
<!-- 												</td> -->
											</tr>
									</c:forEach>
									</tbody>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	
</body>