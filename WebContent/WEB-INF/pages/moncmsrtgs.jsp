<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type='text/javascript' src='/ReportMCB/scripts/money/jquery.js' ></script>
<script type="text/javascript">

function validatemrtgscm(){
    var amont_a;
    var amont_b;
    var channel;
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    channel = document.getElementById('idchannel').value;
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    if (channel == "" || channel == null){
        alert('Silahkan pilih channel');
        return false;
	}
    return true;
}
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<div style="font-size: 11px;">
<table border="0" align="center">
	<tr>
		<td>
			<fieldset style="width:800px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
			<legend> <b>RTGS/SKN-CMS</b> </legend>
			<form action="/ReportMCB/vMonCmsRTGS.do" method="POST" name="mrtgs">
			<table border="0" style="width: 750px;">
				<tr>
					<td style="width: 80px;">No Referensi</td>
					<td style="width: 5px;">:</td>
					<td style="width: 200px;"><input type="text" name="rel_trn" id="rel_trn"></td>
					<td >Start Amount</td>
					<td style="width: 5px;">:</td>
					<td tyle="width: 200px;">
						<input type="text" name="amont_a" id="amont_a" maxlength="16">
						<script type="text/javascript">$("#amont_a").maskMoney();</script>
						<label style="color: red"></label>
					</td>
					<td >Channel</td>
					<td style="width: 5px;">:</td>
					<td tyle="width: 200px;">
						<select name="channel" id="idchannel">
						  <option value="">Pilih</option>
						  <option value="2">RTGS</option>
						  <option value="1">SKN</option>
						</select>
					</td>
				</tr>			
				<tr>
					<td >From Account</td>
					<td >:</td>
					<td><input type="text" name="rek_f_ac" id="rek_f_ac"
						maxlength="16"><label style="color: red"></label></td>
					<td >To Amount</td>
					<td >:</td>
					<td colspan="6"><input type="text" name="amont_b" id="amont_b" maxlength="16">
						<script type="text/javascript">$("#amont_b").maskMoney();</script>
						<label style="color: red"></label>
					</td>
				</tr>			
				<tr>
					<td >To Account</td>
					<td >:</td>
					<td ><input type="text" name="rek_t_ac" id="rek_t_ac"
						maxlength="16"><label style="color: red"></label></td>
					<td >Status</td>
					<td>:</td>
					<td colspan="6">
						<select name="txnsts" id="idtxnsts">
						  <option value="">All</option>
						  <option value="S">Success</option>
						  <option value="F">Failed</option>
						</select> 
					</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td colspan="7"><input onclick="return validatemrtgscm()" type="submit" value="Ok" name="ok" id="ok" /> <!-- onclick="return validatemrtgs()" -->
					</td>
				</tr>
			</table>
			</form>
			</fieldset>
		</td>
	</tr>
</table>
</div>

<logic:equal value="view" name="data">
	<h2 align="center">Monitoring & Reporting RTGS CMS</h2>
	<div style="font-size: 13px; font-family: times new roman;">
	<table id="rounded-corner" style="width: 100%">
		<tr>
			<td><display:table name="ltrn" id="ltrn" class="wb" requestURI="MonRtgsCmsForward.do" pagesize="30" sort="external">
				<display:column title="<h3>Tanggal Trn</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.dateTrx}</display:column>
				<display:column title="<h3>Ref ID </h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.refId}</display:column>
				<display:column title="<h3>Company Code</h3>" sortable="true" style="width:100px; ">${ltrn.companyCode}</display:column> 
				<display:column title="<h3>Nama</h3>" sortable="true" style="width:170px; ">${ltrn.name}</display:column>         
				<display:column title="<h3>Type Transfer</h3>" sortable="true" style="width:100px; text-align:center;">${ltrn.transferType}</display:column>
				<display:column title="<h3>Amount</h3>" sortable="true" style="width:110px; text-align:right;" ><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.amount}" /></display:column>
				<display:column title="<h3>Charge</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.charge}</display:column>
				<display:column title="<h3>Status </h3>" sortable="true" style="width:85px; text-align:center;">${ltrn.status}</display:column>
				<display:column title="<h3>From Account</h3>" sortable="true" style="width:80px; ">${ltrn.accountNo}</display:column> 
				<display:column title="<h3>To Account</h3>" sortable="true" style="width:170px; ">${ltrn.toAccountNo}</display:column>         
				<display:column title="<h3>Nama</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.toName}</display:column>
				<display:column title="<h3>Bank</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.toBankName}</display:column>
				</display:table>
			</td>
		</tr>
	</table>
	</div>
</logic:equal>

<logic:equal name="konfirmasi" value="err">
   		<b>${dataresponse}</b>
</logic:equal>
</body>
</html>