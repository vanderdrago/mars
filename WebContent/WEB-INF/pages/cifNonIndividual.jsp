<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center">
				<div style="font-size: 13px; font-family: times new roman;">
					<table id="rounded-corner" style="width:1075px;">
						<tr>
							<td align="center">
								<div style="font-size: 20px; font-family: times new roman;">LIST PROBLEM CIF NON INDIVIDUAL</div>
							</td>
						</tr>
						<tr>
							<td>
								<display:table name="lphNon" id="lphNon" requestURI="ManageNonIndividual.do" pagesize="30" sort="external">
									<display:column title="<b>RULE GROUP NO</b>"style="width:100px;">${lphNon.profile_rule_group_no}</display:column>
									<display:column title="<b>RULE GROUP NAME</b>"style="width:200px;">${lphNon.profile_rule_group_name}</display:column>
									<display:column title="<b>RULE NO</b>"style="width:50px;">${lphNon.profile_rule_no}</display:column>
									<display:column title="<b>RULE DESC</b>"style="width:725px;">${lphNon.profile_rule_desc}</display:column>
								</display:table>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>