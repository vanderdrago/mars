<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel='stylesheet' href='/ReportMCB/css/report/rpt_jrnl.css'>
<link href="/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/stylePaging.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/buttonStyle.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/text.css" rel="stylesheet" type="text/css"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Laporan Perubahan Data Cif</title>
</head>
<body>
	<table border="0" width="80%">
		<tr>
			<td align="center" colspan="2">
				<fieldset style="color:black; width:350px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
					<legend align="center"><b>Laporan Perubahan Data No Cif</b></legend>
					<form action="LapPerubahanDataCifAct.do" name="jurnal" method="POST">
						<table>
							<tr>
								<td><input type="text" name="branch" id="branch" class="inputs" placeholder="Cabang" style=" width : 320px;"/>
							</tr>
							<tr>
								<td><input type="text" name="cif" id="cif" class="inputs" placeholder="No CIF" style=" width : 320px;"/>
							</tr>
							<tr>
								<td align="right">
									<a href="#" onclick="document.forms['jurnal'].submit(); return false;" class="blue btn">Search</a> 
								</td>
							</tr>
						</table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr style="color: black">
			<td align="center">
			<table border="0" width="1075px">
				<tr>
					<td>
					<table border = "0">
						<tr>
							<td>
								<c:if test="${sawalCif >= 1}">Page ${sawalCif} Of ${noOfPagesCif}</c:if>
							</td>
							<td>
								<c:if test="${sawalCif >= 1}"><a href="manageLphCifFwd.do?page=1" class="page">First</a></c:if>
							</td>
							<td>
								<c:if test="${currentPageCif != 1 && (currentPageCif - 1 > 0)}">
									<a href="manageLphCifFwd.do?page=${currentPageCif - 1}" class="page">Previous</a>
								</c:if>
							</td>
							<td>
								<table>
									<tr>
										<td>
											<c:if test="${sawalCif >= 1}">
					 	  	  	    		<c:forEach begin="${sawalCif}" end="${sakhirCif}" var="i">
					 	  	  	  			<c:choose><td><a href="manageLphCifFwd.do?page=${i}" class="page">${i}</a></td></c:choose>
					 	  	  	   	    	</c:forEach></c:if>
										</td>
									</tr>
								</table>
							</td>
							<td>
								<c:if test="${currentPageCif lt noOfPagesCif}">
		         					<a href="manageLphCifFwd.do?page=${currentPageCif + 1}" class="page">Next</a>
				  				</c:if>
							</td>
							<td>
								<c:if test="${noOfPagesCif - 9 > 0}">
		       						<a href="manageLphCifFwd.do?page=${currentPageCif - 9}"class="page">Last</a>
		       		  			</c:if>
							</td>
						</tr>
					</table>
					</td>
					<td align="right">
						<c:if test="${sawalCif >= 1}">Search Result - ${noOfRecordsCif} Result</c:if>	
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<div style="font-size:13px; font-family:times new roman;">
						<table id="rounded-corner" style="width:1075px" > 
							<thead>
							<tr align="center">
								<th style="width:1px; font-size: 12px;"><b>NO.</b></th>
								<th style="width:50px; font-size: 12px;"><b>BRANCH</b></th>
								<th style="width:75px; font-size: 12px;"><b>ACC NO </b></th>
								<th style="width:150px; font-size: 12px;"><b>NAMA NASABAH</b></th>
								<th style="width:75px; font-size: 12px;"><b>FASTPATH</b></th>
								<th style="width:150px; font-size: 12px;"><b>DATA LAMA</b></th>
								<th style="width:150px; font-size: 12px;"><b>DATA BARU</b></th>
								<th style="width:25px; font-size: 12px;"><b>MODIF KE</b></th>
								<th style="width:100px; font-size: 12px;"><b>KETERANGAN</b></th>
								<th style="width:100px; font-size: 12px;"><b>MAKER DATE</b></th>
								<th style="width:100px; font-size: 12px;"><b>MAKER ID</b></th>
								<th style="width:100px; font-size: 12px;"><b>CHECKER ID</b></th>
							</tr>
						</thead>
						<tbody>
							<c:set var="inc" value="0" />
							<c:forEach var="lphLapCif" items="${lphLapCif}" >
							<tr style="font-size: 11px;">
								<c:set var="inc" value="${inc + 1}" />
								<td>${inc}</td>
								<td>${lphLapCif.branch}</td>
								<td>${lphLapCif.noCif}</td>
								<td>${lphLapCif.nama_nasabah}</td>
								<td>${lphLapCif.fastpath}</td>
								<td>${lphLapCif.data_lama}</td>
								<td>${lphLapCif.data_baru}</td>
								<td>${lphLapCif.modif_ke}</td>
								<td>${lphLapCif.maker_date}</td>
								<td>${lphLapCif.keterangan}</td>
								<td>${lphLapCif.maker_id}</td>
								<td>${lphLapCif.checker_id}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>