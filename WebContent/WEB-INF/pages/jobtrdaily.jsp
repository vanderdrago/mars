<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">
<script type="text/javascript">

$(function() {
   $( "#day" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        dateFormat: 'yymmdd'
    });
   
});
function validateLap(){
    var day;
    
    day = document.getElementById('day').value;
        
    if (day == ""){
        alert('Silahkan isi tanggal');
        return false;
    }
    return true;
}

$(function() {
    $( "#lhr" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
  		yearRange: "1925:2013",
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd',
        changeYear: true
    });


});

function checkDec(el){
    var ex = /^[0-9]+\.?[0-9]*$/;
    if(ex.test(el.value)==false){
        el.value = el.value.substring(0, el.value.length - 1);
    }
}

</script>
<table border="0" width="100%">
  <tr>
    <td align="center" colspan="2">
    	<fieldset style="width:450px; border:0px solid #999; border-radius:8px; box-shadow:0 0 20px #999;">
		    <legend> <b>Job Daily Reports</b> </legend>
		     <form action="/ReportMCB/viewJDAILYReport.do" method="POST" name="jurnal">
		    <table>
		        <tr>
            <td colspan="3">Field Job Daily Report</td>
        </tr>
        <tr>
            <td>Date</td>
            <td>:</td>
            <td><input type="text" name="day" id="day" readonly="true"><label style="color:red">*</label>&nbsp;yyyyMMdd</td>
        </tr>
        <tr>
            <td colspan="2"> </td>
            <td>
                <input onclick="return validateLap()" type="submit" value="View" name="btn" id="ok" />
            </td>
            </tr>
		    </table>
		    </form>
		</fieldset>		    	
    </td>
  </tr>
</table>

<div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:1070px">
               <tr ><td>
		            <display:table name="ltrn" id="ltrn" class="wb" requestURI="jdailyRepFwd.do" pagesize="20" sort="external" >
		            <display:column title="<h3>Date</h3>" sortable="true" style="width:45px; text-align:center;">${ltrn.day}</display:column>
		            <display:column title="<h3>Job Code</h3>" sortable="true" style="width:55px;">${ltrn.job_code}</display:column>
		            <display:column title="<h3>Job Name</h3>" sortable="true" style="width:200px;">${ltrn.job_name}</display:column>
		            <display:column title="<h3>Module</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.module}</display:column>
		            <display:column title="<h3>Runjob Flag</h3>" sortable="true" style="width:70px; text-align:center;">${ltrn.runjob_flag}</display:column>
		            <display:column title="<h3>Start Date</h3>" sortable="true" style="width:110px; text-align:center;">${ltrn.start_date}</display:column>
		            <display:column title="<h3>End Date</h3>" sortable="true" style="width:110px; text-align:center;">${ltrn.end_date}</display:column>
		            <display:column title="<h3>Duration</h3>" sortable="true" style="width:55px; text-align:center;">${ltrn.duration}</display:column>
		            <display:column title="<h3>Status</h3>" sortable="true" style="width:30px; text-align:center;" >${ltrn.status}</display:column>
			        <display:column title="<h3>Description</h3>" sortable="true" style="width:250px; text-align:center;">${ltrn.description}</display:column>
		            </display:table>
                    </td>
               </tr>
            </table>
        </div>
<logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
</logic:equal>