<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type='text/javascript' src='/ReportMCB/scripts/common.js'></script>
<link rel="stylesheet" href="/ReportMCB/css/jquery-ui-1.8.16.custom.css">
<script type='text/javascript' src="/ReportMCB/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" href="/ReportMCB/css/report/rpt_jrnl.css">

<html>
<script type="text/javascript">

function validatemskn(){
    var amont_a;
    var amont_b;
        
    amont_a = parseFloat(document.getElementById('amont_a').value);
    amont_b = parseFloat(document.getElementById('amont_b').value);
    if ( !isNaN(amont_a) && isNaN(amont_b)) {
        alert('To Amount Harus diisi');
        return false;
    }
    if (isNaN(amont_a) && !isNaN(amont_b)){   
        alert('Start Amount Harus diisi');
        return false;
    }
    if (amont_a > amont_b ){   
        alert('Start Amount harus lebih kecil dari To Amount');
        return false;
    }
    return true;
}
</script>

<head>
<style type="text/css">
            body {font-family:times new roman;}
            .td1 {width:150px;}
            .td2 {width:5px;}
        </style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<h2 align="center">Monitoring & Reporting SKN Interface Incoming</h2>
<a href="bukujurnal.do"><span>Buku Jurnal MCB</span></a>
        <form action="/ReportMCB/vMonSKNIn.do" method="POST" name="mskn">
	    
        <table border="0" width="100%">
            <tr>
            	<td style="width: 240px">Ref No</td>
            	<td style="width: 21px">:</td>
                <td><input type="text" name="refno" id="refno" ><label style="color:red">
                </td>
            </tr>
            <tr>
            	<td style="width: 240px">Benef Acc No</td>
            	<td style="width: 21px">:</td>
                <td><input type="text" name="benacc" id="benacc" ><label style="color:red">
                </td>
            </tr>
            <tr>
				<td style="width: 240px">Start Amount</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="amont_a" id="amont_a"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
			<tr>
				<td style="width: 240px">To Amount</td>
				<td style="width: 21px">:</td>
				<td><input type="text" name="amont_b" id="amont_b"
					maxlength="16"><label style="color: red"></label></td>
			</tr>
            
             <tr>
            <td colspan="2"> </td>
            <td>
                <input onclick="return validatemskn()" type="submit" value="Ok" name="ok" id="ok" />
                <!-- onclick="return validatemrtgs()" -->
            </td>
            </tr>
        </table>
        </form>
            <br />
       <div style="font-size:13px; font-family:times new roman;">

           <table id="rounded-corner" style="width:1740px">
               <tr ><td>
		            <display:table name="ltrn" id="ltrn" class="wb" requestURI="MonSknInForward.do" pagesize="20" sort="external" >
		            <display:column title="<h3>Ref No</h3>" sortable="true" style="width:70px;">${ltrn.id_ref_no}</display:column>
		            <display:column title="<h3>Sender Name</h3>" sortable="true" style="width:170px;">${ltrn.id_sender_name}</display:column>
		            <display:column title="<h3>Benef Acc No</h3>" sortable="true" style="width:100px;">${ltrn.id_benef_account}</display:column>
		            <display:column title="<h3>Benef Name</h3>" sortable="true" style="width:170px;">${ltrn.id_benef_name}</display:column>              
		            <display:column title="<h3>Benef Acc Name</h3>" sortable="true" style="width:170px;">${ltrn.id_benef_account_name}</display:column>              
		            <display:column title="<h3>Remark</h3>" sortable="true" style="width:250px;">${ltrn.id_remark}</display:column>          
		            <display:column title="<h3>Amount</h3>" sortable="true" style="width:150px; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ltrn.id_amount}" /></display:column>
		            <display:column title="<h3>Sender Region</h3>" sortable="true" style="width:60px; text-align:center;">${ltrn.id_sender_region_code}</display:column>
		            <display:column title="<h3>Benef Region</h3>" sortable="true" style="width:60px; text-align:center;" >${ltrn.id_benef_region_code}</display:column>
        			<display:column title="<h3>Sender Clearing</h3>" sortable="true" style="width:60px; text-align:center;" >${ltrn.id_sender_clearing_code}</display:column>
        			<display:column title="<h3>SOR</h3>" sortable="true" style="width:60px; text-align:center;" >${ltrn.id_sor}</display:column>
        			<display:column title="<h3>Branch</h3>" sortable="true" style="width:60px; text-align:center;" >${ltrn.id_br_code}</display:column>
        			<display:column title="<h3>Reject Desc</h3>" sortable="true" style="width:160px; text-align:center;" >${ltrn.id_reject_desc}</display:column>
        			<display:column title="<h3>Status Host</h3>" sortable="true" style="width:60px; text-align:center;" >${ltrn.id_status_host}</display:column>
        			<display:column title="<h3>User ID</h3>" sortable="true" style="width:70px; text-align:center;" >${ltrn.id_verify_user}</display:column>
        			<display:column title="<h3>Auth ID</h3>" sortable="true" style="width:70px; text-align:center;" >${ltrn.id_auth_user}</display:column>
        		</display:table>
                </td>
                </tr>
           </table>
        </div>
     <logic:equal name="konfirmasi" value="err">
    		<b>${dataresponse}</b>
	</logic:equal>
</body>
</html>