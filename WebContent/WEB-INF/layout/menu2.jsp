<%--      Document   : menu2     Created on : Oct 13, 2010, 4:40:37 PM     Author     : User --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"     "http://www.w3.org/TR/html4/loose.dtd">
<link id="navxpSkin1639482173" rel="stylesheet" type="text/css" href="/ReportMCB/css/menutab/cache_skin_KFCorp-Green.css">

<script src="/ReportMCB/scripts/menutab/dnncore.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/NavXpJsClient--326194851.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/ScriptResource_002.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/ScriptResource.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/dnn_002.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/dnn_003.js" type="text/javascript"></script>

<!-- <script type="text/javascript" src="/ReportMCB/scripts/script.js"></script> -->
<!-- <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> -->
<!-- <link href="/ReportMCB/scripts/scriptcss.css" rel="stylesheet" type="text/css"/> -->
<link href="/ReportMCB/scripts/menu.css" rel="stylesheet" type="text/css"/>
<!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!--   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.muamalat.reportmcb.entity.Role" %>
<%@ page import="com.muamalat.reportmcb.entity.RoleMenu" %>
<%@ page import="com.muamalat.reportmcb.entity.RoleSubMenu" %>
<!-- Custom Navigation -->
<!-- 	<link href="/scripts/tabmenu.css" rel="stylesheet" type="text/css"/> -->
<link href="Kalbe%20Corporate%20Portal%20%29%20Home_files/style.css" rel="stylesheet" type="text/css" media="screen">

<logic:present name="userlogin">
<div id="dnn_ctr1956_ContentPane">
	<div id="dnn_ctr1956_ModuleContent" class="avt_NavXpContent">
	<div id="dnn_ctr1956_NavXp_pnlErr"></div>
	<div id="dnn_ctr1956_NavXp_menuRender" style="width:1095px;">
	<div class="navxp_MG_3DColumnsDD navxp_MG_3DColumnsDD_KFCorp-Green NavXpRoot">
	<div class="startBk">
	<div class="endBk">
	<div class="itemsBk">
<div id='cssmenu'>
<ul class="menuH decor1">
	<% 
		RoleMenu header = new RoleMenu();
		RoleSubMenu subMenu = new RoleSubMenu();
		Role detail = new Role();
		ArrayList lrolesHeader = new ArrayList();
		if (request.getSession().getAttribute("lrolesHeader") !=null){
			lrolesHeader = (ArrayList) request.getSession().getAttribute("lrolesHeader");
		for (int i = 0; i < lrolesHeader.size(); i++){
			header = (RoleMenu)lrolesHeader.get(i);
	%>
        <li style="margin-left:0px;"><a href='#'><%=header.getHeaderName() %></a>
            <ul>
            	<%
					ArrayList lrolesSubMenu = header.getSubSubMenu();
					for (int j = 0; j < lrolesSubMenu.size(); j++){
						subMenu = (RoleSubMenu)lrolesSubMenu.get(j);
				%>
                <li><a href='#'><%=subMenu.getSubMenu() %></a>
                    <ul>
                    	<%
						ArrayList lrolesDetail = subMenu.getDetail();
						for (int k = 0; k < lrolesDetail.size(); k++){
							detail = (Role)lrolesDetail.get(k);
						%>
                        <li><a href='<%=detail.getFunc_act() %>'><%=detail.getFunc_nm() %></a></li>
                       <%} %>
                    </ul>
                    <%} %>
                </li>
            </ul>
        </li>
        
        <%
	}
}
%> 
<li><a href="LogoutProcess.do">Logout</a></li>

</ul>

	
</div>
</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
</logic:present>
