<%--      Document   : menu2     Created on : Oct 13, 2010, 4:40:37 PM     Author     : User --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"     "http://www.w3.org/TR/html4/loose.dtd">
<link id="navxpSkin1639482173" rel="stylesheet" type="text/css" href="/ReportMCB/css/menutab/cache_skin_KFCorp-Green.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,700' rel='stylesheet' type='text/css'>
<script src="/ReportMCB/scripts/menutab/dnncore.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/NavXpJsClient--326194851.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/ScriptResource_002.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/ScriptResource.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/dnn_002.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/menutab/dnn_003.js" type="text/javascript"></script>
<script src="/ReportMCB/scripts/modernizr.js.js" type="text/javascript"></script>

<link href="/ReportMCB/scripts/stylemenubaru.css" rel="stylesheet" type="text/css"/>
<link href="/ReportMCB/scripts/reset.css" rel="stylesheet" type="text/css"/>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.muamalat.reportmcb.entity.Role" %>
<%@ page import="com.muamalat.reportmcb.entity.RoleMenu" %>
<%@ page import="com.muamalat.reportmcb.entity.RoleSubMenu" %>
<!-- Custom Navigation -->
<!-- 	<link href="/scripts/tabmenu.css" rel="stylesheet" type="text/css"/> -->
<link href="Kalbe%20Corporate%20Portal%20%29%20Home_files/style.css" rel="stylesheet" type="text/css" media="screen">

<logic:present name="userlogin">
	<div id="dnn_ctr1956_ContentPane">
	<div id="dnn_ctr1956_ModuleContent" class="avt_NavXpContent">
	<div id="dnn_ctr1956_NavXp_pnlErr"></div>
	<div id="dnn_ctr1956_NavXp_menuRender" style="width:1095px;">
	<div class="navxp_MG_3DColumnsDD navxp_MG_3DColumnsDD_KFCorp-Green NavXpRoot">
	<div class="startBk">
	<div class="endBk">
	<div class="itemsBk">
<div id='cssmenu'>	
	<header>
		<a id="cd-menu-trigger" href="#0"><span class="cd-menu-text">Menu</span><span class="cd-menu-icon"></span></a>
	</header>
	<nav id="cd-lateral-nav">
		<ul class="cd-navigation">
			<li class="item-has-children">
				<a href="#0">Services</a>
				<ul class="sub-menu">
					<li><a href="#0">Brand</a></li>
					<li><a href="#0">Web Apps</a></li>
					<li><a href="#0">Mobile Apps</a></li>
				</ul>
			</li> <!-- item-has-children -->

			<li class="item-has-children">
				<a href="#0">Products</a>
				<ul class="sub-menu">
					<li class="item-has-children"><a href="#0">Product 1</a>
						<ul class="sub-menu">
							<li><a href="#0">Product tes</a></li>
						</ul>
					</li>
					<li><a href="#0">Product 2</a></li>
					<li><a href="#0">Product 3</a></li>
					<li><a href="#0">Product 4</a></li>
					<li><a href="#0">Product 5</a></li>
				</ul>
			</li> <!-- item-has-children -->

			<li class="item-has-children">
				<a href="#0">Stockists</a>
				<ul class="sub-menu">
					<li><a href="#0">London</a></li>
					<li><a href="#0">New York</a></li>
					<li><a href="#0">Milan</a></li>
					<li><a href="#0">Paris</a></li>
				</ul>
			</li> <!-- item-has-children -->
		</ul> <!-- cd-navigation -->
		
	</nav>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
</logic:present>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/ReportMCB/scripts/js/main.js" type="text/javascript"></script>