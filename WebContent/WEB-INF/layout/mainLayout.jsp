<%-- 
    Document   : mainLayout
    Created on : Oct 13, 2010, 3:16:07 PM
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="scripts/style.css" type="text/css" />
        <!--  <title><tiles:getAsString name="title" /></title>  -->
        <title>Report MCB</title> 
        <script type="text/javascript" src="/ReportMCB/dwr/engine.js"></script>
        <script type="text/javascript" src="/ReportMCB/dwr/util.js"></script>

        <script type="text/javascript" src="scripts/NumberFormat.js"></script>

        <link type="text/css" href="css/menu.css" rel="stylesheet" />
        <script type="text/javascript" src="scripts/jquery.js"></script>
        <script type="text/javascript" src="scripts/menu.js"></script>

        <script type="text/javascript">
            function removeComma(str){
                var newStr = str;
                for(i=0;i<str.length;i++){
                    newStr = newStr.replace(",","");
                }
                return newStr;
            }
        </script>
    </head>
    <body style="margin:0" >
        <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
            <tbody>
            	<tr >
                    <td align="center"><tiles:insert attribute="header" /></td>
                </tr>
                <tr>
                    <td valign="top" align="center"><tiles:insert attribute="menu" />
                        <div style="visibility:hidden"> <a href="http://apycom.com/">jQuery Menu by Apycom</a></div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" height="350px;" align="center">
                    <div style="margin: 10px;">
                        <tiles:insert attribute="body" />
                    </div>
                    <br />
                    </td>
                </tr>
                <tr >
                    <td align="center" >
                    	<table width="1075px;">
                    		<tr>
                    			<td align="center" >
                    	<div style="background-image: url(/ReportMCB/css/images/menutab/footer.jpg); 
                    		height: 100px; width:100%; background-repeat: no-repeat;"><br /><br /><br /><br />
                    	<table width="100%">
                    	<tr>
                    		<td style=" width:80%; color: #5A2380; font-size: 11px; text-align: center; " >©2016 PT. Bank Muamalat Tbk. All Rights Reserved. <br/>Version 2.0</td>
                    	</tr>
                    	</table>
                    	</div> 
                    	</td>
                    		</tr>
                    	</table>               		 
                    </td>
                </tr>
	</tbody>
        </table>
    </body>
</html>
