/**
 * InputTransferMain.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bmi.soa;

public class InputTransferMain  implements java.io.Serializable {
    private java.lang.String MTID1;
    private java.lang.String PAN2;
    private java.lang.String trCd3;
    private java.lang.String amount4;
    private java.lang.String STAN11;
    private java.lang.String merType18;
    private java.lang.String fee28;
    private java.lang.String acqInsId32;
    private java.lang.String chCd33;
    private java.lang.String RRN37;
    private java.lang.String termId41;
    private java.lang.String accIdCd42;
    private java.lang.String acceptAddr43;
    private java.lang.String destBankCd47;
    private java.lang.String desc48;
    private java.lang.String curr49;
    private java.lang.String phnNmbr62;
    private java.lang.String originalDataElement90;
    private java.lang.String repAmount95;
    private java.lang.String FA102;
    private java.lang.String TA103;
    private java.lang.String subTrancode105;

    public InputTransferMain() {
    }

    public InputTransferMain(
           java.lang.String MTID1,
           java.lang.String PAN2,
           java.lang.String trCd3,
           java.lang.String amount4,
           java.lang.String STAN11,
           java.lang.String merType18,
           java.lang.String fee28,
           java.lang.String acqInsId32,
           java.lang.String chCd33,
           java.lang.String RRN37,
           java.lang.String termId41,
           java.lang.String accIdCd42,
           java.lang.String acceptAddr43,
           java.lang.String destBankCd47,
           java.lang.String desc48,
           java.lang.String curr49,
           java.lang.String phnNmbr62,
           java.lang.String originalDataElement90,
           java.lang.String repAmount95,
           java.lang.String FA102,
           java.lang.String TA103,
           java.lang.String subTrancode105) {
           this.MTID1 = MTID1;
           this.PAN2 = PAN2;
           this.trCd3 = trCd3;
           this.amount4 = amount4;
           this.STAN11 = STAN11;
           this.merType18 = merType18;
           this.fee28 = fee28;
           this.acqInsId32 = acqInsId32;
           this.chCd33 = chCd33;
           this.RRN37 = RRN37;
           this.termId41 = termId41;
           this.accIdCd42 = accIdCd42;
           this.acceptAddr43 = acceptAddr43;
           this.destBankCd47 = destBankCd47;
           this.desc48 = desc48;
           this.curr49 = curr49;
           this.phnNmbr62 = phnNmbr62;
           this.originalDataElement90 = originalDataElement90;
           this.repAmount95 = repAmount95;
           this.FA102 = FA102;
           this.TA103 = TA103;
           this.subTrancode105 = subTrancode105;
    }


    /**
     * Gets the MTID1 value for this InputTransferMain.
     * 
     * @return MTID1
     */
    public java.lang.String getMTID1() {
        return MTID1;
    }


    /**
     * Sets the MTID1 value for this InputTransferMain.
     * 
     * @param MTID1
     */
    public void setMTID1(java.lang.String MTID1) {
        this.MTID1 = MTID1;
    }


    /**
     * Gets the PAN2 value for this InputTransferMain.
     * 
     * @return PAN2
     */
    public java.lang.String getPAN2() {
        return PAN2;
    }


    /**
     * Sets the PAN2 value for this InputTransferMain.
     * 
     * @param PAN2
     */
    public void setPAN2(java.lang.String PAN2) {
        this.PAN2 = PAN2;
    }


    /**
     * Gets the trCd3 value for this InputTransferMain.
     * 
     * @return trCd3
     */
    public java.lang.String getTrCd3() {
        return trCd3;
    }


    /**
     * Sets the trCd3 value for this InputTransferMain.
     * 
     * @param trCd3
     */
    public void setTrCd3(java.lang.String trCd3) {
        this.trCd3 = trCd3;
    }


    /**
     * Gets the amount4 value for this InputTransferMain.
     * 
     * @return amount4
     */
    public java.lang.String getAmount4() {
        return amount4;
    }


    /**
     * Sets the amount4 value for this InputTransferMain.
     * 
     * @param amount4
     */
    public void setAmount4(java.lang.String amount4) {
        this.amount4 = amount4;
    }


    /**
     * Gets the STAN11 value for this InputTransferMain.
     * 
     * @return STAN11
     */
    public java.lang.String getSTAN11() {
        return STAN11;
    }


    /**
     * Sets the STAN11 value for this InputTransferMain.
     * 
     * @param STAN11
     */
    public void setSTAN11(java.lang.String STAN11) {
        this.STAN11 = STAN11;
    }


    /**
     * Gets the merType18 value for this InputTransferMain.
     * 
     * @return merType18
     */
    public java.lang.String getMerType18() {
        return merType18;
    }


    /**
     * Sets the merType18 value for this InputTransferMain.
     * 
     * @param merType18
     */
    public void setMerType18(java.lang.String merType18) {
        this.merType18 = merType18;
    }


    /**
     * Gets the fee28 value for this InputTransferMain.
     * 
     * @return fee28
     */
    public java.lang.String getFee28() {
        return fee28;
    }


    /**
     * Sets the fee28 value for this InputTransferMain.
     * 
     * @param fee28
     */
    public void setFee28(java.lang.String fee28) {
        this.fee28 = fee28;
    }


    /**
     * Gets the acqInsId32 value for this InputTransferMain.
     * 
     * @return acqInsId32
     */
    public java.lang.String getAcqInsId32() {
        return acqInsId32;
    }


    /**
     * Sets the acqInsId32 value for this InputTransferMain.
     * 
     * @param acqInsId32
     */
    public void setAcqInsId32(java.lang.String acqInsId32) {
        this.acqInsId32 = acqInsId32;
    }


    /**
     * Gets the chCd33 value for this InputTransferMain.
     * 
     * @return chCd33
     */
    public java.lang.String getChCd33() {
        return chCd33;
    }


    /**
     * Sets the chCd33 value for this InputTransferMain.
     * 
     * @param chCd33
     */
    public void setChCd33(java.lang.String chCd33) {
        this.chCd33 = chCd33;
    }


    /**
     * Gets the RRN37 value for this InputTransferMain.
     * 
     * @return RRN37
     */
    public java.lang.String getRRN37() {
        return RRN37;
    }


    /**
     * Sets the RRN37 value for this InputTransferMain.
     * 
     * @param RRN37
     */
    public void setRRN37(java.lang.String RRN37) {
        this.RRN37 = RRN37;
    }


    /**
     * Gets the termId41 value for this InputTransferMain.
     * 
     * @return termId41
     */
    public java.lang.String getTermId41() {
        return termId41;
    }


    /**
     * Sets the termId41 value for this InputTransferMain.
     * 
     * @param termId41
     */
    public void setTermId41(java.lang.String termId41) {
        this.termId41 = termId41;
    }


    /**
     * Gets the accIdCd42 value for this InputTransferMain.
     * 
     * @return accIdCd42
     */
    public java.lang.String getAccIdCd42() {
        return accIdCd42;
    }


    /**
     * Sets the accIdCd42 value for this InputTransferMain.
     * 
     * @param accIdCd42
     */
    public void setAccIdCd42(java.lang.String accIdCd42) {
        this.accIdCd42 = accIdCd42;
    }


    /**
     * Gets the acceptAddr43 value for this InputTransferMain.
     * 
     * @return acceptAddr43
     */
    public java.lang.String getAcceptAddr43() {
        return acceptAddr43;
    }


    /**
     * Sets the acceptAddr43 value for this InputTransferMain.
     * 
     * @param acceptAddr43
     */
    public void setAcceptAddr43(java.lang.String acceptAddr43) {
        this.acceptAddr43 = acceptAddr43;
    }


    /**
     * Gets the destBankCd47 value for this InputTransferMain.
     * 
     * @return destBankCd47
     */
    public java.lang.String getDestBankCd47() {
        return destBankCd47;
    }


    /**
     * Sets the destBankCd47 value for this InputTransferMain.
     * 
     * @param destBankCd47
     */
    public void setDestBankCd47(java.lang.String destBankCd47) {
        this.destBankCd47 = destBankCd47;
    }


    /**
     * Gets the desc48 value for this InputTransferMain.
     * 
     * @return desc48
     */
    public java.lang.String getDesc48() {
        return desc48;
    }


    /**
     * Sets the desc48 value for this InputTransferMain.
     * 
     * @param desc48
     */
    public void setDesc48(java.lang.String desc48) {
        this.desc48 = desc48;
    }


    /**
     * Gets the curr49 value for this InputTransferMain.
     * 
     * @return curr49
     */
    public java.lang.String getCurr49() {
        return curr49;
    }


    /**
     * Sets the curr49 value for this InputTransferMain.
     * 
     * @param curr49
     */
    public void setCurr49(java.lang.String curr49) {
        this.curr49 = curr49;
    }


    /**
     * Gets the phnNmbr62 value for this InputTransferMain.
     * 
     * @return phnNmbr62
     */
    public java.lang.String getPhnNmbr62() {
        return phnNmbr62;
    }


    /**
     * Sets the phnNmbr62 value for this InputTransferMain.
     * 
     * @param phnNmbr62
     */
    public void setPhnNmbr62(java.lang.String phnNmbr62) {
        this.phnNmbr62 = phnNmbr62;
    }


    /**
     * Gets the originalDataElement90 value for this InputTransferMain.
     * 
     * @return originalDataElement90
     */
    public java.lang.String getOriginalDataElement90() {
        return originalDataElement90;
    }


    /**
     * Sets the originalDataElement90 value for this InputTransferMain.
     * 
     * @param originalDataElement90
     */
    public void setOriginalDataElement90(java.lang.String originalDataElement90) {
        this.originalDataElement90 = originalDataElement90;
    }


    /**
     * Gets the repAmount95 value for this InputTransferMain.
     * 
     * @return repAmount95
     */
    public java.lang.String getRepAmount95() {
        return repAmount95;
    }


    /**
     * Sets the repAmount95 value for this InputTransferMain.
     * 
     * @param repAmount95
     */
    public void setRepAmount95(java.lang.String repAmount95) {
        this.repAmount95 = repAmount95;
    }


    /**
     * Gets the FA102 value for this InputTransferMain.
     * 
     * @return FA102
     */
    public java.lang.String getFA102() {
        return FA102;
    }


    /**
     * Sets the FA102 value for this InputTransferMain.
     * 
     * @param FA102
     */
    public void setFA102(java.lang.String FA102) {
        this.FA102 = FA102;
    }


    /**
     * Gets the TA103 value for this InputTransferMain.
     * 
     * @return TA103
     */
    public java.lang.String getTA103() {
        return TA103;
    }


    /**
     * Sets the TA103 value for this InputTransferMain.
     * 
     * @param TA103
     */
    public void setTA103(java.lang.String TA103) {
        this.TA103 = TA103;
    }


    /**
     * Gets the subTrancode105 value for this InputTransferMain.
     * 
     * @return subTrancode105
     */
    public java.lang.String getSubTrancode105() {
        return subTrancode105;
    }


    /**
     * Sets the subTrancode105 value for this InputTransferMain.
     * 
     * @param subTrancode105
     */
    public void setSubTrancode105(java.lang.String subTrancode105) {
        this.subTrancode105 = subTrancode105;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InputTransferMain)) return false;
        InputTransferMain other = (InputTransferMain) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MTID1==null && other.getMTID1()==null) || 
             (this.MTID1!=null &&
              this.MTID1.equals(other.getMTID1()))) &&
            ((this.PAN2==null && other.getPAN2()==null) || 
             (this.PAN2!=null &&
              this.PAN2.equals(other.getPAN2()))) &&
            ((this.trCd3==null && other.getTrCd3()==null) || 
             (this.trCd3!=null &&
              this.trCd3.equals(other.getTrCd3()))) &&
            ((this.amount4==null && other.getAmount4()==null) || 
             (this.amount4!=null &&
              this.amount4.equals(other.getAmount4()))) &&
            ((this.STAN11==null && other.getSTAN11()==null) || 
             (this.STAN11!=null &&
              this.STAN11.equals(other.getSTAN11()))) &&
            ((this.merType18==null && other.getMerType18()==null) || 
             (this.merType18!=null &&
              this.merType18.equals(other.getMerType18()))) &&
            ((this.fee28==null && other.getFee28()==null) || 
             (this.fee28!=null &&
              this.fee28.equals(other.getFee28()))) &&
            ((this.acqInsId32==null && other.getAcqInsId32()==null) || 
             (this.acqInsId32!=null &&
              this.acqInsId32.equals(other.getAcqInsId32()))) &&
            ((this.chCd33==null && other.getChCd33()==null) || 
             (this.chCd33!=null &&
              this.chCd33.equals(other.getChCd33()))) &&
            ((this.RRN37==null && other.getRRN37()==null) || 
             (this.RRN37!=null &&
              this.RRN37.equals(other.getRRN37()))) &&
            ((this.termId41==null && other.getTermId41()==null) || 
             (this.termId41!=null &&
              this.termId41.equals(other.getTermId41()))) &&
            ((this.accIdCd42==null && other.getAccIdCd42()==null) || 
             (this.accIdCd42!=null &&
              this.accIdCd42.equals(other.getAccIdCd42()))) &&
            ((this.acceptAddr43==null && other.getAcceptAddr43()==null) || 
             (this.acceptAddr43!=null &&
              this.acceptAddr43.equals(other.getAcceptAddr43()))) &&
            ((this.destBankCd47==null && other.getDestBankCd47()==null) || 
             (this.destBankCd47!=null &&
              this.destBankCd47.equals(other.getDestBankCd47()))) &&
            ((this.desc48==null && other.getDesc48()==null) || 
             (this.desc48!=null &&
              this.desc48.equals(other.getDesc48()))) &&
            ((this.curr49==null && other.getCurr49()==null) || 
             (this.curr49!=null &&
              this.curr49.equals(other.getCurr49()))) &&
            ((this.phnNmbr62==null && other.getPhnNmbr62()==null) || 
             (this.phnNmbr62!=null &&
              this.phnNmbr62.equals(other.getPhnNmbr62()))) &&
            ((this.originalDataElement90==null && other.getOriginalDataElement90()==null) || 
             (this.originalDataElement90!=null &&
              this.originalDataElement90.equals(other.getOriginalDataElement90()))) &&
            ((this.repAmount95==null && other.getRepAmount95()==null) || 
             (this.repAmount95!=null &&
              this.repAmount95.equals(other.getRepAmount95()))) &&
            ((this.FA102==null && other.getFA102()==null) || 
             (this.FA102!=null &&
              this.FA102.equals(other.getFA102()))) &&
            ((this.TA103==null && other.getTA103()==null) || 
             (this.TA103!=null &&
              this.TA103.equals(other.getTA103()))) &&
            ((this.subTrancode105==null && other.getSubTrancode105()==null) || 
             (this.subTrancode105!=null &&
              this.subTrancode105.equals(other.getSubTrancode105())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMTID1() != null) {
            _hashCode += getMTID1().hashCode();
        }
        if (getPAN2() != null) {
            _hashCode += getPAN2().hashCode();
        }
        if (getTrCd3() != null) {
            _hashCode += getTrCd3().hashCode();
        }
        if (getAmount4() != null) {
            _hashCode += getAmount4().hashCode();
        }
        if (getSTAN11() != null) {
            _hashCode += getSTAN11().hashCode();
        }
        if (getMerType18() != null) {
            _hashCode += getMerType18().hashCode();
        }
        if (getFee28() != null) {
            _hashCode += getFee28().hashCode();
        }
        if (getAcqInsId32() != null) {
            _hashCode += getAcqInsId32().hashCode();
        }
        if (getChCd33() != null) {
            _hashCode += getChCd33().hashCode();
        }
        if (getRRN37() != null) {
            _hashCode += getRRN37().hashCode();
        }
        if (getTermId41() != null) {
            _hashCode += getTermId41().hashCode();
        }
        if (getAccIdCd42() != null) {
            _hashCode += getAccIdCd42().hashCode();
        }
        if (getAcceptAddr43() != null) {
            _hashCode += getAcceptAddr43().hashCode();
        }
        if (getDestBankCd47() != null) {
            _hashCode += getDestBankCd47().hashCode();
        }
        if (getDesc48() != null) {
            _hashCode += getDesc48().hashCode();
        }
        if (getCurr49() != null) {
            _hashCode += getCurr49().hashCode();
        }
        if (getPhnNmbr62() != null) {
            _hashCode += getPhnNmbr62().hashCode();
        }
        if (getOriginalDataElement90() != null) {
            _hashCode += getOriginalDataElement90().hashCode();
        }
        if (getRepAmount95() != null) {
            _hashCode += getRepAmount95().hashCode();
        }
        if (getFA102() != null) {
            _hashCode += getFA102().hashCode();
        }
        if (getTA103() != null) {
            _hashCode += getTA103().hashCode();
        }
        if (getSubTrancode105() != null) {
            _hashCode += getSubTrancode105().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InputTransferMain.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.bmi.com", ">InputTransferMain"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MTID1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "MTID1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAN2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "PAN2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trCd3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "TrCd3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "Amount4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STAN11");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "STAN11"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merType18");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "MerType18"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fee28");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "Fee28"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acqInsId32");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "AcqInsId32"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chCd33");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "ChCd33"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RRN37");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "RRN37"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("termId41");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "TermId41"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accIdCd42");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "AccIdCd42"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptAddr43");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "AcceptAddr43"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destBankCd47");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "DestBankCd47"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc48");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "Desc48"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("curr49");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "Curr49"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phnNmbr62");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "PhnNmbr62"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalDataElement90");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "OriginalDataElement90"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("repAmount95");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "RepAmount95"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FA102");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "FA102"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TA103");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "TA103"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subTrancode105");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "SubTrancode105"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
