/**
 * Execute_ptt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bmi.soa.MainTransaction.mainprocess;

public interface Execute_ptt extends java.rmi.Remote {
    public com.bmi.soa.OutputTransferMain execute(com.bmi.soa.InputTransferMain mcbfs) throws java.rmi.RemoteException;
}
