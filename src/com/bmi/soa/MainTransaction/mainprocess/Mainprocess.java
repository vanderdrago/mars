/**
 * Mainprocess.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bmi.soa.MainTransaction.mainprocess;

public interface Mainprocess extends javax.xml.rpc.Service {
    public java.lang.String getexecute_ptAddress();

    public com.bmi.soa.MainTransaction.mainprocess.Execute_ptt getexecute_pt() throws javax.xml.rpc.ServiceException;

    public com.bmi.soa.MainTransaction.mainprocess.Execute_ptt getexecute_pt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
