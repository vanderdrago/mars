package com.bmi.soa.MainTransaction.mainprocess;

public class Execute_pttProxy implements com.bmi.soa.MainTransaction.mainprocess.Execute_ptt {
  private String _endpoint = null;
  private com.bmi.soa.MainTransaction.mainprocess.Execute_ptt execute_ptt = null;
  
  public Execute_pttProxy() {
    _initExecute_pttProxy();
  }
  
  public Execute_pttProxy(String endpoint) {
    _endpoint = endpoint;
    _initExecute_pttProxy();
  }
  
  private void _initExecute_pttProxy() {
    try {
      execute_ptt = (new com.bmi.soa.MainTransaction.mainprocess.MainprocessLocator()).getexecute_pt();
      if (execute_ptt != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)execute_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)execute_ptt)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (execute_ptt != null)
      ((javax.xml.rpc.Stub)execute_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bmi.soa.MainTransaction.mainprocess.Execute_ptt getExecute_ptt() {
    if (execute_ptt == null)
      _initExecute_pttProxy();
    return execute_ptt;
  }
  
  public com.bmi.soa.OutputTransferMain execute(com.bmi.soa.InputTransferMain mcbfs) throws java.rmi.RemoteException{
    if (execute_ptt == null)
      _initExecute_pttProxy();
    return execute_ptt.execute(mcbfs);
  }
  
  
}