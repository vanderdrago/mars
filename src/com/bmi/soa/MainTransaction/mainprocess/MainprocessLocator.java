/**
 * MainprocessLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bmi.soa.MainTransaction.mainprocess;

public class MainprocessLocator extends org.apache.axis.client.Service implements com.bmi.soa.MainTransaction.mainprocess.Mainprocess {

    public MainprocessLocator() {
    }


    public MainprocessLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MainprocessLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for execute_pt
    private java.lang.String execute_pt_address = "http://10.81.18.13:8051/soa-infra/services/fsmdw/MainTransaction/mainprocess";

    public java.lang.String getexecute_ptAddress() {
        return execute_pt_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String execute_ptWSDDServiceName = "execute_pt";

    public java.lang.String getexecute_ptWSDDServiceName() {
        return execute_ptWSDDServiceName;
    }

    public void setexecute_ptWSDDServiceName(java.lang.String name) {
        execute_ptWSDDServiceName = name;
    }

    public com.bmi.soa.MainTransaction.mainprocess.Execute_ptt getexecute_pt() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(execute_pt_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getexecute_pt(endpoint);
    }

    public com.bmi.soa.MainTransaction.mainprocess.Execute_ptt getexecute_pt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.bmi.soa.MainTransaction.mainprocess.Execute_pttBindingStub _stub = new com.bmi.soa.MainTransaction.mainprocess.Execute_pttBindingStub(portAddress, this);
            _stub.setPortName(getexecute_ptWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setexecute_ptEndpointAddress(java.lang.String address) {
        execute_pt_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.bmi.soa.MainTransaction.mainprocess.Execute_ptt.class.isAssignableFrom(serviceEndpointInterface)) {
                com.bmi.soa.MainTransaction.mainprocess.Execute_pttBindingStub _stub = new com.bmi.soa.MainTransaction.mainprocess.Execute_pttBindingStub(new java.net.URL(execute_pt_address), this);
                _stub.setPortName(getexecute_ptWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("execute_pt".equals(inputPortName)) {
            return getexecute_pt();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soa.bmi.com/MainTransaction/mainprocess", "mainprocess");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soa.bmi.com/MainTransaction/mainprocess", "execute_pt"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("execute_pt".equals(portName)) {
            setexecute_ptEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
