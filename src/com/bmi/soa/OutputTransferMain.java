/**
 * OutputTransferMain.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bmi.soa;

public class OutputTransferMain  implements java.io.Serializable {
    private java.lang.String MTID1;
    private java.lang.String transDate7;
    private java.lang.String systemTraceAuditNum11;
    private java.lang.String RRN37;
    private java.lang.String notifStatus39;
    private java.lang.String names48;
    private java.lang.String saldo54;
    public OutputTransferMain() {
    }

    public OutputTransferMain(
           java.lang.String MTID1,
           java.lang.String transDate7,
           java.lang.String systemTraceAuditNum11,
           java.lang.String RRN37,
           java.lang.String notifStatus39,
           java.lang.String names48,
           java.lang.String saldo54) {
           this.MTID1 = MTID1;
           this.transDate7 = transDate7;
           this.systemTraceAuditNum11 = systemTraceAuditNum11;
           this.RRN37 = RRN37;
           this.notifStatus39 = notifStatus39;
           this.names48 = names48;
           this.saldo54 = saldo54;
    }


    /**
     * Gets the MTID1 value for this OutputTransferMain.
     * 
     * @return MTID1
     */
    public java.lang.String getMTID1() {
        return MTID1;
    }


    /**
     * Sets the MTID1 value for this OutputTransferMain.
     * 
     * @param MTID1
     */
    public void setMTID1(java.lang.String MTID1) {
        this.MTID1 = MTID1;
    }


    /**
     * Gets the transDate7 value for this OutputTransferMain.
     * 
     * @return transDate7
     */
    public java.lang.String getTransDate7() {
        return transDate7;
    }


    /**
     * Sets the transDate7 value for this OutputTransferMain.
     * 
     * @param transDate7
     */
    public void setTransDate7(java.lang.String transDate7) {
        this.transDate7 = transDate7;
    }


    /**
     * Gets the systemTraceAuditNum11 value for this OutputTransferMain.
     * 
     * @return systemTraceAuditNum11
     */
    public java.lang.String getSystemTraceAuditNum11() {
        return systemTraceAuditNum11;
    }


    /**
     * Sets the systemTraceAuditNum11 value for this OutputTransferMain.
     * 
     * @param systemTraceAuditNum11
     */
    public void setSystemTraceAuditNum11(java.lang.String systemTraceAuditNum11) {
        this.systemTraceAuditNum11 = systemTraceAuditNum11;
    }


    /**
     * Gets the RRN37 value for this OutputTransferMain.
     * 
     * @return RRN37
     */
    public java.lang.String getRRN37() {
        return RRN37;
    }


    /**
     * Sets the RRN37 value for this OutputTransferMain.
     * 
     * @param RRN37
     */
    public void setRRN37(java.lang.String RRN37) {
        this.RRN37 = RRN37;
    }


    /**
     * Gets the notifStatus39 value for this OutputTransferMain.
     * 
     * @return notifStatus39
     */
    public java.lang.String getNotifStatus39() {
        return notifStatus39;
    }


    /**
     * Sets the notifStatus39 value for this OutputTransferMain.
     * 
     * @param notifStatus39
     */
    public void setNotifStatus39(java.lang.String notifStatus39) {
        this.notifStatus39 = notifStatus39;
    }


    /**
     * Gets the names48 value for this OutputTransferMain.
     * 
     * @return names48
     */
    public java.lang.String getNames48() {
        return names48;
    }


    /**
     * Sets the names48 value for this OutputTransferMain.
     * 
     * @param names48
     */
    public void setNames48(java.lang.String names48) {
        this.names48 = names48;
    }


    /**
     * Gets the saldo54 value for this OutputTransferMain.
     * 
     * @return saldo54
     */
    public java.lang.String getSaldo54() {
        return saldo54;
    }


    /**
     * Sets the saldo54 value for this OutputTransferMain.
     * 
     * @param saldo54
     */
    public void setSaldo54(java.lang.String saldo54) {
        this.saldo54 = saldo54;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutputTransferMain)) return false;
        OutputTransferMain other = (OutputTransferMain) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MTID1==null && other.getMTID1()==null) || 
             (this.MTID1!=null &&
              this.MTID1.equals(other.getMTID1()))) &&
            ((this.transDate7==null && other.getTransDate7()==null) || 
             (this.transDate7!=null &&
              this.transDate7.equals(other.getTransDate7()))) &&
            ((this.systemTraceAuditNum11==null && other.getSystemTraceAuditNum11()==null) || 
             (this.systemTraceAuditNum11!=null &&
              this.systemTraceAuditNum11.equals(other.getSystemTraceAuditNum11()))) &&
            ((this.RRN37==null && other.getRRN37()==null) || 
             (this.RRN37!=null &&
              this.RRN37.equals(other.getRRN37()))) &&
            ((this.notifStatus39==null && other.getNotifStatus39()==null) || 
             (this.notifStatus39!=null &&
              this.notifStatus39.equals(other.getNotifStatus39()))) &&
            ((this.names48==null && other.getNames48()==null) || 
             (this.names48!=null &&
              this.names48.equals(other.getNames48()))) &&
            ((this.saldo54==null && other.getSaldo54()==null) || 
             (this.saldo54!=null &&
              this.saldo54.equals(other.getSaldo54())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMTID1() != null) {
            _hashCode += getMTID1().hashCode();
        }
        if (getTransDate7() != null) {
            _hashCode += getTransDate7().hashCode();
        }
        if (getSystemTraceAuditNum11() != null) {
            _hashCode += getSystemTraceAuditNum11().hashCode();
        }
        if (getRRN37() != null) {
            _hashCode += getRRN37().hashCode();
        }
        if (getNotifStatus39() != null) {
            _hashCode += getNotifStatus39().hashCode();
        }
        if (getNames48() != null) {
            _hashCode += getNames48().hashCode();
        }
        if (getSaldo54() != null) {
            _hashCode += getSaldo54().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutputTransferMain.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soa.bmi.com", ">outputTransferMain"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MTID1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "MTID1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transDate7");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "TransDate7"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceAuditNum11");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "SystemTraceAuditNum11"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RRN37");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "RRN37"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notifStatus39");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "NotifStatus39"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("names48");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "Names48"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saldo54");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soa.bmi.com", "Saldo54"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
