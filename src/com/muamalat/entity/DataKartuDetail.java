/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.entity;

/**
 *
 * @author herrysuganda
 */
public class DataKartuDetail {
    private String nomorkartu;
    private String rekeningMCB;
    private String rekeningAlt;
    private String nama;
    private String noDebet;
    private String noCredit;
    private String frozen;
    private String dormant;
    private String saldo;

    /**
     * @return the nomorkartu
     */
    public String getNomorkartu() {
        return nomorkartu;
    }

    /**
     * @param nomorkartu the nomorkartu to set
     */
    public void setNomorkartu(String nomorkartu) {
        this.nomorkartu = nomorkartu;
    }

    /**
     * @return the rekeningMCB
     */
    public String getRekeningMCB() {
        return rekeningMCB;
    }

    /**
     * @param rekeningMCB the rekeningMCB to set
     */
    public void setRekeningMCB(String rekeningMCB) {
        this.rekeningMCB = rekeningMCB;
    }

    /**
     * @return the rekeningAlt
     */
    public String getRekeningAlt() {
        return rekeningAlt;
    }

    /**
     * @param rekeningAlt the rekeningAlt to set
     */
    public void setRekeningAlt(String rekeningAlt) {
        this.rekeningAlt = rekeningAlt;
    }

    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the noDebet
     */
    public String getNoDebet() {
        return noDebet;
    }

    /**
     * @param noDebet the noDebet to set
     */
    public void setNoDebet(String noDebet) {
        this.noDebet = noDebet;
    }

    /**
     * @return the noCredit
     */
    public String getNoCredit() {
        return noCredit;
    }

    /**
     * @param noCredit the noCredit to set
     */
    public void setNoCredit(String noCredit) {
        this.noCredit = noCredit;
    }

    /**
     * @return the saldo
     */
    public String getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    /**
     * @return the frozen
     */
    public String getFrozen() {
        return frozen;
    }

    /**
     * @param frozen the frozen to set
     */
    public void setFrozen(String frozen) {
        this.frozen = frozen;
    }

    /**
     * @return the dormant
     */
    public String getDormant() {
        return dormant;
    }

    /**
     * @param dormant the dormant to set
     */
    public void setDormant(String dormant) {
        this.dormant = dormant;
    }

}
