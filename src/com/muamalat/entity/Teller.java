package com.muamalat.entity;

public class Teller {
	private String username;
	private String password;
	private int level;
	private String kodecabang;

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the kodecabang
	 */
	public String getKodecabang() {
		return kodecabang;
	}

	/**
	 * @param kodecabang
	 *            the kodecabang to set
	 */
	public void setKodecabang(String kodecabang) {
		this.kodecabang = kodecabang;
	}
}
