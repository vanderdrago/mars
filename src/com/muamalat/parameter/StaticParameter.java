/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.parameter;

/**
 *
 * @author herry.suganda
 */
public class StaticParameter {

//    public static String MFEP_CONFIG_FILENAME = "C:\\propertiesfile\\mfep\\mfepconfig.properties";
//    public static String PACKAGE_ISO_FILENAME = "C:\\propertiesfile\\mfep\\iso87ascii.xml";
//    public static String MITRA_CONFIG_FILENAME = "C:\\propertiesfile\\mfep\\mitra.properties";
//    public static String MFEP_CONFIG_FILENAME = "/opt/propertiesfile/mfep/mfepconfig.properties";
//    public static String PACKAGE_ISO_FILENAME = "/opt/propertiesfile/mfep/iso87ascii.xml";
//    public static String MITRA_CONFIG_FILENAME = "/opt/propertiesfile/mfep/mitra.properties";
    public static int TIMEOUT = 10;
    public static String SESSION_LOGIN_VAL = "loginval";
    public static String MENU_STATUS = "menu";
    public static String SESSION_USER = "userlogin";
    public static String KODE_CABANG = "kodecabang";
    public static String NAMA_CABANG = "namacabang";
    public static String SESSION_PIL_CAB = "selCabList";
    public static String USER_LEVEL = "userlevel";
    public static String USER_ROLE = "role";
    public static String USER_ROLE_STRING = "role_string";
    public static String USER_PASSWORD = "userpassword";
    public static String USER_ENTITY = "userentity";
    public static String ACTIVITY_AUDIT = "Activity";
    public static String SESSION_PAYMENT = "payment";
    public static String CETAK_SLIP_TRX = "cetakSlipTransaksi";
    public static String ID_TRX = "ID";
    public static String ERROR = "error";
    public static String Method_Payment_Cash = "Cash";
    public static String Method_Payment_Debit = "Debit";
    public static String REPORT_NAME = "com/muamalat/report/MFEP_slip2.jasper";
    public static int Max_CHAR_VA = 16;
    public static int Max_CHAR_CUST_NAME = 30;
    public static int Max_CHAR_AMOUNT = 15;
    public static int Max_CHAR_DESC = 40;
    public static String PENDING_REVERSAL = "PR";
    public static String FILE_NAME = "File name";
    public static int Max_CHAR_DESC2 = 40;
    public static String UPLOAD_FOLDER = "d:\\data\\MFEP\\upload";
    public static String DOWNLOAD_FOLDER = "d:\\data\\MFEP\\download";
//    public static String UPLOAD_FOLDER = "/data/app_data/mfep/upload";
//    public static String DOWNLOAD_FOLDER = "/data/app_data/mfep/download";    

    public static String SETOR_TUNAI_TRX = "setortunai";
    public static String PAYMENT_VA_TRX = "paymentva";
    
}
