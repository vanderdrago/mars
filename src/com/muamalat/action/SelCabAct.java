/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.action;

import WSInvoker.Utils.RetVal.LoginRetVal;
import com.muamalat.entity.Teller;
import com.muamalat.parameter.StaticParameter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Windows7
 */
public class SelCabAct extends org.apache.struts.action.Action {

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(SelCabAct.class);

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = "success";

        if (request.getSession(true).getAttribute(StaticParameter.SESSION_LOGIN_VAL) == null) {
//            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            SUCCESS = "loginpage";
            return mapping.findForward(SUCCESS);
        }

        String kdCab = (String) request.getParameter("cabang");

        if (kdCab != null) {
            LoginRetVal retVal = (LoginRetVal) request.getSession(true).getAttribute(StaticParameter.SESSION_LOGIN_VAL);
            int level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
            Teller user = new Teller();
            user.setKodecabang(retVal.getHomeBranch());
            user.setKodecabang(kdCab);
            user.setLevel(level);
            user.setUsername(retVal.getUserName().toUpperCase());
            user.setPassword("");

            request.getSession(true).setAttribute(StaticParameter.SESSION_USER, retVal.getUserName().toUpperCase());
            request.getSession(true).setAttribute(StaticParameter.USER_ENTITY, user);
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, level);
            request.getSession(true).setAttribute(StaticParameter.KODE_CABANG, user.getKodecabang());
            SUCCESS = "success";
        } else {
            SUCCESS = "loginpage";
        }

        return mapping.findForward(SUCCESS);
    }
}
