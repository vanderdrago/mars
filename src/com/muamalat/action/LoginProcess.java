/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.action;

import java.util.Iterator;
import java.util.List;

import WSInvoker.Utils.RetVal.LoginRetVal;

import com.muamalat.database.process.ProcessOnDatabase;
import com.muamalat.database.process.ProcessOnDatabaseBean;
import com.muamalat.function.AuditFuntion;
import com.muamalat.parameter.StaticParameter;
import com.muamalat.parameter.UserLevelName;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.RoleMenu;
import com.muamalat.reportmcb.function.CountLoginSqlFunction;
import com.muamalat.reportmcb.function.SqlFunction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author User
 */
public class LoginProcess extends Action {

    private static Logger log = Logger.getLogger(LoginProcess.class);
    /* forward name="success" path="" */
    private static String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        int level = 5;
   
        ProcessOnDatabase login = new ProcessOnDatabaseBean();
        System.out.println("masuk proses login");
        LoginRetVal retVal = login.login2Mcb(username, password, level);
        System.out.println("retval kode cabang" + retVal.getKdCabang());
        System.out.println("retval username" + retVal.getUserName());
        System.out.println("retval home branch" + retVal.getHomeBranch());
        System.out.println("retval role" + retVal.getRoles());
        if (retVal != null) {
            if (retVal.isRespOk()) {
                if (retVal.getKdCabang().size() > 1) {
                	  Admin user = new Admin();
                      user.setKodecabang(retVal.getHomeBranch());
                      user.setLevel(level);
                      user.setUsername(username.toUpperCase());
                      user.setPassword("");
                      
                      List role = retVal.getRoles();
                      SqlFunction sql = new SqlFunction();
                      List<RoleMenu> lrolesHeader = sql.getRole2(role);
                      request.getSession(true).setAttribute("lrolesHeader", lrolesHeader);
                      
                      RoleMenu header = new RoleMenu();
              		  for (int i = 0; i < lrolesHeader.size(); i++){
              			header = (RoleMenu)lrolesHeader.get(i);
              			 System.out.println("header:" + header.getHeaderName());
              		  }
              		 
              		  String subRole = "(";
                   
            		Iterator it = role.iterator();
                	while (it.hasNext()) {
                		subRole = subRole +"'"+it.next()+"'"+",";
                	}
                	subRole = subRole.substring(0,subRole.length()-1)  + ")";	
            	    	System.out.println("subRole=" + subRole);
            	    	
            	    	
              		  request.getSession(true).setAttribute(StaticParameter.MENU_STATUS, role);
                      request.getSession(true).setAttribute(StaticParameter.SESSION_LOGIN_VAL, retVal);
                      request.getSession(true).setAttribute(StaticParameter.SESSION_USER, username.toUpperCase());
                      request.getSession(true).setAttribute(StaticParameter.USER_ENTITY, user);
                      request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, level);
                      request.getSession(true).setAttribute(StaticParameter.KODE_CABANG, user.getKodecabang());
                      request.getSession(true).setAttribute(StaticParameter.USER_ROLE_STRING, subRole );
                      
//                      request.getSession(true).setAttribute(StaticParameter.USER_ROLE, role);
                      new AuditFuntion().login(username);
                      SUCCESS = "success";
                } else if (retVal.getKdCabang().size() == 1) {
                    Admin user = new Admin();
                    user.setKodecabang(retVal.getHomeBranch());
                    user.setLevel(level);
                    user.setUsername(username.toUpperCase());
                    user.setPassword("");
                    
                    List role = retVal.getRoles();
                    SqlFunction sql = new SqlFunction();
                    List<RoleMenu> lrolesHeader = sql.getRole2(role);
                    int menu_status = lrolesHeader.size();
                    System.out.println("tes2 action:" +lrolesHeader.size());
                    request.getSession(true).setAttribute("lrolesHeader", lrolesHeader);
                    RoleMenu header = new RoleMenu();
            		  for (int i = 0; i < lrolesHeader.size(); i++){
            			header = (RoleMenu)lrolesHeader.get(i);
            			 System.out.println("header:" + header.getHeaderName());
            		  }
            		  
            		  String subRole = "(";
                      
              		Iterator it = role.iterator();
                  	while (it.hasNext()) {
                  		subRole = subRole +"'"+it.next()+"'"+",";
                  	}
                  	subRole = subRole.substring(0,subRole.length()-1)  + ")";	
              	    	System.out.println("subRole=" + subRole);
              	    	
              	    	
            		request.getSession(true).setAttribute(StaticParameter.MENU_STATUS, role);
                	request.getSession(true).setAttribute(StaticParameter.SESSION_LOGIN_VAL, retVal);
                    request.getSession(true).setAttribute(StaticParameter.SESSION_USER, username.toUpperCase());
                    request.getSession(true).setAttribute(StaticParameter.USER_ENTITY, user);
                    request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, level);
                    request.getSession(true).setAttribute(StaticParameter.KODE_CABANG, user.getKodecabang());
                    request.getSession(true).setAttribute(StaticParameter.USER_ROLE_STRING, subRole );
                    new AuditFuntion().login(username);
                    SUCCESS = "success";
//                    	}else {
//                    		request.setAttribute("dataresponse", "Tidak ditemukan transaksi hari ini");
//        	                request.setAttribute("konfirmasi", "err");
//                    	}
//                    	return mapping.findForward(SUCCESS);
                    } else {
                    request.setAttribute("message", "Login gagal tidak ada hak akses cabang!");
                    request.setAttribute("level", level);
                    request.setAttribute("levelname", UserLevelName.getUserNameLevel(level));
                    SUCCESS = "failed";
                } 	 
                CountLoginSqlFunction countloginsql = new CountLoginSqlFunction();
           	 countloginsql.getDeleteCount(username);
            } else {
                request.setAttribute("message", "Login gagal !");
                request.setAttribute("level", level);
                request.setAttribute("levelname", UserLevelName.getUserNameLevel(5));
                System.out.println(retVal.getErrCode());
                if(!retVal.getErrCode().equals("login :5: SM-00006 ") && !retVal.getErrCode().equals("login :2: SM-11140 ") ){
                	 CountLoginSqlFunction countloginsql = new CountLoginSqlFunction();
                	 countloginsql.getCountLogin(username);
                	
                	
                }
               
                System.out.println(retVal.getErrCode());
                System.out.println(retVal.getErrMessage());
                SUCCESS = "failed";
            }
        } else {
        	System.out.println("masuk else null");
            request.setAttribute("message", "Login gagal !");
            request.setAttribute("level", level);
            request.setAttribute("levelname", UserLevelName.getUserNameLevel(level));
            SUCCESS = "failed";
        }

        return mapping.findForward(SUCCESS);
    }
}
