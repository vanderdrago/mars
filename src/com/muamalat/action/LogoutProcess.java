/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.action;

import com.muamalat.function.AuditFuntion;
import com.muamalat.function.CookieUtil;
import com.muamalat.parameter.StaticParameter;
import com.muamalat.parameter.UserLevelName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author User
 */
public class LogoutProcess extends org.apache.struts.action.Action {
    private static Logger log = Logger.getLogger(LogoutProcess.class);
    
    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
//        new AuditFuntion().logout((String) session.getAttribute(StaticParameter.SESSION_USER));
        //remove all session
        session.invalidate();
        CookieUtil.setNull2Cookies(request, response);
            request.setAttribute("level", 2);
            request.setAttribute("levelname", UserLevelName.getUserNameLevel(2));
        return mapping.findForward(SUCCESS);
    }
}
