/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.database.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;

import WSInvoker.Util.DB.FCLogin;
import WSInvoker.Utils.RetVal.LoginRetVal;
import com.muamalat.entity.DataKartuDetail;
import com.muamalat.singleton.DatasourceEntry;
import java.util.ArrayList;

/**
 * 
 * @author User
 */
public class ProcessOnDatabaseBean implements ProcessOnDatabase {

	private String kode_cabang = null;
	private String totalPembayaran = null;
	private static Logger log = Logger.getLogger(ProcessOnDatabaseBean.class);
	private List listCabang = null;

	public List getListCabang() {
		return listCabang;
	}

	public void setListCabang(List listCabang) {
		this.listCabang = listCabang;
	}

	public LoginRetVal login2Mcb(String username, String password, int level) {
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String loginFilter = null;
		FCLogin fCLogin;
		LoginRetVal loginRetVal = null;
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();

			fCLogin = new FCLogin();
			loginRetVal = fCLogin.login(conn, username, password, "");

			if (loginRetVal != null) {
				if (loginRetVal.isRespOk()) {
					if (loginRetVal.getKdCabang() != null) {
						if (loginRetVal.getKdCabang().size() == 1) {
							kode_cabang = loginRetVal.getKdCabang().get(0).toString();
						} else {
							kode_cabang = "";
							listCabang = loginRetVal.getKdCabang();
						}
					}
					return loginRetVal;
				} else {
				}
			} else {
			}
		} catch (SQLException ex) {
			log.error(" login2Mcb :1: " + ex.getMessage());
		} catch (Exception ex) {
			log.error(" login2Mcb :5: " + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (SQLException ex) {
					log.error(" login2Mcb :2: " + ex.getMessage());
				}
			}
			if (stat != null) {
				try {
					stat.clearBatch();
					stat.clearParameters();
					stat.close();
					stat = null;
				} catch (SQLException ex) {
					log.error(" login2Mcb :3: " + ex.getMessage());
				}
			}
			if (conn != null) {
				try {
					conn.close();
					conn = null;
				} catch (SQLException ex) {
					log.error(" login2Mcb :4: " + ex.getMessage());
				}
			}
		}
		return loginRetVal;
	}

	public String getAllowedRoles(int level) {
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String strQuery = "";
		String roles = null;
		try {
			conn = DatasourceEntry.getInstance().getPostgreDS().getConnection();
			strQuery = "Select roles from dat_level_roles where level = ?";
			stat = conn.prepareStatement(strQuery);
			stat.setInt(1, level);

			rs = stat.executeQuery();
			while (rs.next()) {
				roles = rs.getString("roles");
			}
		} catch (SQLException ex) {
			log.error(" getAllowedRoles :1: " + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (SQLException ex) {
					log.error(" getAllowedRoles :2: " + ex.getMessage());
				}
			}
			if (stat != null) {
				try {
					stat.clearBatch();
					stat.clearParameters();
					stat.close();
					stat = null;
				} catch (SQLException ex) {
					log.error(" getAllowedRoles :3: " + ex.getMessage());
				}
			}
			if (conn != null) {
				try {
					conn.close();
					conn = null;
				} catch (SQLException ex) {
					log.error(" getAllowedRoles :4: " + ex.getMessage());
				}
			}
		}
		return roles;
	}
	
	public boolean insertAudit(HashMap hm, String user) {
		Connection conn = null;
		PreparedStatement stat = null;
		String key = "", value = "";
		for (Iterator iter = hm.entrySet().iterator(); iter.hasNext();) {
			Map.Entry entry = (Map.Entry) iter.next();
			key += (String) entry.getKey() + ";";
			value += (String) entry.getValue() + ";";

		}
		try {
			conn = DatasourceEntry.getInstance().getPostgreDS().getConnection();
            stat = conn.prepareStatement("INSERT INTO \"Audittrail\"(\"key\", \"value\", \"user\") VALUES (?, ?, ?)");
			stat.setString(1, key);
			stat.setString(2, value);
			stat.setString(3, user);
			stat.executeUpdate();
		} catch (SQLException ex) {
			log.error(" insertAudit :1: " + ex.getMessage());
		} finally {
			if (stat != null) {
				try {
					stat.clearBatch();
					stat.clearParameters();
					stat.close();
					stat = null;
				} catch (SQLException ex) {
					log.error(" insertAudit :2: " + ex.getMessage());
				}
			}
			if (conn != null) {
				try {
					conn.close();
					conn = null;
				} catch (SQLException ex) {
					log.error(" insertAudit :3: " + ex.getMessage());
				}
			}
		}
		return true;
	}

    public HashMap getDataKartuDetail(String nomorKartuArray, HashMap dataKartu) {
        System.out.println("masuk get data kartu detail");
//        List l = new ArrayList();
//        l.add("nomor kartu,rekening mcb,rekening alt,nama,saldo,no debet,no kredit,frozen,dormant");
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        DataKartuDetail dkd = null;
        System.out.println("conn luar: " + conn);
        System.out.println("stat luar: " + stat);
        try {
        	System.out.println("conn dalam1: " + conn);
            //conn = DatasourceEntry.getInstance().getDb2BmiprodDS().getConnection();
        	conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
            System.out.println("conn dalam12: " + conn);
//            String query = "select  a.contract_number, a.RBS_NUMBER, nvl(b.cust_ac_no, c.cust_ac_no) cust_ac_no, " +
//                    "nvl(b.alt_ac_no, c.alt_ac_no) alt_ac_no, nvl(b.ACY_AVL_BAL, c.ACY_AVL_BAL) ACY_AVL_BAL, " +
//                    "nvl(b.AC_STAT_NO_DR, c.AC_STAT_NO_DR) AC_STAT_NO_DR, nvl(b.AC_STAT_NO_CR, c.AC_STAT_NO_CR) AC_STAT_NO_CR, " +
//                    "nvl(b.AC_STAT_DORMANT, c.AC_STAT_DORMANT) AC_STAT_DORMANT, nvl(b.AC_STAT_FROZEN, c.AC_STAT_FROZEN) AC_STAT_FROZEN, " +
//                    "nvl(b.AC_DESC, c.AC_DESC) AC_DESC from  BMISTG.STG_DOD_W4 a " +
//                    "LEFT JOIN  BMISTG.STG_DOD_BMIREP b On b.CUST_AC_NO=a.RBS_NUMBER " +
//                    "LEFT JOIN  BMISTG.STG_DOD_BMIREP c On c.ALT_AC_NO=a.RBS_NUMBER " +
//                    "WHERE a.contract_number in ("+nomorKartuArray+") and a.con_cat = 'C' order by a.contract_number";
            String query = "select * from stg_dod_repw4 " +
            "WHERE contract_number in ("+nomorKartuArray+") order by contract_number";
            System.out.println(query);
            stat = conn.prepareStatement(query);
            rs = stat.executeQuery();
            while (rs.next()) {
                System.out.println("cust_ac_no : " + rs.getString("cust_ac_no"));
                dkd = new DataKartuDetail();
                dkd.setNomorkartu(rs.getString("contract_number"));
                dkd.setRekeningMCB(rs.getString("cust_ac_no"));
                dkd.setRekeningAlt(rs.getString("alt_ac_no"));
                dkd.setSaldo(rs.getString("ACY_AVL_BAL"));
                dkd.setNoDebet(rs.getString("AC_STAT_NO_DR"));
                dkd.setNoCredit(rs.getString("AC_STAT_NO_CR"));
                dkd.setDormant(rs.getString("AC_STAT_DORMANT"));
                dkd.setFrozen(rs.getString("AC_STAT_FROZEN"));
                dkd.setNama(rs.getString("AC_DESC"));
                dataKartu.put(rs.getString("contract_number"), dkd);
//                l.add(rs.getString("contract_number")+","+rs.getString("cust_ac_no")+","+rs.getString("alt_ac_no")+","+
//                        rs.getString("CUSTOMER_NAME1")+","+rs.getString("ACY_AVL_BAL")+","+rs.getString("AC_STAT_NO_DR")+","+
//                        rs.getString("AC_STAT_NO_CR")+","+rs.getString("AC_STAT_FROZEN")+","+rs.getString("AC_STAT_DORMANT"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" getDataKartuDetail :1: " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException ex) {
                    log.error(" getDataKartuDetail :2: " + ex.getMessage());
                }
            }
            if (stat != null) {
                try {
                    stat.clearBatch();
                    stat.clearParameters();
                    stat.close();
                    stat = null;
                } catch (SQLException ex) {
                    log.error(" getDataKartuDetail :3: " + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                    conn = null;
                } catch (SQLException ex) {
                    log.error(" getDataKartuDetail :4: " + ex.getMessage());
                }
            }
        }
        return dataKartu;
    }
    
    public String getStatusServer() {
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String strQuery = "";
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			strQuery = "select status_srv from status_tbl";
			System.out.println("sql :" + strQuery);
			stat = conn.prepareStatement(strQuery);

			rs = stat.executeQuery();
			while (rs.next()) {
				return rs.getString("status_srv");
			}
		} catch (SQLException ex) {
			log.error(" getStatusServer :1: " + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (SQLException ex) {
					log.error(" getStatusServer :2: " + ex.getMessage());
				}
			}
			if (stat != null) {
				try {
					stat.clearBatch();
					stat.clearParameters();
					stat.close();
					stat = null;
				} catch (SQLException ex) {
					log.error(" getStatusServer :3: " + ex.getMessage());
				}
			}
			if (conn != null) {
				try {
					conn.close();
					conn = null;
				} catch (SQLException ex) {
					log.error(" getStatusServer :4: " + ex.getMessage());
				}
			}
		}
		return "prod";
	}
}
