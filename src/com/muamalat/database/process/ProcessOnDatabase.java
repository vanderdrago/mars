/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.database.process;

import WSInvoker.Utils.RetVal.LoginRetVal;
import java.util.HashMap;


/**
 *
 * @author User
 */
public interface ProcessOnDatabase {
    
    public LoginRetVal login2Mcb(String username, String password, int level);
    
    public String getAllowedRoles(int level) ;
    
    public boolean insertAudit(HashMap hm, String user);

    public HashMap getDataKartuDetail(String nomorKartuArray, HashMap dataKartu);
}
