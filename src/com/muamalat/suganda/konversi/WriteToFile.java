/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.suganda.konversi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class WriteToFile {
    private static Logger logger = Logger.getLogger(WriteToFile.class);

    private FileWriter fstreamTrx = null;
    private BufferedWriter outTrx = null;
    //public static String rootDownloadFolder = "/download";
    private String filename;
    public WriteToFile(String path, String filename){
        this.filename = filename;
        cekFolderAndFile(path);
        try {
            //fstreamTrx = new FileWriter(path + rootDownloadFolder + "/" + this.filename);
            fstreamTrx = new FileWriter(path + "/" + this.filename);
            outTrx = new BufferedWriter(fstreamTrx);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    public void writeRecord(String msg) {
        try {
            outTrx.write(msg);
            outTrx.newLine();
            outTrx.flush();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    public void done(){
        try {
            fstreamTrx.flush();
            fstreamTrx.close();
            outTrx.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    private void cekFolderAndFile(String path) {
//        System.out.println(System.getProperty("user.dir"));
        File folderLog = new File(path);
        if (folderLog.exists()) {
            if (!folderLog.isDirectory()) {
                folderLog.mkdir();
            }
        } else {
            folderLog.mkdir();
        }

    }

    /**
     * @return the fileNameFull
     */
//    public String getFileNameFull() {
//        return rootDownloadFolder + "/" + filename;
//    }
}
