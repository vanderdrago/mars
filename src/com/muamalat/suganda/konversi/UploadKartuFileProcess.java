/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.suganda.konversi;

import com.muamalat.database.process.ProcessOnDatabase;
import com.muamalat.database.process.ProcessOnDatabaseBean;
import com.muamalat.entity.DataKartuDetail;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class UploadKartuFileProcess {

    private static Logger log = Logger.getLogger(UploadKartuFileProcess.class);
    private InputStream in;
    private String fileName;
    private String userUpload;

    /**
     * @return the in
     */
    public InputStream getIn() {
        return in;
    }

    /**
     * @param in the in to set
     */
    public void setIn(InputStream in) {
        this.in = in;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        String DATE_FORMAT = "yyyyMMdd_hhmmss";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

        Calendar c1 = Calendar.getInstance(); // today

        this.fileName = fileName.substring(0, fileName.length() - 4) + '_' + sdf.format(c1.getTime()) + fileName.substring(fileName.length() - 4);
    }

    public List process() {
        System.out.println("masuk proses konversi kartu");
        List resultList = new ArrayList();
        DataInputStream inStream = new DataInputStream(in);
        BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
        String strLine;
        List nomorKartu = new ArrayList();
        List nomorKartu2 = new ArrayList();
        HashMap dataKartu = new HashMap();
        String nomorKartuArray = null;
        try {
            strLine = br.readLine();
            while ((strLine = br.readLine()) != null) {
                if (strLine != null) {
                    nomorKartu.add("'" + strLine.trim() + "'");
                    nomorKartu2.add(strLine.trim());
                    dataKartu.put(strLine.trim(), null);
                }
            }
            nomorKartuArray = StringUtils.join(nomorKartu, ",");
        } catch (IOException ex) {
            ex.printStackTrace();
            log.error(ex.getMessage());
        }
        System.out.println("nomor kartu array : " + nomorKartuArray);
        if (nomorKartuArray != null) {
            ProcessOnDatabase dp = new ProcessOnDatabaseBean();
            dataKartu = dp.getDataKartuDetail(nomorKartuArray, dataKartu);
            dp = null;
        }

        resultList.add("nomor kartu,rekening mcb,rekening alt,nama,saldo,no debet,no kredit,frozen,dormant");
        Iterator iter = dataKartu.keySet().iterator();
        String nomorKartuString = "";
        DataKartuDetail dkd = null;

        for (int i=0;i<nomorKartu2.size();i++){
            nomorKartuString = nomorKartu2.get(i).toString();
            if (dataKartu.get(nomorKartuString) != null) {
                dkd = (DataKartuDetail) dataKartu.get(nomorKartuString);
                resultList.add(dkd.getNomorkartu() + "," + dkd.getRekeningMCB() + "," + dkd.getRekeningAlt() + "," +
                        dkd.getNama() + "," + dkd.getSaldo() + "," + dkd.getNoDebet() + "," +
                        dkd.getNoCredit() + "," + dkd.getFrozen() + "," + dkd.getDormant());

            }else{
                resultList.add(nomorKartuString + ",,,,,,,,");
            }
        }

//        while (iter.hasNext()) {
//            nomorKartuString = iter.next().toString();
//            if (dataKartu.get(nomorKartuString) != null) {
//                dkd = (DataKartuDetail) dataKartu.get(nomorKartuString);
//                resultList.add(dkd.getNomorkartu() + "," + dkd.getRekeningMCB() + "," + dkd.getRekeningAlt() + "," +
//                        dkd.getNama() + "," + dkd.getSaldo() + "," + dkd.getNoDebet() + "," +
//                        dkd.getNoCredit() + "," + dkd.getFrozen() + "," + dkd.getDormant());
//
//            }else{
//                resultList.add(nomorKartuString + ",,,,,,,,");
//            }
//        }
        return resultList;
    }

    /**
     * @return the userUpload
     */
    public String getUserUpload() {
        return userUpload;
    }

    /**
     * @param userUpload the userUpload to set
     */
    public void setUserUpload(String userUpload) {
        this.userUpload = userUpload;
    }

    public void saveFile(String filePath, byte[] fileData) {
        try {
            /* Save file on the server */
            cekFolder(filePath);
            //System.out.println("Server path:" + filePath);

            //Create file
            File fileToCreate = new File(filePath, fileName);

            //If file does not exists create file
            if (!fileToCreate.exists()) {
                FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);
                fileOutStream.write(fileData);
                fileOutStream.flush();
                fileOutStream.close();
            }


        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    private void cekFolder(String filePath) {
//        System.out.println(System.getProperty("user.dir"));
        File folderData = new File(filePath);
        if (folderData.exists()) {
            if (!folderData.isDirectory()) {
                folderData.mkdir();
            }
        } else {
            folderData.mkdir();
        }

    }
}
