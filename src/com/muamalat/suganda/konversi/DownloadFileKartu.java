/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.suganda.konversi;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.muamalat.reportmcb.parameter.Parameter;
import com.muamalat.singleton.ParamSingleton;

/**
 *
 * @author User
 */
public class DownloadFileKartu extends org.apache.struts.action.Action {

    private static Logger log = Logger.getLogger(DownloadFileKartu.class);

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int level;
        int validLevel = 2;
//        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
//            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
//            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
//            return mapping.findForward("loginpage");
//        }

        /*level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level != validLevel){
        request.setAttribute("message", "Unauthorized Access!");
        request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
        //request.setAttribute("error_code", "Error_timeout");
        return mapping.findForward("msgpage");
        }*/

        String filename = request.getParameter("filename");
        File f = null;


        //File f = new File(WriteToFile.rootDownloadFolder + "/" + filename);
        //File f = new File(getServlet().getServletContext().getRealPath("/") + "/" + WriteToFile.rootDownloadFolder + "/" + filename);
        f = new File(Parameter.getKonversiKartuPathDownload(ParamSingleton.getInstance().getStatusServer()) + "/" + filename);

        int length = 0;
        ServletOutputStream op = response.getOutputStream();

        ServletContext context = servlet.getServletContext();
        //String mimetype = context.getMimeType(WriteToFile.rootDownloadFolder + "/" + filename);
        String mimetype = context.getMimeType(Parameter.getKonversiKartuPathDownload(ParamSingleton.getInstance().getStatusServer()) + "/" + filename);
        response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
        response.setContentLength((int) f.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        byte[] bbuf = new byte[1024];
        DataInputStream in = new DataInputStream(new FileInputStream(f));

        while ((in != null) && ((length = in.read(bbuf)) != -1)) {
            op.write(bbuf, 0, length);
        }

        in.close();
        op.flush();
        op.close();
        return mapping.findForward(SUCCESS);
    }
}
