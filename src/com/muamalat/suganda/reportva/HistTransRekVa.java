/**
 * HistTransRek.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.muamalat.suganda.reportva;


public class HistTransRekVa  implements java.io.Serializable {
    private java.lang.String account;

    private java.lang.String akumulasi;

    private java.lang.String auth_stat;

    private java.lang.String desc;

    private java.lang.String drcr;

    private java.math.BigDecimal ekivalen;

    private java.lang.String instrument_code;

    private java.math.BigDecimal nominal;

    private java.math.BigDecimal saldo;

    private java.lang.String trn_code;

    private java.lang.String trn_dt;

    private java.lang.String trn_ref_no;

    private java.lang.String val_dt;

    public HistTransRekVa() {
    }

	public java.lang.String getAccount() {
		return account;
	}

	public void setAccount(java.lang.String account) {
		this.account = account;
	}

	public java.lang.String getAkumulasi() {
		return akumulasi;
	}

	public void setAkumulasi(java.lang.String akumulasi) {
		this.akumulasi = akumulasi;
	}

	public java.lang.String getAuth_stat() {
		return auth_stat;
	}

	public void setAuth_stat(java.lang.String auth_stat) {
		this.auth_stat = auth_stat;
	}

	public java.lang.String getDesc() {
		return desc;
	}

	public void setDesc(java.lang.String desc) {
		this.desc = desc;
	}

	public java.lang.String getDrcr() {
		return drcr;
	}

	public void setDrcr(java.lang.String drcr) {
		this.drcr = drcr;
	}

	public java.math.BigDecimal getEkivalen() {
		return ekivalen;
	}

	public void setEkivalen(java.math.BigDecimal ekivalen) {
		this.ekivalen = ekivalen;
	}

	public java.lang.String getInstrument_code() {
		return instrument_code;
	}

	public void setInstrument_code(java.lang.String instrument_code) {
		this.instrument_code = instrument_code;
	}

	public java.math.BigDecimal getNominal() {
		return nominal;
	}

	public void setNominal(java.math.BigDecimal nominal) {
		this.nominal = nominal;
	}

	public java.math.BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(java.math.BigDecimal saldo) {
		this.saldo = saldo;
	}

	public java.lang.String getTrn_code() {
		return trn_code;
	}

	public void setTrn_code(java.lang.String trn_code) {
		this.trn_code = trn_code;
	}

	public java.lang.String getTrn_dt() {
		return trn_dt;
	}

	public void setTrn_dt(java.lang.String trn_dt) {
		this.trn_dt = trn_dt;
	}

	public java.lang.String getTrn_ref_no() {
		return trn_ref_no;
	}

	public void setTrn_ref_no(java.lang.String trn_ref_no) {
		this.trn_ref_no = trn_ref_no;
	}

	public java.lang.String getVal_dt() {
		return val_dt;
	}

	public void setVal_dt(java.lang.String val_dt) {
		this.val_dt = val_dt;
	}
}
