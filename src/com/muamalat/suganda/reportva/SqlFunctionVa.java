/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.suganda.reportva;

import com.muamalat.reportmcb.function.*;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;


import com.muamalat.reportmcb.entity.BatchMaster;
import com.muamalat.reportmcb.entity.Bukubesar;
import com.muamalat.reportmcb.entity.Customer;
import com.muamalat.reportmcb.entity.DetailBatch;
import com.muamalat.reportmcb.entity.Gl;
import com.muamalat.reportmcb.entity.Gltb_avgbal;
import com.muamalat.reportmcb.entity.Lsp;
import com.muamalat.reportmcb.entity.Pcode;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.singleton.DatasourceEntry;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 *
 * @author Suganda
 */
public class SqlFunctionVa {

    private static Logger log = Logger.getLogger(SqlFunctionVa.class);
    private BigDecimal totMutDb;
    private BigDecimal totMutCb;
    private int jmlItmD;
    private int jmlItmC;
    private BigDecimal totD;
    private BigDecimal totC;
    private BigDecimal sld_akhir;

    public int getJmlItmD() {
        return jmlItmD;
    }

    public void setJmlItmD(int jmlItmD) {
        this.jmlItmD = jmlItmD;
    }

    public int getJmlItmC() {
        return jmlItmC;
    }

    public void setJmlItmC(int jmlItmC) {
        this.jmlItmC = jmlItmC;
    }

    public BigDecimal getTotD() {
        return totD;
    }

    public void setTotD(BigDecimal totD) {
        this.totD = totD;
    }

    public BigDecimal getTotC() {
        return totC;
    }

    public void setTotC(BigDecimal totC) {
        this.totC = totC;
    }

    public void setTotMutDb(BigDecimal totMutDb) {
        this.totMutDb = totMutDb;
    }

    public void setTotMutCb(BigDecimal totMutCb) {
        this.totMutCb = totMutCb;
    }

    public BigDecimal getSld_akhir() {
        return sld_akhir;
    }

    public void setSld_akhir(BigDecimal sld_akhir) {
        this.sld_akhir = sld_akhir;
    }

    public void closeConnDb(Connection conn, PreparedStatement stat,
            ResultSet rs) {
        if (conn != null) {
            try {
                conn.close();
                conn = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 1 : " + ex.getMessage());
            }
        }
        if (rs != null) {
            try {
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 2 : " + ex.getMessage());
            }
        }
        if (stat != null) {
            try {
                stat.close();
                stat = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 3 : " + ex.getMessage());
            }
        }
    }

    public List getListBukubesar(String tgl1, String tgl2, String ac_no,
            String brnch, String ccy, String cbsrekoto, BigDecimal saldo) {
        List lbb = new ArrayList();
        Bukubesar bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
//					+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
//					+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
//					+ ") || ' ' || (select s.contract_ref_no || ' ' || c.customer_name1 from cstb_contract s, lctb_contract_master b, sttm_customer c "
//					+ "where s.contract_ref_no = b.contract_ref_no and s.counterparty = c.customer_no and s.contract_ref_no(+) = a.trn_ref_no "
//					+ "group by s.contract_ref_no , c.customer_name1) CF_ADDLTEXT,"
//					+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount "
//					+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
//					+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
//					+ "and a.ac_branch = ? ";

//			sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
//				+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
//				+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
//				+ ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
//				+ "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
//				+ "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)"
//				+ "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
//				+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, "
//				+ "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code "
//	            + "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no "
//				+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
//				+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
//				+ "and a.ac_branch = ? ";

            sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " +
                    "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
                    "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
                    "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
                    ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
                    "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
                    "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
                    "group by s.user_ref_no , c.cust_name) || ', ' || " +
                    "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
                    "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " +
                    "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
                    "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
                    "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
                    "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
                    "from ACVWS_ALL_AC_ENTRIES_HIST a, sttms_trn_code s, setb_event_log c  " +
                    "where a.trn_code = s.trn_code " +
                    "and a.trn_code = s.trn_code " +
                    "and c.event_reference(+) = a.trn_ref_no " +
                    "and c.esn(+) = a.event_sr_no " +
                    "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
                    "a.ac_no = ? and a.ac_branch = ? ";

            if ("485052001".equals(ac_no) || "585054001".equals(ac_no)) {
            } else {
                sql += " and a.ac_ccy = ? ";
            }
            if (!"1".equals(cbsrekoto)) {
                sql += " and a.ac_no not in (?) ";
            }
            sql += " order by a.AC_ENTRY_SR_NO, a.trn_dt  ";
            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, tgl1.trim());
            stat.setString(i++, tgl2.trim());
            stat.setString(i++, ac_no.trim());
            stat.setString(i++, brnch.trim());
            if ("485052001".equals(ac_no) || "585054001".equals(ac_no)) {
            } else {
                stat.setString(i++, ccy.trim());
            }
            if (!"1".equals(cbsrekoto)) {
                stat.setString(i++, "195001006");
            }
            ;
            rs = stat.executeQuery();
            totMutCb = new BigDecimal(0);
            totMutDb = new BigDecimal(0);
            BigDecimal mutDb = new BigDecimal(0);
            BigDecimal mutCb = new BigDecimal(0);
            BigDecimal mutDbEq = new BigDecimal(0);
            BigDecimal mutCbEq = new BigDecimal(0);
            while (rs.next()) {
                bb = new Bukubesar();
                bb.setTrn_ref_no(rs.getString("trn_ref_no"));
                bb.setTrn_dt(rs.getDate("trn_dt"));
                bb.setValue_dt(rs.getDate("value_dt"));
                bb.setDrcr(rs.getString("drcr_ind"));
                if (rs.getString("batch_no") != null) {
                    bb.setBatch_no(rs.getString("batch_no"));
                } else {
                    bb.setBatch_no("");
                }
                bb.setAc_no(rs.getString("ac_no"));
                bb.setMaker_id(rs.getString("user_id"));
                bb.setAuth_id(rs.getString("auth_id"));
                bb.setEvent(rs.getString("event"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setKet(rs.getString("CF_ADDLTEXT"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setAc_ccy(rs.getString("ac_ccy"));
                bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
                bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));

                if ("D".equals(bb.getDrcr().trim())) {
                    if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
                        mutDb = bb.getLcy_amount();
                    } else {
                        mutDb = bb.getFcy_amount();
                        mutDbEq = bb.getLcy_amount();
                    }
                } else {
                    mutDb = new BigDecimal(0);
                    mutDbEq = new BigDecimal(0);
                }
                totMutDb = totMutDb.subtract(mutDb);
                bb.setAmt_db_eq(mutDbEq);
                bb.setAmt_db(mutDb);
                if ("C".equals(bb.getDrcr().trim())) {
                    if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
                        mutCb = bb.getLcy_amount();
                    } else {
                        mutCb = bb.getFcy_amount();
                        mutCbEq = bb.getLcy_amount();
                    }
                } else {
                    mutCb = new BigDecimal(0);
                    mutCbEq = new BigDecimal(0);
                }
                bb.setAmt_cr_eq(mutCbEq);
                bb.setAmt_cr(mutCb);
                totMutCb = totMutCb.add(mutCb);
                if ("C".equals(bb.getDrcr().trim())) {
                    saldo = saldo.add(bb.getAmt_cr());
                } else if ("D".equals(bb.getDrcr().trim())) {
                    saldo = saldo.subtract(bb.getAmt_db());
                }
                bb.setSaldo(saldo);
                bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListBukubesar : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List searchBukubesar(String trn_ref_no) {
        List lbb = new ArrayList();
        Bukubesar bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            String sql1 = "";
            if (!"".equals(trn_ref_no) && trn_ref_no != null) {
                sql = "select a.trn_dt, a.batch_no, a.ac_no, a.ac_branch, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " + "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||(" + "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no" + ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " + "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " + "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)" + "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT," + "a.drcr_ind, a.ac_ccy, a.exch_rate, a.fcy_amount, a.lcy_amount " + "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code " + "where a.trn_ref_no = ? order by a.trn_dt desc";
            } else {
                sql = "select a.trn_dt, a.batch_no, a.ac_no, a.ac_branch, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " + "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||(" + "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no" + ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " + "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " + "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)" + "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT," + "a.drcr_ind, a.ac_ccy, a.exch_rate, a.fcy_amount, a.lcy_amount " + "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code " +
                        "order by a.trn_dt desc";
            }

            stat = conn.prepareStatement(sql);
            if (!"".equals(trn_ref_no) && trn_ref_no != null) {
                stat.setString(1, trn_ref_no);
            }
            rs = stat.executeQuery();
            totMutCb = new BigDecimal(0);
            totMutDb = new BigDecimal(0);
            BigDecimal mutDb = new BigDecimal(0);
            BigDecimal mutCb = new BigDecimal(0);
            while (rs.next()) {
                bb = new Bukubesar();
                bb.setTrn_ref_no(rs.getString("trn_ref_no"));
                bb.setTrn_dt(rs.getDate("trn_dt"));
                bb.setValue_dt(rs.getDate("VALUE_DT"));
                bb.setDrcr(rs.getString("drcr_ind"));
                bb.setBatch_no(rs.getString("batch_no"));
                bb.setAc_no(rs.getString("ac_no"));
                bb.setAc_branch(rs.getString("ac_branch"));
                bb.setMaker_id(rs.getString("user_id"));
                bb.setAuth_id(rs.getString("AUTH_ID"));
                bb.setEvent(rs.getString("event"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setKet(rs.getString("CF_ADDLTEXT"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setAc_ccy(rs.getString("ac_ccy"));
                if (rs.getBigDecimal("exch_rate") == null) {
                    bb.setExh_rate(new BigDecimal(0));
                } else {
                    bb.setExh_rate(rs.getBigDecimal("exch_rate"));
                }
                if (rs.getBigDecimal("fcy_amount") == null) {
                    bb.setFcy_amount(new BigDecimal(0));
                } else {
                    bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
                }
                if (rs.getBigDecimal("lcy_amount") == null) {
                    bb.setLcy_amount(new BigDecimal(0));
                } else {
                    bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
                }
                if ("D".equals(bb.getDrcr().trim())) {
                    if ("IDR".equals(bb.getAc_ccy())) {
                        mutDb = bb.getLcy_amount();
                    } else {
                        mutDb = bb.getFcy_amount();
                    }
                    totMutDb = totMutDb.subtract(bb.getLcy_amount());
                } else {
                    mutDb = new BigDecimal(0);
                }
//				totMutDb = totMutDb.subtract(mutDb);
                bb.setAmt_db(mutDb);
                if ("C".equals(bb.getDrcr().trim())) {
                    if ("IDR".equals(bb.getAc_ccy())) {
                        mutCb = bb.getLcy_amount();
                    } else {
                        mutCb = bb.getFcy_amount();
                    }
                    totMutCb = totMutCb.add(bb.getLcy_amount());
                } else {
                    mutCb = new BigDecimal(0);
                }
                bb.setAmt_cr(mutCb);
//				totMutCb = totMutCb.add(mutCb);
                lbb.add(bb);
            }
        } catch (SQLException ex) {

            // ex.getMessage());
            log.error("Error in searchBukubesar : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListbatch(String userId) {
        List lbb = new ArrayList();
        BatchMaster bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "SELECT last_oper_dt_stamp, batch_no,description,branch_code, last_oper_id,Auth_Stat,dr_ent_total,cr_ent_total FROM DETB_BATCH_MASTER where last_oper_id=? ";

            stat = conn.prepareStatement(sql);
            stat.setString(1, userId);
            rs = stat.executeQuery();

            while (rs.next()) {
                bb = new BatchMaster();
                bb.setBatch_no(rs.getString("batch_no"));
                bb.setDescription(rs.getString("description"));
                bb.setLast_oper_id(rs.getString("last_oper_id"));
                bb.setCr_ent_total(rs.getBigDecimal("cr_ent_total"));
                bb.setDr_ent_total(rs.getBigDecimal("dr_ent_total"));
                bb.setLast_oper_dt_stamp(rs.getDate("last_oper_dt_stamp"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListbatch : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getDetailBatch(String batch_no, String userId) {
        List lbb = new ArrayList();
        DetailBatch bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select a.trn_ref_no,a.trn_dt,a.user_id,a.event,a.ac_entry_sr_no," + "a.ac_branch,a.ac_no,d.gl_desc, a.ac_ccy,a.drcr_ind,a.trn_code,a.lcy_amount,a.auth_id," + "b.trn_desc, remarks from actb_daily_log a, sttms_trn_code b, csvw_addl_text c, gltm_glmaster d " + "where a.trn_code = b.trn_code and a.ac_no = d.gl_code and a.trn_ref_no = c.reference_no and batch_no=? and user_id = ? " + "order by trn_ref_no";

            stat = conn.prepareStatement(sql);
            stat.setString(1, batch_no);
            stat.setString(2, userId);

            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new DetailBatch();
                bb.setTrn_ref_no(rs.getString("trn_ref_no"));
                bb.setTrn_dt(rs.getDate("trn_dt"));
                bb.setLast_oper_id(rs.getString("user_id"));
                bb.setEvent(rs.getString("event"));
                bb.setAc_entry_sr_no(rs.getString("ac_entry_sr_no"));
                bb.setAc_branch(rs.getString("ac_branch"));
                bb.setAc_no(rs.getString("ac_no"));
                bb.setAc_ccy(rs.getString("ac_ccy"));
                bb.setDrcr_ind(rs.getString("drcr_ind"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
                bb.setAuth_id(rs.getString("auth_id"));
                bb.setTrn_desc(rs.getString("trn_desc"));
                bb.setRemarks(rs.getString("remarks"));
                bb.setGl_desc(rs.getString("gl_desc"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListbatch : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListbatchh(String userId, String branch, String tgl1,
            String tgl2, String batch) {
        List lbb = new ArrayList();
        BatchMaster bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "SELECT last_oper_dt_stamp, batch_no,description,branch_code," + "last_oper_id,Auth_Stat,dr_ent_total,cr_ent_total FROM DETB_BATCH_MASTER_HIST " + "where last_oper_dt_stamp >= TO_DATE(?, 'DD-MM-YYYY') and " + "last_oper_dt_stamp <= TO_DATE(?, 'DD-MM-YYYY')  AND auth_stat=? ";
            if (!"".equals(userId) && userId != null) {
                sql += " and last_oper_id = ? ";
            }
            if (!"".equals(branch) && branch != null) {
                sql += " and branch_code = ? ";
            }
            if (!"".equals(batch) && batch != null) {
                sql += " and batch_no = ? ";
            }
            sql += " order by last_oper_dt_stamp ";

            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, tgl1);
            stat.setString(i++, tgl2);
            stat.setString(i++, "A");
            if (!"".equals(userId) && userId != null) {
                stat.setString(i++, userId);
            }
            if (!"".equals(branch) && branch != null) {
                stat.setString(i++, branch);
            }
            if (!"".equals(batch) && batch != null) {
                stat.setString(i++, batch);
            }
            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new BatchMaster();
                bb.setBatch_no(rs.getString("batch_no"));
                bb.setDescription(rs.getString("description"));
                bb.setLast_oper_id(rs.getString("last_oper_id"));
                bb.setAuth_stat(rs.getString("auth_stat"));
                bb.setCr_ent_total(rs.getBigDecimal("cr_ent_total"));
                bb.setDr_ent_total(rs.getBigDecimal("dr_ent_total"));
                bb.setLast_oper_dt_stamp(rs.getDate("last_oper_dt_stamp"));
                bb.setBranch_code(rs.getString("branch_code"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListbatch : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getDetailBatchh(String batch_no, String userId, String kdcab, String trn_dt) {
        List lbb = new ArrayList();
        DetailBatch bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select a.batch_no, a.trn_ref_no,a.trn_dt,a.user_id,a.event,a.ac_entry_sr_no,a.ac_branch,a.ac_no,d.gl_desc," + "a.ac_ccy,a.drcr_ind,a.trn_code,a.lcy_amount,a.auth_id, b.trn_desc," + "(select remarks from csvw_addl_text where reference_no = a.trn_ref_no and serial_no = a.curr_no ) remarks " + "from actb_history a, sttms_trn_code b, gltm_glmaster d " + "where a.trn_code = b.trn_code  and  a.batch_no=? and a.user_id = ? and " + "a.ac_no = d.gl_code and a.ac_branch= ? and a.trn_dt = to_date(?, 'YYYY-MM-DD') order by trn_ref_no, a.ac_entry_sr_no";

            stat = conn.prepareStatement(sql);

            stat.setString(1, batch_no);
            stat.setString(2, userId);
            stat.setString(3, kdcab);
            stat.setString(4, trn_dt);

            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new DetailBatch();
                bb.setTrn_ref_no(rs.getString("trn_ref_no"));
                bb.setTrn_dt(rs.getDate("trn_dt"));
                bb.setLast_oper_id(rs.getString("user_id"));
                bb.setEvent(rs.getString("event"));
                bb.setAc_entry_sr_no(rs.getString("ac_entry_sr_no"));
                bb.setAc_branch(rs.getString("ac_branch"));
                bb.setAc_no(rs.getString("ac_no"));
                bb.setAc_ccy(rs.getString("ac_ccy"));
                bb.setDrcr_ind(rs.getString("drcr_ind"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setLcy_amount((rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount"));
                bb.setAuth_id(rs.getString("auth_id"));
                bb.setTrn_desc(rs.getString("trn_desc"));
                bb.setRemarks(rs.getString("remarks"));
                bb.setBatch_no(rs.getString("batch_no"));
                bb.setGl_desc(rs.getString("gl_desc"));

                lbb.add(bb);

            }

        } catch (SQLException ex) {

            log.error(" getListbatch : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public BigDecimal getTotMutCb() {
        return totMutCb;
    }

    public BigDecimal getTotMutDb() {
        return totMutDb;
    }

    public Saldo getSaldo(String acc, String brnch, String ccy, String dt1) {
        Saldo sld = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            // conn = ConnToDb.getConnectionOracle();
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = "select branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal from actb_accbal_history where account=? and branch_code=? and acc_ccy = ? "
//					+ "and BKG_DATE < TO_DATE(?, 'DD-MM-YY') and rownum = ? order by bkg_date desc ";
//			sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
//					"from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
//					"and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE <= TO_DATE(?, 'DD-MM-YY'))";
            DateFormat formatter = null;
            Date convertedDate = null;
            Date todayDate = new Date();
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
            try {
                formatter = new SimpleDateFormat("dd-MM-yyyy");
                convertedDate = (Date) formatter.parse(dt1);
                todayDate = dateFormatter.parse(dateFormatter.format(new Date()));
            } catch (Exception e) {
                log.info(e.getMessage());
            }
            if (todayDate.compareTo(convertedDate) == 0) {
                sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
                        "from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
                        "and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE < TO_DATE(?, 'DD-MM-YY'))";
            } else {
                sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
                        "from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
                        "and BKG_DATE = (select min(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE >= TO_DATE(?, 'DD-MM-YY'))";
            }
            stat = conn.prepareStatement(sql);
            stat.setString(1, acc.trim());
            stat.setString(2, brnch.trim());
            if ("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())) {
                stat.setString(3, "IDR");
            } else {
                stat.setString(3, ccy.trim());
            }

            stat.setString(4, acc.trim());
            stat.setString(5, brnch.trim());
            if ("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())) {
                stat.setString(6, "IDR");
            } else {
                stat.setString(6, ccy.trim());
            }
            stat.setString(7, dt1.trim());
            rs = stat.executeQuery();
            while (rs.next()) {
                sld = new Saldo();
                sld.setBranch_code(rs.getString("branch_code"));
                sld.setAccount(rs.getString("account"));
                sld.setAcc_ccy(rs.getString("acc_ccy"));
                if (todayDate.compareTo(convertedDate) == 0) {
                    sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
                    sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
                    sld.setAcy_opening_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
                    sld.setLcy_opening_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
                } else {
                    sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
                    sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
                    sld.setAcy_opening_bal((rs.getBigDecimal("acy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_opening_bal"));
                    sld.setLcy_opening_bal((rs.getBigDecimal("lcy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_opening_bal"));
                }

            }

            if (sld == null) {
                sld = new Saldo();
                sld.setBranch_code(brnch);
                sld.setAccount(acc);
                sld.setAcc_ccy(ccy);
                sld.setAcy_closing_bal(new BigDecimal(0));
                sld.setLcy_closing_bal(new BigDecimal(0));
                sld.setAcy_opening_bal(new BigDecimal(0));
                sld.setLcy_opening_bal(new BigDecimal(0));
            }
        } catch (SQLException ex) {
            log.error("error in getSaldo : " + ex.getMessage());

        } finally {
            closeConnDb(conn, stat, rs);
        }
        return sld;
    }

    public BigDecimal getSaldo2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto) {
        BigDecimal saldo = new BigDecimal(0);
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "SELECT inc, trn_dt, batch_no, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, " + "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount FROM " + "(select rownum as inc, a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " + "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||(  " + "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " + ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " + "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " + "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " + "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT," + "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount " + "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code " + "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " + "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? ";

            if (!"1".equals(cbsrekoto)) {
                sql += " and a.ac_no not in (?) order by a.AC_ENTRY_SR_NO, a.trn_dt  ";
            }
            sql += ") where inc >= ? and inc <= ? ";
            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, tgl1.trim());
            stat.setString(i++, tgl2.trim());
            stat.setString(i++, ac_no.trim());
            stat.setString(i++, brnch.trim());
            stat.setString(i++, ccy.trim());
            if (!"1".equals(cbsrekoto)) {
                stat.setString(i++, "195001006");
            }
            stat.setInt(i++, begin);
            stat.setInt(i++, delta);
            rs = stat.executeQuery();
            BigDecimal mutDb = new BigDecimal(0);
            BigDecimal mutCb = new BigDecimal(0);
            while (rs.next()) {
                if ("D".equals(rs.getString("drcr_ind"))) {
                    if ("IDR".equals(rs.getString("ac_ccy"))) {
                        mutDb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
                    } else {
                        mutDb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
                    }
                } else {
                    mutDb = new BigDecimal(0);
                }
                if ("C".equals(rs.getString("drcr_ind"))) {
                    if ("IDR".equals(rs.getString("ac_ccy"))) {
                        mutCb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
                    } else {
                        mutCb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
                    }
                } else {
                    mutCb = new BigDecimal(0);
                }
                if ("C".equals(rs.getString("drcr_ind"))) {
                    saldo = saldo.add(mutCb);
                } else if ("D".equals(rs.getString("drcr_ind"))) {
                    saldo = saldo.subtract(mutDb);
                }
            }
        } catch (SQLException ex) {
            log.error(" getSaldo2 : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return saldo;
    }

    public List getListPCdMcb(String dt, String network, String rtgs,
            String trncd1, String trncd2, String trncd3, String norekttpn) {
        List lbb = new ArrayList();
        Pcode bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            String pcd = "";
            if ("RTGSBI".equals(network)) {
                pcd = "ROIB";
            } else if ("SKNBI".equals(network)) {
                pcd = "SOIB";
            }
            if ("out".equals(rtgs)) {
                sql = "select distinct pc as product_code, status, sum(total) as total, sum(nominal) as nominal from ( " + "select ( case when product_code = '" + pcd + "' then 'IB/CMS' when product_code <> 'ROIB' then 'MCB' end ) pc, status, total, nominal " + "from( select distinct product_code, status, count(*) as total, sum(txn_amount) as nominal " + "from( select contract_ref_no, dispatch_ref_no, checker_id, exception_queue, product_code, txn_amount," + "(case when dispatch_ref_no is not null and checker_id is not null and exception_queue = '##' then 'PROCEED' else 'UNPROCEED' end) status " + "from PCTBS_CONTRACT_MASTER where activation_dt = TO_DATE(?, 'DD-MM-YY') and network = ? )" + "group by product_code, status " + "order by product_code, status desc )" + ")group by pc, status order by pc, status desc ";
            } else if ("in".equals(rtgs)) {
                sql = "select count(*) as total, sum(lcy_amount) as nominal " + "from actb_daily_log where trn_code in (?, ?, ?) and (length(ac_no) = ? or ac_no = ?) and " + "trn_dt =TO_DATE(?, 'DD-MM-YY') and user_id = ? ";
            }

            stat = conn.prepareStatement(sql);
            if ("out".equals(rtgs)) {
                stat.setString(1, dt);
                stat.setString(2, network);
            } else if ("in".equals(rtgs)) {
                stat.setString(1, trncd1);
                stat.setString(2, trncd2);
                stat.setString(3, trncd3);
                stat.setInt(4, 10);
                stat.setString(5, norekttpn);
                stat.setString(6, dt);
                stat.setString(7, "FLEXGW");
            }
            rs = stat.executeQuery();

            while (rs.next()) {
                bb = new Pcode();
                if ("out".equals(rtgs)) {
                    bb.setPcd(rs.getString("product_code"));
                    bb.setSts(rs.getString("status"));
                } else if ("in".equals(rtgs)) {
                    bb.setPcd("-");
                    bb.setSts("-");
                }
                bb.setTot(rs.getInt("total"));
                if (rs.getBigDecimal("nominal") == null) {
                    bb.setNom(new BigDecimal(0));
                } else {
                    bb.setNom(rs.getBigDecimal("nominal"));
                }
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListPCd : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListPCdIntf(String rtgs) {
        List lbb = new ArrayList();
        Pcode bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
            if ("out".equals(rtgs)) {
//				sql = "SELECT DISTINCT substring(reltrn,4,4) AS product_code, Status, count(*) AS total,"
//						+ "sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) as nominal FROM dbo.T_RTIFTSOUT "
//						+ "GROUP BY substring(reltrn,4,4), Status ORDER BY product_code";
                sql = "SELECT DISTINCT product_code, Status, sum(total) AS total, sum(nominal) AS nominal " +
                        "FROM (" +
                        "SELECT CASE WHEN substring(reltrn,4,4) = 'CROP' THEN 'MCB' ELSE 'IB/CMS + TITIPAN' END AS product_code," +
                        "Status, count(*) AS total, sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) as nominal FROM dbo.T_RTIFTSOUT " +
                        "GROUP BY substring(reltrn,4,4), Status) a GROUP BY product_code, Status ";
            } else if ("in".equals(rtgs)) {
                sql = "SELECT Status, count(*) AS total, " + "sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) AS nominal FROM dbo.T_RTIFTSIN " + "GROUP BY Status ORDER BY Status";
            }
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new Pcode();
                if ("out".equals(rtgs)) {
                    bb.setPcd(rs.getString("product_code"));
                } else {
                    bb.setPcd("-");
                }
                bb.setSts(rs.getString("status"));
                bb.setTot(rs.getInt("total"));
                bb.setNom(rs.getBigDecimal("nominal"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {

            log.error(" getListPCdIntf : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListPCdIntfSkn(String skn, String tbl) {
        List lbb = new ArrayList();
        Pcode bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getSknDS().getConnection();
            if ("out".equals(skn)) {
//				sql = "SELECT DISTINCT substring(od_ref_no,4,4) AS product_code, CAST(od_status1 AS CHAR(1)) +''+ CAST(od_status2 AS CHAR(1)) AS status, "
//						+ "count(*) AS total, sum(od_amount) AS nominal FROM "
//						+ tbl
//						+ " "
//						+ "GROUP BY CAST(od_status1 AS CHAR(1)) +''+ CAST(od_status2 AS CHAR(1)), substring(od_ref_no,4,4) ";
                sql = "SELECT DISTINCT product_code, CAST(od_status1 AS CHAR(1)) +''+ CAST(od_status2 AS CHAR(1)) AS status, " +
                        "sum(total) AS total, sum(nominal) AS nominal " +
                        "FROM (" +
                        "SELECT CASE WHEN product_code = 'CSOP' THEN 'MCB' ELSE 'IB/CMS/SKNMSSL + TITIPAN' END product_code, " +
                        "od_status1, od_status2, sum(total) total, sum(nominal) nominal " +
                        "FROM( " +
                        "SELECT DISTINCT substring(od_ref_no,4,4) AS product_code, od_status1, od_status2, count(*) AS total, sum(od_amount) AS nominal " +
                        "FROM " + tbl + " GROUP BY od_status1, od_status2, substring(od_ref_no,4,4)) asd " +
                        "GROUP BY product_code, od_status1, od_status2) asddsf " +
                        "GROUP BY product_code, od_status1, od_status2";

            } else if ("in".equals(skn)) {
                sql = "SELECT DISTINCT CAST(id_status_host AS CHAR(1)) AS status, '-' AS product_code, " +
                        "count(*) AS total, sum(id_amount) AS nominal " +
                        "FROM " + tbl + " " +
                        "GROUP BY id_status_host";
            }
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new Pcode();
                if ("out".equals(skn)) {
                    bb.setPcd(rs.getString("product_code"));
                } else {
                    bb.setPcd("-");
                }
                bb.setSts(rs.getString("status"));
                bb.setTot(rs.getInt("total"));
                bb.setNom(rs.getBigDecimal("nominal"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {

            log.error(" getListPCdIntf : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public Gl getGl(String acc) {
        Gl gl = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select gl_code, gl_desc from gltm_glmaster where gl_code = ? ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, acc);
            rs = stat.executeQuery();
            if (rs.next()) {
                gl = new Gl();
                gl.setGl_code(rs.getString("gl_code"));
                gl.setGl_desc(rs.getString("gl_desc"));
            }
        } catch (SQLException ex) {
            log.error("error getGl : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return gl;
    }

    public List getLspCurrent(String acc, String branch, String ccy, String allcab, String allval) {
        List lsp = new ArrayList();
        Lsp sp = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = "select ac_no, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan " +
//					"from (select h.ac_no, h.ac_branch, h.ac_ccy," +
//					"sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, " +
//					"sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL " +
//					"from ACVWS_ALL_AC_ENTRIES_HIST h " +
//					"where h.trn_dt = to_date(sysdate, 'DD-MM-YY') and h.ac_no = ? ";
            sql = "select ac_no, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan " +
                    "from (select h.ac_no, h.ac_branch, h.ac_ccy," +
                    "sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, " +
                    "sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL " +
                    "from actb_daily_log h " +
                    "where h.ac_no = ? and h.delete_stat <> ? ";
            if (allval == null) {
                sql += " and h.ac_ccy = ? ";
            }
            if ("1".equals(allcab)) {
            } else {
                sql += " and h.ac_branch = ? ";
            }
            sql += "group by h.ac_no, h.ac_branch, h.ac_ccy) order by ac_ccy";
            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, acc);
            stat.setString(i++, "D");
            if (allval == null) {
                stat.setString(i++, ccy);
            }
            if ("1".equals(allcab)) {
            } else {
                stat.setString(i++, branch);
            }
            rs = stat.executeQuery();
            while (rs.next()) {
                sp = new Lsp();
                sp.setBranch_code(rs.getString("ac_branch"));
                sp.setGl_code(rs.getString("ac_no"));
                sp.setCcy(rs.getString("ac_ccy"));
                sp.setSld_prcobaan((rs.getBigDecimal("saldoPercobaan") == null) ? new BigDecimal(0) : rs.getBigDecimal("saldoPercobaan"));
                lsp.add(sp);
            }
        } catch (SQLException ex) {
            log.error("error getLspCurrent : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lsp;
    }

    public List getListLsp(String ccy, String brnch, String gl_a, String gl_b, String allcab, String allval, String cbxspnoll) {
        List llsp = new ArrayList();
        Lsp bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            if ("1".equals(allcab)) {
                sql = "  select ac_no, gl_desc, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan " + "from (select h.ac_no, a.gl_desc, h.ac_branch, ac_ccy, " + "sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, " + "sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL " + "from ACVWS_ALL_AC_ENTRIES_HIST h, gltm_glmaster a " + "where a.leaf = ? and h.ac_no >= ? and h.ac_no <= ? and  a.gl_code = h.ac_no ";
                if (allval == null) {
                    sql += " and h.ac_ccy = ? ";
                }

                sql += "group by h.ac_no, a.gl_desc, h.ac_branch, h.ac_ccy ) ";
            } else {
                sql = "  select ac_no, gl_desc, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan " + "from (select h.ac_no, a.gl_desc, h.ac_branch, ac_ccy, " + "sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, " + "sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL " + "from ACVWS_ALL_AC_ENTRIES_HIST h, gltm_glmaster a " + "where a.leaf = ? and h.ac_branch = ? and h.ac_no >= ? and h.ac_no <= ? and  a.gl_code = h.ac_no ";
                if (allval == null) {
                    sql += " and h.ac_ccy = ? ";
                }
                sql += "group by h.ac_no, a.gl_desc, h.ac_branch, h.ac_ccy ) ";
            }
            if (cbxspnoll == null) {
                sql += " where creditGL - debitGL <> ? ";
            }
            sql += " order by ac_branch, ac_no ";
            int i = 1;
            stat = conn.prepareStatement(sql);
            if ("1".equals(allcab)) {
                stat.setString(i++, "Y");
                stat.setString(i++, gl_a);
                stat.setString(i++, gl_b);
            } else {
                stat.setString(i++, "Y");
                stat.setString(i++, brnch);
                stat.setString(i++, gl_a);
                stat.setString(i++, gl_b);
            }
            if (allval == null) {
                stat.setString(i++, ccy);
            }
            if (cbxspnoll == null) {
                stat.setInt(i++, 0);
            }
            rs = stat.executeQuery();
            BigDecimal temp = new BigDecimal(0);
            while (rs.next()) {
                bb = new Lsp();
                bb.setGl_code(rs.getString("ac_no"));
                bb.setGl_desc(rs.getString("gl_desc"));
                bb.setBranch_code(rs.getString("ac_branch"));
                bb.setCcy(rs.getString("ac_ccy"));
                bb.setMov_lcy(rs.getBigDecimal("saldoPercobaan"));
                llsp.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListLSp : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return llsp;
    }

    public List getListLspTotal(String ccy, String brnch, String gl_a, String gl_b, String allcab, String allval, String cbxspnoll) {
        List llsp = new ArrayList();
        Lsp bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            if ("1".equals(allcab)) {
                sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, " +
                        "h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
                        "from actb_accbal_history h, gltm_glmaster a " +
                        "where a.leaf = ? and a.gl_code = h.account and h.account>= ? and h.account <= ? ";
                if (allval == null) {
                    sql += " and h.acc_ccy = ? ";
                }
                sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account  and acc_ccy = h.acc_ccy and branch_code= h.branch_code ";
                if (allval == null) {
                    sql += " and acc_ccy = ? ";
                }
                sql += ") " + "order by h.account, h.branch_code, h.BKG_DATE asc ";
            } else {
                sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, " +
                        "h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
                        "from actb_accbal_history h, gltm_glmaster a " +
                        "where a.leaf = ? and a.gl_code = h.account and h.account>= ? and h.account <= ? and h.branch_code= ? ";
                if (allval == null) {
                    sql += " and h.acc_ccy = ? ";
                }
                sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account and acc_ccy = h.acc_ccy and branch_code= h.branch_code and branch_code=? ";
                if (allval == null) {
                    sql += " and acc_ccy = ? ";
                }
                sql += ") " + "order by h.account, h.branch_code, h.BKG_DATE asc ";
            }
            int i = 1;
            stat = conn.prepareStatement(sql);
            if ("1".equals(allcab)) {
                stat.setString(i++, "Y");
                stat.setString(i++, gl_a);
                stat.setString(i++, gl_b);
                if (allval == null) {
                    stat.setString(i++, ccy);
                    stat.setString(i++, ccy);
                }
            } else {
                stat.setString(i++, "Y");
                stat.setString(i++, gl_a);
                stat.setString(i++, gl_b);
                stat.setString(i++, brnch);
                if (allval == null) {
                    stat.setString(i++, ccy);
                }
                stat.setString(i++, brnch);
                if (allval == null) {
                    stat.setString(i++, ccy);
                }
            }
            rs = stat.executeQuery();
            BigDecimal temp = new BigDecimal(0);
            while (rs.next()) {
                bb = new Lsp();
                bb.setGl_code(rs.getString("account"));
                bb.setGl_desc(rs.getString("gl_desc"));
                bb.setBranch_code(rs.getString("branch_code"));
                bb.setCcy(rs.getString("acc_ccy"));
//				BigDecimal lspCurr = getLspCurrent(rs.getString("account").trim(), rs.getString("branch_code").trim(), rs.getString("acc_ccy").trim());
                BigDecimal t = new BigDecimal(0);
                if ("IDR".equals(ccy.toUpperCase())) {
//					t = rs.getBigDecimal("lcy_closing_bal").add(lspCurr);
                    bb.setMov_lcy(t);
                } else {
//					t = rs.getBigDecimal("acy_closing_bal").add(lspCurr);
                    bb.setMov_lcy(t);
                }

                if (cbxspnoll == null) {
                    if (t.compareTo(new BigDecimal(0)) != 0) {
                        llsp.add(bb);
                    }
                } else {
                    llsp.add(bb);
                }
            }
        } catch (SQLException ex) {
            log.error(" getListLspTotal : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return llsp;
    }

    public List getListGL(String gl_fr, String gl_to) {
        List lbb = new ArrayList();
        Gl bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select gl_code, gl_desc from gltm_glmaster where gl_code >= ? and gl_code <= ? order by gl_code asc";
            stat = conn.prepareStatement(sql);
            stat.setString(1, gl_fr);
            stat.setString(2, gl_to);
            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new Gl();
                bb.setGl_code(rs.getString("gl_code"));
                bb.setGl_desc(rs.getString("gl_desc"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListGL : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListLspBefore(String ccy, String brnch, String gl_a, String allcab, String allval, String cbxspnoll) {
        List llsp = new ArrayList();
        Lsp bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            if ("1".equals(allcab)) {
                sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, " +
                        "h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
                        "from actb_accbal_history h, gltm_glmaster a " +
                        "where a.leaf = ? and a.gl_code = h.account and h.account= ? ";
                if (allval == null) {
                    sql += " and h.acc_ccy = ? ";
                }
                sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account  and acc_ccy = h.acc_ccy and branch_code= h.branch_code ";
                if (allval == null) {
                    sql += " and acc_ccy = ? ";
                }
                sql += ") " + "order by h.account, h.branch_code, h.acc_ccy, h.BKG_DATE asc ";
            } else {
                sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, " +
                        "h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
                        "from actb_accbal_history h, gltm_glmaster a " +
                        "where a.leaf = ? and a.gl_code = h.account and h.account= ? and h.branch_code= ? ";
                if (allval == null) {
                    sql += " and h.acc_ccy = ? ";
                }
                sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account and acc_ccy = h.acc_ccy and branch_code= h.branch_code and branch_code=? ";
                if (allval == null) {
                    sql += " and acc_ccy = ? ";
                }
                sql += ") " + "order by h.account, h.branch_code, h.acc_ccy, h.BKG_DATE asc ";
            }
            int i = 1;
            stat = conn.prepareStatement(sql);
            if ("1".equals(allcab)) {
                stat.setString(i++, "Y");
                stat.setString(i++, gl_a);
                if (allval == null) {
                    stat.setString(i++, ccy);
                    stat.setString(i++, ccy);
                }
            } else {
                stat.setString(i++, "Y");
                stat.setString(i++, gl_a);
                stat.setString(i++, brnch);
                if (allval == null) {
                    stat.setString(i++, ccy);
                }
                stat.setString(i++, brnch);
                if (allval == null) {
                    stat.setString(i++, ccy);
                }
            }
            rs = stat.executeQuery();
            BigDecimal temp = new BigDecimal(0);
            while (rs.next()) {
                bb = new Lsp();
                bb.setGl_code(rs.getString("account"));
                bb.setGl_desc(rs.getString("gl_desc"));
                bb.setBranch_code(rs.getString("branch_code"));
                bb.setCcy(rs.getString("acc_ccy"));
                bb.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
                bb.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
                llsp.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListLspBefore : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return llsp;
    }

    public List getcontohbuku2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto, BigDecimal saldo) {
        List lbb = new ArrayList();
        Bukubesar bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();

            sql = "SELECT inc, trn_dt, batch_no, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, " +
                    "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount, rcc_no " +
                    "FROM (select rownum as inc, a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " +
                    "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
                    "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
                    "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
                    ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' '|| (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
                    "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
                    "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
                    "group by s.user_ref_no , c.cust_name) || ', ' || " +
                    "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
                    "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " +
                    "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
                    "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
                    "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
                    "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
                    "from ACVWS_ALL_AC_ENTRIES_HIST a, sttms_trn_code s, setb_event_log c  " +
                    "where a.trn_code = s.trn_code " +
                    "and a.trn_code = s.trn_code " +
                    "and c.event_reference(+) = a.trn_ref_no " +
                    "and c.esn(+) = a.event_sr_no " +
                    "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
                    "a.ac_no = ? and a.ac_branch = ? ";
            if ("485052001".equals(ac_no) || "585054001".equals(ac_no)) {
            } else {
                sql += " and a.ac_ccy = ? ";
            }
            if (!"1".equals(cbsrekoto)) {
                sql += " and a.ac_no not in (?) order by a.AC_ENTRY_SR_NO, a.trn_dt ";
            }
            sql += ") where inc >= ? and inc <= ? ";
            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, tgl1.trim());
            stat.setString(i++, tgl2.trim());
            stat.setString(i++, ac_no.trim());
            stat.setString(i++, brnch.trim());
            if ("485052001".equals(ac_no) || "585054001".equals(ac_no)) {
            } else {
                stat.setString(i++, ccy.trim());
            }
            if (!"1".equals(cbsrekoto)) {
                stat.setString(i++, "195001006");
            }
            stat.setInt(i++, begin);
            stat.setInt(i++, delta);
            rs = stat.executeQuery();
            totMutCb = new BigDecimal(0);
            totMutDb = new BigDecimal(0);
            BigDecimal mutDb = new BigDecimal(0);
            BigDecimal mutCb = new BigDecimal(0);
            BigDecimal mutDbEq = new BigDecimal(0);
            BigDecimal mutCbEq = new BigDecimal(0);
            while (rs.next()) {
                bb = new Bukubesar();
                bb.setTrn_ref_no(rs.getString("trn_ref_no"));
                bb.setTrn_dt(rs.getDate("trn_dt"));
                bb.setValue_dt(rs.getDate("value_dt"));
                bb.setDrcr(rs.getString("drcr_ind"));
                if (rs.getString("batch_no") != null) {
                    bb.setBatch_no(rs.getString("batch_no"));
                } else {
                    bb.setBatch_no("");
                }
                bb.setAc_no(rs.getString("ac_no"));
                bb.setMaker_id(rs.getString("user_id"));
                bb.setAuth_id(rs.getString("auth_id"));
                bb.setEvent(rs.getString("event"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setKet(rs.getString("CF_ADDLTEXT"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setAc_ccy(rs.getString("ac_ccy"));
                bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
                bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
                if ("D".equals(bb.getDrcr().trim())) {
                    if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
                        mutDb = bb.getLcy_amount();
                    } else {
                        mutDb = bb.getFcy_amount();
                        mutDbEq = bb.getLcy_amount();
                    }
                } else {
                    mutDb = new BigDecimal(0);
                    mutDbEq = new BigDecimal(0);
                }
                totMutDb = totMutDb.subtract(mutDb);
                bb.setAmt_db_eq(mutDbEq);
                bb.setAmt_db(mutDb);
                if ("C".equals(bb.getDrcr().trim())) {
                    if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
                        mutCb = bb.getLcy_amount();
                    } else {
                        mutCb = bb.getFcy_amount();
                        mutCbEq = bb.getLcy_amount();
                    }
                } else {
                    mutCb = new BigDecimal(0);
                    mutCbEq = new BigDecimal(0);
                }

                bb.setAmt_cr_eq(mutCbEq);
                bb.setAmt_cr(mutCb);
                totMutCb = totMutCb.add(mutCb);
                if ("C".equals(bb.getDrcr().trim())) {
                    saldo = saldo.add(bb.getAmt_cr());
                } else if ("D".equals(bb.getDrcr().trim())) {
                    saldo = saldo.subtract(bb.getAmt_db());
                }
                bb.setSaldo(saldo);
                bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListBukubesar : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public Bukubesar getnoOfRecords(String tgl1, String tgl2, String ac_no,
            String brnch, String ccy, String cbsrekoto) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        Bukubesar bb = null;
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            // sql = "select count(*) as totrownum " +
            // "from ACVWS_ALL_AC_ENTRIES_HIST a " +
            // "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
            // +
            // "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? and a.ac_no not in (?)";
            sql = "select count(*) as totrow, sum(debet) as d, sum(kredit) as k from ( " + "select decode(drcr_ind, 'D', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) debet," + " decode(drcr_ind, 'C', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) kredit " + "from ACVWS_ALL_AC_ENTRIES_HIST " + "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " + "ac_no = ? and ac_branch = ? ";
            if ("485052001".equals(ac_no) || "585054001".equals(ac_no)) {
            } else {
                sql += " and ac_ccy = ? ";
            }
            if (!"1".equals(cbsrekoto)) {
                sql += " and ac_no not in (?) ";
            }
            sql += " ) ";
            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, tgl1.trim());
            stat.setString(i++, tgl2.trim());
            stat.setString(i++, ac_no.trim());
            stat.setString(i++, brnch.trim());
            if ("485052001".equals(ac_no) || "585054001".equals(ac_no)) {
            } else {
                stat.setString(i++, ccy.trim());
            }
            if (!"1".equals(cbsrekoto)) {
                stat.setString(i++, "195001006");
            }
            rs = stat.executeQuery();
            if (rs.next()) {
                bb = new Bukubesar();
                bb.setTotrow(rs.getInt("totrow"));
                BigDecimal x = (rs.getBigDecimal("d") == null) ? new BigDecimal(0) : rs.getBigDecimal("d");
                bb.setTotdebet(x.multiply(new BigDecimal(-1)));
                bb.setTotkredit(rs.getBigDecimal("k"));
            }
        } catch (SQLException ex) {
            log.error(" getnoOfRecords : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return bb;
    }

    public int getnoOfRecords2(String userId, String branch, String tgl1,
            String tgl2, String mod) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        int totrow = 0;
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select count(*) as total from acvw_all_ac_entries " + "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and " + "trn_dt <= TO_DATE(?, 'DD-MM-YYYY')  AND auth_stat=? ";
            if (!"".equals(userId) && userId != null) {
                sql += " and user_id = ? ";
            }
            if (!"".equals(branch) && branch != null) {
                sql += " and ac_branch = ? ";
            }
            if (!"".equals(mod) && mod != null) {
                sql += " and module = ? ";
            }

            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, tgl1);
            stat.setString(i++, tgl2);
            stat.setString(i++, "A");
            if (!"".equals(userId) && userId != null) {
                stat.setString(i++, userId);
            }
            if (!"".equals(branch) && branch != null) {
                stat.setString(i++, branch);
            }
            if (!"".equals(mod) && mod != null) {
                stat.setString(i++, mod);
            }

            rs = stat.executeQuery();
            if (rs.next()) {
                totrow = rs.getInt("total");
            }
        } catch (SQLException ex) {
            log.error(" getnoOfRecords2 : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return totrow;
    }

    public List getListPCdIB(String type, String dt) {
        List lbb = new ArrayList();
        Pcode bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getIbDS().getConnection();
            sql = "select distinct status, count(*) as total, sum(amount) as nominal from " + "(SELECT AMOUNT, (case when status = '11' then 'Success' else 'Failed' end) STATUS " + "FROM IB_TRANSFERS_DOM where TRANSFER_TYPE= ? and trunc(DATE_TRX) = TO_DATE(?, 'dd-MM-yyyy')) " + "group by status";
            stat = conn.prepareStatement(sql);
            stat.setString(1, type);
            stat.setString(2, dt);
            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new Pcode();
                bb.setPcd("-");
                bb.setSts(rs.getString("status"));
                bb.setTot(rs.getInt("total"));
                bb.setNom(rs.getBigDecimal("nominal"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListPCdIB : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListbatchht(String userId, String branch, String tgl1,
            String tgl2, String mod, int begin, int delta) {
        List lbb = new ArrayList();
        BatchMaster bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select inc, trn_dt,RELATED_ACCOUNT, trn_ref_no,ac_branch,ac_no,lcy_amount,ac_ccy,fcy_amount," + "user_id,auth_id,trn_code,drcr_ind,module,auth_stat from " + "(select rownum as inc, trn_dt,RELATED_ACCOUNT, trn_ref_no,ac_branch,ac_no,lcy_amount,ac_ccy,fcy_amount," + "user_id,auth_id,trn_code,drcr_ind,module,auth_stat  from acvw_all_ac_entries " + "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and " + "trn_dt <= TO_DATE(?, 'DD-MM-YYYY')  AND auth_stat=?   and rownum <= ? ";

            if (!"".equals(userId) && userId != null) {
                sql += " and user_id = ? ";
            }
            if (!"".equals(branch) && branch != null) {
                sql += " and ac_branch = ? ";
            }
            if (!"".equals(mod) && mod != null) {
                sql += " and module = ? ";
            }

            sql += ") where inc >= ? and inc <= ? ";

            int i = 1;
            stat = conn.prepareStatement(sql);
            stat.setString(i++, tgl1);
            stat.setString(i++, tgl2);
            stat.setString(i++, "A");
            stat.setInt(i++, delta);

            if (!"".equals(userId) && userId != null) {
                stat.setString(i++, userId);
            }
            if (!"".equals(branch) && branch != null) {
                stat.setString(i++, branch);
            }
            if (!"".equals(mod) && mod != null) {
                stat.setString(i++, mod);
            }

            stat.setInt(i++, begin);
            stat.setInt(i++, delta);

            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new BatchMaster();
                bb.setTrn_dt(rs.getDate("trn_dt"));
                bb.setTrn_ref_no(rs.getString("trn_ref_no"));
                bb.setBranch_code(rs.getString("ac_branch"));
                bb.setAc_no(rs.getString("ac_no"));
                bb.setCr_ent_total(rs.getBigDecimal("lcy_amount"));
                bb.setAc_ccy(rs.getString("ac_ccy"));
                bb.setModule(rs.getString("module"));
                bb.setUser_id(rs.getString("user_id"));
                bb.setAuth_id(rs.getString("auth_id"));
                bb.setAuth_stat(rs.getString("auth_stat"));
                bb.setTrn_code(rs.getString("trn_code"));
                bb.setDrcr_ind(rs.getString("drcr_ind"));
                bb.setRELATED_ACCOUNT(rs.getString("RELATED_ACCOUNT"));
                lbb.add(bb);

            }

        } catch (SQLException ex) {
            log.error(" getListbatch  : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListPCdCMS(String type, String dt) {
        List lbb = new ArrayList();
        Pcode bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getCmsDS().getConnection();
            sql = "select distinct status, count(*) as total, sum(amount) as nominal from " + "(SELECT AMOUNT, decode(status, '3', 'Success', 'Fail') status " + "FROM CB_TRANSFERS_DOM where transfer_type= ? and  trunc(DATE_TRX) = TO_DATE(?, 'dd-MM-yyyy')) " + "group by status";
            stat = conn.prepareStatement(sql);
            stat.setString(1, type);
            stat.setString(2, dt);
            rs = stat.executeQuery();
            while (rs.next()) {
                bb = new Pcode();
                bb.setPcd("-");
                bb.setSts(rs.getString("status"));
                bb.setTot(rs.getInt("total"));
                bb.setNom(rs.getBigDecimal("nominal"));
                lbb.add(bb);
            }
        } catch (SQLException ex) {
            log.error(" getListPCdCMS : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public List getListCustom(String norek, String nama, String ktp, String lhr, int begin) {
        List lbb = new ArrayList();
        SttmCustAccount bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        int total = 0;


//		System.out.println( "lhr : " + lhr);

        try {
            conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
            sql = "select cust_ac_no, ac_desc, cust_no, ccy, account_class, UNIQUE_ID_VALUE, DATE_OF_BIRTH from  " +
                    "(SELECT cust_ac_no, ac_desc, cust_no, ccy, account_class, UNIQUE_ID_VALUE, DATE_OF_BIRTH  " +
                    "FROM cust_account_master where ";
//			if((!"".equals(norek) && norek != null) && (!"".equals(nama) && nama != null)
//					&& (!"".equals(ktp) && ktp != null) && (!"".equals(lhr) && lhr != null)) {
//				sql += " ac_desc like ? and cust_ac_no = ? and UNIQUE_ID_VALUE = ? and DATE_OF_BIRTH =  '"+lhr+"' and ";
//			}
            if (!"".equals(nama) && nama != null) {
                sql += " ac_desc like ? and ";
            }
            if (!"".equals(norek) && norek != null) {
                sql += " cust_ac_no = ? and ";
            }
            if (!"".equals(ktp) && ktp != null) {
                sql += " UNIQUE_ID_VALUE = ? and ";
            }
            if (!"".equals(lhr) && lhr != null) {
                sql += "DATE_OF_BIRTH =  '" + lhr + "' and ";
            }

            total = sql.length();
            sql = sql.substring(0, total - 4);

            sql += " order by cust_ac_no LIMIT 30 OFFSET ?) AS HASIL";

            int i = 1;
            stat = conn.prepareStatement(sql);
//			if((!"".equals(norek) && norek != null) && (!"".equals(nama) && nama != null) &&
//					(!"".equals(ktp) && ktp != null) && (!"".equals(lhr) && lhr != null) ) {
//				stat.setString(i++, "%" + nama + "%".trim());
//				stat.setString(i++, norek.trim());
//				stat.setString(i++, ktp.trim());

//			}
            if (!"".equals(nama) && nama != null) {
                stat.setString(i++, "%" + nama + "%".trim());
            }
            if (!"".equals(norek) && norek != null) {
                stat.setString(i++, norek.trim());
            }
            if (!"".equals(ktp) && ktp != null) {
                stat.setString(i++, ktp.trim());
            }
//			else if (!"".equals(lhr) && lhr != null){
//				stat.setString(i++, lhr);
//				System.out.println( "lhr : " + lhr);
//			}
//
            stat.setInt(i++, begin);

//			System.out.println( "sql : " + sql);

            rs = stat.executeQuery();



            while (rs.next()) {
                bb = new SttmCustAccount();
                bb.setCust_ac_no(rs.getString("cust_ac_no"));
                bb.setAc_desc(rs.getString("ac_desc"));
                bb.setCust_no(rs.getString("cust_no"));
                bb.setCcy(rs.getString("ccy"));
                bb.setAccount_class(rs.getString("account_class"));
                bb.setUNIQUE_ID_VALUE(rs.getString("UNIQUE_ID_VALUE"));
                bb.setDATE_OF_BIRTH(rs.getString("DATE_OF_BIRTH"));
                lbb.add(bb);


            }
        } catch (SQLException ex) {
            log.error(" getListCustom : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

    public int getTotCust(String norek, String nama, String ktp, String lhr) {
        int tot = 0;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        int total = 0;




//		System.out.println( "lhr : " + lhr);

        try {
            conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
            sql = "SELECT count(*) as total FROM cust_account_master where ";
//			if((!"".equals(norek) && norek != null) && (!"".equals(nama) && nama != null)
//					 && (!"".equals(ktp) && ktp != null) && (!"".equals(lhr) && lhr != null)) {
//				sql += " ac_desc like ? and cust_ac_no = ? and UNIQUE_ID_VALUE = ? and DATE_OF_BIRTH='"+lhr+"' and ";
//			}
            if (!"".equals(nama) && nama != null) {
                sql += " ac_desc like ? and ";
            }
            if (!"".equals(norek) && norek != null) {
                sql += " cust_ac_no = ? and ";
            }
            if (!"".equals(ktp) && ktp != null) {
                sql += "  UNIQUE_ID_VALUE= ? and ";
            }
            if (!"".equals(lhr) && lhr != null) {
                sql += " DATE_OF_BIRTH = '" + lhr + "' and ";
            }

            total = sql.length();
            sql = sql.substring(0, total - 4);


//			sql += " AND account_class not in ('S18A','S18B')";
            int i = 1;
            stat = conn.prepareStatement(sql);
//			if((!"".equals(norek) && norek != null) && (!"".equals(nama) && nama != null) &&
//					(!"".equals(ktp) && ktp != null) && (!"".equals(lhr) && lhr != null) ) {
//				stat.setString(i++, "%" + nama + "%".trim());
//				stat.setString(i++, norek.trim());
//				stat.setString(i++, ktp.trim());
////				stat.setString(i++, lhr);

//			}

            if (!"".equals(nama) && nama != null) {
                stat.setString(i++, "%" + nama + "%".trim());
            }
            if (!"".equals(norek) && norek != null) {
                stat.setString(i++, norek.trim());
            }
            if (!"".equals(ktp) && ktp != null) {
                stat.setString(i++, ktp.trim());
            }
//			else if (!"".equals(lhr) && lhr != null){
//				stat.setString(i++, lhr);
//			}

            rs = stat.executeQuery();

            if (rs.next()) {
                tot = rs.getInt("total");
            }
        } catch (SQLException ex) {
//			System.out.println(" getListCustom : " + ex.getMessage());
            log.error(" getListCustom : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return tot;
    }

    public Saldo getSaldoRek(String acc, String dt1) {
        Saldo sld = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = " select ac_no as nomor_rekening,decode(ac_ccy,'IDR', saldo_awal1,saldo_awal2) as saldo_awal, saldo_awal1 as ekivalen " +
                    "from (select ac_no,ac_ccy,sum(decode (drcr_ind, 'C', lcy_amount, -lcy_amount)) saldo_awal1, " +
                    "sum(decode (drcr_ind, 'C', fcy_amount, -fcy_amount)) as saldo_awal2 " +
                    "from ACVWS_ALL_AC_ENTRIES_HIST where ac_no = ? and trn_dt < to_date (?, 'DD-MM-YY') " +
                    "group by ac_no,ac_ccy) ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, acc.trim());
            stat.setString(2, dt1.trim());
            rs = stat.executeQuery();
            if (rs.next()) {
                sld = new Saldo();
                sld.setAccount(rs.getString("nomor_rekening"));
                BigDecimal t = new BigDecimal(0);
                if (rs.getBigDecimal("saldo_awal") != null) {
                    t = rs.getBigDecimal("saldo_awal");
                }
                BigDecimal e = new BigDecimal(0);
                if (rs.getBigDecimal("ekivalen") != null) {
                    e = rs.getBigDecimal("ekivalen");
                }
                sld.setLcy_closing_bal(t);
                sld.setEkivalen(e);
            }
            if (sld == null) {
                sld = new Saldo();
                sld.setAccount(acc);
                sld.setLcy_closing_bal(new BigDecimal(0));
                sld.setEkivalen(new BigDecimal(0));
            }
        } catch (SQLException ex) {
            log.error(" getSaldoRek : " + ex.getMessage());

        } finally {
            closeConnDb(conn, stat, rs);
        }
        return sld;
    }

    public BigDecimal getPlafondPRK3String(Customer cust) {
        BigDecimal sld = new BigDecimal(0);
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = " select b.cust_ac_no, a.limit_amount total_plafond,  (a.LINE_CODE || a.LINE_SERIAL) as liab, c.linked_ref_no " +
                    "from GETM_FACILITY a, STTMS_CUST_ACCOUNT b, STTM_CUST_ACCOUNT_LINKAGES c " +
                    "where a.LINE_CODE || a.LINE_SERIAL = c.linked_ref_no and b.cust_ac_no = c.cust_ac_no and " +
                    "b.cust_no = c.customer_no and b.cust_no = a.LINE_CODE and " +
                    "b.account_class = ? and b.cust_ac_no = ? and b.ccy=? and b.branch_code=? ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, cust.getCust_acc_clas());
            stat.setString(2, cust.getCust_acc());
            stat.setString(3, cust.getCust_ccy());
            stat.setString(4, cust.getBranch_cd());
            rs = stat.executeQuery();
            if (rs.next()) {
                sld = rs.getBigDecimal("total_plafond");
            }
        } catch (SQLException ex) {
            log.error(" getPlafondPRK3String : " + ex.getMessage());

        } finally {
            closeConnDb(conn, stat, rs);
        }
        return sld;
    }

    public List getListHistTrans(String norek, String dt1, String dt2, BigDecimal sldawal, String tipe) {
        List lbb = new ArrayList();
        HistTransRekVa ht = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        BigDecimal akumulasi = new BigDecimal(0);
        try {
            jmlItmD = 0;
            jmlItmC = 0;
            totD = new BigDecimal(0);
            totC = new BigDecimal(0);
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();


            

//            sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, TO_CHAR(a.value_dt, 'DD-MM-YY') as value_dt, a.trn_code, a.event, " +
//                    "((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
//                    "(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
//                    "(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
//                    "|| ' ' || " +
//                    /*"decode(s.trn_desc,'SI', (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
//                    "AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)), " +
//                    "decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'')) " +*/
//                    "(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
//                    "AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
//                    "decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
//                    "|| ' ' || " +
//                    "(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
//                    "'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
//                    "from cstb_contract s, " +
//                    "(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
//                    "from LCTBS_CONTRACT_MASTER a, " +
//                    "(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
//                    "(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
//                    "where a.contract_ref_no = b.contract_ref_no " +
//                    "and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
//                    "and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
//                    "(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
//                    "from BCTB_CONTRACT_MASTER d, " +
//                    "(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
//                    "(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
//                    "where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
//                    "and d.event_seq_no = f.event_seq_no " +
//                    "and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
//                    "where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
//                    "and s.contract_ref_no = a.trn_ref_no " +
//                    "group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
//                    "a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat " +
//                    "from ACVWS_ALL_AC_ENTRIES_HIST a , sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
//                    "where a.ac_no = ? " +
//                    "and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
//                    "and  a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
//                    "and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
//                    "order by a.AC_ENTRY_SR_NO, a.trn_dt";

            sql = " SELECT a.ac_no,"
+" a.trn_ref_no,"
+" TO_CHAR (a.trn_dt, 'DD-MM-YY') AS trn_dt,"
+" TO_CHAR (a.value_dt, 'DD-MM-YY') AS value_dt,"
+" a.trn_code,"
+" ( (SELECT TRIM (ADDL_TEXT)"
+" FROM CSTBS_ADDL_TEXT"
+" WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no)"
+" || ' '"
+" || (SELECT narrative"
+" FROM DETB_RTL_TELLER"
+" WHERE TRN_REF_NO = a.trn_ref_no)"
+" || ' '"
+" || (SELECT DISTINCT ADDL_TEXT"
+" FROM DETB_JRNL_TXN_DETAIL"
+" WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no)"
+" || ' '"
+" || s.trn_desc"
+" || ' ' || "
+" (CASE"
+" WHEN s.trn_desc LIKE 'SI %'"
+" THEN"
+" (SELECT internal_remarks"
+" FROM SITB_CONTRACT_MASTER"
+" WHERE contract_ref_no = a.trn_ref_no"
+" AND version_no ="
+" (SELECT MAX (version_no)"
+" FROM sitb_contract_master"
+" WHERE contract_ref_no = a.TRN_REF_NO))"
+" ELSE"
+" DECODE (a.related_account,"
+" '', '',"
+" ' For the Account No.' || a.related_account)"
+" || DECODE (a.module, 'CI', ' a.n ' || y.customer_name1, '')"
+" END)"
+" || ' '"
+" || (  SELECT s.user_ref_no || ' '"
+" || DECODE ("
+" s.module_code,"
+" 'IB',    'Mtr_dt: '"
+" || TO_CHAR (u.maturity_date, 'YYYY-MM-DD')"
+" || ' '"
+" || u.ket2,"
+" 'Exp_dt: '"
+" || TO_CHAR (t.expiry_date, 'YYYY-MM-DD')"
+" || ' '"
+" || t.ket1)"
+" FROM cstb_contract s,"
+" (SELECT a.contract_ref_no,"
+" a.expiry_date,"
+" (   'APP: '"
+" || b.cust_name"
+" || ' & '"
+" || 'BEN: '"
+" || c.cust_name)"
+" ket1"
+" FROM LCTBS_CONTRACT_MASTER a,"
+" (SELECT contract_ref_no, event_seq_no, cust_name"
+" FROM LCTB_PARTIES"
+" WHERE party_type = 'APP') b,"
+" (SELECT contract_ref_no, event_seq_no, cust_name"
+" FROM LCTB_PARTIES"
+" WHERE party_type = 'BEN') c"
+" WHERE     a.contract_ref_no = b.contract_ref_no"
+" AND a.contract_ref_no = c.contract_ref_no"
+" AND a.event_seq_no = b.event_seq_no"
+" AND a.event_seq_no = c.event_seq_no"
+" AND a.version_no ="
+" (SELECT MAX (version_no)"
+"    FROM LCTBS_CONTRACT_MASTER"
+"   WHERE contract_ref_no = a.contract_ref_no)) t,"
+" (SELECT d.bcrefno,"
+" d.maturity_date,"
+" (   'DRAWEE: '"
+" || e.party_name"
+" || ' & '"
+" || 'DRAWER: '"
+" || f.party_name)"
+" ket2"
+" FROM BCTB_CONTRACT_MASTER d,"
+" (SELECT bcrefno, event_seq_no, party_name"
+" FROM BCTB_CONTRACT_PARTIES"
+" WHERE party_type = 'DRAWEE') e,"
+" (SELECT bcrefno, event_seq_no, party_name"
+" FROM BCTB_CONTRACT_PARTIES"
+" WHERE party_type = 'DRAWER') f"
+" WHERE     d.bcrefno = e.bcrefno"
+" AND d.bcrefno = f.bcrefno"
+" AND d.event_seq_no = e.event_seq_no"
+" AND d.event_seq_no = f.event_seq_no"
+" AND d.version_no = (SELECT MAX (version_no)"
+"                FROM BCTB_CONTRACT_MASTER"
+"               WHERE bcrefno = d.bcrefno)) u"
+" WHERE     s.contract_ref_no = t.contract_ref_no(+)"
+" AND s.contract_ref_no = u.bcrefno(+)"
+" AND s.contract_ref_no = a.trn_ref_no"
+" GROUP BY s.user_ref_no,"
+" s.module_code,"
+" t.expiry_date,"
+" u.maturity_date,"
+" t.ket1,"
+" u.ket2))"
+" keterangan,"
+" a.drcr_ind,"
+" DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,"
+" DECODE (z.ac_ccy, 'IDR', z.lcy_amount, z.fcy_amount) AS fee,"
+" DECODE (a.ac_ccy, 'IDR', a.lcy_amount+z.lcy_amount, a.fcy_amount+z.fcy_amount) AS total, "
+" a.auth_stat"
+" FROM ACVWS_ALL_AC_ENTRIES_HIST a,"
+" sttms_trn_code s,"
+" cltb_account_apps_master x,"
+" sttm_customer y,"
+" (select trn_ref_no, lcy_amount, fcy_amount, ac_ccy, trn_dt, event from ACVWS_ALL_AC_ENTRIES where ac_no='485007050' and trn_code='HVA'"
+" AND trn_dt >= TO_DATE ('" + dt1 + "',    'DD-MM-YYYY')"
+" AND trn_dt <= TO_DATE ('" + dt2 + "' , 'DD-MM-YYYY')) z "
+" WHERE a.ac_no =  '" + norek + "'"
+" and a.trn_dt >= TO_DATE ('" + dt1 + "',    'DD-MM-YYYY') "
+" and a.trn_dt <= TO_DATE ('" + dt2 + "' , 'DD-MM-YYYY') "
+" and a.trn_code = s.trn_code and a.related_account = x.account_number(+)"
+" and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+)"
+" AND A.AC_NO NOT IN ('3170003148')"
+" AND a.trn_ref_no NOT IN ('7018547131610003','701IJ70131850001')"
+" AND TO_CHAR(a.trn_dt,'DD-MM-YYYY') <> '10-06-2013'"
+" AND TO_CHAR(a.trn_dt,'DD-MM-YYYY') <> '04-07-2013'"
+" and a.trn_ref_no = z.trn_ref_no and a.event=z.event "
+" order by a.AC_ENTRY_SR_NO, a.trn_dt";

            System.out.println(sql);
            stat = conn.prepareStatement(sql);
//            stat.setString(3, norek);
//            stat.setString(1, dt1);
//            stat.setString(2, dt2);
//            stat.setString(4, dt1);
//            stat.setString(5, dt2);
            rs = stat.executeQuery();
            BigDecimal tempSld = sldawal;
            while (rs.next()) {
                ht = new HistTransRekVa();
                if ("3170003148".equals(norek.trim()) &&
                        (("7018547131610003".equals(rs.getString("trn_ref_no")) && "2013-06-10".equals(rs.getString("trn_dt").substring(0, 10))) ||
                        ("701IJ70131850001".equals(rs.getString("trn_ref_no")) && "2013-07-04".equals(rs.getString("trn_dt").substring(0, 10))))) {
                    continue;
                }
                if ("ZZX".equals(rs.getString("trn_code").toUpperCase().trim()) && "CONVERSION - :,".equals(rs.getString("keterangan").toUpperCase().trim())) {
                    continue;
                }
                ht.setAccount(rs.getString("ac_no"));
                ht.setTrn_ref_no(rs.getString("trn_ref_no"));
                ht.setTrn_dt(rs.getString("trn_dt"));
                ht.setInstrument_code("");
                ht.setVal_dt(rs.getString("value_dt"));
                ht.setTrn_code(rs.getString("trn_code"));
                ht.setDesc(rs.getString("keterangan"));
                ht.setDrcr(rs.getString("drcr_ind"));
                ht.setNominal(rs.getBigDecimal("total") == null ? new BigDecimal(0) : rs.getBigDecimal("total"));

                BigDecimal t = new BigDecimal(0);
                if (rs.getBigDecimal("total") != null) {
                    t = rs.getBigDecimal("total");
                }
                if ("D".equals(rs.getString("drcr_ind"))) {
                    tempSld = tempSld.subtract(t);
                    akumulasi = akumulasi.subtract(t);
                    jmlItmD++;
                    totD = totD.add(ht.getNominal());
                } else if ("C".equals(rs.getString("drcr_ind"))) {
                    tempSld = tempSld.add(t);
                    akumulasi = akumulasi.add(t);
                    jmlItmC++;
                    totC = totC.add(ht.getNominal());
                }
                ht.setAkumulasi(convertAmountString(akumulasi));
                ht.setSaldo(tempSld);
                sld_akhir = ht.getSaldo();
//                if (rs.getBigDecimal("ekivalen") != null) {
//                    ht.setEkivalen(rs.getBigDecimal("ekivalen"));
//                } else {
                    ht.setEkivalen(new BigDecimal(0));
//                }
                ht.setAuth_stat(rs.getString("auth_stat"));
                lbb.add(ht);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" getListHistTrans : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return lbb;
    }

//    public List getListHistTrans(String norek, String dt1, String dt2, BigDecimal sldawal, String tipe) {
////        com.muamalat.service.DanaOptimListHistTrans result = null;
////        try { // Call Web Service Operation
////            System.out.println("Call Web Service Operation");
////            com.muamalat.service.DanaOptimDataService service = new com.muamalat.service.DanaOptimDataService();
////            com.muamalat.service.DanaOptimData port = service.getDanaOptimDataPort();
////            // TODO initialize WS operation arguments here
////            // TODO process result here
////            result = port.getListHistTrans(norek, dt1, dt2, sldawal, tipe);
////            sld_akhir = result.getSldAkhir();
////            jmlItmC = result.getJmlItmC();
////            jmlItmD = result.getJmlItmD();
////            totD = result.getTotD();
////            totC = result.getTotC();
////            return result.getListTrans();
////        } catch (Exception ex) {
////            // TODO handle custom exceptions here
////            ex.printStackTrace();
////        }
//
//        DanaOptimDataProxy proxy = new DanaOptimDataProxy();
//        List<HistTransRek> lbb = new ArrayList();
//        try {
//            DanaOptimListHistTrans result = proxy.getListHistTrans(norek, dt1, dt2, sldawal, tipe);
//            for (int i = 0;i<result.getListTrans().length;i++){
//                lbb.add(result.getListTrans(i));
//            }
//
//            sld_akhir = result.getSld_akhir();
//            jmlItmC = result.getJmlItmC();
//            jmlItmD = result.getJmlItmD();
//            totD = result.getTotD();
//            totC = result.getTotC();
//        } catch (RemoteException ex) {
//            ex.printStackTrace();
//        }
//
//        return lbb;
//    }

    public SttmCustAccount getDetailCustom(String norek, String username) {
        SttmCustAccount bb = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        String sql2 = "";
        String sql3 = "";
        String sql4 = "";
        String sql5 = "";

        try {
//            CetakPassbookFunction cpf = new CetakPassbookFunction();
            String rr = "";
            SMTB_USER_ROLE user_r = null;
//            if ("".equals(username)) {
//                user_r = cpf.getUserHCO(username, "HCO");
//            }

            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select a.BRANCH_CODE,d.branch_name,f.nationality, a.CUST_AC_NO,a.AC_DESC,CUST_NO,a.CCY,a.ACCOUNT_CLASS,a.acy_curr_balance, " +
                    "a.acy_blocked_amount, a.acy_avl_bal, c.min_balance,b.acy_bal, f.CUSTOMER_TYPE, f.Address_line1,f.Address_line2,f.Address_line3," +
                    "a.ADDRESS1,a.ADDRESS2,a.ADDRESS3, e.DESCRIPTION, a.ac_open_date,a.alt_ac_no,a.record_stat,a.CHECKER_DT_STAMP " +
                    "FROM Sttm_Cust_Account a, STTM_ACCLS_CCY_BALANCES c, GLTB_AVGBAL b,sttm_branch d, sttm_account_class e, sttm_Customer f " +
                    "WHERE a.CUST_aC_NO= ? and a.account_class = c.account_class and f.customer_no = a.cust_no and a.CUST_aC_NO=b.gl_code(+) " +
                    "and a.BRANCH_CODE=d.BRANCH_CODE and a.account_class=e.account_class";
            stat = conn.prepareStatement(sql);
            stat.setString(1, norek);
            rs = stat.executeQuery();
            String nocif = "";
            if (rs.next()) {
                bb = new SttmCustAccount();
                bb.setBranch_code(rs.getString("branch_code"));
                bb.setBranch_name(rs.getString("branch_name"));
                bb.setNationality(rs.getString("nationality"));
                bb.setCUST_AC_NO(rs.getString("CUST_AC_NO"));
                bb.setAcDesc(rs.getString("AC_DESC"));
                bb.setCustNo(rs.getString("CUST_NO"));
                bb.setCcy(rs.getString("Ccy"));
                bb.setCustomer_type(rs.getString("CUSTOMER_TYPE"));
                bb.setAddress1(rs.getString("ADDRESS1"));
                if (rs.getString("ADDRESS1") == null) {
                    bb.setAddress1(rs.getString("Address_line1"));
                    bb.setAddress2(rs.getString("Address_line2"));
                    bb.setAddress3(rs.getString("Address_line3"));
                } else {
                    bb.setAddress1(rs.getString("ADDRESS1"));
                    bb.setAddress2(rs.getString("ADDRESS2"));
                    bb.setAddress3(rs.getString("ADDRESS3"));
                }

                bb.setDesc_acc(rs.getString("DESCRIPTION").toUpperCase().trim());
                bb.setAC_OPEN_DATE(rs.getString("AC_OPEN_DATE").substring(0, 10));
                bb.setRecord_stat(rs.getString("record_stat"));
                if (rs.getString("record_stat").equals("C")) {
                    bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP").substring(0, 10));
                } else {
                    bb.setAC_CLOSE_DATE(new String("-"));
                }
                bb.setAlt_ac_no(rs.getString("alt_ac_no"));

                bb.setAccountClass(rs.getString("ACCOUNT_CLASS"));

                if (user_r != null) {
                    rr = "";
                } else {
                    if ("S18A".equals(rs.getString("ACCOUNT_CLASS")) || "S18B".equals(rs.getString("ACCOUNT_CLASS"))) {
                        rr = "1";
                    }
                }

//				if(rs.getString("ACCOUNT_CLASS").equals("S18A")||(rs.getString("ACCOUNT_CLASS").equals ("S18B"))){
                if ("1".equals(rr)) {
                    bb.setAcyCurrBalance(new BigDecimal(999999999));
                    bb.setAcyBlockedAmount(new BigDecimal(999999999));
                    bb.setAcyAvlBal(new BigDecimal(999999999));
                    bb.setMinBalance(new BigDecimal(999999999));
                    bb.setAcy_bal(new BigDecimal(999999999));
                } else {
                    bb.setAcyCurrBalance(rs.getBigDecimal("acy_curr_balance"));
                    bb.setAcyBlockedAmount(rs.getBigDecimal("acy_blocked_amount"));
                    bb.setAcyAvlBal(rs.getBigDecimal("acy_avl_bal"));
                    bb.setMinBalance(rs.getBigDecimal("min_balance"));
                    bb.setAcy_bal(rs.getBigDecimal("acy_bal"));
                }
                nocif = rs.getString("CUST_NO");
            }

            if (bb == null) {
                sql5 = "select a.BRANCH_CODE, d.branch_name,a.CUST_AC_NO,a.AC_DESC,CUST_NO,a.CCY,a.ACCOUNT_CLASS,a.acy_curr_balance, " +
                        "a.acy_blocked_amount, a.acy_avl_bal, c.min_balance, " +
                        "a.ADDRESS1,a.ADDRESS2,a.ADDRESS3, e.DESCRIPTION, a.ac_open_date,a.alt_ac_no,a.record_stat,a.CHECKER_DT_STAMP " +
                        "FROM Sttm_Cust_Account a, STTM_ACCLS_CCY_BALANCES c, sttm_branch d, sttm_account_class e " +
                        "WHERE a.CUST_aC_NO=? and a.BRANCH_CODE=d.BRANCH_CODE and a.account_class=e.account_class ";
                stat = conn.prepareStatement(sql5);
                stat.setString(1, norek);
                rs = stat.executeQuery();
                if (rs.next()) {
                    bb = new SttmCustAccount();
                    bb.setBranch_code(rs.getString("branch_code"));
                    bb.setBranch_name(rs.getString("branch_name"));
                    bb.setCUST_AC_NO(rs.getString("CUST_AC_NO"));
                    bb.setAcDesc(rs.getString("AC_DESC"));
                    bb.setCustNo(rs.getString("CUST_NO"));
                    bb.setCcy(rs.getString("Ccy"));
                    bb.setAddress1(rs.getString("ADDRESS1"));
                    bb.setAddress2(rs.getString("ADDRESS2"));
                    bb.setAddress3(rs.getString("ADDRESS3"));
                    bb.setDesc_acc(rs.getString("DESCRIPTION").toUpperCase().trim());
                    bb.setAC_OPEN_DATE(rs.getString("AC_OPEN_DATE").substring(0, 10));
                    bb.setRecord_stat(rs.getString("record_stat"));
                    if (rs.getString("record_stat").equals("C")) {
                        bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP").substring(0, 10));
                    } else {
                        bb.setAC_CLOSE_DATE(new String("-"));
                    }
                    bb.setAlt_ac_no(rs.getString("alt_ac_no"));
                    bb.setAccountClass(rs.getString("ACCOUNT_CLASS"));

                    if (user_r != null) {
                        rr = "";
                    } else {
                        if ("S18A".equals(rs.getString("ACCOUNT_CLASS")) || "S18B".equals(rs.getString("ACCOUNT_CLASS"))) {
                            rr = "1";
                        }
                    }

//					if(rs.getString("ACCOUNT_CLASS").equals("S18A")||(rs.getString("ACCOUNT_CLASS").equals ("S18B"))){
                    if ("1".equals(rr)) {
                        bb.setAcyCurrBalance(new BigDecimal(999999999));
                        bb.setAcyBlockedAmount(new BigDecimal(999999999));
                        bb.setAcyAvlBal(new BigDecimal(999999999));
                        bb.setMinBalance(new BigDecimal(999999999));
                        bb.setAcy_bal(new BigDecimal(999999999));
                    } else {
                        bb.setAcyCurrBalance(rs.getBigDecimal("acy_curr_balance"));
                        bb.setAcyBlockedAmount(rs.getBigDecimal("acy_blocked_amount"));
                        bb.setAcyAvlBal(rs.getBigDecimal("acy_avl_bal"));
                        bb.setMinBalance(rs.getBigDecimal("min_balance"));
                        bb.setAcy_bal(rs.getBigDecimal("-"));
                    }
                }
            }
            sql2 = "select FUNCTION_ID,FIELD_VAL_1, (select lov_desc from udtm_lov where field_Name = 'AGAMA' and lov = field_val_2) field_val_2, " +
                    "FIELD_VAL_3,FIELD_VAL_4,FIELD_VAL_5,FIELD_VAL_6, FIELD_VAL_7,FIELD_VAL_8,FIELD_VAL_9, " +
                    "(select lov_desc from udtm_lov where field_Name = 'PEKERJAAN' and lov = FIELD_VAL_10) FIELD_VAL_10, " +
                    "(select lov_desc from udtm_lov where field_Name = 'BIDANG PEKERJAAN' and lov = FIELD_VAL_11) FIELD_VAL_11, " +
                    "(select lov_desc from udtm_lov where field_Name = 'STATUS PERKAWINAN' and lov = FIELD_VAL_12) FIELD_VAL_12, " +
                    "(select lov_desc from udtm_lov where field_Name = 'PENDIDIKAN' and lov = FIELD_VAL_13)FIELD_VAL_13, " +
                    "FIELD_VAL_14,FIELD_VAL_15,FIELD_VAL_16,FIELD_VAL_17,FIELD_VAL_18, " +
                    "FIELD_VAL_19,FIELD_VAL_20,FIELD_VAL_21,FIELD_VAL_22,FIELD_VAL_23,FIELD_VAL_24 " +
                    "FROM CSTM_FUNCTION_USERDEF_FIELDS " +
                    "WHERE rec_key = ? and function_id='STDCIF'";

            stat = conn.prepareStatement(sql2);
            stat.setString(1, nocif + "~");
            rs = stat.executeQuery();
            if (rs.next()) {
                bb.setFieldVal1(rs.getString("FIELD_VAL_1"));
                bb.setFieldVal2(rs.getString("FIELD_VAL_2"));
                bb.setFieldVal3(rs.getString("FIELD_VAL_3"));
                bb.setFieldVal4(rs.getString("FIELD_VAL_4"));
                bb.setFieldVal5(rs.getString("FIELD_VAL_5"));
                bb.setFieldVal6(rs.getString("FIELD_VAL_6"));
                bb.setFieldVal7(rs.getString("FIELD_VAL_7"));
                bb.setFieldVal8(rs.getString("FIELD_VAL_8"));
                bb.setFieldVal9(rs.getString("FIELD_VAL_9"));
                bb.setFieldVal10(rs.getString("FIELD_VAL_10"));
                bb.setFieldVal11(rs.getString("FIELD_VAL_11"));
                bb.setFieldVal12(rs.getString("FIELD_VAL_12"));
                bb.setFieldVal13(rs.getString("FIELD_VAL_13"));
                bb.setFieldVal14(rs.getString("FIELD_VAL_14"));
                bb.setFieldVal15(rs.getString("FIELD_VAL_15"));
                bb.setFieldVal16(rs.getString("FIELD_VAL_16"));
                bb.setFieldVal17(rs.getString("FIELD_VAL_17"));
                bb.setFieldVal18(rs.getString("FIELD_VAL_18"));
                bb.setFieldVal19(rs.getString("FIELD_VAL_19"));
                bb.setFieldVal20(rs.getString("FIELD_VAL_20"));
                bb.setFieldVal21(rs.getString("FIELD_VAL_21"));
                bb.setFieldVal22(rs.getString("FIELD_VAL_22"));
                bb.setFieldVal23(rs.getString("FIELD_VAL_23"));
                bb.setFieldVal24(rs.getString("FIELD_VAL_24"));
            }


            sql3 = "select to_CHAR(date_of_birth,'DD/MM/YYYY') AS LAHIR,UNIQUE_ID_VALUE,customer_type FROM sttm_customer WHERE customer_no = ?";
            stat = conn.prepareStatement(sql3);
            stat.setString(1, nocif);
            rs = stat.executeQuery();
            if (rs.next()) {
                if ("I".equals(rs.getString("customer_type"))) {
                    bb.setDATE_OF_BIRTH(rs.getString("LAHIR").substring(0, 10));
                    bb.setUNIQUE_ID_VALUE(rs.getString("UNIQUE_ID_VALUE"));
                } else {
                    bb.setDATE_OF_BIRTH("");
                    bb.setUNIQUE_ID_VALUE("");
                }
            }

            sql4 = "select telephone,e_mail,mobile_number,P_NATIONAL_ID, to_char(DATE_OF_BIRTH, 'dd/MM/yyyy') as tgl_lahir FROM sttm_cust_personal WHERE customer_no = ?";
            stat = conn.prepareStatement(sql4);
            stat.setString(1, nocif);
            rs = stat.executeQuery();
            if (rs.next()) {
                bb.setTelephone(rs.getString("telephone"));
                bb.setEmail(rs.getString("e_mail"));
                bb.setMobile_number(rs.getString("mobile_number"));
                bb.setP_nationalid(rs.getString("P_NATIONAL_ID"));
            }

            sql5 = "select FUNCTION_ID,FIELD_VAL_1 FROM CSTM_FUNCTION_USERDEF_FIELDS  WHERE rec_key = ? and function_id='IADCUSAC'";
            stat = conn.prepareStatement(sql5);
            stat.setString(1, nocif + "~");
            rs = stat.executeQuery();
            if (rs.next()) {
                bb.setFunction_id(rs.getString("FUNCTION_ID"));
                bb.setKode_marketing(rs.getString("FIELD_VAL_1"));
            }
            /* ============================================================ tambahan get cif ===============================================================*/
            sql5 = "select EXPIRY_DATE, NAMA_IBUKANDUNG, AGAMA,PEKERJAAN,BIDANG_PEKERJAAN,STATUS_PERKAWINAN,PENDIDIKAN, " +
                    "PNGHSLN_TTP_PRBLN,PNGHSLN_TDK_TTP_PRBULN,PENGELUARAN_TETAP_PERBULAN, RATA2_PNGHSLN_TDK_TTP_PRBLN,INFO_BGMN_MMPROLH_HSL_TMBHN " +
                    "from CSTMS_FIELDS_STDCIFI9 where customer_no = ?";
            stat = conn.prepareStatement(sql5);
            stat.setString(1, nocif);
            rs = stat.executeQuery();
            if (rs.next()) {
                bb.setTgl_kadaluarsa_id(rs.getString("EXPIRY_DATE") == null ? "" : rs.getString("EXPIRY_DATE"));
                bb.setFieldVal1(rs.getString("NAMA_IBUKANDUNG") == null ? "" : rs.getString("NAMA_IBUKANDUNG"));
                bb.setFieldVal2(rs.getString("AGAMA") == null ? "" : rs.getString("AGAMA"));
                bb.setFieldVal10(rs.getString("PEKERJAAN") == null ? "" : rs.getString("PEKERJAAN"));
                bb.setFieldVal11(rs.getString("BIDANG_PEKERJAAN") == null ? "" : rs.getString("BIDANG_PEKERJAAN"));
                bb.setSts_perkawinan(rs.getString("STATUS_PERKAWINAN") == null ? "" : rs.getString("STATUS_PERKAWINAN"));
                bb.setFieldVal13(rs.getString("PENDIDIKAN") == null ? "" : rs.getString("PENDIDIKAN"));
                bb.setPenghsl_ttp(rs.getString("PNGHSLN_TTP_PRBLN") == null ? "" : rs.getString("PNGHSLN_TTP_PRBLN"));
                bb.setPenghsl_tdk_ttp(rs.getString("PNGHSLN_TDK_TTP_PRBULN") == null ? "" : rs.getString("PNGHSLN_TDK_TTP_PRBULN"));
                bb.setPengeluaran_ttp(rs.getString("PENGELUARAN_TETAP_PERBULAN") == null ? "" : rs.getString("PENGELUARAN_TETAP_PERBULAN"));
                bb.setRtrt_penghsl_tdk_ttp(rs.getString("RATA2_PNGHSLN_TDK_TTP_PRBLN") == null ? "" : rs.getString("RATA2_PNGHSLN_TDK_TTP_PRBLN"));
                bb.setInfo_mmprolh_hsl(rs.getString("INFO_BGMN_MMPROLH_HSL_TMBHN") == null ? "" : rs.getString("INFO_BGMN_MMPROLH_HSL_TMBHN"));
            }
            sql5 = "select UNIQUE_ID_NAME, UNIQUE_ID_VALUE from STTMS_CUSTOMER where customer_no = ?";
            stat = conn.prepareStatement(sql5);
            stat.setString(1, nocif);
            rs = stat.executeQuery();
            if (rs.next()) {
                bb.setP_nationalid(rs.getString("UNIQUE_ID_NAME") == null ? "" : rs.getString("UNIQUE_ID_NAME"));
                bb.setUNIQUE_ID_VALUE(rs.getString("UNIQUE_ID_VALUE") == null ? "" : rs.getString("UNIQUE_ID_VALUE"));
            }
        } catch (SQLException ex) {
            log.error(" getDetailCustom : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return bb;

    }

    public Customer getCustomer(String acc) {
        Customer cust = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = " select b.branch_code, b.branch_name, b.branch_addr1 || '\n' || b.branch_addr2 || '\n' || b.branch_addr3 as alamat_cab, " +
//					"a.cust_ac_no, a.ac_desc, a.cust_no, a.account_class, (select description from sttm_account_class where account_class = a.account_class) as description, " +
//					"a.ccy, a.address1 || ' ' || a.address2 || ' ' || a.address3 || ' ' || a.address4 as alamat_nsbh, a.acy_uncollected, a.acy_blocked_amount, c.min_balance, a.acy_avl_bal," +
//					"(select e_mail from sttm_cust_personal where customer_no = a.cust_no) as email " +
//					"from sttm_cust_account a, sttm_branch b, STTM_ACCLS_CCY_BALANCES c " +
//					"where b.branch_code = a.branch_code and a.cust_ac_no = ? and a.account_class = c.account_class and a.ccy = c.ccy_code";
            sql = "select b.branch_code, b.branch_name, b.branch_addr1 || '\n' || b.branch_addr2 || '\n' || b.branch_addr3 as alamat_cab, " +
                    "a.cust_ac_no, a.ac_desc, a.cust_no, a.account_class, (select description from sttm_account_class where account_class = a.account_class) as description," +
                    "a.ccy, d.address_line1 || ' ' || d.address_line3 || ' ' || d.address_line2 || ' ' || d.address_line4 as alamat_nsbh," +
                    "a.acy_uncollected, a.acy_blocked_amount, c.min_balance, a.acy_avl_bal, " +
                    "(select e_mail from sttm_cust_personal where customer_no = a.cust_no) as email " +
                    "from sttm_cust_account a, sttm_branch b, STTM_ACCLS_CCY_BALANCES c, sttm_customer d " +
                    "where b.branch_code = a.branch_code and a.account_class = c.account_class and a.cust_no = d.customer_no and " +
                    "a.cust_ac_no = ? and a.ccy = c.ccy_code ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, acc.trim());
            rs = stat.executeQuery();
            if (rs.next()) {
                cust = new Customer();
                cust.setBranch_cd(rs.getString("branch_code") == null ? "" : rs.getString("branch_code"));
                cust.setBranch_name(rs.getString("branch_name") == null ? "" : rs.getString("branch_name"));
                cust.setBranch_addrs(rs.getString("alamat_cab") == null ? "" : rs.getString("alamat_cab"));
                cust.setCust_acc(rs.getString("cust_ac_no") == null ? "" : rs.getString("cust_ac_no"));
                cust.setCust_name(rs.getString("ac_desc") == null ? "" : rs.getString("ac_desc"));
                cust.setCust_no(rs.getString("cust_no") == null ? "" : rs.getString("cust_no"));
                cust.setCust_acc_clas(rs.getString("account_class") == null ? "" : rs.getString("account_class"));
                cust.setCust_acc_clas_desc(rs.getString("description") == null ? "" : rs.getString("description"));
                cust.setCust_ccy(rs.getString("ccy") == null ? "" : rs.getString("ccy"));
                cust.setCust_addrs(rs.getString("alamat_nsbh") == null ? "" : rs.getString("alamat_nsbh"));
                cust.setSld_hold(rs.getBigDecimal("acy_blocked_amount") == null ? new BigDecimal(0) : rs.getBigDecimal("acy_blocked_amount"));
                cust.setSld_float(rs.getBigDecimal("acy_uncollected") == null ? new BigDecimal(0) : rs.getBigDecimal("acy_uncollected"));
                cust.setSld_min(rs.getBigDecimal("min_balance") == null ? new BigDecimal(0) : rs.getBigDecimal("min_balance"));
                cust.setCust_email(rs.getString("email") == null ? "" : rs.getString("email"));
            }
        } catch (SQLException ex) {
            log.error("Error in getCustomer : " + ex.getMessage());
            System.out.println("Error in getCustomer : " + ex.getMessage());

        } finally {
            closeConnDb(conn, stat, rs);
        }
        return cust;
    }

    public Gltb_avgbal getGltb_avgbal(String acc) {
        Gltb_avgbal cust = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select branch_code, gl_code, fin_year, period_code, ccy, acy_bal, lcy_bal, from_date, user_id, ytodacy_bal, ytodlcy_bal " +
                    "from gltb_avgbal where gl_code = ? ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, acc.trim());
            rs = stat.executeQuery();
            if (rs.next()) {
                cust = new Gltb_avgbal();
                cust.setBranch_code(rs.getString("branch_code") == null ? "" : rs.getString("branch_code"));
                cust.setGl_code(rs.getString("gl_code") == null ? "" : rs.getString("gl_code"));
                cust.setFin_year(rs.getString("fin_year") == null ? "" : rs.getString("fin_year"));
                cust.setPeriod_code(rs.getString("period_code") == null ? "" : rs.getString("period_code"));
                cust.setAcy_bal(rs.getBigDecimal("acy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("acy_bal"));
                cust.setLcy_bal(rs.getBigDecimal("lcy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("lcy_bal"));
                cust.setFrom_date(rs.getDate("from_date"));
                cust.setUser_id(rs.getString("user_id") == null ? "" : rs.getString("user_id"));
                cust.setYtodacy_bal(rs.getBigDecimal("ytodacy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("ytodacy_bal"));
                cust.setYtodlcy_bal(rs.getBigDecimal("ytodlcy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("ytodlcy_bal"));
            }
        } catch (SQLException ex) {
            log.error("Error in getGltb_avgbal : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return cust;
    }

    private String convertAmountString(BigDecimal amount) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        String pattern = "#,##0.00#";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
        decimalFormat.setParseBigDecimal(true);

// parse the string
        return decimalFormat.format(amount);
    }
}
