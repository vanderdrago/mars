package com.muamalat.suganda.reportva;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.KonversiRekening;
import com.muamalat.reportmcb.entity.SknIn;
import java.math.BigDecimal;

public class PerantaraRpt {

    public ArrayList moveListToHashMap(List lhist) {
        BigDecimal akumulasi = new BigDecimal(0);
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        int inc = 1;
        for (Iterator<HistTransRekVa> i = lhist.iterator(); i.hasNext();) {
            HistTransRekVa hist = i.next();
            rowMap = new HashMap();
            rowMap.put("account", hist.getAccount());
            rowMap.put("id", inc++);
            rowMap.put("noref", hist.getTrn_ref_no());
            rowMap.put("trn_dt", hist.getTrn_dt());
            rowMap.put("value_dt", hist.getVal_dt());
            rowMap.put("trn_cd", hist.getTrn_code());
            rowMap.put("desc", hist.getDesc());
            rowMap.put("no_warkat", hist.getInstrument_code());
            rowMap.put("akumulasi", hist.getAkumulasi());
            rowMap.put("drcr", hist.getDrcr());
            rowMap.put("nominal", hist.getNominal());
            rowMap.put("saldo", hist.getSaldo());
            detail.add(rowMap);
        }
        return detail;
    }

    public ArrayList moveListToHashMapRepSknIn(List lhist) {
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        int inc = 1;
        for (Iterator<SknIn> i = lhist.iterator(); i.hasNext();) {
            SknIn hist = i.next();
            rowMap = new HashMap();
            rowMap.put("no", inc++);
            rowMap.put("noref", hist.getNoref());
            rowMap.put("sor", hist.getSor());
            rowMap.put("sbp", hist.getSbp());
            rowMap.put("nmpngirim", hist.getNm_pengirim());
            rowMap.put("nmpenerima", hist.getNm_penerima());
            rowMap.put("nmpenerimabmi", hist.getNm_penerima_core());
            rowMap.put("rektujuan", hist.getRek_tujuan());
            rowMap.put("nominal", hist.getNominal());
            rowMap.put("ket", hist.getKet());
            rowMap.put("sts", hist.getSts());
            detail.add(rowMap);
        }
        return detail;
    }

    public List getSknInRepRetTtpn(List lhist, int a, int b) {
        List lttpn = new ArrayList();
        int inc = 1;
        for (Iterator<SknIn> i = lhist.iterator(); i.hasNext();) {
            SknIn hist = i.next();
            if (hist.getNo() >= a && hist.getNo() <= b) {
                lttpn.add(hist);
            }
        }
        return lttpn;
    }

    public List getAcc_lineHist(List lhist, int a, int b) {
        List lttpn = new ArrayList();
        int inc = 1;
        for (Iterator<Acc_line> i = lhist.iterator(); i.hasNext();) {
            Acc_line hist = i.next();
            if (hist.getNo() >= a && hist.getNo() <= b) {
                lttpn.add(hist);
            }
        }
        return lttpn;
    }

    public ArrayList moveListToHashMapKonvRek(List lhist) {
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        int inc = 1;
        for (Iterator<KonversiRekening> i = lhist.iterator(); i.hasNext();) {
            KonversiRekening hist = i.next();
            rowMap = new HashMap();
            rowMap.put("no", inc++);
            rowMap.put("altnmrek", hist.getAlt_nm_rekening());
            rowMap.put("altrek", hist.getAlt_rekening());
            rowMap.put("rek", hist.getRekening());
            rowMap.put("nmrek", hist.getNmrekening());
            rowMap.put("sts", hist.getStatus());
            rowMap.put("saldo", hist.getSaldo());
            detail.add(rowMap);
        }
        return detail;
    }

    
}
