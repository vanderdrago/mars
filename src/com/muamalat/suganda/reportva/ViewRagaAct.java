package com.muamalat.suganda.reportva;

import com.muamalat.reportmcb.action.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class ViewRagaAct extends org.apache.struts.action.Action {
	/* forward name="success" path="" */
	private static Logger log = Logger.getLogger(ViewRagaAct.class);
	private static String SUCCESS = "success";

	/** Action called on save button click
     */
	
	
	  public ActionForward execute(ActionMapping mapping, ActionForm form,
	  HttpServletRequest request, HttpServletResponse response) throws
	  Exception {
	  
	  SUCCESS = isValidUser(request);
	  
	  Admin user = (Admin)
	  request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
	  
	  if (!SUCCESS.equals("success")) { return mapping.findForward(SUCCESS); }
	  try{ 
		  List lph = new ArrayList(); 
		  SqlFunction f = new SqlFunction();
		  String norek = request.getParameter("acount"); 
		  String nama = request.getParameter("nama"); 
		  String ktp = request.getParameter("ktp");
		  String lhr = request.getParameter("lhr");
	  
	  
	  request.getSession(true).setAttribute("currentPage", null);
	  request.getSession(true).setAttribute("noOfRecords", null);
	  request.getSession(true).setAttribute("noOfPages", null);
	  request.getSession(true).setAttribute("norek", null);
	  request.getSession(true).setAttribute("nama", null);
	  
	  int sawal = 1; int sakhir = 10; int page = 1; int noOfRecords = 0; int noOfPages = 0; int recordsPerPage = 30; 
	  noOfRecords = f.getTotCust(norek,nama.toUpperCase(),ktp,lhr); 
	  if(noOfRecords > 0){ 
		  noOfPages = (int)
		  Math.ceil(noOfRecords * 1.0 / recordsPerPage); 
		  if (noOfPages < sakhir){
			  sakhir = noOfPages; 
		  } 
		  lph = f.getListCustom(norek,nama.toUpperCase(),ktp,lhr, sawal - 1); 
		  request.setAttribute("sawal", sawal); 
		  request.setAttribute("sakhir", sakhir);
		  request.setAttribute("lph", lph); 
		  request.setAttribute("viewlph", "vph");
		  request.setAttribute("noOfRecords", noOfRecords);
		  request.setAttribute("noOfPages", noOfPages);
		  request.setAttribute("currentPage", page);
		  request.getSession(true).setAttribute("currentPage", page);
		  request.getSession(true).setAttribute("noOfRecords", noOfRecords);
		  request.getSession(true).setAttribute("noOfPages", noOfPages);
		  request.getSession(true).setAttribute("norek", norek);
		  request.getSession(true).setAttribute("nama", nama);
		  request.getSession(true).setAttribute("ktp", ktp);
		  request.getSession(true).setAttribute("lhr", lhr);
		  request.getSession(true).setAttribute("llsp", lph); 
	  } return
	  mapping.findForward(SUCCESS); } catch (Exception e) {
	  request.setAttribute("errpage", "Error : " + e.getMessage()); 
	  return mapping.findForward("failed"); 
	  } 
	  }
	  
	  private String isValidUser(HttpServletRequest request) {
		  int validLevel = 5;
	        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
	            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
	            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
	            return "loginpage";
	        }
	        List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
	        SqlFunction sql = new SqlFunction();
	        if (sql.getMenuStatus(lrole, "peragaanHistVaFwd.do")){
		        return SUCCESS;
		    } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
		        }
	    }
	  
	 
	/*
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,
					validLevel);
			request.setAttribute("message",
					"Login Session Expired, silahkan login kembali!");
			return mapping.findForward("loginpage");
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(
				StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5
				|| level == 6 || level == 7) {
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(
					StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm
					.getKodecabang());
			System.out.println("user : " + adm.getUsername());
			System.out.println("cabang : " + adm.getKodecabang());
			System.out.println("role : " + user.getRole_id());
			String role = cpf.getAllowedPrintPassbook(12, user.getRole_id());
			System.out.println("role11 : " + role);
			System.out.println("user.BranchCode: " + user.getBranch_code());

			if ("000".equals(adm.getKodecabang()) || (role != null && role.indexOf(user.getRole_id().toUpperCase()) > -1)) {
				List lph = new ArrayList();
				SqlFunction f = new SqlFunction();
				String norek = request.getParameter("acount");
				String nama = request.getParameter("nama");
				String ktp = request.getParameter("ktp");
				String lhr = request.getParameter("lhr");

				request.getSession(true).setAttribute("currentPage", null);
				request.getSession(true).setAttribute("noOfRecords", null);
				request.getSession(true).setAttribute("noOfPages", null);
				request.getSession(true).setAttribute("norek", null);
				request.getSession(true).setAttribute("nama", null);

				int sawal = 1;
				int sakhir = 10;
				int page = 1;
				int noOfRecords = 0;
				int noOfPages = 0;
				int recordsPerPage = 30;
				noOfRecords = f.getTotCust(norek, nama.toUpperCase(), ktp, lhr);
				if (noOfRecords > 0) {
					noOfPages = (int) Math.ceil(noOfRecords * 1.0
							/ recordsPerPage);
					if (noOfPages < sakhir) {
						sakhir = noOfPages;
					}
					lph = f.getListCustom(norek, nama.toUpperCase(), ktp, lhr,
							sawal - 1);
					request.setAttribute("sawal", sawal);
					request.setAttribute("sakhir", sakhir);
					request.setAttribute("lph", lph);
					request.setAttribute("viewlph", "vph");
					request.setAttribute("noOfRecords", noOfRecords);
					request.setAttribute("noOfPages", noOfPages);
					request.setAttribute("currentPage", page);
					request.getSession(true).setAttribute("currentPage", page);
					request.getSession(true).setAttribute("noOfRecords",
							noOfRecords);
					request.getSession(true).setAttribute("noOfPages",
							noOfPages);
					request.getSession(true).setAttribute("norek", norek);
					request.getSession(true).setAttribute("nama", nama);
					request.getSession(true).setAttribute("ktp", ktp);
					request.getSession(true).setAttribute("lhr", lhr);
					request.getSession(true).setAttribute("llsp", lph);
				}
				return mapping.findForward(SUCCESS);
			} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
				return mapping.findForward("msgpage");
			}

		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			return mapping.findForward("msgpage");
		}
	}*/
}
