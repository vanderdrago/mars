/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.singleton;

import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author herry.suganda
 */
public class DatasourceEntry {

    private static Logger logger = Logger.getLogger(DatasourceEntry.class);
    private static DatasourceEntry log = null;
    private DataSource postgreDS = null;
    private DataSource alimnetDS = null;
    private DataSource cmspostgreDS = null;
    private DataSource way4DS = null;
    private DataSource eDapemDS = null;
	private DataSource mig2DS = null;
    private DataSource txnMassalDS = null;
    private DataSource rtgsDS = null;
    private DataSource sknDS = null;
    private DataSource ibDS = null;
    private DataSource cmsDS = null;
    private DataSource mcb2DS = null;
    private DataSource stgDS = null;
    private DataSource locpgDS = null;
    private DataSource refdwhDS = null;
    private DataSource mig26mssl = null;
    private DataSource PERFMON = null;
    private DataSource mcbprod = null;
    private DataSource estatement = null;
    private DataSource ctkpssbk = null;
    private DataSource db2BmiprodDS = null;
    private DataSource netezzaDS = null;
    private DataSource marsDs = null;
    
    private DataSource loc2pgDS = null;
    
    private DataSource smtbuserDS = null;
    
//    private DataSource mBankingDS = null;    
    
//	public DataSource getmBankingDS() {
//		return mBankingDS;
//	}
//
//	public void setmBankingDS(DataSource mBankingDS) {
//		this.mBankingDS = mBankingDS;
//	}

	public DataSource getNetezzaDS() {
		return netezzaDS;
	}

	public DataSource getMarsDs() {
		return marsDs;
	}

	public void setMarsDs(DataSource marsDs) {
		this.marsDs = marsDs;
	}

	public void setNetezzaDS(DataSource netezzaDS) {
		this.netezzaDS = netezzaDS;
	}

	public DataSource getDb2BmiprodDS() {
		return db2BmiprodDS;
	}

	public void setDb2BmiprodDS(DataSource db2BmiprodDS) {
		this.db2BmiprodDS = db2BmiprodDS;
	}

	public DataSource getRefdwhDS() {
		return refdwhDS;
	}

	public void setRefdwhDS(DataSource refdwhDS) {
		this.refdwhDS = refdwhDS;
	}

	public DataSource getStgDS() {
		return stgDS;
	}

	public void setStgDS(DataSource stgDS) {
		this.stgDS = stgDS;
	}

	public DataSource getIbDS() {
		return ibDS;
	}

	public void setIbDS(DataSource ibDS) {
		this.ibDS = ibDS;
	}

	public DataSource getRtgsDS() {
		return rtgsDS;
	}

	public void setRtgsDS(DataSource rtgsDS) {
		this.rtgsDS = rtgsDS;
	}

	
	public DataSource getSknDS() {
		return sknDS;
	}

	public void setSknDS(DataSource sknDS) {
		this.sknDS = sknDS;
	}

	public DatasourceEntry() {
    }

    public static DatasourceEntry getInstance() {
        if (log == null) {
            log = new DatasourceEntry();
        }

        return log;
    }


    /**
     * @return the postgreDS
     */
    public DataSource getPostgreDS() {
        return postgreDS;
    }

    /**
     * @param postgreDS the postgreDS to set
     */
    public void setPostgreDS(DataSource postgreDS) {
        this.postgreDS = postgreDS;
    }

    /**
     * @return the alimnetDS
     */
    public DataSource getAlimnetDS() {
        return alimnetDS;
    }

    /**
     * @param alimnetDS the alimnetDS to set
     */
    public void setAlimnetDS(DataSource alimnetDS) {
        this.alimnetDS = alimnetDS;
    }

    /**
     * @return the cmspostgreDS
     */
    public DataSource getCmspostgreDS() {
        return cmspostgreDS;
    }

    /**
     * @param cmspostgreDS the cmspostgreDS to set
     */
    public void setCmspostgreDS(DataSource cmspostgreDS) {
        this.cmspostgreDS = cmspostgreDS;
    }

    /**
     * @return the cmsoracleDS
     */
//    public DataSource getCmsoracleDS() {
//        return cmsoracleDS;
//    }
//
//    /**
//     * @param cmsoracleDS the cmsoracleDS to set
//     */
//    public void setCmsoracleDS(DataSource cmsoracleDS) {
//        this.cmsoracleDS = cmsoracleDS;
//    }

    public DataSource getWay4DS() {
        return way4DS;
    }

    public void setWay4DS(DataSource way4DS) {
        this.way4DS = way4DS;
    }

	public DataSource getEDapemDS() {
		return this.eDapemDS;
	}

	public void setEDapemDS(DataSource dapemDS) {
		this.eDapemDS = dapemDS;
	}
	
//	public DataSource getMcbDS() {
//        return mcbDS;
//    }

//    public void setMcbDS(DataSource mcbDS) {
//        this.mcbDS = mcbDS;
//    }
    
    public DataSource getMig2DS() {
        return mig2DS;
    }

    public void setMig2DS(DataSource mig2DS) {
        this.mig2DS = mig2DS;
    }

    public DataSource getTxnMassalDS() {
        return txnMassalDS;
    }

    public void setTxnMassalDS(DataSource txnMassalDS) {
        this.txnMassalDS = txnMassalDS;
    }

	public DataSource getCmsDS() {
		return cmsDS;
	}

	public void setCmsDS(DataSource cmsDS) {
		this.cmsDS = cmsDS;
	}

	public DataSource getMcb2DS() {
		return mcb2DS;
	}

	public void setMcb2DS(DataSource mcb2ds) {
		mcb2DS = mcb2ds;
	}

	public DataSource getLocpgDS() {
		return locpgDS;
	}

	public void setLocpgDS(DataSource locpgDS) {
		this.locpgDS = locpgDS;
	}

	public DataSource getMig26mssl() {
		return mig26mssl;
	}

	public void setMig26mssl(DataSource mig26mssl) {
		this.mig26mssl = mig26mssl;
	}

	public DataSource getPERFMON() {
		return PERFMON;
	}

	public void setPERFMON(DataSource perfmon) {
		PERFMON = perfmon;
	}

	public DataSource getMcbprod() {
		return mcbprod;
	}

	public void setMcbprod(DataSource mcbprod) {
		this.mcbprod = mcbprod;
	}

	public DataSource getEstatement() {
		return estatement;
	}

	public void setEstatement(DataSource estatement) {
		this.estatement = estatement;
	}

	public DataSource getCtkpssbk() {
		return ctkpssbk;
	}

	public void setCtkpssbk(DataSource ctkpssbk) {
		this.ctkpssbk = ctkpssbk;
	}

	public DataSource getLoc2pgDS() {
		return loc2pgDS;
	}

	public void setLoc2pgDS(DataSource loc2pgDS) {
		this.loc2pgDS = loc2pgDS;
	}

	public DataSource getSmtbuserDS() {
		return smtbuserDS;
	}

	public void setSmtbuserDS(DataSource smtbuserDS) {
		this.smtbuserDS = smtbuserDS;
	}
 
	    
}
