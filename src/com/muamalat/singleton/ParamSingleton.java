/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.singleton;

import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author herry.suganda
 */
public class ParamSingleton {

    private static Logger logger = Logger.getLogger(ParamSingleton.class);
    private static ParamSingleton log = null;
    private String statusServer;
    
	public String getStatusServer() {
		return statusServer;
	}

	public void setStatusServer(String statusServer) {
		this.statusServer = statusServer;
	}

	public ParamSingleton() {
    }

    public static ParamSingleton getInstance() {
        if (log == null) {
            log = new ParamSingleton();
        }

        return log;
    }


}
