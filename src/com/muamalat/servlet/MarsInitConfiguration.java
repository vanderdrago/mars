package com.muamalat.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.muamalat.database.process.ProcessOnDatabaseBean;
import com.muamalat.singleton.ParamSingleton;

/**
 * Servlet implementation class MarsInitConfiguration
 */
public class MarsInitConfiguration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MarsInitConfiguration() {
        super();
        // TODO Auto-generated constructor stub
        System.out.println("====================running MarsInitConfiguration==================");
        ParamSingleton.getInstance().setStatusServer(new ProcessOnDatabaseBean().getStatusServer());
    }

}
