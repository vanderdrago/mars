/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.servlet;

import com.muamalat.singleton.DatasourceEntry;
import java.io.IOException;
import java.io.PrintWriter;
import javax.jms.*;
import javax.naming.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class DatasourceLookup extends HttpServlet {

    private static Logger log = Logger.getLogger(DatasourceLookup.class);
 
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DatasourceLookup</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DatasourceLookup at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public DatasourceLookup() throws NamingException, JMSException {
        super();
        //System.out.println("=-=-=-=-=-=-=-=-=-=-= DatasourceLookup started =-=-=-=-=-=-=-=-=-=-=");
//        DatasourceEntry.getInstance().setOracleDS(getDataSource("MFEPOracleDS"));
//        DatasourceEntry.getInstance().setPostgreDS(getDataSource("MFEPPostgresDS"));
//        DatasourceEntry.getInstance().setAlimnetDS(getDataSource("MFEPPostgresDS"));
        DatasourceEntry.getInstance().setPostgreDS(getDataSource("almDatasource"));
//        DatasourceEntry.getInstance().setAlimnetDS(getDataSource("almDatasource"));
//        DatasourceEntry.getInstance().setEDapemDS(getDataSource("ReportDS"));
//        DatasourceEntry.getInstance().setCmspostgreDS(getDataSource("cmsDataSource"));
//        DatasourceEntry.getInstance().setCmsoracleDS(getDataSource("cmsOracleDS"));
//        DatasourceEntry.getInstance().setWay4DS(getDataSource("way4ds"));
//		DatasourceEntry.getInstance().setMcbDS(getDataSource("MCBds"));
		DatasourceEntry.getInstance().setRtgsDS(getDataSource("MSSQLDS"));
		DatasourceEntry.getInstance().setSknDS(getDataSource("MSSKNDS"));
		DatasourceEntry.getInstance().setIbDS(getDataSource("IBDS"));
		DatasourceEntry.getInstance().setCmsDS(getDataSource("CMSds"));
		DatasourceEntry.getInstance().setMig2DS(getDataSource("mig26-ds"));
		DatasourceEntry.getInstance().setMcb2DS(getDataSource("MCB2ds"));
 		DatasourceEntry.getInstance().setLocpgDS(getDataSource("locpgDS"));
 		DatasourceEntry.getInstance().setStgDS(getDataSource("stgds"));
 		DatasourceEntry.getInstance().setRefdwhDS(getDataSource("refdwhds"));
        DatasourceEntry.getInstance().setMig26mssl(getDataSource("mig26mssl-ds"));
        DatasourceEntry.getInstance().setPERFMON(getDataSource("PERFMON"));
        DatasourceEntry.getInstance().setMcbprod(getDataSource("MCBprodds"));
        DatasourceEntry.getInstance().setEstatement(getDataSource("estatement"));
        DatasourceEntry.getInstance().setCtkpssbk(getDataSource("ctkpssbk"));
        DatasourceEntry.getInstance().setDb2BmiprodDS(getDataSource("db2bmiprodds"));
        DatasourceEntry.getInstance().setNetezzaDS(getDataSource("netezzaDataSource"));
        DatasourceEntry.getInstance().setMarsDs(getDataSource("marsRepDS"));
        DatasourceEntry.getInstance().setWay4DS(getDataSource("pinpadDS"));
        DatasourceEntry.getInstance().setSmtbuserDS(getDataSource("smtbuserDS"));
//        DatasourceEntry.getInstance().setmBankingDS(getDataSource("mBankingDS"));
    }

    @Override
    public void destroy() {
        //System.out.println("=-=-=-=-=-=-=-=-=-=-= destroy");


    }

    private DataSource getDataSource(String dsName) {
        InitialContext ctx = null;
        DataSource datasource = null;
        try {
            ctx = new InitialContext();
            log.debug("env" + ctx.getEnvironment());
        } catch (NamingException e1) {
            log.error(e1.getMessage());
        }
        if (datasource == null) {
            try {
                log.debug("get data source from java:comp/env/" + dsName);
                datasource = (DataSource) ctx.lookup("java:comp/env/" + dsName);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (datasource == null) {
            try {
                log.debug("get data source from java:/" + dsName);
                datasource = (DataSource) ctx.lookup("java:/" + dsName);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (datasource == null) {
            try {
                datasource = (DataSource) ctx.lookup(dsName);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return datasource;
    }
}
