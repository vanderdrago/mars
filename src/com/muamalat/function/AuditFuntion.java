/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.function;

import com.muamalat.database.process.ProcessOnDatabase;
import com.muamalat.database.process.ProcessOnDatabaseBean;
import com.muamalat.parameter.StaticParameter;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class AuditFuntion {

    private static Logger log = Logger.getLogger(AuditFuntion.class);
    ProcessOnDatabase audit = new ProcessOnDatabaseBean();

    public void login(String user) {
        HashMap hm = new HashMap();
        hm.put(StaticParameter.ACTIVITY_AUDIT, "Login");
        audit.insertAudit(hm, user);
    }

    public void logout(String user) {
        HashMap hm = new HashMap();
        hm.put(StaticParameter.ACTIVITY_AUDIT, "Logout");
        audit.insertAudit(hm, user);
    }

}
