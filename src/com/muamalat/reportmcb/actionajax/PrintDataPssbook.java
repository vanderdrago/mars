package com.muamalat.reportmcb.actionajax;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.LmtTrnsPssbk;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.DataFunction;
import com.muamalat.reportmcb.perantara.PerantaraCetakPassbook;

public class PrintDataPssbook extends org.apache.struts.action.Action {
	
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(PrintDataPssbook.class);
    
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
		String acc = (String) request.getSession(true).getAttribute("acc");	
		String optlne = (String) request.getParameter("lne");	
		String strtlne = (String) request.getParameter("startlne");
		int startline = 1;
		
		List lhist = (List) request.getSession(true).getAttribute("lhist_sisa");
		Reklist cust_acc = (Reklist) request.getSession(true).getAttribute("cust_acc");
		if(lhist.size() > 0){
	    	CetakPassbookFunction cpf = new CetakPassbookFunction();
			LmtTrnsPssbk lmt = cpf.getlimit_trans_passbook();
			Acc_line ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "print");			

			if("2".equals(optlne)){
				startline = Integer.parseInt(strtlne);
			} else {
				startline = ac_line.getPrev_line_no() + 1;
			}
			int page = ac_line.getPrev_page_no();
			if (startline > lmt.getLimit_per_page()){
				startline = 1;
				page = page + 1;
			}
			JSONArray listobj = new JSONArray();
			JSONObject obj = null;
			CetakPassbook pbook1 = null;
			int sz = 1;
			int tot_kosong = startline;
			if(startline > 1){
				if (startline >= ((lmt.getLimit_per_page()/2) + 1)){
					tot_kosong = startline + 3;
				} else {
					tot_kosong = startline;
				}
				for (int n = 1; n<tot_kosong; n++){
					obj = new JSONObject();
					obj.put("no", "&nbsp;");
					obj.put("dt", "&nbsp;");
					obj.put("trncd", "&nbsp;");
					obj.put("drcr", "&nbsp;");
					obj.put("mutasi", "&nbsp;");
					obj.put("detail", "&nbsp;");
					obj.put("saldo", "&nbsp;");
					obj.put("auth", "&nbsp;");
					listobj.add(obj);
				}
//				sz = listobj.size() + 1;
				sz = startline;
			} 
			PerantaraCetakPassbook pcp = new PerantaraCetakPassbook();
			int w = 1;
    		for (Iterator<CetakPassbook> i = lhist.iterator(); i.hasNext();) {
				CetakPassbook pbook = i.next();
				if (sz <= lmt.getLimit_per_page() && (pbook.getAc_entry_sr_no() != null)){
//						System.out.println("a : " + pbook.getAc_entry_sr_no() + ", b : " + ac_line.getAc_entry_sr_no() + ", :: " + ac_line.getAc_entry_sr_no().compareTo(pbook.getAc_entry_sr_no()) );
					if (sz == ((lmt.getLimit_per_page()/2) + 1)){
						for (int x=1; x<=3; x++){
							obj = new JSONObject();
							obj.put("no", "&nbsp;");
							obj.put("dt", "&nbsp;");
							obj.put("trncd", "&nbsp;");
							obj.put("drcr", "&nbsp;");
							obj.put("mutasi", "&nbsp;");
							obj.put("detail", "&nbsp;");
							obj.put("saldo", "&nbsp;");
							obj.put("auth", "&nbsp;");
							listobj.add(obj);
						}
					}
					obj = new JSONObject();
					obj.put("no", DataFunction.converttostr(sz));
					obj.put("dt", pbook.getTrn_dt_s());
					obj.put("trncd", pbook.getTrn_code());
					obj.put("drcr", pbook.getDrcr_ind());
					obj.put("mutasi", pbook.getMutasi_s());
					obj.put("detail", pbook.getDetail());
					obj.put("saldo", pbook.getSaldo_s());
					obj.put("auth", pbook.getAuth_id());
					listobj.add(obj);
					sz++;
					pbook1 = pbook;
				}
				w++;
    		}
			long time = System.currentTimeMillis();
	        java.sql.Timestamp timestamp = new java.sql.Timestamp(time);
    		if (listobj.size() > lmt.getLimit_per_page()){
				page = page + 1;
			}
    		if(cust_acc.getPassbook_number() == null || "".equals(cust_acc.getPassbook_number())){
	    		ac_line.setPassbook_number("");
    			
    		} else {
	    		ac_line.setPassbook_number(cust_acc.getPassbook_number());	    			
    		}
//    		ac_line.setTotal_trns(sz - 1);
    		ac_line.setTotal_trns(lhist.size());
			Acc_line acc_new = new Acc_line();
			acc_new.setAccount_no(ac_line.getAccount_no());
			acc_new.setBranch(ac_line.getBranch());	
			acc_new.setPrev_page_no(page);			
			int prev_ln = (startline - 1) + lhist.size();
			if(prev_ln <= lmt.getLimit_per_page()){
				acc_new.setPrev_line_no(prev_ln);	
				acc_new.setPrev_line_no_start(startline);	
			} else {
				acc_new.setPrev_line_no(0);
				acc_new.setPrev_line_no_start(0);
			}
			acc_new.setAc_entry_sr_no(pbook1.getAc_entry_sr_no());
	        acc_new.setPrint_date(timestamp);
			acc_new.setLast_date_trn(pbook1.getTrn_dt());
			
			ac_line.setPrev_line_no_start(startline-1);
    		cpf.insertAccLineHist(ac_line, user.getUsername());
			cpf.updateAccLine(ac_line, acc_new);
			response.setContentType("text/text;charset=utf-8");
			response.setHeader("cache-control", "no-cache");
			PrintWriter out = response.getWriter();
			out.println(listobj);
			out.flush();			
		} else {
			log.info("data tidak ditemukan");
		}
		return null;
	}
	
	private String isValidUser(HttpServletRequest request) {    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
    }
}
