package com.muamalat.reportmcb.actionajax;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.EstateFunction;

public class EStatGetCif extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(GetCountPrintPassbook.class);
    
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
		JSONObject obj = new JSONObject();
        try{
        	EstateFunction ef = new EstateFunction();
    		String acc = request.getParameter("acc");  
    		if (acc != null && !"".equals(acc)){
    			System.out.println("1");
    			E_stat_cust st_cust_in = ef.getEStatCust(acc, "estatin");	
    			if(st_cust_in == null){
    				System.out.println("2");
    				E_stat_cust cust = ef.getCustomer(acc);    				
    				if (cust == null){
    					System.out.println("3");
    					obj.put("data", "<b>Rekening tidak ditemukan.</b>");    						
    				} else {
    					if ("O".equals(cust.getRecord_stat().trim())){
        					String ret = "<form action='/ReportMCB/insertRegEStat.do' method='POST' name='jurnal'><table border='0' width=550px>";
        					if ("I".equals(cust.getCust_type())){
        						ret += "<tr><td colspan = 3><b>NASABAH INDIVIDU</b></td>";
    	    				} else {
    	    					ret += "<tr><td colspan = 3><b>NASABAH NON INDIVIDU</b></td>";
    	    				}
        					ret += "<tr><td width=150px;>Nomor Rekening</td><td width=5px;>:</td><td>" + cust.getAccount() + "</td>" +
        						"<tr><td>No. CIF</td><td>:</td><td>" + cust.getCust_no() + "</td>" +
        						"<tr><td valign='top'>Nama Rekening</td><td valign='top'>:</td><td>" + cust.getCust_name() + "</td>" +
    							"<tr><td valign='top'>Nama Produk</td><td valign='top'>:</td><td>" + cust.getAcc_class() + " - " + cust.getAcc_class_desc() + "</td>" +
    							"<tr><td>Tanggal Lahir</td><td>:</td><td>" + cust.getDt_birth() + "</td>" +
    							"<tr><td>Nama Ibu Kandung</td><td>:</td><td>" + cust.getIbu_kndng() + "</td>" +
    							"<tr><td valign='top'>Alamat</td><td valign='top'>:</td><td>" + cust.getCust_addrs() + "</td>" +
    							"<tr><td>Email (to)</td><td>:</td><td><input type='text' name='email_to' id='email_to' size='50'><label style='color:red'>*</label></td>" +
    							"<tr><td>Email (cc)</td><td>:</td><td><input type='text' name='email_cc' id='email_cc' size='50'></td>" +
    							"<tr><td>Status</td><td>:</td><td><select name='updtsts' id='updtsts'>" +
    							"<option value=''>Pilih</option>" +
										"<option value='O'>Active</option>" +
    									"<option value='H'>Hold</option>" +
    									"</select></td></tr>" +
    							"<tr><td ></td><td></td><td><input onclick='return valRegEStat()' type='submit' value='Submit' name='btn' id='regestat'/>&nbsp;&nbsp;<input type='submit' value='Batal' name='btn' id='idbtl'/></td></tr>" +
    						 "</table></form>";
    	    				obj.put("data", ret);
    	    				request.getSession(true).setAttribute("cust_e_state", cust);    						
    					} else if ("C".equals(cust.getRecord_stat().trim())){
        					obj.put("data", "<b>Rekening Tutup.</b>");      						
    					}
    				}
    			} else {
					obj.put("data", "<b>Rekening sudah terdaftar. " + st_cust_in.getSts_data_desc()+ ".</b>");
    			}
    		} else {
    			obj.put("data", "<b>Silahkan isi nomor Rekening.</b>");
    		}
        } catch (Exception e) {
			obj.put("data", "Error : " + e.getMessage());
		}
	 		
		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();
		out.println(obj);
		out.flush();
		return null;
	}
	
	private String isValidUser(HttpServletRequest request) {    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			/*================= tambahan baru ==============*/
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			List lroles = cpf.getRolesUser(adm.getUsername());
		 	int ada = 0;
		 	if (lroles.size() > 0) {
		 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
		 			String rl = it.next();		 		
		 			String role3 = cpf.getAllowedStatement(11, rl.toUpperCase());
		 			if (!"".equals(role3) && role3!=null){
		 				ada++;
		 			}
		 		}
			} 
		 	cpf= null;
		 	if (ada > 0){
				return SUCCESS;					
		 	} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
		 	}
			/*
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(11, user.getRole_id());
			if ("".equals(role) || role == null){
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)){
					return SUCCESS;					
				} else {	
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";						
				}
			}*/
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			//request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
	    }


    }

}
