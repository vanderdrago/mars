package com.muamalat.reportmcb.actionajax;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.LmtTrnsPssbk;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.DataFunction;

public class RePrintDataPssbook extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(RePrintDataPssbook.class);
    
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }		
        String no = (String) request.getParameter("no");
        String ket = (String) request.getParameter("ket");
		List lreprint = (List) request.getSession(true).getAttribute("lreprint");
		LmtTrnsPssbk lmt = (LmtTrnsPssbk) request.getSession(true).getAttribute("LmtTrnsPssbk");

		Acc_line ac_line = (Acc_line) request.getSession(true).getAttribute("ac_line_hist");
		String usr_spv = (String) request.getSession(true).getAttribute("usr_spv");
		Reklist cust_acc = (Reklist) request.getSession(true).getAttribute("cust_acc");
		try {		
			JSONArray listobj = new JSONArray();
			JSONObject obj = null;
			if (lreprint != null){
				CetakPassbook pbook1 = null;
				int sz = 1; 
				int tottrns = 0;
		    	CetakPassbookFunction cpf = new CetakPassbookFunction();
	    		for (Iterator<CetakPassbook> i = lreprint.iterator(); i.hasNext();) {
					CetakPassbook pbook = i.next();
					if(pbook.getAc_entry_sr_no() != null ){
						tottrns++;
					}
					if (sz == ((lmt.getLimit_per_page()/2) + 1)){
						for (int x=1; x<=3; x++){
							obj = new JSONObject();
							obj.put("no", "&nbsp;");
							obj.put("dt", "&nbsp;");
							obj.put("trncd", "&nbsp;");
							obj.put("drcr", "&nbsp;");
							obj.put("mutasi", "&nbsp;");
							obj.put("detail", "&nbsp;");
							obj.put("saldo", "&nbsp;");
							obj.put("auth", "&nbsp;");
							listobj.add(obj);
						}
					}
					if(pbook.getNo() < Integer.parseInt(no)){
						obj = new JSONObject();
						obj.put("no", "&nbsp;");
						obj.put("dt", "&nbsp;");
						obj.put("trncd", "&nbsp;");
						obj.put("drcr", "&nbsp;");
						obj.put("mutasi", "&nbsp;");
						obj.put("detail", "&nbsp;");
						obj.put("saldo", "&nbsp;");
						obj.put("auth", "&nbsp;");
					} else {
						obj = new JSONObject();
						obj.put("no", DataFunction.converttostr(pbook.getNo()));
						obj.put("dt", pbook.getTrn_dt_s());
						obj.put("trncd", pbook.getTrn_code());
						obj.put("drcr", pbook.getDrcr_ind());
						obj.put("mutasi", pbook.getMutasi_s());
						obj.put("detail", pbook.getDetail());
						obj.put("saldo", pbook.getSaldo_s());
						obj.put("auth", pbook.getAuth_id());
					} 
					sz++;
					listobj.add(obj);
	    		}
	    		ac_line.setUser_print(user.getUsername());
	    		ac_line.setUser_spv_print(usr_spv.toUpperCase());
	    		ac_line.setSts("RP");
	    		ac_line.setPrev_line_no_start(Integer.parseInt(no) - 1);
	    		if(cust_acc.getPassbook_number() == null || "".equals(cust_acc.getPassbook_number())){
		    		ac_line.setPassbook_number("");
	    			
	    		} else {
		    		ac_line.setPassbook_number(cust_acc.getPassbook_number());	    			
	    		}
	    		ac_line.setTotal_trns(tottrns);
	    		ac_line.setNotes(ket);
	    		cpf.insertAccLineReprint(ac_line);
			}
			response.setContentType("text/text;charset=utf-8");
			response.setHeader("cache-control", "no-cache");
			PrintWriter out = response.getWriter();
			out.println(listobj);
			out.flush();			
		} catch (Exception e) {
			System.out.println("error in RePrintDataPssbook : " + e.getMessage());
		} 
		return null;
	}
	
	private String isValidUser(HttpServletRequest request) {    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
    }

}
