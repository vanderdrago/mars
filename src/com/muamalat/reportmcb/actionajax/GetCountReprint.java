package com.muamalat.reportmcb.actionajax;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.CetakPassbookFunction;

public class GetCountReprint extends org.apache.struts.action.Action {

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(GetCountReprint.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}

		JSONObject obj = new JSONObject();
        try{
    		String acc = request.getParameter("acc");
    		if(!"".equals(acc) && acc != null){
    			CetakPassbookFunction cpf = new CetakPassbookFunction();
    			Acc_line line = cpf.getAcc_lineRePrint(acc);
    			if (line != null){
    				String resp = "cetak ulang pada rekening " + acc + " sudah pernah dilakukan sebanyak " + line.getNo()+ " dengan alasan " + line.getNotes();
    				obj.put("sts", "0000"); 
    				obj.put("resp", resp); 
    			} else {
        			obj.put("sts", "8080"); 
        			obj.put("resp", "Tidak ditemukan history cetak ulang passbook");    			
        		}
    		} else {
    			obj.put("sts", "8080"); 
    			obj.put("resp", "No. Rekening null");    			
    		}
        } catch (Exception e) {
			obj.put("sts", "8080"); 
			obj.put("resp", "Error : " + e.getMessage());
		}
		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();
		out.println(obj);
		out.flush();
		return null;
	}

	private String isValidUser(HttpServletRequest request) {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
		}
	}

}
