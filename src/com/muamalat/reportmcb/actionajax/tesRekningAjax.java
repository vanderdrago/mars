package com.muamalat.reportmcb.actionajax;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;

import com.muamalat.parameter.StaticParameter;



public class tesRekningAjax extends org.apache.struts.action.Action{
	
	/* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(tesRekningAjax.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);
		if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
		JSONObject obj = new JSONObject();
		
		return super.execute(mapping, form, request, response);
	}
	private String isValidUser(HttpServletRequest request) {
		int level;
        int validLevel = 8;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level != validLevel) {
            request.setAttribute("message", "Unauthorized Access!");
            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
            return "msgpage";
        } else {
        	return "success";
        }
	}
    
}
