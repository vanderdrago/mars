package com.muamalat.reportmcb.actionajax;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.LmtTrnsPssbk;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.function.CetakPassbookFunction;

public class NextCtkPssbook extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(NextCtkPssbook.class);
    
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        
		String acc = (String) request.getSession(true).getAttribute("acc");			
		List lhist = (List) request.getSession(true).getAttribute("lhist_sisa");
		Reklist cust_acc = (Reklist) request.getSession(true).getAttribute("cust_acc");
		if(lhist != null){
	    	CetakPassbookFunction cpf = new CetakPassbookFunction();
//			LmtTrnsPssbk lmt = cpf.getlimit_trans_passbook(cust_acc.getAcc_clas());
			LmtTrnsPssbk lmt = cpf.getlimit_trans_passbook();
			Acc_line ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "print");			
			
		 		
			List lhist_sisa = new ArrayList(); 
			int line = ac_line.getPrev_line_no();
			int page = ac_line.getPrev_page_no();
			if (lhist != null){
	    		for (Iterator<CetakPassbook> i = lhist.iterator(); i.hasNext();) {
					CetakPassbook pbook = i.next();
					if ((pbook.getAc_entry_sr_no() != null)){
						int ret = pbook.getAc_entry_sr_no().compareTo(ac_line.getAc_entry_sr_no());
						if (ret == 1){
							lhist_sisa.add(pbook);
							line++;
						}  				
					}
	    		}
	            request.getSession(true).setAttribute("lhist_sisa", lhist_sisa);
	            request.getSession(true).setAttribute("cust_acc", cust_acc);
	            
	            
			} 
			if (lhist_sisa.size() >= lmt.getLimit_per_page()){
				page = page + 1;
				line = 0;
			}
			JSONObject obj = new JSONObject();
			if(lhist_sisa.size() > 0){
				ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "print");
				int x = lhist_sisa.size();
				String header = "<table><tr><td width='130px;'>Cabang</td><td>:</td><td>"+cust_acc.getKdcab()+"</td></tr>" +
					"<tr><td>Nomor Rekening</td><td>:</td><td> " + cust_acc.getNomrek()+ "</td></tr>" +
					"<tr><td>Nama</td><td>:</td><td> "+ cust_acc.getRekname()+ "</td></tr>" +
					"<tr valign='baseline'><td>Alamat</td><td>:</td><td> "+ cust_acc.getAddress()+" </td></tr>" +
					"<tr><td>No. Passbook</td><td>:</td><td> <b> " + cust_acc.getPassbook_number()+ " </b> </td></tr></table>";
				obj.put("header", header);
				obj.put("data1", x  + " History transaksi belum dicetak.");
				int pg = 0;
				int hal = 0;
				if((ac_line.getPrev_line_no() + 1) > lmt.getLimit_per_page()){	
					hal = ac_line.getPrev_page_no() + 2;
					obj.put("data2", "Cetak mulai hal. "+ hal +", baris ke 1.");
				} else {				
					hal = ac_line.getPrev_page_no() + 1;
					pg = ac_line.getPrev_line_no() + 1;
					obj.put("data2", "Cetak mulai hal. " + hal + ", baris ke "+ pg +".");
				}
			} else {				
				obj.put("header", "Tidak ada History transaksi yang belum dicetak.");
				obj.put("data1", 0);
				obj.put("data2", 0);
			}
			response.setContentType("text/text;charset=utf-8");
			response.setHeader("cache-control", "no-cache");
			PrintWriter out = response.getWriter();
			out.println(obj);
			out.flush();
			
		} else {
			System.out.println("data tidak ditemukan");
		}
		return null;
	}
	
	private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
