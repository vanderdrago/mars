package com.muamalat.reportmcb.actionajax;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.LmtTrnsPssbk;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.perantara.PerantaraCetakPassbook;

public class GetCountPrintPassbook extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(GetCountPrintPassbook.class);
    
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        CetakPassbookFunction cpf = new CetakPassbookFunction();
		JSONObject obj = new JSONObject();
		
        SMTB_USER_ROLE sor = cpf.getUser(user.getUsername(), user.getKodecabang());
		String role = cpf.getAllowedPrintPassbook(8, sor.getRole_id());
		if ("".equals(role) || role == null){
			obj.put("header", "Unauthorized Access! <br /> Anda tidak memiliki akses ke halaman yang diminta!");
			obj.put("data1", 0);
			obj.put("data2", 0);
			obj.put("stsnext", 0);
		} else {
    		request.getSession(true).setAttribute("lhist_sisa", null);

			String acc = request.getParameter("acc");    	        
	    	PerantaraCetakPassbook pcp = new PerantaraCetakPassbook();        	
	        Reklist cust_acc = cpf.getReklist(acc);	        
	        LmtTrnsPssbk lmt = null;
	        List lhist_v = null;
	        Acc_line ac_line = null;
	        if (cust_acc != null){
	        	if ("S18A".equals(cust_acc.getAcc_clas().toUpperCase().trim()) || "S18B".equals(cust_acc.getAcc_clas().toUpperCase().trim())){	
	        		obj.put("header", "Restricted Account");
					obj.put("data1", 0);
					obj.put("data2", 0);
					obj.put("stsnext", 0); 
	        	} else if ("1".equals(cust_acc.getReksts())){
	    			lmt = cpf.getlimit_trans_passbook();
	    			if (lmt != null){
		    			BigDecimal sldawal = null;
		        		ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "print");
		        		if (ac_line == null){
		        			ac_line = cpf.getAcc_lineMCBProd(acc);
		        			if(ac_line == null){
		        				ac_line = cpf.getAcc_lineMCBHist(acc);
			        			if(ac_line != null){
				        			boolean ret = cpf.insertAccLineprint(ac_line);
				        			ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "print");
			        			}
		        			} else {
			        			boolean ret = cpf.insertAccLineprint(ac_line);
			        			ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "print");
		        			}
		        		} 
		        		if (ac_line != null){
		            		sldawal = cpf.getSaldoAwalTrns(ac_line);
		            		if (sldawal != null){
		    	        		if(ac_line != null){
		    	            		lhist_v = pcp.getListTrans(ac_line, sldawal, "print", lmt);
		    	            		request.getSession(true).setAttribute("lhist_sisa", lhist_v);
		                            request.getSession(true).setAttribute("cust_acc", cust_acc);
		                            request.getSession(true).setAttribute("acc", acc);
		    	        		}
		            		} 
		        		}
		        		/*
		        		if (ac_line != null){
		            		sldawal = cpf.getSaldoAwalTrns(ac_line);
		            		if (sldawal != null){
		    	        		if(ac_line != null){
		    	            		lhist_v = pcp.getListTrans(ac_line, sldawal, "print", lmt);
		    	            		request.getSession(true).setAttribute("lhist_sisa", lhist_v);
		                            request.getSession(true).setAttribute("cust_acc", cust_acc);
		                            request.getSession(true).setAttribute("acc", acc);
		    	        		}
		            		}   		        			
		        		} else {
		        			ac_line = cpf.getAcc_lineMCBProd(acc);
		        			if(ac_line != null){
			        			boolean ret = cpf.insertAccLineprint(ac_line);
			        			if (ret){
					        		ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "print");
			        				sldawal = cpf.getSaldoAwalTrns(ac_line);
				            		if (sldawal != null){
				    	        		if(ac_line != null){
				    	            		lhist_v = pcp.getListTrans(ac_line, sldawal, "print", lmt);
				    	            		if (lhist_v != null){
					    	            		request.getSession(true).setAttribute("lhist_sisa", lhist_v);
					                            request.getSession(true).setAttribute("cust_acc", cust_acc);
					                            request.getSession(true).setAttribute("acc", acc);
				    	            		} else {
				    	            			lhist_v = null;
				    	            		}
				    	        		}
				            		}  
			        			} 
		        			} else {
		        				
		        			}
		        		}
		        		*/
	    			} else {
	    				ac_line = null;
	    			}
	    			
	    			if (ac_line != null){
			    		if (lhist_v != null){
			    			String header = "<table><tr><td width='130px;'>Cabang</td><td>:</td><td>"+cust_acc.getKdcab()+"</td></tr>" +
			    					"<tr><td>Nomor Rekening</td><td>:</td><td> " + cust_acc.getNomrek()+ "</td></tr>" +
			    					"<tr><td>Nama</td><td>:</td><td> "+ cust_acc.getRekname()+ "</td></tr>" +
			    					"<tr valign='baseline'><td>Alamat</td><td>:</td><td> "+ cust_acc.getAddress()+" </td></tr>" +
			    					"<tr><td>No. Passbook</td><td>:</td><td> <b> " + cust_acc.getPassbook_number()+ " </b> </td></tr></table>";
			    			obj.put("header", header);
			    			obj.put("data1", lhist_v.size()  + " History transaksi belum dicetak.");
			    			int pg = 0;
			    			int ln = 0;
			    			if((ac_line.getPrev_line_no() + 1) > lmt.getLimit_per_page()){
			    				pg = ac_line.getPrev_page_no() + 2;
			    				obj.put("data2", "Cetak mulai baris ke 1");
			    			} else {
			    				pg = ac_line.getPrev_page_no() + 1;
			    				ln = ac_line.getPrev_line_no() + 1;
			    				obj.put("data2", "Cetak mulai baris ke "+ ln +".");
			    			}	
			    			if(lhist_v.size() <= lmt.getLimit_per_page()){
			    				obj.put("stsnext", 0);
			    			} else {
			    				obj.put("stsnext", 1);
			    			}
			    		} else {
			    			obj.put("header", "Tidak ada History transaksi yang belum dicetak.");
			    			obj.put("data1", 0);
			    			obj.put("data2", 0);
			    			obj.put("stsnext", 0);
			    		}
			        } else {
						obj.put("header", "Tidak ditemukan mutasi rekening.");
						obj.put("data1", 0);
						obj.put("data2", 0);
						obj.put("stsnext", 0);
					}	    			
	    		} else {
	    			obj.put("header", "Invalid account");
	    			obj.put("data1", 0);
	    			obj.put("data2", 0);
	    			obj.put("stsnext", 0);
	    		}
	        	
	    	} else {
    			obj.put("header", "Account tidak ditemukan.");
    			obj.put("data1", 0);
    			obj.put("data2", 0);
    			obj.put("stsnext", 0);
    		}
		}		
	 		
		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();
		out.println(obj);
		out.flush();
		return null;
	}
	
	private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
