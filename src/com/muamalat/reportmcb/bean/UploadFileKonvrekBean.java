package com.muamalat.reportmcb.bean;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class UploadFileKonvrekBean extends ActionForm {
	  private FormFile theFile;
	  
	  public FormFile getTheFile() {
	    return this.theFile;
	  }

	  public void setTheFile(FormFile theFile) {
	    this.theFile = theFile;
	  }
}
