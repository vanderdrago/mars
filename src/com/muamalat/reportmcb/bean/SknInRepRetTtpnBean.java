package com.muamalat.reportmcb.bean;

import org.apache.struts.action.ActionForm;

public class SknInRepRetTtpnBean extends ActionForm{
	private String bsnsdt;
	private String kdcab;
	private String rpt;
	
	public String getBsnsdt() {
		return bsnsdt;
	}
	public void setBsnsdt(String bsnsdt) {
		this.bsnsdt = bsnsdt;
	}
	public String getKdcab() {
		return kdcab;
	}
	public void setKdcab(String kdcab) {
		this.kdcab = kdcab;
	}
	public String getRpt() {
		return rpt;
	}
	public void setRpt(String rpt) {
		this.rpt = rpt;
	}
	
	

}
