package com.muamalat.reportmcb.bean;
import org.apache.struts.action.*; 

public class IntfRtgsOutBean extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kdcab;
	private String acc;
	private String txnsts;
	private String trn;
	private String trn_cd;
	private String bsnsdt;
	private String tmmbr;
	private String fmmbr;
	private String amtto;
	private String amtf;
	private String reltrn;
	private String channel;
	private String toacc;
	private String fracc;
	private String contractno;
	private String cust_ac_no;
	private String type_trns;
	
	public String getKdcab() {
		return kdcab;
	}
	public void setKdcab(String kdcab) {
		this.kdcab = kdcab;
	}
	public String getAcc() {
		return acc;
	}
	public void setAcc(String acc) {
		this.acc = acc;
	}
	public String getTxnsts() {
		return txnsts;
	}
	public void setTxnsts(String txnsts) {
		this.txnsts = txnsts;
	}
	public String getTrn() {
		return trn;
	}
	public void setTrn(String trn) {
		this.trn = trn;
	}
	public String getTrn_cd() {
		return trn_cd;
	}
	public void setTrn_cd(String trn_cd) {
		this.trn_cd = trn_cd;
	}
	public String getBsnsdt() {
		return bsnsdt;
	}
	public void setBsnsdt(String bsnsdt) {
		this.bsnsdt = bsnsdt;
	}
	public String getTmmbr() {
		return tmmbr;
	}
	public void setTmmbr(String tmmbr) {
		this.tmmbr = tmmbr;
	}
	public String getFmmbr() {
		return fmmbr;
	}
	public void setFmmbr(String fmmbr) {
		this.fmmbr = fmmbr;
	}
	public String getAmtto() {
		return amtto;
	}
	public void setAmtto(String amtto) {
		this.amtto = amtto;
	}
	public String getAmtf() {
		return amtf;
	}
	public void setAmtf(String amtf) {
		this.amtf = amtf;
	}
	public String getReltrn() {
		return reltrn;
	}
	public void setReltrn(String reltrn) {
		this.reltrn = reltrn;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getToacc() {
		return toacc;
	}
	public void setToacc(String toacc) {
		this.toacc = toacc;
	}
	public String getFracc() {
		return fracc;
	}
	public void setFracc(String fracc) {
		this.fracc = fracc;
	}
	public String getContractno() {
		return contractno;
	}
	public void setContractno(String contractno) {
		this.contractno = contractno;
	}
	public String getCust_ac_no() {
		return cust_ac_no;
	}
	public void setCust_ac_no(String cust_ac_no) {
		this.cust_ac_no = cust_ac_no;
	}
	public String getType_trns() {
		return type_trns;
	}
	public void setType_trns(String type_trns) {
		this.type_trns = type_trns;
	}
	
}
