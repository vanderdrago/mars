package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.SqlFunction;

public class LapPerubahanDataCifAct extends org.apache.struts.action.Action{
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(LapPerubahanDataCifAct.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);
		if (!SUCCESS.equals("success")){
			return mapping.findForward(SUCCESS);
		}
		try {
			List lphLapCif = new ArrayList();
			String noCif = request.getParameter("cif");
			String branch = request.getParameter("branch");
			SqlFunction sql = new SqlFunction();
			int noOfRecords = 0;
		    int sawal = 1;
		    int sakhir = 10; 
		    int page = 1; 
		    int noOfPages = 0; 
		    int recordsPerPage = 30;
		    
		    noOfRecords = sql.getTotLapPerubahanCif(noCif,branch);
			if (noOfRecords > 0){
				noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
				if (noOfPages < sakhir){
					sakhir = noOfPages;
				}
				lphLapCif = sql.getLapPerubahanCif(noCif,branch,sawal - 1,recordsPerPage);
				request.setAttribute("sawalCif", sawal); 
				request.setAttribute("sakhirCif", sakhir); 
				request.setAttribute("lphLapCif", lphLapCif); 
				request.setAttribute("noOfRecordsCif", noOfRecords);
				request.setAttribute("noOfPagesCif", noOfPages); 
				request.setAttribute("currentPageCif", page); 
				request.getSession(true).setAttribute("pageCif", page); 
				request.getSession(true).setAttribute("llsp", lphLapCif); 
				request.getSession(true).setAttribute("recordsCif", noOfRecords);
				request.getSession(true).setAttribute("noOfPagesCif", noOfPages);
				request.getSession(true).setAttribute("cif", noCif); 
				request.getSession(true).setAttribute("branch", branch); 
//				request.setAttribute("cif", noCif);
//				request.getSession(true).setAttribute("cif", noCif);
			}
		} catch (Exception e) {
			 request.setAttribute("errpage", "Error : " + e.getMessage()); 
			  return mapping.findForward("failed"); 
		}
		return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
	}
}
