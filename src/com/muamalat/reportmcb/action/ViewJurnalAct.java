/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.Bukubesar;
import com.muamalat.reportmcb.entity.Gl;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.function.FileFunction;
import com.muamalat.reportmcb.function.SqlFunction;

/**
 *
 * @author Utis
 */
public class ViewJurnalAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewJurnalAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
//        try{
            request.setAttribute("viewccy", request.getParameter("val").toUpperCase().trim());
            List lbukbes = new ArrayList();
            SqlFunction f = new SqlFunction();
            String opt = request.getParameter("btn");
            String acc = request.getParameter("acount");
            String brnch = request.getParameter("kdcab");
            String ccy = request.getParameter("val");
            String tgl1 = request.getParameter("tgl1");
            String tgl2 = request.getParameter("tgl2");
            String cbsrekoto = request.getParameter("cbsrekoto");
            request.getSession(true).setAttribute("saldoakhir", null);
            request.getSession(true).setAttribute("sdawal", null);
            //penambahan allcab-allval, AGUSTUS 2016
            String allcab = request.getParameter("allcab");
            String allval = request.getParameter("allval");
			
            int sawal = 1;
            int sakhir = 10;
            int page = 1;
            String title = "";

//			Saldo sd = f.getSaldo(acc, brnch, ccy, tgl1);
			Saldo sd = f.getSaldo(acc, brnch, ccy, tgl1, allcab, allval);
        	Gl gl = f.getGl(acc);
        	
        	if (gl != null){
            	if("Download".equals(opt)){
            		if (sd != null) {
            			System.out.println("masuk if sd");
                  		String filename = user.getUsername() + "_" + acc + ".txt";
//LINUX                 String pathfile = "/opt/temp/" + filename;
						String pathfile = "D:\\" + filename;
                        FileFunction ff = new FileFunction();
//                     	ff.createFile(pathfile, sd, gl, acc, ccy, brnch, tgl1, tgl2, cbsrekoto);
						ff.createFile(pathfile, sd, gl, acc, ccy, brnch, tgl1, tgl2, cbsrekoto, allcab, allval);
                          File fl = new File(pathfile);
                          System.out.println(fl);
                          if (fl.exists()) {
                              try {
                              	int length = 0;
                                  ServletOutputStream op = response.getOutputStream();
                                  response.reset();
                                  ServletContext context = servlet.getServletContext();
                                  String mimetype = context.getMimeType(StaticParameter.DOWNLOAD_FOLDER + "/" + filename);
                                  response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
                                  response.setContentLength((int) fl.length());
                                  response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                                  byte[] bbuf = new byte[1024];
                                  DataInputStream in = new DataInputStream(new FileInputStream(fl));

                                  while ((in != null) && ((length = in.read(bbuf)) != -1)) {
                                      op.write(bbuf, 0, length);
                                  }
                                  
                                  in.close();
                                  op.flush();
                                  op.close();
                              } catch (IOException ex) {
                                  //System.out.println("error in donwload : " + ex);
                              }
                              
                              ff.deleteFile(pathfile);
                          }
                          return mapping.findForward("blank");
                  	} else {
                  		System.out.println("masuk else sd");
                          return mapping.findForward("blank");
                  	}

                	
                } else {
                	int recordsPerPage = 30;
                    
                    if (sd != null) {
                    	sd.setAc_name(gl.getGl_desc());
                        request.setAttribute("saldo", sd);
                        request.getSession(true).setAttribute("saldo", sd);
                        BigDecimal sdawal = new BigDecimal(0);
                        if("IDR".equals(ccy.toUpperCase())){
                        	sdawal = sd.getLcy_opening_bal();
                        } else {
                        	sdawal = sd.getAcy_opening_bal();
                        }
//                      lbukbes = f.getcontohbuku2(tgl1, tgl2, acc, brnch, ccy, 1, 30, cbsrekoto, sdawal);
						lbukbes = f.getcontohbuku2(tgl1, tgl2, acc, brnch, ccy, 1, 30, cbsrekoto, sdawal, allcab, allval);
                        request.getSession(true).setAttribute("cbsrekoto", null);
                        request.getSession(true).setAttribute("cbsrekoto", cbsrekoto);
                        //penambahan allcab-allval, AGUSTUS 2016
                        request.getSession(true).setAttribute("allcab", null);
                        request.getSession(true).setAttribute("allcab", allcab);
                        request.getSession(true).setAttribute("allval", null);
                        request.getSession(true).setAttribute("allval", allval);
						
						if (lbukbes.size() > 0) {
                            int noOfRecords = 0;
                            int noOfPages = 0;
                            Bukubesar bukbes = null;
//                          bukbes = f.getnoOfRecords(tgl1, tgl2, acc, brnch, ccy, cbsrekoto);
							bukbes = f.getnoOfRecords(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, allcab, allval);
                            noOfRecords = bukbes.getTotrow();
                            noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                            if (noOfPages < sakhir){
                            	sakhir = noOfPages;
                            }
//                        	BigDecimal tsldo = sdawal.add(f.getSaldo2(tgl1, tgl2, acc, brnch, ccy, 1, noOfRecords, cbsrekoto));
							BigDecimal tsldo = sdawal.add(f.getSaldo2(tgl1, tgl2, acc, brnch, ccy, 1, noOfRecords, cbsrekoto, allcab, allval));
                            System.out.println("sdawal => " +sdawal);
                        	System.out.println("sdakhir => " +f.getSaldo2(tgl1, tgl2, acc, brnch, ccy, 1, noOfRecords, cbsrekoto, allcab, allval));                        	
                        	System.out.println("SALDO AKHIR (tsldo) => " +tsldo);
							
							request.setAttribute("sawal", sawal);
                            request.setAttribute("sakhir", sakhir);
                            request.getSession(true).setAttribute("tgl1", tgl1);
                            request.getSession(true).setAttribute("tgl2", tgl2);
                            request.getSession(true).setAttribute("acount", acc);
                            request.getSession(true).setAttribute("kdcab", brnch);
                            request.getSession(true).setAttribute("val", ccy);
                            request.setAttribute("lbukbes", lbukbes);
                            request.setAttribute("bukbes", bukbes);
                            request.setAttribute("noOfRecords", noOfRecords);
                            request.setAttribute("noOfPages", noOfPages);
                            request.setAttribute("currentPage", page);
                            request.getSession(true).setAttribute("bukbes", bukbes);
                            request.getSession(true).setAttribute("sdawal", sdawal);
                            request.getSession(true).setAttribute("saldoakhir", tsldo);
                            request.getSession(true).setAttribute("noOfRecords", noOfRecords);
                            request.getSession(true).setAttribute("noOfPages", noOfPages);
                        }
                    }
                }
              	title = "BUKU BESAR <br /> " + tgl1 + " - " + tgl2;
                request.setAttribute("title", title);
                request.getSession(true).setAttribute("title", title);
                request.setAttribute("view", "data");
        	} else {
                request.setAttribute("teks", "GL " + acc + " tidak ditemukan");  
                request.setAttribute("view", "nodata");      		
        	}
            return mapping.findForward(SUCCESS);
//        }
//        catch(Exception e){
//        	System.out.println("e.getMessage1():"+ e.getMessage());
//        	request.setAttribute("errpage", "Error : " + e.getMessage());
//            return mapping.findForward("failed");        	
//        }
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
