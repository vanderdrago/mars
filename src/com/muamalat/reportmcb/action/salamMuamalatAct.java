package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.salamMuamalatSql;

public class salamMuamalatAct extends org.apache.struts.action.Action{
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(salamMuamalatAct.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		try {
			request.getSession(true).setAttribute("noOfRecords", null);
			salamMuamalatSql sql = new salamMuamalatSql();
			String no_kartu = request.getParameter("no_kartu");
			String no_rek = request.getParameter("no_rek");
			String nama = request.getParameter("nama");
			String lahir = request.getParameter("lahir");
			
			int noOfRecords = 0;
			int sawal = 1;
			int sakhir = 10; 
			int page = 1; 
			int noOfPages = 0; 
			int recordsPerPage = 30; 
			  
			noOfRecords = sql.GetTotSalam(no_kartu,no_rek,nama.toUpperCase(),lahir);
			if (noOfRecords > 0){
				noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
				if (noOfPages < sakhir) {
					  sakhir = noOfPages;
				}
				List lphSalamMuamalat = sql.getSalamMuamalat(no_kartu,no_rek,nama.toUpperCase(),lahir,sawal,recordsPerPage);
				request.setAttribute("sawalSalam", sawal);
				request.setAttribute("sakhirSalam", sakhir);
				request.setAttribute("lphSalamMuamalat", lphSalamMuamalat);
				request.setAttribute("noOfRecordsSalam", noOfRecords);
			    request.setAttribute("noOfPagesSalam", noOfPages);
			    request.setAttribute("currentPageSalam", page);
			    request.getSession(true).setAttribute("PageSalam", page);
			    request.getSession(true).setAttribute("llsp", lphSalamMuamalat); 
			    request.getSession(true).setAttribute("recordsSalam", noOfRecords);
			    request.getSession(true).setAttribute("OfPagesSalam", noOfPages);
			    request.getSession(true).setAttribute("no_kartu", no_kartu); 
			    request.getSession(true).setAttribute("no_rek", no_rek); 
			    request.getSession(true).setAttribute("nama", nama); 
			    request.getSession(true).setAttribute("lahir", lahir); 
			}
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "salamMuamalat.do")){
			List lphSalamMuamalat = (List) request.getSession(true).getAttribute("lphSalamMuamalat");
			request.setAttribute("lphSalamMuamalat", lphSalamMuamalat);
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
}
