/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.MonSqlFunctionSkn;

/**
 *
 * @author Utis
 */
public class ViewMonSKNInAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewMonSKNInAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        request.getSession(true).setAttribute("refno", null);
        request.getSession(true).setAttribute("benacc", null);
        request.getSession(true).setAttribute("amont_a", null);
        request.getSession(true).setAttribute("amont_b", null);
        
        String refno = request.getParameter("refno");
        String benacc = request.getParameter("benacc");
        String amont_a = request.getParameter("amont_a");
		String amont_b = request.getParameter("amont_b");
		//ltrn = f.getskninList(sDate);
		
		List ltrn = new ArrayList();
        MonSqlFunctionSkn f = new MonSqlFunctionSkn();
        
		
		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String sDate = formatter.format(today);
		//sDate = "20130703";
		
		SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
		String sDate1 = formatter1.format(today);
		//sDate1 = "03/07/2013";
		
		ltrn = f.getskninList(sDate);
		
		if (ltrn.size() > 0) {
			ltrn = f.searchskninList(sDate, sDate1, refno, benacc, amont_a, amont_b);
		}
        if (ltrn.size() == 0) {
			String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
            request.setAttribute("dataresponse", dataresponse);
            request.setAttribute("konfirmasi", "err");
        }
        request.setAttribute("ltrn", ltrn);
        request.getSession(true).setAttribute("ltrn", ltrn);
        
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
