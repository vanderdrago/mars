package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class KPR10act extends org.apache.struts.action.Action{
	
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(KPR10act.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try {
			
			String nama = request.getParameter("nama");
			String Dplafond = request.getParameter("plafond");
			String tanggal = request.getParameter("tgl");
		    int tenor = 120;
		    
		    double plafond = Double.parseDouble(Dplafond);
		    double Pertama = (plafond * ((5.00/100/12) / (1 - (1/Math.pow((1+(5.00/100/12)), tenor)))));
		    double Kedua = Pertama * 160 / 100;
		    double ketiga = ((plafond * ((12.75/100/12) / (1 - (1/Math.pow((1+(12.75/100/12)), tenor))))) * tenor);
		    double pembagi = 24;
		    
		    BigDecimal resultPlafond = new BigDecimal(plafond);
		    BigDecimal rumusAngsuran = new BigDecimal(pembagi);
			BigDecimal angsuranPertama = new BigDecimal(Pertama);
			BigDecimal angsuranKedua = new BigDecimal(Kedua);
			BigDecimal angsuranKetiga = new BigDecimal(0);
			BigDecimal TotalAngsuranPertama = new BigDecimal(0);
			BigDecimal TotalAngsuranKedua = new BigDecimal(0);
			BigDecimal TotalMargin = new BigDecimal(0);
			BigDecimal TotalAngsuran = new BigDecimal(ketiga);
			TotalMargin = TotalAngsuran.subtract(resultPlafond);
			angsuranPertama = angsuranPertama.setScale(2, RoundingMode.HALF_UP);
			angsuranKedua = angsuranKedua.setScale(2, RoundingMode.HALF_UP);
			TotalAngsuran = TotalAngsuran.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal resultOs = resultPlafond;
			BigDecimal resultAngsPertama = angsuranPertama; 
			
			BigDecimal resultPokokPertama = new BigDecimal(0);
			BigDecimal resultMarginPertama = new BigDecimal(0);
			BigDecimal resultAngsuranPertama = new BigDecimal(0);
			BigDecimal resultPokokKedua = new BigDecimal(0);
			BigDecimal resultMarginKedua = new BigDecimal(0);
			BigDecimal resultPokokKetiga = new BigDecimal(0);
			BigDecimal resultMarginKetiga = new BigDecimal(0);
			BigDecimal resultPokokKeempat = new BigDecimal(0);
			BigDecimal resultMarginKeempat = new BigDecimal(0);
			BigDecimal resultPokokKelima= new BigDecimal(0);
			BigDecimal resultMarginKelima = new BigDecimal(0);
			
			BigDecimal sumPertama = new BigDecimal(0);
			BigDecimal sumKedua = new BigDecimal(0);
			BigDecimal sumKetiga = new BigDecimal(0);
			BigDecimal sumKeempat = new BigDecimal(0);
			BigDecimal TotalSumPertama = new BigDecimal(0);
			BigDecimal TotalSumKedua = new BigDecimal(0);
			BigDecimal TotalSumKetiga = new BigDecimal(0);
			
			List<HashMap> map = new ArrayList<HashMap>();
			
			for (int i = 1; i < tenor + 1; i++)
			{
				if (i < 13){
					HashMap temp = new HashMap();
					double rumusMargin = (11.50/100/12);
					BigDecimal rumus = new BigDecimal(rumusMargin);
					
					resultAngsuranPertama = resultAngsPertama;
					resultMarginPertama = resultOs.multiply(rumus);
					resultMarginPertama = resultMarginPertama.setScale(2, RoundingMode.HALF_UP);
					resultPokokPertama = resultAngsuranPertama.subtract(resultMarginPertama);
					resultOs = resultOs.subtract(resultPokokPertama);
					sumPertama = sumPertama.add(resultAngsuranPertama);
					
					temp.put("AngsuranPertama", resultAngsuranPertama);
					temp.put("MarginPertama", resultMarginPertama);
					temp.put("PokokPertama", resultPokokPertama);
					temp.put("OsPertama", resultOs);
					map.add(temp);
					
					System.out.println("Bulan :" + i + "Os :" + resultOs + "Pokok :" + resultPokokPertama + "Margin :" + resultMarginPertama + "Angsuran :" + resultAngsuranPertama);
				}
			}
			
			BigDecimal resultOsKedua = resultOs;
			BigDecimal tempOsKedua = resultOsKedua;
			BigDecimal resultAngsuranKedua = resultAngsuranPertama;
			System.out.println(resultOs);
			for (int i = 1; i < tenor + 1; i++) 
			{
				if (i > 12 && i < 25)
				{
					HashMap temp = new HashMap();
					double rumusMargin = (10.50/100/12);
					BigDecimal rumus = new BigDecimal(rumusMargin);
					
					resultMarginKedua = tempOsKedua.multiply(rumus);
					resultMarginKedua = resultMarginKedua.setScale(2, RoundingMode.HALF_UP);
					resultPokokKedua = resultAngsuranKedua.subtract(resultMarginKedua);
					tempOsKedua = tempOsKedua.subtract(resultPokokKedua);
					sumKedua = sumKedua.add(resultAngsuranKedua);
					
					temp.put("AngsuranKedua", resultAngsuranKedua);
					temp.put("MarginKedua", resultMarginKedua);
					temp.put("PokokKedua", resultPokokKedua);
					temp.put("OsKedua", tempOsKedua);
					map.add(temp);
					
					System.out.println("Bulan :" + i + "Os :" + tempOsKedua + "Pokok :" + resultPokokKedua + "Margin :" + resultMarginKedua + "Angsuran :" + resultAngsuranKedua);
				}
			}
			
			BigDecimal resultOsKetiga = tempOsKedua;
			BigDecimal tempOsKetiga = resultOsKetiga;
			BigDecimal resultAngsuranKetiga = resultAngsuranPertama;
			
			for (int i = 1; i < tenor + 1; i++) 
			{
				if (i > 24 && i < 73)
				{
					HashMap temp = new HashMap();
					double rumusMargin = (10.00/100/12);
					BigDecimal rumus = new BigDecimal(rumusMargin);
					
					resultMarginKetiga = tempOsKetiga.multiply(rumus);
					resultMarginKetiga = resultMarginKetiga.setScale(2, RoundingMode.HALF_UP);
					resultPokokKetiga = resultAngsuranKetiga.subtract(resultMarginKetiga);
					tempOsKetiga = tempOsKetiga.subtract(resultPokokKetiga);
					sumKetiga = sumKetiga.add(resultAngsuranKetiga);
					
					temp.put("AngsuranKetiga", resultAngsuranKetiga);
					temp.put("MarginKetiga", resultMarginKetiga);
					temp.put("PokokKetiga", resultPokokKetiga);
					temp.put("OsKetiga", tempOsKetiga);
					map.add(temp);
					System.out.println("Bulan :" + i + "Os :" + tempOsKetiga + "Pokok :" + resultPokokKetiga + "Margin :" + resultMarginKetiga + "Angsuran :" + resultAngsuranKetiga);
				}
			}
			
			System.out.println(tempOsKetiga);
			TotalSumPertama = sumPertama.add(sumKedua.add(sumKetiga));
			BigDecimal resultOsKeempat = tempOsKetiga;
			BigDecimal tempOsKeempat = resultOsKeempat;
			BigDecimal resultAngsuranKeempat = angsuranKedua;
			
			for (int i = 1; i < tenor + 1; i++) 
			{
				if (i > 72 && i < 97)
				{
					HashMap temp = new HashMap();
					double rumusMargin = (10.00/100/12);
					BigDecimal rumus = new BigDecimal(rumusMargin);
					
					resultMarginKeempat = tempOsKeempat.multiply(rumus);
					resultMarginKeempat = resultMarginKeempat.setScale(2, RoundingMode.HALF_UP);
					resultPokokKeempat = resultAngsuranKeempat.subtract(resultMarginKeempat);
					tempOsKeempat = tempOsKeempat.subtract(resultPokokKeempat);
					sumKeempat = sumKeempat.add(resultAngsuranKeempat);
					
					temp.put("AngsuranKeempat", resultAngsuranKeempat);
					temp.put("MarginKeempat", resultMarginKeempat);
					temp.put("PokokKeempat", resultPokokKeempat);
					temp.put("OsKeempat", tempOsKeempat);
					map.add(temp);
					System.out.println("Bulan :" + i + "Os :" + tempOsKeempat + "Pokok :" + resultPokokKeempat + "Margin :" + resultMarginKeempat + "Angsuran :" + resultAngsuranKeempat);
				}
			}
			
			TotalSumKedua = sumKeempat;
			TotalSumKetiga = TotalAngsuran.subtract(TotalSumPertama.add(TotalSumKedua));
			angsuranKetiga = TotalSumKetiga.divide(rumusAngsuran, BigDecimal.ROUND_HALF_UP);
			
			BigDecimal resultOsKelima = tempOsKeempat;
			BigDecimal tempOsKelima = resultOsKelima;
			BigDecimal resultAngsuranKelima = angsuranKetiga;
			
			for (int i = 1; i < tenor + 1; i++) 
			{
				if (i > 96)
				{
					HashMap temp = new HashMap();
					double rumusMargin = (10.839181/100/12);
					BigDecimal rumus = new BigDecimal(rumusMargin);
					
					resultMarginKelima = tempOsKelima.multiply(rumus);
					resultMarginKelima = resultMarginKelima.setScale(2, RoundingMode.HALF_UP);
					resultPokokKelima = resultAngsuranKelima.subtract(resultMarginKelima);
					tempOsKelima = tempOsKelima.subtract(resultPokokKelima);
					
					temp.put("AngsuranKelima", resultAngsuranKelima);
					temp.put("MarginKelima", resultMarginKelima);
					temp.put("PokokKelima", resultPokokKelima);
					temp.put("OsKelima", tempOsKelima);
					map.add(temp);
//					System.out.println("Bulan :" + i + "Os :" + tempOsKelima + "Pokok :" + resultPokokKelima + "Margin :" + resultMarginKelima + "Angsuran :" + resultAngsuranKelima);
				}
			}
			
			
			
			for (HashMap temp : map){
				
			}

			request.getSession(true).setAttribute("nama", nama);
			request.getSession(true).setAttribute("plafond", plafond);
			request.getSession(true).setAttribute("jangkaWaktu", tenor);
			request.getSession(true).setAttribute("tanggal", tanggal);
			request.getSession(true).setAttribute("TotalPokok", resultPlafond);
			request.getSession(true).setAttribute("TotalAngsuran", TotalAngsuran);
			request.getSession(true).setAttribute("TotalMargin", TotalMargin);
			request.getSession(true).setAttribute("AngsuranPertama", resultAngsuranPertama);
			request.getSession(true).setAttribute("AngsuranKedua", resultAngsuranKeempat);
			request.getSession(true).setAttribute("AngsuranKetiga", resultAngsuranKelima);
			request.setAttribute("kpr", map);
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
		
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		System.out.println("lrole :" + lrole);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "KPR10th.do")){
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
	
	
}
