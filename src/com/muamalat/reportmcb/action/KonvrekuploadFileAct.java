package com.muamalat.reportmcb.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jboss.logging.Logger;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.bean.UploadFileKonvrekBean;
import com.muamalat.reportmcb.function.FileFunction;
import com.muamalat.reportmcb.function.FileUploadFunction;
import com.muamalat.reportmcb.parameter.Parameter;

public class KonvrekuploadFileAct extends Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(KonvrekuploadFileAct.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);

		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}
		
            try {        	
            	UploadFileKonvrekBean uploadFile = (UploadFileKonvrekBean) form;
                if (uploadFile != null) {
                    String filename = uploadFile.getTheFile().getFileName();
                    String ext_filename1 = filename.substring(filename.length() - 3, filename.length());
                    if ("xls".equals(ext_filename1.toLowerCase())) {
                    	FileFunction ff = new FileFunction();
                    	if(ff.containsWhitespace(filename)){
                    		request.setAttribute("datanotok", "Nama File tidak boleh ada karakter spasi.");
    	                    request.setAttribute("view", "notok");
                        } else {
                        	FileUploadFunction file = new FileUploadFunction();
		                	file.setFileName(uploadFile.getTheFile().getFileName());
		                    file.setIn(uploadFile.getTheFile().getInputStream());
		                    byte[] fileData = uploadFile.getTheFile().getFileData();
//		                    ff.deleteFile(Parameter.READFILE + file.getFileName());                    
		                    ff.saveFile(Parameter.READFILE, fileData, file.getFileName());
		                    request.setAttribute("respon", "Nama File : " + filename);   
		                    request.getSession(true).setAttribute("filename", filename);
		                    request.setAttribute("conf", "resp");
                        }
                    } else {
                        request.setAttribute("datanotok", "Format file harus *.xls.");
                        request.setAttribute("view", "notok");
                    }
                } else {
                    request.setAttribute("datanotok", "UploadFileBean null.");
                    return mapping.findForward("errPage"); 
                }       
                return mapping.findForward(SUCCESS);
            } catch (Exception e) {
                request.setAttribute("respon", "Error in UploadFileAction : " + e.getMessage());
                log.info("Error in UploadFileAction : " + e.getMessage());
                return mapping.findForward("errPage"); 
            }
	}

	private String isValidUser(HttpServletRequest request) {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,
					validLevel);
			request.setAttribute("message",
					"Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(
				StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5
				|| level == 6 || level == 7) {
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error",
					"Anda tidak memiliki akses ke halaman yang diminta!");
			return "msgpage";
		}
	}
}
