package com.muamalat.reportmcb.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.function.SqlFunction;

public class DetailCustomAct extends org.apache.struts.action.Action {

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(DetailCustomAct.class);

	/**
	 * Action called on save button click
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SUCCESS = isValidUser(request);

		Admin user = (Admin) request.getSession(true).getAttribute(
				StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")) {

			return mapping.findForward(SUCCESS);
		}

		 try{
		request.setAttribute("lbd", null);
		request.getSession(true).setAttribute("lbd", null);
		SqlFunction f = new SqlFunction();
		String norek = (String) request.getParameter("norek");
		SttmCustAccount DetailCustom = f.getDetailCustom(norek, "");
		if (DetailCustom != null) {
			request.setAttribute("DetailCustom", DetailCustom);			
//			request.setAttribute("viewcustomer", "data");
			
						
		}
		
		SUCCESS = "success";
			
		return mapping.findForward(SUCCESS);
		  } catch (Exception e){
	        	request.setAttribute("errpage", "Error : " + e.getMessage());
	            return mapping.findForward(".errPage");
	        }
		
	}

	
	private String isValidUser(HttpServletRequest request) {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,
					validLevel);
			request.setAttribute("message",
					"Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(
				StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5
				|| level == 6 || level == 7) {
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error",
					"Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
		}

	}

}
