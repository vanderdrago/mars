package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.SaldoRataRata;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class peragaanSaldoAct extends org.apache.struts.action.Action {
	/* forward name="success" path="" */
	private static Logger log = Logger.getLogger(peragaanSaldoAct.class);
	private static String SUCCESS = "success";

	/** Action called on save button click
     */
	
	
	  public ActionForward execute(ActionMapping mapping, ActionForm form,
	  HttpServletRequest request, HttpServletResponse response) throws
	  Exception {
	  
	  SUCCESS = isValidUser(request);
	  
	  Admin user = (Admin)
	  request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
	  
	  if (!SUCCESS.equals("success")) { 
		  return mapping.findForward(SUCCESS); 
	  }
	  try{ 
		  List lphSaldo = new ArrayList(); 
		  SqlFunction sql = new SqlFunction();
		  String acc = request.getParameter("acc");
		  String nama = request.getParameter("nama");
		  System.out.println("acc :" + acc);

		  int noOfRecords = 0; 
		  int sawal = 1;
		  int sakhir = 10; 
		  int page = 1; 
		  int noOfPages = 0; 
		  int recordsPerPage = 100; 
		  System.out.println("record :" + noOfRecords);
		  noOfRecords = sql.getTotalSaldo(acc, nama.toUpperCase());
		  System.out.println("record :" + noOfRecords);
		  if (noOfRecords > 0) {
			  noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
			  if (noOfPages < sakhir) {
				  sakhir = noOfPages;
			  }
			  lphSaldo = sql.getSaldoRatarata(acc, nama.toUpperCase(), sawal - 1);
			  System.out.println("size :" + lphSaldo.size());
			  if (lphSaldo.size() > 0){
				  System.out.println("if");
				  request.setAttribute("sawalSaldo", sawal);
				  request.setAttribute("sakhirSaldo", sakhir);
				  request.setAttribute("lphSaldo", lphSaldo); 
				  request.setAttribute("noOfRecordsSaldo", noOfRecords);
				  request.setAttribute("noOfPagesSaldo", noOfPages);
				  request.setAttribute("currentPageSaldo", page);
				  request.getSession(true).setAttribute("PageSaldo", page);
				  request.getSession(true).setAttribute("llsp", lphSaldo); 
				  request.getSession(true).setAttribute("recordsSaldo", noOfRecords);
				  request.getSession(true).setAttribute("OfPagesSaldo", noOfPages);
				  request.getSession(true).setAttribute("acc", acc); 
				  request.getSession(true).setAttribute("nama", nama); 
			  } 
		  } else {
			  System.out.println("else");
			  request.setAttribute("confrm","err");
        	  request.setAttribute("respon", "No Rekening Tidak ada Hubungi Pihak Terkait");
		  }
		  return  mapping.findForward(SUCCESS); 
	   } catch (Exception e) {
		   request.setAttribute("errpage", "Error : " + e.getMessage()); 
		   return mapping.findForward("failed"); 
	   }
	}
	  
	  private String isValidUser(HttpServletRequest request) {
		  int level; int validLevel = 5; 
		  if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
			  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
			  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
			  return "loginpage"; 
		  }
	  
		  level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
		  if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
			  return "success";
		  }else { 
			  request.setAttribute("message", "Unauthorized Access!");
			  request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			  request.setAttribute("error_code", "Error_timeout"); return "msgpage";
		  }
	  }
	}
