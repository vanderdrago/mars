package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.salamMuamalat;
import com.muamalat.reportmcb.function.SqlFunctionNew;
import com.muamalat.reportmcb.function.salamMuamalatSql;

public class detailKartu extends org.apache.struts.action.Action{
	
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(detailKartu.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        List lsql = new ArrayList();
        salamMuamalatSql sql = new salamMuamalatSql();
        String noKartu = request.getParameter("noKartu");
        String period = (String) request.getSession(true).getAttribute("period");
        	            
        System.out.println("noKartu :" + noKartu + "period :" + period);
        salamMuamalat salam = sql.getDetailSalam(noKartu);
        
        if (salam != null){
			salamMuamalat total = sql.getAngsuran(noKartu, period);
			if (total != null){
				List<HashMap> MapResult = new ArrayList<HashMap>();
				DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
				DecimalFormatSymbols format = new DecimalFormatSymbols();
				format.setMonetaryDecimalSeparator(',');
				format.setGroupingSeparator('.');
				df.setDecimalFormatSymbols(format);
				
				HashMap map = new HashMap();
				BigDecimal osSaatini = new BigDecimal(0);
				BigDecimal angsuranBulanan = new BigDecimal(0);
				BigDecimal angsuranTerakhir = new BigDecimal(0);
				BigDecimal tunggakanAngsuran = new BigDecimal(0);
				BigDecimal denda = new BigDecimal(0);
				BigDecimal tunggakanDenda = new BigDecimal(0);
				
				map.put("osSaatini",df.format(salam.getPembiayaan()));
				map.put("angsuranBulanan",df.format(total.getTotal_angsuran()));
				map.put("angsuranTerakhir",df.format(total.getAngsuran_terakhir()));
				map.put("tunggakanAngsuran",df.format(salam.getTunggakan_angsuran()));
				map.put("denda", df.format(salam.getDenda()));
				map.put("tunggakanDenda",df.format(salam.getTunggakan_denda()));
				request.setAttribute("saldo", MapResult);
				request.setAttribute("total", total);
				MapResult.add(map);
				System.out.println("OS :" + map.get("osSaatini"));
			}
			request.setAttribute("salam", salam);
			return mapping.findForward(SUCCESS);
		} else {
			request.setAttribute("message", "NO DATA!");
			request.setAttribute("message_error", "ERROR NO DATA!");
			return mapping.findForward("msgpage");
		}
        
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
	}

    
}
