/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author Trio Kwek-kwek
 */
public class ViewIbRTGSAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewIbRTGSAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        String trfType = "2";   
        String refno = (String) request.getParameter("refno"); 
        String fromacc = (String) request.getParameter("fromacc");        
        String toacc = (String) request.getParameter("toacc");         
        String amont_a = (String) request.getParameter("amont_a");
		String amont_b = (String) request.getParameter("amont_b"); 
        
        List ltrn = new ArrayList();
        MonSqlFunction f = new MonSqlFunction();
        
        Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = formatter.format(today);
		//sDate = "13-05-2013";
		
		if ((refno != null) || (fromacc != null) || (toacc != null) || (amont_a != null) || (amont_b != null)){
        	String tempsql = "";         	
        	if ((refno == null || "".equals(refno)) && (fromacc == null || "".equals(fromacc)) && (toacc == null || "".equals(toacc)) && (amont_a == null || "".equals(amont_a)) && (amont_b == null || "".equals(amont_b))){
        		tempsql = " where d.TRANSFER_TYPE= '"+trfType+"' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
        	} else {
        		if (refno == null) {
        			refno = "";
            		}
        		if (fromacc == null) {
        			fromacc = "";
            		}
            	if (toacc == null) {
            		toacc = "";
            		}
            	if (amont_a == null) {
            		amont_a = "";
            		}
            	if (amont_b == null) {
            		amont_b = "";
            		}
            	tempsql = " where (t.REF_NO = '" + refno.trim()
            			+ "' or d.ACCOUNT_NO = '" + fromacc.trim()
						+ "' or d.TO_ACCOUNT_NO = '" + toacc.trim()
						+ "' or (d.AMOUNT = '" + amont_a.trim()
						+ "' and d.AMOUNT = '" + amont_b.trim()
						+ "')) and d.TRANSFER_TYPE = '" + trfType.trim()
						+ "' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
        	}
        	ltrn = f.searchIbRtgsMcb(tempsql + " order by d.TO_NAME " );
        }
		if (ltrn.size() == 0) {
			String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
            request.setAttribute("dataresponse", dataresponse);
            request.setAttribute("konfirmasi", "err");
        }
        request.setAttribute("ltrn", ltrn);
        request.getSession(true).setAttribute("ltrn", ltrn);
        
        return mapping.findForward(SUCCESS);
    }


    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
