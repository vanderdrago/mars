/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.FinSqlFunction;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author Utis
 */
public class ViewGetEventAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewGetEventAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        List ltrn = new ArrayList(); 
        FinSqlFunction sql = new FinSqlFunction();
        String accno = request.getParameter("accno");
        
        int noOfRecords = 0;
        int sawal = 1;
        int sakhir = 10; 
        int page = 1; 
        int noOfPages = 0; 
        int recordsPerPage = 30;
        
		noOfRecords = sql.TotEvent(accno);
		  if (noOfRecords > 0) {
			  noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
			  if (noOfPages < sakhir) {
				  sakhir = noOfPages;
			  }
			  ltrn = sql.getevent(accno,sawal,recordsPerPage);
			  request.setAttribute("sawalEvent", sawal); 
			  request.setAttribute("sakhirEvent", sakhir); 
			  request.setAttribute("ltrn", ltrn); 
			  request.setAttribute("noOfRecordsEvent", noOfRecords);
			  request.setAttribute("noOfPagesEvent", noOfPages); 
			  request.setAttribute("currentPageEvent", page); 
			  request.getSession(true).setAttribute("pageEvent", page); 
			  request.getSession(true).setAttribute("llsp", ltrn); 
			  request.getSession(true).setAttribute("recordsEvent", noOfRecords);
			  request.getSession(true).setAttribute("noOfPagesEvent", noOfPages);
			  request.getSession(true).setAttribute("accno", accno);
		  } return mapping.findForward(SUCCESS); 
        
//        request.getSession(true).setAttribute("accno", null);
//        
//		List ltrn = new ArrayList();
//		FinSqlFunction f = new FinSqlFunction();
//		String accno = request.getParameter("accno");
//        ltrn = f.getevent(accno);
//		
//		if (ltrn.size() > 0) {
//			ltrn = f.getevent(accno);
//		}
//		if (ltrn.size() == 0) {
//				String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
//	            request.setAttribute("dataresponse", dataresponse);
//	            request.setAttribute("konfirmasi", "err");
//		}
//        request.setAttribute("ltrn", ltrn);
//        request.getSession(true).setAttribute("ltrn", ltrn);
//        
//        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
