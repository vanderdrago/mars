/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.JoinRelTrnSKNMaster;
import com.muamalat.reportmcb.entity.PctbContractMaster;
import com.muamalat.reportmcb.entity.TRskn;
import com.muamalat.reportmcb.function.MonSqlFunction;
import com.muamalat.reportmcb.function.MonSqlFunctionSkn;

/**
 *
 * @author Utis-ds
 */
public class ViewMonJoinSKNAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewMonJoinSKNAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        //String intrn = request.getParameter("inputtrn");
        //String strtgs = request.getParameter("stsrtgs");
        
        String network = "SKNBI";        
        String rel_trn = (String) request.getParameter("rel_trn");        
        String rek_f_ac = (String) request.getParameter("rek_f_ac");         
        String rek_t_ac = (String) request.getParameter("rek_t_ac");
        String amont_a = (String) request.getParameter("amont_a");
		String amont_b = (String) request.getParameter("amont_b");
            
        Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String sDate = formatter.format(today);
		//sDate = "20130520";
		
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
		String sDate2 = formatter2.format(today);
		//sDate2 = "20-05-2013";
        
        MonSqlFunctionSkn f = new MonSqlFunctionSkn();
        MonSqlFunction fs = new MonSqlFunction();
		String fr = (String) request.getParameter("fr");
		List mcbltrn = new ArrayList();
        if ("f".equals(fr)) {
        	mcbltrn = f.searchMonSknMcb(sDate2, network, rel_trn, rek_f_ac, rek_t_ac, amont_a, amont_b);
        }else if ("u".equals(fr)) {
			String srv = (String) request.getParameter("srv");
			String sts = (String) request.getParameter("sts");
			mcbltrn = fs.searchMonRtgsMcbu(sDate2, network, srv, sts, "SOIB");
		}
        
        List joinltrn = new ArrayList(); 
        if (mcbltrn.size() > 0) {
            for (Iterator<PctbContractMaster> i = mcbltrn.iterator();
            i.hasNext();) 
            {
            	PctbContractMaster jmcb = i.next();
            	String whrstr = " where od_ref_no ='"+jmcb.getContractRefNo()+"' and od_accno = '"+jmcb.getCptyAcNo()+"'";
            	TRskn sintf = f.searchjoinskn(whrstr, sDate);
            	JoinRelTrnSKNMaster jTrnSKNM = new JoinRelTrnSKNMaster();
            	jTrnSKNM.setContractRefNo(jmcb.getContractRefNo());
            	jTrnSKNM.setProdRefNo(jmcb.getProdRefNo());
            	jTrnSKNM.setBookingDt(jmcb.getBookingDt());
            	jTrnSKNM.setCheckerId(jmcb.getCheckerId());
            	jTrnSKNM.setCptyAcNo(jmcb.getCptyAcNo());
            	jTrnSKNM.setCptyName(jmcb.getCptyName());
            	jTrnSKNM.setCustAcNo(jmcb.getCustAcNo());
            	jTrnSKNM.setCustName(jmcb.getCustName());
            	jTrnSKNM.setCustBankcode(jmcb.getCptyBankcode());
            	jTrnSKNM.setDispatchRefNo(jmcb.getDispatchRefNo());
            	jTrnSKNM.setExceptionQueue(jmcb.getExceptionQueue());
            	jTrnSKNM.setMakerId(jmcb.getMakerId());
            	jTrnSKNM.setNetwork(jmcb.getNetwork());
            	jTrnSKNM.setPayDetails(jmcb.getPaymentDetails1());
            	jTrnSKNM.setTxnAmount(jmcb.getTxnAmount());
            	jTrnSKNM.setUdf_2(jmcb.getUdf2());
            	
            	if (sintf != null ){
            		jTrnSKNM.setOd_ref_no(sintf.getOd_ref_no());
            		jTrnSKNM.setOd_bankno(sintf.getOd_bankno());
            		jTrnSKNM.setOd_op_name(sintf.getOd_op_name());
            		jTrnSKNM.setOd_accno(sintf.getOd_accno());
            		jTrnSKNM.setOd_cu_name(sintf.getOd_cu_name());
            		jTrnSKNM.setOd_desc(sintf.getOd_desc());
            		jTrnSKNM.setOd_amount(sintf.getOd_amount());
            		jTrnSKNM.setOd_status1(sintf.getOd_status1());
            		jTrnSKNM.setOd_status2(sintf.getOd_status2());
            	}else {
            		jTrnSKNM.setOd_ref_no("");
            		jTrnSKNM.setOd_bankno("");
            		jTrnSKNM.setOd_op_name("");
            		jTrnSKNM.setOd_accno("");
            		jTrnSKNM.setOd_cu_name("");
            		jTrnSKNM.setOd_desc("");
            		jTrnSKNM.setOd_amount(new BigDecimal(0));
            		jTrnSKNM.setOd_status1("");
            		jTrnSKNM.setOd_status2("");
	            }
            	joinltrn.add(jTrnSKNM);
            }
        }
        if (mcbltrn.size() == 0) {
			String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
            request.setAttribute("dataresponse", dataresponse);
            request.setAttribute("konfirmasi", "err");
        }        
        request.setAttribute("ltrn", joinltrn);
        request.getSession(true).setAttribute("ltrn", joinltrn);
        
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
