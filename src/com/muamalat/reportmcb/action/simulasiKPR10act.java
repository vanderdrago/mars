package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class simulasiKPR10act extends org.apache.struts.action.Action {
	
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(simulasiKPR10act.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		try {
			String nama = request.getParameter("nama");
			String plafond = request.getParameter("plafond");
			String jangkaWaktu = "120";
			String tgl = request.getParameter("tgl");
			
			int jangka = Integer.parseInt(jangkaWaktu);
			int amount = Integer.parseInt(plafond);
			int bulan = Integer.parseInt(jangkaWaktu);
			int i;
			double Dplafond = Double.parseDouble(plafond);
			
			//Nilai angsuran tahun 1-6 jika jangka waktu 10th
			double pengurang = (Dplafond * 0.4 /100);
			double rumus = ((12.25/100/12) * 100) / 100.0;
			double rumusPokok7 = (0.37);
			double rumusPokok9 = (0.63);
			double pembagi = 1 - (1/Math.pow((1+(12.25/100/12)), bulan));
			double total = (12.25/100/12) / pembagi; 
			double angsuran = (amount * total) - pengurang;
			double angsuran2 = (amount * total) * bulan;
			double tempAngsuran = angsuran;	
			double bunga = angsuran2 - Dplafond;
			
			BigDecimal DecimalrumusPokok7 = new BigDecimal(rumusPokok7);
			DecimalrumusPokok7 = DecimalrumusPokok7.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal DecimalrumusPokok9 = new BigDecimal(rumusPokok9);
			DecimalrumusPokok9 = DecimalrumusPokok9.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal DecimalPlafond = new BigDecimal(Dplafond);
			DecimalPlafond = DecimalPlafond.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal bigAngsuran = new BigDecimal(angsuran);
			bigAngsuran = bigAngsuran.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal bigAngsuran2 = new BigDecimal(angsuran2);
			bigAngsuran2 = bigAngsuran2.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal DecimaltempAngsuran = new BigDecimal(tempAngsuran);
			DecimaltempAngsuran = DecimaltempAngsuran.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal DecimalBunga = new BigDecimal(bunga);
			DecimalBunga = DecimalBunga.setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal DecimalTahun = new BigDecimal(48);
			BigDecimal DecimalTahun7 = new BigDecimal(24);
			
			List<HashMap> MapResult = new ArrayList<HashMap>();
			
			BigDecimal resultPlafond = DecimalPlafond;
			BigDecimal resultAngsuran = DecimaltempAngsuran;
			BigDecimal SisaPlafond = DecimalPlafond;
			BigDecimal resultSisa = DecimaltempAngsuran;
			
			BigDecimal tempSumAngsuran = new BigDecimal(0);
			BigDecimal tempSumMargin = new BigDecimal(0);
			BigDecimal tempSumAngPokok = new BigDecimal(0);
			BigDecimal tempSumAngsuran7 = new BigDecimal(0);
			BigDecimal tempSumMargin7 = new BigDecimal(0);
			BigDecimal tempSumAngPokok7 = new BigDecimal(0);
			BigDecimal tempSumAngsuran9 = new BigDecimal(0);
			BigDecimal tempSumMargin9 = new BigDecimal(0);
			BigDecimal tempSumAngPokok9 = new BigDecimal(0);
			BigDecimal sisaMargin = new BigDecimal(0);
			BigDecimal sisaPokok = new BigDecimal(0);
			BigDecimal marginTahunSisa = new BigDecimal(0);
			BigDecimal pokokTahun7 = new BigDecimal(0);
			BigDecimal pokokTahun9 = new BigDecimal(0);
			BigDecimal TempPokokTahun7 = new BigDecimal(0);
			BigDecimal TempPokokTahun9 = new BigDecimal(0);
			BigDecimal AngsuranTahun7 = new BigDecimal(0);
			BigDecimal AngsuranTahun9 = new BigDecimal(0);
			BigDecimal AngPokok7 = new BigDecimal(0);
			BigDecimal AngPokok9 = new BigDecimal(0);
			BigDecimal TotalSumAngPokok = new BigDecimal(0);
			BigDecimal TotalSumMargin = new BigDecimal(0);
			BigDecimal TotalSumAngsuran = new BigDecimal(0);
			BigDecimal rumusRate = new BigDecimal(1200);
			
			for (i = 1; i < jangka; i++){
				if (i < 73) {
					HashMap temp = new HashMap();
					double rumusMargin = (12.25/100/12);
					BigDecimal temprumusMargin = new BigDecimal(rumusMargin);
					BigDecimal Hashmargin = new BigDecimal(0);
					BigDecimal tempAngsPokok = new BigDecimal(0);
					BigDecimal rate = new BigDecimal(0);
					BigDecimal tempOutput = new BigDecimal(0);
					DecimalFormat format = new DecimalFormat("###,###.##");
					
					BigDecimal temptTotAng = resultAngsuran;
					BigDecimal tempPlafond = resultPlafond;
					BigDecimal tempAng = resultAngsuran;
					
					Hashmargin = tempPlafond.multiply(temprumusMargin);
					BigDecimal tempHashMargin = Hashmargin;
					tempAngsPokok = resultAngsuran.subtract(tempHashMargin);
					resultPlafond = resultPlafond.subtract(tempAngsPokok);
					
					tempHashMargin = tempHashMargin.setScale(0, RoundingMode.HALF_UP);
					tempPlafond = tempPlafond.setScale(0, RoundingMode.HALF_UP);
					
					String output = format.format(tempOutput);
					rate = tempHashMargin.divide(tempPlafond, 5, RoundingMode.HALF_UP).multiply(rumusRate);
					rate = rate.setScale(2, RoundingMode.HALF_UP);
					String rateFormat = String.valueOf(rate);
					rateFormat = (rateFormat + "%");
					
					BigDecimal DecimaltempHashMargin = Hashmargin;
					DecimaltempHashMargin = DecimaltempHashMargin.setScale(2, RoundingMode.HALF_UP);
					BigDecimal DecimaltempAngsPokok = tempAngsPokok;
					DecimaltempAngsPokok = DecimaltempAngsPokok.setScale(2, RoundingMode.HALF_UP);
					
					tempSumAngsuran = tempSumAngsuran.add(tempAng);
					tempSumMargin = tempSumMargin.add(DecimaltempHashMargin);
					tempSumAngPokok = tempSumAngPokok.add(DecimaltempAngsPokok);
					
					temp.put("resultPlafond", resultPlafond);
					temp.put("Angsuran_pokok", tempAngsPokok);
					temp.put("margin", tempHashMargin);
					temp.put("angsuran2", resultAngsuran);
					temp.put("rate", rateFormat);
					MapResult.add(temp);
				}
			}
			
			//Dari tahun 6-8
			sisaMargin = DecimalBunga.subtract(tempSumMargin);
			sisaPokok = DecimalPlafond.subtract(tempSumAngPokok);
			marginTahunSisa = sisaMargin.divide(DecimalTahun, 2, RoundingMode.HALF_UP);
			pokokTahun7 = sisaPokok.multiply(DecimalrumusPokok7);
			pokokTahun9 = sisaPokok.multiply(DecimalrumusPokok9);
			TempPokokTahun7 = pokokTahun7.divide(DecimalTahun7, 2, RoundingMode.HALF_UP);
			TempPokokTahun9 = pokokTahun9.divide(DecimalTahun7, 2, RoundingMode.HALF_UP);
			AngsuranTahun7 = TempPokokTahun7.add(marginTahunSisa);
			AngsuranTahun9 = TempPokokTahun9.add(marginTahunSisa);
			AngPokok7 = AngsuranTahun7.subtract(marginTahunSisa);
			AngPokok9 = AngsuranTahun9.subtract(marginTahunSisa);
			
			BigDecimal tempPokok = resultPlafond;
			BigDecimal tempSisaPlafond = tempPokok;
			BigDecimal DecimaltempSisaPlafond7 = tempSisaPlafond;
			DecimaltempSisaPlafond7 = DecimaltempSisaPlafond7.setScale(2, RoundingMode.HALF_UP);
			tempPokok = tempPokok.setScale(2, RoundingMode.HALF_UP);
			tempSisaPlafond = tempSisaPlafond.setScale(2, RoundingMode.HALF_UP);
			
			for (i = 1; i < jangka; i++){
				if (i > 71 && i < 96){
					HashMap temp = new HashMap();
					BigDecimal rate = new BigDecimal(0);
					BigDecimal tempOutput = new BigDecimal(0);
					DecimalFormat format = new DecimalFormat("###,###.##");
					
					String output = format.format(tempOutput);
					rate = marginTahunSisa.divide(DecimaltempSisaPlafond7, 5, RoundingMode.HALF_UP).multiply(rumusRate);
					rate = rate.setScale(2, RoundingMode.HALF_UP);
					String rateFormat = String.valueOf(rate);
					rateFormat = (rateFormat + "%");
					
					DecimaltempSisaPlafond7 = DecimaltempSisaPlafond7.subtract(AngPokok7);
					tempSumAngsuran7 = tempSumAngsuran7.add(AngsuranTahun7);
					tempSumMargin7 = tempSumMargin7.add(marginTahunSisa);
					tempSumAngPokok7 = tempSumAngPokok7.add(AngPokok7);
					
					temp.put("plafond7", DecimaltempSisaPlafond7);
					temp.put("AngPokok7", AngPokok7);
					temp.put("angsuran7", AngsuranTahun7);
					temp.put("marginTahunSisa", marginTahunSisa);
					temp.put("rateTahun7", rateFormat);
					MapResult.add(temp);
				} 
			}
			
			//Dari tahun 9-10
			BigDecimal tempSisaPlafond9 = DecimaltempSisaPlafond7;
			BigDecimal DecimaltempSisaPlafond9 = tempSisaPlafond9;
			BigDecimal tempPlafond9 = DecimaltempSisaPlafond9;
			tempSisaPlafond9 = tempSisaPlafond9.setScale(2, RoundingMode.HALF_UP);
			DecimaltempSisaPlafond9 = DecimaltempSisaPlafond9.setScale(2, RoundingMode.HALF_UP);
			tempPlafond9 = tempPlafond9.setScale(2, RoundingMode.HALF_UP);
			for (i = 1; i < jangka; i++){
				 if (i > 95){
					HashMap temp = new HashMap();
					BigDecimal rate = new BigDecimal(0);
					BigDecimal tempOutput = new BigDecimal(0);
					DecimalFormat format = new DecimalFormat("###,###.##");
					
					String output = format.format(tempOutput);
					rate = marginTahunSisa.divide(tempPlafond9, 5, RoundingMode.HALF_UP).multiply(rumusRate);
					rate = rate.setScale(2, RoundingMode.HALF_UP);
					String rateFormat = String.valueOf(rate);
					rateFormat = (rateFormat + "%");
					
					tempPlafond9 = tempPlafond9.subtract(AngPokok9);
//					System.out.println("margin 9-10 :" + marginTahunSisa + "plafond :" + tempPlafond9);
					tempSumAngsuran9 = tempSumAngsuran9.add(AngsuranTahun9);
					tempSumMargin9 = tempSumMargin9.add(marginTahunSisa);
					tempSumAngPokok9 = tempSumAngPokok9.add(AngPokok9);
					
					temp.put("plafond9", tempPlafond9);
					temp.put("AngPokok9", AngPokok9);
					temp.put("angsuran9", AngsuranTahun9);
					temp.put("marginTahunSisa", marginTahunSisa);
					temp.put("rateTahun9", rateFormat);
					MapResult.add(temp);
				 }
			}
			
			TotalSumAngPokok = tempSumAngPokok.add(tempSumAngPokok7).add(tempSumAngPokok9);
			TotalSumMargin = tempSumMargin.add(tempSumMargin7).add(tempSumMargin9);
			TotalSumAngsuran = tempSumAngsuran.add(tempSumAngsuran7).add(tempSumAngsuran9);
			
			BigDecimal sisaAng = TotalSumAngsuran;
			BigDecimal sisaMar = TotalSumMargin;
			BigDecimal tempSisaAng = sisaAng;
			BigDecimal tempSisaMar = sisaMar; 
			
			tempSisaAng = tempSisaAng.setScale(2, RoundingMode.HALF_UP);
			tempSisaMar = tempSisaMar.setScale(2, RoundingMode.HALF_UP);
			
			List<HashMap> MapResult2 = new ArrayList<HashMap>();
			for (int j = 0; j < MapResult.size(); j++) {
				HashMap<String, Object> x = MapResult.get(j); 
				double rumusMargin = (12.25/100/12);
				BigDecimal temprumusMargin = new BigDecimal(rumusMargin);
				BigDecimal tempTotAng = new BigDecimal(0);
				BigDecimal tempPlafond = new BigDecimal(0);
				BigDecimal Hashmargin = new BigDecimal(0);
				BigDecimal tempAngsPokok = new BigDecimal(0);
				tempTotAng = resultSisa;
				tempPlafond = SisaPlafond;
				
				Hashmargin = tempPlafond.multiply(temprumusMargin);
				tempAngsPokok = resultSisa.subtract(Hashmargin);
				SisaPlafond = SisaPlafond.subtract(tempAngsPokok);
				
				if (j < 72) {
					tempSisaAng = tempSisaAng.subtract(resultAngsuran);
					tempSisaMar = tempSisaMar.subtract(Hashmargin);
				} else if (j > 71 && j < 96) {
					tempSisaAng = tempSisaAng.subtract(AngsuranTahun7);
					tempSisaMar = tempSisaMar.subtract(marginTahunSisa);
				} else {
					tempSisaAng = tempSisaAng.subtract(AngsuranTahun9);
					tempSisaMar = tempSisaMar.subtract(marginTahunSisa);
				}
				
			    x.put("sisaAngsuran", tempSisaAng);
			    x.put("sisaMargin", tempSisaMar);
			}
			
			for (HashMap map : MapResult){
//				System.out.println("sisaAngsuran :" + map.get("sisaAngsuran") + "Margin :" + map.get("sisaMargin"));
			}
			
			request.getSession(true).setAttribute("nama", nama); 
			request.getSession(true).setAttribute("plafond", plafond); 
			request.getSession(true).setAttribute("tgl", tgl); 
			request.getSession(true).setAttribute("waktu", jangkaWaktu);
			request.getSession(true).setAttribute("period1", resultAngsuran);
			request.getSession(true).setAttribute("period2", AngsuranTahun7);
			request.getSession(true).setAttribute("period3", AngsuranTahun9);
			request.getSession(true).setAttribute("sumAngsuran", TotalSumAngsuran);
			request.getSession(true).setAttribute("sumMargin", TotalSumMargin);
			request.getSession(true).setAttribute("sumAngPokok", TotalSumAngPokok);
			request.setAttribute("kpr", MapResult);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
		
		return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "simulasiKPR5th.do")){
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
	
}
