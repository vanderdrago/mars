package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.BatchMaster;
import com.muamalat.reportmcb.function.SqlFunction;

public class ViewTHJurnalBmiAct extends org.apache.struts.action.Action { 

	 private static String SUCCESS = "success";
	    private static Logger log = Logger.getLogger(ViewTHJurnalBmiAct.class);

	    /** Action called on save button click
	     */
	    public ActionForward execute(ActionMapping mapping, ActionForm form,
	            HttpServletRequest request, HttpServletResponse response)
	            throws Exception {

	        SUCCESS = isValidUser(request);
	        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

	        if (!SUCCESS.equals("success")) {
	            return mapping.findForward(SUCCESS);
	        }

	        List lBatchM = new ArrayList();
	        SqlFunction f = new SqlFunction();
	        BatchMaster bb = null;
        	String title = "	BATCH TRANSAKSI <br /> ";
            request.setAttribute("title", title);
            request.getSession(true).setAttribute("title", title);
	        String user_id = request.getParameter("user");
	        String brnch = request.getParameter("kdcab");
	        String tgl1 = request.getParameter("tgl1").trim();
	        String tgl2 = request.getParameter("tgl2").trim();
	        String mod = request.getParameter("mod").trim();

	        int page = 1;
	        int sawal = 1;
	        int sakhir = 10;
        	int recordsPerPage = 30;
	        lBatchM = f.getListbatchht(user_id,brnch,tgl1,tgl2,mod, page, recordsPerPage);
	        if (lBatchM.size() > 0) {
	        	   int noOfRecords = 0;
                   int noOfPages = 0;
                   int totrow = f.getnoOfRecords2(user_id, brnch, tgl1, tgl2, mod);
                   noOfRecords = totrow;
                   noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

                   request.setAttribute("sawal", sawal);
                   request.setAttribute("sakhir", sakhir);	
                   request.getSession(true).setAttribute("title", title);
                   request.getSession(true).setAttribute("tgl1", tgl1);
                   request.getSession(true).setAttribute("tgl2", tgl2);
                   request.getSession(true).setAttribute("user", user_id);
                   request.getSession(true).setAttribute("kdcab", brnch);
                   request.setAttribute("lBatchM", lBatchM);
                   request.setAttribute("bb", bb);
                   request.setAttribute("noOfRecords", noOfRecords);
                   request.setAttribute("noOfPages", noOfPages);
                   request.setAttribute("currentPage", page);
                   request.getSession(true).setAttribute("bb", bb);
                   request.getSession(true).setAttribute("noOfRecords", noOfRecords);
                   request.getSession(true).setAttribute("noOfPages", noOfPages);                   

	        }

	        return mapping.findForward(SUCCESS);
	    }
	    
	    private String isValidUser(HttpServletRequest request) {
	        int level;
	        int validLevel = 5;
	        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
	            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
	            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
	            return "loginpage";
	        }

	        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//	        if (level != validLevel) {
//	            request.setAttribute("message", "Unauthorized Access!");
//	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//	            //request.setAttribute("error_code", "Error_timeout");
//	            return "msgpage";
//	        }
			 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			        return "success";
		        } else {
		            request.setAttribute("message", "Unauthorized Access!");
		            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
		            //request.setAttribute("error_code", "Error_timeout");
		            return "msgpage";
		        }


	    }
	
}
