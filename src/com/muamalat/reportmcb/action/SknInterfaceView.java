package com.muamalat.reportmcb.action;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.bean.IntfRtgsOutBean;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.IntfRtgsOut;
import com.muamalat.reportmcb.function.FileFunction;
import com.muamalat.reportmcb.function.SknRtgsMsslFunction;

public class SknInterfaceView extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(SknInterfaceView.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        

        try{
            String opt = request.getParameter("btn");
        	IntfRtgsOutBean intfOut = (IntfRtgsOutBean) form;              
            int sawal = 1;
            int sakhir = 10;
            int page = 1;
            int recordsPerPage = 30;
    		int noOfRecords = 0;
            int noOfPages = 0;
            
            SknRtgsMsslFunction rf = new SknRtgsMsslFunction();
            List ltrnsknmsl = null;
            IntfRtgsOut rekap = null;
        	rekap = rf.getTotNomSknOutMssl(intfOut);
        	ltrnsknmsl = rf.getListSknMssl(intfOut, sawal -1, "V"); 
        	  
        	
            if("Download".equals(opt)){
        		String filename = "SKN_MASSAL.txt";
            	String pathfile = "/opt/temp/" + filename;
//        		String pathfile = "C:\\" + filename;
                FileFunction ff = new FileFunction();
            	ff.createFileSknMssl(intfOut, rekap.getTottrans(), pathfile);
                File fl = new File(pathfile);
                if (fl.exists()) {
                    try {
                    	int length = 0;
                        ServletOutputStream op = response.getOutputStream();
                        response.reset();
                        ServletContext context = servlet.getServletContext();
                        String mimetype = context.getMimeType(pathfile + "/" + filename);
                        response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
                        response.setContentLength((int) fl.length());
                        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                        byte[] bbuf = new byte[1024];
                        DataInputStream in = new DataInputStream(new FileInputStream(fl));

                        while ((in != null) && ((length = in.read(bbuf)) != -1)) {
                            op.write(bbuf, 0, length);
                        }
                        
                        in.close();
                        op.flush();
                        op.close();
                    } catch (IOException ex) {
                        //System.out.println("error in donwload : " + ex);
                    }
                    
                    ff.deleteFile(pathfile);
                }
            } else {
            	if (ltrnsknmsl.size() > 0 && ltrnsknmsl != null) {
                	if (rekap != null) {
                    	request.setAttribute("teks", "<h3> TRANSAKSI SKN OUT MASSAL</h3>");  
                    	request.setAttribute("respon", "teks"); 
            			noOfRecords = 0;
                        noOfPages = 0;
                        noOfRecords = rekap.getTottrans();
                        noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                        if (noOfPages < sakhir){
                        	sakhir = noOfPages;
                        }  
                        request.setAttribute("sawal", sawal);
                        request.setAttribute("sakhir", sakhir);
                        request.setAttribute("ltrnsknmsl", ltrnsknmsl);
                        request.setAttribute("trans", rekap);
                        request.setAttribute("noOfRecords", noOfRecords);
                        request.setAttribute("noOfPages", noOfPages);
                        request.setAttribute("currentPage", page);
                        request.getSession(true).setAttribute("trans", rekap);
                        request.getSession(true).setAttribute("intfOut", intfOut);
                        request.getSession(true).setAttribute("noOfRecords", noOfRecords);
                        request.getSession(true).setAttribute("noOfPages", noOfPages);
                        request.setAttribute("data", "out");
                	} else {
                    	request.setAttribute("teks", "Tidak ditemukan data transaksi RTGS.");  
                    	request.setAttribute("respon", "teks");       		
                	}
                } else {
                	request.setAttribute("teks", "Tidak ditemukan data transaksi RTGS.");  
                	request.setAttribute("respon", "teks");
            	}  
                
            }
            return mapping.findForward(SUCCESS);
        }
        catch(Exception e){
        	request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward("failed");        	
        }
    }
        
        
    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
    }

}
