package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class simulasiKPR15act extends org.apache.struts.action.Action{

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(simulasiKPR15act.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try {
			
			String nama = request.getParameter("nama");
			String plafond = request.getParameter("plafond");
			String waktu = "180";
			String tanggal = request.getParameter("tgl");
			
			int tenor = Integer.parseInt(waktu);
			double Dplafond = Double.parseDouble(plafond);
			double angsuranPertama = (Dplafond * ((7.77/100/12) / (1 - (1/Math.pow((1+(7.77/100/12)), tenor)))));
			double angsuranKedua = 1.5 * angsuranPertama;
			double rumusAngsuranKetiga = (Dplafond * ((13.5/100/12) / (1 - (1/Math.pow((1+(13.5/100/12)), tenor)))));
			double angsuranKetiga = rumusAngsuranKetiga * 180 ;
			double pembagi = 48;
			
	        BigDecimal rumus = new BigDecimal(pembagi);
			BigDecimal decimalPlafond = new BigDecimal(Dplafond);
			BigDecimal totalAngsuran = new BigDecimal(angsuranKetiga);
			totalAngsuran = totalAngsuran.setScale(0, RoundingMode.HALF_UP);
			
			BigDecimal tempAngsuranPertama = new BigDecimal(angsuranPertama);
			tempAngsuranPertama = tempAngsuranPertama.setScale(2, RoundingMode.HALF_UP);
			BigDecimal tempAngsuranKedua = new BigDecimal(angsuranKedua);
			
			BigDecimal tempAngsuranKetiga = new BigDecimal(0);
			
			List<HashMap> map = new ArrayList<HashMap>();
			
			BigDecimal resultPlafond = decimalPlafond;
			BigDecimal resultOs = resultPlafond;
			BigDecimal resultAngsPertama = tempAngsuranPertama;
			BigDecimal resultAngsKedua = tempAngsuranKedua;
			BigDecimal resultAngsKetiga = new BigDecimal(0);
			BigDecimal tempAngsKetiga = new BigDecimal(0);
			BigDecimal TempAngsuranKetiga = new BigDecimal(0);
			
			BigDecimal resultPokokPertama = new BigDecimal(0);
			BigDecimal resultMarginPertama = new BigDecimal(0);
			BigDecimal resultAngsuranPertama = new BigDecimal(0);
			BigDecimal resultAngsuranKedua = new BigDecimal(0);
			BigDecimal resultAngsuranKetiga= new BigDecimal(0);
			
			BigDecimal sumAngsuranPertama = new BigDecimal(0);
			BigDecimal sumAngsuranPertamaLanjutan = new BigDecimal(0);
			BigDecimal sumAngsuranKedua = new BigDecimal(0);
			BigDecimal sumAngsuranKetiga = new BigDecimal(0);
			BigDecimal totalSumPertama = new BigDecimal(0);
			BigDecimal totalMargin = new BigDecimal(0);
			
			for (int i = 1; i < tenor; i++)
			{
				if (i < 73){
					HashMap temp = new HashMap();
					
					double rumusMargin = (10.75/100/12);
					BigDecimal margin = new BigDecimal(rumusMargin);
					BigDecimal sumtempAngsPertama = resultAngsPertama;
					
					resultAngsuranPertama = resultAngsPertama;
					resultMarginPertama = resultOs.multiply(margin);
					resultMarginPertama = resultMarginPertama.setScale(2, RoundingMode.HALF_UP);
					resultPokokPertama = resultAngsuranPertama.subtract(resultMarginPertama);
					resultOs = resultOs.subtract(resultPokokPertama);
					sumAngsuranPertama = sumAngsuranPertama.add(resultAngsuranPertama);
					temp.put("osPertama", resultOs);
					temp.put("pokokPertama", resultPokokPertama);
					temp.put("marginPertama", resultMarginPertama);
					temp.put("AngsuranPertama", resultAngsuranPertama);
//					System.out.println("bulan :" + i + "Os :" + resultOs + "Pokok :" + resultPokokPertama + "Margin :" + resultMarginPertama + "Angsuran :" + resultAngsuranPertama);
					map.add(temp);
				}			
				
			}
			
			BigDecimal resultOsKedua = resultOs;		
			BigDecimal tempOsKedua = resultOsKedua;
			BigDecimal tempPokokKedua = new BigDecimal(0);
			BigDecimal tempMarginKedua = new BigDecimal(0);
			
			for (int i = 1; i < tenor + 1; i++)
			{
				if (i > 72 && i < 85){
					HashMap temp = new HashMap();
					double rumusMarginKedua = (11.75/100/12);
					BigDecimal tempRumusMarginKedua = new BigDecimal(rumusMarginKedua);
					
					BigDecimal tempSum = resultAngsPertama;
					tempMarginKedua = tempOsKedua.multiply(tempRumusMarginKedua);
					tempMarginKedua = tempMarginKedua.setScale(2, RoundingMode.HALF_UP);
					tempPokokKedua = resultAngsuranPertama.subtract(tempMarginKedua);
					tempOsKedua = tempOsKedua.subtract(tempPokokKedua);
					sumAngsuranPertamaLanjutan = sumAngsuranPertamaLanjutan.add(tempSum);
					
					temp.put("osKedua", tempOsKedua);
					temp.put("pokokKedua", tempPokokKedua);
					temp.put("marginKedua", tempMarginKedua);
					temp.put("TempAngsuranPertama", resultAngsuranPertama);
					
//					System.out.println("bulan :" + i + "Os :" + tempOsKedua + "Pokok :" + tempPokokKedua + "Margin :" + tempMarginKedua + "Angsuran :" + resultAngsuranPertama);
					map.add(temp);
				}
			}
			
			totalSumPertama = sumAngsuranPertama.add(sumAngsuranPertamaLanjutan);
			
			BigDecimal resultKetiga = tempOsKedua;
			BigDecimal tempOsKetiga = resultKetiga;
			BigDecimal tempMarginKetiga = new BigDecimal(0);
			BigDecimal tempPokokKetiga = new BigDecimal(0);
			
			for (int i = 1; i < tenor + 1; i++){
				if (i > 84 && i < 133){
					HashMap temp = new HashMap();
					double rumusMarginKetiga = (11.75/100/12);
					BigDecimal tempRumusMarginKetiga = new BigDecimal(rumusMarginKetiga);
					resultAngsuranKedua = resultAngsKedua;
					resultAngsuranKedua = resultAngsuranKedua.setScale(2, RoundingMode.HALF_UP);
					
					tempMarginKetiga = tempOsKetiga.multiply(tempRumusMarginKetiga);
					tempMarginKetiga = tempMarginKetiga.setScale(2, RoundingMode.HALF_UP);
					tempPokokKetiga = resultAngsKedua.subtract(tempMarginKetiga);
					tempOsKetiga = tempOsKetiga.subtract(tempPokokKetiga);
					sumAngsuranKedua = sumAngsuranKedua.add(resultAngsuranKedua);
					sumAngsuranKedua = sumAngsuranKedua.setScale(2, RoundingMode.HALF_UP);
					
					temp.put("osKetiga", tempOsKetiga);
					temp.put("pokokKetiga", tempPokokKetiga);
					temp.put("marginKetiga", tempMarginKetiga);
					temp.put("AngsuranKedua", resultAngsKedua);
//					System.out.println("bulan :" + i + "Os :" + tempOsKedua + "Pokok :" + tempPokokKedua + "Margin :" + tempMarginKedua + "Angsuran :" + resultAngsKedua);
					map.add(temp);
				}
			}
			
			sumAngsuranKetiga = totalSumPertama.add(sumAngsuranKedua);
			totalMargin = totalAngsuran.subtract(decimalPlafond);
			tempAngsKetiga = (totalAngsuran.subtract(totalSumPertama.add(sumAngsuranKedua)));		
			TempAngsuranKetiga = tempAngsKetiga.divide(rumus, BigDecimal.ROUND_HALF_UP);

			
			BigDecimal tempResultAngsKetiga = TempAngsuranKetiga;
			BigDecimal resultKeempat = tempOsKetiga;
			BigDecimal tempPokokKeempat = new BigDecimal(0);
			BigDecimal tempMarginKeempat = new BigDecimal(0);
			
			for (int i = 1; i < tenor + 1; i++){
				if (i > 132){
					HashMap temp = new HashMap();
					double rumusMarginKeempat = (14.168364/100/12);
					BigDecimal tempRumusMarginKeempat = new BigDecimal(rumusMarginKeempat);
					
					resultAngsuranKetiga = TempAngsuranKetiga;
					tempMarginKeempat = resultKeempat.multiply(tempRumusMarginKeempat);
					tempMarginKeempat = tempMarginKeempat.setScale(2, RoundingMode.HALF_UP);
					tempPokokKeempat = resultAngsuranKetiga.subtract(tempMarginKeempat);
					tempPokokKeempat = tempPokokKeempat.setScale(2, RoundingMode.HALF_UP);
					resultKeempat = resultKeempat.subtract(tempPokokKeempat);
					
					temp.put("osKeempat", resultKeempat);
					temp.put("pokokKeempat", tempPokokKeempat);
					temp.put("MarginKeempat", tempMarginKeempat);
					temp.put("AngsuranKetiga", resultAngsuranKetiga);
					
//					System.out.println("Angsuran :" + temp.get("AngsuranKetiga"));
					
					map.add(temp);
					
				}
			}
			
			System.out.println("angsuran pertama :" + resultAngsuranPertama);
			System.out.println("Angsuran kedua :" + resultAngsuranKedua);
			System.out.println("Angsuran ketiga :" + resultAngsuranKetiga);
			System.out.println("totalAngsuran :" + totalAngsuran);
			System.out.println("sumAngsuranPertama :" + sumAngsuranPertama);
			System.out.println("sumAngsuranPertamaLanjutan :" + sumAngsuranPertamaLanjutan);
			System.out.println("totalSumPertama :" + totalSumPertama);
			System.out.println("sumAngsuranKedua :" + sumAngsuranKedua);
			System.out.println("sumAngsuranKetiga :" + sumAngsuranKetiga);
			
			for (HashMap temp : map){
				
			}
			
			request.getSession(true).setAttribute("nama", nama);
			request.getSession(true).setAttribute("plafond", Dplafond);
			request.getSession(true).setAttribute("jangkaWaktu", tenor);
			request.getSession(true).setAttribute("tanggal", tanggal);
			request.getSession(true).setAttribute("angsuranPertama", tempAngsuranPertama);
			request.getSession(true).setAttribute("angsuranKedua", tempAngsuranKedua);
			request.getSession(true).setAttribute("angsuranketiga", resultAngsuranKetiga);
			request.getSession(true).setAttribute("totalPokok", decimalPlafond);
			request.getSession(true).setAttribute("totalMargin", totalMargin);
			request.getSession(true).setAttribute("totalAngsuran", totalAngsuran);
			request.setAttribute("kpr", map);
			
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
		
	}
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		System.out.println("lrole :" + lrole);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "simulasiKPR15th.do")){
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
	
	
}
