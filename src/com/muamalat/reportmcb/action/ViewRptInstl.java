package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.Instalment;
import com.muamalat.reportmcb.function.FinSqlFunction;

public class ViewRptInstl extends org.apache.struts.action.Action {
	/* forward name="success" path="" */

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(ViewRptInstl.class);

	/**
	 * Action called on save button click
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SUCCESS = isValidUser(request);

		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}

		List lInstl = new ArrayList();
		String noInstl = (String) request.getParameter("instNo");
		lInstl = (List) request.getSession(true).getAttribute("llsp");
		
		Instalment mstrInstl = null;
		if(lInstl.size() > 0){
			for (Iterator<Instalment> i = lInstl.iterator(); i.hasNext();) {
				Instalment jmcb = i.next();
				System.out.println("jmcb :" + noInstl.equals(jmcb.getAccount_number()));
				if(noInstl.equals(jmcb.getAccount_number())){
					mstrInstl = jmcb;
					System.out.println("mstrInstl :" + mstrInstl);
					System.out.println("jmcb :" + jmcb);
					break;
				}
			}
		}
		System.out.println("mstrInstl :" + mstrInstl);
		FinSqlFunction f = new FinSqlFunction();
		Instalment heds = f.gethedjadang(mstrInstl.getAccount_number(), mstrInstl.getAmount_disbursed(), mstrInstl.getUpfront_profit_booked(), mstrInstl.getUjroh(), mstrInstl.getDenda());
//		Instalment heds = f.gethedjadang(mstrInstl.getAccount_number(), mstrInstl.getAmount_disbursed(), mstrInstl.getUpfront_profit_booked());
		mstrInstl.setTempTotPkh(heds.getTempTotPkh());
		mstrInstl.setTempTotMrgnh(heds.getTempTotMrgnh());
		mstrInstl.setTempPaidh(heds.getTempPaidh());
		mstrInstl.setTempTotUjroh(heds.getTempTotUjroh());
		mstrInstl.setTempTotDenda(heds.getTempTotDenda());
		if(mstrInstl!=null){
			List angs = f.getdjadang(mstrInstl);
			List angs2 = f.getdjadangdenda(mstrInstl);
			request.setAttribute("angs", angs);
			request.setAttribute("angs2", angs2);
			request.setAttribute("mstrInstl", mstrInstl);
		}
		return mapping.findForward(SUCCESS);
	}

	private String isValidUser(HttpServletRequest request) {

		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,
					validLevel);
			request.setAttribute("message",
					"Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(
				StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5
				|| level == 6 || level == 7) {
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error",
					"Anda tidak memiliki akses ke halaman yang diminta!");
			return "msgpage";
		}
	}

}
