/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.JoinTrnRTGSMCBInMstr;
import com.muamalat.reportmcb.entity.TRTGSmcbin;
import com.muamalat.reportmcb.entity.TRtiftsin;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author Utis
 */
public class ViewMonJoinRTGSInAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewMonJoinRTGSInAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
		MonSqlFunction f = new MonSqlFunction();
		String fr = (String) request.getParameter("fr");
		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String sDate = formatter.format(today);
//		sDate = "20-05-2013";
		List mcbltrn = new ArrayList();
		List joinltrn = new ArrayList();
		//String network = "RTGSBI";
		if ("f".equals(fr)) {
			String inputtrn = (String) request.getParameter("inputtrn");
			String stsrtgs = (String) request.getParameter("stsrtgs");
			String amont_a = (String) request.getParameter("amont_a");
			String amont_b = (String) request.getParameter("amont_b");
			mcbltrn = f.searchRtgsIn(sDate, inputtrn, stsrtgs, amont_a, amont_b);
		}
		if (mcbltrn.size() > 0) {
			for (Iterator<TRtiftsin> i = mcbltrn.iterator(); 
					i.hasNext();) {
				TRtiftsin jint = i.next();
				String whrstr = " and a.ac_no in ( select cust_ac_no from sttm_cust_account " +
								"where alt_ac_no = '"+jint.getUltimatebeneacc().trim()+"' or cust_ac_no = '"+jint.getUltimatebeneacc().trim()+"') " ;//+
								//" and a.lcy_amount = '"+jint.getNominal()+"'";
				TRTGSmcbin jmcb = f.getrtgsjmcbin(whrstr);
				JoinTrnRTGSMCBInMstr jTrnRTGSM = new JoinTrnRTGSMCBInMstr();
				jTrnRTGSM.setValuedate(jint.getValuedate());
				jTrnRTGSM.setRelTRN(jint.getRelTRN());
				jTrnRTGSM.setTRN(jint.getTRN());
				jTrnRTGSM.setTranscode(jint.getTranscode());
				jTrnRTGSM.setBor(jint.getBor());
				jTrnRTGSM.setOsn(jint.getOsn());
				jTrnRTGSM.setFromaccnum(jint.getFromaccnum());
				jTrnRTGSM.setFromaccname(jint.getFromaccname());
				jTrnRTGSM.setToaccnum(jint.getToaccnum());
				jTrnRTGSM.setToaccname(jint.getToaccname());
				jTrnRTGSM.setUltimatebeneacc(jint.getUltimatebeneacc());
				jTrnRTGSM.setUltimatebenename(jint.getUltimatebenename());
				jTrnRTGSM.setNominal(jint.getNominal());
				jTrnRTGSM.setPayDetails(jint.getPayDetails());
				jTrnRTGSM.setStatus(jint.getStatus());
				jTrnRTGSM.setSaktinum(jint.getSaktinum());
				if (jmcb != null) {
					jTrnRTGSM.setTrn_ref_no(jmcb.getTrn_ref_no());
					jTrnRTGSM.setEvent(jmcb.getEvent());
					jTrnRTGSM.setModule(jmcb.getModule());
					jTrnRTGSM.setAc_branch(jmcb.getAc_branch());
					jTrnRTGSM.setTrn_code(jmcb.getTrn_code());
					jTrnRTGSM.setBatch_no(jmcb.getBatch_no());
					jTrnRTGSM.setAc_no(jmcb.getAc_no());
					jTrnRTGSM.setAc_desc(jmcb.getAc_desc());
					jTrnRTGSM.setDrcr_ind(jmcb.getDrcr_ind());
					jTrnRTGSM.setLcy_amount(jmcb.getLcy_amount());
					jTrnRTGSM.setAuth_stat(jmcb.getAuth_stat());
					jTrnRTGSM.setUser_id(jmcb.getUser_id());
					jTrnRTGSM.setAuth_id(jmcb.getAuth_id());
				} else {
					jTrnRTGSM.setTrn_ref_no("");
					jTrnRTGSM.setEvent("");
					jTrnRTGSM.setModule("");
					jTrnRTGSM.setAc_branch("");
					jTrnRTGSM.setTrn_code("");
					jTrnRTGSM.setBatch_no("");
					jTrnRTGSM.setAc_no("");
					jTrnRTGSM.setAc_desc("");
					jTrnRTGSM.setDrcr_ind("");
					jTrnRTGSM.setLcy_amount(new BigDecimal("0"));
					jTrnRTGSM.setAuth_stat("");
					jTrnRTGSM.setUser_id("");
					jTrnRTGSM.setAuth_id("");
				}
				joinltrn.add(jTrnRTGSM);
			}
		} 
		if (mcbltrn.size() == 0) {
				String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
	            request.setAttribute("dataresponse", dataresponse);
	            request.setAttribute("konfirmasi", "err");
		}
		
		request.setAttribute("ltrn", joinltrn);
		request.getSession(true).setAttribute("ltrn", joinltrn);
		return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
