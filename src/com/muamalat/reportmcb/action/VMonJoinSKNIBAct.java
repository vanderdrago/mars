/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.IbTransfersDom;
import com.muamalat.reportmcb.entity.JoinTrnRTGSIBMaster;
import com.muamalat.reportmcb.entity.PctbContractMaster;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author TrioKwek-Kwek
 */
public class VMonJoinSKNIBAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(VMonJoinSKNIBAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
		MonSqlFunction f = new MonSqlFunction();
		String fr = (String) request.getParameter("fr");
		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = formatter.format(today);
		//sDate = "20-05-2013";
		List ltrn = new ArrayList();
		List joinltrn = new ArrayList();
		String trfType = "1";
		String refno = (String) request.getParameter("refno"); 
	    String fromacc = (String) request.getParameter("fromacc");        
	    String toacc = (String) request.getParameter("toacc");         
	    String amont_a = (String) request.getParameter("amont_a");
		String amont_b = (String) request.getParameter("amont_b");
		
		if ("f".equals(fr)) {
			if ((refno != null) || (fromacc != null) || (toacc != null) || (amont_a != null) || (amont_b != null)){
	        	String whrstr = "";         	
	        	if ((refno == null || "".equals(refno)) && (fromacc == null || "".equals(fromacc)) && (toacc == null || "".equals(toacc)) && (amont_a == null || "".equals(amont_a))&& (amont_b == null || "".equals(amont_b))){
	        		whrstr = " where d.TRANSFER_TYPE= '"+trfType+"' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
	        	} else {
	        		if (refno == null) {
	        			refno = "";
	            		}
	        		if (fromacc == null) {
	        			fromacc = "";
	            		}
	            	if (toacc == null) {
	            		toacc = "";
	            		}
	            	if (amont_a == null) {
	            		amont_a = "";
	            		}
	            	if (amont_b == null) {
	            		amont_b = "";
	            		}
	            	whrstr = " where (t.REF_NO = '" + refno.trim()
	            			+ "' or d.ACCOUNT_NO = '" + fromacc.trim()
							+ "' or d.TO_ACCOUNT_NO = '" + toacc.trim()
							+ "' or (d.AMOUNT >= '" + amont_a.trim()
							+ "' or d.AMOUNT <= '" + amont_b.trim()
							+ "')) and d.TRANSFER_TYPE = '" + trfType.trim()
							+ "' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
	        	}
	        	ltrn = f.searchjrtgsib(whrstr + " order by d.TO_NAME " );
	        }         
	       	
//			String whrstr = " where d.TRANSFER_TYPE= '"+trfType+"' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
//			ltrn  = f.searchjrtgsib(whrstr);
		} else if ("u".equals(fr)) {
			String srv = (String) request.getParameter("srv");
			String sts = (String) request.getParameter("sts");
			if ((refno != null) || (fromacc != null) || (toacc != null) || (amont_a != null) || (amont_b != null)){
	        	String whrstr = "";         	
	        	if ((refno == null || "".equals(refno)) && (fromacc == null || "".equals(fromacc)) && (toacc == null || "".equals(toacc)) && (amont_a == null || "".equals(amont_a)) && (amont_b == null || "".equals(amont_b))){
	        		whrstr = " where d.TRANSFER_TYPE= '"+trfType+"' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
	        	} else {
	        		if (refno == null) {
	        			refno = "";
	            		}
	        		if (fromacc == null) {
	        			fromacc = "";
	            		}
	            	if (toacc == null) {
	            		toacc = "";
	            		}
	            	if (amont_a == null) {
	            		amont_a = "";
	            		}
	            	if (amont_b == null) {
	            		amont_b = "";
	            		}
	            	whrstr = " where (t.REF_NO = '" + refno.trim()
	            			+ "' or d.ACCOUNT_NO = '" + fromacc.trim()
							+ "' or d.TO_ACCOUNT_NO = '" + toacc.trim()
							+ "' or (d.AMOUNT >= '" + amont_a.trim()
							+ "' or d.AMOUNT <= '" + amont_b.trim()
							+ "')) and d.TRANSFER_TYPE = '" + trfType.trim()
							+ "' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
	        	}
	        	ltrn = f.searchjrtgsib(whrstr + " order by d.TO_NAME " );
	        }  
//			String whrstr = " where d.TRANSFER_TYPE= '"+trfType+"' and trunc(d.DATE_TRX) = TO_DATE('"+sDate+"', 'dd-MM-yyyy')";
//			ltrn = f.searchjrtgsib(whrstr);
			
//			mcbltrn = f.searchMonRtgsMcbu(sDate, network, srv, sts);
		}
		if (ltrn.size() > 0) {
			for (Iterator<IbTransfersDom> i = ltrn.iterator(); i.hasNext();) {
				IbTransfersDom jibf = i.next();
				String whrstr = " where cpty_ac_no = '" + jibf.getToAccountNo()
								+ "' and cpty_name = '" + jibf.getToName()
								+ "' and txn_amount = '" + jibf.getAmount()
								+ "' and product_code = 'SOIB' ";
				PctbContractMaster jmcb = f.searchMonRtgsIb(whrstr);
				JoinTrnRTGSIBMaster jTrnRTGSIB = new JoinTrnRTGSIBMaster();
				jTrnRTGSIB.setRefNo(jibf.getRefNo());
				jTrnRTGSIB.setRefId(jibf.getRefId());
				jTrnRTGSIB.setTransferType(jibf.getTransferType());
				jTrnRTGSIB.setAccountNo(jibf.getAccountNo());
				jTrnRTGSIB.setToAccountNo(jibf.getToAccountNo());
				jTrnRTGSIB.setToName(jibf.getToName());
				jTrnRTGSIB.setToBankName(jibf.getToBankName());
				jTrnRTGSIB.setAmount(jibf.getAmount());
				jTrnRTGSIB.setResponseCode(jibf.getResponseCode());
				jTrnRTGSIB.setStatus(jibf.getStatus());
				if(jmcb != null){
					jTrnRTGSIB.setContractRefNo(jmcb.getContractRefNo());
					jTrnRTGSIB.setProdRefNo(jmcb.getContractRefNo());
					jTrnRTGSIB.setBookingDt(jmcb.getBookingDt());
					jTrnRTGSIB.setCheckerId(jmcb.getCheckerId());
					jTrnRTGSIB.setCptyAcNo(jmcb.getCptyAcNo());
					jTrnRTGSIB.setCptyName(jmcb.getCptyName());
					jTrnRTGSIB.setCustAcNo(jmcb.getCustAcNo());
					jTrnRTGSIB.setCustName(jmcb.getCustName());
					jTrnRTGSIB.setCustBankcode(jmcb.getCptyBankcode());
					jTrnRTGSIB.setDispatchRefNo(jmcb.getDispatchRefNo());
					jTrnRTGSIB.setExceptionQueue(jmcb.getExceptionQueue());
					jTrnRTGSIB.setMakerId(jmcb.getMakerId());
					jTrnRTGSIB.setNetwork(jmcb.getNetwork());
					jTrnRTGSIB.setPayDetails(jmcb.getPaymentDetails1());
					jTrnRTGSIB.setTxnAmount(jmcb.getTxnAmount());
					jTrnRTGSIB.setUdf_2(jmcb.getUdf2());
				} else {
					jTrnRTGSIB.setContractRefNo("");
					jTrnRTGSIB.setProdRefNo("");
					jTrnRTGSIB.setBookingDt(null);
					jTrnRTGSIB.setCheckerId("");
					jTrnRTGSIB.setCptyAcNo("");
					jTrnRTGSIB.setCptyName("");
					jTrnRTGSIB.setCustAcNo("");
					jTrnRTGSIB.setCustName("");
					jTrnRTGSIB.setCustBankcode("");
					jTrnRTGSIB.setDispatchRefNo("");
					jTrnRTGSIB.setExceptionQueue("");
					jTrnRTGSIB.setMakerId("");
					jTrnRTGSIB.setNetwork("");
					jTrnRTGSIB.setPayDetails("");
					jTrnRTGSIB.setTxnAmount(new BigDecimal(0));
					jTrnRTGSIB.setUdf_2("");
					}
				joinltrn.add(jTrnRTGSIB);
			}
		}
		if (ltrn.size() == 0) {
			String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
            request.setAttribute("dataresponse", dataresponse);
            request.setAttribute("konfirmasi", "err");
        }
		request.setAttribute("ltrn", joinltrn);
		request.getSession(true).setAttribute("ltrn", joinltrn);
		return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            return "msgpage";
	        }


    }
}
