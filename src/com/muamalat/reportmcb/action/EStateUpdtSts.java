package com.muamalat.reportmcb.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.EstateFunction;

public class EStateUpdtSts extends org.apache.struts.action.Action {
	/* forward name="success" path="" */

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(EStateOto.class);

	/**
	 * Action called on save button click
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}
		try {

        	EstateFunction ef = new EstateFunction();
    		String updtsts = request.getParameter("updtsts");
			E_stat_cust cust = (E_stat_cust)request.getSession(true).getAttribute("cust_oto_updtsts");
			if (cust != null){
	        	cust.setUser_aut(user.getUsername());
	        	String sts_data = "";
	        	String sts_data_desc = "";
	        	if("H".equals(updtsts)){
	        		sts_data = "H";
	        		sts_data_desc = "Hold";	        		
	        	} else if("C".equals(updtsts)){
	        		sts_data = "C";
	        		sts_data_desc = "Closed";	        		
	        	} else if("O".equals(updtsts)){
	        		sts_data = "O";
	        		sts_data_desc = "Active";	        		
	        	}
	        	Date myDate = new Date();
	        	java.sql.Timestamp dt_aut = new Timestamp(myDate.getTime());  
	        	cust.setDt_aut(dt_aut);
	        	cust.setSts_proc("I");
	        	cust.setSts_proc_desc("Unauthorized");
	        	cust.setSts_data(sts_data);
	        	cust.setSts_data_desc(sts_data_desc);
	        	boolean ret = ef.updateEStateCust(cust);
	        	if(ret) {
	    			request.setAttribute("respon","Update data Berhasil.");
	    			request.setAttribute("data", "konfirm");
	        	} else {
	    			request.setAttribute("respon","Update data gagal.");
	    			request.setAttribute("data", "konfirm");        		
	        	}
			}
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
		}
		return mapping.findForward("success");

	}

	private String isValidUser(HttpServletRequest request) {

		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel);
			request.setAttribute("message","Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			/*================= tambahan baru ==============*/
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			List lroles = cpf.getRolesUser(adm.getUsername());
		 	int ada = 0;
		 	if (lroles.size() > 0) {
		 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
		 			String rl = it.next();		 		
		 			String role3 = cpf.getAllowedStatement(11, rl.toUpperCase());
		 			if (!"".equals(role3) && role3!=null){
		 				ada++;
		 			}
		 		}
			} 
		 	cpf= null;
		 	if (ada > 0){
				return SUCCESS;					
		 	} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
		 	}
			/*
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(11, user.getRole_id());
			if ("".equals(role) || role == null) {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if ((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)) {
					return SUCCESS;
				} else {
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";
				}
			}*/
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
		}
	}

}
