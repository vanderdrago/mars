package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.facility;
import com.muamalat.reportmcb.function.SqlFunctionNew;

public class detailFasilitas extends org.apache.struts.action.Action{
	
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(detailFasilitas.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        List lsql = new ArrayList();
        SqlFunctionNew sql = new SqlFunctionNew();
        String FasilitasKe = request.getParameter("fasilitasKe");
        String status = (String) request.getSession(true).getAttribute("status");
      //format period
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMM");//dd/MM/yyyy
	    Date now = new Date();
	    String period = sdfDate.format(now);
        System.out.println("noFasilitas :" + FasilitasKe + "Periode :" + period + "status :" + status);
        lsql = sql.getdetailFasilitas(FasilitasKe, status);
        System.out.println("");
        if (lsql.size() > 0){
        	request.setAttribute("lph", lsql);
        	request.setAttribute("noKartu", "noKartu");
        	request.setAttribute("period", period);
        	request.getSession(true).setAttribute("llsp", lsql);
        	request.getSession(true).setAttribute("FasilitasKe", FasilitasKe);
        	request.getSession(true).setAttribute("noKartu", "noKartu");
        	request.getSession(true).setAttribute("period", period);
        }
        
        return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
	}

    
}
