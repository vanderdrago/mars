package com.muamalat.reportmcb.action;

import java.io.ByteArrayInputStream;
import java.lang.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import javax.mail.Session;
import javax.xml.bind.DatatypeConverter;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.util.Base64;
import org.jpos.util.Log;

import com.ibm.db2.jcc.am.ne;
import com.lowagie.text.Cell;
import com.lowagie.text.Row;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import com.sun.xml.xsom.visitor.XSSimpleTypeFunction;
import java.nio.charset.StandardCharsets;

public class tes {
	 public static void main(String args[]) throws Exception{
		 
		String tenor = "180"; 
	    int plafond = 50000000;
	    int waktu = Integer.parseInt(tenor);
	   
		double angsuranPertama = (plafond * ((7.77/100/12) / (1 - (1/Math.pow((1+(7.77/100/12)), waktu)))));
		double angsuranKedua = 1.5 * angsuranPertama;
		double rumusAngsuranKetiga = (plafond * ((13.5/100/12) / (1 - (1/Math.pow((1+(13.5/100/12)), waktu)))));
		double angsuranKetiga = (rumusAngsuranKetiga * 180) ;
		double pembagi = 48;
        BigDecimal rumus = new BigDecimal(pembagi);
		
		BigDecimal totalAngsuran = new BigDecimal(angsuranKetiga);
		totalAngsuran = totalAngsuran.setScale(0, RoundingMode.HALF_UP);
		
		BigDecimal tempAngsuranPertama = new BigDecimal(angsuranPertama);
		tempAngsuranPertama = tempAngsuranPertama.setScale(2, RoundingMode.HALF_UP);
		BigDecimal tempAngsuranKedua = new BigDecimal(angsuranKedua);
		BigDecimal tempAngsuranKetiga = new BigDecimal(angsuranKetiga);
		tempAngsuranKetiga = tempAngsuranKetiga.setScale(0, RoundingMode.HALF_UP);
		List<HashMap> map = new ArrayList<HashMap>();
		
		BigDecimal resultPlafond = new BigDecimal(plafond);
		BigDecimal resultOs = resultPlafond;
		BigDecimal resultAngsPertama = tempAngsuranPertama;
		BigDecimal resultAngsKedua = tempAngsuranKedua;
		BigDecimal tempAngsKetiga = new BigDecimal(0);
		BigDecimal TempAngsuranKetiga = new BigDecimal(0);
				
		BigDecimal resultPokokPertama = new BigDecimal(0);
		BigDecimal resultMarginPertama = new BigDecimal(0);
		BigDecimal resultAngsuranPertama = new BigDecimal(0);
		BigDecimal resultAngsuranKedua = new BigDecimal(0);
		BigDecimal resultAngsuranKetiga= new BigDecimal(0);
		
		BigDecimal sumAngsuranPertama = new BigDecimal(0);
		BigDecimal sumAngsuranPertamaLanjutan = new BigDecimal(0);
		BigDecimal totalSumPertama = new BigDecimal(0);
		BigDecimal sumAngsuranKedua = new BigDecimal(0);
		BigDecimal sumAngsuranKetiga = new BigDecimal(0);
		
		for (int i = 1; i < waktu + 1; i++)
		{
			if (i < 73){
				HashMap temp = new HashMap();
				
				double rumusMargin = (10.75/100/12);
				BigDecimal margin = new BigDecimal(rumusMargin);
				BigDecimal sumtempAngsPertama = resultAngsPertama;
				
				resultAngsuranPertama = resultAngsPertama;
				resultMarginPertama = resultOs.multiply(margin);
				resultMarginPertama = resultMarginPertama.setScale(2, RoundingMode.HALF_UP);
				resultPokokPertama = resultAngsuranPertama.subtract(resultMarginPertama);
				resultOs = resultOs.subtract(resultPokokPertama);
				sumAngsuranPertama = sumAngsuranPertama.add(resultAngsuranPertama);
				System.out.println("Bulan :" + i + "Angsuran :" + resultAngsuranPertama);
//				System.out.println("Sum :" + sumAngsuranPertama);
//				System.out.println("bulan :" + i + "os :" + resultOs + "pokok :" + resultPokokPertama + "Margin :" + resultMarginPertama + "Angsuran :" + resultAngsuranPertama);
				map.add(temp);
			}
		}
		
		BigDecimal resultOsKedua = resultOs;		
		BigDecimal tempOsKedua = resultOsKedua;
		BigDecimal tempPokokKedua = new BigDecimal(0);
		BigDecimal tempMarginKedua = new BigDecimal(0);
		
		for (int i = 1; i < waktu + 1; i++)
			{
				if (i > 72 && i < 85){
					HashMap temp = new HashMap();
					double rumusMarginKedua = (11.75/100/12);
					BigDecimal tempRumusMarginKedua = new BigDecimal(rumusMarginKedua);
					BigDecimal tempSum = resultAngsPertama;
					
					tempMarginKedua = tempOsKedua.multiply(tempRumusMarginKedua);
					tempMarginKedua = tempMarginKedua.setScale(2, RoundingMode.HALF_UP);
					tempPokokKedua = resultAngsuranPertama.subtract(tempMarginKedua);
					tempOsKedua = tempOsKedua.subtract(tempPokokKedua);
					sumAngsuranPertamaLanjutan = sumAngsuranPertamaLanjutan.add(tempSum);
					System.out.println("Bulan :" + i + "Angsuran :" + resultAngsuranPertama);
//					System.out.println("Sum :" + sumAngsuranPertamaLanjutan);
//					System.out.println("bulan :" + i + "os :" + tempOsKedua + "pokok :" + tempPokokKedua + "Margin :" + tempMarginKedua + "Angsuran :" + resultAngsuranPertama);
					map.add(temp);
				}
			}
		
		totalSumPertama = sumAngsuranPertama.add(sumAngsuranPertamaLanjutan);
		
		BigDecimal resultKetiga = tempOsKedua;
		BigDecimal tempOsKetiga = resultKetiga;
		BigDecimal tempMarginKetiga = new BigDecimal(0);
		BigDecimal tempPokokKetiga = new BigDecimal(0);
		
		for (int i = 1; i < waktu + 1; i++){
			if (i > 84 && i < 133){
				HashMap temp = new HashMap();
				double rumusMarginKetiga = (11.75/100/12);
				BigDecimal tempRumusMarginKetiga = new BigDecimal(rumusMarginKetiga);
				resultAngsuranKedua = resultAngsKedua;
				resultAngsuranKedua = resultAngsuranKedua.setScale(2, RoundingMode.HALF_UP);
				tempMarginKetiga = tempOsKetiga.multiply(tempRumusMarginKetiga);
				tempMarginKetiga = tempMarginKetiga.setScale(2, RoundingMode.HALF_UP);
				tempPokokKetiga = resultAngsuranKedua.subtract(tempMarginKetiga);
				tempOsKetiga = tempOsKetiga.subtract(tempPokokKetiga);
				sumAngsuranKedua = sumAngsuranKedua.add(resultAngsuranKedua);
				sumAngsuranKedua = sumAngsuranKedua.setScale(2, RoundingMode.HALF_UP);
//				System.out.println("bulan :" + i + "os :" + tempOsKetiga + "pokok :" + tempPokokKetiga + "Margin :" + tempMarginKetiga + "Angsuran :" + resultAngsuranKedua);
				map.add(temp);
			}	
		}
		
		sumAngsuranKetiga = totalSumPertama.add(sumAngsuranKedua);
		tempAngsKetiga = (totalAngsuran.subtract(totalSumPertama.add(sumAngsuranKedua)));		
		TempAngsuranKetiga = tempAngsKetiga.divide(rumus, BigDecimal.ROUND_HALF_UP);
				
		BigDecimal tempResultAngsKetiga = TempAngsuranKetiga;
		BigDecimal resultKeempat = tempOsKetiga;
		
		BigDecimal tempPokokKeempat = new BigDecimal(0);
		BigDecimal tempMarginKeempat = new BigDecimal(0);
		
		for (int i = 1; i < waktu + 1; i++){
			if (i > 132) {
				HashMap temp = new HashMap();
				double rumusMarginKeempat = (14.17/100/12);
				BigDecimal tempRumusMarginKeempat = new BigDecimal(rumusMarginKeempat);
				
				resultAngsuranKetiga = TempAngsuranKetiga;
				
				tempMarginKeempat = resultKeempat.multiply(tempRumusMarginKeempat);
				tempMarginKeempat = tempMarginKeempat.setScale(2, RoundingMode.HALF_UP);
				tempPokokKeempat = resultAngsuranKetiga.subtract(tempMarginKeempat);
				tempPokokKeempat = tempPokokKeempat.setScale(2, RoundingMode.HALF_UP);
				resultKeempat = resultKeempat.subtract(tempPokokKeempat);
//				System.out.println("bulan :" + i + "os :" + resultKeempat + "pokok :" + tempPokokKeempat + "Margin :" + tempMarginKeempat + "Angsuran :" + resultAngsuranKetiga);
				map.add(temp);
			}
		}
		System.out.println("angsuran pertama :" + resultAngsuranPertama);
		System.out.println("Angsuran kedua :" + resultAngsuranKedua);
		System.out.println("Angsuran ketiga :" + resultAngsuranKetiga);
		System.out.println("totalAngsuran :" + totalAngsuran);
		System.out.println("sumAngsuranPertama :" + sumAngsuranPertama);
		System.out.println("sumAngsuranPertamaLanjutan :" + sumAngsuranPertamaLanjutan);
		System.out.println("totalSumPertama :" + totalSumPertama);
		System.out.println("sumAngsuranKedua :" + sumAngsuranKedua);
		System.out.println("sumAngsuranKetiga :" + sumAngsuranKetiga);
		
//		List<HashMap> map1 = new ArrayList<HashMap>();
//		int a = 100;
//		int b = a;
//		int c = 5;
////		int d = 6;
//		
//		for (int i = 0; i < 10; i++){
//			HashMap temp = new HashMap();
//			int e = a;
////			d = a - c;
//			b = b + c;
//			map.add(temp);
//			 System.out.println("" + b + "" + e);
			 
//		}
		
		
		 
//		byte[]message = "Nama Saya Tri wahyudi".getBytes("UTF-8");
//		String encoded = DatatypeConverter.printBase64Binary(message);
//		byte[] decoded = DatatypeConverter.parseBase64Binary(encoded);
//
//		System.out.println(encoded);
//		System.out.println(new String(decoded, "UTF-8"));
		 
//		 String tes = "2016-02-20";
		 
//		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		 Date date2= sdf.parse(tes);
		 
//		 Date date1 = sdf.parse("2016-07-05");
//		 Date date2 = sdf.parse("2015-07-05");
//		 System.out.println("date1 -->" + date1);
//		 System.out.println("date2 -->" + date2);
//		 
//		 if (date1.compareTo(date2) < 0){
//			 System.out.println("if date1 < date2");
//		 } else if (date1.compareTo(date2) > 0){
//			 System.out.println("if date1 > date2");
//		 } else if (date1.compareTo(date2) == 0){
//			 System.out.println("if date1 == date2");
//		 } else {
//			 System.out.println("when");
//		 }
		 
		//Blank workbook
//		 XSSFWorkbook worbook = new XSSFWorkbook();
//		//Blank workbook
//		 XSSFSheet sheet = worbook.createSheet("Employee Data");
//		//This data needs to be written (Object[])
//		 Map<String, Object[]> data = new TreeMap<String, Object[]>();
//		 data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
//		 data.put("2", new Object[] {1, "Amit", "Shukla"});
//         data.put("3", new Object[] {2, "Lokesh", "Gupta"});
//         data.put("4", new Object[] {3, "John", "Adwards"});
//         data.put("5", new Object[] {4, "Brian", "Schultz"});
//         
//       //Iterate over data and write to sheet
//         Set<String> keyset = data.keySet();
//         int rownum = 0;
//         for (String key : keyset) {
//        	 Row row = sheet.createRow(rownum++);
//             Object [] objArr = data.get(key);
//             int cellnum = 0;
//             for (Object obj : objArr)
//             {
//                Cell cell = row.createCell(cellnum++);
//                if(obj instanceof String)
//                     cell.setCellValue((String)obj);
//                 else if(obj instanceof Integer)
//                     cell.setCellValue((Integer)obj);
//             }
//			
//		}
         
       //Blank workbook
//         XSSFWorkbook workbook = new XSSFWorkbook(); 
//          
//         //Create a blank sheet
//         XSSFSheet sheet = workbook.createSheet("Employee Data");
//           
//         //This data needs to be written (Object[])
//         Map<String, Object[]> data = new TreeMap<String, Object[]>();
//         data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
//         data.put("2", new Object[] {1, "Amit", "Shukla"});
//         data.put("3", new Object[] {2, "Lokesh", "Gupta"});
//         data.put("4", new Object[] {3, "John", "Adwards"});
//         data.put("5", new Object[] {4, "Brian", "Schultz"});
//           
//         //Iterate over data and write to sheet
//         Set<String> keyset = data.keySet();
//         int rownum = 0;
//         for (String key : keyset)
//         {
//             Row row = sheet.createRow(rownum++);
//             Object [] objArr = data.get(key);
//             int cellnum = 0;
//             for (Object obj : objArr)
//             {
//                Cell cell = row.createCell(cellnum++);
//                if(obj instanceof String)
//                     cell.setCellValue((String)obj);
//                 else if(obj instanceof Integer)
//                     cell.setCellValue((Integer)obj);
//             }
//         }
//         try
//         {
//             //Write the workbook in file system
//             FileOutputStream out = new FileOutputStream(new File("howtodoinjava_demo.xlsx"));
//             workbook.write(out);
//             out.close();
//             System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
//         } 
//         catch (Exception e) 
//         {
//             e.printStackTrace();
//         }
//     }
		 
//		Map<Integer, ArrayList<String>> data = new LinkedHashMap<Integer, ArrayList<String>>();
//		
//		Calendar kal = Calendar.getInstance();
//		int before = 2000;
//		int after = 2010;
//		
//		for (int year = before; year <= after; year++) {
//			if (! data.containsKey(year)){
//				data.put(year, new ArrayList<String>());
//			}
//			
//		}
////		  System.out.println(data.keySet());
//
//			  String[] months = new DateFormatSymbols().getMonths();
//		        for (String month : months) {
//		            System.out.println("month = " + month + data.keySet());
//		        }
		  
		  
		 
//		int year = 2010;
//		while (year <2015) {
//			year++;
//			System.out.println("year :" + year);
//		}

		  

		 
//		 BigDecimal a = new BigDecimal(714583);
//		 BigDecimal b = new BigDecimal(70000000);
//		 BigDecimal c = new BigDecimal(1200);
//		 BigDecimal hasil = new BigDecimal(0);
//		 hasil = hasil.setScale(2, RoundingMode.HALF_UP);
//		 a = a.setScale(2, RoundingMode.HALF_UP);
//		 b = b.setScale(2, RoundingMode.HALF_UP);
		 
//		 hasil = a.divide(b,5, RoundingMode.HALF_UP).multiply(c);
//		 hasil = hasil.setScale(2, RoundingMode.HALF_UP);
//		 System.out.println("hasil :" + hasil);
		 
//		List<HashMap> map = new ArrayList<HashMap>();
//		int a = 100;
//		int b = a;
//		int c = 5;
//		int d = 6;
//		
//		for (int i = 1; i < 10; i++){
//			HashMap temp = new HashMap();
////			int e = a;
////			d = a - c;
//			b = b - c;
//			map.add(temp);
//			 System.out.println("" + b);
//			 
//		}
//		
//		int e = b;
//		int f = e;
//		
//		List<HashMap> MapResult2 = new ArrayList<HashMap>();
//		for (int i = 0; i < map.size(); i++) {
//			f = f - d;
//			
//			System.out.println("" + f);
//		}
//		
//		for (HashMap hashMap : map){
//			System.out.println("" + b);
//		}

		
//		int[] values = { 0, 10, 20, 30, 40, 50 };
//
//		// Copy elements from index 2 to 5 (2, 3, 4).
//		int[] result = Arrays.copyOfRange(values, 1, 5);
//		for (int value : result) {
//		    System.out.println(value);
//		}
		
//		 int[] array1 = {1, 3, 5};
//			String[] array2 = {"frog", "toad", "squirrel"};
//
//			// Array lengths.
//			System.out.println(array1.length);
//			System.out.println(array2.length);
//
//			// First elements in each array.
//			System.out.println(array1[2]);
//			System.out.println(array2[1]);
			
//			int[] values = { 0, 10, 20, 30, 40, 50 };

			// Copy elements from index 2 to 5 (2, 3, 4).
//			int[] result = Arrays.copyOfRange(values, 2, 3);
//			for (int value : result) {
//			    System.out.println(value);
//			}
		    
		
//		List<HashMap> MapResult2 = new ArrayList<HashMap>();
//		for (HashMap hashMap : map) {
//			for (i = 5; i < 6;i++){
//			Object[] st = map.toArray();
//			for (Object s : st) {
//				if (map.indexOf(s) != map.lastIndexOf(s)){
//					map.remove(map.lastIndexOf(s));
//				}
//			}
				
//			}
//		}
		

//		 System.out.println("Distinct List "+ map);
//		 ArrayList<String> lst = new ArrayList<String>();
//		    lst.add("ABC");
//		    lst.add("ABC");
//		    lst.add("ABCD");
//		    lst.add("ABCD");
//		    lst.add("ABCE");
//
//		    System.out.println("Duplicates List "+lst);
//
//		    Object[] st = lst.toArray();
//		      for (Object s : st) {
//		        if (lst.indexOf(s) != lst.lastIndexOf(s)) {
//		            lst.remove(lst.lastIndexOf(s));
//		         }
//		      }
//
//		    System.out.println("Distinct List "+lst);
		    
//		int a = 100;
//		int b = a;
//		int c = 0;
//		int k = 0;
//		int waktu = 10;
//		int sum = 0;
//
//		for (int i = 0; i < waktu; i++){
//			if (i < 6) {
//			HashMap temp = new HashMap();
//			int d = a;
//			int e = d;
//			int f = 0;
//			b = b - 5;
//			e = d - 5;
//			c += b;
//			temp.put("b", b);
//			temp.put("e", e);
//			map.add(temp);
//			System.out.println("" + b);
//			}
//		}
//		
//		System.out.println("" + b);
//		System.out.println("==========");
//		System.out.println("" + c);
//		 
//		
//		for (int i = 0; i < waktu; i++){
//			if (i > 5){
//				HashMap temp = new HashMap();
//				int g = b;
//				int j = g - 5;
//				k += j;
//				temp.put("j", j);
//				map.add(temp);
//				System.out.println("" + j);
//				
//			}
//		}
//		System.out.println("==========");
//		System.out.println("" + k);
//		
//		sum = c + k;
//		System.out.println("==========");
//		System.out.println("Total" + sum);
//		double a = 40000000;
//		double b = 0.35;
//		double c = 40000000 * 0.35 / 100;
//		
//		int d = (int) c;
//		System.out.println("c :" + d);
//		 String tes = "";
//		for (int i = 1; i < 5; i++){
//			System.out.println("No :" + i);
//			if (i < 2){
//				 tes = "a";
//			} else {
//				 tes = "b";
//			}
//			System.out.println("tes" + tes);
//		}
		
	 }

	private static boolean verify(String string) {
		// TODO Auto-generated method stub
		return false;
	}
}
