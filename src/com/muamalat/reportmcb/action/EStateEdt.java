package com.muamalat.reportmcb.action;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.Customer;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.EstateFunction;
import com.muamalat.reportmcb.parameter.Parameter;

public class EStateEdt extends org.apache.struts.action.Action {
	/* forward name="success" path="" */

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(EStateEdt.class);

	/**
	 * Action called on save button click
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}
//		try {
			E_stat_cust e_cust = (E_stat_cust)request.getSession(true).getAttribute("cust");
        	String email_to = request.getParameter("email_to");
        	String email_cc = request.getParameter("email_cc");
        	String updtsts = request.getParameter("updtsts");
        	if(e_cust != null){
            	EstateFunction ef = new EstateFunction();
            	e_cust.setEmail(email_to);
            	e_cust.setEmail_cc(email_cc);
            	e_cust.setSts_proc("I");
            	e_cust.setBranch_reg(user.getKodecabang());
            	e_cust.setUser_input(user.getUsername());
            	Date myDate = new Date();
	        	java.sql.Timestamp dt_in = new Timestamp(myDate.getTime()); 
            	e_cust.setDt_input(dt_in);
            	e_cust.setEmail(email_to);
            	e_cust.setEmail_cc(email_cc);
            	e_cust.setSts_data(updtsts);
            	if("H".equals(updtsts.trim())){
            		e_cust.setSts_data_desc(Parameter.H);
            	} else if("O".equals(updtsts.trim())){
            		e_cust.setSts_data_desc(Parameter.O);
            	}
//            	e_cust.setDt_aut(dt_in);
            	
            	boolean ret = ef.updateEStateCust(e_cust);
            	if (ret){
        			request.setAttribute("respon","Update data berhasil.");
        			request.setAttribute("data", "konfirm");             		
            	} else {
        			request.setAttribute("respon","Update data berhasil.");
        			request.setAttribute("data", "konfirm");             		
            	}
            	
        	} else {
    			request.setAttribute("respon","Error update data.");
    			request.setAttribute("data", "konfirm");         		
        	}
    		return mapping.findForward("success");

//		} catch (Exception e) {
//			request.setAttribute("errpage", "Error : " + e.getMessage());
//    		return mapping.findForward("failed");
//		}

	}

	private String isValidUser(HttpServletRequest request) {

		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel);
			request.setAttribute("message","Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			/*================= tambahan baru ==============*/
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			List lroles = cpf.getRolesUser(adm.getUsername());
		 	int ada = 0;
		 	if (lroles.size() > 0) {
		 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
		 			String rl = it.next();		 		
		 			String role3 = cpf.getAllowedStatement(11, rl.toUpperCase());
		 			if (!"".equals(role3) && role3!=null){
		 				ada++;
		 			}
		 		}
			} 
		 	cpf= null;
		 	if (ada > 0){
				return SUCCESS;					
		 	} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
		 	}
			/*
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(11, user.getRole_id());
			if ("".equals(role) || role == null) {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if ((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)) {
					return SUCCESS;
				} else {
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";
				}
			}*/
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
		}
	}

}
