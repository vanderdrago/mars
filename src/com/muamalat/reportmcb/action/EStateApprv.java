package com.muamalat.reportmcb.action;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.EstateFunction;
import com.muamalat.reportmcb.parameter.Parameter;

public class EStateApprv extends org.apache.struts.action.Action {
	/* forward name="success" path="" */

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(EStateApprv.class);

	/**
	 * Action called on save button click
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}
		try {
			E_stat_cust cust = (E_stat_cust)request.getSession(true).getAttribute("cust_oto_apprv");
        	String btn = request.getParameter("btn");
			if (cust != null){
	        	EstateFunction ef = new EstateFunction();
	        	cust.setUser_aut(user.getUsername());
	        	cust.setSts_proc("C");
	        	Date myDate = new Date();
	        	java.sql.Timestamp dt_aut = new Timestamp(myDate.getTime());  
	        	cust.setDt_aut(dt_aut);
	        	if("Approve".equals(btn)){
	        		cust.setSts_send_email(false);
		        	boolean ret = ef.updateEStateCust(cust);
		        	if(ret) {
		    			request.setAttribute("respon","Otorisasi Berhasil.");
		    			request.setAttribute("data", "konfirm");
		        	} else {
		    			request.setAttribute("respon","Otorisasi gagal.");
		    			request.setAttribute("data", "konfirm");        		
		        	}	
	        	} else if("Reject".equals(btn)){
	        		cust.setSts_data("R");
	        		cust.setSts_data_desc(Parameter.R);
	        		boolean ret = ef.updateEStateCust(cust);
		        	if(ret) {
		    			request.setAttribute("respon","Reject Success.");
		    			request.setAttribute("data", "konfirm");
		        	} else {
		    			request.setAttribute("respon","Reject failed.");
		    			request.setAttribute("data", "konfirm");        		
		        	}
	        	}
			} else {
    			request.setAttribute("respon","Tidak ditemukan data nasabah e-Statement.");
    			request.setAttribute("data", "konfirm"); 
			}
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
		}
		return mapping.findForward("success");

	}

	private String isValidUser(HttpServletRequest request) {

		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel);
			request.setAttribute("message","Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			/*================= tambahan baru ==============*/
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			List lroles = cpf.getRolesUser(adm.getUsername());
		 	int ada = 0;
		 	if (lroles.size() > 0) {
		 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
		 			String rl = it.next();		 		
		 			String role3 = cpf.getAllowedStatement(10, rl.toUpperCase());
		 			if (!"".equals(role3) && role3!=null){
		 				ada++;
		 			}
		 		}
			} 
		 	cpf= null;
		 	if (ada > 0){
				return SUCCESS;					
		 	} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
		 	}
			/*
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(10, user.getRole_id());
			if ("".equals(role) || role == null) {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if ((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)) {
					return "success";
				} else {
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";
				}
			}*/
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
		}
	}

}
