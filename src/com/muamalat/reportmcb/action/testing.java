package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang.time.DateUtils;

public class testing {

	public static void main(String args[]) throws Exception{
		for (int i=1; i<10; i += 2)
		{
		    for (int k=0; k < (4 - i / 2); k++)
		    {
		        System.out.print(" ");
		    }
		    for (int j=0; j<i; j++)
		    {
		        System.out.print("*");
		    }
		    System.out.println("");
		}

		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMM");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    System.out.println(strDate);
	    
	    String dateOld = "10/2017";
	    Date date = new SimpleDateFormat("mm/yyyy").parse(dateOld);
	    String newDate = new SimpleDateFormat("yyyymm").format(date);
	    System.out.println("new date " + newDate);
	    
		
		Calendar cal = Calendar.getInstance();
		System.out.println("Before "+cal.getTime());  //Before Thu Jan 31 10:16:23 IST 2013
		
		cal.add(Calendar.MONTH, 1);
		System.out.println("After "+cal.getTime()); //After Thu Feb 28 10:16:23 IST 2013
		
		List<String> crunchifyList = new ArrayList<String>();
		 
		// add 4 different values to list
		crunchifyList.add("eBay");
		crunchifyList.add("Paypal");
		crunchifyList.add("Google");
		crunchifyList.add("Yahoo");
 
		// iterate via "for loop"
		System.out.println("==> For Loop Example.");
		for (int i = 0; i < crunchifyList.size(); i++) {
			System.out.println(crunchifyList.get(i));
		}
 
		// iterate via "New way to loop"
		System.out.println("\n==> Advance For Loop Example..");
		for (String temp : crunchifyList) {
			System.out.println(temp);
		}
 
		// iterate via "iterator loop"
		System.out.println("\n==> Iterator Example...");
		Iterator<String> crunchifyIterator = crunchifyList.iterator();
		while (crunchifyIterator.hasNext()) {
			System.out.println(crunchifyIterator.next());
		}
 
		// iterate via "while loop"
		System.out.println("\n==> While Loop Example....");
		int i = 0;
		while (i < crunchifyList.size()) {
			System.out.println(crunchifyList.get(i));
			i++;
		}
 
		
	}

	
}
