/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author Utis
 */
public class ViewMcbSKNAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewMcbSKNAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        String network = "SKNBI";        
        String rel_trn = (String) request.getParameter("rel_trn");        
        String rek_f_ac = (String) request.getParameter("rek_f_ac");         
        String rek_t_ac = (String) request.getParameter("rek_t_ac");
        String amont_a = (String) request.getParameter("amont_a");
		String amont_b = (String) request.getParameter("amont_b");
        
        
        List ltrn = new ArrayList();
        MonSqlFunction f = new MonSqlFunction();
        
        Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = formatter.format(today);
		//sDate = "20-05-2013";
        
		if ((rel_trn != null) || (rek_f_ac != null) || (rek_t_ac != null) || (amont_a != null) || (amont_b != null)){
        	String tempsql = ""; 
        	
        	if ((rel_trn == null || "".equalsIgnoreCase(rel_trn)) && (rek_f_ac == null || "".equalsIgnoreCase(rek_f_ac)) && (rek_t_ac == null || "".equalsIgnoreCase(rek_t_ac)) && (amont_a == null || "".equalsIgnoreCase(amont_a)) && (amont_b == null || "".equalsIgnoreCase(amont_b))){
        		tempsql = "where network= '"+network+"' and activation_dt = TO_DATE('"+sDate+"', 'DD-MM-YY')";
        	} else {
        		if (rel_trn == null) {
        			rel_trn = "";
            		}
            	if (rek_f_ac == null) {
            		rek_f_ac = "";
            		}
            	if (rek_t_ac == null) {
            		rek_t_ac = "";
            		}
            	if (amont_a == null) {
            		amont_a = "";
            		}
            	if (amont_b == null) {
            		amont_b = "";
            		}
				tempsql = "where (contract_ref_no='" + rel_trn.trim()
						+ "' or cust_ac_no = '" + rek_f_ac.trim()
						+ "' or cpty_ac_no  = '" + rek_t_ac.trim()
						+ "' or (txn_amount >= '" + amont_a.trim()
						+ "' and txn_amount <= '" + amont_b.trim()
						+ "')) and network = '" + network
						+"' and activation_dt = TO_DATE('"+sDate+"', 'DD-MM-YY')";
        	}
        	ltrn = f.searchMonRtgsMcb2(tempsql);
        }
		if (ltrn.size() == 0) {
			String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
            request.setAttribute("dataresponse", dataresponse);
            request.setAttribute("konfirmasi", "err");
        }
        request.setAttribute("ltrn", ltrn);
        request.getSession(true).setAttribute("ltrn", ltrn);
        
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
