package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.kurs;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.SqlFunctionNew;

public class KursAct extends org.apache.struts.action.Action{
	
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(KursAct.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try {
			System.out.println("1");
			List listKurs = new ArrayList();
			SqlFunctionNew sql = new SqlFunctionNew();
			String branch = request.getParameter("branch");
			System.out.println("branch :" + branch);
			int sawal = 1; int noOfRecords = 0; int noOfPages = 0; 
			noOfRecords = sql.getTotKurs(branch);
			System.out.println("record :" + noOfRecords);
			if (noOfRecords > 0){
				listKurs = sql.getKurs(); 
				
			    request.setAttribute("sawal", sawal); 
			    request.setAttribute("lph", listKurs); 
			    request.setAttribute("viewlph", "vph");
			    request.setAttribute("noOfRecords", noOfRecords);
			    request.setAttribute("noOfPages", noOfPages);
			    request.getSession(true).setAttribute("noOfRecordsRek", noOfRecords);
			  	request.getSession(true).setAttribute("noOfPagesRek", noOfPages);
			  	request.getSession(true).setAttribute("branch", branch);
			  	

			}return mapping.findForward(SUCCESS); 
			
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "Kurs.do")){
			List lphUserRef = (List) request.getSession(true).getAttribute("lphUserRef");
			request.setAttribute("lphUserRef", lphUserRef);
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}

	
}
