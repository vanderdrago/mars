package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.pendingTransaksi;
import com.muamalat.reportmcb.function.SqlFunctionNew;


public class pendingTransaksiAct extends org.apache.struts.action.Action{
	private static Logger log = Logger.getLogger(pendingTransaksiAct.class);
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser (request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		
		try {
			String area = request.getParameter("countySel");
			String brn = request.getParameter("stateSel");
			String testing = request.getParameter("tes");
			String test = brn.substring(0, 3);
			String kcu = request.getParameter("kcu");
			SqlFunctionNew sql = new SqlFunctionNew();
			List pending = new ArrayList();
			List Leod = new ArrayList(); 
			pendingTransaksi eod = null;
			
			String btn = request.getParameter("btn");
			if ("Cari".equals(btn)){
				pending = sql.getPendingTransaksi(test, area);
				if (pending.size() > 0 ){
					request.setAttribute("area", pending);
					request.setAttribute("userId", "userId");
					request.getSession(true).setAttribute("llsp", pending);
					request.getSession(true).setAttribute("userId", "userId");
				} else {
					request.setAttribute("respon", "Tidak ada pending transaksi untuk saat ini");
		            request.setAttribute("confrm","err");  
				}
			} 
			
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
		}
		
		return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level; int validLevel = 5; 
		  if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
			  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
			  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
			  return "loginpage"; 
		  }
	  
		  level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
		  if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
			  return "success";
		  }else { 
			  request.setAttribute("message", "Unauthorized Access!");
			  request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			  request.setAttribute("error_code", "Error_timeout"); return "msgpage";
		  }
	}
	
}
