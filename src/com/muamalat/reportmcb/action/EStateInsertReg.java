package com.muamalat.reportmcb.action;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jboss.logging.Logger;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.Customer;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.EstateFunction;
import com.muamalat.reportmcb.parameter.Parameter;

public class EStateInsertReg extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(EStateInsertReg.class);
    
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        E_stat_cust cust = (E_stat_cust) request.getSession(true).getAttribute("cust_e_state");
        if(cust != null){
        	String btn = request.getParameter("btn");
        	if("Submit".equals(btn)){
            	String email_to = request.getParameter("email_to");
            	String email_cc = request.getParameter("email_cc");
            	String updtsts = request.getParameter("updtsts");
            	String temp = "";
            	if (!"".equals(updtsts) && updtsts != null){
	            	if ("H".equals(updtsts.trim())){
	            		temp = Parameter.H;
	            	} else if ("O".equals(updtsts.trim())){
	            		temp = Parameter.O;
	            	}
	            	EstateFunction ef = new EstateFunction();
	            	boolean ret = ef.insertEStateCust(cust, email_to, email_cc, "I", user.getUsername(), user.getKodecabang(), updtsts, temp);
	            	if(ret){
	            		request.setAttribute("teks", "<b>Registrasi berhasil. Menunggu otorisasi.</b>");
	            	} else {
	            		request.setAttribute("teks", "<b>Registrasi gagal.</b>");
	            	}       
            	} else {
            		request.setAttribute("teks", "<b>Status belum dipilih.</b>");            		
            	}
        	} else {
        		request.getSession(true).setAttribute("cust_e_state", null);
        		request.setAttribute("teks", "<b>Registrasi Batal.</b>");
        	}
        } else {
			request.setAttribute("teks", "<b>Data nasabah tidak ditemukan.</b>");
        }

		request.setAttribute("respon", "ret");
        return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			/*================= tambahan baru ==============*/
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			List lroles = cpf.getRolesUser(adm.getUsername());
		 	int ada = 0;
		 	if (lroles.size() > 0) {
		 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
		 			String rl = it.next();		 		
		 			String role3 = cpf.getAllowedStatement(11, rl.toUpperCase());
		 			if (!"".equals(role3) && role3!=null){
		 				ada++;
		 			}
		 		}
			} 
		 	cpf= null;
		 	if (ada > 0){
				return SUCCESS;					
		 	} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
		 	}
			/*
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(11, user.getRole_id());
			if ("".equals(role) || role == null){
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)){
					return SUCCESS;					
				} else {	
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";						
				}
			}
			*/
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			//request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
	    }
    }
}
