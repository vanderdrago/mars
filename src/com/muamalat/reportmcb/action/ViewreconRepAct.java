package com.muamalat.reportmcb.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.ReportNeraca;
import com.muamalat.reportmcb.function.FileFunction;

public class ViewreconRepAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewreconRepAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        try{
	        List lph = new ArrayList();
	        FileFunction f = new FileFunction();
	        String tanggal = request.getParameter("tgl3");
	        String kdcab = request.getParameter("kdcab");
	        
	        String dir = "/data/report_recon/" +tanggal+ "/"+ kdcab;
	        //String dir = "c:/data/report_recon/" +tanggal+ "/"+ kdcab;
	        boolean isdirectory = new File(dir).getCanonicalFile().isDirectory();
            if (isdirectory) {
            	List fn = f.getRekonList(dir);
    	        if (fn.size() > 0) {
    	            Collections.sort(fn, new ReportNeraca.OrderByFilename());
    	        	request.setAttribute("fn", fn);
    	        	request.setAttribute("reconReport", "viewdata");
    	        	request.getSession(true).setAttribute("dir", dir);
    	        }else {
    	        	request.setAttribute("respon", "<br><br><b>Data Report Tidak Tersedia</b></br></br>");
    	        	request.setAttribute("reconReport", "notok");
    	        }
            }else {
	        	request.setAttribute("respon", "<br><br><b>Data Report Tidak Tersedia</b></br></br>");
	        	request.setAttribute("reconReport", "notok");
	        }
	        
	        
	        
	        return mapping.findForward(SUCCESS);
        } catch (Exception e) {
        	request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward("failed");
		}
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }

}
