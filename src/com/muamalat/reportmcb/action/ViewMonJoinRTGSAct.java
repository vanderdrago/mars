/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.JoinRelTrnRTGSMaster;
import com.muamalat.reportmcb.entity.PctbContractMaster;
import com.muamalat.reportmcb.entity.TRtiftsout;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author Utis
 */
public class ViewMonJoinRTGSAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewMonJoinRTGSAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
		MonSqlFunction f = new MonSqlFunction();
		String fr = (String) request.getParameter("fr");
		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = formatter.format(today);
//		sDate = "20-05-2013";
		List mcbltrn = new ArrayList();
		List joinltrn = new ArrayList();
		String network = "RTGSBI";
		if ("f".equals(fr)) {
			String rel_trn = (String) request.getParameter("rel_trn");
			String rek_f_ac = (String) request.getParameter("rek_f_ac");
			String rek_t_ac = (String) request.getParameter("rek_t_ac");
			String amont_a = (String) request.getParameter("amont_a");
			String amont_b = (String) request.getParameter("amont_b");
			mcbltrn = f.searchMonRtgsMcb(sDate, network, rel_trn, rek_f_ac,
					rek_t_ac, amont_a, amont_b);
		} else if ("u".equals(fr)) {
			String srv = (String) request.getParameter("srv");
			String sts = (String) request.getParameter("sts");
			mcbltrn = f.searchMonRtgsMcbu(sDate, network, srv, sts, "ROIB");
		}
		if (mcbltrn.size() > 0) {
			for (Iterator<PctbContractMaster> i = mcbltrn.iterator(); 
					i.hasNext();) {
				PctbContractMaster jmcb = i.next();
				String whrstr = " where relTRN ='" + jmcb.getContractRefNo()
						+ "' and FromAccNum = '" + jmcb.getCustAcNo() + "'";
				TRtiftsout jintf = f.searchjoinrtgs(whrstr);
				JoinRelTrnRTGSMaster jTrnRTGSM = new JoinRelTrnRTGSMaster();
				jTrnRTGSM.setContractRefNo(jmcb.getContractRefNo());
				jTrnRTGSM.setProdRefNo(jmcb.getContractRefNo());
				jTrnRTGSM.setBookingDt(jmcb.getBookingDt());
				jTrnRTGSM.setCheckerId(jmcb.getCheckerId());
				jTrnRTGSM.setCptyAcNo(jmcb.getCptyAcNo());
				jTrnRTGSM.setCptyName(jmcb.getCptyName());
				jTrnRTGSM.setCustAcNo(jmcb.getCustAcNo());
				jTrnRTGSM.setCustName(jmcb.getCustName());
				jTrnRTGSM.setCustBankcode(jmcb.getCptyBankcode());
				jTrnRTGSM.setDispatchRefNo(jmcb.getDispatchRefNo());
				jTrnRTGSM.setExceptionQueue(jmcb.getExceptionQueue());
				jTrnRTGSM.setMakerId(jmcb.getMakerId());
				jTrnRTGSM.setNetwork(jmcb.getNetwork());
				jTrnRTGSM.setPayDetails(jmcb.getPaymentDetails1());
				jTrnRTGSM.setTxnAmount(jmcb.getTxnAmount());
				jTrnRTGSM.setUdf_2(jmcb.getUdf2());
				if (jintf != null) {
					jTrnRTGSM.setUltimateBeneAcc(jintf.getUltimateBeneAcc());
					jTrnRTGSM.setUltimateBeneName(jintf.getUltimateBeneName());
					jTrnRTGSM.setBor(jintf.getBor());
					jTrnRTGSM.setRelTRN(jintf.getRelTRN());
					jTrnRTGSM.setStatus(jintf.getStatus());
					jTrnRTGSM.setTrn(jintf.getTrn());
				} else {
					jTrnRTGSM.setUltimateBeneAcc("");
					jTrnRTGSM.setUltimateBeneName("");
					jTrnRTGSM.setBor("");
					jTrnRTGSM.setRelTRN("");
					jTrnRTGSM.setStatus("");
					jTrnRTGSM.setTrn("");
				}
				joinltrn.add(jTrnRTGSM);
			}
		} 
		if (mcbltrn.size() == 0) {
				String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
	            request.setAttribute("dataresponse", dataresponse);
	            request.setAttribute("konfirmasi", "err");
		}
		
		request.setAttribute("ltrn", joinltrn);
		request.getSession(true).setAttribute("ltrn", joinltrn);
		return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//        if (level != validLevel) {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            //request.setAttribute("error_code", "Error_timeout");
//            return "msgpage";
//        }
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
