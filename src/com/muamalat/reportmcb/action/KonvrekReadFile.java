package com.muamalat.reportmcb.action;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.FileFunction;
import com.muamalat.reportmcb.function.ReadFileExcel;
import com.muamalat.reportmcb.parameter.Parameter;
import com.muamalat.reportmcb.perantara.PerantaraKonvRek;
import com.muamalat.reportmcb.perantara.PerantaraRpt;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jboss.logging.Logger;

public class KonvrekReadFile extends Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(KonvrekReadFile.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SUCCESS = isValidUser(request);

		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}

		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		try{
        	String fileName = (String) request.getSession(true).getAttribute("filename");
        	String opt = request.getParameter("btn");
        	if("Download".equals(opt)){
            	if (fileName != null){
            		ReadFileExcel uf = new ReadFileExcel();
                	FileInputStream fileInputStream = new FileInputStream(Parameter.READFILE + fileName);
                    HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream); 
                    if (workbook.getSheet("rekening") == null){
                    	fileInputStream.close();
                    	request.setAttribute("datanotok", "Tidak ditemukan sheet dengan nama 'rekening'.");
    	        		request.setAttribute("view", "notok");                	
                    } else { 
                    	fileInputStream.close();
                    	uf.prosesReadExcl(Parameter.READFILE, fileName);

    		        	List arrDatOk = uf.getArrDataOk();
    		        	List rejectData = uf.getArrRejectData();
    		        	if(rejectData.size() == 0){
    		        		PerantaraKonvRek pkr = new PerantaraKonvRek();
    		        		PerantaraRpt prpt = new PerantaraRpt();
    		        		List lkonvrek = pkr.ListKonvRek(arrDatOk);
    		        			
    		        		HashMap params = new HashMap();
    		                
    		        		JasperReport jasperReport;
    		                JasperPrint jasperPrint;
    		                try {
    		                    ArrayList arrListLrpp = prpt.moveListToHashMapKonvRek(lkonvrek);
    		                    byte[] bytes = null;
    		                    if (!arrListLrpp.isEmpty()) {
    		                        JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrListLrpp);
    		                        jasperReport = (JasperReport) JRLoader.loadObjectFromLocation("com/muamalat/reportmcb/report/KonversiRekening.jasper");
    		                        jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);

    	                            OutputStream ouputStream = response.getOutputStream();
    	                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    	                            JRXlsExporter exporterXls = new JRXlsExporter();
    	                            exporterXls.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
    	                            exporterXls.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);

    	                            exporterXls.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
    	                            exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
    	                            exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
    	                            //exporterXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
    	                            exporterXls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
    	                            exporterXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
    	                            exporterXls.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);

    	                            exporterXls.exportReport();
    	                            byte[] encData2 = byteArrayOutputStream.toByteArray();

    	                            response.setContentType("application/octet-stream");
    	                            response.setContentType("application/xls");
    	                            response.setHeader("Content-disposition", "attachment;filename=" + fileName);

    	                            ouputStream.write(encData2);
    	                            ouputStream.flush();
    	                            ouputStream.close();
    		                    }
    		                } catch (IOException ex) {
    		                    request.setAttribute("message", "Print failed ! <br/>Exception");
    		                    log.error(ex.getMessage());
    		                } catch (JRException ex) {
    		                    request.setAttribute("message", "Print failed ! <br/>Exception");
    		                    log.error(ex.getMessage());
    		                }
    		                return null;
    		        	} else {
    		        		request.setAttribute("rejectData", rejectData);
    		        		request.setAttribute("view", "reject");
    		        	}	
                    } 	        	
            	} 
                return mapping.findForward(SUCCESS);    
        	} else if("Clear".equals(opt)){
                FileFunction ff = new FileFunction();
                ff.deleteFile(Parameter.READFILE + fileName);
            	request.setAttribute("datanotok", "Data berhasil dibersihkan.");
        		request.setAttribute("view", "notok");  
        	}
        	 
            return mapping.findForward(SUCCESS);    
        }catch(Exception e){
            return mapping.findForward("errPage");        	
        }
	}

	private String isValidUser(HttpServletRequest request) {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,
					validLevel);
			request.setAttribute("message",
					"Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(
				StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5
				|| level == 6 || level == 7) {
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error",
					"Anda tidak memiliki akses ke halaman yang diminta!");
			return "msgpage";
		}
	}
}
