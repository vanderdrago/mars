package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.perantara.PerantaraRpt;

public class CetakPassbookHistView extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(CetakPassbookHistView.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

    	SUCCESS = isValidUser(request);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        CetakPassbookFunction cpf = new CetakPassbookFunction();
		Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
//		SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
//		String role3 = cpf.getAllowedPrintPassbook(10, user.getRole_id());
		
//		if ("".equals(role3) || role3 == null){
//			request.setAttribute("message", "Unauthorized Access!");
//			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//			return mapping.findForward("msgpage");	
//			
//		} else {
	        try{
//	            String rbn = request.getParameter("rbn"); 
	            String acc = request.getParameter("acc");  
	            String userid = request.getParameter("userid");    
	            String pengesah = request.getParameter("pengesah");         
	            int sawal = 1;
	            int sakhir = 10;
	            int page = 1;
	            int recordsPerPage = 30;
	    		int noOfRecords = 0;
	            int noOfPages = 0;
	            List lctkhst = new ArrayList();
	            List ltrnprinthist = new ArrayList();
	        	PerantaraRpt p = new PerantaraRpt();
	        	/*
	            if("acc".equals(rbn)){
		            if (acc != null){
		            	Reklist cust_acc = cpf.getReklist(acc);
		            	if (cust_acc != null){
		                    ltrnprinthist = cpf.getAcc_lineHist(rbn, acc, "", "", adm.getUsername()); 
		                    lctkhst = p.getAcc_lineHist(ltrnprinthist, 1, recordsPerPage);
		                	request.setAttribute("cust_acc", cust_acc);  
		                    request.getSession(true).setAttribute("cust_acc", cust_acc);        
		                    request.setAttribute("hdr", "data");  
		            	} 
		            } 		            	
	            } else if ("rbusr".equals(rbn)){
	            	ltrnprinthist = cpf.getAcc_lineHist(rbn, "", userid.toUpperCase(), "", adm.getUsername()); 
	                lctkhst = p.getAcc_lineHist(ltrnprinthist, 1, recordsPerPage);
	            }
	            */
            	ltrnprinthist = cpf.getAcc_lineHist(acc, userid, pengesah); 
                lctkhst = p.getAcc_lineHist(ltrnprinthist, 1, recordsPerPage);
	            if (lctkhst.size() > 0) {
	            	noOfRecords = 0;
	                noOfPages = 0;
	                noOfRecords = ltrnprinthist.size();
	                noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
	                if (noOfPages < sakhir){
	                	sakhir = noOfPages;
	                }  
	                request.setAttribute("sawal", sawal);
	                request.setAttribute("sakhir", sakhir);

	                request.setAttribute("noOfRecords", noOfRecords);
	                request.setAttribute("noOfPages", noOfPages);
	                request.setAttribute("currentPage", page);
	                request.getSession(true).setAttribute("noOfRecords", noOfRecords);
	                request.getSession(true).setAttribute("noOfPages", noOfPages);
	                request.getSession(true).setAttribute("ltrnprinthist", ltrnprinthist);
	                request.setAttribute("lctkhst", lctkhst);
	                request.setAttribute("data", "viewdata");
	            } else {
	            	request.setAttribute("teks", "Tidak ditemukan history cetak passbook.");  
	            	request.setAttribute("respon", "teks");                	
	            }
	            return mapping.findForward(SUCCESS);
	        }
	        catch(Exception e){
	        	request.setAttribute("errpage", "Error : " + e.getMessage());
	            return mapping.findForward("failed");        	
	        }	
		}

//    }
        
        
    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {			
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(9, user.getRole_id());
			if ("".equals(role) || role == null){
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "success";
			} else {
				if((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)){
					return "success";					
				} else {	
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";						
				}
			}	
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			return "msgpage";
	    }
    }

}
