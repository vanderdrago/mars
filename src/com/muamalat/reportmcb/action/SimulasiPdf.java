package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.kpr;
import com.muamalat.reportmcb.function.SqlFunction;

public class SimulasiPdf extends org.apache.struts.action.Action{
	
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(simulasiKPRactMarketing.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try {

			List<HashMap> tes = (List<HashMap>) request.getSession(true).getAttribute("kpr");
			
			for (HashMap map : tes) {
				System.out.println("angsuran :" + map.get("resultPlafond"));
				System.out.println("sisaAngsuran :" + map.get("sisaAngsuran") + "Margin :" + map.get("sisaMargin"));
			}
			
			String plafond = (String) request.getSession(true).getAttribute("plafond");
			BigDecimal hargaJual = (BigDecimal) request.getSession(true).getAttribute("sumAngsuran");
			BigDecimal totalMargin = (BigDecimal) request.getSession(true).getAttribute("sumMargin");
			
			List<HashMap> file = (List<HashMap>) request.getSession(true).getAttribute("kpr");
			List<HashMap> result = (List<HashMap>) request.getSession(true).getAttribute("MapResult2");
			System.out.println("plafond :" + plafond);
			System.out.println("Harga Jual :" + hargaJual);
			System.out.println("Total Margin :" + totalMargin);
			System.out.println("MapResult2 :" + tes);

			request.setAttribute(plafond, "plafond");
			
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
	}
	
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "simulasiKPR5th.do")){
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
}
