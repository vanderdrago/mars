package com.muamalat.reportmcb.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.User;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.SqlFunctionNew;
import com.muamalat.reportmcb.perantara.PerantaraRpt;
import com.sun.net.httpserver.Authenticator.Success;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

public class quickCifAct extends org.apache.struts.action.Action {
	private static Logger log = Logger.getLogger(quickCifAct.class);
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser (request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		
		if (!SUCCESS.equals("success")){
			return mapping.findForward(SUCCESS);
		}
		try {

			CetakPassbookFunction cpf = new CetakPassbookFunction();
			SqlFunctionNew sql = new SqlFunctionNew();
			
			request.getSession(true).setAttribute("branch", null);
			String branch = request.getParameter("branch");
			String chkCab = request.getParameter("chkBranch");
			String startDate = request.getParameter("StartDate");
			String endDate = request.getParameter("EndDate");
//			String opt = request.getParameter("btndwnld");
			String send = request.getParameter("btnsend");
    		String cetak = request.getParameter("btndwnld");
    		System.out.println("startDate :" + startDate);
    		System.out.println("endDate :" + endDate);
    		String pathjasper = "";
    		
			int noOfRecords = 0; 
			int sawal = 1;
			int sakhir = 10; 
			int page = 1; 
			int noOfPages = 0; 
			int recordsPerPage = 1000;
			
    		System.out.println("btn send :" + send);
    		System.out.println("btn cetak :" + cetak);
    		if ("send".equals(send)){
    			List lph = sql.getQuickCif(branch, chkCab, startDate, endDate);
        		if (lph.size() > 0){
        			request.setAttribute("listQuickCif", lph);
      			    request.setAttribute("sakhirCif", sakhir); 
	      			request.setAttribute("noOfRecordsCif", noOfRecords);
	    			request.setAttribute("noOfPagesCif", noOfPages); 
	    			request.setAttribute("currentPageCif", page); 
	    			request.getSession(true).setAttribute("pageCif", page); 
	    			request.getSession(true).setAttribute("recordsCif", noOfRecords);
	  			    request.getSession(true).setAttribute("noOfPagesCif", noOfPages);
        			request.getSession(true).setAttribute("llsp", lph);
        			request.getSession(true).setAttribute("branch", branch);
        		} else {
        			String respon = "Tidak ditemukan Record Data yang Dicari.";
        			request.setAttribute("respon", respon);
        			request.setAttribute("konfirmasi", "err");
        		}
    		} else if ("Cetak".equals(cetak)){
    			List cetakCif = sql.getCetakQuickCif(branch, chkCab, startDate, endDate);
    			if (cetakCif.size() > 0){
    				System.out.println("cetakCif size :" + cetakCif.size());
    				pathjasper = "com/muamalat/reportmcb/report/RPT_QUICK_ALL.jasper";
    				JasperReport jasper;
		    		JasperPrint jasperPrint;
		    		String formatFile = "pdf";
		    		System.out.println("pathJasper :" + pathjasper);
		    		System.out.println("file :" + formatFile);
		    		
		    		byte[] bytes = null;
   					PerantaraRpt p = new PerantaraRpt();
   					HashMap param = new HashMap();
	   			 	DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
	            	Date dateee = new Date();
	            	String fileName = "RPT_QUICK_CIF_" + branch;
//	            	System.out.println("branch :" + branch);
	            	System.out.println("fileName :" + fileName);
	                ArrayList arrHist = null;
	                
	                if (cetakCif.size() > 0){
	                	arrHist = p.moveListToHashMapCif(cetakCif);
	                	System.out.println("arrHist: " + arrHist);
            	    	System.out.println("size lph: " + cetakCif.size());
	                } else {
	                	HashMap rowMap = null;
	                    ArrayList detail = new ArrayList();
	                    rowMap = new HashMap();
	                    rowMap.put("tglLaporan","");
	        			rowMap.put("customerNo", "");
	        			rowMap.put("customerName", "");
	        			rowMap.put("custAcNo", "");
	        			rowMap.put("branchCode", "");
	        			rowMap.put("parentBranch", "");
	        			rowMap.put("functionId", "");
	        			rowMap.put("modNo", "");
	        			rowMap.put("makerId", "");
	        			rowMap.put("checkerId", "");
	        			rowMap.put("tglUpdateCif", "");
	        			rowMap.put("checkerDtStamp", "");
	        			detail.add(rowMap);
	                    arrHist = detail;
	                }
	                System.out.println("Deklarasi Print Jasper");
					JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
					System.out.println("1");
					log.error(ds);
					jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathjasper);
					System.out.println("2");
					log.error(jasper);
					jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
					log.error(jasperPrint);
					System.out.println("3");
					
					JRExporter exporter = new JRPdfExporter();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					response.setContentType("application/pdf");
					response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
					exporter.exportReport();
					
					bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                       
                    System.out.println("jasperPrint" + jasperPrint);
                    System.out.println("bytes" + bytes);
                       
                    response.getOutputStream().write(bytes, 0, bytes.length); 
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
    			}
    		}
			
//			User usr = sql.getUser(userId);
//			
////			if (usr != null) {
//				List lphCs = sql.GetLapSpvCs(userId,sawal,recordsPerPage);
//				List lphCsCetak = sql.GetLapSpvCsCetak(userId);
//				String pathjasper = "";
//				
//				noOfRecords = sql.GetTotLapSpvCs(userId);
//				if (noOfRecords > 0){
//					noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
//					if (noOfPages < sakhir) {
//						  sakhir = noOfPages;
//					}
////					lphCs = sql.GetLapSpvCs(userId,sawal,recordsPerPage);
//					  request.setAttribute("sawalCs", sawal);
//					  request.setAttribute("sakhirCs", sakhir);
//					  request.setAttribute("llsp", lphCs); 
//					  request.setAttribute("noOfRecordsCs", noOfRecords);
//					  request.setAttribute("noOfPagesCs", noOfPages);
//					  request.setAttribute("currentPageCs", page);
//					  request.getSession(true).setAttribute("PageCs", page);
//					  request.getSession(true).setAttribute("llsp", lphCs); 
//					  request.getSession(true).setAttribute("recordsCs", noOfRecords);
//					  request.getSession(true).setAttribute("OfPagesCs", noOfPages);
//					  request.getSession(true).setAttribute("userId", userId); 
//				}
//				
////				if (lphCs.size() > 0) {
////					pathjasper = "com/muamalat/reportmcb/report/statement_monitoring_cs.jasper";
////					request.setAttribute("lphCs", lphCs);
////					request.getSession(true).setAttribute("lphCs", lphCs);
////				} 
//				//tambahan untuk download pdf MEI 2016
//				
//		    	if ("Cetak".equals(opt)){
//		    		if (lphCsCetak.size() > 0){
////	    				System.out.println("lph size :" + lph.size());
//	    				System.out.println("masuk if lph size");
//	    				pathjasper = "com/muamalat/reportmcb/report/statement_monitoring_cs.jasper";
//	    				JasperReport jasper;
//			    		JasperPrint jasperPrint;
//			    		String formatFile = "pdf";
//			    		System.out.println("pathJasper :" + pathjasper);
//			    		System.out.println("file :" + formatFile);
//			    		
//			    		byte[] bytes = null;
//	   					PerantaraRpt p = new PerantaraRpt();
//	   					HashMap param = new HashMap();
//		   			 	DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
//		            	Date dateee = new Date();
////		            	String fileName = "RPT_QUICK_CIF_" + branch;
////		            	System.out.println("branch :" + branch);
//		            	String fileName = "Statement_" + userId;
//		            	System.out.println("fileName :" + fileName);
//		                ArrayList arrHist = null;
//		                
//		                if (lphCsCetak.size() > 0){
//		                	arrHist = p.moveListToHashMapUserCs(lphCsCetak);
//		                	System.out.println("arrHist: " + arrHist);
//	            	    	System.out.println("size lph: " + lphCsCetak.size());
//		                } else {
//		                	HashMap rowMap = null;
//	                        ArrayList detail = new ArrayList();
//							rowMap.put("branch_code", "");
//							rowMap.put("branch_name", "");
//							rowMap.put("tanggal", "");
//							rowMap.put("jam", "");
//							rowMap.put("key_id", "");
//							rowMap.put("maker_id", "");
//							rowMap.put("checker_id", "");
//							rowMap.put("function_id", "");
//							rowMap.put("modifikasi_ke", "");
//							detail.add(rowMap);
//		                    arrHist = detail;
//		                }
//		                System.out.println("Deklarasi Print Jasper");
//						JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
//						System.out.println("1");
//						log.error(ds);
//						jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathjasper);
//						System.out.println("2");
//						log.error(jasper);
//						jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
//						log.error(jasperPrint);
//						System.out.println("3");
//						
//						JRExporter exporter = new JRPdfExporter();
//						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//						response.setContentType("application/pdf");
//						response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
//						exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//						exporter.exportReport();
//						
//						bytes = JasperExportManager.exportReportToPdf(jasperPrint);
//	                       
//	                    System.out.println("jasperPrint" + jasperPrint);
//	                    System.out.println("bytes" + bytes);
//	                       
//	                    response.getOutputStream().write(bytes, 0, bytes.length); 
//	                    response.getOutputStream().flush();
//	                    response.getOutputStream().close();
//		    		}
//		    	}
//			}
//					} else {
//						request.setAttribute("message", "Unauthorized Access!");
//						request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
//						return mapping.findForward("msgpage");
//	   				}
//				} else {
//		    		request.setAttribute("lphCs", lphCs);
//		    		request.setAttribute("sawalCs", sawal);
//					request.setAttribute("sakhirCs", sakhir);
//					request.setAttribute("noOfRecordsCs", noOfRecords);
//					request.setAttribute("noOfPagesCs", noOfPages);
//					request.setAttribute("currentPageCs", page);
//					request.getSession(true).setAttribute("PageCs", page);
//					request.getSession(true).setAttribute("llsp", lphCs); 
//					request.getSession(true).setAttribute("recordsCs", noOfRecords);
//					request.getSession(true).setAttribute("OfPagesCs", noOfPages);
//					request.getSession(true).setAttribute("userId", userId);
//		    	}
//			} else {
//	    		request.setAttribute("respon", "Transaksi tidak ditemukan.");
//	            request.setAttribute("confrm","err");     
//	    	}
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
		}
		return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level; int validLevel = 5; 
		  if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
			  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
			  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
			  return "loginpage"; 
		  }
	  
		  level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
		  if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
			  return "success";
		  }else { 
			  request.setAttribute("message", "Unauthorized Access!");
			  request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			  request.setAttribute("error_code", "Error_timeout"); return "msgpage";
		  }
	}
}
