/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.MonSqlFunction;
import com.muamalat.reportmcb.function.SqlFunction;

/**
 *
 * @author Utis
 */
public class ViewGetAltaccAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewGetAltaccAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {        	
            return mapping.findForward(SUCCESS);
        }
        
        try{
//        	request.getSession(true).setAttribute("accno", null);
//            request.getSession(true).setAttribute("name", null);
//            
//    		MonSqlFunction f = new MonSqlFunction();
//    		String accno = request.getParameter("accno");
//    		String name = request.getParameter("name");
//    		List alt = f.getListAltacc(accno, name);
//    		if (alt.size() > 0) {
//    			request.setAttribute("alt", alt);
//                request.getSession(true).setAttribute("alt", alt);
//    		}else {
//    			request.setAttribute("dataresponse", "No Rekening tidak ditemukan");
//                request.setAttribute("konfirmasi", "err");
//    		}
//            return mapping.findForward(SUCCESS);
        	
          List alt = new ArrayList(); 
          MonSqlFunction sql = new MonSqlFunction();
          String accno = request.getParameter("accno");
          String name = request.getParameter("name");
          
  		  int noOfRecords = 0;
  		  int sawal = 1;
  		  int sakhir = 10; 
  		  int page = 1; 
  		  int noOfPages = 0; 
  		  int recordsPerPage = 30;
          
  		  noOfRecords = sql.getTotAltacc(accno, name.toUpperCase());
		  if (noOfRecords > 0) {
			  noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
			  if (noOfPages < sakhir) {
				  sakhir = noOfPages;
			  }
			  alt = sql.getListAltacc(accno, name.toUpperCase(), sawal - 1);
			  request.setAttribute("sawalAlt", sawal); 
			  request.setAttribute("sakhirAlt", sakhir); 
			  request.setAttribute("alt", alt); 
			  request.setAttribute("noOfRecordsAlt", noOfRecords);
			  request.setAttribute("noOfPagesAlt", noOfPages); 
			  request.setAttribute("currentPageAlt", page); 
			  request.getSession(true).setAttribute("pageAlt", page); 
			  request.getSession(true).setAttribute("llsp", alt); 
			  request.getSession(true).setAttribute("recordsAlt", noOfRecords);
			  request.getSession(true).setAttribute("noOfPagesAlt", noOfPages);
			  request.getSession(true).setAttribute("name", name); 
			  request.getSession(true).setAttribute("accno", accno);
		  } return mapping.findForward(SUCCESS); 
        } catch (Exception e){
        	request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
        }
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
