package com.muamalat.reportmcb.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.User;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.perantara.PerantaraRpt;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

public class LapSpvAct extends org.apache.struts.action.Action {
	/* forward name="success" path="" */
	private static Logger log = Logger.getLogger(LapSpvAct.class);
	private static String SUCCESS = "success";
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request,HttpServletResponse response) 
			throws Exception {
		
		SUCCESS = isValidUser (request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		System.out.println("masuk view Laporan CS");
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try {
//			List lphTeller = new ArrayList();
//			CetakPassbookFunction cpf = new CetakPassbookFunction();
//			SqlFunction sql = new SqlFunction();
//			
//			request.getSession(true).setAttribute("user", null);
//			
//			String userid = request.getParameter("user");
//			User usr = sql.getUser(userid);
//			
//			if (usr != null) {
//			    lphTeller = sql.GetListLapSpvTeller(userid);
//			    System.out.println("masuk usr");
//				String pathJasper = "";
//				if (lphTeller.size() > 0) {
//					pathJasper = "com/muamalat/reportmcb/report/statement_monitoring_teller.jasper";
//				}
//				String opt = request.getParameter("btndwnld");
//				if ("Cetak".equals(opt)) {
//					JasperReport jasper;
//					JasperPrint jasperPrint;
//					String formatFile = "pdf";
//					
//					/*tambahan role*/
//        			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
//        			SMTB_USER_ROLE userRole = cpf.getUser(adm.getUsername(), adm.getKodecabang());
//        			String role = cpf.getAllowedPrintPassbook(12, userRole.getRole_id());
//        			
//        			/*================= tambahan baru ==============*/
//        			List lroles = cpf.getRolesUser(adm.getUsername());
//   				 	int ada = 0;
//   				 	if (lroles.size() > 0) {
//   				 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
//   				 			String rl = it.next();
//   				 			String role2 = cpf.getAllowedStatement(12, rl.toUpperCase());
//   				 			if (!"".equals(role2) && role2!=null){
//   				 				ada++;
//   				 			}
//   				 		}
//	   				} 
//   				 	
//   				 	if ("000".equals(adm.getKodecabang()) || ada > 0) {
//   				 		byte[] bytes = null;
//   				 		PerantaraRpt p = new PerantaraRpt();
//	   				 	HashMap param = new HashMap();
//	   				 	DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
//	            	    Date dateee = new Date();
//	            	    param.put("tanggal", dateFormattt.format(dateee));
////	            	    param.put("cabang", usr.getBranch_code());
//	            	    String fileName = "Statement_" + userid;
//	            	    ArrayList arrHist = null;
//	            	    
//	            	    if(lphTeller.size() > 0) {
//	            	    	arrHist = p.moveListToHasMapTeller(lphTeller);
//	            	    	System.out.println("arrHist :" +arrHist);
//	            	    } else {
//	            	    	HashMap rowMap = null;
//	            	    	ArrayList detail = new ArrayList();
//	                        rowMap = new HashMap();
//	                        rowMap.put("user_id", "");
//                            rowMap.put("branch", "");
//                            rowMap.put("jam", "");
//                            rowMap.put("trn_code", "");
//                            rowMap.put("ac_no", "");
//                            rowMap.put("no_warkat", "");
//                            rowMap.put("trn_ref_no", "");
//                            rowMap.put("function_desc", "");
//                            rowMap.put("drcr_ind", "");
//                            rowMap.put("nominal", "");
//                            rowMap.put("user_id", "");
//                            rowMap.put("auth_id", "");
//                            detail.add(rowMap);
//	                        arrHist = detail;
//	                        System.out.println("rowMap :" +rowMap);
//	            	    }
//	            	    
//	            	    JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
//	            	    jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathJasper);
//	            	    jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
//	            	    JRExporter exporter = new JRPdfExporter();
//	            	    
//	            	    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//	            	    response.setContentType("application/pdf");
//                        response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
//                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//                        exporter.exportReport();
//                        
//                        bytes = JasperExportManager.exportReportToPdf(jasperPrint);
//                        response.getOutputStream().write(bytes, 0, bytes.length);
//                        
//                        response.getOutputStream().flush();
//                        response.getOutputStream().close();
//   				 	} else {
//	   				 	request.setAttribute("message", "Unauthorized Access!");
//						request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
//						return mapping.findForward("msgpage");
//   				 	} 
//				} else {
//					request.setAttribute("lphTeller", lphTeller);
//	                request.getSession(true).setAttribute("lphTeller", lphTeller);
//	                System.out.println("masuk kesini else 1");
//	                System.out.println("lph :" + lphTeller.size());
//				} 
//			} else {
//				request.setAttribute("respon", "Transaksi tidak ditemukan.");
//	            request.setAttribute("confrm","err");     
//	            System.out.println("masuk kesini else 2");
//			}
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			SqlFunction sql = new SqlFunction();
			
			request.getSession(true).setAttribute("user", null);
			String userId = request.getParameter("user");
			String send = request.getParameter("btn");
    		String cetak = request.getParameter("btndwnld");
    		String pathjasper = "";
    		System.out.println("btn send :" + send);
    		System.out.println("btn cetak :" + cetak);
			int noOfRecords = 0; 
			int sawal = 1;
			int sakhir = 10; 
			int page = 1; 
			int noOfPages = 0; 
			int recordsPerPage = 30; 
			
			List lphTeller = sql.GetListLapSpvTeller(userId);
    		System.out.println("size :" + lphTeller.size());
    		if ("Cari".equals(send)){
    			System.out.println("masuk if send");
        		if (lphTeller.size() > 0){
        			request.setAttribute("lphTeller", lphTeller);
        			request.getSession(true).setAttribute("llsp", lphTeller);
        			request.getSession(true).setAttribute("userId", userId);
        		} else {
        			String respon = "Tidak ditemukan Record Data yang Dicari.";
        			request.setAttribute("respon", respon);
        			request.setAttribute("konfirmasi", "err");
        		}
    		} else if ("Cetak".equals(cetak)){
    			System.out.println("masuk else if Cetak");
    			if (lphTeller.size() > 0){
    				System.out.println("lph size :" + lphTeller.size());
    				System.out.println("masuk if lph size");
    				pathjasper = "com/muamalat/reportmcb/report/statement_monitoring_teller.jasper";
    				JasperReport jasper;
		    		JasperPrint jasperPrint;
		    		String formatFile = "pdf";
		    		System.out.println("pathJasper :" + pathjasper);
		    		System.out.println("file :" + formatFile);
		    		
		    		byte[] bytes = null;
   					PerantaraRpt p = new PerantaraRpt();
   					HashMap param = new HashMap();
	   			 	DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
	            	Date dateee = new Date();
	            	String fileName = "Statement_" + userId;
//	            	System.out.println("branch :" + branch);
	            	System.out.println("fileName :" + fileName);
	                ArrayList arrHist = null;
	                
	                if (lphTeller.size() > 0){
	                	arrHist = p.moveListToHasMapTeller(lphTeller);
	                	System.out.println("arrHist: " + arrHist);
            	    	System.out.println("size lph: " + lphTeller.size());
	                } else {
	                	HashMap rowMap = null;
            	    	ArrayList detail = new ArrayList();
                        rowMap = new HashMap();
                        rowMap.put("user_id", "");
                        rowMap.put("branch", "");
                        rowMap.put("jam", "");
                        rowMap.put("trn_code", "");
                        rowMap.put("ac_no", "");
                        rowMap.put("no_warkat", "");
                        rowMap.put("trn_ref_no", "");
                        rowMap.put("function_desc", "");
                        rowMap.put("drcr_ind", "");
                        rowMap.put("nominal", "");
                        rowMap.put("user_id", "");
                        rowMap.put("auth_id", "");
                        detail.add(rowMap);
	                    arrHist = detail;
	                }
	                System.out.println("Deklarasi Print Jasper");
					JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
					log.error(ds);
					jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathjasper);
					log.error(jasper);
					jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
					log.error(jasperPrint);
					
					JRExporter exporter = new JRPdfExporter();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					response.setContentType("application/pdf");
					response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
					exporter.exportReport();
					
					bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                       
                    System.out.println("jasperPrint" + jasperPrint);
                    System.out.println("bytes" + bytes);
                       
                    response.getOutputStream().write(bytes, 0, bytes.length); 
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
    			}
    		}
		} catch (Exception e) {

            System.out.println("masuk kesini catch");
        	request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
		}
		return mapping.findForward(SUCCESS);
//		try {
//			request.getSession(true).setAttribute("user", null);
//			SqlFunction sql = new SqlFunction();
//			String userid = request.getParameter("user");
//			List lphTeller = sql.GetListLapSpvTeller(userid);
//			if (lphTeller.size() > 0) {
//				System.out.println("lphteller :" + lphTeller.size());
//				request.setAttribute("lphTeller", lphTeller);
//                request.getSession(true).setAttribute("lphTeller", lphTeller);
//			} else {
//				request.setAttribute("dataresponse", "Tidak ditemukan transaksi hari ini");
//                request.setAttribute("konfirmasi", "err");
//			}
//			return mapping.findForward(SUCCESS);
//		} catch (Exception e){
//        	request.setAttribute("errpage", "Error : " + e.getMessage());
//            return mapping.findForward(".errPage");
//		}
		
	}

		private String isValidUser(HttpServletRequest request) {
			if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
				request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
	            return "loginpage";
			}
			List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
			SqlFunction sql = new SqlFunction();
			if (sql.getMenuStatus(lrole, "LapSpvTeller.do")){
				List lphTeller = (List) request.getSession(true).getAttribute("lphTeller");
				request.setAttribute("lphTeller", lphTeller);
				return "success";
			} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
			}
		}
}
