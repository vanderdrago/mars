package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.prks;
import com.muamalat.reportmcb.function.SqlFunctionNew;

public class detailPrks extends org.apache.struts.action.Action{
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
			String acc = (String) request.getSession(true).getAttribute("accNo");
			String prd1 = (String) request.getParameter("tgl1");
			String prd2 = (String) request.getParameter("tgl2");
			System.out.println("acc:" + acc + "prd1:" + prd1 + "prd2:" + prd2);
			List detail = new ArrayList();
			SqlFunctionNew sql = new SqlFunctionNew();
			prks head = sql.getHeadPrks(acc);
			detail = sql.getDetailPRKS(acc, prd1, prd2);
			if (head != null){
				if (detail.size() > 0){
					request.setAttribute("detail", detail);
					request.getSession(true).setAttribute("listDetail", detail);
				} else {
					request.setAttribute("dataresponse", "Detail PRKS yang dicari tidak ada");
	                request.setAttribute("konfirmasi", "err");
				}
				request.setAttribute("head", head);
				return mapping.findForward(SUCCESS);
			} else {
				request.setAttribute("message", "NO DATA!");
				request.setAttribute("message_error", "ERROR NO DATA!");
				return mapping.findForward("msgpage");
			}
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level; int validLevel = 5; 
		  if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
			  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
			  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
			  return "loginpage"; 
		  }
	  
		  level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
		  if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
			  return "success";
		  }else { 
			  request.setAttribute("message", "Unauthorized Access!");
			  request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			  request.setAttribute("error_code", "Error_timeout"); return "msgpage";
		  }
	}

	
}
