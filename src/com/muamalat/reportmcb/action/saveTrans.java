package com.muamalat.reportmcb.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Instalment;
import com.muamalat.reportmcb.entity.JurnalTodayDetail;
import com.muamalat.reportmcb.function.SqlFunction;
import com.sun.tools.xjc.Driver;

public class saveTrans extends org.apache.struts.action.Action {

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(saveTrans.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        try {
        	List lphtoday = new ArrayList();
        	String user = (String) request.getParameter("userId");
        	lphtoday = (List) request.getSession(true).getAttribute("lphtoday");
        	JurnalTodayDetail detail = null;
        	System.out.println("detail :" + detail);
    		if(lphtoday.size() > 0){
    			for (Iterator<JurnalTodayDetail> i = lphtoday.iterator(); i.hasNext();) {
    				JurnalTodayDetail jmcb = i.next();
    				System.out.println("user :" + detail.getUser_id());
    				if(user.equals(jmcb.getUser_id())){
    					System.out.println("tes5");
    					detail = jmcb;
    					System.out.println("tes6");
    					break;
    				}
    			}
    		}
    		
    		System.out.println("user :" + detail.getUser_id());
    		System.out.println("ac :" + detail.getAc_no());
    		
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
        
		
	}
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "transaksiTodayfwd.do")){
			List lphtoday = (List) request.getSession(true).getAttribute("lphtoday");
			request.setAttribute("lphtoday", lphtoday);
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
	
	
}
