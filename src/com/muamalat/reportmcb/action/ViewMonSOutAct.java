package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class ViewMonSOutAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewMonSOutAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }

        String skn = request.getParameter("skn");
        SqlFunction f = new SqlFunction();
        Date today = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String tgl1 = formatter.format(today);
        List lpcdMcb = new ArrayList();
        List lpcdIntf = new ArrayList();
        String trncd1 = "410";
        String trncd2 = "4D0";
        String trncd3 = "4E0";
        String norekttpn = "205005005";
//        tgl1 = "13-05-2013";
        lpcdMcb = f.getListPCdMcb(tgl1, "SKNBI", skn, trncd1, trncd2, trncd3, norekttpn);
        SimpleDateFormat frmt = new SimpleDateFormat("yyyyMMdd"); 
        String tbl = "";
        if ("out".equals(skn)){
        	tbl = "bmi_skn_sect"+frmt.format(today)+".dbo.outcreditnotes";
            request.setAttribute("hdr", "out");
        } else if ("in".equals(skn)){
        	tbl = "bmi_skn_smiw"+frmt.format(today)+".dbo.increditnotes";
//        	tbl = "bmi_skn_smiw20130709.dbo.increditnotes";
            request.setAttribute("hdr", "in");
        }
        
        lpcdIntf = f.getListPCdIntfSkn(skn, tbl); 
        if (lpcdMcb.size() > 0) {        	
            request.setAttribute("lpcd", lpcdMcb);
        }
        if (lpcdIntf.size() > 0) {        	
            request.setAttribute("lpcdintf", lpcdIntf);
        }
        request.setAttribute("viewlpcd", "lpcd");
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
    }

}
