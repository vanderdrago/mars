package com.muamalat.reportmcb.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.User;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.perantara.PerantaraRpt;
import com.sun.net.httpserver.Authenticator.Success;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

public class LapSpvCSAct extends org.apache.struts.action.Action {
	private static Logger log = Logger.getLogger(LapSpvCSAct.class);
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser (request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		
		if (!SUCCESS.equals("success")){
			return mapping.findForward(SUCCESS);
		}
		try {

			CetakPassbookFunction cpf = new CetakPassbookFunction();
			SqlFunction sql = new SqlFunction();
			
			request.getSession(true).setAttribute("userId", null);
			String userId = request.getParameter("userId");
//			String opt = request.getParameter("btndwnld");
			String send = request.getParameter("btn");
    		String cetak = request.getParameter("btndwnld");
    		String pathjasper = "";
    		System.out.println("btn send :" + send);
    		System.out.println("btn cetak :" + cetak);
			int noOfRecords = 0; 
			int sawal = 1;
			int sakhir = 10; 
			int page = 1; 
			int noOfPages = 0; 
			int recordsPerPage = 30; 
			
			List lphCs = sql.GetLapSpvCs(userId,sawal,recordsPerPage);
    		System.out.println("size :" + lphCs.size());
    		if ("Cari".equals(send)){
    			System.out.println("masuk if send");
        		if (lphCs.size() > 0){
        			request.setAttribute("lphCs", lphCs);
        			request.getSession(true).setAttribute("llsp", lphCs);
        			request.getSession(true).setAttribute("userId", userId);
        		} else {
        			String respon = "Tidak ditemukan Record Data yang Dicari.";
        			request.setAttribute("respon", respon);
        			request.setAttribute("konfirmasi", "err");
        		}
    		} else if ("Cetak".equals(cetak)){
    			System.out.println("masuk else if Cetak");
    			if (lphCs.size() > 0){
    				
    				pathjasper = "com/muamalat/reportmcb/report/statement_monitoring_cs.jasper";
    				JasperReport jasper;
		    		JasperPrint jasperPrint;
		    		String formatFile = "pdf";
		    		
		    		byte[] bytes = null;
   					PerantaraRpt p = new PerantaraRpt();
   					HashMap param = new HashMap();
	   			 	DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
	            	Date dateee = new Date();
	            	String fileName = "Statement_" + userId;
//	            	System.out.println("branch :" + branch);
	            	System.out.println("fileName :" + fileName);
	                ArrayList arrHist = null;
	                
	                if (lphCs.size() > 0){
	                	arrHist = p.moveListToHashMapUserCs(lphCs);
	                	System.out.println("arrHist: " + arrHist);
            	    	System.out.println("size lph: " + lphCs.size());
	                } else {
	                	HashMap rowMap = null;
                        ArrayList detail = new ArrayList();
						rowMap.put("branch_code", "");
						rowMap.put("branch_name", "");
						rowMap.put("tanggal", "");
						rowMap.put("jam", "");
						rowMap.put("key_id", "");
						rowMap.put("maker_id", "");
						rowMap.put("checker_id", "");
						rowMap.put("function_id", "");
						rowMap.put("modifikasi_ke", "");
						detail.add(rowMap);
	                    arrHist = detail;
	                }
					JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
					jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathjasper);
					jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
					
					JRExporter exporter = new JRPdfExporter();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					response.setContentType("application/pdf");
					response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
					exporter.exportReport();
					
					bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                       
                    response.getOutputStream().write(bytes, 0, bytes.length); 
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
    			}
    		}
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
		}
		return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level; int validLevel = 5; 
		  if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
			  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
			  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
			  return "loginpage"; 
		  }
	  
		  level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
		  if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
			  return "success";
		  }else { 
			  request.setAttribute("message", "Unauthorized Access!");
			  request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			  request.setAttribute("error_code", "Error_timeout"); return "msgpage";
		  }
	}
}
