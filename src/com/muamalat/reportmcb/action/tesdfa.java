package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.lang.System;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class tesdfa {
	/**
	 * @param args
	 * @throws Exception
	 */
	
	public static void main(String args[]) throws Exception{

	    int plafond = 50000000;
	    int waktu = 120;
	   
		double Pertama = (plafond * ((5.00/100/12) / (1 - (1/Math.pow((1+(5.00/100/12)), waktu)))));
		double Kedua = Pertama * 160 / 100;
		double ketiga = ((plafond * ((12.75/100/12) / (1 - (1/Math.pow((1+(12.75/100/12)), waktu))))) * waktu);
		double pembagi = 24;

		BigDecimal rumusAngsuran = new BigDecimal(pembagi);
		BigDecimal angsuranPertama = new BigDecimal(Pertama);
		BigDecimal angsuranKedua = new BigDecimal(Kedua);
		BigDecimal angsuranKetiga = new BigDecimal(0);
		BigDecimal TotalAngsuranPertama = new BigDecimal(0);
		BigDecimal TotalAngsuranKedua = new BigDecimal(0);
		BigDecimal TotalAngsuran = new BigDecimal(ketiga);
		angsuranPertama = angsuranPertama.setScale(2, RoundingMode.HALF_UP);
		angsuranKedua = angsuranKedua.setScale(2, RoundingMode.HALF_UP);
		TotalAngsuran = TotalAngsuran.setScale(2, RoundingMode.HALF_UP);
		
		System.out.println(TotalAngsuran);
		
		BigDecimal resultPlafond = new BigDecimal(plafond);
		BigDecimal resultOs = resultPlafond;
		BigDecimal resultAngsPertama = angsuranPertama; 
		
		BigDecimal resultPokokPertama = new BigDecimal(0);
		BigDecimal resultMarginPertama = new BigDecimal(0);
		BigDecimal resultAngsuranPertama = new BigDecimal(0);
		BigDecimal resultPokokKedua = new BigDecimal(0);
		BigDecimal resultMarginKedua = new BigDecimal(0);
		BigDecimal resultPokokKetiga= new BigDecimal(0);
		BigDecimal resultMarginKetiga = new BigDecimal(0);
		BigDecimal resultPokokKeempat= new BigDecimal(0);
		BigDecimal resultMarginKeempat = new BigDecimal(0);
		BigDecimal resultPokokKelima= new BigDecimal(0);
		BigDecimal resultMarginKelima = new BigDecimal(0);
		
		BigDecimal sumPertama = new BigDecimal(0);
		BigDecimal sumKedua = new BigDecimal(0);
		BigDecimal sumKetiga = new BigDecimal(0);
		BigDecimal sumKeempat = new BigDecimal(0);
		BigDecimal TotalSumPertama = new BigDecimal(0);
		BigDecimal TotalSumKedua = new BigDecimal(0);
		BigDecimal TotalSumKetiga = new BigDecimal(0);
		
		
		List<HashMap> map = new ArrayList<HashMap>();
		System.out.println(resultPlafond);
		for (int i = 1; i < waktu + 1; i++)
		{
			if (i < 13){
				HashMap temp = new HashMap();
				double rumusMargin = (11.50/100/12);
				BigDecimal rumus = new BigDecimal(rumusMargin);
				
				resultAngsuranPertama = resultAngsPertama;
				resultMarginPertama = resultOs.multiply(rumus);
				resultMarginPertama = resultMarginPertama.setScale(2, RoundingMode.HALF_UP);
				resultPokokPertama = resultAngsuranPertama.subtract(resultMarginPertama);
				resultOs = resultOs.subtract(resultPokokPertama);
				sumPertama = sumPertama.add(resultAngsuranPertama);
				map.add(temp);
				
				System.out.println("Bulan :" + i + "Os :" + resultOs + "Pokok :" + resultPokokPertama + "Margin :" + resultMarginPertama + "Angsuran :" + resultAngsuranPertama);
			}
		}
		
		BigDecimal resultOsKedua = resultOs;
		BigDecimal tempOsKedua = resultOsKedua;
		BigDecimal resultAngsuranKedua = resultAngsuranPertama;
		System.out.println(resultOs);
		for (int i = 1; i < waktu + 1; i++) 
		{
			if (i > 12 && i < 25)
			{
				HashMap temp = new HashMap();
				double rumusMargin = (10.50/100/12);
				BigDecimal rumus = new BigDecimal(rumusMargin);
				
				resultMarginKedua = tempOsKedua.multiply(rumus);
				resultMarginKedua = resultMarginKedua.setScale(2, RoundingMode.HALF_UP);
				resultPokokKedua = resultAngsuranKedua.subtract(resultMarginKedua);
				tempOsKedua = tempOsKedua.subtract(resultPokokKedua);
				sumKedua = sumKedua.add(resultAngsuranKedua);
				map.add(temp);
				
				System.out.println("Bulan :" + i + "Os :" + tempOsKedua + "Pokok :" + resultPokokKedua + "Margin :" + resultMarginKedua + "Angsuran :" + resultAngsuranKedua);
			}
		}
		
		System.out.println(tempOsKedua);
		BigDecimal resultOsKetiga = tempOsKedua;
		BigDecimal tempOsKetiga = resultOsKetiga;
		BigDecimal resultAngsuranKetiga = resultAngsuranPertama;
		
		for (int i = 1; i < waktu + 1; i++) 
		{
			if (i > 24 && i < 73)
			{
				HashMap temp = new HashMap();
				double rumusMargin = (10.00/100/12);
				BigDecimal rumus = new BigDecimal(rumusMargin);
				
				resultMarginKetiga = tempOsKetiga.multiply(rumus);
				resultMarginKetiga = resultMarginKetiga.setScale(2, RoundingMode.HALF_UP);
				resultPokokKetiga = resultAngsuranKetiga.subtract(resultMarginKetiga);
				tempOsKetiga = tempOsKetiga.subtract(resultPokokKetiga);
				sumKetiga = sumKetiga.add(resultAngsuranKetiga);
				
				map.add(temp);
				System.out.println("Bulan :" + i + "Os :" + tempOsKetiga + "Pokok :" + resultPokokKetiga + "Margin :" + resultMarginKetiga + "Angsuran :" + resultAngsuranKetiga);
			}
		}
		
		System.out.println(tempOsKetiga);
		TotalSumPertama = sumPertama.add(sumKedua.add(sumKetiga));
		System.out.println("Sum Pertama :" + TotalSumPertama);
		BigDecimal resultOsKeempat = tempOsKetiga;
		BigDecimal tempOsKeempat = resultOsKeempat;
		BigDecimal resultAngsuranKeempat = angsuranKedua;
		
		for (int i = 1; i < waktu + 1; i++) 
		{
			if (i > 72 && i < 97)
			{
				HashMap temp = new HashMap();
				double rumusMargin = (10.00/100/12);
				BigDecimal rumus = new BigDecimal(rumusMargin);
				
				resultMarginKeempat = tempOsKeempat.multiply(rumus);
				resultMarginKeempat = resultMarginKeempat.setScale(2, RoundingMode.HALF_UP);
				resultPokokKeempat = resultAngsuranKeempat.subtract(resultMarginKeempat);
				tempOsKeempat = tempOsKeempat.subtract(resultPokokKeempat);
				sumKeempat = sumKeempat.add(resultAngsuranKeempat);
				
				map.add(temp);
				System.out.println("Bulan :" + i + "Os :" + tempOsKeempat + "Pokok :" + resultPokokKeempat + "Margin :" + resultMarginKeempat + "Angsuran :" + resultAngsuranKeempat);
			}
		}
		System.out.println(tempOsKeempat);
		TotalSumKedua = sumKeempat;
		TotalSumKetiga = TotalAngsuran.subtract(TotalSumPertama.add(TotalSumKedua));
		angsuranKetiga = TotalSumKetiga.divide(rumusAngsuran, BigDecimal.ROUND_HALF_UP);
		
		BigDecimal resultOsKelima = tempOsKeempat;
		BigDecimal tempOsKelima = resultOsKelima;
		BigDecimal resultAngsuranKelima = angsuranKetiga;
		System.out.println("tempOsKelima :" + tempOsKelima);
		for (int i = 1; i < waktu + 1; i++) 
		{
			if (i > 96)
			{
				HashMap temp = new HashMap();
				double rumusMargin = (10.839181/100/12);
				BigDecimal rumus = new BigDecimal(rumusMargin);
				
				resultMarginKelima = tempOsKelima.multiply(rumus);
				resultMarginKelima = resultMarginKelima.setScale(2, RoundingMode.HALF_UP);
				resultPokokKelima = resultAngsuranKelima.subtract(resultMarginKelima);
				tempOsKelima = tempOsKelima.subtract(resultPokokKelima);
				
				map.add(temp);
				System.out.println("Bulan :" + i + "Os :" + tempOsKelima + "Pokok :" + resultPokokKelima + "Margin :" + resultMarginKelima + "Angsuran :" + resultAngsuranKelima);
			}
		}
    }
}