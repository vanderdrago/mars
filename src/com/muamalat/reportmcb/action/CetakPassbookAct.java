package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import WSInvoker.Utils.RetVal.LoginRetVal;

import com.muamalat.database.process.ProcessOnDatabase;
import com.muamalat.database.process.ProcessOnDatabaseBean;
import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.LmtTrnsPssbk;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.DataFunction;
import com.muamalat.reportmcb.perantara.PerantaraCetakPassbook;

public class CetakPassbookAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(CetakPassbookAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SUCCESS = isValidUser(request);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        try{
            Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
            request.getSession(true).setAttribute("lhist_v", null);
            String acc = request.getParameter("acc");
            String ket = request.getParameter("ket");
            String ket1 = request.getParameter("ket1");
            String usr_spv = request.getParameter("username");
            String pwd_spv = request.getParameter("password");
                       
            if ("1".equals(ket)){
            	ket = "Printer Rusak";
            } else if ("2".equals(ket)){
            	ket = "Sistem Offline";
            } else if ("3".equals(ket)){
            	ket = "User ID menggantung";
            } else if ("4".equals(ket)){
            	ket = ket1;
            }
            
            if (!"".equals(ket)){
            	CetakPassbookFunction cpf = new CetakPassbookFunction();
            	PerantaraCetakPassbook pcp = new PerantaraCetakPassbook();
            	Reklist cust_acc = cpf.getReklist(acc);                    
            	 if (cust_acc != null){
     	        	if ("S18A".equals(cust_acc.getAcc_clas().toUpperCase().trim()) || "S18B".equals(cust_acc.getAcc_clas().toUpperCase().trim())){
        	        	request.setAttribute("teks", "Restricted Account.");
        	        	request.setAttribute("data", "konfrm");
     	        	} else {
     	               ProcessOnDatabase login = new ProcessOnDatabaseBean();
     	               LoginRetVal retVal = login.login2Mcb(usr_spv.toUpperCase().trim(), pwd_spv, 3);
     	               if(retVal.isRespOk()){
     	                   String s = cpf.checkSupervisorCabangMCB(usr_spv.toUpperCase().trim(), user.getKodecabang(), 9);
     	                   if (s!=null && usr_spv.toUpperCase().equals(s)){
     	                       if(usr_spv.toUpperCase().trim().equals(s.toUpperCase().trim())){
    	 	                       	if (cust_acc != null){
    	 	                       		if ("1".equals(cust_acc.getReksts())){
//    	 	                       			LmtTrnsPssbk lmt = cpf.getlimit_trans_passbook(cust_acc.getAcc_clas());
    	 	                       			LmtTrnsPssbk lmt = cpf.getlimit_trans_passbook();
    	 	                       			BigDecimal sldawal = null;
    	 	                           		Acc_line ac_line = cpf.getAcc_line(cust_acc.getNomrek(), "reprint");
    	 	                           		if (ac_line != null){
    	 	                               		sldawal = cpf.getSaldoAwalTrns(ac_line);
    	 	                               		if (sldawal != null){
    	                       	        			List lreprint = new ArrayList(); 
    	                       	            		List lhist_v = pcp.getListTrans(ac_line, sldawal, "reprint", lmt);
    	                       	            		if (lhist_v != null){
    	                       	            			int l = 1;
    	                       	            			for(int x=1; x <= ac_line.getPrev_line_no_start(); x++){
    	                       	            				CetakPassbook cp = new CetakPassbook();
    	                       	    						cp.setNo(x);
    	                       	    						cp.setNo_s("&nbsp;");
    	                       	    						cp.setTrn_dt_s("&nbsp;");
    	                       	    						cp.setTrn_code("&nbsp;");
    	                       	    						cp.setDrcr_ind("&nbsp;");
    	                       	    						cp.setMutasi_s("&nbsp;");
    	                       	    						cp.setDetail("&nbsp;");
    	                       	    						cp.setSaldo_s("&nbsp;");
    	                       	    						cp.setAuth_id("&nbsp;");
    	                       	    						cp.setRbtn("&nbsp;");
    	                       	    						lreprint.add(cp);
    	                       	    					}
    	                       	            			l = ac_line.getPrev_line_no_start() + 1;
    	                       	            			int sz = lreprint.size();
    	                           	            		for (Iterator<CetakPassbook> i = lhist_v.iterator(); i.hasNext();) {
    	                           	    					CetakPassbook pbook = i.next();
    	                           	    					if (sz <= lmt.getLimit_per_page() && (pbook.getAc_entry_sr_no() != null)){
    	                           	    						if (pbook.getAc_entry_sr_no().compareTo(ac_line.getAc_entry_sr_no()) == 1){
    	       	                    	    						pbook.setNo(l);
    	       	                    	    						pbook.setNo_s(DataFunction.converttostr(pbook.getNo()));
    	       	                    	    						pbook.setRbtn("<input type='radio' name='rbnNumber'  value='"+pbook.getNo()+"'>");
    	       	                    	    						lreprint.add(pbook);
    	       	                    	    						l++;
    	       	                    	    						sz++;
    	       	                    	    					} 
    	                           	    					}
    	                           	            		}
    	                           	            		if (lreprint != null){
    	                           	            			Acc_line line = cpf.getAcc_lineRePrint(acc);
    		                           	         			if (line != null){
    		                           	         				String resp = "Cetak ulang pada rekening " + acc + " sudah pernah dilakukan sebanyak " + line.getNo()+ " kali dengan alasan : " + line.getNotes();
    		                   	        	                	request.setAttribute("notif", "view");
    		                   	        	                	request.setAttribute("resp", resp);
    		                           	         			} 
    	                           	                        request.getSession(true).setAttribute("LmtTrnsPssbk", lmt);
    	                           	                        request.getSession(true).setAttribute("lreprint", lreprint);
    	                           	                        request.getSession(true).setAttribute("ac_line_hist", ac_line);
    	                           	                        request.getSession(true).setAttribute("usr_spv", usr_spv);
    	                           	                        request.getSession(true).setAttribute("cust_acc", cust_acc);
    	                   	        	                	request.setAttribute("lreprint", lreprint);
    	                   	        	                	request.setAttribute("cust_acc", cust_acc);
    	                   	        	                	request.setAttribute("ket", ket);
    	                   	        	    	        	request.setAttribute("data", "viewdata");
    	                           	            		} else {
    	                   	        	    	        	request.setAttribute("teks", "Tidak ada data untuk dicetak ulang.");
    	                   	        	    	        	request.setAttribute("data", "konfrm");
    	                           	            		}        	            			
    	                       	            		} else {
    	                           	    	        	request.setAttribute("teks", "Tidak ada data untuk dicetak ulang.");
    	                           	    	        	request.setAttribute("data", "konfrm");
    	                       	            		}
    	 	                               		}                     			
    	 	                           		} else {
    	 	                       	        	request.setAttribute("teks", "Tidak ada data untuk dicetak ulang");
    	 	                       	        	request.setAttribute("data", "konfrm");
    	 	                               	}          			
    	 	                       		} else {
    	 	                   	        	request.setAttribute("teks", "Rekening tidak aktif");
    	 	                   	        	request.setAttribute("data", "konfrm");
    	 	                           	}           		
    	 	                       	} else {
    	 	               	        	request.setAttribute("teks", "Rekening tidak ditemukan");
    	 	               	        	request.setAttribute("data", "konfrm");
    	 	                       	}
     	                       } else {
     	           	        	request.setAttribute("teks", "Supervisor salah");
     	           	        	request.setAttribute("data", "konfrm");
     	               		} 
     	                   } else {
     	       	        	request.setAttribute("teks", usr_spv + ", bukan supervisor anda.");
     	       	        	request.setAttribute("data", "konfrm");
     	           		} 
     	               } else {
     	   	        	request.setAttribute("teks", "Password supervisor salah");
     	   	        	request.setAttribute("data", "konfrm");
     	       			}    
     	        	}
            	 } else {
            		request.setAttribute("teks", "Rekening tidak ditemukan");
     	        	request.setAttribute("data", "konfrm");
            	 }  
            } else {
        		request.setAttribute("teks", "Keterangan tidak boleh kosong");
 	        	request.setAttribute("data", "konfrm");            	
            }
        } catch (Exception e){
        	request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward("failed");  
        }
        
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
        	CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(8, user.getRole_id());
			if ("".equals(role) || role == null){
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)){
					return "success";				
				} else {	
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";						
				}
			}
	    } else {
	    	request.setAttribute("message", "Unauthorized Access!");
	    	request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	    	//request.setAttribute("error_code", "Error_timeout");
	    	return "msgpage";
	    }
    }
}
