/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.FinSqlFunction;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author Utis
 */
public class ViewGetInstlmtAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewGetInstlmtAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        List ltrnAngsuran = new ArrayList(); 
        FinSqlFunction sql = new FinSqlFunction();
        String accno = request.getParameter("accno");
        String cusid = request.getParameter("cusid");
		String cusname = request.getParameter("cusname");
		String status = request.getParameter("status");

		System.out.println("status :" + status);
		
        int noOfRecords = 0;
        int sawal = 1;
        int sakhir = 10; 
        int page = 1; 
        int noOfPages = 0; 
        int recordsPerPage = 100;
        
		noOfRecords = sql.getTotListInstlmnt(accno, cusid, cusname, status);
		System.out.println("noOfRecords :" + noOfRecords);
		  if (noOfRecords > 0) {
			  System.out.println("masuk if");
			  noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
			  if (noOfPages < sakhir) {
				  sakhir = noOfPages;
			  }
			  ltrnAngsuran = sql.getListInstlmnt(accno, cusid, cusname.toUpperCase(), status, sawal,recordsPerPage);
			  request.setAttribute("sawalAngsuran", sawal); 
			  request.setAttribute("sakhirAngsuran", sakhir); 
			  request.setAttribute("ltrnAngsuran", ltrnAngsuran); 
			  request.setAttribute("noOfRecordsAngsuran", noOfRecords);
			  request.setAttribute("noOfPagesAngsuran", noOfPages); 
			  request.setAttribute("currentPageAngsuran", page); 
			  request.getSession(true).setAttribute("pageAngsuran", page); 
			  request.getSession(true).setAttribute("llsp", ltrnAngsuran); 
			  request.getSession(true).setAttribute("recordsAngsuran", noOfRecords);
			  request.getSession(true).setAttribute("noOfPagesAngsuran", noOfPages);
			  request.getSession(true).setAttribute("accno", accno);
			  request.getSession(true).setAttribute("cusid", cusid);
			  request.getSession(true).setAttribute("cusname", cusname);
//			  request.getSession(true).setAttribute("active", active);
//			  request.getSession(true).setAttribute("liquidate", liquidate);
			  request.getSession(true).setAttribute("status", status);
		  } 
		  else{
			  System.out.println("masuk else");
			  String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
	            request.setAttribute("dataresponse", dataresponse);
	            request.setAttribute("konfirmasi", "err");
		  }
		  return mapping.findForward(SUCCESS); 
//        request.getSession(true).setAttribute("accno", null);
//        
//		List ltrn = new ArrayList();
//		FinSqlFunction f = new FinSqlFunction();
//		String accno = request.getParameter("accno");
//        ltrn = f.getevent(accno);
//		
//		if (ltrn.size() > 0) {
//			ltrn = f.getevent(accno);
//		}
//		if (ltrn.size() == 0) {
//				String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
//	            request.setAttribute("dataresponse", dataresponse);
//	            request.setAttribute("konfirmasi", "err");
//		}
//        request.setAttribute("ltrn", ltrn);
//        request.getSession(true).setAttribute("ltrn", ltrn);
//        
//        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
