package com.muamalat.reportmcb.action;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.EstateFunction;

public class EStatdetail extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(EStatdetail.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String ret = isValidUser(request);
        String r = "";
        if("1".equals(ret) || "2".equals(ret)){
        	r = SUCCESS;
        } else {
        	r = ret;
        }

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
        if (!r.equals("success")) {
            return mapping.findForward(r);
        }
        try{
        	String acc = request.getParameter("acc");
        	EstateFunction ef = new EstateFunction(); 
        	E_stat_cust cust = ef.getEStatCust(acc, "estatin");
        	if(cust != null){
        		if("I".equals(cust.getCust_type().trim())){
        			request.setAttribute("jnsnsbh", "NASABAH INDIVIDU");
        		} else {
        			request.setAttribute("jnsnsbh", "NASABAH NON INDIVIDU");
        		}
    			request.setAttribute("cust",cust);
				request.setAttribute("data", "cust");
        	} else {
	            request.setAttribute("respon", "Tidak ditemukan data nasabah e-Statement."); 
	            request.setAttribute("data", "konfirm");
        	}
        }catch(Exception e){
            request.setAttribute("errpage", "Error : " + e.getMessage());
        }
        return mapping.findForward("success");        	
    
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			/*================= tambahan baru ==============*/
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			List lroles = cpf.getRolesUser(adm.getUsername());
		 	int ada = 0;
		 	String no_role = "";
		 	if (lroles.size() > 0) {
		 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
		 			String rl = it.next();		 			
		 			String role2 = cpf.getAllowedStatement(10, rl.toUpperCase());
		 			if (!"".equals(role2) && role2!=null){
		 				no_role = "2";
		 				ada++;
		 			}
		 			String role3 = cpf.getAllowedStatement(11, rl.toUpperCase());
		 			if (!"".equals(role3) && role3!=null){
		 				no_role = "1";
		 				ada++;
		 			}
		 		}
			} 
		 	cpf= null;
		 	if (ada > 0){
		 		return no_role;
		 	} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
		 	}
			/*
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role1 = cpf.getAllowedPrintPassbook(11, user.getRole_id());
			String role2 = cpf.getAllowedPrintPassbook(10, user.getRole_id());
			if (("".equals(role1) || role1 == null) && ("".equals(role2) || role2 == null)){
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if (role1 != null && !"".equals(role1)){
					if((role1.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)){
						return "1";
					} else {
						request.setAttribute("message", "Unauthorized Access!");
						request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
						return "msgpage";						
					}
				} else {
					if (role2 != null && !"".equals(role2)){
						if((role2.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)){
							return "2";			
						} else {
							request.setAttribute("message", "Unauthorized Access!");
							request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
							return "msgpage";						
						}
					} else {
						request.setAttribute("message", "Unauthorized Access!");
						request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
						return "msgpage";						
					}
				}				
			}*/
		} else {
	    	request.setAttribute("message", "Unauthorized Access!");
	    	request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	    	//request.setAttribute("error_code", "Error_timeout");
	    	return "msgpage";
	    }
    }

}
