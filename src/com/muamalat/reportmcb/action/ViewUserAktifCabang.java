package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.UserAktifCabang;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class ViewUserAktifCabang extends org.apache.struts.action.Action {
	/* forward name="success" path="" */
	private static Logger log = Logger.getLogger(ViewUserAktifCabang.class);
	private static String SUCCESS = "success";

	/** Action called on save button click
     */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response) throws
			  Exception {
		
		 SUCCESS = isValidUser(request);
		  
		  Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		  
		  System.out.println("masuk view UserAKtifCabang");
		  
		  if (!SUCCESS.equals("success")) {
	            return mapping.findForward(SUCCESS);
	        }
		  try {
			  List lph = new ArrayList();
			  SqlFunction sql = new SqlFunction();
			  String branch = request.getParameter("kode");
			  
			  lph = sql.getListUserAktifCabang(branch);
			  if (lph.size() > 0) {
				  request.setAttribute("lph", lph);
				  request.getSession(true).setAttribute("lph", lph);
				  request.setAttribute("kode", branch);
				  request.getSession(true).setAttribute("kode", branch);
			  }
			  else {
	    			request.setAttribute("dataresponse", "Tidak ditemukan transaksi hari ini");
	                request.setAttribute("konfirmasi", "err");
		      }
			  return mapping.findForward(SUCCESS);
		   } catch (Exception e) {
				  request.setAttribute("errpage", "Error : " + e.getMessage()); 
				  return mapping.findForward("failed"); 
		 }
	}

	private String isValidUser(HttpServletRequest request) {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
	}
}
