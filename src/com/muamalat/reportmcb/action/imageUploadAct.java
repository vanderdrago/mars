package com.muamalat.reportmcb.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.tools.codec.Base64Decoder;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

import sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl.ThreadStateMap.Byte0;
import sun.misc.BASE64Decoder;

public class imageUploadAct extends org.apache.struts.action.Action{

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(imageUploadAct.class);
	
//	byte[] imageInByte = null;
//	String imageId;
//
//	private HttpServletRequest servletRequest;
//
//	public String getImageId() {
//		return imageId;
//	}
//
//	public void setImageId(String imageId) {
//		this.imageId = imageId;
//	}
//
//	public ImageAction() {
//		System.out.println("ImageAction");
//	}
//
//	public String execute() {
//		return SUCCESS;
//	}
//	
//	public byte[] get CustomImageInByte(){
//		
//		BufferedImage originalImage;
//		
//		try {
//			originalImage = ImageIO.read(getImageFile(this.imageId));
//			// convert BufferedImage to byte array
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			ImageIO.write(originalImage, "jpg", baos);
//			baos.flush();
//			imageInByte = baos.toByteArray();
//			baos.close();
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//	}
//	
//	private File getImageFile(String imageId) {
//		String filePath = servletRequest.getSession().getServletContext().getRealPath("/");
//		File file = new File(filePath + "/Image/", imageId);
//		System.out.println(file.toString());
//		return file;
//	}
//	
//	public String getCustomContentType() {
//		return "image/jpeg";
//	}
//
//	public String getCustomContentDisposition() {
//		return "anyname.jpg";
//	}
//	
//	public void setServletRequest(HttpServletRequest request) {
//		this.servletRequest = request;
//
//	}
//	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
//			HttpServletResponse response) throws Exception {
//		
//		SUCCESS = isValidUser(request);
//		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
//		if (!SUCCESS.equals("success")){
//			 return mapping.findForward(SUCCESS);
//		}
//		
//		byte[] imageInByte = null;
//		String image = request.getParameter("base64string");
//		
//		
//		
////		try {
////			String image = request.getParameter("base64string");
////			
////			BASE64Decoder decoder = new BASE64Decoder();
////			byte[] imgByte = decoder.decodeBuffer(image);
////			
////			
////			System.out.println(image);
////			
////			return mapping.findForward(SUCCESS);
////		} catch (Exception e) {
////			request.setAttribute("errpage", "Error : " + e.getMessage());
////	        return mapping.findForward(".errPage");
////		}
//		
//		
//	}
	
	
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		System.out.println("lrole :" + lrole);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "imageUpload.do")){
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
	
	
}
