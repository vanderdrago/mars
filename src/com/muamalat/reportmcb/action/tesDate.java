package com.muamalat.reportmcb.action;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

public class tesDate {
	
	public static void main(String args[]) throws Exception{
		
		SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
		String tgl1 = "05-05-2017";
		Date tgl2 = format.parse(tgl1);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyymm");
		String tgl3 = format1.format(tgl2);
		System.out.println(tgl3);
		
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMM");//dd/MM/yyyy
	    Date now = new Date();
	    String period = sdfDate.format(now);
		System.out.println(period);
		
		final String OLD = "yyyy-mm-dd";
		final String NEW = "dd-mm-yyyy";
		
		String oldDateString = "2017-05-21";
		String newDateString;
		
		SimpleDateFormat sdf = new SimpleDateFormat(OLD);
		Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW);
		newDateString = sdf.format(d);
		System.out.println(newDateString);
		
//		Date today = Calendar.getInstance().getTime();
//		SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
//		String name = format.format(today);
//		System.out.println(name);
	}
}
