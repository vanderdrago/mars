/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.FinSqlFunction;

/**
 *
 * @author Utis
 */
public class ViewGetFacltAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewGetFacltAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        request.getSession(true).setAttribute("liabno", null);
        request.getSession(true).setAttribute("liabname", null);
        request.getSession(true).setAttribute("linecode", null);
         
		List ltrn = new ArrayList();
		FinSqlFunction f = new FinSqlFunction();
		String liabno = request.getParameter("liabno");
		String liabname = request.getParameter("liabname");
        String linecode = request.getParameter("linecode");
		ltrn = f.getFcltList(liabno, liabname, linecode);
		
		if (ltrn.size() > 0) {
			ltrn = f.sGetmFcltList(liabno, liabname, linecode);
		}
		if (ltrn.size() == 0) {
				String dataresponse = "Tidak ditemukan Record Data yang Dicari.";
	            request.setAttribute("dataresponse", dataresponse);
	            request.setAttribute("konfirmasi", "err");
		}
        request.setAttribute("ltrn", ltrn);
        request.getSession(true).setAttribute("ltrn", ltrn);
        
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
        if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
