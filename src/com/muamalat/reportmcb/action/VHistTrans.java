package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.AvgBal;
import com.muamalat.reportmcb.entity.Customer;
import com.muamalat.reportmcb.entity.Gltb_avgbal;
import com.muamalat.reportmcb.entity.Role;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.HistCorporateFunction;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.parameter.Parameter;
import com.muamalat.reportmcb.perantara.PerantaraRpt;
import com.muamalat.singleton.DatasourceEntry;

import WSInvoker.Utils.RetVal.LoginRetVal;

public class VHistTrans extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(VHistTrans.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
       

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        
        try{
        	List lhist = new ArrayList();
            List lhistCetakan = new ArrayList();
            SqlFunction f = new SqlFunction();
            
            HistCorporateFunction hcf = new HistCorporateFunction();
    		CetakPassbookFunction cpf = new CetakPassbookFunction();
            
            String norek = (String)request.getSession(true).getAttribute("norek");
            String nama = (String)request.getSession(true).getAttribute("nama");
			List lrek = (List) request.getSession(true).getAttribute("llsp");
			
			System.out.println("no rek :" + norek + "nama :" + nama + "lrek :" + lrek);
			SttmCustAccount objRek = null;
			SMTB_USER_ROLE userRole = null;
			System.out.println("lrek :" + lrek.size());
			
			if(lrek.size() > 0){
				for (Iterator<SttmCustAccount> i = lrek.iterator(); i.hasNext();) {
					SttmCustAccount rek = i.next();
					if(norek.equals(rek.getCust_ac_no().trim())){
						System.out.println("error 3");
						objRek = rek;
						break;			
					}
				}
			}
			/* ================ Tambahan user role HCO dan CFD1 ROLE================*/
			System.out.println("product :" + objRek.getAccount_class());
            String rr = "";
            List listRole =  cpf.getUserRoles(user.getUsername(),"HCO","CFD1_ROLE");
            if (listRole.size() > 0){
            	for (Iterator<SMTB_USER_ROLE> x = listRole.iterator(); x.hasNext();){
            		SMTB_USER_ROLE z = x.next();
            		userRole = z;
        			break;
            	}
            } 
            
            System.out.println("role:" + userRole.getRole_id());
            if ("N/A".equals(userRole.getRole_id())){
            	System.out.println("tes if");
            	if ("S18A".equals(objRek.getAccount_class().toUpperCase().trim())|| "S18B".equals(objRek.getAccount_class().toUpperCase().trim()) || "S90B".equals(objRek.getAccount_class().toUpperCase().trim()))  {           		
	            	rr = "1";
	            } else {
	            	System.out.println("2");
            		rr = "2";
            	}
            } else if ("HCO".equals(userRole.getRole_id())){
            	rr = "2";
            } else if ("CFD1_ROLE".equals(userRole.getRole_id())) {
            	System.out.println("tes if cfd");
            	if ("S18A".equals(objRek.getAccount_class().toUpperCase().trim())|| "S18B".equals(objRek.getAccount_class().toUpperCase().trim())){
            		System.out.println("1");
            		rr = "1";
            	} else {
            		System.out.println("2");
            		rr = "2";
            	}
            }
            if ("1".equals(rr)){ 
            	request.setAttribute("confrm","err");
        		request.setAttribute("respon", "Restricted Account");
            } else if ("2".equals(rr)){
            	SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
            	
	            String dt1 = (String)request.getParameter("tgl1");
	            String dt2 = (String)request.getParameter("tgl2");
	            Date tgl2 = format.parse(dt2);
	            SimpleDateFormat format1 = new SimpleDateFormat("yyyymm");
	            String ConvertTgl2 = format1.format(tgl2);
	            
	            System.out.println("dt 1:" + dt1 + "dt2 :" + dt2);
	            System.out.println("tgl2 :" + tgl2);
	            System.out.println("convert tgl2 :" + ConvertTgl2);
	            
	            String period = dt1.substring(0, 2) + " " + Parameter.getMonth(Integer.parseInt(dt1.substring(3, 5))) + " " + dt1.substring(6) + " s/d " + 
	            dt2.substring(0, 2) + " " + Parameter.getMonth(Integer.parseInt(dt2.substring(3, 5))) + " " + dt2.substring(6);
	            Customer cust = f.getCustomer(norek);
	            if (cust != null){
	            	Saldo sld = f.getSaldoRek(norek, dt1);
		            if(sld == null){
		            	lhist = f.getListHistTrans(norek, dt1, dt2, new BigDecimal(0)); 
                        lhistCetakan = f.getListHistTransCetakan(norek, dt1, dt2, new BigDecimal(0));
		            } else {
		            	lhist = f.getListHistTrans(norek, dt1, dt2, sld.getLcy_closing_bal());
                        lhistCetakan = f.getListHistTransCetakan(norek, dt1, dt2, sld.getLcy_closing_bal());  
		            }
	            	
//	            	lhist = hcf.getListHistTrans("3040031726", "3410003493", dt1, dt2, new BigDecimal(0)); 
	            	
		            if(sld != null){
		            	request.setAttribute("saldoawal", sld.getLcy_closing_bal());
		            	request.setAttribute("saldoawaleki", sld.getEkivalen());
	            	} else {
	            		request.setAttribute("saldoawal", new BigDecimal(0));
	            		request.setAttribute("saldoawaleki", new BigDecimal(0));
	            	}
		            BigDecimal sld_akhir = new BigDecimal(0);
		            int jmlItemD = 0;
		            int jmlItemC = 0;
		            BigDecimal totD = new BigDecimal(0);
		            BigDecimal totC = new BigDecimal(0);
		            String pathJasper = "";
		            String opt = request.getParameter("btndwnld");
		            
                   if (lhistCetakan.size() > 0){
                   //if (lhist.size() > 0){
	            	sld_akhir = f.getSld_akhir();
	            	jmlItemD = f.getJmlItmD();
	            	jmlItemC = f.getJmlItmC();
	            	totD = f.getTotD();
	            	totC = f.getTotC();
	            	
	            	
	            	if("Cetak".equals(opt)){
		            	if("PRK3".equals(cust.getCust_acc_clas().toUpperCase())){
		            		System.out.println("Statement_Hist_Acc_newPRK3.jasper");
			            	pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_newPRK3.jasper";
		            	} else {
		            		pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_new.jasper";
			            	//pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_new.jasper";		            		
		            	} 
		            } else {
		            	if("PRK3".equals(cust.getCust_acc_clas().toUpperCase())){
		            		
			            	pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_newPRK3_eng.jasper";
		            	} else {
		            		pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_new_eng.jasper";
			            	//pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_new.jasper";		            		
		            	} 
		            	
		            }
	            	
	            	
		            } else {
		            	System.out.println("else");
		            	
		            	if("Cetak".equals(opt)){
		            	if("PRK3".equals(cust.getCust_acc_clas().toUpperCase())){
		            		pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_newPRK3_NoHist.jasper";
		            	} else {
		            		pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_No_Hist.jasper";
		            	}
		            	} else {
		            		if("PRK3".equals(cust.getCust_acc_clas().toUpperCase())){
			            		pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_newPRK3_NoHist_eng.jasper";
			            	} else {
			            		pathJasper = "com/muamalat/reportmcb/report/Statement_Hist_Acc_No_Hist_Eng.jasper";
			            	}
		            		
		            	}
		            	
		            	
		            	if(sld != null){
	                    	sld_akhir = sld.getLcy_closing_bal();
		            	}
		            }
                   System.out.println("pakai jasper :"+ pathJasper);
	            	
	            	if("Cetak".equals(opt) || "Cetak_Eng".equals(opt)){
	            		System.out.println("pakai jasper :"+ pathJasper);
	            		JasperReport jasperReport;
	                    JasperPrint jasperPrint;
	                    String formatFile = "pdf";
	            		/*tambahan role*/
	        			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
	        			SMTB_USER_ROLE usr = cpf.getUser(adm.getUsername(), adm.getKodecabang());
	        			String role = cpf.getAllowedPrintPassbook(12, usr.getRole_id());
	        			
	        			/*================= tambahan baru ==============*/
	        			List lroles = cpf.getRolesUser(adm.getUsername());
	   				 	int ada = 0;
	   				 	if (lroles.size() > 0) {
	   				 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
	   				 			String rl = it.next();
	   				 			String role2 = cpf.getAllowedStatement(12, rl.toUpperCase());
	   				 			if (!"".equals(role2) && role2!=null){
	   				 				ada++;
	   				 			}
	   				 		}
		   				} 
	        			/*==============================================*/
	        			
	        			if ("000".equals(adm.getKodecabang()) || ada > 0) {
//		        		if ("000".equals(adm.getKodecabang()) || (role != null && role.indexOf(usr.getRole_id().toUpperCase()) > -1)) {
	        				/*tambahan role*/
	        				byte[] bytes = null;
		            		PerantaraRpt p = new PerantaraRpt();
		            		HashMap params = new HashMap();
		            		DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
		            	    Date dateee = new Date();
		                    params.put("tgl_cetak", dateFormattt.format(dateee));
		                    params.put("namaUser", cust.getCust_name());
		                    params.put("alamat_nsbh", cust.getCust_addrs());
		                    params.put("norek", cust.getCust_acc());
		                    params.put("jns_rek", cust.getCust_acc_clas() + " - " + cust.getCust_acc_clas_desc());
		                    params.put("valuta", cust.getCust_ccy());			                    
		                    params.put("cabang", cust.getBranch_cd() + " - " + cust.getBranch_name());
		                    params.put("periode", period);
		                    if(sld != null){
		                    	params.put("saldoawal", sld.getLcy_closing_bal());
			            	} else {
			            		params.put("saldoawal", new BigDecimal(0));
			            	}
		                    params.put("jmlItmD", jmlItemD);
		                    params.put("jmlItmC", jmlItemC);
		                    params.put("totD", totD);
		                    params.put("totC", totC);
		                    params.put("saldo_akhir", sld_akhir);
		                    params.put("saldo_hold", cust.getSld_hold());
		                    params.put("saldo_min", cust.getSld_min());
		                    System.out.println("convert Tgl2 :" + ConvertTgl2);
		                    AvgBal avgBal = f.getAvgBal(norek,ConvertTgl2);
//		                    Gltb_avgbal avgBal = f.getGltb_avgbal(norek);
		                    if (avgBal == null){
			                    params.put("saldo_rata", new BigDecimal(0));		                    	
		                    } else {
		                    	if ("IDR".equals(cust.getCust_acc_clas().toUpperCase().trim())){
//				                    params.put("saldo_rata", avgBal.getLcy_bal());	
		                    		params.put("saldo_rata", avgBal.getLcyBal());
		                    	} else {
//		                    		params.put("saldo_rata", avgBal.getAcy_bal());
		                    		params.put("saldo_rata", avgBal.getAcyBal()); 
		                    	}
		                    }
		                    params.put("saldo_float", cust.getSld_float());
		                    params.put("saldo_efektif", sld_akhir.subtract(cust.getSld_float().add(cust.getSld_hold().add(cust.getSld_min()))));
		                    
		                    if("PRK3".equals(cust.getCust_acc_clas().toUpperCase())){
		                    	BigDecimal plafond = f.getPlafondPRK3String(cust);
			                    params.put("plafond", plafond);
			                    params.put("plafondTerpakai", sld_akhir);
			                    params.put("plafondSisa", plafond.add(sld_akhir));
		                    }
		                    
		                    		                    
		                    String filename = "Statement_"+ norek + "_" + dt1 + "_" + dt2;
		                    System.out.println("filename:" + filename);
		                    ArrayList arrHist = null;
                                    if(lhistCetakan.size() > 0){  
			            		arrHist = p.moveListToHashMap(lhistCetakan);	
		                    //if(lhist.size() > 0){  
			            //		arrHist = p.moveListToHashMap(lhist);			                    	
		                    } else {
		                    	HashMap rowMap = null;
		                        ArrayList detail = new ArrayList();
		                        rowMap = new HashMap();
	                            rowMap.put("noref", "");
	                            rowMap.put("trn_dt", "");
	                            rowMap.put("value_dt", "");
	                            rowMap.put("trn_cd", "");
	                            rowMap.put("desc", "");
	                            rowMap.put("no_warkat", "");
	                            rowMap.put("drcr", "");
	                            rowMap.put("nominal", "");
	                            rowMap.put("saldo", "");
	                            detail.add(rowMap);
		                        arrHist = detail;
		                    }
		            		
		            	   JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
                           jasperReport = (JasperReport) JRLoader.loadObjectFromLocation(pathJasper);
                           jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);
                           JRExporter exporter = new JRPdfExporter();
                           exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                           response.setContentType("application/pdf");
                           response.setHeader("Content-disposition", "attachment;filename="+filename+".pdf");
                           exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                           exporter.exportReport();
                           System.out.println(pathJasper);
                           bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                           
                           response.getOutputStream().write(bytes, 0, bytes.length);
                           
                           response.getOutputStream().flush();
                           response.getOutputStream().close();

                 	      
	                    }else {
							request.setAttribute("message", "Unauthorized Access!");
							request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
							return mapping.findForward("msgpage");
							//sampe sini
						}     
	            		return null;
			}
	            else {
		                request.setAttribute("lhist",lhist);
		                request.setAttribute("vht","ht");
		        		request.setAttribute("nama", nama);
		        		request.setAttribute("period", period);
		                request.setAttribute("norek",norek); 	            		
	            	}
		                request.getSession(true).setAttribute("llsp", lrek);
		            } else{
		        		request.setAttribute("respon", "Rekening tidak ditemukan.");
		                request.setAttribute("confrm","err");           	
		            }            
//	            } 
			}
        }catch(Exception e){
            request.setAttribute("errpage",e.getMessage());           	
        	SUCCESS = "failed";
        }
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			return "success";
	    } else {
	    	request.setAttribute("message", "Unauthorized Access!");
	    	request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	    	return "msgpage";
	    }
    }

}
