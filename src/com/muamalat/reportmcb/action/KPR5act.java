package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class KPR5act extends org.apache.struts.action.Action {

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(KPR5act.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try {
			
			String nama = request.getParameter("nama");
			String Dplafond = request.getParameter("plafond");
			System.out.println("Dplafond :" + Dplafond);
			String tanggal = request.getParameter("tgl");
			int tenor = 60;
			
			double plafond = Double.parseDouble(Dplafond);
			System.out.println("plafond :" + plafond);
			double pertama = (plafond * ((5.00/100/12) / (1 - (1/Math.pow((1+(5.00/100/12)), tenor)))));
			double kedua = ((11.123206/100/12) / (1 - (1/Math.pow((1+(11.123206/100/12)), 24))));
			
			BigDecimal angsuranPertama = new BigDecimal(pertama);
			angsuranPertama = angsuranPertama.setScale(2, RoundingMode.HALF_UP);
			BigDecimal angsuranKedua = new BigDecimal(kedua);
			angsuranKedua = angsuranKedua.setScale(8, RoundingMode.HALF_UP);
			
			BigDecimal resultPlafond = new BigDecimal(plafond);
			BigDecimal resultOs = resultPlafond;
			 
			BigDecimal pokokPertama = new BigDecimal(0);
			BigDecimal marginPertama = new BigDecimal(0);
			BigDecimal pokokKedua = new BigDecimal(0);
			BigDecimal marginKedua = new BigDecimal(0);
			BigDecimal resultAngsuranPertama = new BigDecimal(0);
			BigDecimal resultAngsuranKedua = new BigDecimal(0);
			
			BigDecimal sumPertama = new BigDecimal(0);
			BigDecimal sumKedua = new BigDecimal(0);
			BigDecimal TotalMargin = new BigDecimal(0);
			BigDecimal TotalAngsuran = new BigDecimal(0);
			
			List<HashMap> map = new ArrayList<HashMap>();
			for (int i = 1; i < tenor + 1; i++)
			{
				if (i < 37){
					HashMap temp = new HashMap();
					double rumus = (11.123207/100/12);
					BigDecimal tempRumus = new BigDecimal(rumus);
					
					resultAngsuranPertama = angsuranPertama;
					marginPertama = tempRumus.multiply(resultOs);
					marginPertama = marginPertama.setScale(2, RoundingMode.HALF_UP);
					pokokPertama = angsuranPertama.subtract(marginPertama);
					resultOs = resultOs.subtract(pokokPertama);
					sumPertama = sumPertama.add(resultAngsuranPertama);
					
					temp.put("AngsuranPertama", resultAngsuranPertama);
					temp.put("MarginPertama", marginPertama);
					temp.put("PokokPertama", pokokPertama);
					temp.put("OsPertama", resultOs);
					map.add(temp);
					System.out.println("Bulan :" + i + "Os : " + resultOs + "Pokok : " + pokokPertama + "Margin : " + marginPertama + "Angsuran :" + angsuranPertama);
				}
			}
			
			BigDecimal resultOsKedua = resultOs;
			BigDecimal tempOsKedua = resultOsKedua;
			for (int i = 1; i < tenor + 1; i++)
			{
				if (i > 36)
				{
					HashMap temp = new HashMap();
					double rumus = (11.123207/100/12);
					BigDecimal tempRumus = new BigDecimal(rumus);
					
					resultAngsuranKedua = resultOs.multiply(angsuranKedua);
					resultAngsuranKedua = resultAngsuranKedua.setScale(2, RoundingMode.HALF_UP);
					marginKedua = tempRumus.multiply(tempOsKedua);
					marginKedua = marginKedua.setScale(2, RoundingMode.HALF_UP);
					pokokKedua = resultAngsuranKedua.subtract(marginKedua);
					tempOsKedua = tempOsKedua.subtract(pokokKedua);
					sumKedua = sumKedua.add(resultAngsuranKedua);
					
					temp.put("AngsuranKedua", resultAngsuranKedua);
					temp.put("MarginKedua", marginKedua);
					temp.put("PokokKedua", pokokKedua);
					temp.put("OsKedua", tempOsKedua);
					map.add(temp);
//					System.out.println("Bulan :" + i + "Os : " + tempOsKedua + "Pokok : " + pokokKedua + "Margin : " + marginKedua + "Angsuran :" + resultAngsuranKedua);
				}
			}
			
			TotalAngsuran = sumPertama.add(sumKedua);
			TotalMargin = TotalAngsuran.subtract(resultPlafond);
			
			request.getSession(true).setAttribute("nama", nama);
			request.getSession(true).setAttribute("plafond", plafond);
			request.getSession(true).setAttribute("jangkaWaktu", tenor);
			request.getSession(true).setAttribute("tanggal", tanggal);
			request.getSession(true).setAttribute("TotalPokok", resultPlafond);
			request.getSession(true).setAttribute("TotalAngsuran", TotalAngsuran);
			request.getSession(true).setAttribute("TotalMargin", TotalMargin);
			request.getSession(true).setAttribute("AngsuranPertama", resultAngsuranPertama);
			request.getSession(true).setAttribute("AngsuranKedua", resultAngsuranKedua);
			request.setAttribute("kpr", map);
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
		
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		System.out.println("lrole :" + lrole);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "KPR5th.do")){
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
	
}
