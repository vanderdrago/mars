package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class kprBackUp extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(simulasiKPRact.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		try {
			String nama = request.getParameter("nama");
			String plafond = request.getParameter("plafond");
			String jangkaWaktu = request.getParameter("waktu");
			String tgl = request.getParameter("tgl");
			
			int jangka = Integer.parseInt(jangkaWaktu);
			int amount = Integer.parseInt(plafond);
			int bulan = Integer.parseInt(jangkaWaktu);
			int i;
			double Dplafond = Double.parseDouble(plafond);
			
			//Nilai angsuran tahun 1-3 jika jangka waktu 5th
			double pengurang = (Dplafond * 0.35 /100);
			double rumus = ((12.25/100/12) * 100) / 100.0;
			double pembagi = 1 - (1/Math.pow((1+(12.25/100/12)), bulan));
			double total = (12.25/100/12) / pembagi; 
			double angsuran = (amount * total) - pengurang;
			double angsuran2 = (amount * total) * bulan;
			double tempAngsuran = angsuran;	
			double bunga = angsuran2 - Dplafond;
			
			BigDecimal DecimalPlafond = new BigDecimal(Dplafond);
			DecimalPlafond = DecimalPlafond.setScale(2, RoundingMode.HALF_UP);
			BigDecimal bigAngsuran = new BigDecimal(angsuran);
			bigAngsuran = bigAngsuran.setScale(2, RoundingMode.HALF_UP);
			BigDecimal bigAngsuran2 = new BigDecimal(angsuran2);
			bigAngsuran2 = bigAngsuran2.setScale(2, RoundingMode.HALF_UP);
			BigDecimal DecimalBunga = new BigDecimal(bunga);
			DecimalBunga = DecimalBunga.setScale(2, RoundingMode.HALF_UP);	
			BigDecimal DecimalTahun = new BigDecimal(24);
			
			List<HashMap> MapResult = new ArrayList<HashMap>();
			double resultPlafond = amount;
			double resultAngsuran = tempAngsuran;
			
			BigDecimal tempSumAngsuran = new BigDecimal(0);
			BigDecimal tempSumMargin = new BigDecimal(0);
			BigDecimal tempSumAngPokok = new BigDecimal(0);
			BigDecimal sisaMargin = new BigDecimal(0);
			BigDecimal sisaPokok = new BigDecimal(0);
			BigDecimal marginTahunSisa = new BigDecimal(0);
			BigDecimal angPokokSisa = new BigDecimal(0);
			BigDecimal angsuranSisa = new BigDecimal(0);
			BigDecimal tempSumSisaAngsuran = new BigDecimal(0);
			BigDecimal tempSumSisaMargin = new BigDecimal(0);
			BigDecimal tempSumSisaAngPokok = new BigDecimal(0);
			BigDecimal TotalSumSisaAngsuran = new BigDecimal(0);
			BigDecimal TotalSumSisaMargin = new BigDecimal(0);
			BigDecimal TotalSumSisaAngPokok = new BigDecimal(0);
			BigDecimal period1 = new BigDecimal(0);
			BigDecimal period2 = new BigDecimal(0);
			
			for( i = 1; i < jangka + 1; i++){
				if (i < 37){
					HashMap temp = new HashMap();
					BigDecimal tempAng = new BigDecimal(resultAngsuran);
					double tempTotAng = resultAngsuran;
					double tempPlafond = resultPlafond;
					double Hashmargin = tempPlafond*(12.25/100/12);
					double tempHashMargin = Hashmargin;
					double tempAngsPokok = resultAngsuran - tempHashMargin;
					resultPlafond = resultPlafond - tempAngsPokok;
					
					BigDecimal DecimaltempHashMargin = new BigDecimal(Hashmargin);
					DecimaltempHashMargin = DecimaltempHashMargin.setScale(2, RoundingMode.HALF_UP);
					BigDecimal DecimaltempAngsPokok = new BigDecimal(tempAngsPokok);
					DecimaltempAngsPokok = DecimaltempAngsPokok.setScale(2, RoundingMode.HALF_UP);
					
					tempSumAngsuran = tempSumAngsuran.add(tempAng);
					tempSumMargin = tempSumMargin.add(DecimaltempHashMargin);
					tempSumAngPokok = tempSumAngPokok.add(DecimaltempAngsPokok);
					
					temp.put("resultPlafond", resultPlafond);
					temp.put("Os_Pokok", tempPlafond);
					temp.put("Angsuran", resultAngsuran);
					temp.put("TempHashMargin", tempHashMargin);
					temp.put("Angsuran_pokok", tempAngsPokok);
					MapResult.add(temp);
				} 
			}
			
			//Sisa tahun 4-5
			sisaMargin = DecimalBunga.subtract(tempSumMargin);
			marginTahunSisa = sisaMargin.divide(DecimalTahun, 2, RoundingMode.HALF_UP);
			sisaPokok = DecimalPlafond.subtract(tempSumAngPokok);
			angPokokSisa = sisaPokok.divide(DecimalTahun, 2, RoundingMode.HALF_UP);
			angsuranSisa = marginTahunSisa.add(angPokokSisa);
			
			double tempPokok = resultPlafond;	
			double tempSisaPlafond = tempPokok;
			BigDecimal DecimaltempPokok = new BigDecimal(tempPokok);
			BigDecimal DecimaltempSisaPlafond = new BigDecimal(tempSisaPlafond);
			BigDecimal DecimalresultPlafond = new BigDecimal(resultPlafond);
			for ( i = 1; i < jangka; i++){
				if (i > 35){
					HashMap temp = new HashMap();
					DecimaltempSisaPlafond = DecimaltempSisaPlafond.subtract(angPokokSisa);
					
					tempSumSisaAngPokok = tempSumSisaAngPokok.add(angPokokSisa);
					tempSumSisaMargin = tempSumSisaMargin.add(marginTahunSisa);
					tempSumSisaAngsuran = tempSumSisaAngsuran.add(angsuranSisa);
					
					temp.put("sisa_angsuran", angsuranSisa);
					temp.put("sisa_margin", marginTahunSisa);
					temp.put("sisa_pokok", angPokokSisa);
					temp.put("sisa_plafond", DecimaltempSisaPlafond);
					MapResult.add(temp);
				}
			}
			
			TotalSumSisaAngPokok = tempSumAngPokok.add(tempSumSisaAngPokok);
			TotalSumSisaMargin = tempSumMargin.add(tempSumSisaMargin);
			TotalSumSisaAngsuran = tempSumAngsuran.add(tempSumSisaAngsuran);
			
			System.out.println("sum :" + TotalSumSisaAngsuran);
			
			BigDecimal sisaAng = TotalSumSisaAngPokok;
			BigDecimal tempSisaAng = sisaAng;
			tempSisaAng = tempSisaAng.setScale(2, RoundingMode.HALF_UP);
			
			List<HashMap> MapResult2 = new ArrayList<HashMap>();
			
			for ( i = 1; i < jangka; i++){
				if (i < 37){
					if (Integer.toString(i) != null){
					HashMap temp = new HashMap();
					tempSisaAng = TotalSumSisaAngsuran.subtract(bigAngsuran);
					
					temp.put("i", i);
					temp.put("sisa_ang", tempSisaAng);
					MapResult2.add(temp);
					System.out.println("Sisa :" + tempSisaAng + "Total sum :" + TotalSumSisaAngsuran + "angsur :" + bigAngsuran);
					}
				}
			}
			
			for (HashMap map : MapResult){
				System.out.println("No bawah:" + map.get("i") + "Sisa angsuran :" + map.get("sisa_ang"));
			}
			
			request.getSession(true).setAttribute("nama", nama); 
			request.getSession(true).setAttribute("plafond", plafond); 
			request.getSession(true).setAttribute("tgl", tgl); 
			request.getSession(true).setAttribute("waktu", jangkaWaktu);
			request.getSession(true).setAttribute("period1", resultAngsuran);
			request.getSession(true).setAttribute("period2", angsuranSisa);
			request.getSession(true).setAttribute("sumAngsuran", TotalSumSisaAngsuran);
			request.getSession(true).setAttribute("sumMargin", TotalSumSisaMargin);
			request.getSession(true).setAttribute("sumAngPokok", TotalSumSisaAngPokok);
			request.setAttribute("kpr", MapResult2);
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	        return mapping.findForward(".errPage");
		}
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "simulasiKPR.do")){
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
}
