package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.SqlFunctionNew;

public class prksAct extends org.apache.struts.action.Action{
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(prksAct.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try{ 
			SqlFunctionNew sql = new SqlFunctionNew();
			List tes = new ArrayList();
			String acc = request.getParameter("acc");
			tes = sql.getPrks(acc);
			if (tes.size()>0){
				request.setAttribute("prks", tes);
				request.setAttribute("acc", acc);
				request.setAttribute("brn", "brn");
				request.setAttribute("custName", "custName");
    			request.getSession(true).setAttribute("llsp", tes);
    			request.getSession(true).setAttribute("acc", acc);
    			request.getSession(true).setAttribute("brn", "brn");
    			request.getSession(true).setAttribute("custName", "custName");
			}
		} catch (Exception e) {
		  request.setAttribute("errpage", "Error : " + e.getMessage()); 
		  return mapping.findForward("failed"); 
	   }
		return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level; int validLevel = 5; 
		  if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
			  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
			  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
			  return "loginpage"; 
		  }
	  
		  level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
		  if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
			  return "success";
		  }else { 
			  request.setAttribute("message", "Unauthorized Access!");
			  request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			  request.setAttribute("error_code", "Error_timeout"); return "msgpage";
		  }
	}
	
	

}
