package com.muamalat.reportmcb.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.User;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.SqlFunctionNew;
import com.muamalat.reportmcb.parameter.Parameter;
import com.muamalat.reportmcb.perantara.PerantaraRpt;
import com.sun.net.httpserver.Authenticator.Success;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

public class quickCifActPeriode extends org.apache.struts.action.Action {
	private static Logger log = Logger.getLogger(quickCifActPeriode.class);
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser (request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		
		if (!SUCCESS.equals("success")){
			return mapping.findForward(SUCCESS);
		}
		try {

			CetakPassbookFunction cpf = new CetakPassbookFunction();
			SqlFunctionNew sql = new SqlFunctionNew();
			
			request.getSession(true).setAttribute("branch", null);
			String branch = request.getParameter("branch");
			String chkCab = request.getParameter("chkBranch");
			String startDate = request.getParameter("StartDate");
			String endDate = request.getParameter("EndDate");
			String send = request.getParameter("btnsend");
    		System.out.println("startDate :" + startDate);
    		System.out.println("endDate :" + endDate);
    		String pathjasper = "";
    		String periode = startDate.substring(0, 2) + " " + Parameter.getMonth(Integer.parseInt(startDate.substring(3, 5))) + " " + startDate.substring(6) + " s/d " + 
    				endDate.substring(0, 2) + " " + Parameter.getMonth(Integer.parseInt(endDate.substring(3, 5))) + " " + endDate.substring(6);
    		System.out.println("periode:" + periode);
    		List lph = sql.getQuickCifTes(branch, chkCab, startDate, endDate);
    			
    			System.out.println("size :" + lph.size());
        		if (lph.size() > 0){
        			request.setAttribute("listQuickCif", lph);
        			request.getSession(true).setAttribute("llsp", lph);
        			request.getSession(true).setAttribute("branch", branch);
        			request.setAttribute("vht","ht");
        			request.setAttribute("branch", branch);
        			request.setAttribute("periode", periode);
        		} else {
        			String respon = "Tidak ditemukan Record Data yang Dicari.";
        			request.setAttribute("respon", respon);
        			request.setAttribute("konfirmasi", "err");
        		}
    		
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
		}
		return mapping.findForward(SUCCESS);
	}
	
	private String isValidUser(HttpServletRequest request) {
		int level; int validLevel = 5; 
		  if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
			  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
			  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
			  return "loginpage"; 
		  }
	  
		  level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
		  if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
			  return "success";
		  }else { 
			  request.setAttribute("message", "Unauthorized Access!");
			  request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
			  request.setAttribute("error_code", "Error_timeout"); return "msgpage";
		  }
	}
}
