package com.muamalat.reportmcb.action;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;

public class dwdRepCognos extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(dwdRepCognos.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }
        try{
	        String filename = request.getParameter("filename");
	        String dir =(String) request.getSession(true).getAttribute("dir");
	        
	        	File fl = new File(dir+"/"+filename);
	        	//System.out.println("dir :"+ dir);
                if (fl.exists()) {
                    try {
                    	int length = 0;
                        ServletOutputStream op = response.getOutputStream();
                        response.reset();
                        ServletContext context = servlet.getServletContext();
                        String mimetype = context.getMimeType(StaticParameter.DOWNLOAD_FOLDER + "/" + filename);
                        response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
                        response.setContentLength((int) fl.length());
                        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                        byte[] bbuf = new byte[1024];
                        DataInputStream in = new DataInputStream(new FileInputStream(fl));

                        while ((in != null) && ((length = in.read(bbuf)) != -1)) {
                            op.write(bbuf, 0, length);
                        }
                        
                        in.close();
                        op.flush();
                        op.close();
                    } catch (IOException ex) {
                        //System.out.println("error in donwload : " + ex);
                    }
                }
           
        }catch(Exception e){
            	request.setAttribute("errpage", "Error : " + e.getMessage());
        }
        return mapping.findForward("success");        	
    
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }

}
