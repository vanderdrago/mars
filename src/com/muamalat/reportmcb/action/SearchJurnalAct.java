package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class SearchJurnalAct extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(ViewJurnalAct.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }

        List lbb = new ArrayList();
        SqlFunction f = new SqlFunction();

        String trnRefNo = request.getParameter("trnRefNo");
        String tgl = request.getParameter("tgl").trim();
        String role = request.getParameter("role");
        System.out.println("role="+role+ role.contains("HCO"));
        
        lbb = f.searchBukubesar(trnRefNo,tgl);

      
        request.getSession(true).setAttribute("s_title", null);
        request.getSession(true).setAttribute("s_totDB", null);
        request.getSession(true).setAttribute("s_totCR", null);
        request.getSession(true).setAttribute("s_lbb", null);
        //restric HC
        if (lbb.size() > 0 && ((!f.getCode_trn().equals("314")&&!f.getCode_trn().equals("PCM") ) || role.contains("HCO"))) {
        //if (lbb.size() > 0 ) {
       // if (role.contains("HCO") && f.getCode_trn().equals("314") ){
        	String title = "BUKU BESAR <br /> " + tgl;
            request.getSession(true).setAttribute("R003", "R003");
            request.setAttribute("s_title", title);
            request.getSession(true).setAttribute("s_title", title);
            request.setAttribute("s_totDB", f.getTotMutDb());
            request.getSession(true).setAttribute("s_totDB", f.getTotMutDb());
            request.setAttribute("s_totCR", f.getTotMutCb());
            request.getSession(true).setAttribute("s_totCR", f.getTotMutCb());
            request.setAttribute("s_lbb", lbb);
            request.getSession(true).setAttribute("s_lbb", lbb);
            //}
        }
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }

}
