package com.muamalat.reportmcb.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class tes5th {

	public static void main(String args[]) throws Exception {
		
		int plafond = 50000000;
		int tenor = 60;
		
		double pertama = (plafond * ((5.00/100/12) / (1 - (1/Math.pow((1+(5.00/100/12)), tenor)))));
		double kedua = ((11.123206/100/12) / (1 - (1/Math.pow((1+(11.123206/100/12)), 24))));
				
		BigDecimal angsuranPertama = new BigDecimal(pertama);
		angsuranPertama = angsuranPertama.setScale(4, RoundingMode.HALF_UP);
		BigDecimal angsuranKedua = new BigDecimal(kedua);
		angsuranKedua = angsuranKedua.setScale(8, RoundingMode.HALF_UP);
		
		BigDecimal resultPlafond = new BigDecimal(plafond);
		BigDecimal resultOs = resultPlafond;
		
		 
		BigDecimal pokokPertama = new BigDecimal(0);
		BigDecimal marginPertama = new BigDecimal(0);
		BigDecimal pokokKedua = new BigDecimal(0);
		BigDecimal marginKedua = new BigDecimal(0);
		BigDecimal resultAngsuranPertama = new BigDecimal(0);
		BigDecimal resultAngsuranKedua = new BigDecimal(0);
		BigDecimal sumPertama = new BigDecimal(0);
		BigDecimal sumKedua = new BigDecimal(0);
		BigDecimal TotalMargin = new BigDecimal(0);
		BigDecimal TotalAngsuran = new BigDecimal(0);
		
		List<HashMap> map = new ArrayList<HashMap>();
		for (int i = 1; i < tenor + 1; i++)
		{
			if (i < 37){
				HashMap temp = new HashMap();
				double rumus = (11.123207/100/12);
				BigDecimal tempRumus = new BigDecimal(rumus);
				
				resultAngsuranPertama = angsuranPertama;
				marginPertama = tempRumus.multiply(resultOs);
				marginPertama = marginPertama.setScale(2, RoundingMode.HALF_UP);
				pokokPertama = angsuranPertama.subtract(marginPertama);
				resultOs = resultOs.subtract(pokokPertama);
				sumPertama = sumPertama.add(resultAngsuranPertama);
				map.add(temp);
				System.out.println("Bulan :" + i + "Os : " + resultOs + "Pokok : " + pokokPertama + "Margin : " + marginPertama + "Angsuran :" + resultAngsuranPertama);
			}
		}
		
		BigDecimal resultOsKedua = resultOs;
		BigDecimal tempOsKedua = resultOsKedua;
		for (int i = 1; i < tenor + 1; i++)
		{
			if (i > 36)
			{
				HashMap temp = new HashMap();
				double rumus = (11.123207/100/12);
				BigDecimal tempRumus = new BigDecimal(rumus);
				
				resultAngsuranKedua = resultOs.multiply(angsuranKedua);
				resultAngsuranKedua = resultAngsuranKedua.setScale(2, RoundingMode.HALF_UP);
				marginKedua = tempRumus.multiply(tempOsKedua);
				marginKedua = marginKedua.setScale(2, RoundingMode.HALF_UP);
				pokokKedua = resultAngsuranKedua.subtract(marginKedua);
				tempOsKedua = tempOsKedua.subtract(pokokKedua);
				sumKedua = sumKedua.add(resultAngsuranKedua);
				map.add(temp);
				System.out.println("Bulan :" + i + "Os : " + tempOsKedua + "Pokok : " + pokokKedua + "Margin : " + marginKedua + "Angsuran :" + resultAngsuranKedua);
			}
		}
		
		TotalAngsuran = sumPertama.add(sumKedua);
		TotalMargin = TotalAngsuran.subtract(resultPlafond);
		
		BigDecimal TempSisaAngsuran = TotalAngsuran;
		BigDecimal SisaAngsuran = TempSisaAngsuran;
		
		System.out.println("TotalAngsuran :" + TotalAngsuran + "Angsuran Pertama : " + resultAngsuranPertama + "Kedua :" + resultAngsuranKedua);
		
		List<HashMap> MapResult2 = new ArrayList<HashMap>();
		
		for (int j = 1; j < map.size(); j++) {
			HashMap<String, Object> x = map.get(j);
			if(j < 37){
				SisaAngsuran = SisaAngsuran.subtract(resultAngsuranPertama);
			} else {
				SisaAngsuran = SisaAngsuran.subtract(resultAngsuranKedua);
			}
			System.out.println("Bulan : " + j + "sisa :" + SisaAngsuran);
		}
		
//		BigDecimal tempSisa = SisaAngsuran;
//		BigDecimal tempSisaAngsuran = tempSisa;
//		System.out.println(tempSisa);
//		
//		for (int j = 1; j < map.size(); j++) {
//			if (j > 36){
//				tempSisaAngsuran = tempSisaAngsuran.subtract(resultAngsuranKedua);
//			}
//			System.out.println("Bulan : " + j + "sisa :" + tempSisaAngsuran);
//		}
		
		
		
	
	}
}
