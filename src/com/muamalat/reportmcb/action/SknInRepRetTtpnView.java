package com.muamalat.reportmcb.action;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jboss.logging.Logger;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.bean.SknInRepRetTtpnBean;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.BranchMaster;
import com.muamalat.reportmcb.entity.Member_bank;
import com.muamalat.reportmcb.entity.SknIn;
import com.muamalat.reportmcb.function.DataFunction;
import com.muamalat.reportmcb.function.FileFunction;
import com.muamalat.reportmcb.function.SknInFunction;
import com.muamalat.reportmcb.perantara.PerantaraRpt;

public class SknInRepRetTtpnView extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(SknInRepRetTtpnView.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }

        try{
            String opt = request.getParameter("btn"); 
            SknInFunction rf = new SknInFunction();
            DataFunction df = new DataFunction();
    		PerantaraRpt p = new PerantaraRpt(); 
            if ("xx".equals(opt)){      
            	SknInRepRetTtpnBean inptskn = new SknInRepRetTtpnBean();
            	DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    	        Date date = new Date();
            	inptskn.setBsnsdt(dateFormat.format(date));
                String cab = request.getParameter("cab"); 
            	inptskn.setKdcab(cab);
                String rpt = request.getParameter("rpt");
                inptskn.setRpt(rpt);

                BranchMaster mb = df.getBranchMaster(inptskn.getKdcab());
            	String filename = "";
            	String rep = "";
            	String pathJsRpt = "";
            	if("03".equals(inptskn.getRpt())){
            		filename = "REP_RETUR_SKN_IN_"+mb.getBrnch_cd();
            		rep = "REPORT RETUR SKN INCOMING";
            		pathJsRpt = "com/muamalat/reportmcb/report/RepSknInRetTitipan.jasper";
            	} else if("05".equals(inptskn.getRpt())){
            		filename = "REP_TITIPAN_SKN_IN_"+mb.getBrnch_cd();   
            		rep = "REPORT TITIPAN SKN INCOMING"; 
            		pathJsRpt = "com/muamalat/reportmcb/report/RepSknInTitipan.jasper";
            	}
            	
            	String pathfile = "/opt/temp/" + filename;
//        		String pathfile = "C:\\" + filename;
        		JasperReport jasperReport;
                JasperPrint jasperPrint;
        		try {
        			byte[] bytes = null;
            		HashMap params = new HashMap();	            		
                    params.put("namafile", rep);
                    params.put("cabang", mb.getBrnch_cd() + " - " + mb.getBrnch_name());
                    params.put("tgl", inptskn.getBsnsdt());
                    List lhist = rf.getSknInRepRetTtpn(inptskn);
                    if(lhist.size() > 0){
	            		ArrayList arrHist = p.moveListToHashMapRepSknIn(lhist);
                        if (!arrHist.isEmpty()) {
                            JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
                            jasperReport = (JasperReport) JRLoader.loadObjectFromLocation(pathJsRpt);
                            jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);
                        	JRExporter exporter = new JRPdfExporter();
                            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                            response.setContentType("application/pdf");
                            response.setHeader("Content-disposition", "attachment;filename="+filename+".pdf");
                            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                            exporter.exportReport();

                            bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                            
                            response.getOutputStream().write(bytes, 0, bytes.length);
                            
                            //filename = "HisReqCard_" + user.getKodecabang() + "_" + StringFunction.getCurrentDateYYYYMMDDHHMMSS() + ".pdf";
                            response.getOutputStream().flush();
                            response.getOutputStream().close();
                        }
                		return null;
                    } else {
                    	request.setAttribute("teks", "Tidak ditemukan data Retur/Titipan.");  
                    	request.setAttribute("respon", "teks"); 
                    }
                }
        		catch(Exception ex){
        	          //System.out.println("err 1 : " + ex.getMessage());
        			System.out.println("error : " + ex.getMessage());
        		}
            } else {
				request.setAttribute("lrkpskn", null);	
            	SknInRepRetTtpnBean inptskn = (SknInRepRetTtpnBean) form; 
        		
            	if (inptskn != null){  
                    
                    int sawal = 1;
                    int sakhir = 10;
                    int page = 1;
                    int recordsPerPage = 30;
            		int noOfRecords = 0;
                    int noOfPages = 0;
                    
                    BranchMaster mb = df.getBranchMaster(inptskn.getKdcab());
                    
                    if (mb != null){
                        if("Download".equals(opt)){                	
                        	String filename = "";
                        	String rep = "";
                        	String pathJsRpt = "";
                        	if("03".equals(inptskn.getRpt())){
                        		filename = "REP_RETUR_SKN_IN_"+mb.getBrnch_cd();
                        		rep = "REPORT RETUR SKN INCOMING";
                        		pathJsRpt = "com/muamalat/reportmcb/report/RepSknInRetTitipan.jasper";
                        	} else if("05".equals(inptskn.getRpt())){
                        		filename = "REP_TITIPAN_SKN_IN_"+mb.getBrnch_cd();   
                        		rep = "REPORT TITIPAN SKN INCOMING"; 
                        		pathJsRpt = "com/muamalat/reportmcb/report/RepSknInTitipan.jasper";
                        	}
                        	
                        	String pathfile = "/opt/temp/" + filename;
//                    		String pathfile = "C:\\" + filename;
                    		JasperReport jasperReport;
                            JasperPrint jasperPrint;
                    		try {
                    			byte[] bytes = null;
        	            		HashMap params = new HashMap();	            		
        	                    params.put("namafile", rep);
        	                    params.put("cabang", mb.getBrnch_cd() + " - " + mb.getBrnch_name());
        	                    params.put("tgl", inptskn.getBsnsdt());
        	                    List lhist = rf.getSknInRepRetTtpn(inptskn);
        	                    if(lhist.size() > 0){
            	            		ArrayList arrHist = p.moveListToHashMapRepSknIn(lhist);
                                    if (!arrHist.isEmpty()) {
                                        JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
                                        jasperReport = (JasperReport) JRLoader.loadObjectFromLocation(pathJsRpt);
                                        jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);
                                    	JRExporter exporter = new JRPdfExporter();
                                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                                        response.setContentType("application/pdf");
                                        response.setHeader("Content-disposition", "attachment;filename="+filename+".pdf");
                                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                                        exporter.exportReport();

                                        bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                                        
                                        response.getOutputStream().write(bytes, 0, bytes.length);
                                        
                                        //filename = "HisReqCard_" + user.getKodecabang() + "_" + StringFunction.getCurrentDateYYYYMMDDHHMMSS() + ".pdf";
                                        response.getOutputStream().flush();
                                        response.getOutputStream().close();
                                    }
                            		return null;
        	                    } else {
                                	request.setAttribute("teks", "Tidak ditemukan data Retur/Titipan.");  
                                	request.setAttribute("respon", "teks"); 
        	                    }
                            }
                    		catch(Exception ex){
                    	          //System.out.println("err 1 : " + ex.getMessage());
                    			System.out.println("errrrr : " + ex.getMessage());
                    		} 
                        } else {
                            SknIn skn = rf.getTotNomSknInRepRetTtpn(inptskn);
                            List ltrnskninrettit = rf.getSknInRepRetTtpn(inptskn); 
                            List ltrnsknin = p.getSknInRepRetTtpn(ltrnskninrettit, 1, recordsPerPage);
                            
                        	if (ltrnsknin.size() > 0 && ltrnsknin != null) {
                        		if (skn != null) {
                        			String title = "";
                                	if("03".equals(inptskn.getRpt())){
                                		title = "REPORT RETUR SKN INCOMING";
                                	} else if("05".equals(inptskn.getRpt())){
                                		title = "REPORT TITIPAN SKN INCOMING";   
                                        request.setAttribute("clm", "05");         		
                                	}
                                	title += "<br /> Cabang : " + mb.getBrnch_cd() + "-" + mb.getBrnch_name();
                                	request.setAttribute("teks", "<h3>"+title+"</h3>");  
                                	request.setAttribute("respon", "teks"); 
                        			noOfRecords = 0;
                                    noOfPages = 0;
                                    noOfRecords = skn.getTottrns();
                                    noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                                    if (noOfPages < sakhir){
                                    	sakhir = noOfPages;
                                    }  
                                    request.setAttribute("sawal", sawal);
                                    request.setAttribute("sakhir", sakhir);
                                    request.setAttribute("ltrnsknin", ltrnsknin);
                                    request.setAttribute("trans", skn);
                                    request.setAttribute("noOfRecords", noOfRecords);
                                    request.setAttribute("noOfPages", noOfPages);
                                    request.setAttribute("currentPage", page);
                                    request.getSession(true).setAttribute("title", title);
                                    request.getSession(true).setAttribute("trans", skn);
                                    request.getSession(true).setAttribute("noOfRecords", noOfRecords);
                                    request.getSession(true).setAttribute("ltrnskninrettit", ltrnskninrettit);
                                    request.getSession(true).setAttribute("noOfPages", noOfPages);
                                    request.setAttribute("data", "out");
                            	} else {
                                	request.setAttribute("teks", "Tidak ditemukan data Retur/Titipan.");  
                                	request.setAttribute("respon", "teks");       		
                            	}
                            } else {
                            	request.setAttribute("teks", "Tidak ditemukan data Retur/Titipan.");  
                            	request.setAttribute("respon", "teks");
                        	}  
                            
                        }    
                    } else {
                    	request.setAttribute("teks", "Kode cabang tidak ditemukan.");  
                    	request.setAttribute("respon", "teks");
                	}             	
                    request.getSession(true).setAttribute("inptskn", inptskn);
                    
            	} else {
                	request.setAttribute("teks", "Tidak ditemukan data Retur/Titipan.");  
                	request.setAttribute("respon", "teks");        		
            	}
            }
            return mapping.findForward(SUCCESS);
        }
        catch(Exception e){
        	request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward("failed");        	
        }
    }
        
        
    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
    }

}
