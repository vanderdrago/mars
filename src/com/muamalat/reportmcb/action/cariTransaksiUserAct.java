package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;

public class cariTransaksiUserAct extends org.apache.struts.action.Action {
	
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(cariTransaksiUserAct.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		try{ 
			List lphUserRef = new ArrayList(); 
			SqlFunction sql = new SqlFunction();
			String userRef = request.getParameter("userRef");
			String tgl = request.getParameter("tgl");
			System.out.println("user :" + userRef);
			int noOfRecords = 0;
			int sawal = 1;
			int sakhir = 10; 
			int page = 1; 
			int noOfPages = 0; 
			int recordsPerPage = 1000;
			
			noOfRecords = sql.getCountUser(userRef);
			System.out.println("record :" + noOfRecords);
			if (noOfRecords > 0){
				noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
				  if (noOfPages < sakhir) {
					  sakhir = noOfPages;
				  }
				  lphUserRef = sql.getSearchUser(userRef);
				  request.setAttribute("sawalNorek", sawal); 
				  request.setAttribute("sakhirNorek", sakhir); 
				  request.setAttribute("lphUserRef", lphUserRef); 
				  request.setAttribute("noOfRecordsNorek", noOfRecords);
				  request.setAttribute("noOfPagesNorek", noOfPages); 
				  request.setAttribute("currentPageNorek", page); 
				  request.getSession(true).setAttribute("pageNorek", page); 
				  request.getSession(true).setAttribute("llsp", lphUserRef); 
				  request.getSession(true).setAttribute("recordsNorek", noOfRecords);
				  request.getSession(true).setAttribute("noOfPagesNorek", noOfPages);
				  request.getSession(true).setAttribute("userRef", userRef);
			} 
			return mapping.findForward(SUCCESS); 
		} catch (Exception e) {
		  request.setAttribute("errpage", "Error : " + e.getMessage()); 
		  return mapping.findForward("failed"); 
	   }
		
	}
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "cariTransaksiUser.do")){
			List lphUserRef = (List) request.getSession(true).getAttribute("lphUserRef");
			request.setAttribute("lphUserRef", lphUserRef);
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            return "msgpage";
		}
	}
	
	
}
