package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.SqlFunctionNew;
import com.muamalat.reportmcb.perantara.PerantaraRpt;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;


public class downloadPinpad extends org.apache.struts.action.Action{
	
	private static Logger log = Logger.getLogger(downloadPinpad.class);

	/* forward name="success" path="" */
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser (request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		
		try {
			 String termId = (String) request.getSession(true).getAttribute("termId");
			 String tgl1 = request.getParameter("tgl1");
			 String tgl2 = request.getParameter("tgl2");
			 String cari = request.getParameter("btn");
			 String dwn = request.getParameter("btndwnld");
			 System.out.println("termId:" + termId + "tgl1:" + tgl1 + "tgl2:" + tgl2 + "cari:" + cari + "dwn:" + dwn);
			 SqlFunctionNew sql = new SqlFunctionNew();
			 
			 if("Cari".equals(cari)){
				 System.out.println("if");
				 List download = sql.getSearchPinpad(termId, tgl1, tgl2);
				 if (download.size() > 0){
					 request.setAttribute("detail", download);
	    			 request.getSession(true).setAttribute("edc", download);
				 } else {
	        		String respon = "Tidak ditemukan Record Data yang Dicari.";
	        		request.setAttribute("respon", respon);
	        		request.setAttribute("confrm", "err");
	        	}
			 } else if ("Cetak".equals(dwn)){
				 System.out.println("else");
				 List cetak = sql.getDownloadPinpad(termId, tgl1, tgl2);
				 System.out.println("size:" + cetak.size());
				 if (cetak.size() > 0){
					 String pathjasper = "";
					 pathjasper = "com/muamalat/reportmcb/report/Lap_Pinpad.jasper";
					 JasperReport jasper;
		    		 JasperPrint jasperPrint;
		    		 String formatFile = "pdf";
		    		 byte[] bytes = null;
					 PerantaraRpt p = new PerantaraRpt();
					 HashMap param = new HashMap();
					 String fileName = "Laporan_edc_Terminal_Id_" + termId;
					 ArrayList arrHist = null;
					 if (cetak.size() > 0){
						 System.out.println("if");
						 arrHist = p.moveListToHashMapPinpad(cetak);
					 } else {
						 System.out.println("else");
						 HashMap rowMap = null;
	                     ArrayList detail = new ArrayList();
						 rowMap.put("tgl", "");
						 rowMap.put("termId", "");
						 rowMap.put("mercId", "");
						 rowMap.put("kartu", "");
						 rowMap.put("acNo", "");
						 rowMap.put("trnRef", "");
						 rowMap.put("status", "");
						 rowMap.put("branch", "");
						 rowMap.put("mercName", "");
						 rowMap.put("tglCetak", "");
						 detail.add(rowMap);
		                 arrHist = detail;
					 }
					 	JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
						jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathjasper);
						jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
						JRExporter exporter = new JRPdfExporter();
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
						response.setContentType("application/pdf");
						response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
						exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
						exporter.exportReport();
						bytes = JasperExportManager.exportReportToPdf(jasperPrint);
		                response.getOutputStream().write(bytes, 0, bytes.length); 
		                response.getOutputStream().flush();
		                response.getOutputStream().close();
				 }
				 return mapping.findForward(SUCCESS); 
			 }
//			 if (cari.equals("Cari")){
//				 System.out.println("if");
//				 List download = sql.getSearchPinpad(termId, tgl1, tgl2);
//				 if (download.size() > 0){
//					 request.setAttribute("detail", download);
//	    			 request.getSession(true).setAttribute("edc", download);
//				 } else {
//	        			String respon = "Tidak ditemukan Record Data yang Dicari.";
//	        			request.setAttribute("respon", respon);
//	        			request.setAttribute("confrm", "err");
//	        	}
//			 } else if (dwn.equals("Cetak")){
//				 System.out.println("else");
//				 List cetak = sql.getDownloadPinpad(termId, tgl1, tgl2);
//				 System.out.println("size:" + cetak.size());
//				 if (cetak.size() > 0){
//					 String pathjasper = "";
//					 pathjasper = "com/muamalat/reportmcb/report/Lap_Pinpad.jasper";
//					 JasperReport jasper;
//		    		 JasperPrint jasperPrint;
//		    		 String formatFile = "pdf";
//		    		 byte[] bytes = null;
//					 PerantaraRpt p = new PerantaraRpt();
//					 HashMap param = new HashMap();
//					 String fileName = "Laporan_edc_Terminal_Id_" + termId;
//					 ArrayList arrHist = null;
//					 if (cetak.size() > 0){
//						 System.out.println("if");
//						 arrHist = p.moveListToHashMapPinpad(cetak);
//					 } else {
//						 System.out.println("else");
//						 HashMap rowMap = null;
//	                     ArrayList detail = new ArrayList();
//						 rowMap.put("tgl", "");
//						 rowMap.put("termId", "");
//						 rowMap.put("mercId", "");
//						 rowMap.put("kartu", "");
//						 rowMap.put("acNo", "");
//						 rowMap.put("trnRef", "");
//						 rowMap.put("status", "");
//						 rowMap.put("branch", "");
//						 rowMap.put("mercName", "");
//						 rowMap.put("tglCetak", "");
//						 detail.add(rowMap);
//		                 arrHist = detail;
//					 }
//					 	JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
//						jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathjasper);
//						jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
//						JRExporter exporter = new JRPdfExporter();
//						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//						response.setContentType("application/pdf");
//						response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
//						exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//						exporter.exportReport();
//						bytes = JasperExportManager.exportReportToPdf(jasperPrint);
//		                response.getOutputStream().write(bytes, 0, bytes.length); 
//		                response.getOutputStream().flush();
//		                response.getOutputStream().close();
//				 }
//				 return mapping.findForward(SUCCESS); 
//			 }
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
		}
		return mapping.findForward(SUCCESS);
	}

	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "LapSpvTeller.do")){
			List lphTeller = (List) request.getSession(true).getAttribute("lphTeller");
			request.setAttribute("lphTeller", lphTeller);
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            //request.setAttribute("error_code", "Error_timeout");
            return "msgpage";
		}
	}
	
}
