package com.muamalat.reportmcb.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.bean.IntfRtgsOutBean;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.function.FileFunction;
import com.muamalat.reportmcb.function.McbFunction;

public class RtgsMcbView extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(RtgsMcbView.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);

        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }

        request.getSession(true).setAttribute("trans", null);
        request.getSession(true).setAttribute("noOfRecords", null);
        request.getSession(true).setAttribute("noOfPages", null);
        request.getSession(true).setAttribute("opt", null);
        
        IntfRtgsOutBean intfOut = (IntfRtgsOutBean) form;  
        String opt = request.getParameter("rtgsmcb");
        String optview = request.getParameter("btn");
        
        int sawal = 1;
        int sakhir = 10;
        int page = 1;
        int recordsPerPage = 10;
		int noOfRecords = 0;
        int noOfPages = 0;
        
        McbFunction rf = new McbFunction();
        List ltrans = null;
    	int tottrans = rf.getTotRtgsMcb(intfOut, opt, "RTGSBI");
        if("out".equals(opt)){ 
        	if (tottrans > 0){  
            		if("Download".equals(optview)){
                    	ltrans = rf.getListRtgsMcb(intfOut, opt, sawal, tottrans, "RTGSBI"); 
            			FileFunction ff = new FileFunction();
            			ff.downloadRtgsMcbXls(ltrans, request, response, intfOut, "RTGSBI");
            			return null;
            		} else {
                    	ltrans = rf.getListRtgsMcb(intfOut, opt, sawal, sakhir, "RTGSBI"); 
	                	if (ltrans.size() > 0) {
	                		String title = "<h3> RTGS MCB OUTGOING</h3>";
	                    	request.setAttribute("teks", title);  
	                    	request.setAttribute("respon", "teks"); 
	            			noOfRecords = 0;
	                        noOfPages = 0;
	                        noOfRecords = tottrans;
	                        noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
	                        if (noOfPages < sakhir){
	                        	sakhir = noOfPages;
	                        }
	                        request.getSession(true).setAttribute("title", title);
	                        request.getSession(true).setAttribute("intfOut", intfOut);
	                        request.setAttribute("data", "out");
	            		
	                	} else {
	                    	request.setAttribute("teks", "Tidak ditemukan data transaksi RTGS.");  
	                    	request.setAttribute("respon", "teks");       		
	                	}
            		}
        	} else {
            	request.setAttribute("teks", "Tidak ditemukan data transaksi RTGS.");  
            	request.setAttribute("respon", "teks");        		
        	}
        } else if("in".equals(opt)){
        	if (tottrans > 0){  
        		if("Download".equals(optview)){
                	ltrans = rf.getListRtgsMcb(intfOut, opt, sawal, tottrans, "RTGSBI"); 
        			FileFunction ff = new FileFunction();
        			ff.downloadRtgsMcbIncXls(ltrans, request, response, intfOut, "RTGSBI");
        			return null;
        		} else {
                	ltrans = rf.getListRtgsMcb(intfOut, opt, sawal, sakhir, "RTGSBI"); 
                	if (ltrans.size() > 0) {
                		String title = "<h3> RTGS MCB INCOMING</h3>";
                    	request.setAttribute("teks", title);  
                    	request.setAttribute("respon", "teks"); 
            			noOfRecords = 0;
                        noOfPages = 0;
                        noOfRecords = tottrans;
                        noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
                        if (noOfPages < sakhir){
                        	sakhir = noOfPages;
                        }
                        request.getSession(true).setAttribute("title", title);
                        request.getSession(true).setAttribute("intfOut", intfOut);
                        request.setAttribute("data", "in");
            		
                	} else {
                    	request.setAttribute("teks", "Tidak ditemukan data transaksi RTGS.");  
                    	request.setAttribute("respon", "teks");       		
                	}
        		}
    	} else {
        	request.setAttribute("teks", "Tidak ditemukan data transaksi RTGS.");  
        	request.setAttribute("respon", "teks");        		
    	}
        }        
        request.setAttribute("sawal", sawal);
        request.setAttribute("sakhir", sakhir);
        request.setAttribute("ltrans", ltrans);
        request.setAttribute("noOfRecords", noOfRecords);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);
        request.getSession(true).setAttribute("noOfRecords", noOfRecords);
        request.getSession(true).setAttribute("noOfPages", noOfPages);
        request.getSession(true).setAttribute("opt", opt);
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }
    }

}
