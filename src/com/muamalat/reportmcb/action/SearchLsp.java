package com.muamalat.reportmcb.action;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.Gl;
import com.muamalat.reportmcb.entity.Lsp;
import com.muamalat.reportmcb.entity.ReportNeraca;
import com.muamalat.reportmcb.function.FileFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class SearchLsp extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(SearchLsp.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        SUCCESS = isValidUser(request);

        Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
        
        if (!SUCCESS.equals("success")) {
            return mapping.findForward(SUCCESS);
        }

        request.getSession(true).setAttribute("ssl_ab", null);
        request.getSession(true).setAttribute("cab", null);
        request.getSession(true).setAttribute("val", null);
        request.getSession(true).setAttribute("llsp", null);
        
        List lspbf = new ArrayList();
        List lspaf = new ArrayList();
        List llsp = new ArrayList();
        List lsgl = new ArrayList();
        SqlFunction f = new SqlFunction();
        String ssl_a = request.getParameter("ssl_a");
        String ssl_b = request.getParameter("ssl_b").trim();
        String cab = request.getParameter("cab");
        String val = request.getParameter("val").trim();
        String allcab = request.getParameter("allcab");
        String allval = request.getParameter("allval");

        String cbxspnoll = request.getParameter("cbxspnoll");
          
        //llsp = f.getListLspTotal(val, cab, ssl_a, ssl_b, allcab, allval, cbxspnoll); 

        lsgl = f.getListGL(ssl_a, ssl_b);
        Lsp bb = null;
        for (Iterator<Gl> i = lsgl.iterator(); i.hasNext();) {
        	Gl g = i.next();
        	bb = new Lsp();
        	lspbf = f.getListLspBefore(val, cab, g.getGl_code(), allcab, allval, cbxspnoll);
        	System.out.println("val :" + val + "cab :" + cab + "gl :" + g.getGl_code() + "allcab :" + allcab + "allval :" + allval);
        	lspaf = f.getLspCurrent(g.getGl_code(), cab, val, allcab, allval);
        	if(lspbf.size() > 0 && lspaf.size() > 0){
        		if (lspbf.size() >= lspaf.size()){
                    for (Iterator<Lsp> b = lspbf.iterator(); b.hasNext();) {
                    	Lsp bf = b.next();
                    	bb = new Lsp();
                    	bb.setGl_code(g.getGl_code());
                    	bb.setGl_desc(g.getGl_desc());
                    	Lsp af_t = null; 
                    	for (Iterator<Lsp> a = lspaf.iterator(); a.hasNext();) {
                    		Lsp af = a.next();  
                    		if(af.getCcy().equals(bf.getCcy()) && af.getBranch_code().equals(bf.getBranch_code()) && af.getGl_code().equals(bf.getGl_code())){
                    			af_t = af;
                    			lspaf.remove(af);
                    			break;
                    		} 
                        } 
                    	if(af_t != null){
                        	bb.setBranch_code(af_t.getBranch_code());
                        	bb.setCcy(af_t.getCcy());
                        	if ("IDR".equals(af_t.getCcy())){
                            	bb.setMov_lcy(bf.getLcy_closing_bal().add(af_t.getSld_prcobaan()));                		
                        	} else {
                            	bb.setMov_lcy(bf.getAcy_closing_bal().add(af_t.getSld_prcobaan()));
                        	}
                    	} else {
                    		bb.setBranch_code(bf.getBranch_code());
                        	bb.setCcy(bf.getCcy());
                        	if ("IDR".equals(bf.getCcy())){
                            	bb.setMov_lcy(bf.getLcy_closing_bal());                		
                        	} else {
                        		bb.setMov_lcy(bf.getAcy_closing_bal());
                        	}
                    	}
                    	if (cbxspnoll == null){
        					if(bb.getMov_lcy().compareTo(new BigDecimal(0)) != 0){	
        			        	llsp.add(bb);					
        					} 
        		        } else {
        		        	llsp.add(bb);
        		        }
                    }  
                    if (lspaf.size() > 0){
                        for (Iterator<Lsp> a = lspaf.iterator(); a.hasNext();) {
                    		Lsp af = a.next(); 
                        	bb = new Lsp();
                        	bb.setGl_code(g.getGl_code());
                        	bb.setGl_desc(g.getGl_desc());
                        	bb.setBranch_code(af.getBranch_code());
                        	bb.setCcy(af.getCcy());
                        	if ("IDR".equals(af.getCcy())){
                            	bb.setMov_lcy(af.getSld_prcobaan());                		
                        	} else {
                            	bb.setMov_lcy(af.getSld_prcobaan());
                        	}
        		        	llsp.add(bb);
                        }                     	
                    }
        		} else {
        			for (Iterator<Lsp> a = lspaf.iterator(); a.hasNext();) {
        				Lsp af = a.next();
            			bb = new Lsp();
                    	bb.setGl_code(g.getGl_code());
                    	bb.setGl_desc(g.getGl_desc()); 
                    	Lsp bf_t = null; 
        				for (Iterator<Lsp> b = lspbf.iterator(); b.hasNext();) {
        					Lsp bf = a.next();    
        					if(af.getCcy().equals(bf.getCcy()) && af.getBranch_code().equals(bf.getBranch_code()) && af.getGl_code().equals(bf.getGl_code())){
        						bf_t = bf;
        						lspbf.remove(af);
                    			break;
                    		} 
                        }
        				if(bf_t != null){
                        	bb.setBranch_code(bf_t.getBranch_code());
                        	bb.setCcy(bf_t.getCcy());
                        	if ("IDR".equals(bf_t.getCcy())){
                            	bb.setMov_lcy(bf_t.getLcy_closing_bal().add(bf_t.getSld_prcobaan()));                		
                        	} else {
                            	bb.setMov_lcy(bf_t.getAcy_closing_bal().add(bf_t.getSld_prcobaan()));
                        	}
                    	} else {
                    		bb.setBranch_code(af.getBranch_code());
                        	bb.setCcy(af.getCcy());
                        	if ("IDR".equals(af.getCcy())){
                            	bb.setMov_lcy(af.getLcy_closing_bal());                		
                        	} else {
                        		bb.setMov_lcy(af.getAcy_closing_bal());
                        	}
                    	}
        				if (cbxspnoll == null){
        					if(bb.getMov_lcy().compareTo(new BigDecimal(0)) != 0){	
        			        	llsp.add(bb);					
        					} 
        		        } else {
        		        	llsp.add(bb);
        		        }
                    } 
                    if (lspbf.size() > 0){
            			for (Iterator<Lsp> a = lspbf.iterator(); a.hasNext();) {
                    		Lsp bf = a.next(); 
                        	bb = new Lsp();
                        	bb.setGl_code(g.getGl_code());
                        	bb.setGl_desc(g.getGl_desc());
                        	bb.setBranch_code(bf.getBranch_code());
                        	bb.setCcy(bf.getCcy());
                        	if ("IDR".equals(bf.getCcy())){
                            	bb.setMov_lcy(bf.getLcy_closing_bal());                		
                        	} else {
                            	bb.setMov_lcy(bf.getAcy_closing_bal());
                        	}
        		        	llsp.add(bb);
                        } 
                    	
                    }
        		}      		
        	} else if(lspbf.size() > 0 ){
        		for (Iterator<Lsp> af = lspbf.iterator(); af.hasNext();) {
        			Lsp p = af.next();
        			bb = new Lsp();
                	bb.setGl_code(g.getGl_code());
                	bb.setGl_desc(g.getGl_desc());
                	bb.setBranch_code(p.getBranch_code());
                	bb.setCcy(p.getCcy());
                	if ("IDR".equals(p.getCcy())){
                    	bb.setMov_lcy(p.getLcy_closing_bal());                		
                	} else {
                		bb.setMov_lcy(p.getAcy_closing_bal());
                	}
                	if (cbxspnoll == null){
    					if(bb.getMov_lcy().compareTo(new BigDecimal(0)) != 0){	
    			        	llsp.add(bb);					
    					} 
    		        } else {
    		        	llsp.add(bb);
    		        }
        		}
        	} else if(lspaf.size() > 0){
        		for (Iterator<Lsp> af = lspaf.iterator(); af.hasNext();) {
        			Lsp p = af.next();
        			bb = new Lsp();
                	bb.setGl_code(g.getGl_code());
                	bb.setGl_desc(g.getGl_desc());
                	bb.setBranch_code(p.getBranch_code());
                	bb.setCcy(p.getCcy());
                	bb.setMov_lcy(p.getSld_prcobaan());
                	if (cbxspnoll == null){
    					if(bb.getMov_lcy().compareTo(new BigDecimal(0)) != 0){	
    			        	llsp.add(bb);					
    					} 
    		        } else {
    		        	llsp.add(bb);
    		        }
        		}        		
        	}
        }
        
        if (allcab != null){
        	cab = "Semua Cabang";
        } 
        if (allval != null){
        	val = "Semua Valuta";
        }
        Collections.sort(llsp, new Lsp.OrderByBranch());
        
        String opt = request.getParameter("btnsp");
        if("Download".equals(opt)){
        	String filename = user.getUsername() + "_" + ssl_a + " - " + ssl_b + ".txt";
        	String pathfile = "/opt/temp/" + filename;
//    		String pathfile = "C:\\" + filename;
            FileFunction ff = new FileFunction();
        	ff.createFileLsp(pathfile, ssl_a, ssl_b, cab, val, llsp);
            File fl = new File(pathfile);
            if (fl.exists()) {
                try {
                	int length = 0;
                    ServletOutputStream op = response.getOutputStream();
                    response.reset();
                    ServletContext context = servlet.getServletContext();
                    String mimetype = context.getMimeType(StaticParameter.DOWNLOAD_FOLDER + "/" + filename);
                    response.setContentType((mimetype != null) ? mimetype : "application/octet-stream");
                    response.setContentLength((int) fl.length());
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                    byte[] bbuf = new byte[1024];
                    DataInputStream in = new DataInputStream(new FileInputStream(fl));

                    while ((in != null) && ((length = in.read(bbuf)) != -1)) {
                        op.write(bbuf, 0, length);
                    }
                    
                    in.close();
                    op.flush();
                    op.close();
                } catch (IOException ex) {
                    SUCCESS = "blank";
                }
                
                ff.deleteFile(pathfile);
            } else {
            	SUCCESS = "blank";
            }
            return mapping.findForward(SUCCESS);
        } else {
            if (llsp.size() > 0) {
                request.setAttribute("ssl_ab", ssl_a + " - " + ssl_b);
                request.getSession(true).setAttribute("ssl_ab", ssl_a + " - " + ssl_b);
                request.setAttribute("cab", cab);
                request.getSession(true).setAttribute("cab", cab);
                request.setAttribute("val", val);
                request.getSession(true).setAttribute("val", val);
                request.setAttribute("llsp", llsp);
                request.getSession(true).setAttribute("llsp", llsp);
                request.setAttribute("viewrpt", "R004");
            } else{
                request.setAttribute("respon", "data");
                request.setAttribute("teks", "Tidak ditermuakan data Saldo Percobaan");
            }
        }
        return mapping.findForward(SUCCESS);
    }

    private String isValidUser(HttpServletRequest request) {
    	
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
		        return "success";
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return "msgpage";
	        }


    }
}
