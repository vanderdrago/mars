package com.muamalat.reportmcb.action;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.BranchMaster;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.DataFunction;
import com.muamalat.reportmcb.function.EstateFunction;

public class EStateUpdt extends org.apache.struts.action.Action {
	/* forward name="success" path="" */

	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(EStateOto.class);

	/**
	 * Action called on save button click
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")) {
			return mapping.findForward(SUCCESS);
		}
		try {
        	String acc = request.getParameter("acc");
        	String opt = request.getParameter("opt");
        	EstateFunction ef = new EstateFunction();

    		E_stat_cust cust = ef.getEStatCust(acc, "estatin");
        	if("e".equals(opt.trim())){ 
        		if (cust == null){
	    			request.setAttribute("respon","Rekening tidak ditemukan.");
	    			request.setAttribute("data", "konfirm");  						
				} else {
	        		if("I".equals(cust.getCust_type().trim())) {
						request.setAttribute("jnsnsbh", "NASABAH INDIVIDU");
					} else {
						request.setAttribute("jnsnsbh", "NASABAH NON INDIVIDU");
					}
					DataFunction df = new DataFunction();
	            	E_stat_cust cust1 = ef.getEStatCust(acc, "estatin");
	            	BranchMaster bm = df.getBranchMaster(cust1.getBranch_reg().trim());
	            	cust.setBranch_reg(bm.getBrnch_cd());
	            	cust.setBranch_reg_name(bm.getBrnch_name());
	            	cust.setUser_input(cust1.getUser_input());
	            	cust.setUser_aut(cust1.getUser_aut());
	            	cust.setTgl_input(cust1.getTgl_input());
	            	cust.setEmail(cust1.getEmail());
	            	cust.setEmail_cc(cust1.getEmail_cc());
	            	
	    			request.setAttribute("cust", cust);
	    			request.setAttribute("data", "cust_edit");
	    			request.getSession(true).setAttribute("cust", cust);
				}
        		/*
        		E_stat_cust cust = ef.getCustomer(acc);
        		if (cust == null){
	    			request.setAttribute("respon","Rekening tidak ditemukan.");
	    			request.setAttribute("data", "konfirm");  						
				} else {
					if ("O".equals(cust.getRecord_stat().trim())){
						if("I".equals(cust.getCust_type().trim())) {
							request.setAttribute("jnsnsbh", "NASABAH INDIVIDU");
						} else {
							request.setAttribute("jnsnsbh", "NASABAH NON INDIVIDU");
						}
						DataFunction df = new DataFunction();
		            	E_stat_cust cust1 = ef.getEStatCust(acc, "estatin");
		            	BranchMaster bm = df.getBranchMaster(cust1.getBranch_reg().trim());
		            	cust.setBranch_reg(bm.getBrnch_cd());
		            	cust.setBranch_reg_name(bm.getBrnch_name());
		            	cust.setUser_input(cust1.getUser_input());
		            	cust.setUser_aut(cust1.getUser_aut());
		            	cust.setTgl_input(cust1.getTgl_input());
		            	cust.setEmail(cust1.getEmail());
		            	cust.setEmail_cc(cust1.getEmail_cc());
		            	
		    			request.setAttribute("cust", cust);
		    			request.setAttribute("data", "cust_edit");
		    			request.getSession(true).setAttribute("cust", cust);
					} else {
		    			request.setAttribute("respon","Rekening tidak aktif.");
		    			request.setAttribute("data", "konfirm"); 
					}
				}
        		*/
        	} else if("u".equals(opt.trim())){
//        		E_stat_cust cust = ef.getEStatCust(acc, "estatin");
        		if (cust == null){
	    			request.setAttribute("respon","Rekening tidak ditemukan.");
	    			request.setAttribute("data", "konfirm");  						
				} else {
					if("I".equals(cust.getCust_type().trim())) {
						request.setAttribute("jnsnsbh", "NASABAH INDIVIDU");
					} else {
						request.setAttribute("jnsnsbh", "NASABAH NON INDIVIDU");
					}
	    			request.setAttribute("cust", cust);
	    			request.setAttribute("data", "cust_oto_updtsts");
	    			request.getSession(true).setAttribute("cust_oto_updtsts", cust);
				}
        	} 
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
		}
		return mapping.findForward("success");

	}

	private String isValidUser(HttpServletRequest request) {

		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel);
			request.setAttribute("message","Login Session Expired, silahkan login kembali!");
			return "loginpage";
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			/*================= tambahan baru ==============*/
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			List lroles = cpf.getRolesUser(adm.getUsername());
		 	int ada = 0;
		 	if (lroles.size() > 0) {
		 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
		 			String rl = it.next();		 		
		 			String role3 = cpf.getAllowedStatement(11, rl.toUpperCase());
		 			if (!"".equals(role3) && role3!=null){
		 				ada++;
		 			}
		 		}
			} 
		 	cpf= null;
		 	if (ada > 0){
				return SUCCESS;					
		 	} else {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
		 	}
			/*
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			SMTB_USER_ROLE user = cpf.getUser(adm.getUsername(), adm.getKodecabang());
			String role = cpf.getAllowedPrintPassbook(11, user.getRole_id());
			if ("".equals(role) || role == null) {
				request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
				return "msgpage";
			} else {
				if ((role.indexOf(user.getRole_id().toUpperCase()) > -1 ? true : false)) {
					return SUCCESS;
				} else {
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
					return "msgpage";
				}
			}*/
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return "msgpage";
		}
	}

}
