package com.muamalat.reportmcb.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.UserAktifCabang;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.User;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.perantara.PerantaraRpt;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

public class transaksiToday extends org.apache.struts.action.Action {
	/* forward name="success" path="" */
	private static Logger log = Logger.getLogger(transaksiToday.class);
	private static String SUCCESS = "success";

	/** Action called on save button click
     */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response) throws
			  Exception {

		  SUCCESS = isValidUser(request);
		  Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		  if (!SUCCESS.equals("success")) {
	            return mapping.findForward(SUCCESS);
	        }
		  try {
			    List lphtoday = new ArrayList();
			    CetakPassbookFunction cpf = new CetakPassbookFunction();
			    SqlFunction sql = new SqlFunction();
			    
			    request.getSession(true).setAttribute("kodecabang", null);
	            request.getSession(true).setAttribute("userId", null);
	                        
	            String kodecabang = request.getParameter("kodecabang");
	    		String userId = request.getParameter("userId");
	    		User usr = sql.getUser(userId);
	    		System.out.println("usr :" + userId);
	    		
	    		if (usr != null) {
	    			lphtoday = sql.getListJurnalDetail(userId);
	    			
	    			String pathjasper = "";
		    		if (lphtoday.size() > 0 ) {
		    			pathjasper = "com/muamalat/reportmcb/report/statement_mutasi_new.jasper";
		    		}
		    		
		    		String opt = request.getParameter("btndwnld");
		    		if ("Cetak".equals(opt)){
		    			
		    			JasperReport jasper;
		    			JasperPrint jasperPrint;
		    			String formatFile = "pdf";
		    			/*tambahan role*/
	        			Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
	        			SMTB_USER_ROLE userRole = cpf.getUser(adm.getUsername(), adm.getKodecabang());
	        			String role = cpf.getAllowedPrintPassbook(12, userRole.getRole_id());
	        			
	        			/*================= tambahan baru ==============*/
	        			List lroles = cpf.getRolesUser(adm.getUsername());
	   				 	int ada = 0;
	   				 	System.out.println("ada :" + ada);
	   				 	if (lroles.size() > 0) {
	   				 		for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
	   				 			String rl = it.next();
	   				 			String role2 = cpf.getAllowedStatement(12, rl.toUpperCase());
	   				 			if (!"".equals(role2) && role2!=null){
	   				 				ada++;
	   				 			}
	   				 		}
		   				}
	   				 	
//	   				 if ("000".equals(adm.getKodecabang()) || ada > 0 ) {
	   				 		byte[] bytes = null;
	   				 		PerantaraRpt p = new PerantaraRpt();
	   				 		HashMap param = new HashMap();
		   				 	DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
		            	    Date dateee = new Date();
		            	    param.put("tanggal", dateFormattt.format(dateee));
		            	    param.put("user_id", usr.getUser_id());
		            	    param.put("cabang", usr.getBranch_code());
		            	    String fileName = "Statement_" + userId;
		            	    ArrayList arrHist = null;
		            	    
		            	    if (lphtoday.size() > 0 ) {
	            	    		arrHist = p.moveListToHashMapUser(lphtoday);
	            	    		 System.out.println("lph 1");
	            	    	} else {
	            	    		 System.out.println("lph 2");
	            	    		HashMap rowMap = null;
		                        ArrayList detail = new ArrayList();
		                        rowMap = new HashMap();
		                        rowMap.put("user_id", "");
	                            rowMap.put("kode_cabang", "");
	                            rowMap.put("jam", "");
	                            rowMap.put("kode_transaksi", "");
	                            rowMap.put("ket_kode_transaksi", "");
	                            rowMap.put("nomor_warkat", "");
	                            rowMap.put("debet", "");
	                            rowMap.put("kredit", "");
	                            rowMap.put("user_approval", "");
	                            rowMap.put("ac_no", "");
	                            rowMap.put("ac_desc", "");
	                            detail.add(rowMap);
		                        arrHist = detail;
	            	    	}
		            	    System.out.println("jasper :" + pathjasper);
		            	    
		            	   JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrHist);
		            	   System.out.println("arrhist :" + arrHist);
	            	       jasper = (JasperReport) JRLoader.loadObjectFromLocation(pathjasper);
	            	       System.out.println("jasper 2 :" + jasper);
	            	       System.out.println("param :" + param);
	            	       System.out.println("ds :" + ds);
	            	       
	            	      // try{
	            	       System.out.println( "jasper 2.5 :");
                           jasperPrint = JasperFillManager.fillReport(jasper, param, ds);
                           if (jasperPrint == null)
                           {
                        	   System.out.println("jasper x :" + jasperPrint);
                           }
                           System.out.println("jasper 3 :" + jasperPrint);
                         //  System.out.println("jasper 2 :" + jasperPrint);
                           JRExporter exporter = new JRPdfExporter();
                           exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                           response.setContentType("application/pdf");
                           response.setHeader("Content-disposition", "attachment;filename="+fileName+".pdf");
                           exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                           exporter.exportReport();

                           bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                           response.getOutputStream().write(bytes, 0, bytes.length);
                           
                           response.getOutputStream().flush();
                           response.getOutputStream().close();
	            	     /*  } catch(JRException ex)
	            	       {
	            	    	   ex.printStackTrace();
	            	    	   System.out.println("eror");
	            	    	   }*/
//	   				 } 
//	   				 else {
//	   					System.out.println("masuk else");
//	   					request.setAttribute("message", "Unauthorized Access!");
//						request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
//						return mapping.findForward("msgpage");
//	   				 }
		    	}else {
		    			request.setAttribute("lphtoday", lphtoday);
		                request.getSession(true).setAttribute("trnToday", lphtoday);
		                request.getSession(true).setAttribute("userId", userId);
		    	}
	    	} else {
	    		request.setAttribute("respon", "Transaksi tidak ditemukan.");
	            request.setAttribute("confrm","err");     
	    	}
		} catch (Exception e){
        	request.setAttribute("errpage", "Error : " + e.getMessage());
            return mapping.findForward(".errPage");
	    }
	return mapping.findForward(SUCCESS);
}

	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "transaksiTodayfwd.do")){
			List lphtoday = (List) request.getSession(true).getAttribute("lphtoday");
			request.setAttribute("lphtoday", lphtoday);
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            //request.setAttribute("error_code", "Error_timeout");
            return "msgpage";
		}
	}
}
