package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.entity.SaldoRataRata;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class vSaldoAct extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(vSaldoAct.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		SUCCESS = isValidUser(request);
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (!SUCCESS.equals("success")){
			 return mapping.findForward(SUCCESS);
		}
		try {
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			
			System.out.println("masuk saldo act");
			List lphSaldoDetail = new ArrayList();
			SqlFunction sql = new SqlFunction();
			
			String acc = (String) request.getSession(true).getAttribute("acc");
			String prd1 = (String) request.getParameter("prd1");
			String prd2 = (String) request.getParameter("prd2");
			List llsp = (List) request.getSession(true).getAttribute("llsp");
			
//			System.out.println("size :" + llsp.size());
//			System.out.println("llsp :" + llsp.toString());
//			
//			SaldoRataRata objSaldo = null;
//			
//			if (llsp.size() > 0){
//				for (Iterator<SaldoRataRata> i = llsp.iterator(); i.hasNext();){
//					SaldoRataRata saldo = i.next();
//					if (acc.equals(objSaldo.getAcc_no().trim())){
//						objSaldo = saldo;
//						break;
//					}
//				}
//			}
//			
//			System.out.println("product :" + objSaldo.getProduct_code());
//			String rr = "";
//			
//			SMTB_USER_ROLE user_r = cpf.getUserHCO(user.getUsername(), "HCO");
//			
//			if (user_r != null){
//				user_r = null;
//			/* ================ Untuk merestricted account ================*/
//			} else {
//				if ("S18A".equals(objSaldo.getProduct_code().toUpperCase().trim()) || "S18B".equals(objSaldo.getProduct_code().toUpperCase().trim())) { 
////						|| "S09A".equals(objRek.getAccount_class().toUpperCase().trim()) || "S09B".equals(objRek.getAccount_class().toUpperCase())){ 
//	            	rr = "1";
//	            }
//			}
//			
//			if ("1".equals(rr)){ 
//            	System.out.println("error 2");
//            	request.setAttribute("confrm","err");
//        		request.setAttribute("respon", "Restricted Account");
//            } else {
//            
//            }
			
			int noOfRecords = 0; 
			int sawal = 1;
			int sakhir = 10; 
			int page = 1; 
			int noOfPages = 0; 
			int recordsPerPage = 1000; 
			
			noOfRecords = sql.getTotSaldoDetail(acc, prd1, prd2);
			  if (noOfRecords > 0) {
				  noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
				  if (noOfPages < sakhir) {
					  sakhir = noOfPages;
				  }
				  lphSaldoDetail = sql.getSaldoDetail(acc, prd1, prd2, sawal - 1);
				  request.setAttribute("sawalSaldoDetail", sawal);
				  request.setAttribute("sakhirSaldoDetail", sakhir);
				  request.setAttribute("lphSaldoDetail", lphSaldoDetail); 
				  request.setAttribute("noOfRecordsSaldoDetail", noOfRecords);
				  request.setAttribute("noOfPagesSaldoDetail", noOfPages);
				  request.setAttribute("currentPageSaldoDetail", page);
				  request.getSession(true).setAttribute("PageSaldoDetail", page);
				  request.getSession(true).setAttribute("llsp", lphSaldoDetail); 
				  request.getSession(true).setAttribute("recordsSaldoDetail", noOfRecords);
				  request.getSession(true).setAttribute("OfPagesSaldoDetail", noOfPages);
				  request.getSession(true).setAttribute("acc", acc); 
				  request.getSession(true).setAttribute("prd1", prd1); 
				  request.getSession(true).setAttribute("prd2", prd2);
			  }
			  return  mapping.findForward(SUCCESS); 
			
//			List lphSaldoDetail = new ArrayList();
//			SqlFunction sql = new SqlFunction();
//			
//			String acc = (String) request.getSession(true).getAttribute("acc");
//			String prd1 = (String) request.getParameter("prd1");
//			String prd2 = (String) request.getParameter("prd2");
//			
//			
//			lphSaldoDetail = sql.getSaldoDetail(acc, prd1, prd2);
//			if(lphSaldoDetail.size() > 0) {
//				request.setAttribute("lphSaldoDetail", lphSaldoDetail);
//				request.setAttribute("acc", acc);
//			} else {
//				request.setAttribute("dataresponse", "Tidak ditemukan transaksi hari ini");
//	              request.setAttribute("konfirmasi", "err");
//			}
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
	           return mapping.findForward(".errPage");
		}
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null){
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "peragaanSaldo.do")){
			List lphSaldo = (List) request.getSession(true).getAttribute("lphSaldo");
			request.setAttribute("lphSaldo", lphSaldo);
			return "success";
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            //request.setAttribute("error_code", "Error_timeout");
            return "msgpage";
		}
	}
}
