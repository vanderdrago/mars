package com.muamalat.reportmcb.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class LapPerubahanDataAct extends org.apache.struts.action.Action {
	/* forward name="success" path="" */
	private static Logger log = Logger.getLogger(LapPerubahanDataAct.class);
	private static String SUCCESS = "success";

	/** Action called on save button click
     */
	
	
	  public ActionForward execute(ActionMapping mapping, ActionForm form,
	  HttpServletRequest request, HttpServletResponse response) throws
	  Exception {
	  
	  SUCCESS = isValidUser(request);
	  
	  Admin user = (Admin)
	  request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
	  
	  if (!SUCCESS.equals("success")) { 
		  return mapping.findForward(SUCCESS); 
	  }
	  try{ 
		  List lphRek = new ArrayList(); 
		  SqlFunction sql = new SqlFunction();
		  String branch = request.getParameter("branch");
		  String acc = request.getParameter("acc");
//		  System.out.println("tgl " + tgl);
		  int noOfRecords = 0;
		  int sawal = 1;
		  int sakhir = 10; 
		  int page = 1; 
		  int noOfPages = 0; 
		  int recordsPerPage = 1000;
		  
		  noOfRecords = sql.getTotLapPerubahanNorek(branch,acc);
		  if (noOfRecords > 0) {
			  noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
			  if (noOfPages < sakhir) {
				  sakhir = noOfPages;
			  }
			  lphRek = sql.getLapPerubahanRek(branch,acc, sawal - 1, recordsPerPage);
			  request.setAttribute("sawalNorek", sawal); 
			  request.setAttribute("sakhirNorek", sakhir); 
			  request.setAttribute("lphRek", lphRek); 
			  request.setAttribute("noOfRecordsNorek", noOfRecords);
			  request.setAttribute("noOfPagesNorek", noOfPages); 
			  request.setAttribute("currentPageNorek", page); 
			  request.getSession(true).setAttribute("pageNorek", page); 
			  request.getSession(true).setAttribute("llsp", lphRek); 
			  request.getSession(true).setAttribute("recordsNorek", noOfRecords);
			  request.getSession(true).setAttribute("noOfPagesNorek", noOfPages);
			  request.getSession(true).setAttribute("branch", "branch"); 
			  request.getSession(true).setAttribute("acc", acc);
		  }
//		  List lphRek = sql.getLapPerubahanRek(norek);
		  
//		  if (lphRek.size() > 0) {
//			  request.setAttribute("lphRek", lphRek);
//			  request.getSession(true).setAttribute("llsp", lphRek);
//		  } else {
//			  request.setAttribute("dataresponse", "No Account tidak ditemukan");
//              request.setAttribute("konfirmasi", "err");
//		  }
//		  System.out.println("masuk action");
//		  List lphTes = new ArrayList(); 
//		  SqlFunction sql = new SqlFunction();
//		  String acc = request.getParameter("acc");
//		  System.out.println("acc action" + acc);
//		  lphTes = sql.getLapTes(acc);
//
//		  request.setAttribute("lphTes", lphTes); 
////		  request.setAttribute("viewlph", "vph");
//		  request.getSession(true).setAttribute("lphTes", lphTes); 
	   return mapping.findForward(SUCCESS); 
	   } catch (Exception e) {
		  request.setAttribute("errpage", "Error : " + e.getMessage()); 
		  return mapping.findForward("failed"); 
	   }
}
	  
private String isValidUser(HttpServletRequest request) {
	int level; int validLevel = 5; 
	if(request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) { 
	  request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel); 
	  request.setAttribute("message", "Login Session Expired, silahkan login kembali!"); 
	  return "loginpage"; 
	}
  
	level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString()); 
	if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) { 
	  return "success";
	} else { 
		request.setAttribute("message", "Unauthorized Access!");
		request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta!");
		request.setAttribute("error_code", "Error_timeout");
		return "msgpage";
	}
 }
}
