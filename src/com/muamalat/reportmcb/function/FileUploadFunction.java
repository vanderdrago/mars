package com.muamalat.reportmcb.function;

import java.io.InputStream;

public class FileUploadFunction {
	private String fileName;
	private InputStream in;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public InputStream getIn() {
		return in;
	}
	public void setIn(InputStream in) {
		this.in = in;
	}
	
	
}
