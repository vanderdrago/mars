/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.muamalat.reportmcb.entity.Gl;
import com.muamalat.reportmcb.entity.PctbContractMaster;
import com.muamalat.reportmcb.entity.TRTGSmcbin;
import com.muamalat.reportmcb.entity.TRskn;
import com.muamalat.reportmcb.entity.TRsknin;
import com.muamalat.singleton.DatasourceEntry;

/**
 *
 * @author Utis
 */
public class MonSqlFunctionSkn {

    private static Logger log = Logger.getLogger(MonSqlFunctionSkn.class);
    
    public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                log.error("closeConnDb 1 : " + ex.getMessage());
            }
        }
        if (rs != null) {
            try {
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 2 : " + ex.getMessage());
            }
        }
        if (stat != null) {
            try {
                stat.close();
                stat = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 3 : " + ex.getMessage());
            }
        }
    }

    
     public List gettrxsknList(String argdt) {
        List ltrn = new ArrayList();
        TRskn trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        String stabels = "bmi_skn_sect"+argdt+".dbo.outcreditnotes";
        
        try {
            conn = DatasourceEntry.getInstance().getSknDS().getConnection();
            
   		 	sql = "SELECT od_ref_no,  od_bankno, od_op_name, od_accno, od_cu_name, od_desc, od_amount, " + 
            	  "od_status1, od_status2 FROM "+ stabels;
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
            	trn = new TRskn();
                
            	trn.setOd_ref_no(rs.getString("od_ref_no"));
            	trn.setOd_bankno(rs.getString("od_bankno"));
            	trn.setOd_op_name(rs.getString("od_op_name"));
            	trn.setOd_accno(rs.getString("od_accno"));
            	trn.setOd_cu_name(rs.getString("od_cu_name"));
            	trn.setOd_desc(rs.getString("od_desc"));
            	trn.setOd_amount(new BigDecimal(rs.getString("od_amount")));
            	trn.setOd_status1(rs.getString("od_status1"));
            	trn.setOd_status2(rs.getString("od_status2"));
                
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListSknFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
     public List searchtrxsknList(String argRelTrn, String argdt) {
         
     	List ltrn = new ArrayList();
         
     	 TRskn trn = null;
         Connection conn = null;
         PreparedStatement stat = null;
         
         ResultSet rs = null;
        
         String stabels = "bmi_skn_sect"+argdt+".dbo.outcreditnotes";
         
         String sql = "SELECT od_ref_no, od_bankno, od_op_name, od_accno, od_cu_name, od_desc, od_amount, " + 
			          "od_status1, od_status2 FROM "+stabels+ " "+argRelTrn+"";
         try {
             conn = DatasourceEntry.getInstance().getSknDS().getConnection();
             stat = conn.prepareStatement(sql);
             rs = stat.executeQuery();
             while (rs.next()) {
            	 trn = new TRskn();
                 
            	 trn.setOd_ref_no(rs.getString("Od_ref_no"));
            	 trn.setOd_bankno(rs.getString("Od_bankno"));
            	 trn.setOd_op_name(rs.getString("Od_op_name"));
            	 trn.setOd_accno(rs.getString("Od_accno"));
            	 trn.setOd_cu_name(rs.getString("Od_cu_name"));
            	 trn.setOd_desc(rs.getString("Od_desc"));
            	 trn.setOd_amount(new BigDecimal(rs.getString("Od_amount")));
            	 trn.setOd_status1(rs.getString("Od_status1"));
            	 trn.setOd_status2(rs.getString("Od_status2"));
                 
                 ltrn.add(trn);
             }
         } catch (SQLException ex) {
             log.error(" getListSKNFunct2 : " + ex.getMessage());
         } finally {
             closeConnDb(conn, stat, rs);
         }
         return ltrn;
     }

     public TRskn searchjoinskn(String argRelTrn, String argdt) {
         
    	 TRskn trn = null;
         Connection conn = null;
         PreparedStatement stat = null;
         
         ResultSet rs = null;
     	 
		 String stabels = "bmi_skn_sect"+argdt+".dbo.outcreditnotes";
		 
         String sql ="SELECT od_ref_no, od_bankno, od_op_name, od_accno, od_cu_name, od_desc, od_amount, " + 
         			 "od_status1, od_status2 FROM "+stabels+ " "+argRelTrn+"";
         try {
             conn = DatasourceEntry.getInstance().getSknDS().getConnection();
             stat = conn.prepareStatement(sql);
             rs = stat.executeQuery();
             if (rs.next()) {
            	 trn = new TRskn();
                 
            	 trn.setOd_ref_no(rs.getString("Od_ref_no"));
            	 trn.setOd_bankno(rs.getString("Od_bankno"));
            	 trn.setOd_op_name(rs.getString("Od_op_name"));
            	 trn.setOd_accno(rs.getString("Od_accno"));
            	 trn.setOd_cu_name(rs.getString("Od_cu_name"));
            	 trn.setOd_desc(rs.getString("Od_desc"));
            	 trn.setOd_amount(new BigDecimal(rs.getString("Od_amount")));
            	 trn.setOd_status1(rs.getString("Od_status1"));
            	 trn.setOd_status2(rs.getString("Od_status2"));
                 
             }
         } catch (SQLException ex) {
             log.error(" getListSKNFunct2 : " + ex.getMessage());
         } finally {
             closeConnDb(conn, stat, rs);
         }
         return trn;
     }
     
     public List getListMCBMon(String network, String dt) {
         List ltrn = new ArrayList();
         PctbContractMaster trn = null;
         Connection conn = null;
         PreparedStatement stat = null;
         ResultSet rs = null;
         String sql = "";

         try {
             conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
             
             sql = " select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue," +
             		"product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, txn_amount," +
             		"udf_2, dispatch_ref_no, remarks, cust_name " +
             		"from PCTBS_CONTRACT_MASTER " +
             		"where activation_dt = TO_DATE(?, 'DD-MM-YYYY') and network = ? ";
             stat = conn.prepareStatement(sql);
             stat.setString(1, dt);
             stat.setString(2, network);
             rs = stat.executeQuery();
             
             while (rs.next()) {
             	trn = new PctbContractMaster();
             	trn.setBookingDt(rs.getDate("booking_dt"));
             	trn.setContractRefNo(rs.getString("contract_ref_no"));
             	trn.setBranchOfInput(rs.getString("branch_of_input"));
             	trn.setMakerId(rs.getString("maker_id"));
             	trn.setCheckerId(rs.getString("checker_id"));
             	trn.setNetwork(rs.getString("network"));
             	trn.setProductCode(rs.getString("product_code"));
             	trn.setCptyBankcode(rs.getString("cpty_bankcode"));
             	trn.setCptyAcNo(rs.getString("cpty_ac_no"));
             	trn.setCptyName(rs.getString("cpty_name"));
             	trn.setCustAcNo(rs.getString("cust_ac_no"));
             	trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
             	trn.setExceptionQueue(rs.getString("exception_queue"));
         
                ltrn.add(trn);
             }
         } catch (SQLException ex) {
             log.error(" get getListMCBMon : " + ex.getMessage());
         } finally {
             closeConnDb(conn, stat, rs);
         }
         return ltrn;
     }
     
     public List searchMonSknMcb(String dt, String network, String rel_trn,  String rek_f_ac, String rek_t_ac, String amont_a, String amont_b) {
    	 List ltrn = new ArrayList();
         PctbContractMaster trn = null;
         Connection conn = null;
         PreparedStatement stat = null;
         ResultSet rs = null;
         String sql = "";

         try {
        	 if ((rel_trn==null ) && (rek_f_ac==null ) && (rek_t_ac==null ) && (amont_a==null ) && (amont_b==null )){
        		 return ltrn;
        	 }
        			 
             conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
             int i = 1;
             
             sql = "select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue," +
             		"product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, cust_name, txn_amount, prod_ref_no," +
             		"udf_2, dispatch_ref_no, remarks, cust_name " +
             		"from PCTBS_CONTRACT_MASTER where activation_dt = TO_DATE(?, 'DD-MM-YYYY') ";
             if(network!=null && !"".equals(network)){
            	 sql += " and network = ? ";
             }
             if(rel_trn!=null && !"".equals(rel_trn)){
            	 sql += " and contract_ref_no = ? ";
             }
             if(rek_f_ac!=null && !"".equals(rek_f_ac) ){
            	 sql += " and cust_ac_no = ? ";
             }
             if(rek_t_ac!=null && !"".equals(rek_t_ac)){
            	 sql += " and cpty_ac_no = ? ";
             }
             if(amont_a!=null && !"".equals(amont_a)){
            	 sql += " and txn_amount >= ? ";
             }
             if(amont_b!=null && !"".equals(amont_b)){
            	 sql += " and txn_amount <= ? ";
             }
             stat = conn.prepareStatement(sql);
             stat.setString(i++, dt);
             if(network!=null && !"".equals(network)){
                 stat.setString(i++, network);
             }
             if(rel_trn!=null && !"".equals(rel_trn)){
            	 sql += " and contract_ref_no = ? ";
                 stat.setString(i++,rel_trn);
             }
             if(rek_f_ac!=null && !"".equals(rek_f_ac)){
            	 sql += " and cust_ac_no = ? ";
                 stat.setString(i++,rek_f_ac);
             }
             if(rek_t_ac!=null && !"".equals(rek_t_ac)){
            	 sql += " and cpty_ac_no = ? ";
                 stat.setString(i++,rek_t_ac);
             }
             if(amont_a!=null && !"".equals(amont_a)){
            	 sql += " and txn_amount >= ? ";
                 stat.setString(i++,amont_a);
             }
             if(amont_b!=null && !"".equals(amont_b)){
            	 sql += " and txn_amount <= ? ";
                 stat.setString(i++,amont_b);
             }
             rs = stat.executeQuery();
             while (rs.next()) {
             	trn = new PctbContractMaster();
             	trn.setBookingDt(rs.getDate("booking_dt"));
             	trn.setContractRefNo(rs.getString("contract_ref_no"));
             	trn.setBranchOfInput(rs.getString("branch_of_input"));
             	trn.setMakerId(rs.getString("maker_id"));
             	trn.setCheckerId(rs.getString("checker_id"));
             	trn.setNetwork(rs.getString("network"));
             	trn.setUdf2(rs.getString("udf_2"));
             	trn.setProductCode(rs.getString("product_code"));
             	trn.setCptyBankcode(rs.getString("cpty_bankcode"));
             	trn.setCptyAcNo(rs.getString("cpty_ac_no"));
             	trn.setCptyName(rs.getString("cpty_name"));
             	trn.setCustAcNo(rs.getString("cust_ac_no"));
             	trn.setCustName(rs.getString("cust_name"));
             	trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
             	trn.setDispatchRefNo(rs.getString("dispatch_ref_no"));
             	trn.setExceptionQueue(rs.getString("exception_queue"));
         
                ltrn.add(trn);
             }
         } catch (SQLException ex) {
             log.error(" get getListSKNMon : " + ex.getMessage());
         } finally {
             closeConnDb(conn, stat, rs);
         }
         return ltrn;
    }
     
     public List getskninList(String sDate) {
         List ltrn = new ArrayList();
         TRsknin trn = null;
         Connection conn = null;
         PreparedStatement stat = null;
         ResultSet rs = null;
         String sql = "";

         String stabels = "bmi_skn_smiw"+sDate+".dbo.increditnotes";
         
         try {
             conn = DatasourceEntry.getInstance().getSknDS().getConnection();
             
    		 sql = "SELECT id_upload1_time, id_ref_no, id_sender_name, id_benef_account, id_benef_name, id_benef_account_name, " +
    		 	   "id_remark, id_amount, id_sender_region_code, id_benef_region_code, id_sender_clearing_code, " +
    		 	   "id_sor, id_br_code, id_reject_desc, id_status_host, id_verify_user, id_auth_user " +
    		 	   "from "+ stabels;
             stat = conn.prepareStatement(sql);
             rs = stat.executeQuery();
             while (rs.next()) {
             	trn = new TRsknin();
             	trn.setId_upload1_time(rs.getString("id_upload1_time"));
                trn.setId_ref_no(rs.getString("id_ref_no"));
             	trn.setId_sender_name(rs.getString("id_sender_name"));
             	trn.setId_benef_account(rs.getString("id_benef_account"));
             	trn.setId_benef_name(rs.getString("id_benef_name"));
             	trn.setId_benef_account_name(rs.getString("id_benef_account_name"));
             	trn.setId_remark(rs.getString("id_remark"));
             	trn.setId_amount(rs.getString("id_amount"));
             	trn.setId_sender_region_code(rs.getString("id_sender_region_code"));
             	trn.setId_benef_region_code(rs.getString("id_benef_region_code"));
             	trn.setId_sender_clearing_code(rs.getString("id_sender_clearing_code"));
             	trn.setId_sor(rs.getString("id_sor"));
             	trn.setId_br_code(rs.getString("id_br_code"));
             	trn.setId_reject_desc(rs.getString("id_reject_desc"));
             	trn.setId_status_host(rs.getString("id_status_host"));
             	trn.setId_verify_user(rs.getString("id_verify_user"));
             	trn.setId_auth_user(rs.getString("id_auth_user"));
                 
                ltrn.add(trn);
             }
         } catch (SQLException ex) {
             log.error(" getListSknFunc : " + ex.getMessage());
         } finally {
             closeConnDb(conn, stat, rs);
         }
         return ltrn;
     }
     
      public List searchskninList(String sDate, String sDate1, String refno, String benacc, String amont_a, String amont_b) {
          
	  List ltrn = new ArrayList();
      TRsknin trn = null;
      Connection conn = null;
      PreparedStatement stat = null;
      ResultSet rs = null;
      String sql = "";
     
      try {
        	 if ((refno==null ) && (benacc==null ) && (amont_a==null )&& (amont_b==null )){
        		 return ltrn;
        	 }
        			 
             conn = DatasourceEntry.getInstance().getSknDS().getConnection();  
             int i = 1;
      
             String stabels = "bmi_skn_smiw"+sDate+".dbo.increditnotes";
          
             sql = "SELECT id_upload1_time, id_ref_no, id_sender_name, id_benef_account, id_benef_name, id_benef_account_name, " +
	 	   	  		"id_remark, id_amount, id_sender_region_code, id_benef_region_code, id_sender_clearing_code, " +
	 	   	  		"id_sor, id_br_code, id_reject_desc, id_status_host, id_verify_user, id_auth_user " +
	 	   	  		"from "+stabels + " WHERE  CONVERT(VARCHAR(10), id_upload1_time, 103)= '"+sDate1 +"' ";
             if(refno!=null && !"".equals(refno)){
               	 sql += " and id_ref_no = ? ";
             }
             if(benacc!=null && !"".equals(benacc) ){
           	   sql += " and id_benef_account = ? ";
             }
             if(amont_a!=null && !"".equals(amont_a)){
           	   sql += " and (CAST(id_amount AS NUMERIC(20, 2))) >= ? ";
             }
             if(amont_b!=null && !"".equals(amont_b)){
           	   sql += " and (CAST(id_amount AS NUMERIC(20, 2))) <= ? ";
            }
         
             stat = conn.prepareStatement(sql);
             if(refno!=null && !"".equals(refno)){
                 stat.setString(i++,refno);
             }
             if(benacc!=null && !"".equals(benacc)){
                 stat.setString(i++,benacc);
             }
             if(amont_a!=null && !"".equals(amont_a)){
                 stat.setString(i++,amont_a);
             }
             if(amont_b!=null && !"".equals(amont_b)){
                 stat.setString(i++,amont_b);
             }
             rs = stat.executeQuery();
             while (rs.next()) {
			 trn = new TRsknin();
			 trn.setId_upload1_time(rs.getString("id_upload1_time"));
             trn.setId_ref_no(rs.getString("id_ref_no"));
          	 trn.setId_sender_name(rs.getString("id_sender_name"));
          	 trn.setId_benef_account(rs.getString("id_benef_account"));
          	 trn.setId_benef_name(rs.getString("id_benef_name"));
          	 trn.setId_benef_account_name(rs.getString("id_benef_account_name"));
          	 trn.setId_remark(rs.getString("id_remark"));
          	 trn.setId_amount(rs.getString("id_amount"));
          	 trn.setId_sender_region_code(rs.getString("id_sender_region_code"));
          	 trn.setId_benef_region_code(rs.getString("id_benef_region_code"));
          	 trn.setId_sender_clearing_code(rs.getString("id_sender_clearing_code"));
          	 trn.setId_sor(rs.getString("id_sor"));
          	 trn.setId_br_code(rs.getString("id_br_code"));
          	 trn.setId_reject_desc(rs.getString("id_reject_desc"));
          	 trn.setId_status_host(rs.getString("id_status_host"));
          	 trn.setId_verify_user(rs.getString("id_verify_user"));
          	 trn.setId_auth_user(rs.getString("id_auth_user"));
		   
          	 ltrn.add(trn);
			}
		} catch (SQLException ex) {
			log.error(" getListSKNFunct2 : " + ex.getMessage());
		} finally {
		closeConnDb(conn, stat, rs);
		}
		return ltrn;
	}
    
      public List getsknmcbin2(String sDate, String rel_trn, String rek_t_ac, String amont_a, String amont_b) {
          List ltrn = new ArrayList();
          TRTGSmcbin trn = null;
          Connection conn = null;
          PreparedStatement stat = null;
          ResultSet rs = null;
          String sql = "";
          
          try {
              conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
              sql =  "select a.trn_dt, a.trn_ref_no, a.event, a.module, a.ac_branch, a.trn_code, a.batch_no, " +
              	   "a.user_id, a.auth_id, a.auth_stat, a.ac_no, b.ac_desc, a.drcr_ind, a.lcy_amount " +
              	   "from actb_daily_log a, sttm_cust_account b " +
              	   "where a.ac_no=b.cust_ac_no(+) " +
              	   "and a.ac_branch = b.branch_code(+) " +
              	   "and a.trn_code in ('410', '4D0', '4E0') " +
              	   "and a.user_id = 'FLEXGW' and (length(a.ac_no) = 10 or a.ac_no = '205005005') " +
              	   "and a.trn_dt =TO_DATE('"+sDate+"', 'DD-MM-YYYY')";
              stat = conn.prepareStatement(sql);
              rs = stat.executeQuery();
              
              while (rs.next()) {
                trn = new TRTGSmcbin();
                trn.setTrn_dt(rs.getString("trn_dt").substring(0, 10));
              	trn.setTrn_ref_no(rs.getString("trn_ref_no"));
              	trn.setEvent(rs.getString("event"));
              	trn.setModule(rs.getString("module"));
              	trn.setAc_branch(rs.getString("ac_branch"));
              	trn.setTrn_code(rs.getString("trn_code"));
              	trn.setBatch_no(rs.getString("batch_no"));
              	trn.setUser_id(rs.getString("user_id"));
              	trn.setAuth_id(rs.getString("auth_id"));
              	trn.setAuth_stat(rs.getString("auth_stat"));
              	trn.setAc_no(rs.getString("ac_no"));
              	trn.setAc_desc(rs.getString("ac_desc"));
              	trn.setDrcr_ind(rs.getString("drcr_ind"));
              	trn.setLcy_amount(rs.getBigDecimal("lcy_amount"));
              	
                  ltrn.add(trn);
              }
          } catch (SQLException ex) {
          	  log.error(" getListRTGSFunc : " + ex.getMessage());
          } finally {
              closeConnDb(conn, stat, rs);
          }
          return ltrn;
      }
  	
  	public List sMCBSknIn(String sDate, String rel_trn, String rek_t_ac, String amont_a, String amont_b) {
 	 	  List ltrn = new ArrayList();
 	 	  TRTGSmcbin trn = null;
          Connection conn = null;
          PreparedStatement stat = null;
          ResultSet rs = null;
          String sql = "";
          SqlFunction f = new SqlFunction();
          try {
         	 if ((rel_trn==null) && (rek_t_ac==null) && (amont_a==null) && (amont_b==null)){
         		 return ltrn;
         	 }
         			 
              conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
              int i = 1;
              Gl g = f.getGl("205005005");
              sql = "select a.trn_dt, a.trn_ref_no, a.event, a.module, a.ac_branch, a.trn_code, a.batch_no, " +
       	   		  	"a.user_id, a.auth_id, a.auth_stat, a.ac_no, b.ac_desc, a.drcr_ind, a.lcy_amount " +
       	   		  	"from actb_daily_log a, sttm_cust_account b " +
       	   		  	"where a.ac_no=b.cust_ac_no(+) " +
       	   		  	"and a.ac_branch = b.branch_code(+) " +
       	   		  	"and a.trn_code in ('410', '4D0', '4E0') " +
       	   		  	"and a.user_id = 'FLEXGW' and (length(a.ac_no) = 10 or a.ac_no = '205005005') " +
       	   		  	"and a.trn_dt = TO_DATE('"+sDate+"', 'DD-MM-YYYY') ";
                   
              if(rel_trn!=null && !"".equals(rel_trn)){
             	 sql += " and a.trn_ref_no = ? ";
              }
              if(rek_t_ac!=null && !"".equals(rek_t_ac) ){
             	 sql += " and a.ac_no = ? ";
              }
              if(amont_a!=null && !"".equals(amont_a)){
             	 sql += " and a.lcy_amount >= ? ";
              }
              if(amont_b!=null && !"".equals(amont_b)){
             	 sql += " and a.lcy_amount <= ? ";
              }

              stat = conn.prepareStatement(sql);
              if(rel_trn!=null && !"".equals(rel_trn)){
                  stat.setString(i++,rel_trn);
              }
              if(rek_t_ac!=null && !"".equals(rek_t_ac)){
                  stat.setString(i++,rek_t_ac);
              }
              if(amont_a!=null && !"".equals(amont_a)){
                  stat.setString(i++,amont_a);
              }
              if(amont_b!=null && !"".equals(amont_b)){
                  stat.setString(i++,amont_b);
              }

              rs = stat.executeQuery();
              while (rs.next()) {
  	           	trn = new TRTGSmcbin();
  	           	trn.setTrn_dt(rs.getString("trn_dt").substring(0, 10));
              	trn.setTrn_ref_no(rs.getString("trn_ref_no"));
              	trn.setEvent(rs.getString("event"));
              	trn.setModule(rs.getString("module"));
              	trn.setAc_branch(rs.getString("ac_branch"));
              	trn.setTrn_code(rs.getString("trn_code"));
              	trn.setBatch_no(rs.getString("batch_no"));
              	trn.setUser_id(rs.getString("user_id"));
              	trn.setAuth_id(rs.getString("auth_id"));
              	trn.setAuth_stat(rs.getString("auth_stat"));
              	if("205005005".equals(rs.getString("ac_no"))){
                  	trn.setAc_no("205005005");     
                  	trn.setAc_desc(g.getGl_desc());       		
              	} else {
                  	trn.setAc_no(rs.getString("ac_no"));     
                  	trn.setAc_desc(rs.getString("ac_desc"));       		            		
              	}
              	trn.setDrcr_ind(rs.getString("drcr_ind"));
              	trn.setLcy_amount(rs.getBigDecimal("lcy_amount"));
              	
                 ltrn.add(trn);
              }
          } catch (SQLException ex) {
         	 	//System.out.println("err : " +  ex.getMessage());
              log.error(" get getListRTGSin : " + ex.getMessage());
          } finally {
              closeConnDb(conn, stat, rs);
          }
          return ltrn;
     }
  	
  	public TRTGSmcbin getsknjmcbin(String argRelTrn) {
        List ltrn = new ArrayList();
        TRTGSmcbin trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql =  "select a.trn_dt, a.trn_ref_no, a.event, a.module, a.ac_branch, a.trn_code, a.batch_no, " +
            	   "a.user_id, a.auth_id, a.auth_stat, a.ac_no, b.ac_desc, a.drcr_ind, a.lcy_amount " +
            	   "from actb_daily_log a, sttm_cust_account b " +
            	   "where a.ac_no=b.cust_ac_no(+) " +
            	   "and a.ac_branch = b.branch_code(+) " +
            	   "and a.trn_code in ('410', '4D0', '4E0') " +
            	   "and a.user_id = 'FLEXGW' and (length(a.ac_no) = 10 or a.ac_no = '205005005') "+argRelTrn+"";
            	   //"and a.trn_dt =TO_DATE('"+sDate+"', 'DD-MM-YYYY')";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new TRTGSmcbin();
                trn.setTrn_dt(rs.getString("trn_dt").substring(0, 10));
            	trn.setTrn_ref_no(rs.getString("trn_ref_no"));
            	trn.setEvent(rs.getString("event"));
            	trn.setModule(rs.getString("module"));
            	trn.setAc_branch(rs.getString("ac_branch"));
            	trn.setTrn_code(rs.getString("trn_code"));
            	trn.setBatch_no(rs.getString("batch_no"));
            	trn.setUser_id(rs.getString("user_id"));
            	trn.setAuth_id(rs.getString("auth_id"));
            	trn.setAuth_stat(rs.getString("auth_stat"));
            	trn.setAc_no(rs.getString("ac_no"));
            	trn.setAc_desc(rs.getString("ac_desc"));
            	trn.setDrcr_ind(rs.getString("drcr_ind"));
            	trn.setLcy_amount(rs.getBigDecimal("lcy_amount"));
            	
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return trn;
    }

}
