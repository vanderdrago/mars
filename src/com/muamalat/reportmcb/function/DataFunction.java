package com.muamalat.reportmcb.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.entity.BranchMaster;
import com.muamalat.reportmcb.entity.Member_bank;
import com.muamalat.singleton.DatasourceEntry;

public class DataFunction {
	private Logger log = Logger.getLogger(DataFunction.class);
	
	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public List getMemberBank(String intrf) {
		List lm = new ArrayList();
		Member_bank name = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			if ("SKN".equals(intrf)){
//				sql = "SELECT kode_member, nama_member, codename FROM member_bank order by kode_member asc ";
				sql = "SELECT kode_member, nama_member FROM bank_bi_skn order by kode_member asc ";
			} else if ("RTGS".equals(intrf)){
//				sql = "SELECT kode_member, nama_member, codename FROM member_bank order by codename asc ";
				sql = "SELECT codename, nama_member FROM bank_bi_rtgs order by codename asc ";
			}
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				name = new Member_bank();				
				if ("SKN".equals(intrf)){
					name.setNama_member(rs.getString("nama_member"));
					name.setKode_member(rs.getString("kode_member"));					
				} else if ("RTGS".equals(intrf)){
					name.setCodename(rs.getString("codename"));
					name.setNama_member(rs.getString("nama_member"));					
				}
				lm.add(name);
			}
		} catch (SQLException ex) {	
			log.error("getMemberBank : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lm;
	}
	
	public Member_bank getMemberBankByCdBmi(String cd) {
		Member_bank name = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "SELECT kode_member, nama_member, codename FROM member_bank where kode_member = ? ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, cd);
			rs = stat.executeQuery();
			while (rs.next()) {
				name = new Member_bank();
				name.setKode_member(rs.getString("kode_member"));
				name.setNama_member(rs.getString("nama_member"));
				name.setCodename(rs.getString("codename"));
			}
		} catch (SQLException ex) {	
			log.error("getMemberBankByCdBmi : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return name;
	}
	
	public BranchMaster getBranchMaster(String cd) {
		BranchMaster name = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select branch_code, branch_name from sttm_branch where branch_code = ? ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, cd);
			rs = stat.executeQuery();
			while (rs.next()) {
				name = new BranchMaster();
				name.setBrnch_cd(rs.getString("branch_code"));
				name.setBrnch_name(rs.getString("branch_name"));
			}
		} catch (SQLException ex) {	
			log.error("getBranchMaster : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return name;
	}
	
	public static String converttostr(int n){
		String ret = "";
		if (n < 10){
			ret = "0" + n;
		} else {
			ret = n + "";
		}
		return ret;
	}
}
