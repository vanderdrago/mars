/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.muamalat.reportmcb.entity.Altacct;
import com.muamalat.reportmcb.entity.CbTransfersDom;
import com.muamalat.reportmcb.entity.Gl;
import com.muamalat.reportmcb.entity.IbTransfersDom;
import com.muamalat.reportmcb.entity.Instalment;
import com.muamalat.reportmcb.entity.PctbContractMaster;
import com.muamalat.reportmcb.entity.TRTGSmcbin;
import com.muamalat.reportmcb.entity.TRtiftsin;
import com.muamalat.reportmcb.entity.TRtiftsout;
import com.muamalat.singleton.DatasourceEntry;

/**
 *
 * @author TrioKwek-kwek
 */
public class MonSqlFunction {

    private static Logger log = Logger.getLogger(MonSqlFunction.class);
    private BigDecimal totMutDb;
    private BigDecimal totMutCb;

    public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                log.error("closeConnDb 1 : " + ex.getMessage());
            }
        }
        if (rs != null) {
            try {
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 2 : " + ex.getMessage());
            }
        }
        if (stat != null) {
            try {
                stat.close();
                stat = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 3 : " + ex.getMessage());
            }
        }
    }

    public List gettrxrtgsList(String argdt) {
        List ltrn = new ArrayList();
        TRtiftsout trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        String stabels = ""+argdt+"";
        
        try {
            conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
            sql = "select ValueDate, RelTRN, TRN, ToMember, ToAccName, UltimateBeneAcc," +
                  "UltimateBeneName, FromAccNum, FromAccName, PayDetails, BOR, Status" +
                  " from dbo.T_RTIFTSOUT order by DataID";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new TRtiftsout();
                
                trn.setValueDate(rs.getString("ValueDate"));
                trn.setRelTRN(rs.getString("relTRN"));
                trn.setTrn(rs.getString("TRN"));
                trn.setToMember(rs.getString("ToMember"));
                trn.setToAccName(rs.getString("ToAccName")); 
                trn.setUltimateBeneAcc(rs.getString("UltimateBeneAcc"));
                trn.setUltimateBeneName(rs.getString("UltimateBeneName"));
                trn.setFromAccNum(rs.getString("FromAccNum"));
                trn.setFromAccName(rs.getString("FromAccName"));
                trn.setPayDetails(rs.getString("PayDetails"));
                trn.setBor(rs.getString("BOR"));
                trn.setStatus(rs.getString("Status"));
                
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public List searchtrxrtgsList(String argRelTrn) {
        
    	List ltrn = new ArrayList();
        TRtiftsout trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        
        ResultSet rs = null;
       
        String sql = "select ValueDate, RelTRN, TRN, ToMember, ToAccName, UltimateBeneAcc, " +
        			 "UltimateBeneName, FromAccNum, FromAccName, (CAST(amount AS NUMERIC(20, 2)) / 100 ) as Nominal, " +
        			 "PayDetails, BOR, Status" +
        			 " from T_RTIFTSOUT "+argRelTrn+" order by DataID";
        try {
            conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            while (rs.next()) {
                trn = new TRtiftsout();
                
                trn.setValueDate(rs.getString("ValueDate"));
                trn.setRelTRN(rs.getString("relTRN"));
                trn.setTrn(rs.getString("TRN"));
                trn.setToMember(rs.getString("ToMember"));
                trn.setToAccName(rs.getString("ToAccName")); 
                trn.setUltimateBeneAcc(rs.getString("UltimateBeneAcc"));
                trn.setUltimateBeneName(rs.getString("UltimateBeneName"));
                trn.setFromAccNum(rs.getString("FromAccNum"));
                trn.setFromAccName(rs.getString("FromAccName"));
                trn.setNominal(rs.getBigDecimal("Nominal"));
                trn.setPayDetails(rs.getString("PayDetails"));
                trn.setBor(rs.getString("BOR"));
                trn.setStatus(rs.getString("Status"));
                
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunct2 : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }

    public List gettrxrtgsList2(String inputtrn, String stsrtgs, String amont_a, String amont_b) {
        List ltrn = new ArrayList();
        TRtiftsout trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
            sql = "select ValueDate, RelTRN, TRN, ToMember, ToAccName, UltimateBeneAcc," +
                  "UltimateBeneName, FromAccNum, FromAccName, PayDetails, BOR, Status " +
                  "from dbo.T_RTIFTSOUT order by DataID";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new TRtiftsout();
                trn.setValueDate(rs.getString("ValueDate"));
                trn.setRelTRN(rs.getString("relTRN"));
                trn.setTrn(rs.getString("TRN"));
                trn.setToMember(rs.getString("ToMember"));
                trn.setToAccName(rs.getString("ToAccName")); 
                trn.setUltimateBeneAcc(rs.getString("UltimateBeneAcc"));
                trn.setUltimateBeneName(rs.getString("UltimateBeneName"));
                trn.setFromAccNum(rs.getString("FromAccNum"));
                trn.setFromAccName(rs.getString("FromAccName"));
                trn.setPayDetails(rs.getString("PayDetails"));
                trn.setBor(rs.getString("BOR"));
                trn.setStatus(rs.getString("Status"));
                
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    
public List searchtrxrtgsList2(String sDate, String inputtrn, String stsrtgs,  String amont_a, String amont_b) {
	 List ltrn = new ArrayList();
	 TRtiftsout trn = null;
     Connection conn = null;
     PreparedStatement stat = null;
     ResultSet rs = null;
     String sql = "";
     
     try {
    	 if ((inputtrn==null ) && (stsrtgs==null ) && (amont_a==null )&& (amont_b==null )){
    		 return ltrn;
    	 }
    	 conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();  
         int i = 1;
         sql = "select ValueDate, RelTRN, TRN, ToMember, ToAccName, UltimateBeneAcc, " +
        	   "UltimateBeneName, FromAccNum, FromAccName, (CAST(amount AS NUMERIC(20, 2)) / 100 ) as Nominal, " +
        	   "PayDetails, BOR, Status " +
        	   "from T_RTIFTSOUT " +
         	   "where ValueDate = '"+sDate+"'";
	        if(inputtrn!=null && !"".equals(inputtrn)){
	       	 sql += " and RelTRN = ? ";
	        }
	        if(stsrtgs!=null && !"".equals(stsrtgs) ){
	       	 sql += " and Status = ? ";
	        }
	        if(amont_a!=null && !"".equals(amont_a)){
	       	 sql += " and (CAST(amount AS NUMERIC(20, 2)) / 100) >= ? ";
	        }
	        if(amont_b!=null && !"".equals(amont_b)){
	       	 sql += " and (CAST(amount AS NUMERIC(20, 2)) / 100) <= ? ";
	        }
	        stat = conn.prepareStatement(sql);
	        if(inputtrn!=null && !"".equals(inputtrn)){
	            stat.setString(i++,inputtrn);
	        }
	        if(stsrtgs!=null && !"".equals(stsrtgs)){
	            stat.setString(i++,stsrtgs);
	        }
	        if(amont_a!=null && !"".equals(amont_a)){
	            stat.setString(i++,amont_a);
	        }
	        if(amont_b!=null && !"".equals(amont_b)){
	            stat.setString(i++,amont_b);
	        }
	        rs = stat.executeQuery();
	        while (rs.next()) {
		       	trn = new TRtiftsout();
		       	trn.setValueDate(rs.getString("ValueDate"));
		        trn.setRelTRN(rs.getString("relTRN"));
		        trn.setTrn(rs.getString("TRN"));
		        trn.setToMember(rs.getString("ToMember"));
		        trn.setToAccName(rs.getString("ToAccName")); 
		        trn.setUltimateBeneAcc(rs.getString("UltimateBeneAcc"));
		        trn.setUltimateBeneName(rs.getString("UltimateBeneName"));
		        trn.setFromAccNum(rs.getString("FromAccNum"));
		        trn.setFromAccName(rs.getString("FromAccName"));
		        trn.setNominal(rs.getBigDecimal("Nominal"));
		        trn.setPayDetails(rs.getString("PayDetails"));
		        trn.setBor(rs.getString("BOR"));
		        trn.setStatus(rs.getString("Status"));
		        
		        ltrn.add(trn);
	        }
	    } catch (SQLException ex) {
	   	 	log.error(" get getListMCBMon : " + ex.getMessage());
	    } finally {
	        closeConnDb(conn, stat, rs);
	    }
	    return ltrn;
	}

    public List gettrxibList(String argdt) {
    	List ltrn = new ArrayList();
    	IbTransfersDom trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        String stabels = ""+argdt+"";
        
        try {
            conn = DatasourceEntry.getInstance().getIbDS().getConnection();
            sql = "SELECT y.NAME_E, decode(d.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE, " + 
			 	"d.date_trx, t.REF_ID, t.REF_NO, t.user_id, " + 
			 	"d.ACCOUNT_NO, d.TO_ACCOUNT_NO, d.TO_NAME, d.TO_BANK_NAME, d.AMOUNT, d.CHARGE, " +
			 	"d.RESPONSE_CODE, " +
			 	"(case when d.status = '11' then 'Success' else 'Failed' end) STATUS  " +
			 	"FROM IB_TRANSFERS_DOM d join ib_tasks t on d.REF_ID = t.REF_ID " +
			 	"join ib_task_types y on t.TASK_TYPE = y.TASK_TYPE order by REF_ID" ;
            
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
            	trn = new IbTransfersDom();
                trn.setRefNo(rs.getString("REF_NO"));
                trn.setRefId(rs.getString("REF_ID"));
	           	trn.setTransferType(rs.getString("TRANSFER_TYPE"));
	           	trn.setDateTrx(rs.getDate("DATE_TRX"));
	           	trn.setAccountNo(rs.getString("ACCOUNT_NO"));
	           	trn.setToAccountNo(rs.getString("TO_ACCOUNT_NO"));
	           	trn.setToName(rs.getString("TO_NAME"));
	           	trn.setToBankName(rs.getString("TO_BANK_NAME"));
	           	trn.setAmount(rs.getBigDecimal("AMOUNT"));
	           	trn.setResponseCode(rs.getString("RESPONSE_CODE"));
	           	trn.setStatus(rs.getString("STATUS"));	           	
	           	ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListIbFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public List searchIbRtgsMcb(String queryib) {
    	  
    	List ltrn = new ArrayList();
    	IbTransfersDom trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        
        ResultSet rs = null;
       
        String sql = "SELECT y.NAME_E, decode(d.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE, " + 
        			 "d.date_trx, t.REF_ID, t.REF_NO, t.user_id, " + 
        			 "d.ACCOUNT_NO, d.TO_ACCOUNT_NO, d.TO_NAME, d.TO_BANK_NAME, d.AMOUNT, d.CHARGE, " +
        			 "d.RESPONSE_CODE, " +
        			 "(case when d.status = '11' then 'Success' else 'Failed' end) STATUS  " +
        			 "FROM IB_TRANSFERS_DOM d join ib_tasks t on d.REF_ID = t.REF_ID " +
        			 "join ib_task_types y on t.TASK_TYPE = y.TASK_TYPE "+queryib+" ";
    	try {
    		conn = DatasourceEntry.getInstance().getIbDS().getConnection();
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            while (rs.next()) {
            	trn = new IbTransfersDom();
                trn.setRefNo(rs.getString("REF_NO"));
                trn.setRefId(rs.getString("REF_ID"));
	           	trn.setTransferType(rs.getString("TRANSFER_TYPE"));
	           	trn.setDateTrx(rs.getDate("DATE_TRX"));
	           	trn.setAccountNo(rs.getString("ACCOUNT_NO"));
	           	trn.setToAccountNo(rs.getString("TO_ACCOUNT_NO"));
	           	trn.setToName(rs.getString("TO_NAME"));
	           	trn.setToBankName(rs.getString("TO_BANK_NAME"));
	           	trn.setAmount(rs.getBigDecimal("AMOUNT"));
	           	trn.setResponseCode(rs.getString("RESPONSE_CODE"));
	           	trn.setStatus(rs.getString("STATUS"));	           	
	           	ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	log.error(" getListRTGSFunct2 : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public List searchMonRtgsMcb2(String queryKu) {
      	  
    	List ltrn = new ArrayList();
    	PctbContractMaster trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        
        ResultSet rs = null;
       
        String sql = "select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue," +
					 "product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, cust_name, txn_amount, prod_ref_no," +
					 "udf_2, dispatch_ref_no, remarks, cust_name " +
					 "from PCTBS_CONTRACT_MASTER "+queryKu+" " ;
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            while (rs.next()) {
                trn = new PctbContractMaster();
	           	trn.setBookingDt(rs.getDate("booking_dt"));
	           	trn.setContractRefNo(rs.getString("contract_ref_no"));
	           	trn.setBranchOfInput(rs.getString("branch_of_input"));
	           	trn.setMakerId(rs.getString("maker_id"));
	           	trn.setCheckerId(rs.getString("checker_id"));
	           	trn.setNetwork(rs.getString("network"));
	           	trn.setUdf2(rs.getString("udf_2"));
	           	trn.setProductCode(rs.getString("product_code"));
	           	trn.setCptyBankcode(rs.getString("cpty_bankcode"));
	           	trn.setCptyAcNo(rs.getString("cpty_ac_no"));
	           	trn.setCptyName(rs.getString("cpty_name"));
	           	trn.setCustAcNo(rs.getString("cust_ac_no"));
	           	trn.setCustName(rs.getString("cust_name"));
	           	trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
	           	trn.setDispatchRefNo(rs.getString("dispatch_ref_no"));
	           	trn.setExceptionQueue(rs.getString("exception_queue"));
                
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunct2 : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
        
public TRtiftsout searchjoinrtgs(String argRelTrn) {
        
    	TRtiftsout trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        
        ResultSet rs = null;
       
        String sql = "select ValueDate, RelTRN, TRN, ToMember, ToAccName, UltimateBeneAcc," +
        			 "UltimateBeneName, FromAccNum, FromAccName, PayDetails, BOR, Status" +
        			 " from T_RTIFTSOUT "+argRelTrn+"";
        try {
            conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            if (rs.next()) {
                trn = new TRtiftsout();
                
                trn.setValueDate(rs.getString("ValueDate"));
                trn.setRelTRN(rs.getString("relTRN"));
                trn.setTrn(rs.getString("TRN"));
                trn.setToMember(rs.getString("ToMember"));
                trn.setToAccName(rs.getString("ToAccName")); 
                trn.setUltimateBeneAcc(rs.getString("UltimateBeneAcc"));
                trn.setUltimateBeneName(rs.getString("UltimateBeneName"));
                trn.setFromAccNum(rs.getString("FromAccNum"));
                trn.setFromAccName(rs.getString("FromAccName"));
                trn.setPayDetails(rs.getString("PayDetails"));
                trn.setBor(rs.getString("BOR"));
                trn.setStatus(rs.getString("Status"));
                
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunct2 : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return trn;
    }

public List searchjrtgsib(String argRelTrn) {
	List ltrn = new ArrayList(); 
	IbTransfersDom trn = null;
    Connection conn = null;
    PreparedStatement stat = null;
    
    ResultSet rs = null;
   
    String sql = "SELECT y.NAME_E, decode(d.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE, " + 
	 			 "d.date_trx, t.REF_ID, t.REF_NO, t.user_id, " + 
	 			 "d.ACCOUNT_NO, d.TO_ACCOUNT_NO, d.TO_NAME, d.TO_BANK_NAME, d.AMOUNT, d.CHARGE, " +
	 			 "d.RESPONSE_CODE, " +
	 			 "(case when d.status = '11' then 'Success' else 'Failed' end) STATUS  " +
	 			 "FROM IB_TRANSFERS_DOM d join ib_tasks t on d.REF_ID = t.REF_ID " +
	 			 "join ib_task_types y on t.TASK_TYPE = y.TASK_TYPE "+argRelTrn+"";
    try {
        conn = DatasourceEntry.getInstance().getIbDS().getConnection();
        stat = conn.prepareStatement(sql);
        rs = stat.executeQuery();
        while (rs.next()) {
        	trn = new IbTransfersDom();
            trn.setRefNo(rs.getString("REF_NO"));
            trn.setRefId(rs.getString("REF_ID"));
           	trn.setTransferType(rs.getString("TRANSFER_TYPE"));
           	trn.setDateTrx(rs.getDate("DATE_TRX"));
           	trn.setAccountNo(rs.getString("ACCOUNT_NO"));
           	trn.setToAccountNo(rs.getString("TO_ACCOUNT_NO"));
           	trn.setToName(rs.getString("TO_NAME"));
           	trn.setToBankName(rs.getString("TO_BANK_NAME"));
           	trn.setAmount(rs.getBigDecimal("AMOUNT"));
           	trn.setResponseCode(rs.getString("RESPONSE_CODE"));
           	trn.setStatus(rs.getString("STATUS"));	           	
           	ltrn.add(trn);
        }
    } catch (SQLException ex) {
        log.error(" getListRTGSMCBIBFunct2 : " + ex.getMessage());
    } finally {
        closeConnDb(conn, stat, rs);
    }
    return ltrn;
}
    
    public List getListMCBMon(String network, String sDate) {
        List ltrn = new ArrayList();
        PctbContractMaster trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            
            sql = " select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue," +
            		"product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, txn_amount," +
            		"udf_2, dispatch_ref_no, remarks, cust_name " +
            		"from PCTBS_CONTRACT_MASTER " +
            		"where activation_dt = TO_DATE(?, 'DD-MM-YYYY') and network = ? ";
            		
            stat = conn.prepareStatement(sql);
            stat.setString(1, sDate);
            stat.setString(2, network);
            rs = stat.executeQuery();
            
            while (rs.next()) {
            	trn = new PctbContractMaster();
            	trn.setBookingDt(rs.getDate("booking_dt"));
            	trn.setContractRefNo(rs.getString("contract_ref_no"));
            	trn.setBranchOfInput(rs.getString("branch_of_input"));
            	trn.setMakerId(rs.getString("maker_id"));
            	trn.setCheckerId(rs.getString("checker_id"));
            	trn.setNetwork(rs.getString("network"));
            	trn.setProductCode(rs.getString("product_code"));
            	trn.setCptyBankcode(rs.getString("cpty_bankcode"));
            	trn.setCptyAcNo(rs.getString("cpty_ac_no"));
            	trn.setCptyName(rs.getString("cpty_name"));
            	trn.setCustAcNo(rs.getString("cust_ac_no"));
            	trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
            	trn.setExceptionQueue(rs.getString("exception_queue"));
        
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" get getListMCBMon : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public List searchMonRtgsMcb(String sDate, String network, String rel_trn,  String rek_f_ac, String rek_t_ac, String amont_a, String amont_b) {
    	 List ltrn = new ArrayList();
         PctbContractMaster trn = null;
         Connection conn = null;
         PreparedStatement stat = null;
         ResultSet rs = null;
         String sql = "";

         try {
        	 if ((rel_trn==null ) && (rek_f_ac==null ) && (rek_t_ac==null )&& (amont_a==null )&& (amont_b==null )){
        		 return ltrn;
        	 }
        			 
             conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
             int i = 1;
             sql = "select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue," +
             		"product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, cust_name, txn_amount, prod_ref_no," +
             		"udf_2, dispatch_ref_no, remarks, cust_name " +
             		"from PCTBS_CONTRACT_MASTER " +
             		"where activation_dt = TO_DATE(?, 'DD-MM-YYYY')";
             
             if(network != null && !"".equals(network)){
            	 sql += " and network = ? ";
             } 
             if(rel_trn!=null && !"".equals(rel_trn)){
            	 sql += " and contract_ref_no = ? ";
             }
             if(rek_f_ac!=null && !"".equals(rek_f_ac) ){
            	 sql += " and cust_ac_no = ? ";
             }
             if(rek_t_ac!=null && !"".equals(rek_t_ac)){
            	 sql += " and cpty_ac_no = ? ";
             }
             if(amont_a!=null && !"".equals(amont_a)){
            	 sql += " and txn_amount >= ? ";
             }
             if(amont_b!=null && !"".equals(amont_b)){
            	 sql += " and txn_amount <= ? ";
             }
             stat = conn.prepareStatement(sql);
             stat.setString(i++, sDate);
             if(network!=null && !"".equals(network)){
                 stat.setString(i++, network);
             }
             if(rel_trn!=null && !"".equals(rel_trn)){
                 stat.setString(i++,rel_trn);
             }
             if(rek_f_ac!=null && !"".equals(rek_f_ac)){
                 stat.setString(i++,rek_f_ac);
             }
             if(rek_t_ac!=null && !"".equals(rek_t_ac)){
                 stat.setString(i++,rek_t_ac);
             }
             if(amont_a!=null && !"".equals(amont_a)){
                 stat.setString(i++,amont_a);
             }
             if(amont_b!=null && !"".equals(amont_b)){
                 stat.setString(i++,amont_b);
             }
             rs = stat.executeQuery();
             while (rs.next()) {
            	trn = new PctbContractMaster();
             	trn.setBookingDt(rs.getDate("booking_dt"));
             	trn.setContractRefNo(rs.getString("contract_ref_no"));
             	trn.setBranchOfInput(rs.getString("branch_of_input"));
             	trn.setMakerId(rs.getString("maker_id"));
             	trn.setCheckerId(rs.getString("checker_id"));
             	trn.setNetwork(rs.getString("network"));
             	trn.setUdf2(rs.getString("udf_2"));
             	trn.setProductCode(rs.getString("product_code"));
             	trn.setCptyBankcode(rs.getString("cpty_bankcode"));
             	trn.setCptyAcNo(rs.getString("cpty_ac_no"));
             	trn.setCptyName(rs.getString("cpty_name"));
             	trn.setCustAcNo(rs.getString("cust_ac_no"));
             	trn.setCustName(rs.getString("cust_name"));
             	trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
             	trn.setDispatchRefNo(rs.getString("dispatch_ref_no"));
             	trn.setExceptionQueue(rs.getString("exception_queue"));
         
                ltrn.add(trn);
             }
         } catch (SQLException ex) {
        	 log.error(" get getListMCBMon : " + ex.getMessage());
         } finally {
             closeConnDb(conn, stat, rs);
         }
         return ltrn;
    }

    public List searchMonRtgsCms(String rel_trn, String rek_f_ac, String rek_t_ac, String amont_a, String amont_b, String sts, String dt, String trns_type) { 
    List ltrn = new ArrayList();     
    CbTransfersDom trn = null;       
    Connection conn = null;         
    PreparedStatement stat = null;               
    ResultSet rs = null;               
//		String sql = "SELECT d.ref_id, c.COMPANY_CODE, c.NAME , y.NAME_E, decode(d.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE,"
//				+ "d.date_trx, d.AMOUNT, d.CHARGE, decode(d.status, '3', 'Success', 'Fail') status,"
//				+ "d.ACCOUNT_NO, d.TO_ACCOUNT_NO, d.TO_NAME, d.TO_BANK_NAME FROM CB_TRANSFERS_DOM d join CB_COMPANIES c on d.company_id = c.company_id "
//				+ "join cb_tasks t on d.ref_id = t.ref_id join cb_task_types y on t.TASK_TYPE = y.TASK_TYPE "
//				+ queryKu + " ";
		String query = "select t.date_trx, t.ref_id,  c.COMPANY_CODE, c.NAME , " +
				"decode(td.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE, " +
				"case when t.trx_count_success < t.trx_count_all then 'Failed' " +
				"when t.trx_count_success = t.trx_count_all then 'Success' end txn_status, td.STATUS, " +
				"td.ACCOUNT_NO, td.TO_ACCOUNT_NO, td.TO_NAME, td.TO_BANK_NAME, td.AMOUNT, td.STATUS, td.RESPONSE_CODE " +
				"from cb_tasks t join cb_task_transfers_dom td on t.ref_id = td.ref_id " +
				"join cb_companies c on t.company_id = c.company_id " +
				"where t.task_type = ? and status_tf = ? " +
				"and trunc(t.date_trx) >= TO_DATE(?, 'dd-MM-yyyy') and td.TRANSFER_TYPE = ? " ;
		if ("S".equals(sts)){
			query += "and t.trx_count_success = t.trx_count_all ";
		} else if ("f".equals(sts)){
			query += "and t.trx_count_success < t.trx_count_all ";
		}
		if (!"".equals(rel_trn) && rel_trn != null){
			query += "and d.ref_id = ? ";
		}
		if (!"".equals(rek_f_ac) && rek_f_ac != null){
			query += "and d.account_no = ? ";			
		}
		if (!"".equals(rek_t_ac) && rek_t_ac != null){
			query += "and d.to_account_no = ? ";						
		}
		if (!"".equals(amont_a) && amont_a != null){
			query += "and td.AMOUNT >= to_number(?) ";						
		}
		if (!"".equals(amont_b) && amont_b != null){
			query += "and td.AMOUNT <= to_number(?) ";				
		}
		System.out.println("sql searchMonRtgsCms :" + query);
    try {            
    	int i = 1;
    	conn = DatasourceEntry.getInstance().getCmsDS().getConnection();    
    	stat = conn.prepareStatement(query);    
    	stat.setString(i++, "104");
    	stat.setString(i++, "2");
    	stat.setString(i++, dt);
    	stat.setString(i++, trns_type);
		if (!"".equals(rel_trn) && rel_trn != null){
	    	stat.setString(i++, rel_trn);
		}
		if (!"".equals(rek_f_ac) && rek_f_ac != null){
	    	stat.setString(i++, rek_f_ac);
		}
		if (!"".equals(rek_t_ac) && rek_t_ac != null){
	    	stat.setString(i++, rek_t_ac);					
		}
		if (!"".equals(amont_a) && amont_a != null){
	    	stat.setString(i++, amont_a);					
		}
		if (!"".equals(amont_b) && amont_b != null){
	    	stat.setString(i++, amont_b);				
		}
    	rs = stat.executeQuery();        
    	while (rs.next()) {                                   
    	trn = new CbTransfersDom(); 	 
    	trn.setDateTrx(rs.getDate("Date_Trx")); 	      
    	trn.setRefId(rs.getString("Ref_Id")); 	          
    	trn.setCompanyCode(rs.getString("COMPANY_CODE")); 	   
    	trn.setName(rs.getString("NAME")); 	       
    	trn.setName_e(rs.getString("NAME_E")); 	        
    	trn.setTransferType(rs.getString("TRANSFER_TYPE")); 	 
    	trn.setAmount(rs.getBigDecimal("AMOUNT")); 	          
    	trn.setCharge(rs.getBigDecimal("CHARGE")); 	   
    	trn.setStatus(rs.getString("STATUS")); 	       
    	trn.setAccountNo(rs.getString("ACCOUNT_NO")); 	       
    	trn.setToAccountNo(rs.getString("TO_ACCOUNT_NO")); 	         
    	trn.setToName(rs.getString("TO_NAME")); 	          
    	trn.setToBankName(rs.getString("TO_BANK_NAME")); 	                
    	ltrn.add(trn);        
    	}     
    	} catch (SQLException ex) {     
	log.error(" getListRTGSFunct2 : " + ex.getMessage());       
	} finally {         
		closeConnDb(conn, stat, rs);    
		}       
	return ltrn;   
	}   
	
    public List searchMonSknCms(String queryKu) {     
		List ltrn = new ArrayList();     	
		CbTransfersDom trn = null;     
		Connection conn = null;       
		PreparedStatement stat = null;      
		ResultSet rs = null;               
		String sql = "SELECT d.ref_id, c.COMPANY_CODE, c.NAME , y.NAME_E, decode(d.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE,"
				+ "d.date_trx, d.AMOUNT, d.CHARGE, decode(d.status, '3', 'Success', 'Fail') status,"
				+ "d.ACCOUNT_NO, d.TO_ACCOUNT_NO, d.TO_NAME, d.TO_BANK_NAME FROM CB_TRANSFERS_DOM d join CB_COMPANIES c on d.company_id = c.company_id "
				+ "join cb_tasks t on d.ref_id = t.ref_id join cb_task_types y on t.TASK_TYPE = y.TASK_TYPE "
				+ queryKu + " ";
		try {        
			conn = DatasourceEntry.getInstance().getCmsDS().getConnection();      
			stat = conn.prepareStatement(sql);        
			rs = stat.executeQuery();          
			while (rs.next()) {                  
				trn = new CbTransfersDom(); 	         
				trn.setDateTrx(rs.getDate("Date_Trx")); 	   
				trn.setRefId(rs.getString("Ref_Id")); 	     
				trn.setCompanyCode(rs.getString("COMPANY_CODE"));
				trn.setName(rs.getString("NAME")); 	        
				trn.setName_e(rs.getString("NAME_E")); 	         
				trn.setTransferType(rs.getString("TRANSFER_TYPE")); 	 
				trn.setAmount(rs.getBigDecimal("AMOUNT")); 	         
				trn.setCharge(rs.getBigDecimal("CHARGE")); 	         
				trn.setStatus(rs.getString("STATUS")); 	         
				trn.setAccountNo(rs.getString("ACCOUNT_NO")); 	     
				trn.setToAccountNo(rs.getString("TO_ACCOUNT_NO")); 	
				trn.setToName(rs.getString("TO_NAME")); 	  
				trn.setToBankName(rs.getString("TO_BANK_NAME")); 	  
				ltrn.add(trn);         
				}         
			} catch (SQLException ex) {   
		log.error(" getListRTGSFunct2 : " + ex.getMessage());    
	} finally {           
		closeConnDb(conn, stat, rs);     
		}        
	return ltrn;   
	}                


    
	public List searchMonRtgsCmsJ(String queryKu) { 
	    List ltrn = new ArrayList();     
	    CbTransfersDom trn = null;       
	    Connection conn = null;         
	    PreparedStatement stat = null;               
	    ResultSet rs = null;               
			String sql = "SELECT d.ref_id, d.BANK_REF_NO, c.COMPANY_CODE, c.NAME , y.NAME_E, decode(d.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE,"
					+ "d.date_trx, d.AMOUNT, d.CHARGE, d.RESPONSE_CODE, decode(d.status, '3', 'Success', 'Fail') status,"
					+ "d.ACCOUNT_NO, d.TO_ACCOUNT_NO, d.TO_NAME, d.TO_BANK_NAME FROM CB_TRANSFERS_DOM d join CB_COMPANIES c on d.company_id = c.company_id "
					+ "join cb_tasks t on d.ref_id = t.ref_id join cb_task_types y on t.TASK_TYPE = y.TASK_TYPE "
					+ queryKu + " ";
	    try {            
	    	conn = DatasourceEntry.getInstance().getCmsDS().getConnection();    
	    	stat = conn.prepareStatement(sql);          
	    	rs = stat.executeQuery();        
	    	while (rs.next()) {                                   
	    	trn = new CbTransfersDom(); 	 
	    	trn.setDateTrx(rs.getDate("Date_Trx")); 
	    	trn.setRefId(rs.getString("Ref_Id")); 
	    	trn.setBankRefNo(rs.getString("BANK_REF_NO"));
	    	trn.setCompanyCode(rs.getString("COMPANY_CODE")); 	   
	    	trn.setName(rs.getString("NAME")); 	       
	    	trn.setName_e(rs.getString("NAME_E")); 	        
	    	trn.setTransferType(rs.getString("TRANSFER_TYPE")); 	 
	    	trn.setAmount(rs.getBigDecimal("AMOUNT")); 	          
	    	trn.setCharge(rs.getBigDecimal("CHARGE")); 	   
	    	trn.setStatus(rs.getString("STATUS")); 	       
	    	trn.setAccountNo(rs.getString("ACCOUNT_NO")); 	       
	    	trn.setToAccountNo(rs.getString("TO_ACCOUNT_NO")); 	         
	    	trn.setToName(rs.getString("TO_NAME")); 	          
	    	trn.setToBankName(rs.getString("TO_BANK_NAME")); 
	    	trn.setResponseCode(rs.getString("RESPONSE_CODE"));
	    	ltrn.add(trn);        
	    	}     
	    } catch (SQLException ex) {     
    		log.error(" getListRTGSFunct2 : " + ex.getMessage());       
    	} finally {         
		closeConnDb(conn, stat, rs);    
		}       
	return ltrn;   
	}   
		
	public List searchMonSknCmsJ(String queryKu) {     
		List ltrn = new ArrayList();     	
		CbTransfersDom trn = null;     
		Connection conn = null;       
		PreparedStatement stat = null;      
		ResultSet rs = null;               
		String sql = "SELECT d.ref_id, c.COMPANY_CODE,d.BANK_REF_NO, c.NAME , y.NAME_E, decode(d.TRANSFER_TYPE, 1, 'SKN', 2, 'RTGS', TRANSFER_TYPE) TRANSFER_TYPE,"
				+ "d.date_trx, d.AMOUNT, d.CHARGE, decode(d.status, '3', 'Success', 'Fail') status,"
				+ "d.ACCOUNT_NO, d.TO_ACCOUNT_NO,d.RESPONSE_CODE, d.TO_NAME, d.TO_BANK_NAME FROM CB_TRANSFERS_DOM d join CB_COMPANIES c on d.company_id = c.company_id "
				+ "join cb_tasks t on d.ref_id = t.ref_id join cb_task_types y on t.TASK_TYPE = y.TASK_TYPE "
				+ queryKu + " ";
		try {        
			conn = DatasourceEntry.getInstance().getCmsDS().getConnection();      
			stat = conn.prepareStatement(sql);        
			rs = stat.executeQuery();          
			while (rs.next()) {                  
				trn = new CbTransfersDom(); 	         
				trn.setDateTrx(rs.getDate("Date_Trx"));
				trn.setBankRefNo(rs.getString("BANK_REF_NO"));
				trn.setRefId(rs.getString("Ref_Id")); 	     
				trn.setCompanyCode(rs.getString("COMPANY_CODE"));
				trn.setName(rs.getString("NAME")); 	        
				trn.setName_e(rs.getString("NAME_E")); 	         
				trn.setTransferType(rs.getString("TRANSFER_TYPE")); 	 
				trn.setAmount(rs.getBigDecimal("AMOUNT")); 	         
				trn.setCharge(rs.getBigDecimal("CHARGE")); 	         
				trn.setStatus(rs.getString("STATUS")); 	         
				trn.setAccountNo(rs.getString("ACCOUNT_NO")); 	     
				trn.setToAccountNo(rs.getString("TO_ACCOUNT_NO")); 	
				trn.setToName(rs.getString("TO_NAME")); 	  
				trn.setToBankName(rs.getString("TO_BANK_NAME")); 
				trn.setResponseCode(rs.getString("RESPONSE_CODE"));
				ltrn.add(trn);         
				}         
			} catch (SQLException ex) {   
			log.error(" getListRTGSFunct2 : " + ex.getMessage());    
		} finally {           
			closeConnDb(conn, stat, rs);     
			}        
		return ltrn;   
		}                

   public List searchMonRtgsIb(String sDate, String network, String rel_trn,  String rek_f_ac, String rek_t_ac, String amont, String product_code) {
   	 List ltrn = new ArrayList();
        PctbContractMaster trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
       	 if ((rel_trn==null ) && (rek_f_ac==null ) && (rek_t_ac==null ) && (amont==null) && (product_code==null )){
       		 return ltrn;
       	 }
       			 
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
            int i = 1;
            sql = "select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue," +
            		"product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, cust_name, txn_amount, prod_ref_no," +
            		"udf_2, dispatch_ref_no, remarks, cust_name " +
            		"from PCTBS_CONTRACT_MASTER " +
            		"where activation_dt = TO_DATE(?, 'DD-MM-YYYY')";
            if(network!=null && !"".equals(network)){
           	 sql += " and network = ? ";
            }
            if(network!=null && !"".equals(network)){
              	 sql += " and product_code = ? ";
            }
            if(rel_trn!=null && !"".equals(rel_trn)){
           	 sql += " and contract_ref_no = ? ";
            }
            if(rek_f_ac!=null && !"".equals(rek_f_ac) ){
           	 sql += " and cust_ac_no = ? ";
            }
            if(rek_t_ac!=null && !"".equals(rek_t_ac)){
           	 sql += " and cpty_ac_no = ? ";
            }
            if(amont!=null && !"".equals(amont)){
              	 sql += " and txn_amount = ? ";
               } 
            stat = conn.prepareStatement(sql);
            stat.setString(i++, sDate);
            if(network!=null && !"".equals(network)){
                stat.setString(i++, network);
            }
            if(product_code!=null && !"".equals(product_code)){
                stat.setString(i++, product_code);
            }
            if(rel_trn!=null && !"".equals(rel_trn)){
                stat.setString(i++,rel_trn);
            }
            if(rek_f_ac!=null && !"".equals(rek_f_ac)){
                stat.setString(i++,rek_f_ac);
            }
            if(rek_t_ac!=null && !"".equals(rek_t_ac)){
                stat.setString(i++,rek_t_ac);
            }
            if(amont!=null && !"".equals(amont)){
                stat.setString(i++,amont);
            }
            rs = stat.executeQuery();
            while (rs.next()) {
            	trn = new PctbContractMaster();
            	trn.setBookingDt(rs.getDate("booking_dt"));
            	trn.setContractRefNo(rs.getString("contract_ref_no"));
            	trn.setBranchOfInput(rs.getString("branch_of_input"));
            	trn.setMakerId(rs.getString("maker_id"));
            	trn.setCheckerId(rs.getString("checker_id"));
            	trn.setNetwork(rs.getString("network"));
            	trn.setUdf2(rs.getString("udf_2"));
            	trn.setProductCode(rs.getString("product_code"));
            	trn.setCptyBankcode(rs.getString("cpty_bankcode"));
            	trn.setCptyAcNo(rs.getString("cpty_ac_no"));
            	trn.setCptyName(rs.getString("cpty_name"));
            	trn.setCustAcNo(rs.getString("cust_ac_no"));
            	trn.setCustName(rs.getString("cust_name"));
            	trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
            	trn.setDispatchRefNo(rs.getString("dispatch_ref_no"));
            	trn.setExceptionQueue(rs.getString("exception_queue"));
        
               ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" get getListMCBMon : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
   }
        
	public List searchMonRtgsMcbu(String sDate, String network, String srv,
			String sts, String pcd) {
		List ltrn = new ArrayList();
		PctbContractMaster trn = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			int i = 1;
			sql = "select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue,"
					+ "product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, cust_name, txn_amount, prod_ref_no,"
					+ "udf_2, dispatch_ref_no, remarks, cust_name "
					+ "from PCTBS_CONTRACT_MASTER where activation_dt = TO_DATE(?, 'DD-MM-YYYY') ";
			if ("IB/CMS".equals(srv) && "PROCEED".equals(sts)) {
				sql += " and network = ? and product_code = ? and dispatch_ref_no is not null and checker_id is not null and exception_queue = ? ";
			} else if ("IB/CMS".equals(srv) && "UNPROCEED".equals(sts)) {
				sql += " and network = ? and product_code = ? and  (dispatch_ref_no is null or checker_id is null or exception_queue <> ?)";
			} else if ("MCB".equals(srv) && "PROCEED".equals(sts)) {
				sql += " and network = ? and product_code <> ? and dispatch_ref_no is not null and checker_id is not null and exception_queue = ?";
			} else if ("MCB".equals(srv) && "UNPROCEED".equals(sts)) {
				sql += " and network = ? and product_code <> ? and  (dispatch_ref_no is null or checker_id is null or exception_queue <> ?)";
			}
			stat = conn.prepareStatement(sql);
			stat.setString(i++, sDate);
			stat.setString(i++, network);
			stat.setString(i++, pcd);
			stat.setString(i++, "##");
			rs = stat.executeQuery();
			while (rs.next()) {
				trn = new PctbContractMaster();
				trn.setBookingDt(rs.getDate("booking_dt"));
				trn.setContractRefNo(rs.getString("contract_ref_no"));
				trn.setBranchOfInput(rs.getString("branch_of_input"));
				trn.setMakerId(rs.getString("maker_id"));
				trn.setCheckerId(rs.getString("checker_id"));
				trn.setNetwork(rs.getString("network"));
				trn.setUdf2(rs.getString("udf_2"));
				trn.setProductCode(rs.getString("product_code"));
				trn.setCptyBankcode(rs.getString("cpty_bankcode"));
				trn.setCptyAcNo(rs.getString("cpty_ac_no"));
				trn.setCptyName(rs.getString("cpty_name"));
				trn.setCustAcNo(rs.getString("cust_ac_no"));
				trn.setCustName(rs.getString("cust_name"));
				trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
				trn.setDispatchRefNo(rs.getString("dispatch_ref_no"));
				trn.setExceptionQueue(rs.getString("exception_queue"));
				ltrn.add(trn);
			}
		} catch (SQLException ex) {
			log.error(" get getListMCBMon : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ltrn;
	}
	
	public PctbContractMaster searchMonRtgsIb(String query) {
		PctbContractMaster trn = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			int i = 1;
			sql = "select booking_dt, contract_ref_no, branch_of_input, maker_id, checker_id, network,exception_queue,"
					+ "product_code, cpty_bankcode, cpty_ac_no, cpty_name, cust_ac_no, cust_name, txn_amount, prod_ref_no,"
					+ "udf_2, dispatch_ref_no, remarks, cust_name "
					+ "from PCTBS_CONTRACT_MASTER " + query;
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			if (rs.next()) {
				trn = new PctbContractMaster();
				trn.setBookingDt(rs.getDate("booking_dt"));
				trn.setContractRefNo(rs.getString("contract_ref_no"));
				trn.setBranchOfInput(rs.getString("branch_of_input"));
				trn.setMakerId(rs.getString("maker_id"));
				trn.setCheckerId(rs.getString("checker_id"));
				trn.setNetwork(rs.getString("network"));
				trn.setUdf2(rs.getString("udf_2"));
				trn.setProductCode(rs.getString("product_code"));
				trn.setCptyBankcode(rs.getString("cpty_bankcode"));
				trn.setCptyAcNo(rs.getString("cpty_ac_no"));
				trn.setCptyName(rs.getString("cpty_name"));
				trn.setCustAcNo(rs.getString("cust_ac_no"));
				trn.setCustName(rs.getString("cust_name"));
				trn.setTxnAmount(rs.getBigDecimal("txn_amount"));
				trn.setDispatchRefNo(rs.getString("dispatch_ref_no"));
				trn.setExceptionQueue(rs.getString("exception_queue"));
			}
		} catch (SQLException ex) {
			log.error(" get getListMCBMon : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return trn;
	}
	
	public List gettrxrtgsin2(String inputtrn, String stsrtgs, String amont_a, String amont_b) {
        List ltrn = new ArrayList();
        TRtiftsin trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
            sql = "SELECT valuedate, reltrn, trn, transCode, bor, osn, fromaccnum, fromaccname, toaccnum, toaccname, " +
            	  "ultimateBeneAcc, ultimateBeneName, amount, PayDetails, status, saktiNum, FromMember, ToMember " +
      	  		  "FROM dbo.T_RTIFTSIN";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new TRtiftsin();
                trn.setValuedate(rs.getString("valuedate"));
            	trn.setRelTRN(rs.getString("reltrn"));
            	trn.setTRN(rs.getString("trn"));
            	trn.setTranscode(rs.getString("transcode"));
            	trn.setBor(rs.getString("bor"));
            	trn.setOsn(rs.getString("osn"));
            	trn.setFromaccnum(rs.getString("fromaccnum"));
            	trn.setFromaccname(rs.getString("fromaccname"));
            	trn.setFromaccnum(rs.getString("toaccnum"));
            	trn.setFromaccname(rs.getString("toaccname"));
            	trn.setUltimatebeneacc(rs.getString("ultimateBeneAcc"));
            	trn.setUltimatebenename(rs.getString("ultimateBeneName"));
            	trn.setAmount(rs.getString("amount"));
            	trn.setPayDetails(rs.getString("PayDetails"));
            	trn.setStatus(rs.getString("status"));
            	trn.setSaktinum(rs.getString("saktiNum"));
                
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
	
	public List searchRtgsIn(String sDate, String inputtrn, String stsrtgs, String amont_a,  String amont_b) {
   	 List ltrn = new ArrayList();
   	 	TRtiftsin trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";

        try {
       	 if ((inputtrn==null ) && (stsrtgs==null ) && (amont_a==null )&& (amont_b==null )){
       		 return ltrn;
       	 }
       			 
            conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();  
            int i = 1;
            sql = "SELECT valuedate, reltrn, trn, transCode, bor, osn, fromaccnum, fromaccname,  toaccnum, toaccname, " +
            	  "ultimateBeneAcc, ultimateBeneName, (CAST(amount AS NUMERIC(20, 2)) / 100 ) as Nominal, PayDetails, " +
            	  "status, saktiNum, FromMember, ToMember " +
            	  "FROM dbo.T_RTIFTSIN " +
            	  "where valuedate = '"+sDate+"'";
            if(inputtrn!=null && !"".equals(inputtrn)){
           	 sql += " and reltrn = ? ";
            }
            if(stsrtgs!=null && !"".equals(stsrtgs) ){
           	 sql += " and status = ? ";
            }
            if(amont_a!=null && !"".equals(amont_a)){
           	 sql += " and (CAST(amount AS NUMERIC(20, 2)) / 100) >= ? ";
            }
            if(amont_b!=null && !"".equals(amont_b)){
           	 sql += " and (CAST(amount AS NUMERIC(20, 2)) / 100) <= ? ";
            }

            stat = conn.prepareStatement(sql);
            if(inputtrn!=null && !"".equals(inputtrn)){
                stat.setString(i++,inputtrn);
            }
            if(stsrtgs!=null && !"".equals(stsrtgs)){
                stat.setString(i++,stsrtgs);
            }
            if(amont_a!=null && !"".equals(amont_a)){
                stat.setString(i++,amont_a);
            }
            if(amont_b!=null && !"".equals(amont_b)){
                stat.setString(i++,amont_b);
            }
            rs = stat.executeQuery();
            while (rs.next()) {
           	trn = new TRtiftsin();
            	trn.setValuedate(rs.getString("valuedate"));
            	trn.setRelTRN(rs.getString("reltrn"));
            	trn.setTRN(rs.getString("trn"));
            	trn.setTranscode(rs.getString("transcode"));
            	trn.setBor(rs.getString("bor"));
            	trn.setOsn(rs.getString("osn"));
            	trn.setFromaccnum(rs.getString("fromaccnum"));
            	trn.setFromaccname(rs.getString("fromaccname"));
            	trn.setToaccnum(rs.getString("toaccnum"));
            	trn.setToaccname(rs.getString("toaccname"));
            	trn.setUltimatebeneacc(rs.getString("ultimateBeneAcc"));
            	trn.setUltimatebenename(rs.getString("ultimateBeneName"));
            	trn.setNominal(rs.getBigDecimal("Nominal"));
            	trn.setPayDetails(rs.getString("PayDetails"));
            	trn.setStatus(rs.getString("status"));
            	trn.setSaktinum(rs.getString("saktiNum"));
            	
               ltrn.add(trn);
            }
        } catch (SQLException ex) {
       	 	log.error(" get getListRTGSin : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
   }
	
	public List getrtgsmcbin2(String sDate, String rel_trn, String rek_t_ac, String amont_a, String amont_b) {
        List ltrn = new ArrayList();
        TRTGSmcbin trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql =  "select a.trn_dt, a.trn_ref_no, a.event, a.module, a.ac_branch, a.trn_code, a.batch_no, " +
            	   "a.user_id, a.auth_id, a.auth_stat, a.ac_no, b.ac_desc, a.drcr_ind, a.lcy_amount " +
            	   "from actb_daily_log a, sttm_cust_account b " +
            	   "where a.ac_no=b.cust_ac_no(+) " +
            	   "and a.ac_branch = b.branch_code(+) " +
            	   "and a.trn_code in ('411', '4D1', '4E1') " +
            	   "and a.user_id = 'FLEXGW' and (length(a.ac_no) = 10 or a.ac_no = '205005014') " +
            	   "and a.trn_dt =TO_DATE('"+sDate+"', 'DD-MM-YYYY')";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new TRTGSmcbin();
                trn.setTrn_dt(rs.getString("trn_dt").substring(0, 10));
            	trn.setTrn_ref_no(rs.getString("trn_ref_no"));
            	trn.setEvent(rs.getString("event"));
            	trn.setModule(rs.getString("module"));
            	trn.setAc_branch(rs.getString("ac_branch"));
            	trn.setTrn_code(rs.getString("trn_code"));
            	trn.setBatch_no(rs.getString("batch_no"));
            	trn.setUser_id(rs.getString("user_id"));
            	trn.setAuth_id(rs.getString("auth_id"));
            	trn.setAuth_stat(rs.getString("auth_stat"));
            	trn.setAc_no(rs.getString("ac_no"));
            	trn.setAc_desc(rs.getString("ac_desc"));
            	trn.setDrcr_ind(rs.getString("drcr_ind"));
            	trn.setLcy_amount(rs.getBigDecimal("lcy_amount"));
            	
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
	
	public List sMCBRtgsIn(String sDate, String rel_trn, String rek_t_ac, String amont_a, String amont_b) {
   	 	List ltrn = new ArrayList();
   	 	TRTGSmcbin trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        SqlFunction f = new SqlFunction();
        try {
       	 if ((rel_trn==null) && (rek_t_ac==null) && (amont_a==null) && (amont_b==null)){
       		 return ltrn;
       	 }
       			 
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
            int i = 1;
            Gl g = f.getGl("205005014");
            sql = "select a.trn_dt, a.trn_ref_no, a.event, a.module, a.ac_branch, a.trn_code, a.batch_no, " +
     	   		  "a.user_id, a.auth_id, a.auth_stat, a.ac_no, b.ac_desc, a.drcr_ind, a.lcy_amount " +
     	   		  "from actb_daily_log a, sttm_cust_account b " +
     	   		  "where a.ac_no=b.cust_ac_no(+) " +
     	   		  "and a.ac_branch = b.branch_code(+) " +
     	   		  "and a.trn_code in ('411', '4D1', '4E1') " +
     	   		  "and a.user_id = 'FLEXGW' and (length(a.ac_no) = 10 or a.ac_no = '205005014') " +
     	   		  "and a.trn_dt = TO_DATE('"+sDate+"', 'DD-MM-YYYY') ";
                 
            if(rel_trn!=null && !"".equals(rel_trn)){
           	 sql += " and a.trn_ref_no = ? ";
            }
            if(rek_t_ac!=null && !"".equals(rek_t_ac) ){
           	 sql += " and a.ac_no = ? ";
            }
            if(amont_a!=null && !"".equals(amont_a)){
           	 sql += " and a.lcy_amount >= ? ";
            }
            if(amont_b!=null && !"".equals(amont_b)){
           	 sql += " and a.lcy_amount <= ? ";
            }

            stat = conn.prepareStatement(sql);
            if(rel_trn!=null && !"".equals(rel_trn)){
                stat.setString(i++,rel_trn);
            }
            if(rek_t_ac!=null && !"".equals(rek_t_ac)){
                stat.setString(i++,rek_t_ac);
            }
            if(amont_a!=null && !"".equals(amont_a)){
                stat.setString(i++,amont_a);
            }
            if(amont_b!=null && !"".equals(amont_b)){
                stat.setString(i++,amont_b);
            }
            rs = stat.executeQuery();
            while (rs.next()) {
	           	trn = new TRTGSmcbin();
	           	trn.setTrn_dt(rs.getString("trn_dt").substring(0, 10));
            	trn.setTrn_ref_no(rs.getString("trn_ref_no"));
            	trn.setEvent(rs.getString("event"));
            	trn.setModule(rs.getString("module"));
            	trn.setAc_branch(rs.getString("ac_branch"));
            	trn.setTrn_code(rs.getString("trn_code"));
            	trn.setBatch_no(rs.getString("batch_no"));
            	trn.setUser_id(rs.getString("user_id"));
            	trn.setAuth_id(rs.getString("auth_id"));
            	trn.setAuth_stat(rs.getString("auth_stat"));
            	if("205005014".equals(rs.getString("ac_no"))){
                	trn.setAc_no("205005014");     
                	trn.setAc_desc(g.getGl_desc());       		
            	} else {
                	trn.setAc_no(rs.getString("ac_no"));     
                	trn.setAc_desc(rs.getString("ac_desc"));       		            		
            	}
            	trn.setDrcr_ind(rs.getString("drcr_ind"));
            	trn.setLcy_amount(rs.getBigDecimal("lcy_amount"));
            	
               ltrn.add(trn);
            }
        } catch (SQLException ex) {
       	 	log.error(" get getListRTGSin : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
   }
	
	public TRTGSmcbin getrtgsjmcbin(String argRelTrn) {
        List ltrn = new ArrayList();
        TRTGSmcbin trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql =  "select a.trn_dt, a.trn_ref_no, a.event, a.module, a.ac_branch, a.trn_code, a.batch_no, " +
            	   "a.user_id, a.auth_id, a.auth_stat, a.ac_no, b.ac_desc, a.drcr_ind, a.lcy_amount " +
            	   "from actb_daily_log a, sttm_cust_account b " +
            	   "where a.ac_no=b.cust_ac_no(+) " +
            	   "and a.ac_branch = b.branch_code(+) " +
            	   "and a.trn_code in ('411', '4D1', '4E1') " +
            	   "and a.user_id = 'FLEXGW' and (length(a.ac_no) = 10 or a.ac_no = '205005014') "+argRelTrn+"";
            	   //"and a.trn_dt =TO_DATE('"+sDate+"', 'DD-MM-YYYY')";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new TRTGSmcbin();
                trn.setTrn_dt(rs.getString("trn_dt").substring(0, 10));
            	trn.setTrn_ref_no(rs.getString("trn_ref_no"));
            	trn.setEvent(rs.getString("event"));
            	trn.setModule(rs.getString("module"));
            	trn.setAc_branch(rs.getString("ac_branch"));
            	trn.setTrn_code(rs.getString("trn_code"));
            	trn.setBatch_no(rs.getString("batch_no"));
            	trn.setUser_id(rs.getString("user_id"));
            	trn.setAuth_id(rs.getString("auth_id"));
            	trn.setAuth_stat(rs.getString("auth_stat"));
            	trn.setAc_no(rs.getString("ac_no"));
            	trn.setAc_desc(rs.getString("ac_desc"));
            	trn.setDrcr_ind(rs.getString("drcr_ind"));
            	trn.setLcy_amount(rs.getBigDecimal("lcy_amount"));
            	
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return trn;
    }
	
	public int getTotAltacc (String accno, String name){
		int tot = 1;
		Altacct bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "select count(*) as total from(SELECT nomrek,altnomrek,rekname,kdcab,kdval,reksts " +
					"FROM reklist where "; 
			if((!"".equals(accno) && accno != null) && (!"".equals(name) && name != null)){
				sql+=" (nomrek = ? or altnomrek = ?) and rekname like ? ";
			} else if(!"".equals(accno) && accno != null){
				sql+=" nomrek = ? or altnomrek = ? ";
			} else if(!"".equals(name) && name != null){
				sql+=" rekname like ?";
			}
			
			sql += " ) as total";
			System.out.println("sql getTotAltacc " + sql);
			stat = conn.prepareStatement(sql);
			if((!"".equals(accno) && accno != null) && (!"".equals(name) && name != null)){
				stat.setString(1, accno);
				stat.setString(2, accno);
				stat.setString(3, "%" + name.toUpperCase() + "%");
			} else if(!"".equals(accno) && accno != null){
				stat.setString(1, accno);
				stat.setString(2, accno);
			} else if(!"".equals(name) && name != null){
				stat.setString(1, "%" + name.toUpperCase() + "%");
			}
			rs = stat.executeQuery();
			while (rs.next()){
				tot = rs.getInt("total");
			}
		} catch (Exception e) {
			log.error("getTotAltacc :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public List getListAltacc(String accno, String name, int begin) {
		List arrAltrek = new ArrayList();
		Altacct bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "select nomrek, altnomrek, rekname, kdcab, kdval, reksts from "
				+ "(SELECT nomrek, altnomrek, rekname, kdcab, kdval, reksts " +
					"FROM reklist where "; 
			if((!"".equals(accno) && accno != null) && (!"".equals(name) && name != null)){
				sql+=" (nomrek = ? or altnomrek = ?) and rekname like ? ";
			} else if(!"".equals(accno) && accno != null){
				sql+=" nomrek = ? or altnomrek = ? ";
			} else if(!"".equals(name) && name != null){
				sql+=" rekname like ? ";
			}
			
			sql += " order by kdcab limit 30 offset ?) as alt";
			System.out.println("sql getListAltacc :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if((!"".equals(accno) && accno != null) && (!"".equals(name) && name != null)){
				stat.setString(i++, accno);
				stat.setString(i++, accno);
				stat.setString(i++, "%" + name.toUpperCase() + "%");
			} else if(!"".equals(accno) && accno != null){
				stat.setString(i++, accno);
				stat.setString(i++, accno);
			} else if(!"".equals(name) && name != null){
				stat.setString(i++, "%" + name.toUpperCase() + "%");
			}
			stat.setInt(i++, begin);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Altacct();
				bb.setCust_ac_no(rs.getString("nomrek"));
				bb.setAlt_ac_no(rs.getString("altnomrek"));
				bb.setAc_desc(rs.getString("rekname"));
				bb.setBranch_code(rs.getString("kdcab"));
				bb.setCcy(rs.getString("kdval"));
				if("1".equals(rs.getString("reksts"))){
					bb.setRecord_stat("Aktif");
				} else if("3".equals(rs.getString("reksts"))){
					bb.setRecord_stat("Tutup");
				} else {
					bb.setRecord_stat("-");
				}
				arrAltrek.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListAcct : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return arrAltrek;
	}

	public List getListPerf() {
		List arrAltrek = new ArrayList();
		Altacct bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getPERFMON().getConnection();
			sql = "SELECT TABLESPACE_NAME " +
					"FROM TBSPACE_REGISTERED "; 
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Altacct();
				bb.setTBSPACE_REGISTERED(rs.getString("TABLESPACE_NAME"));
				arrAltrek.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListAcct : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return arrAltrek;
	}
	
}




