package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.bean.IntfRtgsOutBean;
import com.muamalat.reportmcb.entity.RtgsSknMcb;
import com.muamalat.singleton.DatasourceEntry;

public class McbFunction {
	
	private static Logger log = Logger.getLogger(McbFunction.class);
	
	public void closeConnDb(Connection conn, PreparedStatement stat,
			ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public List getListRtgsMcb(IntfRtgsOutBean rtgs, String opt, int a, int b, String network) {    	  
    	List ltrn = new ArrayList();
    	RtgsSknMcb rtgsskn = null;
        Connection conn = null;
        PreparedStatement stat = null;        
        ResultSet rs = null;
        String sql = "";
        if ("out".equals(opt)) {
        	sql = "select inc, bookdt, init, activ, cust_ac_no, maker_id, checker_id, dispatch_ref_no, contract_ref_no, exception_queue, txn_amount, " +
        			"branch_code, cust_name, ac_entry_ref_no, cpty_bankcode, cpty_ac_no, cpty_name, remarks, event_sts from (" + 
        			"select rownum as inc, TO_CHAR(p.booking_dt, 'DD-MM-YYYY HH24:MI:SS') as bookdt," +
	        		" TO_CHAR(p.initiation_dt, 'DD-MM-YYYY HH24:MI:SS') as init, TO_CHAR(p.activation_dt, 'DD-MM-YYYY HH24:MI:SS') as activ," +
	        		" p.cust_ac_no, p.maker_id, p.checker_id, p.dispatch_ref_no, p.contract_ref_no, p.exception_queue, p.txn_amount, p.branch_code, p.cust_name, " +
	        		" p.ac_entry_ref_no, p.cpty_bankcode, p.cpty_ac_no, p.cpty_name, p.remarks, p.last_event_code as event_sts" +
	        		" from PCTBS_CONTRACT_MASTER p " +
	        		" where p.network = ? and p.activation_dt = to_date (?, 'DD-MM-YY') ";
        	if ("RTGSBI".equals(network)){
            	sql += "and length(p.cust_ac_no) = ? ";
	        }
	        if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
            	sql += " and p.branch_code = ? ";      		
        	}
	        if("C".equals(rtgs.getTxnsts())){
            	sql += " and p.dispatch_ref_no is not null and p.checker_id is not null and p.exception_queue = ? ";      		
        	} else if("O".equals(rtgs.getTxnsts())){
    	        sql += "and (p.dispatch_ref_no is null or p.checker_id is null) ";     		
        	}
	        if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
            	sql += " and p.ac_entry_ref_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getContractno()) && rtgs.getContractno()!= null){
            	sql += " and p.contract_ref_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
            	sql += " and p.cust_ac_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getAcc()) && rtgs.getAcc()!= null){
            	sql += " and p.cpty_ac_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getTmmbr()) && rtgs.getTmmbr()!= null){
            	sql += " and p.cpty_bankcode = ? ";      		
        	}
			if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
				sql += " and p.txn_amount >= ? ";
			}
			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
				sql += " and p.txn_amount <= ? ";
			}
	        
	        sql += ") where inc >= ? and inc <= ? " ;
        } else if ("in".equals(opt)) {
        	sql = "select inc, trn_dt, trn_ref_no, ac_branch, ac_no, trn_code, trn_desc, lcy_amount, user_id, auth_id, CF_ADDLTEXT from (" +
        			"select rownum as inc, TO_CHAR(a.trn_dt, 'DD-MM-YYYY HH24:MI:SS') trn_dt, a.trn_ref_no, a.ac_branch, a.ac_no, a.trn_code, s.trn_desc, " +
        			"a.lcy_amount, a.user_id, a.auth_id, (SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no)||', '||(select distinct trim(ADDL_TEXT) from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no) CF_ADDLTEXT " +
        			"from actb_daily_log a inner join sttms_trn_code s on a.trn_code = s.trn_code " +
        			"where a.trn_code in (?, ?, ?) and (a.ac_no = ? or length(a.ac_no) = ? ) and " +
        			" a.trn_dt =TO_DATE(?, 'DD-MM-YY') and a.user_id = ? ";
	        if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
            	sql += " and a.trn_ref_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
            	sql += " and a.ac_branch = ? ";      		
        	}
	        if(!"".equals(rtgs.getType_trns()) && rtgs.getType_trns()!= null){
            	sql += " and a.trn_code = ? ";      		
        	}
	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
            	sql += " and a.ac_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getTrn_cd()) && rtgs.getTrn_cd()!= null){
            	sql += " and a.trn_code = ? ";      		
        	}
	        if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
				sql += " and a.lcy_amount >= ? ";
			}
			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
				sql += " and a.lcy_amount <= ? ";
			}
	        sql += ") where inc >= ? and inc <= ? " ;
        }
        try {
        	int i = 1;
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            stat = conn.prepareStatement(sql);
            if ("out".equals(opt)) {
            	stat.setString(i++, network);
            	stat.setString(i++, rtgs.getBsnsdt());   
            	if ("RTGSBI".equals(network)){        		
                	stat.setInt(i++, 10);
    	        }               	 
            	if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
                	stat.setString(i++, rtgs.getKdcab());   
            	}
            	if("C".equals(rtgs.getTxnsts())){
                	stat.setString(i++, "##");            		   		
            	}
    	        if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
                	stat.setString(i++, rtgs.getReltrn());            		
            	}
    	        if(!"".equals(rtgs.getContractno()) && rtgs.getContractno()!= null){
                	stat.setString(i++, rtgs.getContractno());            		
            	}
    	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
                	stat.setString(i++, rtgs.getCust_ac_no());            		
            	}
    	        if(!"".equals(rtgs.getAcc()) && rtgs.getAcc()!= null){
                	stat.setString(i++, rtgs.getAcc());            		
            	}
    	        if(!"".equals(rtgs.getTmmbr()) && rtgs.getTmmbr()!= null){
                	stat.setString(i++, rtgs.getTmmbr());            	
            	}
    			if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtf().replace(",", "")));            	
    			}
    			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtto().replace(",", "")));    
    			}
    			stat.setInt(i++, a);
    			stat.setInt(i++, b);
            } else if ("in".equals(opt)) {
            	if("RTGSBI".equals(network)){
                	stat.setString(i++, "411");            	
                	stat.setString(i++, "4D1");            	
                	stat.setString(i++, "4E1");   
                	stat.setString(i++, "205005014");           		
            	} else if("SKNBI".equals(network)){
                	stat.setString(i++, "410");            	
                	stat.setString(i++, "4D0");            	
                	stat.setString(i++, "4E0");   
                	stat.setString(i++, "205005005");           		
            	}
            	stat.setInt(i++, 10); 	
            	stat.setString(i++, rtgs.getBsnsdt()); 
            	stat.setString(i++, "FLEXGW");
            	if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
                	stat.setString(i++, rtgs.getReltrn());
            	}
    	        if(!"".equals(rtgs.getType_trns()) && rtgs.getType_trns()!= null){
                	stat.setString(i++, rtgs.getType_trns());
            	}
    	        if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
                	stat.setString(i++, rtgs.getKdcab());
            	}
    	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
                	stat.setString(i++, rtgs.getCust_ac_no());
            	}
    	        if(!"".equals(rtgs.getTrn_cd()) && rtgs.getTrn_cd()!= null){
                	stat.setString(i++, rtgs.getTrn_cd());
            	}
    			if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtf().replace(",", "")));            	
    			}
    			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtto().replace(",", "")));    
    			}
    			stat.setInt(i++, a);
    			stat.setInt(i++, b);            	
            }
            rs = stat.executeQuery();
            while (rs.next()) {
                if ("out".equals(opt)) {
                	rtgsskn = new RtgsSknMcb();
                	rtgsskn.setBookdt((rs.getString("bookdt") == null) ? "" : rs.getString("bookdt"));
                	rtgsskn.setInit((rs.getString("init") == null) ? "" : rs.getString("init"));
    	           	rtgsskn.setActiv((rs.getString("activ") == null) ? "" : rs.getString("activ"));
    	           	rtgsskn.setCust_ac_no((rs.getString("cust_ac_no") == null) ? "" : rs.getString("cust_ac_no"));
    	           	rtgsskn.setAc_entry_ref_no((rs.getString("ac_entry_ref_no") == null) ? "" : rs.getString("ac_entry_ref_no"));
    	           	rtgsskn.setMaker_id((rs.getString("maker_id") == null) ? "" : rs.getString("maker_id"));
    	           	rtgsskn.setChecker_id((rs.getString("checker_id") == null) ? "" : rs.getString("checker_id"));
    	           	rtgsskn.setDispatch_ref_no((rs.getString("dispatch_ref_no") == null) ? "" : rs.getString("dispatch_ref_no"));
    	           	rtgsskn.setContract_ref_no((rs.getString("contract_ref_no") == null) ? "" : rs.getString("contract_ref_no"));
    	           	rtgsskn.setException_queue((rs.getString("exception_queue") == null) ? "" : rs.getString("exception_queue"));
    	           	rtgsskn.setTxn_amount((rs.getBigDecimal("txn_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("txn_amount"));
    	           	rtgsskn.setBranch_code((rs.getString("branch_code") == null) ? "" : rs.getString("branch_code"));
    	           	rtgsskn.setCust_name((rs.getString("cust_name") == null) ? "" : rs.getString("cust_name"));
    	           	rtgsskn.setCpty_bankcode((rs.getString("cpty_bankcode") == null) ? "" : rs.getString("cpty_bankcode"));
    	           	rtgsskn.setCpty_ac_no((rs.getString("cpty_ac_no") == null) ? "" : rs.getString("cpty_ac_no"));
    	           	rtgsskn.setCpty_name((rs.getString("cpty_name") == null) ? "" : rs.getString("cpty_name"));
    	           	rtgsskn.setRemarks((rs.getString("remarks") == null) ? "" : rs.getString("remarks")); 
    	           	rtgsskn.setEventdesc((rs.getString("event_sts") == null) ? "" : rs.getString("event_sts")); 
                    ltrn.add(rtgsskn);
                } else if ("in".equals(opt)) {
                	rtgsskn = new RtgsSknMcb();
                	rtgsskn.setInc(rs.getInt("inc"));
                	rtgsskn.setBookdt((rs.getString("trn_dt") == null) ? "" : rs.getString("trn_dt"));
    	           	rtgsskn.setBranch_code((rs.getString("ac_branch") == null) ? "" : rs.getString("ac_branch"));
    	           	rtgsskn.setCust_ac_no((rs.getString("ac_no") == null) ? "" : rs.getString("ac_no"));
    	           	rtgsskn.setAc_entry_ref_no((rs.getString("trn_ref_no") == null) ? "" : rs.getString("trn_ref_no"));
    	           	rtgsskn.setTrn_code((rs.getString("trn_code") == null) ? "" : rs.getString("trn_code"));
    	           	rtgsskn.setTrn_desc((rs.getString("trn_desc") == null) ? "" : rs.getString("trn_desc"));
    	           	rtgsskn.setTxn_amount((rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount"));
    	           	rtgsskn.setMaker_id((rs.getString("user_id") == null) ? "" : rs.getString("user_id"));    	           	
    	           	rtgsskn.setChecker_id((rs.getString("auth_id") == null) ? "" : rs.getString("auth_id"));
    	           	rtgsskn.setRemarks((rs.getString("CF_ADDLTEXT") == null) ? "" : rs.getString("CF_ADDLTEXT")); 
                    ltrn.add(rtgsskn);
                }
            }
        } catch (SQLException ex) {
            log.error(" getListRtgsMcb : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
	
	public int getTotRtgsMcb(IntfRtgsOutBean rtgs, String opt, String network) { 
		int tot = 0;
        Connection conn = null;
        PreparedStatement stat = null;        
        ResultSet rs = null;
        String sql = "";
        if ("out".equals(opt)) {
	        sql = "select count(1) as total " +
	        		" from PCTBS_CONTRACT_MASTER " +
	        		" where network = ? and activation_dt = to_date (?, 'DD-MM-YY') ";
	        if ("RTGSBI".equals(network)){
	        	sql += "and length(cust_ac_no) = ?";
	        }
	        if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
            	sql += " and branch_code = ? ";      		
        	}
	        if("C".equals(rtgs.getTxnsts())){
            	sql += " and dispatch_ref_no is not null and checker_id is not null and exception_queue = ? ";      		
        	} else if("O".equals(rtgs.getTxnsts())){
    	        sql += "and (dispatch_ref_no is null or checker_id is null) ";     		
        	}
	        if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
            	sql += " and ac_entry_ref_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getContractno()) && rtgs.getContractno()!= null){
            	sql += " and contract_ref_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
            	sql += " and cust_ac_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getAcc()) && rtgs.getAcc()!= null){
            	sql += " and cpty_ac_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getTmmbr()) && rtgs.getTmmbr()!= null){
            	sql += " and cpty_bankcode = ? ";      		
        	}
			if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
				sql += " and txn_amount >= ? ";
			}
			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
				sql += " and txn_amount <= ? ";
			}
        } else if ("in".equals(opt)) {
        	sql = "select count(1) as total from actb_daily_log a " +
			"where a.trn_code in (?, ?, ?) and (a.ac_no = ? or length(a.ac_no) = ?) and " +
			" a.trn_dt =TO_DATE(?, 'DD-MM-YY') and a.user_id = ? ";
        	if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
            	sql += " and a.trn_ref_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
            	sql += " and a.ac_branch = ? ";      		
        	}
	        if(!"".equals(rtgs.getType_trns()) && rtgs.getType_trns()!= null){
            	sql += " and a.trn_code = ? ";      		
        	}
	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
            	sql += " and a.ac_no = ? ";      		
        	}
	        if(!"".equals(rtgs.getTrn_cd()) && rtgs.getTrn_cd()!= null){
            	sql += " and a.trn_code = ? ";      		
        	}
	        if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
				sql += " and a.lcy_amount >= ? ";
			}
			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
				sql += " and a.lcy_amount <= ? ";
			}
        }
        try {
        	int i = 1;
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            stat = conn.prepareStatement(sql);
            if ("out".equals(opt)) {
            	stat.setString(i++, network);
            	stat.setString(i++, rtgs.getBsnsdt()); 
            	if ("RTGSBI".equals(network)){
                	stat.setInt(i++, 10);
    	        }
            	if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
                	stat.setString(i++, rtgs.getKdcab());   
            	}
            	if("C".equals(rtgs.getTxnsts())){
                	stat.setString(i++, "##");            		   		
            	}
    	        if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
                	stat.setString(i++, rtgs.getReltrn());            		
            	}
    	        if(!"".equals(rtgs.getContractno()) && rtgs.getContractno()!= null){
                	stat.setString(i++, rtgs.getContractno());            		
            	}
    	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
                	stat.setString(i++, rtgs.getCust_ac_no());            		
            	}
    	        if(!"".equals(rtgs.getAcc()) && rtgs.getAcc()!= null){
                	stat.setString(i++, rtgs.getAcc());            		
            	}
    	        if(!"".equals(rtgs.getTmmbr()) && rtgs.getTmmbr()!= null){
                	stat.setString(i++, rtgs.getTmmbr());            	
            	}
    			if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtf().replace(",", "")));   
    			}
    			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtto().replace(",", "")));           	
    			}
            } else if ("in".equals(opt)) {
            	if("RTGSBI".equals(network)){
                	stat.setString(i++, "411");            	
                	stat.setString(i++, "4D1");            	
                	stat.setString(i++, "4E1");   
                	stat.setString(i++, "205005014");           		
            	} else if("SKNBI".equals(network)){
                	stat.setString(i++, "410");            	
                	stat.setString(i++, "4D0");            	
                	stat.setString(i++, "4E0");   
                	stat.setString(i++, "205005005");           		
            	}  
    			stat.setInt(i++, 10);              	
            	stat.setString(i++, rtgs.getBsnsdt()); 
            	stat.setString(i++, "FLEXGW");
            	if(!"".equals(rtgs.getReltrn()) && rtgs.getReltrn()!= null){
                	stat.setString(i++, rtgs.getReltrn());
            	}
    	        if(!"".equals(rtgs.getType_trns()) && rtgs.getType_trns()!= null){
                	stat.setString(i++, rtgs.getType_trns());
            	}
    	        if(!"".equals(rtgs.getKdcab()) && rtgs.getKdcab()!= null){
                	stat.setString(i++, rtgs.getKdcab());
            	}
    	        if(!"".equals(rtgs.getCust_ac_no()) && rtgs.getCust_ac_no()!= null){
                	stat.setString(i++, rtgs.getCust_ac_no());
            	}
    	        if(!"".equals(rtgs.getTrn_cd()) && rtgs.getTrn_cd()!= null){
                	stat.setString(i++, rtgs.getTrn_cd());
            	}
    			if (!"".equals(rtgs.getAmtf()) && rtgs.getAmtf() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtf().replace(",", "")));            	
    			}
    			if (!"".equals(rtgs.getAmtto()) && rtgs.getAmtto() != null){
                	stat.setBigDecimal(i++, new BigDecimal(rtgs.getAmtto().replace(",", "")));    
    			}    	
            }
            rs = stat.executeQuery();
            if (rs.next()) {
            	tot = rs.getInt("total");
            }
        } catch (SQLException ex) {
            log.error(" getTotRtgsMcb : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return tot;
    }
}
