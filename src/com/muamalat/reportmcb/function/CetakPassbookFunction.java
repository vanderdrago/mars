package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.LmtTrnsPssbk;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.cifNonIndividual;
import com.muamalat.singleton.DatasourceEntry;

public class CetakPassbookFunction {
	private static Logger log = Logger.getLogger(CetakPassbookFunction.class);
	private Date minDate;
	private Date maxDate;
	private Date maxDate_1;
	private Date maxDate_2;
	private BigDecimal ac_entry_sr_no_min;
	private BigDecimal ac_entry_sr_no_max_1;
	private int start_no_compress;
	private int nomor2;
	
	public Date getMinDate() {
		return minDate;
	}
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	public Date getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	public Date getMaxDate_1() {
		return maxDate_1;
	}
	public void setMaxDate_1(Date maxDate_1) {
		this.maxDate_1 = maxDate_1;
	}	
	public Date getMaxDate_2() {
		return maxDate_2;
	}
	public void setMaxDate_2(Date maxDate_2) {
		this.maxDate_2 = maxDate_2;
	}
	public BigDecimal getAc_entry_sr_no_max_1() {
		return ac_entry_sr_no_max_1;
	}
	public void setAc_entry_sr_no_max_1(BigDecimal ac_entry_sr_no_max_1) {
		this.ac_entry_sr_no_max_1 = ac_entry_sr_no_max_1;
	}
	public BigDecimal getAc_entry_sr_no_min() {
		return ac_entry_sr_no_min;
	}
	public void setAc_entry_sr_no_min(BigDecimal ac_entry_sr_no_min) {
		this.ac_entry_sr_no_min = ac_entry_sr_no_min;
	}
	public int getStart_no_compress() {
		return start_no_compress;
	}
	public void setStart_no_compress(int start_no_compress) {
		this.start_no_compress = start_no_compress;
	}
	public int getNomor2() {
		return nomor2;
	}
	public void setNomor2(int nomor2) {
		this.nomor2 = nomor2;
	}
	public void closeConnDb(Connection conn, PreparedStatement stat,
			ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	

	public Reklist getReklist(String accno) {
		Reklist bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcbprod().getConnection();
			sql = "select branch_code, cust_ac_no, ac_desc, ccy, account_class, alt_ac_no, " +
					"(case when record_stat = 'O' and auth_stat = 'A' then '1' when record_stat = 'C' then '3' else '2' end) sts, " +
					"address1 ||'<br />'|| address2 ||' '|| address3 ||' '|| address4 as address, passbook_number " +
					"from sttm_cust_account where cust_ac_no = ? or alt_ac_no = ? "; 
			stat = conn.prepareStatement(sql);
			stat.setString(1, accno);
			stat.setString(2, accno);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Reklist();
				bb.setNomrek(rs.getString("cust_ac_no"));
				bb.setAltnomrek(rs.getString("alt_ac_no"));
				bb.setRekname(rs.getString("ac_desc"));
				bb.setAcc_clas(rs.getString("account_class"));
				bb.setKdcab(rs.getString("branch_code"));
				bb.setKdval(rs.getString("ccy"));
				bb.setReksts(rs.getString("sts"));
				bb.setAddress(rs.getString("address"));
				bb.setPassbook_number((rs.getString("passbook_number") == null) ? "" : rs.getString("passbook_number"));
			}
		} catch (SQLException ex) {
			log.error(" getReklist : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public Acc_line getAcc_line(String accno, String prnt) {
		Acc_line bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			if ("print".equals(prnt)){
				sql = "SELECT account_no, branch, ccy, ac_entry_sr_no, prev_line_no, prev_page_no, print_date, last_date_trn, to_char(last_date_trn, 'DD-MM-YYYY') as last_date_trn_s, prev_line_no_start FROM acc_line where account_no = ? ";
			} else if ("reprint".equals(prnt)){
				sql = "SELECT id, account_no, branch, ccy, ac_entry_sr_no, prev_line_no, prev_page_no, print_date, last_date_trn, to_char(last_date_trn, 'DD-MM-YYYY') as last_date_trn_s, user_print," +
						"prev_line_no_start, user_spv_print, sts " +
						"FROM acc_line_hist where account_no = ? and sts = ? and " +
						"print_date = (select max(print_date) from acc_line_hist where account_no = ? and sts = ?); ";
			}
			stat = conn.prepareStatement(sql);
			if ("print".equals(prnt)){
				stat.setString(1, accno);
			} else if ("reprint".equals(prnt)){
				stat.setString(1, accno);	
				stat.setString(2, "P");			
				stat.setString(3, accno);	
				stat.setString(4, "P");			
			}			
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Acc_line();
				if ("reprint".equals(prnt)){
					bb.setId(rs.getBigDecimal("id"));
					bb.setUser_print(rs.getString("user_print"));
					bb.setUser_spv_print(rs.getString("user_spv_print"));
				}
				bb.setAccount_no(rs.getString("account_no"));
				bb.setBranch(rs.getString("branch"));
				bb.setCcy(rs.getString("ccy"));
				bb.setAc_entry_sr_no(rs.getBigDecimal("ac_entry_sr_no"));
				bb.setPrev_line_no(rs.getInt("prev_line_no"));
				bb.setPrev_page_no(rs.getInt("prev_page_no"));
				bb.setPrint_date(rs.getTimestamp("print_date"));
				bb.setLast_date_trn(rs.getDate("last_date_trn"));
				bb.setLast_date_trn_s(rs.getString("last_date_trn_s"));
				bb.setPrev_line_no_start(rs.getInt("prev_line_no_start"));
			}
		} catch (SQLException ex) {
			log.error("Error in getAcc_line : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public Acc_line getAcc_lineMCBProd(String accno) {
		Acc_line bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcbprod().getConnection();
			sql = "select p.account_no, a.branch_code as branch, a.ccy, p.ac_entry_sr_no, p.line_no as prev_line_no, 0 as prev_page_no, p.val_dt as print_date, " +
					"p.trn_dt as last_date_trn, 0 as prev_line_no_start from IFTW_PASSBOOK_DETAIL p, sttm_cust_account a " +
					"where p.ac_entry_sr_no in ( select max(ac_entry_sr_no) from IFTW_PASSBOOK_DETAIL where account_no = p.account_no and ac_entry_sr_no is not null " +
					") and p.account_no = ? and p.account_no = a.cust_ac_no ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, accno);			
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Acc_line();
				bb.setAccount_no(rs.getString("account_no"));
				bb.setBranch(rs.getString("branch"));
				bb.setCcy(rs.getString("ccy"));
				bb.setAc_entry_sr_no(rs.getBigDecimal("ac_entry_sr_no"));
				bb.setPrev_line_no(rs.getInt("prev_line_no"));
				bb.setPrev_page_no(rs.getInt("prev_page_no"));
				bb.setPrint_date(rs.getTimestamp("print_date"));
				bb.setLast_date_trn(rs.getDate("last_date_trn"));
				bb.setPrev_line_no_start(rs.getInt("prev_line_no_start"));
			}
		} catch (SQLException ex) {
			log.error("Error in getAcc_lineMCBProd : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public Acc_line getAcc_lineMCBHist(String accno) {
		Acc_line bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcbprod().getConnection();
			sql = "select a.cust_ac_no as account_no, a.branch_code as branch, a.ccy, h.ac_entry_sr_no, 0 as prev_line_no, 0 as prev_page_no, " +
					"SYSDATE as print_date, h.trn_dt as last_date_trn, 0 as prev_line_no_start " +
					"from ACVWS_ALL_AC_ENTRIES h, sttm_cust_account a " +
					"where h.ac_no = a.cust_ac_no and h.ac_no = ? and " +
					"h.ac_entry_sr_no = (select min(aa.ac_entry_sr_no) from ACVWS_ALL_AC_ENTRIES aa, sttm_cust_account bb " +
					"where aa.ac_no = bb.cust_ac_no and aa.ac_no = ?)";
			stat = conn.prepareStatement(sql);
			stat.setString(1, accno);		
			stat.setString(2, accno);			
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Acc_line();
				bb.setAccount_no(rs.getString("account_no"));
				bb.setBranch(rs.getString("branch"));
				bb.setCcy(rs.getString("ccy"));
				bb.setAc_entry_sr_no(rs.getBigDecimal("ac_entry_sr_no").subtract(new BigDecimal(1)));
				bb.setPrev_line_no(rs.getInt("prev_line_no"));
				bb.setPrev_page_no(rs.getInt("prev_page_no"));
				bb.setPrint_date(rs.getTimestamp("print_date"));
				bb.setLast_date_trn(rs.getDate("last_date_trn"));
				bb.setPrev_line_no_start(rs.getInt("prev_line_no_start"));
			}
		} catch (SQLException ex) {
			log.error("Error in getAcc_lineMCBProd : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public List getAcc_lineHist(String accno, String userid, String spvid) {
		List laccl = new ArrayList();
		Acc_line bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			int i = 1;
			SMTB_USER_ROLE usr = null;
			String s = "";
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			sql = "SELECT id, account_no, branch, passbook_number, ac_entry_sr_no, (prev_line_no + 1) as prev_line_no , (prev_page_no + 1) as prev_page_no, " +
				"total_trns, to_char(print_date, 'DD-MM-YYYY HH12:MI:SS') as print_date_s, last_date_trn, user_print," +
				"(prev_line_no_start + 1) as prev_line_no_start, user_spv_print, sts, notes " +
				"FROM acc_line_hist where ";
			if(!"".equals(accno) && accno != null){
				sql += " account_no = ? and";
			}
			if(!"".equals(userid) && userid != null){
				sql += " user_print = ? and";
			}
			if(!"".equals(spvid) && spvid != null){
				sql += " user_spv_print = ? and";
			}
			sql = sql.substring(0, sql.length() - 3);
            sql += " order by print_date desc";
			stat = conn.prepareStatement(sql);
			int x = 1;
			if(!"".equals(accno) && accno != null){
				stat.setString(x++, accno);
			}
			if(!"".equals(userid) && userid != null){
				stat.setString(x++, userid);
			}
			if(!"".equals(spvid) && spvid != null){
				stat.setString(x++, spvid);
			}
			
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Acc_line();
				bb.setNo(i++);
				bb.setAccount_no(rs.getString("account_no"));
				bb.setBranch(rs.getString("branch"));
				bb.setPassbook_number(rs.getString("passbook_number"));
				bb.setAc_entry_sr_no(rs.getBigDecimal("ac_entry_sr_no"));
				bb.setPrev_line_no(rs.getInt("prev_line_no"));
				bb.setPrev_page_no(rs.getInt("prev_page_no"));
				bb.setTotal_trns(rs.getInt("total_trns"));
				bb.setPrint_date_s(rs.getString("print_date_s"));
				bb.setLast_date_trn(rs.getDate("last_date_trn"));
				bb.setUser_print(rs.getString("user_print"));
				bb.setPrev_line_no_start(rs.getInt("prev_line_no_start"));
				bb.setUser_spv_print(rs.getString("user_spv_print"));
				if ("P".equals(rs.getString("sts"))){
					bb.setSts("Cetak");
				} else if ("RP".equals(rs.getString("sts"))){
					bb.setSts("Cetak Ulang");
				}
				bb.setNotes(rs.getString("notes"));
				laccl.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getAcc_lineHist : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return laccl;
	}
	
	public Acc_line getAcc_lineRePrint(String accno) {
		Acc_line bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			String s = "";
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			sql = "SELECT (select count(*) as total FROM acc_line_hist c where c.ac_entry_sr_no = a.ac_entry_sr_no and c.sts=a.sts) as total," +
					"a.account_no, a.branch, a.ccy, a.passbook_number, a.ac_entry_sr_no, a.prev_line_no, a.prev_page_no, a.total_trns, " +
					"to_char(a.print_date, 'yyyy-MM-dd mm:ss:dd') as print_dates, a.last_date_trn, a.user_print, a.prev_line_no_start, a.user_spv_print, a.sts, a.notes " +
					"FROM acc_line_hist a where a.account_no = ? and a.sts = ? and " +
					"a.print_date = (select max(b.print_date) from acc_line_hist b where b.account_no = ? and b.sts=a.sts)";
			int x = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(x++, accno);			
			stat.setString(x++, "RP");	
			stat.setString(x++, accno);		
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Acc_line();
				bb.setNo(rs.getInt("total"));
				bb.setAccount_no(rs.getString("account_no"));
				bb.setBranch(rs.getString("branch"));
				bb.setPassbook_number(rs.getString("passbook_number"));
				bb.setAc_entry_sr_no(rs.getBigDecimal("ac_entry_sr_no"));
				bb.setPrev_line_no(rs.getInt("prev_line_no"));
				bb.setPrev_page_no(rs.getInt("prev_page_no"));
				bb.setTotal_trns(rs.getInt("total_trns"));
				bb.setPrint_date_s(rs.getString("print_dates"));
				bb.setLast_date_trn(rs.getDate("last_date_trn"));
				bb.setUser_print(rs.getString("user_print"));
				bb.setPrev_line_no_start(rs.getInt("prev_line_no_start"));
				bb.setUser_spv_print(rs.getString("user_spv_print"));
				bb.setNotes(rs.getString("notes"));
			}
		} catch (SQLException ex) {
			log.error("getAcc_lineRePrint : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
//	public LmtTrnsPssbk getlimit_trans_passbook(String acc_class) {
	public LmtTrnsPssbk getlimit_trans_passbook() {
		LmtTrnsPssbk lmt = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			sql = "SELECT distinct product_cd, limit_trans, limit_per_page, limit_max_time FROM limit_trans_passbook where product_cd = ? ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, "XXX");
			rs = stat.executeQuery();
			if (rs.next()) {
				lmt = new LmtTrnsPssbk();
				lmt.setProduct_cd(rs.getString("product_cd"));
				lmt.setLimit_trans(rs.getInt("limit_trans"));
				lmt.setLimit_per_page(rs.getInt("limit_per_page"));
				lmt.setLimit_max_time(rs.getInt("limit_max_time"));
			} 
		} catch (SQLException ex) {
			log.error(" getlimit_trans_passbook : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lmt;
	}
	
	public List getListTrns(Acc_line acc, Acc_line acc_m, BigDecimal saldoawal, String prnt) {
		List lbb = new ArrayList();
		CetakPassbook bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int no = 1;
		try {			
			conn = DatasourceEntry.getInstance().getMcbprod().getConnection();
			if ("print".equals(prnt)){	
				sql = "SELECT a.LCY_AMOUNT, a.FCY_AMOUNT, to_char(a.TRN_DT, 'DD/MM/YYYY') as TRN_DTs, a.TRN_DT, " +
					"to_char(a.VALUE_DT, 'DD/MM/YYYY')as VALUE_DT, a.DRCR_IND, a.TRN_REF_NO, a.EVENT, a.TRN_CODE, SUBSTR(t.trn_desc, 1, 30) as trn_desc, a.AC_ENTRY_SR_NO, a.AUTH_ID " +
					"FROM ACVWS_ALL_AC_ENTRIES a, sttms_trn_code t " +
					"WHERE a.trn_code = t.trn_code and a.ac_branch = ? and a.ac_no = ? and a.TRN_DT >= TO_DATE(?, 'DD-MM-YYYY') and " +
					"a.AC_ENTRY_SR_NO > ? order by a.AC_ENTRY_SR_NO asc ";		
			} else if ("reprint".equals(prnt)){
				sql = "SELECT a.LCY_AMOUNT, a.FCY_AMOUNT, to_char(a.TRN_DT, 'DD/MM/YYYY') as TRN_DTs, a.TRN_DT, " +
					"to_char(a.VALUE_DT, 'DD/MM/YYYY')as VALUE_DT, a.DRCR_IND, a.TRN_REF_NO, a.EVENT, a.TRN_CODE, SUBSTR(t.trn_desc, 1, 30) as trn_desc, a.AC_ENTRY_SR_NO, a.AUTH_ID " +
					"FROM ACVWS_ALL_AC_ENTRIES a, sttms_trn_code t " +
					"WHERE a.trn_code = t.trn_code and a.ac_branch = ? and a.ac_no = ? and " +
					"(a.TRN_DT between  TO_DATE(?, 'DD-MM-YYYY') and TO_DATE(?, 'DD-MM-YYYY')) and " +
					"a.AC_ENTRY_SR_NO > ? and a.AC_ENTRY_SR_NO <= ? order by a.AC_ENTRY_SR_NO asc ";
				
			}
			int i =1;
			stat = conn.prepareStatement(sql);
			if ("print".equals(prnt)){	
				stat.setString(i++, acc.getBranch());			
				stat.setString(i++, acc.getAccount_no());		
				stat.setString(i++, acc.getLast_date_trn_s());
				stat.setBigDecimal(i++, acc.getAc_entry_sr_no());
			} else if ("reprint".equals(prnt)){	
				stat.setString(i++, acc.getBranch());			
				stat.setString(i++, acc.getAccount_no());		
				stat.setString(i++, acc.getLast_date_trn_s());
				stat.setString(i++, acc_m.getLast_date_trn_s());
				stat.setBigDecimal(i++, acc.getAc_entry_sr_no());
				stat.setBigDecimal(i++, acc_m.getAc_entry_sr_no());				
			}
			
			rs = stat.executeQuery();
			BigDecimal salidr = saldoawal;
			Calendar cal = Calendar.getInstance();
			
//			minDate = cal.getTime();
//			maxDate = cal.getTime();
//			Date today = cal.getTime();
			
			DateFormat formatter = null;
	        Date today = null;

	        try {
	            int currentDay = cal.get(Calendar.DATE);
	            int currentMonth = cal.get(Calendar.MONTH) + 1;
	            int currentYear = cal.get(Calendar.YEAR);
	            DataFunction df = new DataFunction();
	            String yyyyMMdd = "" + currentYear + "" + df.converttostr(currentMonth) + "" + df.converttostr(currentDay);
	            formatter = new SimpleDateFormat("yyyyMMdd");
	            today = (Date) formatter.parse(yyyyMMdd);

	        } catch (Exception e) {
	        	log.info("Error : " + e.getMessage());
	        }
			cal.add(Calendar.DATE, -100);
			maxDate_1 = today;
			maxDate_2 = cal.getTime();
			int mulai_kompres = 0;
			while (rs.next()) {
				bb = new CetakPassbook();
				bb.setNo(no++);				
				if (rs.getDate("TRN_DT") != null){
					if (rs.getDate("TRN_DT").compareTo(today) < 0){
						if (rs.getDate("TRN_DT").compareTo(maxDate_1) > 0) {
							mulai_kompres = bb.getNo();
							maxDate_2 = maxDate_1;
		                } 
	                	maxDate_1 = rs.getDate("TRN_DT");
					}
//					System.out.println(bb.getNo() + ". trn_dt : " + rs.getDate("TRN_DT") + ", 1 : " + rs.getDate("TRN_DT").compareTo(today) + 
//							", 2 : " + rs.getDate("TRN_DT").compareTo(maxDate_1) + ", mulai_kompres : " + mulai_kompres + ", maxDate_2 : " + maxDate_2);

					/*
					maxDate = rs.getDate("TRN_DT");	
					int ret = minDate.compareTo(rs.getDate("TRN_DT"));
					if (ret == 1){
						minDate = rs.getDate("TRN_DT");		
						if (ac_entry_sr_no_min == null){
							ac_entry_sr_no_min = rs.getBigDecimal("AC_ENTRY_SR_NO");							
						} else {
							int sr_min = ac_entry_sr_no_min.compareTo(rs.getBigDecimal("AC_ENTRY_SR_NO"));
							if(sr_min == 1){
								ac_entry_sr_no_min = rs.getBigDecimal("AC_ENTRY_SR_NO");								
							}
						}
					}
					int yy = maxDate.compareTo(convertedDate);
					int xx = -2;
					if (yy == -1 || yy == 0){
						maxDate_1 = maxDate;
						xx = maxDate_2.compareTo(maxDate_1);
						if (xx == -1){
							maxDate_2 = maxDate_1;
							nomor = bb.getNo();
						} 
					} */
				}
				bb.setTrn_dt((rs.getDate("TRN_DT") == null) ? null : rs.getDate("TRN_DT"));          
				bb.setTrn_dt_s((rs.getString("TRN_DTs") == null) ? "" : rs.getString("TRN_DTs"));
				bb.setTrn_code((rs.getString("TRN_CODE") == null) ? "" : rs.getString("TRN_CODE"));
				if ("IDR".equals(acc.getCcy().toUpperCase())) {
					bb.setMutasi((rs.getBigDecimal("LCY_AMOUNT") == null) ? new BigDecimal(0) : rs.getBigDecimal("LCY_AMOUNT"));
				} else {
					bb.setMutasi((rs.getBigDecimal("FCY_AMOUNT") == null) ? new BigDecimal(0) : rs.getBigDecimal("FCY_AMOUNT"));
				}
//				System.out.println("No. " + bb.getNo()+ ", trndt : " + rs.getDate("TRN_DT") + ", mindate : " + minDate +", maxdate : " + maxDate + ", maxDate_1 : " + maxDate_1 + 
//						", maxDate_2 : "+maxDate_2+" ,entry_sr_no : " + rs.getBigDecimal("AC_ENTRY_SR_NO") + ", nomor : " + mulai_kompres + ", nominal " + bb.getMutasi());
				bb.setDrcr_ind((rs.getString("DRCR_IND") == null) ? "" : rs.getString("DRCR_IND"));
				bb.setDetail((rs.getString("trn_desc") == null) ? "" : rs.getString("trn_desc"));
				if ("D".equals(bb.getDrcr_ind().trim())) {
					salidr = salidr.subtract(bb.getMutasi());	
				} else {
					salidr = salidr.add(bb.getMutasi());
				}	
				bb.setSaldo(salidr);
				bb.setAuth_id(rs.getString("AUTH_ID"));
				bb.setAc_entry_sr_no((rs.getBigDecimal("AC_ENTRY_SR_NO") == null) ? new BigDecimal(0) : rs.getBigDecimal("AC_ENTRY_SR_NO"));
				lbb.add(bb);
			}
			start_no_compress = mulai_kompres - 1;
//			System.out.println("start_no_compress : " + start_no_compress);
			} catch (SQLException ex) {
			log.error(" getListTrns : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public BigDecimal getSaldoAwalTrns(Acc_line acc) {
		BigDecimal saldo = new BigDecimal(0);
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int no = 1;
		try {
			conn = DatasourceEntry.getInstance().getMcbprod().getConnection();
			sql = "select h.ac_no, h.ac_branch, h.ac_ccy, " +
					"sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, " +
					"sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL " +
					"from ACVWS_ALL_AC_ENTRIES h " +
					"where h.ac_branch = ? and h.ac_no = ? and h.AC_ENTRY_SR_NO <= ? group by h.ac_no, h.ac_branch, h.ac_ccy ";
			
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc.getBranch());			
			stat.setString(2, acc.getAccount_no());
			stat.setBigDecimal(3, acc.getAc_entry_sr_no());
			rs = stat.executeQuery();
			if (rs.next()) {
				saldo = rs.getBigDecimal("creditGL").subtract(rs.getBigDecimal("debitGL"));
			}
		} catch (SQLException ex) {
			log.error(" getSaldoAwalTrns : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return saldo;
	}
	
	public boolean insertAccLineHist(Acc_line acc, String user) {
		boolean ret = false;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int no = 1;
		try {
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			sql = "INSERT INTO acc_line_hist" +
					"(account_no, branch, ccy, passbook_number, ac_entry_sr_no, prev_line_no, prev_page_no, " +
					"total_trns, print_date, last_date_trn, user_print, prev_line_no_start, sts) " +
					"SELECT account_no, branch, ccy, '" + acc.getPassbook_number()+ "' as passbook_number, ac_entry_sr_no, " +
							"prev_line_no, prev_page_no, " + acc.getTotal_trns()+ " as total_trns, ?, " +
							"last_date_trn, '" + user + "' as user_print, '" + acc.getPrev_line_no_start()+ "' as prev_line_no_start, 'P' " +
					"FROM acc_line where account_no = ? and branch = ? and ac_entry_sr_no = ?";		
			stat = conn.prepareStatement(sql);	
			stat.setTimestamp(1, acc.getPrint_date());
			stat.setString(2, acc.getAccount_no());
			stat.setString(3, acc.getBranch());		
			stat.setBigDecimal(4, acc.getAc_entry_sr_no());
			ret = stat.execute();
		} catch (SQLException ex) {
			log.error(" insertAccLineHist : " + ex.getMessage());			
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ret;
	}
	
	public boolean updateAccLine(Acc_line acc, Acc_line acc_new) {
		boolean ret = false;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int no = 1;
		try {
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			sql = "UPDATE acc_line SET  ac_entry_sr_no=?, prev_line_no=?, prev_page_no=?, print_date=?, last_date_trn=?, prev_line_no_start=? " +
					"WHERE account_no=? and branch=? and ac_entry_sr_no = ? ";		
			stat = conn.prepareStatement(sql);	
			stat.setBigDecimal(1, acc_new.getAc_entry_sr_no());
			stat.setInt(2, acc_new.getPrev_line_no());		
			stat.setInt(3, acc_new.getPrev_page_no());		
			stat.setTimestamp(4, acc_new.getPrint_date());
			stat.setDate(5, acc_new.getLast_date_trn());
			stat.setInt(6, (acc_new.getPrev_line_no_start() -1));
			stat.setString(7, acc.getAccount_no());
			stat.setString(8, acc.getBranch());					
			stat.setBigDecimal(9, acc.getAc_entry_sr_no());
			ret = stat.execute();
		} catch (SQLException ex) {
			log.error(" updateAccLine : " + ex.getMessage());			
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ret;
	}
	
	public boolean insertAccLineReprint(Acc_line acc) {
		boolean ret = false;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int no = 1;
		try {
			long time = System.currentTimeMillis(); 
	        java.sql.Timestamp timestamp = new java.sql.Timestamp(time);
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			sql = "INSERT INTO acc_line_hist(account_no, branch, ccy, passbook_number, ac_entry_sr_no, prev_line_no, prev_page_no, total_trns, print_date, last_date_trn, " +
					"user_print, prev_line_no_start, user_spv_print, sts, notes) " +
					"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			stat = conn.prepareStatement(sql);	
			stat.setString(1, acc.getAccount_no());	
			stat.setString(2, acc.getBranch());		
			stat.setString(3, acc.getCcy());
			stat.setString(4, acc.getPassbook_number());		
			stat.setBigDecimal(5, acc.getAc_entry_sr_no());
			stat.setInt(6, acc.getPrev_line_no());
			stat.setInt(7, acc.getPrev_page_no());
			stat.setInt(8, acc.getTotal_trns());
			stat.setTimestamp(9, timestamp);
			stat.setDate(10, acc.getLast_date_trn());
			stat.setString(11, acc.getUser_print());	
			stat.setInt(12, acc.getPrev_line_no_start());	
			stat.setString(13, acc.getUser_spv_print());		
			stat.setString(14, acc.getSts());				
			stat.setString(15, acc.getNotes());					
			ret = stat.execute();
		} catch (SQLException ex) {
			log.error(" insertAccLineReprint : " + ex.getMessage());			
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ret;
	}
	
	public boolean insertAccLineprint(Acc_line acc) {
		boolean ret = false;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int no = 1;
		try {
			long time = System.currentTimeMillis();
	        java.sql.Timestamp timestamp = new java.sql.Timestamp(time);
			conn = DatasourceEntry.getInstance().getCtkpssbk().getConnection();
			sql = "INSERT INTO acc_line(account_no, branch, ccy, ac_entry_sr_no, prev_line_no, prev_page_no, print_date, last_date_trn, prev_line_no_start) " +
					"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
			stat = conn.prepareStatement(sql);	
			stat.setString(1, acc.getAccount_no());	
			stat.setString(2, acc.getBranch());	
			stat.setString(3, acc.getCcy());	
			stat.setBigDecimal(4, acc.getAc_entry_sr_no());
			stat.setInt(5, acc.getPrev_line_no());
			stat.setInt(6, acc.getPrev_page_no());
			stat.setTimestamp(7, acc.getPrint_date());
			stat.setDate(8, acc.getLast_date_trn());
			stat.setInt(9, acc.getPrev_line_no_start());				
			ret = stat.execute();
			ret = true;
		} catch (SQLException ex) {
			log.error("insertAccLineprint : " + ex.getMessage());			
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ret;
	}
	
	
	public String checkSupervisorCabangMCB(String spvId, String kodeCabang, int level) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String supervisorId = null;
        String sql, spvRole;
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            spvRole = getAllowedRoles(level);
            sql = "select user_id from SMTB_USER_role where role_id in ('" + spvRole.replaceAll(",", "','") + "') " +
                    "and auth_stat = 'A' and user_id = ? and branch_code = ? ";
            stat = conn.prepareStatement(sql);

            stat.setString(1, spvId);
            stat.setString(2, kodeCabang);
            rs = stat.executeQuery();
            while (rs.next()) {
                supervisorId = rs.getString("user_id");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" checkSupervisorCabangMCB :1: " + ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(" checkSupervisorCabangMCB :5: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return supervisorId;
    }
	
	public String getListCabUser(String spvId, String role) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        String ret = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select role_id, user_id, auth_stat, branch_code from SMTB_USER_role where user_id = ? and role_id in ("+role+") and auth_stat = ? ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, spvId);
            stat.setString(2, "A");
            rs = stat.executeQuery();
            while (rs.next()) {
            	ret += "\'" + rs.getString("branch_code").trim() + "\',";
            	System.out.println("ret dalam :" + ret);
            	System.out.println("user Id :" + rs.getString("user_id"));
            	System.out.println("role Id :" + rs.getString("role_id"));
            }
            ret = ret.substring(0, ret.length() - 1);
            System.out.println("ret luar :" + ret);
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" getListCabUser :1: " + ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(" getListCabUser :2: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return ret;
    }
	
	public String getAllowedRoles(int level) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String strQuery = "";
        String roles = null;
        try {
            conn = DatasourceEntry.getInstance().getPostgreDS().getConnection();
            strQuery = "Select roles from dat_level_roles where level = ?";
            System.out.println("level :" + level);
            System.out.println("sql :" + strQuery);
            stat = conn.prepareStatement(strQuery);
            stat.setInt(1, level);

            rs = stat.executeQuery();
            while (rs.next()) {
                roles =  rs.getString("roles");
            }
        } catch (SQLException ex) {
            log.error(" getAllowedRoles :1: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return roles;
    }
	
	public SMTB_USER_ROLE getUser(String userid, String cab) {
		SMTB_USER_ROLE user = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String supervisorId = null;
        String sql, spvRole;
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            spvRole = getAllowedRoles(3);
            sql = "select role_id, user_id, auth_stat, branch_code from SMTB_USER_role where auth_stat = 'A' and user_id = ? and branch_code = ? ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, userid);
            stat.setString(2, cab);
            rs = stat.executeQuery();
            while (rs.next()) {
            	user = new SMTB_USER_ROLE();
            	user.setRole_id(rs.getString("role_id"));
            	user.setUser_id(rs.getString("user_id"));
            	user.setAuth_stat(rs.getString("auth_stat"));
            	user.setBranch_code(rs.getString("branch_code"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" getUser :1: " + ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(" getUser :5: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return user;

    }
	
	public SMTB_USER_ROLE getUserHCO(String userid, String role) {
		SMTB_USER_ROLE user = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = null;
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            sql = "select role_id, user_id, auth_stat, branch_code from SMTB_USER_role where auth_stat = 'A' and user_id = ? and role_id = ?";
            sql = "select role_id, user_id, auth_stat, branch_code from SMTB_USER_role where auth_stat = 'A' and user_id = ? and (role_id = ? or role_id = ?)";
            stat = conn.prepareStatement(sql);
            stat.setString(1, userid);
            stat.setString(2, role);
            System.out.println("sql smtb_user role :" + sql);
            
            rs = stat.executeQuery();
            while (rs.next()) {
            	user = new SMTB_USER_ROLE();
            	user.setRole_id(rs.getString("role_id"));
            	System.out.println("role id :" + rs.getString("role_id"));
            	user.setUser_id(rs.getString("user_id"));
            	user.setAuth_stat(rs.getString("auth_stat"));
            	user.setBranch_code(rs.getString("branch_code"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" getUserHCO :1: " + ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(" getUserHCO :5: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return user;

    }
	
	public List getUserRoles(String userid,String hco, String cfd) {
		System.out.println("user id :" + userid);
//		ArrayList<String> list = new ArrayList<String>();
		List lbb = new ArrayList();
		SMTB_USER_ROLE userRole = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "SELECT NVL(MAX(ROLE_ID), 'N/A') AS ROLE_ID FROM SMTB_USER_ROLE WHERE AUTH_STAT = 'A' AND USER_ID = ? AND (ROLE_ID LIKE '%"+ hco +"%' OR ROLE_ID LIKE '%"+ cfd +"%')";
            System.out.println("sql user Role :" + sql);
            stat = conn.prepareStatement(sql);
            stat.setString(1, userid);
            rs = stat.executeQuery();
            while (rs.next()) {
            	userRole = new SMTB_USER_ROLE();
//            	list.add(rs.getString("role_id"));
            	userRole.setRole_id(rs.getString("ROLE_ID"));
            	System.out.println("role id sql:" + rs.getString("ROLE_ID"));
            	lbb.add(userRole);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" getUser :1: " + ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(" getUser :5: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return lbb;

    }
	
	public List getRolesUser(String userid) {
		ArrayList<String> list = new ArrayList<String>();
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select distinct role_id from SMTB_USER_role where auth_stat = 'A' and user_id = ? ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, userid);
            rs = stat.executeQuery();
            while (rs.next()) {
            	list.add(rs.getString("role_id"));
            	System.out.println("role id :" + rs.getString("role_id"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(" getUser :1: " + ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(" getUser :5: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return list;

    }
	
	public String getAllowedPrintPassbook(int level, String role_id) {
		System.out.println("level getAllowedPrintPassbook :" + level);
		System.out.println("role_id getAllowedPrintPassbook :" + role_id);
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String strQuery = "";
        String roles = null;
        try {
            conn = DatasourceEntry.getInstance().getPostgreDS().getConnection();
            strQuery = "Select roles from dat_level_roles where level = ? and roles like '%"+role_id+"%'";
            	
            System.out.println("sql getAllowedPrintPassbook :" + strQuery);
            stat = conn.prepareStatement(strQuery);
            stat.setInt(1, level);

            rs = stat.executeQuery();
            while (rs.next()) {
                roles =  rs.getString("roles");
                System.out.println("roles getAllowedPrintPassbook :" + roles);
            }
        } catch (SQLException ex) {
            log.error(" getAllowedRoles :1: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return roles;
    }
	
	public String getAllowedStatement(int level, String role_id) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String strQuery = "";
        String roles = null;
        try {
            conn = DatasourceEntry.getInstance().getPostgreDS().getConnection();
            strQuery = "Select roles from dat_level_roles where level = ? and roles like '%"+role_id+"%'";
            System.out.println("sql getAllowedStatement :" + strQuery);
            System.out.println("role_id :" + role_id + "level :" + level);
            stat = conn.prepareStatement(strQuery);
            stat.setInt(1, level);
            rs = stat.executeQuery();
            while (rs.next()) {
                roles =  rs.getString("roles");
                System.out.println("role :" + rs.getString("roles"));
            }
        } catch (SQLException ex) {
            log.error(" getAllowedRoles :1: " + ex.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return roles;
    }
	
}
