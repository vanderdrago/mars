package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.bean.IntfRtgsOutBean;
import com.muamalat.reportmcb.entity.IntfRtgsOut;
import com.muamalat.reportmcb.entity.RtgsTtpn;
import com.muamalat.singleton.DatasourceEntry;

public class SknRtgsMsslFunction {
	private static Logger log = Logger.getLogger(SknRtgsMsslFunction.class);

	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public IntfRtgsOut getTotNomSknOutMssl(IntfRtgsOutBean intfOut) {
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMig26mssl().getConnection();	
			sql = "select count(*) as total, sum(amount) as nominal  FROM ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " sknout_massal_hist ";
			} else {
				sql += " sknout_massal ";
			}
			sql += " where trx_dt = ? and";
			DateFormat formatter = null;
		    Date convertedDate = null;

	        try {
	            formatter = new SimpleDateFormat("yyyyMMdd");
	            convertedDate = (Date) formatter.parse(intfOut.getBsnsdt());
	        } catch (Exception e) {
	        	log.info("error in getListSknMssl : convert date : " + e.getMessage());
	        }
            java.sql.Date sqlDate = new java.sql.Date(convertedDate.getTime());
	        
            if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				sql += " branch_cd = ? and";
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				sql += " sender_ref_no = ? and";
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				sql += " acc_from = ? and";
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				sql += " ultimate_benef_acc = ? and";
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				sql += " to_member = ? and";
			}

			sql = sql.substring(0, sql.length() - 3);
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setDate(i++, sqlDate);
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				stat.setString(i++, intfOut.getKdcab());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				stat.setString(i++, intfOut.getFracc());
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				stat.setString(i++, intfOut.getToacc());
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				stat.setString(i++, intfOut.getTmmbr());
			}
			rs = stat.executeQuery();
			if (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setTottrans(rs.getInt("total"));
				rtgsout.setTotnominal(rs.getBigDecimal("nominal"));
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomSknOutMssl : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return rtgsout;
	}
	
	public List getListSknMssl(IntfRtgsOutBean intfOut, int begin, String opt) {
		List lbb = new ArrayList();
		RtgsTtpn rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String sts_dt = "";
		String sts_trns = "";

		try {
			conn = DatasourceEntry.getInstance().getMig26mssl().getConnection();	
			sql = "SELECT trx_dt, branch_cd, sender_ref_no, acc_from, name_from, class_acc, pay_detail, amount, ultimate_benef_acc, " +
					"ultimate_benef_name, sandi_usaha, to_member, member_name, maker_id, checker_id, rekening_pndptn, fee, filename, sts_data, " +
					"sts_trns, err_cd_mcb, err_desc_mcb, to_char(tgl_oto, 'yyyy-mm-dd  HH24:MI:SS') as tgl_oto  FROM ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " sknout_massal_hist ";
			} else {
				sql += " sknout_massal ";
			}
			sql += " where trx_dt = ? and";
			DateFormat formatter = null;
		    Date convertedDate = null;

	        try {
	            formatter = new SimpleDateFormat("yyyyMMdd");
	            convertedDate = (Date) formatter.parse(intfOut.getBsnsdt());
	        } catch (Exception e) {
	        	log.info("error in getListSknMssl : convert date : " + e.getMessage());
	        }
            java.sql.Date sqlDate = new java.sql.Date(convertedDate.getTime());
	        
            if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				sql += " branch_cd = ? and";
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				sql += " sender_ref_no = ? and";
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				sql += " acc_from = ? and";
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				sql += " ultimate_benef_acc = ? and";
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				sql += " to_member = ? and";
			}

			sql = sql.substring(0, sql.length() - 3) + " order by insert_dt asc ";
			if("V".equals(opt)){
				sql += "LIMIT 30 OFFSET ?";
			}
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setDate(i++, sqlDate);
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				stat.setString(i++, intfOut.getKdcab());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				stat.setString(i++, intfOut.getFracc());
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				stat.setString(i++, intfOut.getToacc());
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				stat.setString(i++, intfOut.getTmmbr());
			}
			if("V".equals(opt)){
				stat.setInt(i++, begin);
			}
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new RtgsTtpn();
				rtgsout.setTrx_dt(rs.getDate("trx_dt"));
				rtgsout.setBranch_cd(rs.getString("branch_cd") == null ? "" : rs.getString("branch_cd"));
				rtgsout.setSender_ref_no(rs.getString("sender_ref_no") == null ? "" : rs.getString("sender_ref_no"));
				rtgsout.setFr_acc(rs.getString("acc_from") == null ? "" : rs.getString("acc_from"));
				rtgsout.setName_from(rs.getString("name_from") == null ? "" : rs.getString("name_from"));
				rtgsout.setClass_acc(rs.getString("class_acc") == null ? "" : rs.getString("class_acc"));
				rtgsout.setPay_detail(rs.getString("pay_detail") == null ? "" : rs.getString("pay_detail"));
				rtgsout.setAmount(rs.getBigDecimal("amount") == null ? new BigDecimal(0): rs.getBigDecimal("amount"));
				rtgsout.setTo_acc(rs.getString("ultimate_benef_acc") == null ? "" : rs.getString("ultimate_benef_acc"));
				rtgsout.setUltimate_benef_name(rs.getString("ultimate_benef_name") == null ? "" : rs.getString("ultimate_benef_name"));
				rtgsout.setSandi_usaha(rs.getString("sandi_usaha") == null ? "" : rs.getString("sandi_usaha"));
				rtgsout.setTo_member(rs.getString("to_member") == null ? "" : rs.getString("to_member"));
				rtgsout.setNama_member(rs.getString("member_name") == null ? "" : rs.getString("member_name"));
				rtgsout.setMaker_id(rs.getString("maker_id") == null ? "" : rs.getString("maker_id"));
				rtgsout.setChecker_id(rs.getString("checker_id") == null ? "" : rs.getString("checker_id"));
				rtgsout.setTgl_oto(rs.getString("tgl_oto") == null ? "" : rs.getString("tgl_oto"));
				rtgsout.setFee(rs.getBigDecimal("fee") == null ? new BigDecimal(0): rs.getBigDecimal("fee"));
				rtgsout.setFilename(rs.getString("filename") == null ? "" : rs.getString("filename"));
				String t_dt = rs.getString("sts_data") == null ? "" : rs.getString("sts_data");
				String t_trns = rs.getString("sts_trns") == null ? "" : rs.getString("sts_trns");
				String err_cd_mcb = rs.getString("err_cd_mcb") == null ? "" : rs.getString("err_cd_mcb");
				if("I".equals(t_dt.toUpperCase())){
	            	sts_dt = "Input";
	            	sts_trns = "Unauthorized";
	            } else if("C".equals(t_dt.toUpperCase()) && "C".equals(t_trns.toUpperCase()) && "InV".equals(err_cd_mcb.toUpperCase())){
	            	sts_dt = "Invalid";
	            	sts_trns = "Unauthorized";
	            } else if("S".equals(t_dt.toUpperCase()) && "S".equals(t_trns.toUpperCase()) && "S".equals(err_cd_mcb.toUpperCase())){
	            	sts_dt = "Valid";
	            	sts_trns = "Authorized";
	            } else if("S".equals(t_dt.toUpperCase()) && "S".equals(t_trns.toUpperCase()) && "F".equals(err_cd_mcb.toUpperCase())){
	            	sts_dt = "Valid";
	            	sts_trns = "Unauthorized";
	            } else if("R".equals(t_dt.toUpperCase()) && "R".equals(t_trns.toUpperCase())){
	            	sts_dt = "Reject";
	            	sts_trns = "Unauthorized";
	            } else {
	            	sts_dt = "InValid";
	            	sts_trns = "Unauthorized";
	            }				
				rtgsout.setSts_data(sts_dt);
				rtgsout.setSts_trns(sts_trns);
				rtgsout.setErr_cd_mcb(rs.getString("err_cd_mcb") == null ? "" : rs.getString("err_cd_mcb"));
				rtgsout.setErr_desc_mcb(rs.getString("err_desc_mcb") == null ? "" : rs.getString("err_desc_mcb"));
				lbb.add(rtgsout);
			}
		} catch (SQLException ex) {
			log.error(" getListSknMssl : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public IntfRtgsOut getTotNomRtgsOutMssl(IntfRtgsOutBean intfOut) {
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMig26mssl().getConnection();	
			sql = "select count(*) as total, sum(amount) as nominal  FROM ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " rtgsout_massal_hist ";
			} else {
				sql += " rtgsout_massal ";
			}
			sql += " where trx_dt = ? and";
			DateFormat formatter = null;
		    Date convertedDate = null;

	        try {
	            formatter = new SimpleDateFormat("yyyyMMdd");
	            convertedDate = (Date) formatter.parse(intfOut.getBsnsdt());
	        } catch (Exception e) {
	        	log.info("error in getTotNomRtgsOutMssl : convert date : " + e.getMessage());
	        }
            java.sql.Date sqlDate = new java.sql.Date(convertedDate.getTime());
	        
            if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				sql += " branch_cd = ? and";
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				sql += " sender_ref_no = ? and";
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				sql += " acc_from = ? and";
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				sql += " ultimate_benef_acc = ? and";
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				sql += " to_member = ? and";
			}

			sql = sql.substring(0, sql.length() - 3);
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setDate(i++, sqlDate);
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				stat.setString(i++, intfOut.getKdcab());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				stat.setString(i++, intfOut.getFracc());
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				stat.setString(i++, intfOut.getToacc());
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				stat.setString(i++, intfOut.getTmmbr());
			}
			rs = stat.executeQuery();
			if (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setTottrans(rs.getInt("total"));
				rtgsout.setTotnominal(rs.getBigDecimal("nominal"));
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomRtgsOutMssl : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return rtgsout;
	}
	
	public List getListRtgsMssl(IntfRtgsOutBean intfOut, int begin, String opt) {
		List lbb = new ArrayList();
		RtgsTtpn rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String sts_dt = "";
		String sts_trns = "";

		try {
			conn = DatasourceEntry.getInstance().getMig26mssl().getConnection();	
//			sql = "SELECT trx_dt, branch_cd, sender_ref_no, acc_from, name_from, class_acc, pay_detail, amount, ultimate_benef_acc, " +
//					"ultimate_benef_name, sandi_usaha, to_member, member_name, maker_id, checker_id, rekening_pndptn, fee, filename, sts_data, " +
//					"sts_trns, err_cd_mcb, err_desc_mcb, to_char(tgl_oto, 'yyyy-mm-dd  HH24:MI:SS') as tgl_oto  FROM ";
			
			sql = "SELECT branch_cd, sender_ref_no, acc_from, name_from, ultimate_benef_acc, ultimate_benef_name, amount, pay_detail, " +
					"filename, batch_ref, trx_dt, from_member, to_member, member_name, trn, rel_trn, value_date, " +
					"ta_name, fa_name,  branch_trns,  class_acc, maker_id, checker_id, sts_data, sts_trns, err_cd_mcb, err_desc_mcb, " +
					"rekening_pndptn, fee, to_char(tgl_oto, 'yyyy-mm-dd  HH24:MI:SS') as tgl_oto FROM ";
					
			
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " rtgsout_massal_hist ";
			} else {
				sql += " rtgsout_massal ";
			}
			sql += " where trx_dt = ? and";
			DateFormat formatter = null;
		    Date convertedDate = null;

	        try {
	            formatter = new SimpleDateFormat("yyyyMMdd");
	            convertedDate = (Date) formatter.parse(intfOut.getBsnsdt());
	        } catch (Exception e) {
	        	log.info("error in getListRtgsMssl : convert date : " + e.getMessage());
	        }
            java.sql.Date sqlDate = new java.sql.Date(convertedDate.getTime());
	        
            if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				sql += " branch_cd = ? and";
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				sql += " sender_ref_no = ? and";
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				sql += " acc_from = ? and";
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				sql += " ultimate_benef_acc = ? and";
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				sql += " to_member = ? and";
			}

			sql = sql.substring(0, sql.length() - 3) + " order by insert_dt asc ";
			if("V".equals(opt)){
				sql += "LIMIT 30 OFFSET ?";
			}
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setDate(i++, sqlDate);
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				stat.setString(i++, intfOut.getKdcab());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getFracc()) && intfOut.getFracc() != null){
				stat.setString(i++, intfOut.getFracc());
			}
			if (!"".equals(intfOut.getToacc()) && intfOut.getToacc() != null){
				stat.setString(i++, intfOut.getToacc());
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				stat.setString(i++, intfOut.getTmmbr());
			}
			if("V".equals(opt)){
				stat.setInt(i++, begin);
			}
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new RtgsTtpn();
				rtgsout.setTrx_dt(rs.getDate("trx_dt"));
				rtgsout.setBranch_cd(rs.getString("branch_cd") == null ? "" : rs.getString("branch_cd"));
				rtgsout.setSender_ref_no(rs.getString("sender_ref_no") == null ? "" : rs.getString("sender_ref_no"));
				rtgsout.setFr_acc(rs.getString("acc_from") == null ? "" : rs.getString("acc_from"));
				rtgsout.setName_from(rs.getString("name_from") == null ? "" : rs.getString("name_from"));
				rtgsout.setClass_acc(rs.getString("class_acc") == null ? "" : rs.getString("class_acc"));
				rtgsout.setPay_detail(rs.getString("pay_detail") == null ? "" : rs.getString("pay_detail"));
				rtgsout.setAmount(rs.getBigDecimal("amount") == null ? new BigDecimal(0): rs.getBigDecimal("amount"));
				rtgsout.setTo_acc(rs.getString("ultimate_benef_acc") == null ? "" : rs.getString("ultimate_benef_acc"));
				rtgsout.setUltimate_benef_name(rs.getString("ultimate_benef_name") == null ? "" : rs.getString("ultimate_benef_name"));
				rtgsout.setTo_member(rs.getString("to_member") == null ? "" : rs.getString("to_member"));
				rtgsout.setNama_member(rs.getString("member_name") == null ? "" : rs.getString("member_name"));
				rtgsout.setMaker_id(rs.getString("maker_id") == null ? "" : rs.getString("maker_id"));
				rtgsout.setChecker_id(rs.getString("checker_id") == null ? "" : rs.getString("checker_id"));
				rtgsout.setTgl_oto(rs.getString("tgl_oto") == null ? "" : rs.getString("tgl_oto"));
				rtgsout.setFee(rs.getBigDecimal("fee") == null ? new BigDecimal(0): rs.getBigDecimal("fee"));
				rtgsout.setFilename(rs.getString("filename") == null ? "" : rs.getString("filename"));
				String t_dt = rs.getString("sts_data") == null ? "" : rs.getString("sts_data");
				String t_trns = rs.getString("sts_trns") == null ? "" : rs.getString("sts_trns");
				String err_cd_mcb = rs.getString("err_cd_mcb") == null ? "" : rs.getString("err_cd_mcb");
				if("I".equals(t_dt.toUpperCase())){
	            	sts_dt = "Input";
	            	sts_trns = "Unauthorized";
	            } else if("C".equals(t_dt.toUpperCase()) && "C".equals(t_trns.toUpperCase()) && "InV".equals(err_cd_mcb.toUpperCase())){
	            	sts_dt = "Invalid";
	            	sts_trns = "Unauthorized";
	            } else if("S".equals(t_dt.toUpperCase()) && "S".equals(t_trns.toUpperCase()) && "S".equals(err_cd_mcb.toUpperCase())){
	            	sts_dt = "Valid";
	            	sts_trns = "Authorized";
	            } else if("S".equals(t_dt.toUpperCase()) && "S".equals(t_trns.toUpperCase()) && "F".equals(err_cd_mcb.toUpperCase())){
	            	sts_dt = "Valid";
	            	sts_trns = "Unauthorized";
	            } else if("R".equals(t_dt.toUpperCase()) && "R".equals(t_trns.toUpperCase())){
	            	sts_dt = "Reject";
	            	sts_trns = "Unauthorized";
	            } else {
	            	sts_dt = "InValid";
	            	sts_trns = "Unauthorized";
	            }				
				rtgsout.setSts_data(sts_dt);
				rtgsout.setSts_trns(sts_trns);
				rtgsout.setErr_cd_mcb(rs.getString("err_cd_mcb") == null ? "" : rs.getString("err_cd_mcb"));
				rtgsout.setErr_desc_mcb(rs.getString("err_desc_mcb") == null ? "" : rs.getString("err_desc_mcb"));
				lbb.add(rtgsout);
			}
		} catch (SQLException ex) {
			log.error(" getListSknMssl : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
}
