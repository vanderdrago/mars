/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.function;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;


import com.muamalat.singleton.DatasourceEntry;
import com.opensymphony.xwork2.ActionSupport;


/**
 * 
 * @author Utis
 */
public class CountLoginSqlFunction {
	
	public void getCountLogin(String username) throws SQLException   {
        Connection conn = null;

        Connection conn2 = null;
        
        //PreparedStatement stat = null;
        ResultSet rs = null;
       // String sql = "";
        int x = 0;
    	String sql2 = "";
    	String sql3 = "";
    	String sqldis = "";
        int jumlah=0;
    	System.out.println("--masuk login counter--"+username);
        try {
    	System.out.println(username);
    	conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
    
    		String sql ="select counter from login_count where username=?" ;
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			
			if (rs != null) {
			while (rs.next()) {
				 jumlah=rs.getInt(1);
			}}
			
		} catch (SQLException ex) {
			System.out.println("error");
		} 
       
        java.util.Date date = new java.util.Date();
        
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 
        String tgl_input =formatter.format(date.getTime());
        if(jumlah==0){
      
        	  try {
                  
              	
        	sql2 = "INSERT INTO login_count (username,counter,last_date) values (?,?::integer,?::timestamp)";
        	  PreparedStatement ps2 = conn.prepareStatement(sql2);
              
			
			ps2.setString(1, username);
			ps2.setString(2, "1");
			ps2.setString(3, tgl_input);
			  x = ps2.executeUpdate();
              
            System.out.println("--insert login counter--"+username);	
        	  } catch (Exception e) {
                  e.printStackTrace();
                  //return x;
              } 
        	  conn.close();
        }
        
        else {
        	System.out.println("mau akses mcb1");
        	 if((jumlah+1)%3==0){
        		 System.out.println("mau akses mcb2");
        			try {
        			//conn2 = DatasourceEntry.getInstance().getMcb2DS().getConnection();
        			conn2 = DatasourceEntry.getInstance().getSmtbuserDS().getConnection();
        			System.out.println("akses mcb");
        	  sqldis= "UPDATE SMTB_USER SET USER_STATUS='D' WHERE USER_ID =?";
			  PreparedStatement ps_mcb = conn2.prepareStatement(sqldis);
      		ps_mcb.setString(1, username);
      		  ps_mcb.executeUpdate();
			  conn2.close();
				System.out.println("--disabled user mcb--"+username);
        			} catch (Exception e) {
        				System.out.println("--disabled user mcb--"+e.getMessage());
        			} 
        	 }
			  
         sql3 = "update login_count set last_date=?::timestamp, counter= counter+1 where username=?";
         PreparedStatement ps3 = conn.prepareStatement(sql3);
        		ps3.setString(1, tgl_input);
			ps3.setString(2, username);
			  x = ps3.executeUpdate();

        
			System.out.println("--update login counter--"+username);	
        }
		//return jumlah ;
        conn.close();
    }
	
	public void getDeleteCount(String username) throws SQLException   {
        Connection conn = null;

        
        //PreparedStatement stat = null;
       String sql = "";
        int x = 0;
    	
     	System.out.println("--reset login counter--"+username);
    	  java.util.Date date = new java.util.Date();
          
          java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   
          String tgl_input =formatter.format(date.getTime());
          
        try {
    	conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
    
    	System.out.println("--update login counter--"+username);	
        // sql = "update login_count set last_date=?::timestamp, counter=0 where username=?";
    	sql = "DELETE from login_count where username=?";
         PreparedStatement ps = conn.prepareStatement(sql);
        		//ps.setString(1, tgl_input);
        		ps.setString(1, username);
			  ps.executeUpdate();

        
			
        } catch (Exception e) {
            e.printStackTrace();
         } 
	     conn.close();
    }
	

}
