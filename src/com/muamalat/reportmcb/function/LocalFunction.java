package com.muamalat.reportmcb.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.entity.Mars_report_desc;
import com.muamalat.singleton.DatasourceEntry;

public class LocalFunction {
	private static Logger log = Logger.getLogger(SqlFunction.class);

	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	

	public Mars_report_desc stsDisplay(String filname) {
		Mars_report_desc ret = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			System.out.println("masuk nana stsdisplay");
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			System.out.println("nana conn : " + conn);
			sql = "SELECT filename, description, sts_display  FROM mars_report_desc where filename = ? ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, filname);
			rs = stat.executeQuery();
			if (rs.next()) {
				ret= new Mars_report_desc();
				ret.setFilename(rs.getString("filename"));
				ret.setDesc(rs.getString("description"));
				ret.setSts_display(rs.getBoolean("sts_display"));
			}
		} catch (SQLException ex) {
			log.error(" stsDisplay : " + ex.getMessage());
			ex.printStackTrace();
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ret;
	}
}
