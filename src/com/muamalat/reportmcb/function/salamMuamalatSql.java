package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.muamalat.reportmcb.entity.salamMuamalat;
import com.muamalat.singleton.DatasourceEntry;

public class salamMuamalatSql {
	private static Logger log = Logger.getLogger(salamMuamalatSql.class);
	public void closeConnDb (Connection conn, PreparedStatement stat, ResultSet rs){
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				log.error("closeConnDb 1 : " + e.getMessage()); 
			}
		}
		if (rs != null){
			try {
				rs.close();
				rs = null;
			} catch (Exception e) {
				log.error("closeConnDb 2 : " + e.getMessage()); 
			}
		}
		if (stat != null){
			try {
				stat.close();
				stat = null;
			} catch (Exception e) {
				log.error("closeConnDb 3 : " + e.getMessage()); 
			}
		}
	}
	
	public List getSalamMuamalat (String no_kartu, String no_rek, String nama, String tanggal, int begin, int delta){
		System.out.println("no kartu:" + no_kartu);
		List lhistSalam = new ArrayList();
		salamMuamalat salam = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String tglcair = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
//			Class.forName("org.postgresql.Driver");
//			conn = DriverManager.getConnection("jdbc:postgresql://10.55.109.50:5432/DBMarsRep", "postgres", "postgres");
			sql = "select account_number,field_date_1 as tanggal_pencairan from cltb_account_master"
					+ " where account_number = ?";
			
		
			System.out.println("sql:" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, no_kartu);
			rs = stat.executeQuery();
			if (rs.next()){
				salam = new salamMuamalat();
				salam.setTanggal_pencairan(rs.getString("TANGGAL_PENCAIRAN"));
				System.out.println("tgl:" + rs.getString("TANGGAL_PENCAIRAN"));
				tglcair = rs.getString("TANGGAL_PENCAIRAN");
			}
			
			if (!"".equals(tglcair) && tglcair != null){
				System.out.println("1");
				sql ="SELECT DISTINCT NO,NAMA_NASABAH,TANGGAL_LAHIR,NO_KARTU,NO_REKENING_ANGSURAN,NO_CIF,PRODUK_KODE,PRODUK_KATEGORI,TANGGAL_PENCAIRAN,"
						+" TANGGAL_JATUH_TEMPO,STATUS_KARTU, AUTH_STAT FROM (SELECT ROWNUM AS NO,CUST.CUSTOMER_NAME1 AS NAMA_NASABAH,STTM.DATE_OF_BIRTH AS TANGGAL_LAHIR,"
						+" CLTB.ACCOUNT_NUMBER AS NO_KARTU,COMP.DR_PROD_AC AS NO_REKENING_ANGSURAN,CLTB.CUSTOMER_ID AS NO_CIF,CLTB.PRODUCT_CODE AS PRODUK_KODE,"
					    +" CLTB.PRODUCT_CATEGORY AS PRODUK_KATEGORI,CLTB.FIELD_DATE_1 AS TANGGAL_PENCAIRAN,CLTB.MATURITY_DATE AS TANGGAL_JATUH_TEMPO,"
					    +" CASE WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE' WHEN CLTB.ACCOUNT_STATUS = 'V'  "
					    +" THEN 'REVERSAL' ELSE 'HOLD' END STATUS_KARTU,CLTB.AUTH_STAT FROM CLTB_ACCOUNT_MASTER CLTB JOIN CLTM_PRODUCT CLTM"
					    +" ON CLTB.PRODUCT_CODE = CLTM.PRODUCT_CODE JOIN STTM_CUST_PERSONAL STTM ON CLTB.CUSTOMER_ID = STTM.CUSTOMER_NO JOIN STTM_CUSTOMER CUST"
					    +" ON CLTB.CUSTOMER_ID = CUST.CUSTOMER_NO JOIN (SELECT ACCOUNT_NUMBER,COMPONENT_NAME,DR_PROD_AC FROM CLTB_ACCOUNT_COMPONENTS"
					    +" WHERE COMPONENT_NAME = 'PRINCIPAL') COMP ON CLTB.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER WHERE CLTB.AUTH_STAT = 'A'";
			} else {
				System.out.println("2");
				sql ="SELECT DISTINCT NO,NAMA_NASABAH,TANGGAL_LAHIR,NO_KARTU,NO_REKENING_ANGSURAN,NO_CIF,PRODUK_KODE,PRODUK_KATEGORI,TANGGAL_PENCAIRAN,"
						+" TANGGAL_JATUH_TEMPO,STATUS_KARTU, AUTH_STAT FROM (SELECT ROWNUM AS NO,CUST.CUSTOMER_NAME1 AS NAMA_NASABAH,STTM.DATE_OF_BIRTH AS TANGGAL_LAHIR,"
						+" CLTB.ACCOUNT_NUMBER AS NO_KARTU,COMP.DR_PROD_AC AS NO_REKENING_ANGSURAN,CLTB.CUSTOMER_ID AS NO_CIF,CLTB.PRODUCT_CODE AS PRODUK_KODE,"
					    +" CLTB.PRODUCT_CATEGORY AS PRODUK_KATEGORI,CLTB.BOOK_DATE AS TANGGAL_PENCAIRAN,CLTB.MATURITY_DATE AS TANGGAL_JATUH_TEMPO,"
					    +" CASE WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE' WHEN CLTB.ACCOUNT_STATUS = 'V' "
					    +" THEN 'REVESAL' ELSE 'HOLD' END STATUS_KARTU,CLTB.AUTH_STAT FROM CLTB_ACCOUNT_MASTER CLTB JOIN CLTM_PRODUCT CLTM"
					    +" ON CLTB.PRODUCT_CODE = CLTM.PRODUCT_CODE JOIN STTM_CUST_PERSONAL STTM ON CLTB.CUSTOMER_ID = STTM.CUSTOMER_NO JOIN STTM_CUSTOMER CUST"
					    +" ON CLTB.CUSTOMER_ID = CUST.CUSTOMER_NO JOIN (SELECT ACCOUNT_NUMBER,COMPONENT_NAME,DR_PROD_AC FROM CLTB_ACCOUNT_COMPONENTS "
					    +" WHERE COMPONENT_NAME = 'PRINCIPAL') COMP ON CLTB.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER WHERE CLTB.AUTH_STAT = 'A'";
			}
					
			if (!"".equals(no_kartu) && no_kartu != null){
				sql += " AND CLTB.ACCOUNT_NUMBER = ? ";
			} if (!"".equals(no_rek) && no_rek != null){
				sql += " AND COMP.DR_PROD_AC = ? ";
			} if (!"".equals(tanggal) && tanggal != null){
				sql += " AND TO_CHAR(STTM.DATE_OF_BIRTH,'DD-MM-RRRR') = '" + tanggal + "'";
			} if (!"".equals(nama) && nama != null){
				sql += " AND CUST.CUSTOMER_NAME1 LIKE ?";
			}
			
			sql += " ) WHERE NO >= ? AND NO <= ? ORDER BY TANGGAL_PENCAIRAN";
			System.out.println("sql salaMuamalat :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
		
			if (!"".equals(no_kartu) && no_kartu != null){
				stat.setString(i++, no_kartu);
			}
			if (!"".equals(no_rek) && no_rek != null){
				stat.setString(i++, no_rek);
			}
			if (!"".equals(nama) && nama != null){
				stat.setString(i++, "%" + nama.toUpperCase() + "%" );
			}
			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			rs = stat.executeQuery();
			while(rs.next()){
				salam  = new salamMuamalat();
				salam.setNama_nasabah(rs.getString("NAMA_NASABAH"));
				if (rs.getString("TANGGAL_LAHIR") != null) {
					salam.setLahir(rs.getString("TANGGAL_LAHIR").substring(0, 10));
				} else {
					salam.setLahir(rs.getString("TANGGAL_LAHIR") == null ? "-" : rs.getString("TANGGAL_LAHIR"));
				}
				salam.setNo_kartu(rs.getString("NO_KARTU"));
				salam.setNo_rek_angsuran(rs.getString("NO_REKENING_ANGSURAN"));
				salam.setNo_cif(rs.getString("NO_CIF"));
				salam.setProduk_kode(rs.getString("PRODUK_KODE"));
				salam.setProduk_kategori(rs.getString("PRODUK_KATEGORI"));
				if (rs.getString("TANGGAL_PENCAIRAN") != null) {
					salam.setTanggal_pencairan(rs.getString("TANGGAL_PENCAIRAN").substring(0, 10));
				} else {
					salam.setTanggal_pencairan(rs.getString("TANGGAL_PENCAIRAN"));
				}
				if (rs.getString("TANGGAL_JATUH_TEMPO") != null) {
					salam.setTanggal_jatuh_tempo(rs.getString("TANGGAL_JATUH_TEMPO").substring(0, 10));
				} else {
					salam.setTanggal_jatuh_tempo(rs.getString("TANGGAL_JATUH_TEMPO"));
				}
				salam.setStatus_aktif(rs.getString("STATUS_KARTU"));
				lhistSalam.add(salam);
			}
		} catch (Exception e) {
			log.error("getSalamMuamalat :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lhistSalam;
	}
	
	public salamMuamalat getDetailSalam(String acc_no){
		List lhistSalam = new ArrayList();
		salamMuamalat salam = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sqlTanggal = "";
		String sql = "";
		String sqlPiutang = "";
		String sqlPembiayaan = "";
		String sqlOs = "";
		String sqlDenda = "";
		String sqlTunggakanDenda = "";
		String sqlTunggakanAngsuran = "";
		String sqlBranch = "";
		String branch = "";
		String kode = "";
		String produk = "";
		String tglCair = "";
		String sqlAngsuran = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sqlTanggal = "SELECT ACCOUNT_NUMBER,FIELD_DATE_1 AS TANGGAL_PENCAIRAN "
					+ " FROM CLTB_ACCOUNT_MASTER WHERE ACCOUNT_NUMBER = ?";
			
			stat = conn.prepareStatement(sqlTanggal);
			stat.setString(1, acc_no.trim());
			rs = stat.executeQuery();
			if (rs.next()){
				salam = new salamMuamalat();
				salam.setTanggal_pencairan(rs.getString("TANGGAL_PENCAIRAN"));
				tglCair = rs.getString("TANGGAL_PENCAIRAN");
			}
			
			if (!"".equals(tglCair) && tglCair != null) {
				sql =" SELECT DISTINCT CUST.CUSTOMER_NAME1 AS NAMA_NASABAH,STTM.DATE_OF_BIRTH AS TANGGAL_LAHIR,CLTB.ACCOUNT_NUMBER AS NO_KARTU, CLTB.BRANCH_CODE,COMP.DR_PROD_AC"
					    +" AS NO_REKENING_ANGSURAN,CLTB.CUSTOMER_ID AS NO_CIF,CLTB.PRODUCT_CODE AS PRODUK_KODE,CLTB.NO_OF_INSTALLMENTS AS JANGKA_WAKTU,CLTB.PRODUCT_CATEGORY"
					    +" AS PRODUK_KATEGORI,CLTM.PRODUCT_DESC AS NAMA_PRODUK,CLTB.FIELD_DATE_1 AS TANGGAL_PENCAIRAN,CLTB.FIELD_CHAR_8 AS KODE_MARKETING, MAR.LOV_DESC"
					    +" AS NAMA_MARKETING, SUBSTR(CLTM.PRODUCT_CODE,1,1) AS PRODUK_KODE_TYPE,CLTB.MATURITY_DATE AS TANGGAL_JATUH_TEMPO,CASE WHEN CLTB.ACCOUNT_STATUS = 'A' "
					    +" THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE' WHEN CLTB.ACCOUNT_STATUS = 'V' THEN 'REVESAL' ELSE 'HOLD' END STATUS_KARTU,CLTB.CURRENCY"
					    +" FROM CLTB_ACCOUNT_MASTER CLTB JOIN CLTM_PRODUCT CLTM ON CLTB.PRODUCT_CODE = CLTM.PRODUCT_CODE JOIN STTM_CUST_PERSONAL STTM"
					    +" ON CLTB.CUSTOMER_ID = STTM.CUSTOMER_NO JOIN STTM_CUSTOMER CUST ON CLTB.CUSTOMER_ID = CUST.CUSTOMER_NO "
					    +" JOIN (SELECT ACCOUNT_NUMBER,COMPONENT_NAME,DR_PROD_AC FROM CLTB_ACCOUNT_COMPONENTS WHERE COMPONENT_NAME = 'PRINCIPAL')"
					    +" COMP ON CLTB.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER"
					    +" JOIN UDTMS_LOV MAR ON CLTB.FIELD_CHAR_8 = MAR.LOV WHERE CLTB.ACCOUNT_NUMBER = ? ";
			} else {
				sql =" SELECT DISTINCT CUST.CUSTOMER_NAME1 AS NAMA_NASABAH,STTM.DATE_OF_BIRTH AS TANGGAL_LAHIR,CLTB.ACCOUNT_NUMBER AS NO_KARTU, CLTB.BRANCH_CODE,COMP.DR_PROD_AC"
					    +" AS NO_REKENING_ANGSURAN,CLTB.CUSTOMER_ID AS NO_CIF,CLTB.PRODUCT_CODE AS PRODUK_KODE,CLTB.NO_OF_INSTALLMENTS AS JANGKA_WAKTU,CLTB.PRODUCT_CATEGORY"
					    +" AS PRODUK_KATEGORI,CLTM.PRODUCT_DESC AS NAMA_PRODUK,CLTB.BOOK_DATE AS TANGGAL_PENCAIRAN,CLTB.FIELD_CHAR_8 AS KODE_MARKETING, MAR.LOV_DESC"
					    +" AS NAMA_MARKETING, SUBSTR(CLTM.PRODUCT_CODE,1,1) AS PRODUK_KODE_TYPE,CLTB.MATURITY_DATE AS TANGGAL_JATUH_TEMPO,CASE WHEN CLTB.ACCOUNT_STATUS = 'A' "
					    +" THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE' WHEN CLTB.ACCOUNT_STATUS = 'V' THEN 'REVESAL' ELSE 'HOLD' END STATUS_KARTU,CLTB.CURRENCY"
					    +" FROM CLTB_ACCOUNT_MASTER CLTB JOIN CLTM_PRODUCT CLTM ON CLTB.PRODUCT_CODE = CLTM.PRODUCT_CODE JOIN STTM_CUST_PERSONAL STTM"
					    +" ON CLTB.CUSTOMER_ID = STTM.CUSTOMER_NO JOIN STTM_CUSTOMER CUST ON CLTB.CUSTOMER_ID = CUST.CUSTOMER_NO "
					    +" JOIN (SELECT ACCOUNT_NUMBER,COMPONENT_NAME,DR_PROD_AC FROM CLTB_ACCOUNT_COMPONENTS WHERE COMPONENT_NAME = 'PRINCIPAL')"
					    +" COMP ON CLTB.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER"
					    +" JOIN UDTMS_LOV MAR ON CLTB.FIELD_CHAR_8 = MAR.LOV WHERE CLTB.ACCOUNT_NUMBER = ? ";
			}
			
			System.out.println("SQL getDetailSalam " + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc_no.trim());
			rs = stat.executeQuery();
			while(rs.next()){
				salam  = new salamMuamalat();
				salam.setNama_nasabah(rs.getString("NAMA_NASABAH"));
				if (rs.getString("TANGGAL_LAHIR") != null) {
					salam.setLahir(rs.getString("TANGGAL_LAHIR").substring(0, 10));
				} else {
					salam.setLahir(rs.getString("TANGGAL_LAHIR") == null ? "-" : rs.getString("TANGGAL_LAHIR"));
				}
				salam.setNo_kartu(rs.getString("NO_KARTU"));
				salam.setNo_rek_angsuran(rs.getString("NO_REKENING_ANGSURAN"));
				salam.setNo_cif(rs.getString("NO_CIF"));
				salam.setProduk_kode(rs.getString("PRODUK_KODE"));
				salam.setProduk_kategori(rs.getString("PRODUK_KATEGORI"));
				salam.setNama_produk(rs.getString("NAMA_PRODUK"));
				if (rs.getString("TANGGAL_PENCAIRAN") != null) {
					salam.setTanggal_pencairan(rs.getString("TANGGAL_PENCAIRAN").substring(0, 10));
				} else {
					salam.setTanggal_pencairan(rs.getString("TANGGAL_PENCAIRAN"));
				}
				if (rs.getString("TANGGAL_JATUH_TEMPO") != null) {
					salam.setTanggal_jatuh_tempo(rs.getString("TANGGAL_JATUH_TEMPO").substring(0, 10));
				} else {
					salam.setTanggal_jatuh_tempo(rs.getString("TANGGAL_JATUH_TEMPO"));
				}
				salam.setStatus_aktif(rs.getString("STATUS_KARTU"));
				salam.setJangka_waktu(rs.getString("JANGKA_WAKTU"));
				salam.setProduk_kode_type(rs.getString("PRODUK_KODE_TYPE"));
				salam.setKode_marketing(rs.getString("KODE_MARKETING"));
				salam.setNama_marketing(rs.getString("NAMA_MARKETING"));
				salam.setCurrency(rs.getString("CURRENCY"));
				branch = rs.getString("BRANCH_CODE");
				kode = rs.getString("PRODUK_KODE").substring(1, 1);
				produk = rs.getString("PRODUK_KODE");
				lhistSalam.add(salam);
			}

			if ("A".equals(salam.getProduk_kode_type())){
				System.out.println("OUTSTANDING MURABAHAH PIUTANG :");
				sqlOs = "SELECT ACCOUNT_NUMBER,BRANCH_CODE,SUM(OUTSTANDING) AS PEMBIAYAAN"
						+ " FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('PRINCIPAL','PROFIT')"
						+ " AND ACCOUNT_NUMBER = ? GROUP BY ACCOUNT_NUMBER,BRANCH_CODE";
			} else if ("F".equals(salam.getProduk_kode_type())){
				System.out.println("OUTSTANDING ISTISHNA PIUTANG :");
				sqlOs = "SELECT ACCOUNT_NUMBER,BRANCH_CODE,SUM(OUTSTANDING) AS PEMBIAYAAN"
						+ " FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('PRINCIPAL','PROFIT')"
						+ " AND ACCOUNT_NUMBER = ? GROUP BY ACCOUNT_NUMBER,BRANCH_CODE";
			} else if ("B".equals(salam.getProduk_kode_type())){
				System.out.println("OUTSTANDING MUDHARABA PEMBIAYAAN :");
				sqlOs = "SELECT ACCOUNT_NUMBER,BRANCH_CODE,OUTSTANDING AS PEMBIAYAAN"
						+ " FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('PRINCIPAL') AND ACCOUNT_NUMBER = ?";
			} else if ("C".equals(salam.getProduk_kode_type())){
				if ("C301".equals(produk) || "C302".equals(produk) || "C303".equals(produk) || "C308".equals(produk)){
				    System.out.println("OUTSTANDING MMQ PEMBIAYAAN :");
					sqlOs = "SELECT ACCOUNT_NUMBER,BRANCH_CODE,SUM(OUTSTANDING) AS PEMBIAYAAN"
							+ " FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('PRINCIPAL','PROFIT')"
							+ " AND ACCOUNT_NUMBER = ? GROUP BY ACCOUNT_NUMBER,BRANCH_CODE";
				}else {
					System.out.println("OUTSTANDING MUSYARAKAH PEMBIAYAAN :");
					sqlOs = "SELECT ACCOUNT_NUMBER,BRANCH_CODE,NVL(OUTSTANDING,'0') AS PEMBIAYAAN"
						+ " FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('PRINCIPAL') AND ACCOUNT_NUMBER = ?";
				}
			} else if ("D".equals(salam.getProduk_kode_type())){
				System.out.println("OUTSTANDING QARDH PEMBIAYAAN :");
				sqlOs = "SELECT ACCOUNT_NUMBER,BRANCH_CODE,NVL(OUTSTANDING,'0') AS PEMBIAYAAN"
						+ " FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('PRINCIPAL') AND ACCOUNT_NUMBER = ?";
			} else if ("E".equals(salam.getProduk_kode_type())){
				System.out.println("OUTSTANDING IJARAH PEMBIAYAAN :");
				sqlOs = "SELECT ACCOUNT_NUMBER,BRANCH_CODE,NVL(OUTSTANDING,'0') AS PEMBIAYAAN"
						+ " FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('PROFIT') AND ACCOUNT_NUMBER = ?";
			}
			
			System.out.println("sql OUTSTANDING :" + sqlOs);
			stat = conn.prepareStatement(sqlOs);
			stat.setString(1, acc_no);
			rs = stat.executeQuery();
			if (rs.next()){
				salam.setPembiayaan((rs.getBigDecimal("PEMBIAYAAN") == null) ? new BigDecimal(0) : rs.getBigDecimal("PEMBIAYAAN"));
				System.out.println("OS Pembiayaan :" + rs.getString("PEMBIAYAAN"));
			}

			if ("LIQUIDATE".equals(salam.getStatus_aktif())){
				sqlDenda =" SELECT CALC.ACCOUNT_NUMBER,'0' AS DENDA"
						 +" FROM (SELECT DISTINCT ACCOUNT_NUMBER,"
						 +" BRANCH_CODE from CLTB_ACCOUNT_COMP_CALC) CALC LEFT JOIN(SELECT ACCOUNT_NUMBER,"
						 +" BRANCH_CODE,ACCR_TILL_DATE AS DENDA FROM CLTB_ACCOUNT_COMP_CALC"
						 +" WHERE COMPONENT_NAME IN ('DENDA')) CALC1 ON CALC.ACCOUNT_NUMBER = CALC1.ACCOUNT_NUMBER"
						 +" WHERE CALC.ACCOUNT_NUMBER = ?";
			} if ("ACTIVE".equals(salam.getStatus_aktif())){
				sqlDenda =" SELECT CALC.ACCOUNT_NUMBER,CASE WHEN CALC1.DENDA IS NULL THEN 0"
						 +" ELSE CALC1.DENDA END AS DENDA FROM (SELECT DISTINCT ACCOUNT_NUMBER,"
						 +" BRANCH_CODE from CLTB_ACCOUNT_COMP_CALC) CALC LEFT JOIN(SELECT ACCOUNT_NUMBER,"
						 +" BRANCH_CODE,ACCR_TILL_DATE AS DENDA FROM CLTB_ACCOUNT_COMP_CALC"
						 +" WHERE COMPONENT_NAME IN ('DENDA')) CALC1 ON CALC.ACCOUNT_NUMBER = CALC1.ACCOUNT_NUMBER"
						 +" WHERE CALC.ACCOUNT_NUMBER = ?";
			}
			
			System.out.println("sql denda :" + sqlDenda);
			stat = conn.prepareStatement(sqlDenda);
			stat.setString(1, acc_no.trim());
			rs = stat.executeQuery();
			if(rs.next()){
				salam.setDenda(rs.getBigDecimal("DENDA"));
				System.out.println("DENDA :" + rs.getBigDecimal("DENDA"));
			}
			
			sqlTunggakanDenda = " SELECT DISTINCT BAL.ACCOUNT_NUMBER,BAL.BRANCH_CODE, NVL(BAL1.TUNGGAKAN_DENDA,'0') AS TUNGGAKAN_DENDA"
					+" FROM CLVS_ACC_COMP_BALANCES BAL LEFT JOIN (SELECT ACCOUNT_NUMBER,BRANCH_CODE,"
					+" OUTSTANDING AS TUNGGAKAN_DENDA FROM CLVS_ACC_COMP_BALANCES WHERE COMPONENT_NAME IN ('DENDA'))BAL1"
					+" ON BAL.ACCOUNT_NUMBER = BAL1.ACCOUNT_NUMBER WHERE BAL.ACCOUNT_NUMBER = ?";
			
			System.out.println("sqlTunggakanDenda :" + sqlTunggakanDenda);
			stat = conn.prepareStatement(sqlTunggakanDenda);
			stat.setString(1, acc_no.trim());
			rs = stat.executeQuery();
			if (rs.next()){
				salam.setTunggakan_denda(rs.getBigDecimal("TUNGGAKAN_DENDA"));
			}
			
			if ("A".equals(salam.getProduk_kode_type()) || "B".equals(salam.getProduk_kode_type()) || "C".equals(salam.getProduk_kode_type())
					|| "D".equals(salam.getProduk_kode_type()) || "F".equals(salam.getProduk_kode_type())) {
				System.out.println("TUNGGAKAN ANGSURAN A,B,C,D,F");
				sqlTunggakanAngsuran = "select margin.account_number,margin.branch_code,margin.tunggakan_margin,pokok.tunggakan_pokok,"
						+" margin.tunggakan_margin + pokok.tunggakan_pokok as tunggakan_angsuran from(select account_number,branch_code, "
						+" overdue as tunggakan_margin,outstanding as osmargin from(select account_number,branch_code,component_name,"
						+" sum(overdue) as overdue,sum(expected + overdue) as outstanding from(select account_number,a.branch_code,"
						+" component_name,0 as expected,0 as overdue,settlement_ccy from cltb_Account_Schedules a,sttm_dates b"
						+" WHERE a.branch_code = b.branch_code and component_name = 'PROFIT' AND SCHEDULE_DUE_DATE > b.today"
						+" AND COALESCE(SCHEDULE_FLAG, 'X') <> 'M' group by account_number,a.branch_code,component_name,settlement_ccy"
						+" union select account_number,a.branch_code,component_name,0 as expected,(sum(COALESCE(amount_due, 0) - COALESCE(amount_settled, 0))) as overdue,"
						+" settlement_ccy from cltb_Account_Schedules a,sttm_dates b WHERE a.branch_code = b.branch_code and component_name = 'PROFIT'"
						+" AND SCHEDULE_DUE_DATE <= b.today AND COALESCE(SCHEDULE_FLAG, 'X') <> 'M' group by account_number,a.branch_code,component_name,settlement_ccy"
						+" union select account_number,a.branch_code,component_name,(sum(COALESCE(amount_due, 0) - COALESCE(amount_settled, 0))) as expected,"
						+" 0 as overdue,settlement_ccy from cltb_Account_Schedules a,sttm_dates b WHERE a.branch_code = b.branch_code and component_name = 'PROFIT'"
						+" AND SCHEDULE_DUE_DATE > b.today AND COALESCE(SCHEDULE_FLAG, 'X') <> 'M' group by account_number,a.branch_code,component_name,settlement_ccy)"
						+" group by account_number,branch_code,component_name,settlement_ccy)) margin join(select account_number,branch_code,overdue as tunggakan_pokok,"
						+" outstanding as ospokok from (select account_number,branch_code,component_name,sum(overdue) as overdue,sum(expected + overdue) as outstanding"
						+" from (select account_number,a.branch_code,component_name,0 as expected,0 as overdue,settlement_ccy from cltb_Account_Schedules a,sttm_dates b"
						+" WHERE a.branch_code = b.branch_code and component_name = 'PRINCIPAL' AND SCHEDULE_DUE_DATE > b.today AND COALESCE(SCHEDULE_FLAG, 'X') <> 'M'"
						+" group by account_number,a.branch_code,component_name,settlement_ccy union select account_number,a.branch_code,component_name,0 as expected,"
						+" (sum(COALESCE(amount_due, 0) - COALESCE(amount_settled, 0))) as overdue,settlement_ccy from cltb_Account_Schedules a,sttm_dates b"
						+" WHERE a.branch_code = b.branch_code and component_name = 'PRINCIPAL' AND SCHEDULE_DUE_DATE <= b.today AND COALESCE(SCHEDULE_FLAG, 'X') <> 'M'"
						+" group by account_number,a.branch_code,component_name,settlement_ccy union select account_number,a.branch_code,component_name,"
						+" (sum(COALESCE(amount_due, 0) - COALESCE(amount_settled, 0))) as expected,0 as overdue,settlement_ccy"
						+" from cltb_Account_Schedules a,sttm_dates b WHERE a.branch_code = b.branch_code AND component_name = 'PRINCIPAL' AND SCHEDULE_DUE_DATE > b.today"
						+" AND COALESCE(SCHEDULE_FLAG, 'X') <> 'M' group by account_number,a.branch_code,component_name,settlement_ccy)"
						+" group by account_number,branch_code,component_name,settlement_ccy)) pokok"
						+" on margin.account_number = pokok.account_number where margin.account_number = ?";
			} else {
				System.out.println("TUNGGAKAN ANGSURAN E");
				sqlTunggakanAngsuran = " SELECT account_number,branch_code,tunggakan_margin as tunggakan_angsuran,outstanding AS osmargin FROM"
						+ " (SELECT account_number,branch_code,component_name,SUM(overdue) AS tunggakan_margin,SUM(expected + overdue) AS outstanding FROM"
						+ " (SELECT account_number,a.branch_code,component_name,0 AS expected,0 AS overdue,settlement_ccy FROM cltb_Account_Schedules a,"
						+ " sttm_dates b WHERE a.branch_code = b.branch_code AND component_name = 'PROFIT' AND SCHEDULE_DUE_DATE > b.today AND COALESCE"
						+ " (SCHEDULE_FLAG, 'X') <> 'M' GROUP BY account_number,a.branch_code,component_name,settlement_ccy UNION SELECT account_number,"
						+ " a.branch_code,component_name,0 AS expected,(SUM(COALESCE(amount_due,0)-COALESCE(amount_settled, 0))) AS overdue,settlement_ccy"
						+ " FROM cltb_Account_Schedules a, sttm_dates b WHERE a.branch_code = b.branch_code AND component_name = 'PROFIT' AND SCHEDULE_DUE_DATE"
						+ " <= b.today AND COALESCE (SCHEDULE_FLAG, 'X') <> 'M' GROUP BY account_number,a.branch_code,component_name,settlement_ccy"
						+ " UNION SELECT account_number,a.branch_code,component_name,(SUM(COALESCE(amount_due,0)-COALESCE(amount_settled,0))) AS expected,"
						+ " 0 AS overdue,settlement_ccy FROM cltb_Account_Schedules a, sttm_dates b WHERE a.branch_code = b.branch_code AND component_name = 'PROFIT'"
						+ " AND SCHEDULE_DUE_DATE > b.today AND COALESCE (SCHEDULE_FLAG, 'X') <> 'M' GROUP BY account_number,a.branch_code,component_name,"
						+ " settlement_ccy)where account_number = ? GROUP BY account_number,branch_code,component_name,settlement_ccy)";
			}
		
			System.out.println("sqlTunggakanAngsuran :" + sqlTunggakanAngsuran);
			stat = conn.prepareStatement(sqlTunggakanAngsuran);
			stat.setString(1, acc_no);
			rs = stat.executeQuery();
			if (rs.next()){
				salam.setTunggakan_angsuran(rs.getBigDecimal("tunggakan_angsuran"));
			}
			
			sqlBranch = "SELECT BRANCH_CODE||' - '||BRANCH_NAME AS BRANCH,BRANCH_ADDR1 AS ALAMAT,SUBSTR(BRANCH_ADDR2,10,50) AS TELP"
					+ " FROM STTM_BRANCH WHERE BRANCH_CODE = ?";
			
			System.out.println("sqlBranch :" + sqlBranch);
			stat = conn.prepareStatement(sqlBranch);
			stat.setString(1, branch);
			rs = stat.executeQuery();
			if(rs.next()){
				salam.setBranch(rs.getString("BRANCH"));
				salam.setAlamat(rs.getString("ALAMAT"));
				salam.setTelp(rs.getString("TELP"));
			}
		} catch (Exception e) {
			log.error("getDetailSalam :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return salam;
	}
	
	public salamMuamalat getAngsuran (String noKartu, String period){
    	List lhistSalam = new ArrayList();
    	salamMuamalat salam = null;
    	Connection conn = null;
    	PreparedStatement stat = null;
    	ResultSet rs = null;
    	String sql = "";
    	String sqlAngsuranTerakhir = "";
    	
    	try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
									
			sql = " SELECT DISTINCT CLTB.ACCOUNT_NUMBER,JANGKA_WAKTU.SISA_JANGKA_WAKTU,JANGKA_WAKTU.TGL_ANGSUR_TERAKHIR,JANGKA_WAKTU.TGL_ANGSUR_SELANJUTNYA,"
					+ " JATUH_TEMPO.TANGGAL_JATUH_TEMPO,JATUH_TEMPO.KODE,NVL(ANGSURAN.TOTAL_ANGSURAN,'0') AS TOTAL_ANGSURAN,JATUH_TEMPO.STATUS_KARTU"
					+ " FROM CLTB_ACCOUNT_SCHEDULES CLTB JOIN (SELECT DISTINCT X.ACCOUNT_NUMBER, CASE WHEN X.SCHEDULE_NO IS NULL THEN 0 "
					+ " ELSE (X.JANGKA_WAKTU - X.SCHEDULE_NO) END AS SISA_JANGKA_WAKTU,X.TGL_AWAL_ANGSUR,X.TGL_ANGSUR_TERAKHIR,X.TGL_ANGSUR_SELANJUTNYA"
					+ " FROM (SELECT A.ACCOUNT_NUMBER,A.SCHEDULE_NO,TO_CHAR (A.TGL_AWAL_ANGSUR, 'DD/MM/YYYY') TGL_AWAL_ANGSUR,"
					+ " (CASE WHEN SUBSTR (A.ACCOUNT_NUMBER, 4, 1) = 'E' THEN TO_CHAR (A.TGL_ANGSUR_TERAKHIR_PRO,'DD-MM-YYYY')"
					+ " WHEN SUBSTR (A.ACCOUNT_NUMBER, 4, 1) = 'D' THEN  TO_CHAR (A.TGL_ANGSUR_TERAKHIR_PRI,'DD-MM-YYYY')"
					+ " ELSE (CASE WHEN COALESCE (A.TGL_ANGSUR_TERAKHIR_PRI, TO_DATE ('01-12-1900', 'DD-MM-YYYY')) >"
					+ " COALESCE (A.TGL_ANGSUR_TERAKHIR_PRO,TO_DATE ('01-12-1900', 'DD-MM-YYYY')) THEN TO_CHAR (A.TGL_ANGSUR_TERAKHIR_PRO,'DD-MM-YYYY') "
					+ " ELSE TO_CHAR (A.TGL_ANGSUR_TERAKHIR_PRI,'DD-MM-YYYY') END) END) AS TGL_ANGSUR_TERAKHIR,"
					+ " (CASE WHEN SUBSTR (A.ACCOUNT_NUMBER, 4, 1) = 'E' THEN TO_CHAR (A.TGL_ANGSUR_SLNJT_PRO, 'DD-MM-YYYY')"
					+ " WHEN SUBSTR (A.ACCOUNT_NUMBER, 4, 1) = 'D' THEN TO_CHAR (A.TGL_ANGSUR_SLNJT_PRI, 'DD-MM-YYYY')"
					+ " ELSE (CASE WHEN COALESCE (A.TGL_ANGSUR_SLNJT_PRI, TO_DATE ('31-12-3011', 'DD-MM-YYYY')) <"
					+ " COALESCE (A.TGL_ANGSUR_SLNJT_PRO,TO_DATE('31-12-3011','DD-MM-YYYY')) THEN TO_CHAR (A.TGL_ANGSUR_SLNJT_PRI,'DD-MM-YYYY')"
					+ " ELSE TO_CHAR (A.TGL_ANGSUR_SLNJT_PRO, 'DD-MM-YYYY') END) END) AS TGL_ANGSUR_SELANJUTNYA,D.NO_OF_INSTALLMENTS AS JANGKA_WAKTU"
					+ " FROM (SELECT ACCOUNT_NUMBER,MIN (CASE WHEN AMOUNT_SETTLED = 0 AND AMOUNT_DUE <> 0 THEN SCHEDULE_NO ELSE NULL END) SCHEDULE_NO,"
					+ " MIN (SCHEDULE_DUE_DATE) TGL_AWAL_ANGSUR, MAX(CASE WHEN AMOUNT_SETTLED = AMOUNT_DUE AND COMPONENT_NAME = 'PRINCIPAL' THEN SCHEDULE_DUE_DATE"
					+ " ELSE NULL END) TGL_ANGSUR_TERAKHIR_PRI,MAX(CASE WHEN AMOUNT_SETTLED = AMOUNT_DUE AND COMPONENT_NAME = 'PROFIT' THEN SCHEDULE_DUE_DATE"
					+ " ELSE NULL END) TGL_ANGSUR_TERAKHIR_PRO,MIN(CASE WHEN AMOUNT_SETTLED <> AMOUNT_DUE AND AMOUNT_DUE <> 0 THEN SCHEDULE_DUE_DATE "
					+ " ELSE NULL END) TGL_ANGSUR_SELANJUTNYAH,MIN(CASE WHEN AMOUNT_SETTLED <> AMOUNT_DUE AND AMOUNT_DUE <> 0 AND COMPONENT_NAME = 'PRINCIPAL' THEN SCHEDULE_DUE_DATE"
					+ " ELSE NULL END) TGL_ANGSUR_SLNJT_PRI,MIN(CASE WHEN AMOUNT_SETTLED <> AMOUNT_DUE AND AMOUNT_DUE <> 0  AND COMPONENT_NAME = 'PROFIT' THEN SCHEDULE_DUE_DATE"
					+ " ELSE NULL END) TGL_ANGSUR_SLNJT_PRO FROM (SELECT COMPONENT_NAME,ACCOUNT_NUMBER,BRANCH_CODE,SCHEDULE_ST_DATE,SCHEDULE_DUE_DATE,SCHEDULE_NO,"
					+ " AMOUNT_DUE,AMOUNT_SETTLED FROM CLTB_ACCOUNT_SCHEDULES WHERE COMPONENT_NAME IN ('PRINCIPAL', 'PROFIT'))GROUP BY ACCOUNT_NUMBER) A,"
					+ " CLTB_ACCOUNT_SCHEDULES B, CLTB_ACCOUNT_SCHEDULES C, CLTB_ACCOUNT_MASTER D WHERE A.ACCOUNT_NUMBER = B.ACCOUNT_NUMBER(+) "
					+ " AND A.ACCOUNT_NUMBER = D.ACCOUNT_NUMBER(+) AND A.ACCOUNT_NUMBER = C.ACCOUNT_NUMBER(+) AND B.COMPONENT_NAME(+) = 'PRINCIPAL' "
					+ " AND C.COMPONENT_NAME(+) = 'PROFIT' ORDER BY A.ACCOUNT_NUMBER) X )JANGKA_WAKTU ON CLTB.ACCOUNT_NUMBER  = JANGKA_WAKTU.ACCOUNT_NUMBER"
					+ " JOIN (SELECT DISTINCT CLTB.ACCOUNT_NUMBER,NVL (SUBSTR (SCHEDULE_DUE_DATE, 1, 2), 'SUDAH LUNAS') AS TANGGAL_JATUH_TEMPO,"
					+ " TO_CHAR (SCHEDULE_DUE_DATE, 'DD-MM-YYYY') AS SCHEDULE_DUE_DATE,SUBSTR (CLTB.PRODUCT_CODE, 0, 1) AS KODE,"
					+ " CASE WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE' WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'H' THEN 'HOLD'"
					+ " ELSE 'REVERSAL' END AS STATUS_KARTU FROM  CLTB_ACCOUNT_MASTER CLTB LEFT JOIN (SELECT ACCOUNT_NUMBER,SCHEDULE_DUE_DATE"
					+ " FROM CLTB_ACCOUNT_SCHEDULES SCH WHERE TO_CHAR (SCHEDULE_DUE_DATE, 'rrrrmm') = ?) SCH ON CLTB.ACCOUNT_NUMBER = SCH.ACCOUNT_NUMBER) JATUH_TEMPO"
					+ " ON CLTB.ACCOUNT_NUMBER = JATUH_TEMPO.ACCOUNT_NUMBER JOIN (SELECT CLTB.ACCOUNT_NUMBER,NVL(SCH.TOTAL_ANGSURAN,'0') AS TOTAL_ANGSURAN "
					+ " FROM CLTB_ACCOUNT_MASTER CLTB LEFT JOIN (SELECT ACCOUNT_NUMBER,TO_CHAR(SCHEDULE_DUE_DATE,'DD-MM-YYYY') AS SCHEDULE_DUE_DATE,"
					+ " NVL(SUM(PRINCIPAL),'0') AS PRINCIPAL,NVL(SUM(PROFIT),'0') AS PROFIT,NVL(SUM(PRINCIPAL),'0') + NVL(SUM(PROFIT),'0') AS TOTAL_ANGSURAN"
					+ " FROM (SELECT ACCOUNT_NUMBER,SCHEDULE_DUE_DATE,DECODE (COMPONENT_NAME, 'PRINCIPAL', AMOUNT_DUE) AS PRINCIPAL,"
					+ " DECODE (COMPONENT_NAME,'PROFIT', AMOUNT_DUE) AS PROFIT FROM CLTB_ACCOUNT_SCHEDULES WHERE COMPONENT_NAME IN ('PRINCIPAL','PROFIT'))"
					+ " WHERE TO_CHAR (SCHEDULE_DUE_DATE,'rrrrmm') = ? GROUP BY ACCOUNT_NUMBER, SCHEDULE_DUE_DATE) SCH "
					+ " ON CLTB.ACCOUNT_NUMBER = SCH.ACCOUNT_NUMBER) ANGSURAN ON CLTB.ACCOUNT_NUMBER = ANGSURAN.ACCOUNT_NUMBER "
					+ " WHERE CLTB.ACCOUNT_NUMBER = ? ";
			
				stat = conn.prepareStatement(sql);
				stat.setString(1, period);
				stat.setString(2, period);
				stat.setString(3, noKartu);
				rs = stat.executeQuery();
				if (rs.next()){
					salam = new salamMuamalat();
					if (rs.getString("TGL_ANGSUR_SELANJUTNYA") != null) {
						salam.setTanggal_angsur_selanjutnya(rs.getString("TGL_ANGSUR_SELANJUTNYA"));
					} else {
						salam.setTanggal_angsur_selanjutnya(rs.getString("TGL_ANGSUR_SELANJUTNYA") == null ? "-" : rs.getString("TGL_ANGSUR_SELANJUTNYA"));
					}
					if (rs.getString("TGL_ANGSUR_TERAKHIR") != null) {
						salam.setTanggal_angsuran_terakhir(rs.getString("TGL_ANGSUR_TERAKHIR"));
					} else {
						salam.setTanggal_angsuran_terakhir(rs.getString("TGL_ANGSUR_TERAKHIR") == null ? "-" : rs.getString("TGL_ANGSUR_TERAKHIR"));
					}
					salam.setSisa_angsuran(rs.getString("TANGGAL_JATUH_TEMPO"));
					salam.setStatus_aktif(rs.getString("STATUS_KARTU"));
					salam.setTanggal_jatuh_tempo_angsuran(rs.getString("TANGGAL_JATUH_TEMPO"));
					salam.setProduk_kode_type(rs.getString("KODE"));
					salam.setTotal_angsuran(rs.getBigDecimal("TOTAL_ANGSURAN"));
					System.out.println("total angsuran :" + rs.getString("TOTAL_ANGSURAN"));
				}
				System.out.println("sql salam :" + sql);
				System.out.println("STATUS KARTU :" + salam.getStatus_aktif());
				
				if ("LIQUIDATE".equals(salam.getStatus_aktif())){
					System.out.println("STATUS KARTU LIQUIDATE ANGSURAN");
					sqlAngsuranTerakhir = " SELECT CLTB.ACCOUNT_NUMBER,NVL(SCH.ANGSURAN_TERAKHIR,'0') AS ANGSURAN_TERAKHIR"
							+" FROM CLTB_ACCOUNT_MASTER CLTB LEFT JOIN (SELECT ACCOUNT_NUMBER,NVL(SUM (AMOUNT_SETTLED), 0) AS ANGSURAN_TERAKHIR "
							+" FROM cltb_account_schedules WHERE COMPONENT_NAME IN ('PRINCIPAL', 'PROFIT') "
							+" AND TO_CHAR (SCHEDULE_DUE_DATE, 'rrrrmm') = ? GROUP BY ACCOUNT_NUMBER) SCH"
							+" ON CLTB.ACCOUNT_NUMBER = SCH.ACCOUNT_NUMBER WHERE CLTB.ACCOUNT_NUMBER = ?";
				} else if ("ACTIVE".equals(salam.getStatus_aktif())){
					if ("A".equals(salam.getProduk_kode_type()) || "B".equals(salam.getProduk_kode_type()) || "C".equals(salam.getProduk_kode_type())
							|| "D".equals(salam.getProduk_kode_type()) || "F".equals(salam.getProduk_kode_type())) {
						System.out.println("STATUS KARTU ACTIVE ANGSURAN DAN KODE PRODUK A,B,C,D,F");
//						sqlAngsuranTerakhir = "select ACCOUNT_NUMBER,nvl(sum(amount_settled),0) as angsuran_terakhir"
//								+ " from cltb_account_schedules where component_name in ('PROFIT','PRINCIPAL')"
//								+ " and to_char(schedule_due_date,'rrrrmm') = ? and ACCOUNT_NUMBER = ? group by ACCOUNT_NUMBER";
						sqlAngsuranTerakhir = "SELECT DISTINCT CLTB.ACCOUNT_NUMBER,NVL(ANGS.ANGSURAN_TERAKHIR,0) AS ANGSURAN_TERAKHIR"
								+ " FROM CLTB_ACCOUNT_SCHEDULES CLTB LEFT JOIN (SELECT ACCOUNT_NUMBER, "
								+ " NVL (SUM (amount_settled), 0) AS ANGSURAN_TERAKHIR FROM CLTB_ACCOUNT_SCHEDULES "
								+ " WHERE COMPONENT_NAME IN ('PROFIT','PRINCIPAL') AND TO_CHAR (schedule_due_date, 'rrrrmm') = ?"
								+ " GROUP BY ACCOUNT_NUMBER) ANGS ON CLTB.ACCOUNT_NUMBER = ANGS.ACCOUNT_NUMBER"
								+ " WHERE CLTB.ACCOUNT_NUMBER = ?";
					} else {
						System.out.println("STATUS KARTU ACTIVE ANGSURAN DAN KODE PRODUK E");
						sqlAngsuranTerakhir = "SELECT sch.ACCOUNT_NUMBER,NVL(sch1.angsuran_terakhir,0) AS angsuran_terakhir"
								+ " FROM cltb_account_schedules sch LEFT JOIN (SELECT ACCOUNT_NUMBER,component_name,schedule_st_date,"
								+ " schedule_due_date, amount_settled AS angsuran_terakhir FROM cltb_account_schedules"
								+ " WHERE component_name IN ('PROFIT') AND TO_CHAR (schedule_due_date, 'rrrrmm') = ?) sch1"
								+ " ON sch.ACCOUNT_NUMBER = sch1.ACCOUNT_NUMBER WHERE sch.ACCOUNT_NUMBER = ? GROUP BY sch.account_number,sch1.angsuran_terakhir";
					}
				}
				
				System.out.println("SQL sqlAngsuranTerakhir :" + sqlAngsuranTerakhir);
				stat = conn.prepareStatement(sqlAngsuranTerakhir);
				stat.setString(1, period);
				stat.setString(2, noKartu);
				rs = stat.executeQuery();
				if (rs.next()){
					salam.setAngsuran_terakhir(rs.getBigDecimal("angsuran_terakhir"));
					System.out.println("angsuran_terakhir" + rs.getString("angsuran_terakhir"));
				}	
			
    	} catch (Exception e) {
			log.error("getAngsuran :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
    	return salam;
    }
	
	public int GetTotSalam (String no_kartu, String no_rek, String nama, String tanggal) {
		int tot = 0;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql =" SELECT COUNT (*) AS TOTAL FROM (SELECT DISTINCT NAMA_NASABAH,TANGGAL_LAHIR,NO_KARTU,NO_REKENING_ANGSURAN,NO_CIF,PRODUK_KODE,PRODUK_KATEGORI,TANGGAL_PENCAIRAN,"
					+" TANGGAL_JATUH_TEMPO,STATUS_KARTU, AUTH_STAT FROM (SELECT CUST.CUSTOMER_NAME1 AS NAMA_NASABAH,STTM.DATE_OF_BIRTH AS TANGGAL_LAHIR,"
					+" CLTB.ACCOUNT_NUMBER AS NO_KARTU,COMP.DR_PROD_AC AS NO_REKENING_ANGSURAN,CLTB.CUSTOMER_ID AS NO_CIF,CLTB.PRODUCT_CODE AS PRODUK_KODE,"
				    +" CLTB.PRODUCT_CATEGORY AS PRODUK_KATEGORI,CLTB.FIELD_DATE_1 AS TANGGAL_PENCAIRAN,CLTB.MATURITY_DATE AS TANGGAL_JATUH_TEMPO,"
				    +" CASE WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE' WHEN CLTB.ACCOUNT_STATUS = 'V'  "
				    +" THEN 'REVESAL' ELSE 'HOLD' END STATUS_KARTU,CLTB.AUTH_STAT FROM CLTB_ACCOUNT_MASTER CLTB JOIN CLTM_PRODUCT CLTM"
				    +" ON CLTB.PRODUCT_CODE = CLTM.PRODUCT_CODE JOIN STTM_CUST_PERSONAL STTM ON CLTB.CUSTOMER_ID = STTM.CUSTOMER_NO JOIN STTM_CUSTOMER CUST"
				    +" ON CLTB.CUSTOMER_ID = CUST.CUSTOMER_NO JOIN (SELECT ACCOUNT_NUMBER,COMPONENT_NAME,DR_PROD_AC FROM CLTB_ACCOUNT_COMPONENTS"
				    +" WHERE COMPONENT_NAME = 'PRINCIPAL') COMP ON CLTB.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER) WHERE AUTH_STAT = 'A'";
			
			if (!"".equals(no_kartu) && no_kartu != null){
				sql += " AND NO_KARTU = ? ";
			} if (!"".equals(no_rek) && no_rek != null){
				sql += " AND NO_REKENING_ANGSURAN = ? ";
			} if (!"".equals(tanggal) && tanggal != null){
				sql += " AND TO_CHAR(TANGGAL_LAHIR,'DD-MM-RRRR') = '" + tanggal + "'";
			} if (!"".equals(nama) && nama != null){
				sql += " AND NAMA_NASABAH LIKE ?";
			}
			
			sql += " ORDER BY TANGGAL_PENCAIRAN)";
			System.out.println("SQL TOTAL :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if (!"".equals(no_kartu) && no_kartu != null){
				stat.setString(i++, no_kartu);
			}
			if (!"".equals(no_rek) && no_rek != null){
				stat.setString(i++, no_rek);
			}
			if (!"".equals(nama) && nama != null){
				stat.setString(i++, "%" + nama.toUpperCase() + "%" );
			}
			rs = stat.executeQuery();
			if (rs.next()) {
				tot = rs.getInt("TOTAL");
				System.out.println("total" + tot);
			}
		} catch (Exception e) {
			log.error("GetTotSalam :" + e.getMessage());
		}  finally {
			closeConnDb(conn, stat, rs);
		}
		
		return tot;
	}
}
