/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.logging.SimpleFormatter;

import javax.print.DocFlavor.STRING;

import org.jboss.logging.Logger;

import com.ibm.db2.jcc.am.no;
import com.muamalat.reportmcb.entity.AvgBal;
import com.muamalat.reportmcb.entity.BatchMaster;
import com.muamalat.reportmcb.entity.Bukubesar;
import com.muamalat.reportmcb.entity.Customer;
import com.muamalat.reportmcb.entity.DetailBatch;
import com.muamalat.reportmcb.entity.Gl;
import com.muamalat.reportmcb.entity.Gltb_avgbal;
import com.muamalat.reportmcb.entity.HI;
import com.muamalat.reportmcb.entity.HistTransRek;
import com.muamalat.reportmcb.entity.Instruction;
import com.muamalat.reportmcb.entity.JurnalTodayDetail;
import com.muamalat.reportmcb.entity.LapPerubahanDataCif;
import com.muamalat.reportmcb.entity.LapTes;
import com.muamalat.reportmcb.entity.LaporanSPVTeller;
import com.muamalat.reportmcb.entity.LaporanSpvCs;
import com.muamalat.reportmcb.entity.Lsp;
import com.muamalat.reportmcb.entity.Nik;
import com.muamalat.reportmcb.entity.Pcode;
import com.muamalat.reportmcb.entity.Role;
import com.muamalat.reportmcb.entity.RoleMenu;
import com.muamalat.reportmcb.entity.RoleSubMenu;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.entity.SaldoRataRata;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.entity.User;
import com.muamalat.reportmcb.entity.UserAktifCabang;
import com.muamalat.reportmcb.entity.banking;
import com.muamalat.reportmcb.entity.cariTransaksiUserRef;
import com.muamalat.reportmcb.entity.cifIndividual;
import com.muamalat.reportmcb.entity.cifNonIndividual;
import com.muamalat.reportmcb.entity.kurs;
import com.muamalat.reportmcb.entity.salamMuamalat;
import com.muamalat.reportmcb.entity.saldoHold;
import com.muamalat.singleton.DatasourceEntry;
import com.opensymphony.xwork2.ActionSupport;

import oracle.sql.DATE;

/**
 * 
 * @author Utis
 */
public class SqlFunction {

	private static Logger log = Logger.getLogger(SqlFunction.class);
	private BigDecimal totMutDb;
	private BigDecimal totMutCb;
	private int jmlItmD;
	private int jmlItmC;
	private BigDecimal totD;
	private BigDecimal totC;
	private BigDecimal sld_akhir;
	private int jmlSaldoD;
	private int jmlSaldoC;
	private String code_trn;
	
	public String getCode_trn() {
		return code_trn;
	}

	public void setCode_trn(String code_trn) {
		this.code_trn = code_trn;
	}

	public int getJmlSaldoD() {
		return jmlSaldoD;
	}

	public void setJmlSaldoD(int jmlSaldoD) {
		this.jmlSaldoD = jmlSaldoD;
	}

	public int getJmlSaldoC() {
		return jmlSaldoC;
	}

	public void setJmlSaldoC(int jmlSaldoC) {
		this.jmlSaldoC = jmlSaldoC;
	}

	public int getJmlItmD() {
		return jmlItmD;
	}

	public void setJmlItmD(int jmlItmD) {
		this.jmlItmD = jmlItmD;
	}

	public int getJmlItmC() {
		return jmlItmC;
	}

	public void setJmlItmC(int jmlItmC) {
		this.jmlItmC = jmlItmC;
	}

	public BigDecimal getTotD() {
		return totD;
	}

	public void setTotD(BigDecimal totD) {
		this.totD = totD;
	}

	public BigDecimal getTotC() {
		return totC;
	}

	public void setTotC(BigDecimal totC) {
		this.totC = totC;
	}

	public void setTotMutDb(BigDecimal totMutDb) {
		this.totMutDb = totMutDb;
	}

	public void setTotMutCb(BigDecimal totMutCb) {
		this.totMutCb = totMutCb;
	}

	public BigDecimal getSld_akhir() {
		return sld_akhir;
	}

	public void setSld_akhir(BigDecimal sld_akhir) {
		this.sld_akhir = sld_akhir;
	}

	public void closeConnDb(Connection conn, PreparedStatement stat,
			ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}

//	public List getListBukubesar(String tgl1, String tgl2, String ac_no,
//		String brnch, String ccy, String cbsrekoto, BigDecimal saldo) {
//		List lbb = new ArrayList();
//		Bukubesar bb = null;
//		Connection conn = null;
//		PreparedStatement stat = null;
//		ResultSet rs = null;
//		String sql = "";
//
//		try {
//			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
////			sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
////					+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
////					+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
////					+ ") || ' ' || (select s.contract_ref_no || ' ' || c.customer_name1 from cstb_contract s, lctb_contract_master b, sttm_customer c "
////					+ "where s.contract_ref_no = b.contract_ref_no and s.counterparty = c.customer_no and s.contract_ref_no(+) = a.trn_ref_no "
////					+ "group by s.contract_ref_no , c.customer_name1) CF_ADDLTEXT,"
////					+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount "
////					+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
////					+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
////					+ "and a.ac_branch = ? ";
//		  
////			sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
////				+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
////				+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
////				+ ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
////				+ "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
////				+ "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)"
////				+ "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
////				+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, "
////				+ "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code "
////	            + "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no "
////				+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
////				+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
////				+ "and a.ac_branch = ? ";
//			
//			sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " +
//			      "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
//			      "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
//				  "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
//		    	  ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
//				  "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
//				  "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
//				  "group by s.user_ref_no , c.cust_name) || ', ' || " +
//			      "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
//			      "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " + 
//                  "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
//				  "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
//				  "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
//				  "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
//			      "from ACVWS_ALL_AC_ENTRIES_HIST a, sttms_trn_code s, setb_event_log c  " +
//			      "where a.trn_code = s.trn_code " +
//			      "and a.trn_code = s.trn_code " +
//			      "and c.event_reference(+) = a.trn_ref_no " +
//			      "and c.esn(+) = a.event_sr_no " +
//				  "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
//				  "a.ac_no = ? and a.ac_branch = ? ";
//		  
//		    if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//		    	sql += " and a.ac_ccy = ? ";		    	
//		    }
//			if(!"1".equals(cbsrekoto)){
//				sql += " and a.ac_no not in (?) ";
//			}
//			sql += " order by a.AC_ENTRY_SR_NO, a.trn_dt  ";
//			int i = 1;
//			stat = conn.prepareStatement(sql);
//			stat.setString(i++, tgl1.trim());
//			stat.setString(i++, tgl2.trim());
//			stat.setString(i++, ac_no.trim());
//			stat.setString(i++, brnch.trim());
//			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//				stat.setString(i++, ccy.trim());	    	
//		    }
//			if(!"1".equals(cbsrekoto)){
//				stat.setString(i++, "195001006");
//			};
//			rs = stat.executeQuery();
//			totMutCb = new BigDecimal(0);
//			totMutDb = new BigDecimal(0);
//			BigDecimal mutDb = new BigDecimal(0);
//			BigDecimal mutCb = new BigDecimal(0);
//			BigDecimal mutDbEq = new BigDecimal(0);
//			BigDecimal mutCbEq = new BigDecimal(0);
//			while (rs.next()) {
//				bb = new Bukubesar();
//				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
//				bb.setTrn_dt(rs.getDate("trn_dt"));
//				bb.setValue_dt(rs.getDate("value_dt"));
//				bb.setDrcr(rs.getString("drcr_ind"));
//				if(rs.getString("batch_no") != null){
//					bb.setBatch_no(rs.getString("batch_no"));
//				}else{
//					bb.setBatch_no("");
//				}
//				bb.setAc_no(rs.getString("ac_no"));
//				bb.setMaker_id(rs.getString("user_id"));
//				bb.setAuth_id(rs.getString("auth_id"));
//				bb.setEvent(rs.getString("event"));
//				bb.setTrn_code(rs.getString("trn_code"));
//				bb.setKet(rs.getString("CF_ADDLTEXT"));
//				bb.setTrn_code(rs.getString("trn_code"));
//				bb.setAc_ccy(rs.getString("ac_ccy"));
//				bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
//				bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
//
//				if ("D".equals(bb.getDrcr().trim())) {
//					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
//						mutDb = bb.getLcy_amount();
//					} else {
//						mutDb = bb.getFcy_amount();
//						mutDbEq = bb.getLcy_amount();
//					}
//				} else {
//					mutDb = new BigDecimal(0);
//					mutDbEq =new BigDecimal(0);
//				}
//				totMutDb = totMutDb.subtract(mutDb);
//				bb.setAmt_db_eq(mutDbEq);
//				bb.setAmt_db(mutDb);
//				if ("C".equals(bb.getDrcr().trim())) {
//					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
//						mutCb = bb.getLcy_amount();
//					} else {
//						mutCb = bb.getFcy_amount();
//						mutCbEq = bb.getLcy_amount();
//					}
//				} else {
//					mutCb = new BigDecimal(0);
//					mutCbEq = new BigDecimal(0);
//				}
//				bb.setAmt_cr_eq(mutCbEq);
//				bb.setAmt_cr(mutCb);
//				totMutCb = totMutCb.add(mutCb);
//				if ("C".equals(bb.getDrcr().trim())) {		
//					saldo = saldo.add(bb.getAmt_cr());
//				} else if ("D".equals(bb.getDrcr().trim())) {
//					saldo = saldo.subtract(bb.getAmt_db());
//				}
//				bb.setSaldo(saldo);
//				bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
//				lbb.add(bb);
//			}
//		} catch (SQLException ex) {
//			log.error(" getListBukubesar : " + ex.getMessage());
//		} finally {
//			closeConnDb(conn, stat, rs);
//		}
//		return lbb;
//	}
	
	//penambahan allcab-allval, AGUSTUS 2016
//	public List getListBukubesar(String tgl1, String tgl2, String ac_no, String brnch, String ccy, String cbsrekoto, BigDecimal saldo) {
public List getListBukubesar(String tgl1, String tgl2, String ac_no,
		String brnch, String ccy, String cbsrekoto, BigDecimal saldo, String allcab, String allval) {
		List lbb = new ArrayList();
		Bukubesar bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
//					+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
//					+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
//					+ ") || ' ' || (select s.contract_ref_no || ' ' || c.customer_name1 from cstb_contract s, lctb_contract_master b, sttm_customer c "
//					+ "where s.contract_ref_no = b.contract_ref_no and s.counterparty = c.customer_no and s.contract_ref_no(+) = a.trn_ref_no "
//					+ "group by s.contract_ref_no , c.customer_name1) CF_ADDLTEXT,"
//					+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount "
//					+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
//					+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
//					+ "and a.ac_branch = ? ";
		  
//			sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
//				+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
//				+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
//				+ ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
//				+ "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
//				+ "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)"
//				+ "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
//				+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, "
//				+ "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code "
//	            + "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no "
//				+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
//				+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
//				+ "and a.ac_branch = ? ";

//penambahan cabang				
			sql = "select a.trn_dt, a.batch_no, a.ac_branch, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,TO_CHAR (d.txn_dt_time, 'YYYY-MM-DD HH24:MI:SS') AS txn_dt_time, " +
			      "a.AUTH_ID, a.user_id,s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
			      "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
				  "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
		    	  ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
				  "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
				  "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
				  "group by s.user_ref_no , c.cust_name) || ', ' || " +
			      "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
			      "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " + 
                  "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
				  "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
				  "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
				  "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
			      "from ACVWS_ALL_AC_ENTRIES_HIST a, sttms_trn_code s, setb_event_log c, ACVW_ALL_AC_ENTRIES d " +
			      "where a.trn_code = s.trn_code " +
			      "and a.trn_code = s.trn_code " +
			      "AND a.trn_code = d.trn_code "+
	              "AND a.ac_branch = d.ac_branch "+
	              "AND a.ac_no = d.ac_no "+
	              "AND a.ac_entry_sr_no = d.ac_entry_sr_no "+
	              "AND a.trn_ref_no = d.trn_ref_no "+
	              "AND a.trn_dt = d.trn_dt "+
			      "and c.event_reference(+) = a.trn_ref_no " +
			      "and c.esn(+) = a.event_sr_no " +
				  "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
//				  "a.ac_no = ? and a.ac_branch = ? ";
				  "a.ac_no = ?";
		  
		    if("485052001".equals(ac_no) || "585054001".equals(ac_no)){					
		    } else {
//penambahan allcab-allval, AGUSTUS 2016					
//		    	sql += " and a.ac_ccy = ? ";		    	
				if ("2".equals(allval)) //cek semua valuta
				{
					//tidak ada query
				}
				else //tidak cek semua valuta
				{
					sql += " AND a.ac_ccy = ? ";
				}
//
		    }

			if("1".equals(allcab)) //cek semua cabang
			{
				//tidak ada query
			}
			else //tidak cek semua cabang
			{
    	
				sql += " AND a.ac_branch = ? ";
	        }

			if(!"1".equals(cbsrekoto)){
				sql += " and a.ac_no not in (?) ";
			}
			sql += " order by a.AC_ENTRY_SR_NO, a.trn_dt  ";
			log.info("sql getListBukubesar-->" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, tgl1.trim());
			stat.setString(i++, tgl2.trim());
			stat.setString(i++, ac_no.trim());
//			stat.setString(i++, brnch.trim());
			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
		    } else {	    	
//				stat.setString(i++, ccy.trim());
//penambahan allcab-allval, AGUSTUS 2016
				if("2".equals(allval)) //cek semua valuta
				{
					//tidak ada query
				}
				else //tidak cek semua valuta
				{
					stat.setString(i++, ccy.trim());
				}
			}
			
			if("1".equals(allcab)) //cek semua cabang
			{
				//tidak ada query
			}
			else //tidak cek semua cabang
			{
    	
				stat.setString(i++, brnch.trim());
	        }
//			
			if(!"1".equals(cbsrekoto)){
				stat.setString(i++, "195001006");
			};
			rs = stat.executeQuery();
			totMutCb = new BigDecimal(0);
			totMutDb = new BigDecimal(0);
			BigDecimal mutDb = new BigDecimal(0);
			BigDecimal mutCb = new BigDecimal(0);
			BigDecimal mutDbEq = new BigDecimal(0);
			BigDecimal mutCbEq = new BigDecimal(0);
			
			System.out.println("SQL getListBukubesar Download=> " +sql);
			while (rs.next()) {
				bb = new Bukubesar();
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setTrn_dt(rs.getDate("trn_dt"));
				bb.setValue_dt(rs.getDate("value_dt"));
				bb.setTxn_dt_time(rs.getString("txn_dt_time"));
//penambahan cabang
				bb.setAc_branch(rs.getString("ac_branch"));
//
				bb.setDrcr(rs.getString("drcr_ind"));
				if(rs.getString("batch_no") != null){
					bb.setBatch_no(rs.getString("batch_no"));
				}else{
					bb.setBatch_no("");
				}
				bb.setAc_no(rs.getString("ac_no"));
				bb.setMaker_id(rs.getString("user_id"));
				bb.setAuth_id(rs.getString("auth_id"));
				bb.setEvent(rs.getString("event"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setKet(rs.getString("CF_ADDLTEXT"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setAc_ccy(rs.getString("ac_ccy"));
				bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
				bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));

				if ("D".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
						mutDb = bb.getLcy_amount();
					} else {
						mutDb = bb.getFcy_amount();
						mutDbEq = bb.getLcy_amount();
					}
				} else {
					mutDb = new BigDecimal(0);
					mutDbEq =new BigDecimal(0);
				}
				totMutDb = totMutDb.subtract(mutDb);
				bb.setAmt_db_eq(mutDbEq);
				bb.setAmt_db(mutDb);
				if ("C".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
						mutCb = bb.getLcy_amount();
					} else {
						mutCb = bb.getFcy_amount();
						mutCbEq = bb.getLcy_amount();
					}
				} else {
					mutCb = new BigDecimal(0);
					mutCbEq = new BigDecimal(0);
				}
				bb.setAmt_cr_eq(mutCbEq);
				bb.setAmt_cr(mutCb);
				totMutCb = totMutCb.add(mutCb);
				if ("C".equals(bb.getDrcr().trim())) {		
					saldo = saldo.add(bb.getAmt_cr());
				} else if ("D".equals(bb.getDrcr().trim())) {
					saldo = saldo.subtract(bb.getAmt_db());
				}
				bb.setSaldo(saldo);
//				System.out.println("saldo getlistbukubesar (button download): "+saldo);
				
				bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListBukubesar : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
//=============================================================================================================================================================================================

//penambahan BukuBesarGL, Agustus 2016
		//public List getListBukubesar(String tgl1, String tgl2, String ac_no, BigDecimal saldo) {
		public List getListBukubesarGL(String tgl1, String tgl2, String ac_no, String ac_no2, String ac_no3, String ac_no4, String ac_no5,
				String ac_no6, String ac_no7, String ac_no8, String ac_no9, String ac_no10, BigDecimal saldo) {
				List lbb = new ArrayList();
				Bukubesar bb = null;
				Connection conn = null;
				PreparedStatement stat = null;
				ResultSet rs = null;
				String sql = "";

				try {
					conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//					sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
//							+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
//							+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
//							+ ") || ' ' || (select s.contract_ref_no || ' ' || c.customer_name1 from cstb_contract s, lctb_contract_master b, sttm_customer c "
//							+ "where s.contract_ref_no = b.contract_ref_no and s.counterparty = c.customer_no and s.contract_ref_no(+) = a.trn_ref_no "
//							+ "group by s.contract_ref_no , c.customer_name1) CF_ADDLTEXT,"
//							+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount "
//							+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
//							+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
//							+ "and a.ac_branch = ? ";
				  
//					sql = "select a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
//						+ "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
//						+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
//						+ ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
//						+ "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
//						+ "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)"
//						+ "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
//						+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, "
//						+ "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code "
//			            + "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no "
//						+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
//						+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and a.ac_no = ? "
//						+ "and a.ac_branch = ? ";
					
					sql = "select a.trn_dt, a.batch_no, a.ac_branch, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " +
					      "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
					      "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
						  "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
				    	  ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
						  "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
						  "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
						  "group by s.user_ref_no , c.cust_name) || ', ' || " +
					      "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
					      "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " + 
		                  "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
						  "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
						  "(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
						  "and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
					      "from ACVWS_ALL_AC_ENTRIES_HIST a, sttms_trn_code s, setb_event_log c  " +
					      "where a.trn_code = s.trn_code " +
					      "and a.trn_code = s.trn_code " +
					      "and c.event_reference(+) = a.trn_ref_no " +
					      "and c.esn(+) = a.event_sr_no " +
						  "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
						  //"a.ac_no = ? and a.ac_branch = ? ";
//						  "a.ac_no = ? ";
						  "a.ac_no in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
				  
/*				    if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
				    } else {	    	
				    	sql += " and a.ac_ccy = ? ";		    	
				    }
					if(!"1".equals(cbsrekoto)){
						sql += " and a.ac_no not in (?) ";
					}
*/
					sql += " order by a.AC_ENTRY_SR_NO, a.trn_dt, a.ac_no  ";
					int i = 1;
					stat = conn.prepareStatement(sql);
					stat.setString(i++, tgl1.trim());
					stat.setString(i++, tgl2.trim());
					stat.setString(i++, ac_no.trim());
					stat.setString(i++, ac_no2.trim());
					stat.setString(i++, ac_no3.trim());
					stat.setString(i++, ac_no4.trim());
					stat.setString(i++, ac_no5.trim());
					stat.setString(i++, ac_no6.trim());
					stat.setString(i++, ac_no7.trim());
					stat.setString(i++, ac_no8.trim());
					stat.setString(i++, ac_no9.trim());
					stat.setString(i++, ac_no10.trim());
/*					stat.setString(i++, brnch.trim());
					if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
				    } else {	    	
						stat.setString(i++, ccy.trim());	    	
				    }
					if(!"1".equals(cbsrekoto)){
						stat.setString(i++, "195001006");
					};
*/
					rs = stat.executeQuery();
					totMutCb = new BigDecimal(0);
					totMutDb = new BigDecimal(0);
					BigDecimal mutDb = new BigDecimal(0);
					BigDecimal mutCb = new BigDecimal(0);
					BigDecimal mutDbEq = new BigDecimal(0);
					BigDecimal mutCbEq = new BigDecimal(0);
					
					System.out.println("SQL getListBukubesarGL => " +sql);
					while (rs.next()) {
						bb = new Bukubesar();
						bb.setTrn_ref_no(rs.getString("trn_ref_no"));
						bb.setTrn_dt(rs.getDate("trn_dt"));
						bb.setValue_dt(rs.getDate("value_dt"));
						//penambahan cabang di download-an
						bb.setAc_branch(rs.getString("ac_branch"));
						//
						bb.setDrcr(rs.getString("drcr_ind"));
						if(rs.getString("batch_no") != null){
							bb.setBatch_no(rs.getString("batch_no"));
						}else{
							bb.setBatch_no("");
						}
						bb.setAc_no(rs.getString("ac_no"));
						bb.setMaker_id(rs.getString("user_id"));
						bb.setAuth_id(rs.getString("auth_id"));
						bb.setEvent(rs.getString("event"));
						bb.setTrn_code(rs.getString("trn_code"));
						bb.setKet(rs.getString("CF_ADDLTEXT"));
						bb.setTrn_code(rs.getString("trn_code"));
						bb.setAc_ccy(rs.getString("ac_ccy"));
						bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
						bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));

						if ("D".equals(bb.getDrcr().trim())) {
							if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))
									|| ("485052001".equals(ac_no2) || "585054001".equals(ac_no2)) || ("485052001".equals(ac_no3) || "585054001".equals(ac_no3))
									|| ("485052001".equals(ac_no4) || "585054001".equals(ac_no4)) || ("485052001".equals(ac_no5) || "585054001".equals(ac_no5))
									|| ("485052001".equals(ac_no6) || "585054001".equals(ac_no6))
									|| ("485052001".equals(ac_no7) || "585054001".equals(ac_no7)) || ("485052001".equals(ac_no8) || "585054001".equals(ac_no8))
									|| ("485052001".equals(ac_no9) || "585054001".equals(ac_no9)) || ("485052001".equals(ac_no10) || "585054001".equals(ac_no10))) {
								mutDb = bb.getLcy_amount();
							} else {
								mutDb = bb.getFcy_amount();
								mutDbEq = bb.getLcy_amount();
							}
						} else {
							mutDb = new BigDecimal(0);
							mutDbEq =new BigDecimal(0);
						}
						totMutDb = totMutDb.subtract(mutDb);
						bb.setAmt_db_eq(mutDbEq);
						bb.setAmt_db(mutDb);
						if ("C".equals(bb.getDrcr().trim())) {
							if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))
									|| ("485052001".equals(ac_no2) || "585054001".equals(ac_no2)) || ("485052001".equals(ac_no3) || "585054001".equals(ac_no3))
									|| ("485052001".equals(ac_no4) || "585054001".equals(ac_no4)) || ("485052001".equals(ac_no5) || "585054001".equals(ac_no5))
									|| ("485052001".equals(ac_no6) || "585054001".equals(ac_no6))
									|| ("485052001".equals(ac_no7) || "585054001".equals(ac_no7)) || ("485052001".equals(ac_no8) || "585054001".equals(ac_no8))
									|| ("485052001".equals(ac_no9) || "585054001".equals(ac_no9)) || ("485052001".equals(ac_no10) || "585054001".equals(ac_no10))) {
								mutCb = bb.getLcy_amount();
							} else {
								mutCb = bb.getFcy_amount();
								mutCbEq = bb.getLcy_amount();
							}
						} else {
							mutCb = new BigDecimal(0);
							mutCbEq = new BigDecimal(0);
						}
						bb.setAmt_cr_eq(mutCbEq);
						bb.setAmt_cr(mutCb);
						totMutCb = totMutCb.add(mutCb);
						if ("C".equals(bb.getDrcr().trim())) {		
							saldo = saldo.add(bb.getAmt_cr());
						} else if ("D".equals(bb.getDrcr().trim())) {
							saldo = saldo.subtract(bb.getAmt_db());
						}
						bb.setSaldo(saldo);
						bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
						lbb.add(bb);
					}
				} catch (SQLException ex) {
					log.error("eksepsi getListBukubesarGL : " + ex.getMessage());
				} finally {
					closeConnDb(conn, stat, rs);
				}
				return lbb;
			}

	public List searchBukubesar(String trn_ref_no,String tgl) {
		List lbb = new ArrayList();
		Bukubesar bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			String sql1 = "";
			System.out.println("tanggal "+tgl);
			if ((!"".equals(trn_ref_no) && trn_ref_no != null)&& ("".equals(tgl) || tgl == null)) {
				//System.out.println("masuk 1 "+tgl);
				sql = "select a.trn_dt, a.batch_no, a.ac_no, a.ac_branch, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
						+ "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
						+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no"
						+ ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
				        + "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
				        + "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)"
				        + "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
						+ "a.drcr_ind, a.ac_ccy, a.exch_rate, a.fcy_amount, a.lcy_amount "
						+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
						+ "where a.trn_ref_no = ? order by a.trn_dt desc";
			} 
			else if ((!"".equals(trn_ref_no) && trn_ref_no != null)&&(!"".equals(tgl) || tgl != null)) {
				//System.out.println("masuk 2 "+tgl);
				sql = "select a.trn_dt, a.batch_no, a.ac_no, a.ac_branch, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
						+ "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
						+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no"
						+ ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
				        + "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
				        + "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)"
				        + "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
						+ "a.drcr_ind, a.ac_ccy, a.exch_rate, a.fcy_amount, a.lcy_amount "
						+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
						+ "where a.trn_ref_no = ? and a.trn_dt = TO_DATE(?, 'DD-MM-YYYY') order by a.trn_dt desc";
			} 
			
			else {
				//System.out.println("masuk 3 "+tgl);
				sql = "select a.trn_dt, a.batch_no, a.ac_no, a.ac_branch, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
						+ "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||("
						+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no"
						+ ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
				        + "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
				        + "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no)"
				        + "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"								
						+ "a.drcr_ind, a.ac_ccy, a.exch_rate, a.fcy_amount, a.lcy_amount "
						+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code " +
						  "where a.trn_dt = TO_DATE('?', 'DD-MM-YYYY') order by a.trn_dt desc";
			}

			stat = conn.prepareStatement(sql);
			if ((!"".equals(trn_ref_no) && trn_ref_no != null)&&("".equals(tgl) || tgl == null) ) {
				stat.setString(1, trn_ref_no);
			
			} else if ((!"".equals(trn_ref_no) && trn_ref_no != null)&&(!"".equals(tgl) || tgl != null)){
				stat.setString(1, trn_ref_no);
				stat.setString(2, tgl);
			} else{
			
				stat.setString(1, tgl);
			}
			
			log.info("SQL Search Jurnal -->" + sql );
			
			rs = stat.executeQuery();
			totMutCb = new BigDecimal(0);
			totMutDb = new BigDecimal(0);
			BigDecimal mutDb = new BigDecimal(0);
			BigDecimal mutCb = new BigDecimal(0);
			while (rs.next()) {
				bb = new Bukubesar();
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setTrn_dt(rs.getDate("trn_dt"));
				bb.setValue_dt(rs.getDate("VALUE_DT"));
				bb.setDrcr(rs.getString("drcr_ind"));
				bb.setBatch_no(rs.getString("batch_no"));
				bb.setAc_no(rs.getString("ac_no"));
				bb.setAc_branch(rs.getString("ac_branch"));
				bb.setMaker_id(rs.getString("user_id"));
				bb.setAuth_id(rs.getString("AUTH_ID"));
				bb.setEvent(rs.getString("event"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setKet(rs.getString("CF_ADDLTEXT"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setAc_ccy(rs.getString("ac_ccy"));
				if(rs.getBigDecimal("exch_rate") == null){
					bb.setExh_rate(new BigDecimal(0));
				} else {
					bb.setExh_rate(rs.getBigDecimal("exch_rate"));
				}
				if(rs.getBigDecimal("fcy_amount") == null){
					bb.setFcy_amount(new BigDecimal(0));					
				}else{
					bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));					
				}
				if(rs.getBigDecimal("lcy_amount") == null){
					bb.setLcy_amount(new BigDecimal(0));					
				}else{
					bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));					
				}
				if ("D".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy())) {
						mutDb = bb.getLcy_amount();
					} else {
						mutDb = bb.getFcy_amount();
					}
					totMutDb = totMutDb.subtract(bb.getLcy_amount());
				} else {
					mutDb = new BigDecimal(0);
				}
//				totMutDb = totMutDb.subtract(mutDb);
				bb.setAmt_db(mutDb);
				if ("C".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy())) {
						mutCb = bb.getLcy_amount();
					} else {
						mutCb = bb.getFcy_amount();
					}
					totMutCb = totMutCb.add(bb.getLcy_amount());
				} else {
					mutCb = new BigDecimal(0);
				}
				bb.setAmt_cr(mutCb);
				 code_trn= bb.getTrn_code();
//				totMutCb = totMutCb.add(mutCb);
				lbb.add(bb);
			}
		} catch (SQLException ex) {
	
			// ex.getMessage());
			log.error("Error in searchBukubesar : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public List getListbatch(String userId) {
		List lbb = new ArrayList();
		BatchMaster bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "SELECT last_oper_dt_stamp, batch_no,description,branch_code, last_oper_id,Auth_Stat,dr_ent_total,cr_ent_total FROM DETB_BATCH_MASTER where last_oper_id=? ";

			stat = conn.prepareStatement(sql);
			stat.setString(1, userId);
			rs = stat.executeQuery();

			while (rs.next()) {
				bb = new BatchMaster();
				bb.setBatch_no(rs.getString("batch_no"));
				bb.setDescription(rs.getString("description"));
				bb.setLast_oper_id(rs.getString("last_oper_id"));
				bb.setCr_ent_total(rs.getBigDecimal("cr_ent_total"));
				bb.setDr_ent_total(rs.getBigDecimal("dr_ent_total"));
				bb.setLast_oper_dt_stamp(rs.getDate("last_oper_dt_stamp"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListbatch : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public List getDetailBatch(String batch_no, String userId) {
		List lbb = new ArrayList();
		DetailBatch bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select a.trn_ref_no,a.trn_dt,a.user_id,a.event,a.ac_entry_sr_no,"
					+ "a.ac_branch,a.ac_no,d.gl_desc, a.ac_ccy,a.drcr_ind,a.trn_code,a.lcy_amount,a.auth_id,"
					+ "b.trn_desc, remarks from actb_daily_log a, sttms_trn_code b, csvw_addl_text c, gltm_glmaster d "
					+ "where a.trn_code = b.trn_code and a.ac_no = d.gl_code and a.trn_ref_no = c.reference_no and batch_no=? and user_id = ? "
					+ "order by trn_ref_no";
	
			stat = conn.prepareStatement(sql);
			stat.setString(1, batch_no);
			stat.setString(2, userId);

			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new DetailBatch();
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setTrn_dt(rs.getDate("trn_dt"));
				bb.setLast_oper_id(rs.getString("user_id"));
				bb.setEvent(rs.getString("event"));
				bb.setAc_entry_sr_no(rs.getString("ac_entry_sr_no"));
				bb.setAc_branch(rs.getString("ac_branch"));
				bb.setAc_no(rs.getString("ac_no"));
				bb.setAc_ccy(rs.getString("ac_ccy"));
				bb.setDrcr_ind(rs.getString("drcr_ind"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
				bb.setAuth_id(rs.getString("auth_id"));
				bb.setTrn_desc(rs.getString("trn_desc"));
				bb.setRemarks(rs.getString("remarks"));
				bb.setGl_desc(rs.getString("gl_desc"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListbatch : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public List getListbatchh( String branch, String tgl1) {
		List lbb = new ArrayList();
		BatchMaster bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			/*sql = "SELECT last_oper_dt_stamp, batch_no,description,branch_code,"
					+ "last_oper_id,Auth_Stat,dr_ent_total,cr_ent_total FROM DETB_BATCH_MASTER_HIST "
					+ "where last_oper_dt_stamp = TO_DATE(?, 'DD-MM-YYYY') and "
					+ "last_oper_dt_stamp <= TO_DATE(?, 'DD-MM-YYYY')  AND auth_stat=? ";*/
			
			sql = "SELECT TO_CHAR(last_oper_dt_stamp, 'YYYY-MM-DD') AS last_oper_dt_stamp, batch_no,description,branch_code,"
					+ "last_oper_id,Auth_Stat,dr_ent_total,cr_ent_total FROM DETB_BATCH_MASTER_HIST "
					+ "where last_oper_id not in ('FLEXGW','FLEXSWITCH','SYSTEM') and last_oper_dt_stamp = TO_DATE(?, 'DD-MM-YYYY') "
					+ " AND auth_stat=?";
			/*if (!"".equals(userId) && userId != null) {
				sql += " and last_oper_id = ? ";
			}*/
			if (!"".equals(branch) && branch != null) {
				sql += " and branch_code = ? ";
			}
		/*	if (!"".equals(batch) && batch != null) {
				sql += " and batch_no = ? ";
			}*/
			//sql += " order by last_oper_dt_stamp ";
		
			sql += " UNION select to_char(to_date(to_char(tanggal),'DD/MM/YYYY'),'YYYY-MM-DD'),ket_kode_transaksi,kode_transaksi,kode_cabang,user_id,kode_transaksi,debet,kredit from (select c.user_id,substr(a.trn_ref_no, 1, 3) as kode_cabang,a.trn_code as kode_transaksi,a.instrument_code as nomor_warkat,to_char(a.txn_dt_time,'DD-MM-YYYY') as tanggal,to_char(a.txn_dt_time, 'HH24:MI') as jam,b.trn_desc || ':' ||  ACPKS_FCJ_BMIJRNAL.cf_addltextformula (a.trn_ref_no, a.event_sr_no) || ',' || (SELECT DISTINCT x.ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL x  where x.reference_no = a.trn_ref_no and serial_no = nvl(a.curr_no, a.ENTRY_SEQ_NO))||''||(SELECT y.narrative FROM detbs_rtl_teller y  where y.trn_ref_no = a.trn_ref_no) as ket_kode_transaksi,case when a.drcr_ind = 'D' then (case when a.trn_code = '100' then null  else(case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as debet,case when a.drcr_ind = 'C' then (case  when a.trn_code = '200' then null else (case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as kredit,  a.auth_id AS user_approval,a.ac_no,case when length(a.ac_no)=10 then (select ac_desc from sttm_cust_account where cust_ac_no=a.ac_no) else (select gl_desc from gltm_glmaster where gl_code=a.ac_no) END AS ac_desc from actb_daily_log a, sttm_trn_code b,(select d.user_id from smtb_user d, smtb_user_role c  where c.user_id = d.user_id and c.role_id IN ('TL_IN','TL_IN_JR','BO_IN','BO_IN_JR','BO_ALL_IN', 'TL_IN_JR_1','TL_IN_JR_2','TL_IN_JR_3','TL_IN_SEN_1','TL_IN_SEN_2','TL_IN_SEN_3','TL_IN_SEN_4','TL_IN_SEN_5', 'TL_IN_SEN_6','TL_IN_SEN_7','TL_IN_SEN_8','TL_MULIA_1','TL_MULIA_2','TL_MULIA_3','TLR_HYBRID','TLR_HYBRID_LSTP','FL_HYBRID')) c  where a.trn_code = b.trn_code and a.user_id = c.user_id and a.delete_stat <> 'D' and a.ac_no NOT IN ('195002002', '695001006', '195001006')  and a.ac_no NOT BETWEEN '101002000' AND '1010022099' and a.user_id NOT IN ('FLEXGW','FLEXSWITCH','SYSTEM')  and a.auth_id NOT IN ('FLEXGW','FLEXSWITCH','SYSTEM')) a1 where tanggal = ?  ";
			
			if (!"".equals(branch) && branch != null) {
				sql += " and kode_cabang = ? ";
			}
			
			sql += " group by user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no,ac_desc";
					
					
					
			int i = 1;
			stat = conn.prepareStatement(sql);
			
			stat.setString(i++, tgl1);
			//stat.setString(i++, tgl2);
			stat.setString(i++, "A");
			/*if (!"".equals(userId) && userId != null) {
				stat.setString(i++, userId);
			}*/
			if (!"".equals(branch) && branch != null) {
				stat.setString(i++, branch);
			}
			/*if (!"".equals(batch) && batch != null) {
				stat.setString(i++, batch);
			}*/
			stat.setString(i++, tgl1);
			if (!"".equals(branch) && branch != null) {
				stat.setString(i++, branch);
			}
			
			log.info("SQL getListbatchh -->" + sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new BatchMaster();
				bb.setBatch_no(rs.getString("batch_no"));
				bb.setDescription(rs.getString("description"));
				bb.setLast_oper_id(rs.getString("last_oper_id"));
				bb.setAuth_stat(rs.getString("auth_stat"));
				bb.setCr_ent_total(rs.getBigDecimal("cr_ent_total"));
				bb.setDr_ent_total(rs.getBigDecimal("dr_ent_total"));
				bb.setLast_oper_dt_stamp(rs.getDate("last_oper_dt_stamp"));
				bb.setBranch_code(rs.getString("branch_code"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListbatch : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	
	public List getDetailBatchh(String batch_no, String userId, String kdcab, String trn_dt) {
		List lbb = new ArrayList();
		DetailBatch bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select a.batch_no, a.trn_ref_no,a.trn_dt,a.user_id,a.event,a.ac_entry_sr_no,a.ac_branch,a.ac_no,d.gl_desc,"
					+ "a.ac_ccy,a.drcr_ind,a.trn_code,a.lcy_amount,a.auth_id, b.trn_desc,"
					+ "(select remarks from csvw_addl_text where reference_no = a.trn_ref_no and serial_no = a.curr_no ) remarks "
					+ "from actb_history a, sttms_trn_code b, gltm_glmaster d "
					+ "where a.trn_code = b.trn_code  and  a.batch_no=? and a.user_id = ? and "
					+ "a.ac_no = d.gl_code and a.ac_branch= ? and a.trn_dt = to_date(?, 'YYYY-MM-DD') order by trn_ref_no, a.ac_entry_sr_no";

			stat = conn.prepareStatement(sql);

			stat.setString(1, batch_no);
			stat.setString(2, userId);
			stat.setString(3, kdcab);
			stat.setString(4, trn_dt);
			log.info("SQL getListbatchdetail -->" + sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new DetailBatch();
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setTrn_dt(rs.getDate("trn_dt"));
				bb.setLast_oper_id(rs.getString("user_id"));
				bb.setEvent(rs.getString("event"));
				bb.setAc_entry_sr_no(rs.getString("ac_entry_sr_no"));
				bb.setAc_branch(rs.getString("ac_branch"));
				bb.setAc_no(rs.getString("ac_no"));
				bb.setAc_ccy(rs.getString("ac_ccy"));
				bb.setDrcr_ind(rs.getString("drcr_ind"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setLcy_amount((rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount"));
				bb.setAuth_id(rs.getString("auth_id"));
				bb.setTrn_desc(rs.getString("trn_desc"));
				bb.setRemarks(rs.getString("remarks"));
				bb.setBatch_no(rs.getString("batch_no"));
				bb.setGl_desc(rs.getString("gl_desc"));
				lbb.add(bb);
			}
	
		} catch (SQLException ex) {
		
			log.error(" getListbatch : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public BigDecimal getTotMutCb() {
		return totMutCb;
	}

	public BigDecimal getTotMutDb() {
		return totMutDb;
	}

//	public Saldo getSaldo(String acc, String brnch, String ccy, String dt1) {
//		Saldo sld = null;
//		Connection conn = null;
//		PreparedStatement stat = null;
//		ResultSet rs = null;
//		String sql = "";
//
//		try {
//			// conn = ConnToDb.getConnectionOracle();
//			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
////			sql = "select branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal from actb_accbal_history where account=? and branch_code=? and acc_ccy = ? "
////					+ "and BKG_DATE < TO_DATE(?, 'DD-MM-YY') and rownum = ? order by bkg_date desc ";
////			sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
////					"from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
////					"and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE <= TO_DATE(?, 'DD-MM-YY'))";
//			DateFormat formatter = null;
//	        Date convertedDate = null;
//            Date todayDate = new Date();
//            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
//	        try {
//	            formatter = new SimpleDateFormat("dd-MM-yyyy");
//	            convertedDate = (Date) formatter.parse(dt1);
//	            todayDate = dateFormatter.parse(dateFormatter.format(new Date()));
//	        } catch (Exception e) {
//	        	log.info(e.getMessage());
//	        }
//	        if (todayDate.compareTo(convertedDate) == 0) {
//				sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
//				"from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
//				"and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE < TO_DATE(?, 'DD-MM-YY'))";	        	
//	        } else {
//				sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
//					"from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
//					"and BKG_DATE = (select min(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE >= TO_DATE(?, 'DD-MM-YY'))";
//	        }
//			stat = conn.prepareStatement(sql);
//			stat.setString(1, acc.trim());
//			stat.setString(2, brnch.trim());
//			if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
//				stat.setString(3, "IDR");				
//			}else{
//				stat.setString(3, ccy.trim());
//			}
//
//			stat.setString(4, acc.trim());
//			stat.setString(5, brnch.trim());
//			if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
//				stat.setString(6, "IDR");				
//			}else{
//				stat.setString(6, ccy.trim());
//			}			
//			stat.setString(7, dt1.trim());
//			rs = stat.executeQuery();
//			while (rs.next()) {
//				sld = new Saldo();
//				sld.setBranch_code(rs.getString("branch_code"));
//				sld.setAccount(rs.getString("account"));
//				sld.setAcc_ccy(rs.getString("acc_ccy"));
//				if (todayDate.compareTo(convertedDate) == 0) {
//					sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
//					sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
//					sld.setAcy_opening_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
//					sld.setLcy_opening_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));					
//				} else {
//					sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
//					sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
//					sld.setAcy_opening_bal((rs.getBigDecimal("acy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_opening_bal"));
//					sld.setLcy_opening_bal((rs.getBigDecimal("lcy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_opening_bal"));
//				}
//				
//			}
//			
//			if(sld==null){
//				sld = new Saldo();
//				sld.setBranch_code(brnch);
//				sld.setAccount(acc);
//				sld.setAcc_ccy(ccy);
//				sld.setAcy_closing_bal(new BigDecimal(0));
//				sld.setLcy_closing_bal(new BigDecimal(0));
//				sld.setAcy_opening_bal(new BigDecimal(0));
//				sld.setLcy_opening_bal(new BigDecimal(0));
//			}
//		} catch (SQLException ex) {
//			log.error("error in getSaldo : " + ex.getMessage());
//		
//		} finally {
//			closeConnDb(conn, stat, rs);
//		}
//		return sld;
//	}

	//penambahan allcab-allval, AGUSTUS 2016
		//public Saldo getSaldo(String acc, String brnch, String ccy, String dt1) {
		public Saldo getSaldo(String acc, String brnch, String ccy, String dt1, String allcab, String allval) {
				Saldo sld = null;
				Connection conn = null;
				PreparedStatement stat = null;
				ResultSet rs = null;
				String sql = "";

				try {
					// conn = ConnToDb.getConnectionOracle();
					conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//					sql = "select branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal from actb_accbal_history where account=? and branch_code=? and acc_ccy = ? "
//							+ "and BKG_DATE < TO_DATE(?, 'DD-MM-YY') and rownum = ? order by bkg_date desc ";
//					sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
//							"from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
//							"and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE <= TO_DATE(?, 'DD-MM-YY'))";
					DateFormat formatter = null;
			        Date convertedDate = null;
		            Date todayDate = new Date();
		            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
			        try {
			            formatter = new SimpleDateFormat("dd-MM-yyyy");
			            convertedDate = (Date) formatter.parse(dt1);
			            todayDate = dateFormatter.parse(dateFormatter.format(new Date()));
			        } catch (Exception e) {
			        	log.info(e.getMessage());
			        }
			        if (todayDate.compareTo(convertedDate) == 0) {
	//penambahan allcab-allval, AGUSTUS 2016
						sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
							"from actb_accbal_history where account= ? ";
						if("1".equals(allcab)) //cek semua cabang
						{
							if ("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
						}
						else //tidak cek semua cabang
						{
							if("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
							sql += " and branch_code= ? ";
						}
						sql += "and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account= ? ";
						if("1".equals(allcab)) //cek semua cabang
						{
							if ("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
						}
						else //tidak cek semua cabang
						{
							if ("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
							sql += "and branch_code= ? ";
						}
						sql += " and BKG_DATE < TO_DATE(?, 'DD-MM-YY')) ";					
			        } else {
						sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
							"from actb_accbal_history where account= ? ";
						if("1".equals(allcab)) //cek semua cabang
						{
							if ("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
						}
						else //tidak cek semua cabang
						{
							if("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
							sql += " and branch_code= ? ";
						}
						sql += "and BKG_DATE = (select min(bkg_date) from actb_accbal_history where account= ? ";
						if("1".equals(allcab)) //cek semua cabang
						{
							if ("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
						}
						else //tidak cek semua cabang
						{
							if ("2".equals(allval)) //cek semua valuta
							{
								//tidak ada query
							}
							else //tidak cek semua valuta
							{
								sql += " and acc_ccy = ? ";
							}
							sql += "and branch_code= ? ";
						}
						sql += " and BKG_DATE >= TO_DATE(?, 'DD-MM-YY')) ";
//					
					}
			        System.out.println("sql getSaldo" + sql);
	//penambahan allcab-allval, AGUSTUS 2016
					int i = 1;
					stat = conn.prepareStatement(sql);
					stat.setString(i++, acc.trim());
					System.out.println("1 acc.trim: "+acc.trim());
					
					if("1".equals(allcab)) //cek semua cabang
					{
						if("2".equals(allval)) //cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");
								System.out.println("2 ccy: IDR");
							}else{
								//stat.setString(i++, ccy.trim());
								System.out.println("no query (allcab allval)");
							}
						}
						else //tidak cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");	
								System.out.println("2 ccy: IDR");
							}else{
								stat.setString(i++, ccy.trim());
								System.out.println("2 ccy: "+ccy.trim());
							}
						}
					}
					else //tidak cek semua cabang
					{
						if("2".equals(allval)) //cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");
								System.out.println("2 ccy: IDR");
							}else{
								//stat.setString(i++, ccy.trim());
								System.out.println("no query (allcab allval) 2");
							}
						}
						else //tidak cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");
								System.out.println("2 ccy: IDR");
							}else{
								stat.setString(i++, ccy.trim());
								System.out.println("2 ccy: "+ccy.trim());
							}
						}
						stat.setString(i++, brnch.trim());
						System.out.println("3 brnch: "+brnch.trim());
					}
		//bagian kedua		
					stat.setString(i++, acc.trim());
					System.out.println("4 acc.trim: "+acc.trim());

					if("1".equals(allcab)) //cek semua cabang
					{
						if("2".equals(allval)) //cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");
								System.out.println("5 ccy: IDR");
							}else{
								//stat.setString(i++, ccy.trim());
								System.out.println("no query (allcab allval)");
							}
						}
						else //tidak cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");	
								System.out.println("5 ccy: IDR");
							}else{
								stat.setString(i++, ccy.trim());
								System.out.println("5 ccy: "+ccy.trim());
							}
						}
					}
					else //tidak cek semua cabang
					{		
						if("2".equals(allval)) //cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");
								System.out.println("5 ccy: IDR");
							}else{
								//stat.setString(i++, ccy.trim());
								System.out.println("no query (allcab allval) 2");
							}
						}
						else //tidak cek semua valuta
						{
							if("485052001".equals(acc.trim()) || "585054001".equals(acc.trim())){
								stat.setString(i++, "IDR");
								System.out.println("5 ccy: IDR");
							}else{
								stat.setString(i++, ccy.trim());
								System.out.println("5 ccy: "+ccy.trim());
							}
						}
						stat.setString(i++, brnch.trim());
						System.out.println("6 brnch: "+brnch.trim());
					}
	//
					stat.setString(i++, dt1.trim());
					System.out.println("7 dt1.trim: "+dt1.trim());
					
					rs = stat.executeQuery();
					while (rs.next()) {
						sld = new Saldo();
						sld.setBranch_code(rs.getString("branch_code"));
						sld.setAccount(rs.getString("account"));
						sld.setAcc_ccy(rs.getString("acc_ccy"));
						if (todayDate.compareTo(convertedDate) == 0) {
							sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
							sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
							sld.setAcy_opening_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
							sld.setLcy_opening_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));					
						} else {
							sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
							sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
							sld.setAcy_opening_bal((rs.getBigDecimal("acy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_opening_bal"));
							sld.setLcy_opening_bal((rs.getBigDecimal("lcy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_opening_bal"));
						}
					}
					
					if(sld==null){
						sld = new Saldo();
						sld.setBranch_code(brnch);
						sld.setAccount(acc);
						sld.setAcc_ccy(ccy);
						sld.setAcy_closing_bal(new BigDecimal(0));
						sld.setLcy_closing_bal(new BigDecimal(0));
						sld.setAcy_opening_bal(new BigDecimal(0));
						sld.setLcy_opening_bal(new BigDecimal(0));
					}
				} catch (SQLException ex) {
					log.error("error in getSaldo : " + ex.getMessage());
				
				} finally {
					closeConnDb(conn, stat, rs);
				}
				return sld;
			}
			
	//=============================================================================================================================================================================================

	//penambahan BukuBesarGL, Agustus 2016
		//public Saldo getSaldo(String acc, String brnch, String ccy, String dt1, String allcab, String allval) {
		public Saldo getSaldoGL(String acc, String acc2, String acc3, String acc4, String acc5, String acc6, String acc7, String acc8, String acc9, String acc10, String dt1) {
			Saldo sld = null;
			Connection conn = null;
			PreparedStatement stat = null;
			ResultSet rs = null;
			String sql = "";

			try {
				// conn = ConnToDb.getConnectionOracle();
				conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//				sql = "select branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal from actb_accbal_history where account=? and branch_code=? and acc_ccy = ? "
//						+ "and BKG_DATE < TO_DATE(?, 'DD-MM-YY') and rownum = ? order by bkg_date desc ";
//				sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
//						"from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? " +
//						"and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account= ? and branch_code= ? and acc_ccy = ? and BKG_DATE <= TO_DATE(?, 'DD-MM-YY'))";
				DateFormat formatter = null;
		        Date convertedDate = null;
	            Date todayDate = new Date();
	            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
		        try {
		            formatter = new SimpleDateFormat("dd-MM-yyyy");
		            convertedDate = (Date) formatter.parse(dt1);
		            todayDate = dateFormatter.parse(dateFormatter.format(new Date()));
		        } catch (Exception e) {
		        	log.info(e.getMessage());
		        }
		        if (todayDate.compareTo(convertedDate) == 0) {
		        	System.out.println("todayDate.compareTo(convertedDate) == 0 - Select MAX");
					sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
	//penambahan BukuBesarGL, Agustus 2016
					"from actb_accbal_history where account in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " + //and branch_code= ? and acc_ccy = ? " +
					"and BKG_DATE = (select max(bkg_date) from actb_accbal_history where account in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) and BKG_DATE < TO_DATE(?, 'DD-MM-YY'))";
					//and branch_code= ? and acc_ccy = ?
		        } else {
		        	System.out.println("todayDate.compareTo(convertedDate) == 0 - Select MAX");
					sql = "select bkg_date, branch_code, account, acc_ccy, acy_closing_bal, lcy_closing_bal, acy_opening_bal, lcy_opening_bal " +
	//penambahan BukuBesarGL, Agustus 2016
					"from actb_accbal_history where account in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " + //and branch_code= ? and acc_ccy = ? " +
					"and BKG_DATE = (select min(bkg_date) from actb_accbal_history where account in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) and BKG_DATE >= TO_DATE(?, 'DD-MM-YY'))";
					//and branch_code= ? and acc_ccy = ?
		        }
		        int i = 1;
				stat = conn.prepareStatement(sql);
	//penambahan BukuBesarGL, Agustus 2016
				stat.setString(i++, acc.trim()); stat.setString(i++, acc2.trim()); stat.setString(i++, acc3.trim()); stat.setString(i++, acc4.trim());
				stat.setString(i++, acc5.trim()); stat.setString(i++, acc6.trim()); stat.setString(i++, acc7.trim()); stat.setString(i++, acc8.trim());
				stat.setString(i++, acc9.trim()); stat.setString(i++, acc10.trim());
				
				System.out.println("acc.trim: "+acc.trim()); System.out.println("acc2.trim: "+acc2.trim()); System.out.println("acc3.trim: "+acc3.trim());
				System.out.println("acc4.trim: "+acc4.trim()); System.out.println("acc5.trim: "+acc5.trim()); System.out.println("acc6.trim: "+acc6.trim());
				System.out.println("acc7.trim: "+acc7.trim()); System.out.println("acc8.trim: "+acc8.trim()); System.out.println("acc9.trim: "+acc9.trim());
				System.out.println("acc10.trim: "+acc10.trim());

				stat.setString(i++, acc.trim()); stat.setString(i++, acc2.trim()); stat.setString(i++, acc3.trim());
				stat.setString(i++, acc4.trim()); stat.setString(i++, acc5.trim()); stat.setString(i++, acc6.trim());
				stat.setString(i++, acc7.trim()); stat.setString(i++, acc8.trim()); stat.setString(i++, acc9.trim()); stat.setString(i++, acc10.trim());

//				stat.setString(7, dt1.trim());
				stat.setString(i++, dt1.trim());
				System.out.println("dt1.trim: "+dt1.trim());
				rs = stat.executeQuery();
				while (rs.next()) {
					sld = new Saldo();
//					sld.setBranch_code(rs.getString("branch_code"));
					sld.setAccount(rs.getString("account"));
//					sld.setAcc_ccy(rs.getString("acc_ccy"));
					if (todayDate.compareTo(convertedDate) == 0) {
						System.out.println("masuk todayDate.compareTo(convertedDate) == 0");
						sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
						sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
						sld.setAcy_opening_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
						sld.setLcy_opening_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));					
					} else {
						sld.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
						sld.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
						sld.setAcy_opening_bal((rs.getBigDecimal("acy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_opening_bal"));
						sld.setLcy_opening_bal((rs.getBigDecimal("lcy_opening_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_opening_bal"));
					}
				}
				if(sld==null){
					System.out.println("masuk if(sld==null)");
					sld = new Saldo();
//					sld.setBranch_code(brnch);
	//penambahan BukuBesarGL, Agustus 2016
					sld.setAccount(acc); sld.setAccount(acc2); sld.setAccount(acc3); sld.setAccount(acc4); sld.setAccount(acc5); sld.setAccount(acc6);
					sld.setAccount(acc7); sld.setAccount(acc8); sld.setAccount(acc9); sld.setAccount(acc10);
//					sld.setAcc_ccy(ccy);
					sld.setAcy_closing_bal(new BigDecimal(0));
					sld.setLcy_closing_bal(new BigDecimal(0));
					sld.setAcy_opening_bal(new BigDecimal(0));
					sld.setLcy_opening_bal(new BigDecimal(0));
				}
			} catch (SQLException ex) {
				log.error("error in getSaldoGL: " + ex.getMessage());
			
			} finally {
				closeConnDb(conn, stat, rs);
			}
			return sld;
		}
	
//	public BigDecimal getSaldo2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto) {
//		BigDecimal saldo = new BigDecimal(0);
//		Connection conn = null;
//		PreparedStatement stat = null;
//		ResultSet rs = null;
//		String sql = "";
//		try {
//			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = "SELECT inc, trn_dt, batch_no, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, "
//					+ "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount FROM "
//					+ "(select rownum as inc, a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
//					+ "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||(  "
//					+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
//				    + ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
//					+ "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
//					+ "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) "
//					+ "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
//					+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount "
//					+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
//					+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
//					+ "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? ";
//					
//			if(!"1".equals(cbsrekoto)){
//				sql += " and a.ac_no not in (?) order by a.AC_ENTRY_SR_NO, a.trn_dt  ";
//			}
//			sql += ") where inc >= ? and inc <= ? ";
//			int i = 1;
//			stat = conn.prepareStatement(sql);
//			stat.setString(i++, tgl1.trim());
//			stat.setString(i++, tgl2.trim());
//			stat.setString(i++, ac_no.trim());
//			stat.setString(i++, brnch.trim());
//			stat.setString(i++, ccy.trim());
//			if(!"1".equals(cbsrekoto)){
//				stat.setString(i++, "195001006");
//			}
//			stat.setInt(i++, begin);
//			stat.setInt(i++, delta);
//			rs = stat.executeQuery();
//			BigDecimal mutDb = new BigDecimal(0);
//			BigDecimal mutCb = new BigDecimal(0);
//			while (rs.next()) {
//				if ("D".equals(rs.getString("drcr_ind"))) {
//					if ("IDR".equals(rs.getString("ac_ccy"))) {
//						mutDb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
//					} else {
//						mutDb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
//					}
//				} else {
//					mutDb = new BigDecimal(0);
//				}
//				if ("C".equals(rs.getString("drcr_ind"))) {
//					if ("IDR".equals(rs.getString("ac_ccy"))) {
//						mutCb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
//					} else {
//						mutCb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
//					}
//				} else {
//					mutCb = new BigDecimal(0);
//				}
//				if ("C".equals(rs.getString("drcr_ind"))) {		
//					saldo = saldo.add(mutCb);
//				} else if ("D".equals(rs.getString("drcr_ind"))) {
//					saldo = saldo.subtract(mutDb);
//				}	
//				System.out.println("SQL getSaldo2" + sql);
//			}
//		} catch (SQLException ex) {
//			log.error(" getSaldo2 : " + ex.getMessage());
//		} finally {
//			closeConnDb(conn, stat, rs);
//		}
//		return saldo;
//	}
		
		//penambahan allcab-allval, AGUSTUS 2016	
		//public BigDecimal getSaldo2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto) {
		public BigDecimal getSaldo2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto, String allcab, String allval) {
			BigDecimal saldo = new BigDecimal(0);
			Connection conn = null;
			PreparedStatement stat = null;
			ResultSet rs = null;
			String sql = "";
			try {
				conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
	//penambahan query untuk cabang
				sql = "SELECT inc, trn_dt, batch_no, ac_branch, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, "
						+ "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount FROM "
						+ "(select rownum as inc, a.trn_dt, a.batch_no, a.ac_branch, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
						+ "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||(  "
						+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
						+ ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
						+ "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
						+ "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) "
						+ "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
						+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount "
						+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
						+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
//								+ "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? ";
						+ "a.ac_no = ? ";
								
	//penambahan allcab-allval AGUSTUS 2016
				if("1".equals(allcab)) //cek semua cabang
				{
					if ("2".equals(allval)) //cek semua valuta
					{
						//tidak ada query
					}
					else //tidak cek semua valuta
					{
							sql += " AND a.ac_ccy = ? ";
					}
				}
				else //tidak cek semua cabang
				{
					if("2".equals(allval)) //cek semua valuta
					{
							//tidak ada query
					}
					else //tidak cek semua valuta
					{
						sql += " AND a.ac_ccy = ? ";
					}
						sql += " AND a.ac_branch = ? ";
				}
	//
				if(!"1".equals(cbsrekoto)){
						sql += " and a.ac_no not in (?) order by a.AC_ENTRY_SR_NO, a.trn_dt  ";
				}
				sql += ") where inc >= ? and inc <= ? ";
				int i = 1;
						
				stat = conn.prepareStatement(sql);
						
				stat.setString(i++, tgl1.trim());
				stat.setString(i++, tgl2.trim());
				stat.setString(i++, ac_no.trim());
//						stat.setString(i++, brnch.trim());
//						stat.setString(i++, ccy.trim());

	//penambahan allcab-allval, AGUSTUS 2016
				if("1".equals(allcab)) //cek semua cabang
				{
					if("2".equals(allval)) //cek semua valuta
					{
							//tidak ada query
					}
					else //tidak cek semua valuta
					{
							stat.setString(i++, ccy.trim());
					}
				}
				else //tidak cek semua cabang
				{					
					if("2".equals(allval)) //cek semua valuta
					{
							//tidak ada query
					}
					else //tidak cek semua valuta
					{
							stat.setString(i++, ccy.trim());
					}
					stat.setString(i++, brnch.trim());
				}
				if(!"1".equals(cbsrekoto)){
					stat.setString(i++, "195001006");
				}
				stat.setInt(i++, begin);
				stat.setInt(i++, delta);
				rs = stat.executeQuery();
				BigDecimal mutDb = new BigDecimal(0);
				BigDecimal mutCb = new BigDecimal(0);
				while (rs.next()) {
					if ("D".equals(rs.getString("drcr_ind"))) {
						if ("IDR".equals(rs.getString("ac_ccy"))) {
							mutDb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
						} else {
							mutDb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
						}
					} else {
						mutDb = new BigDecimal(0);
					}
					if ("C".equals(rs.getString("drcr_ind"))) {
						if ("IDR".equals(rs.getString("ac_ccy"))) {
							mutCb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
						} else {
							mutCb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
						}
					} else {
						mutCb = new BigDecimal(0);
					}
					if ("C".equals(rs.getString("drcr_ind"))) {		
						saldo = saldo.add(mutCb);
					} else if ("D".equals(rs.getString("drcr_ind"))) {
						saldo = saldo.subtract(mutDb);
					}				
				}
			} catch (SQLException ex) {
				log.error(" getSaldo2 : " + ex.getMessage());
			} finally {
				closeConnDb(conn, stat, rs);
			}
			return saldo;
		}
		
//=============================================================================================================================================================================================

//penambahan BukuBesarGL, Agustus 2016
//public BigDecimal getSaldo2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto, String allcab, String allval) {
	public BigDecimal getSaldoGL2(String tgl1, String tgl2, String ac_no, String ac_no2, String ac_no3, String ac_no4, String ac_no5, 
			String ac_no6, String ac_no7, String ac_no8, String ac_no9, String ac_no10, int begin, int delta) {
		BigDecimal saldo = new BigDecimal(0);
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		System.out.println("TEST getsaldoGL2 before try-catch");
		try {
			System.out.println("TEST getsaldo2 before connect");
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//					penambahan query untuk cabang
			sql = "SELECT inc, trn_dt, batch_no, ac_branch, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, "
				+ "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount FROM "
				+ "(select rownum as inc, a.trn_dt, a.batch_no, a.ac_branch, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, "
				+ "s.trn_desc ||':'||(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||(  "
				+ "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no "
				+ ") || ' ' || (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c "
				+ "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no "
				+ "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) "
				+ "group by s.user_ref_no , c.cust_name) CF_ADDLTEXT,"
				+ "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount "
				+ "from ACVWS_ALL_AC_ENTRIES_HIST a inner join sttms_trn_code s on a.trn_code = s.trn_code "
				+ "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
//						+ "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? ";
				+ "a.ac_no in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			sql += ") where inc >= ? and inc <= ? ";
			int i = 1;
					
			System.out.println("TEST getsaldoGL2 before stat SQL");
			stat = conn.prepareStatement(sql);
			System.out.println("TEST getsaldoGL2 stat SQL");
					
			stat.setString(i++, tgl1.trim());
			stat.setString(i++, tgl2.trim());
//penambahan BukuBesarGL, Agustus 2016
			stat.setString(i++, ac_no.trim()); stat.setString(i++, ac_no2.trim()); stat.setString(i++, ac_no3.trim()); stat.setString(i++, ac_no4.trim());
			stat.setString(i++, ac_no5.trim()); stat.setString(i++, ac_no6.trim()); stat.setString(i++, ac_no7.trim()); stat.setString(i++, ac_no8.trim());
			stat.setString(i++, ac_no9.trim()); stat.setString(i++, ac_no10.trim());
			//stat.setString(i++, brnch.trim());
			//stat.setString(i++, ccy.trim());
			System.out.println("tgl1: "+tgl1.trim());
			System.out.println("tgl2: "+tgl2.trim());
			System.out.println("ac_no: "+ac_no.trim()); System.out.println("ac_no2: "+ac_no2.trim()); System.out.println("ac_no3: "+ac_no3.trim());
			System.out.println("ac_no4: "+ac_no4.trim()); System.out.println("ac_no5: "+ac_no5.trim()); System.out.println("ac_no6: "+ac_no6.trim());
			System.out.println("ac_no7: "+ac_no7.trim()); System.out.println("ac_no8: "+ac_no8.trim()); System.out.println("ac_no9: "+ac_no9.trim());
			System.out.println("ac_no10: "+ac_no10.trim());
//							
			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			rs = stat.executeQuery();
			BigDecimal mutDb = new BigDecimal(0);
			BigDecimal mutCb = new BigDecimal(0);
			while (rs.next()) {
				if ("D".equals(rs.getString("drcr_ind"))) {
					if ("IDR".equals(rs.getString("ac_ccy"))) {
						mutDb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
					} else {
						mutDb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
					}
				} else {
					mutDb = new BigDecimal(0);
				}
				if ("C".equals(rs.getString("drcr_ind"))) {
					if ("IDR".equals(rs.getString("ac_ccy"))) {
						mutCb = (rs.getBigDecimal("lcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_amount");
					} else {
						mutCb = (rs.getBigDecimal("fcy_amount") == null) ? new BigDecimal(0) : rs.getBigDecimal("fcy_amount");
					}
				} else {
					mutCb = new BigDecimal(0);
				}
				if ("C".equals(rs.getString("drcr_ind"))) {		
					saldo = saldo.add(mutCb);
				} else if ("D".equals(rs.getString("drcr_ind"))) {
					saldo = saldo.subtract(mutDb);
				}				
			}
		} catch (SQLException ex) {
			log.error("Eksepsi getSaldo2GL : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return saldo;
	}
	
	public List getListPCdMcb(String dt, String network, String rtgs,
			String trncd1, String trncd2, String trncd3, String norekttpn) {
		List lbb = new ArrayList();
		Pcode bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			String pcd = "";
			if ("RTGSBI".equals(network)) {
				pcd = "ROIB";
			} else if ("SKNBI".equals(network)) {
				pcd = "SOIB";
			}
			if ("out".equals(rtgs)) {
				sql = "select distinct pc as product_code, status, sum(total) as total, sum(nominal) as nominal from ( "
						+ "select ( case when product_code = '"
						+ pcd
						+ "' then 'IB/CMS' when product_code <> 'ROIB' then 'MCB' end ) pc, status, total, nominal "
						+ "from( select distinct product_code, status, count(*) as total, sum(txn_amount) as nominal "
						+ "from( select contract_ref_no, dispatch_ref_no, checker_id, exception_queue, product_code, txn_amount,"
						+ "(case when dispatch_ref_no is not null and checker_id is not null and exception_queue = '##' then 'PROCEED' else 'UNPROCEED' end) status "
						+ "from PCTBS_CONTRACT_MASTER where activation_dt = TO_DATE(?, 'DD-MM-YY') and network = ? )"
						+ "group by product_code, status "
						+ "order by product_code, status desc )"
						+ ")group by pc, status order by pc, status desc ";
			} else if ("in".equals(rtgs)) {
				sql = "select count(*) as total, sum(lcy_amount) as nominal "
						+ "from actb_daily_log where trn_code in (?, ?, ?) and (length(ac_no) = ? or ac_no = ?) and "
						+ "trn_dt =TO_DATE(?, 'DD-MM-YY') and user_id = ? ";
			}

			stat = conn.prepareStatement(sql);
			if ("out".equals(rtgs)) {
				stat.setString(1, dt);
				stat.setString(2, network);
			} else if ("in".equals(rtgs)) {
				stat.setString(1, trncd1);
				stat.setString(2, trncd2);
				stat.setString(3, trncd3);
				stat.setInt(4, 10);
				stat.setString(5, norekttpn);
				stat.setString(6, dt);
				stat.setString(7, "FLEXGW");
			}
			rs = stat.executeQuery();

			while (rs.next()) {
				bb = new Pcode();
				if ("out".equals(rtgs)) {
					bb.setPcd(rs.getString("product_code"));
					bb.setSts(rs.getString("status"));
				} else if ("in".equals(rtgs)) {
					bb.setPcd("-");
					bb.setSts("-");
				}
				bb.setTot(rs.getInt("total"));
				if (rs.getBigDecimal("nominal") == null) {
					bb.setNom(new BigDecimal(0));
				} else {
					bb.setNom(rs.getBigDecimal("nominal"));
				}
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListPCd : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public List getListPCdIntf(String rtgs) {
		List lbb = new ArrayList();
		Pcode bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
			if ("out".equals(rtgs)) {
//				sql = "SELECT DISTINCT substring(reltrn,4,4) AS product_code, Status, count(*) AS total,"
//						+ "sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) as nominal FROM dbo.T_RTIFTSOUT "
//						+ "GROUP BY substring(reltrn,4,4), Status ORDER BY product_code";
				sql = "SELECT DISTINCT product_code, Status, sum(total) AS total, sum(nominal) AS nominal " +
						"FROM (" +
						"SELECT CASE WHEN substring(reltrn,4,4) = 'CROP' THEN 'MCB' ELSE 'IB/CMS + TITIPAN' END AS product_code," +
						"Status, count(*) AS total, sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) as nominal FROM dbo.T_RTIFTSOUT " +
						"GROUP BY substring(reltrn,4,4), Status) a GROUP BY product_code, Status ";
			} else if ("in".equals(rtgs)) {
				sql = "SELECT Status, count(*) AS total, "
						+ "sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) AS nominal FROM dbo.T_RTIFTSIN "
						+ "GROUP BY Status ORDER BY Status";
			}
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Pcode();
				if ("out".equals(rtgs)) {
					bb.setPcd(rs.getString("product_code"));
				} else {
					bb.setPcd("-");
				}
				bb.setSts(rs.getString("status"));
				bb.setTot(rs.getInt("total"));
				bb.setNom(rs.getBigDecimal("nominal"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
		
			log.error(" getListPCdIntf : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public List getListPCdIntfSkn(String skn, String tbl) {
		List lbb = new ArrayList();
		Pcode bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getSknDS().getConnection();
			if ("out".equals(skn)) {
//				sql = "SELECT DISTINCT substring(od_ref_no,4,4) AS product_code, CAST(od_status1 AS CHAR(1)) +''+ CAST(od_status2 AS CHAR(1)) AS status, "
//						+ "count(*) AS total, sum(od_amount) AS nominal FROM "
//						+ tbl
//						+ " "
//						+ "GROUP BY CAST(od_status1 AS CHAR(1)) +''+ CAST(od_status2 AS CHAR(1)), substring(od_ref_no,4,4) ";
				sql = "SELECT DISTINCT product_code, CAST(od_status1 AS CHAR(1)) +''+ CAST(od_status2 AS CHAR(1)) AS status, " +
						"sum(total) AS total, sum(nominal) AS nominal " +
						"FROM (" +
						"SELECT CASE WHEN product_code = 'CSOP' THEN 'MCB' ELSE 'IB/CMS/SKNMSSL + TITIPAN' END product_code, " +
						"od_status1, od_status2, sum(total) total, sum(nominal) nominal " +
						"FROM( " +
						"SELECT DISTINCT substring(od_ref_no,4,4) AS product_code, od_status1, od_status2, count(*) AS total, sum(od_amount) AS nominal " +
						"FROM " + tbl + " GROUP BY od_status1, od_status2, substring(od_ref_no,4,4)) asd " +
						"GROUP BY product_code, od_status1, od_status2) asddsf " +
						"GROUP BY product_code, od_status1, od_status2";
					
			} else if ("in".equals(skn)) {
				sql = "SELECT DISTINCT CAST(id_status_host AS CHAR(1)) AS status, '-' AS product_code, " +
    			"count(*) AS total, sum(id_amount) AS nominal " +
    			"FROM " + tbl + " " +
    			"GROUP BY id_status_host";
			}
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Pcode();
				if ("out".equals(skn)) {
					bb.setPcd(rs.getString("product_code"));
				} else {
					bb.setPcd("-");
				}
				bb.setSts(rs.getString("status"));
				bb.setTot(rs.getInt("total"));
				bb.setNom(rs.getBigDecimal("nominal"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
		
			log.error(" getListPCdIntf : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public Gl getGl(String acc) {
		Gl gl = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select gl_code, gl_desc from gltm_glmaster where gl_code = ? ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc);
			rs = stat.executeQuery();
			if (rs.next()) {
				gl = new Gl();
				gl.setGl_code(rs.getString("gl_code"));
				gl.setGl_desc(rs.getString("gl_desc"));
			}
		} catch (SQLException ex) {
			log.error("error getGl : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return gl;
	}
	
	public List getLspCurrent(String acc, String branch, String ccy, String allcab, String allval) {
		List lsp = new ArrayList();
		Lsp sp = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = "select ac_no, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan " +
//					"from (select h.ac_no, h.ac_branch, h.ac_ccy," +
//					"sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, " +
//					"sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL " +
//					"from ACVWS_ALL_AC_ENTRIES_HIST h " +
//					"where h.trn_dt = to_date(sysdate, 'DD-MM-YY') and h.ac_no = ? ";	
			sql = "select ac_no, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan " +
					"from (select h.ac_no, h.ac_branch, h.ac_ccy," +
					"sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, " +
					"sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL " +
					"from actb_daily_log h " +
					"where h.ac_no = ? and h.delete_stat <> ? ";
			if (allval == null){
				sql += " and h.ac_ccy = ? ";
			}
			if ("1".equals(allcab)) {				
			} else {
				sql += " and h.ac_branch = ? ";
			}
			sql += "group by h.ac_no, h.ac_branch, h.ac_ccy) order by ac_ccy";
			
			System.out.println("SQL getLspCurrent :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, acc);
			stat.setString(i++, "D");
			if (allval == null){
				stat.setString(i++, ccy);
			}
			if ("1".equals(allcab)) {				
			} else {
				stat.setString(i++, branch);
			}
			System.out.println("ccy :" + ccy);
			System.out.println("branch :" + branch);
			System.out.println("acc :" + acc);
			System.out.println("allcab :" + allcab);
			System.out.println("allval :" + allval);
			rs = stat.executeQuery();
			while (rs.next()) {
				sp = new Lsp();
				sp.setBranch_code(rs.getString("ac_branch"));
				sp.setGl_code(rs.getString("ac_no"));
				sp.setCcy(rs.getString("ac_ccy"));
				sp.setSld_prcobaan((rs.getBigDecimal("saldoPercobaan") == null) ? new BigDecimal(0) : rs.getBigDecimal("saldoPercobaan"));
				lsp.add(sp);
			}
		} catch (SQLException ex) {
			log.error("error getLspCurrent : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lsp;
	}

	public List getListLsp(String ccy, String brnch, String gl_a, String gl_b, String allcab, String allval, String cbxspnoll) {
		List llsp = new ArrayList();
		Lsp bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			if ("1".equals(allcab)) {
				sql = "  select ac_no, gl_desc, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan "
						+ "from (select h.ac_no, a.gl_desc, h.ac_branch, ac_ccy, "
						+ "sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, "
						+ "sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL "
						+ "from ACVWS_ALL_AC_ENTRIES_HIST h, gltm_glmaster a "
						+ "where a.leaf = ? and h.ac_no >= ? and h.ac_no <= ? and  a.gl_code = h.ac_no ";						
				if (allval == null){
					sql += " and h.ac_ccy = ? ";
				}
				
				sql+=  "group by h.ac_no, a.gl_desc, h.ac_branch, h.ac_ccy ) ";
			} else {
				sql = "  select ac_no, gl_desc, ac_branch, ac_ccy, debitGL, creditGL, creditGL - debitGL as saldoPercobaan "
						+ "from (select h.ac_no, a.gl_desc, h.ac_branch, ac_ccy, "
						+ "sum(decode(h.drcr_ind, 'D', decode(h.ac_ccy,  'IDR', h.lcy_amount, h.fcy_amount), 0)) as debitGL, "
						+ "sum(decode(h.drcr_ind, 'C', decode(h.ac_ccy, 'IDR', h.lcy_amount, h.fcy_amount), 0)) as creditGL "
						+ "from ACVWS_ALL_AC_ENTRIES_HIST h, gltm_glmaster a "
						+ "where a.leaf = ? and h.ac_branch = ? and h.ac_no >= ? and h.ac_no <= ? and  a.gl_code = h.ac_no ";						
				if (allval == null){
					sql += " and h.ac_ccy = ? ";
				}
				sql += "group by h.ac_no, a.gl_desc, h.ac_branch, h.ac_ccy ) ";
			}
			if (cbxspnoll == null){
				sql += " where creditGL - debitGL <> ? ";
	        }
			sql+= " order by ac_branch, ac_no ";
			int i = 1;
			stat = conn.prepareStatement(sql);
			if ("1".equals(allcab)) {
				stat.setString(i++, "Y");
				stat.setString(i++, gl_a);
				stat.setString(i++, gl_b);
			} else {
				stat.setString(i++, "Y");
				stat.setString(i++, brnch);
				stat.setString(i++, gl_a);
				stat.setString(i++, gl_b);
			}
			if (allval == null){
				stat.setString(i++, ccy);	
			}
			if (cbxspnoll == null){
				stat.setInt(i++, 0);
	        }
			rs = stat.executeQuery();
			BigDecimal temp = new BigDecimal(0);
			while (rs.next()) {
				bb = new Lsp();
				bb.setGl_code(rs.getString("ac_no"));
				bb.setGl_desc(rs.getString("gl_desc"));
				bb.setBranch_code(rs.getString("ac_branch"));
				bb.setCcy(rs.getString("ac_ccy"));
				bb.setMov_lcy(rs.getBigDecimal("saldoPercobaan"));
				llsp.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListLSp : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return llsp;
	}
	
	public List getListLspTotal(String ccy, String brnch, String gl_a, String gl_b, String allcab, String allval, String cbxspnoll) {
		List llsp = new ArrayList();
		Lsp bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			if ("1".equals(allcab)) {
				sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, "  +
						"h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
						"from actb_accbal_history h, gltm_glmaster a " +
						"where a.leaf = ? and a.gl_code = h.account and h.account>= ? and h.account <= ? ";						
				if (allval == null){
					sql += " and h.acc_ccy = ? ";
				}
				sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account  and acc_ccy = h.acc_ccy and branch_code= h.branch_code ";
				if (allval == null){
					sql += " and acc_ccy = ? ";
				}
				sql += ") " +"order by h.account, h.branch_code, h.BKG_DATE asc ";
			} else {
				sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, "  +
						"h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
						"from actb_accbal_history h, gltm_glmaster a " +
						"where a.leaf = ? and a.gl_code = h.account and h.account>= ? and h.account <= ? and h.branch_code= ? ";						
				if (allval == null){
					sql += " and h.acc_ccy = ? ";
				}
				sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account and acc_ccy = h.acc_ccy and branch_code= h.branch_code and branch_code=? ";
				if (allval == null){
					sql += " and acc_ccy = ? ";
				}
				sql += ") " +"order by h.account, h.branch_code, h.BKG_DATE asc ";
			}
			int i = 1;
			stat = conn.prepareStatement(sql);
			if ("1".equals(allcab)) {
				stat.setString(i++, "Y");
				stat.setString(i++, gl_a);
				stat.setString(i++, gl_b);
				if (allval == null){
					stat.setString(i++, ccy);
					stat.setString(i++, ccy);
				}
			} else {
				stat.setString(i++, "Y");
				stat.setString(i++, gl_a);
				stat.setString(i++, gl_b);
				stat.setString(i++, brnch);
				if (allval == null){
					stat.setString(i++, ccy);
				}
				stat.setString(i++, brnch);
				if (allval == null){
					stat.setString(i++, ccy);
				}
			}
			rs = stat.executeQuery();
			BigDecimal temp = new BigDecimal(0);
			while (rs.next()) {
				bb = new Lsp();
				bb.setGl_code(rs.getString("account"));
				bb.setGl_desc(rs.getString("gl_desc"));
				bb.setBranch_code(rs.getString("branch_code"));
				bb.setCcy(rs.getString("acc_ccy"));
//				BigDecimal lspCurr = getLspCurrent(rs.getString("account").trim(), rs.getString("branch_code").trim(), rs.getString("acc_ccy").trim());
				BigDecimal t = new BigDecimal(0);
				if("IDR".equals(ccy.toUpperCase())){
//					t = rs.getBigDecimal("lcy_closing_bal").add(lspCurr);
					bb.setMov_lcy(t);					
				} else {
//					t = rs.getBigDecimal("acy_closing_bal").add(lspCurr);
					bb.setMov_lcy(t);								
				}

				if (cbxspnoll == null){
					if(t.compareTo(new BigDecimal(0)) != 0){	
			        	llsp.add(bb);					
					} 
		        } else {
		        	llsp.add(bb);
		        }
			}
		} catch (SQLException ex) {
			log.error(" getListLspTotal : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return llsp;
	}

	public List getListGL(String gl_fr, String gl_to) {
		List lbb = new ArrayList();
		Gl bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select gl_code, gl_desc from gltm_glmaster where gl_code >= ? and gl_code <= ? order by gl_code asc";
			stat = conn.prepareStatement(sql);
			stat.setString(1, gl_fr);
			stat.setString(2, gl_to);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Gl();
				bb.setGl_code(rs.getString("gl_code"));
				bb.setGl_desc(rs.getString("gl_desc"));
				lbb.add(bb);
			}
			System.out.println("sql : "+sql);
		} catch (SQLException ex) {
			log.error(" getListGL : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getListLspBefore(String ccy, String brnch, String gl_a, String allcab, String allval, String cbxspnoll) {
		List llsp = new ArrayList();
		Lsp bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			if ("1".equals(allcab)) {
				sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, "  +
						"h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
						"from actb_accbal_history h, gltm_glmaster a " +
						"where a.leaf = ? and a.gl_code = h.account and h.account= ? ";						
				if (allval == null){
					sql += " and h.acc_ccy = ? ";
				}
				sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account  and acc_ccy = h.acc_ccy and branch_code= h.branch_code ";
				if (allval == null){
					sql += " and acc_ccy = ? ";
				}
				sql += ") " +"order by h.account, h.branch_code, h.acc_ccy, h.BKG_DATE asc ";
				
				System.out.println("Allcab");
				System.out.println("SQL getListLspBefore: " + sql);
				
			} else {
				sql = "select h.BKG_DATE, h.branch_code, h.account, a.gl_desc, "  +
						"h.acc_ccy, h.acy_closing_bal, h.lcy_closing_bal, h.acy_opening_bal, h.lcy_opening_bal " +
						"from actb_accbal_history h, gltm_glmaster a " +
						"where a.leaf = ? and a.gl_code = h.account and h.account= ? and h.branch_code= ? ";						
				if (allval == null){
					sql += " and h.acc_ccy = ? ";
				}
				sql += "and h.BKG_DATE = (select max(BKG_DATE) as mx_dt from actb_accbal_history where account = h.account and acc_ccy = h.acc_ccy and branch_code= h.branch_code and branch_code=? ";
				if (allval == null){
					sql += " and acc_ccy = ? ";
				}
				sql += ") " +"order by h.account, h.branch_code, h.acc_ccy, h.BKG_DATE asc ";
				
				System.out.println("allval");
				System.out.println("SQL getListLspBefore: " + sql);
			}
			int i = 1;
			stat = conn.prepareStatement(sql);
			if ("1".equals(allcab)) {
				stat.setString(i++, "Y");
				stat.setString(i++, gl_a);
				if (allval == null){
					stat.setString(i++, ccy);
					stat.setString(i++, ccy);
				}
			} else {
				stat.setString(i++, "Y");
				stat.setString(i++, gl_a);
				stat.setString(i++, brnch);
				if (allval == null){
					stat.setString(i++, ccy);
				}
				stat.setString(i++, brnch);
				if (allval == null){
					stat.setString(i++, ccy);
				}
			}
			System.out.println("ccy :" + ccy);
			System.out.println("brnch :" + brnch);
			System.out.println("gl_a :" + gl_a);
			System.out.println("allcab :" + allcab);
			System.out.println("allval :" + allval);
			System.out.println("cbxspnoll :" + cbxspnoll);
			rs = stat.executeQuery();
			BigDecimal temp = new BigDecimal(0);
			while (rs.next()) {
				bb = new Lsp();
				bb.setGl_code(rs.getString("account"));
				bb.setGl_desc(rs.getString("gl_desc"));
				bb.setBranch_code(rs.getString("branch_code"));
				bb.setCcy(rs.getString("acc_ccy"));
				bb.setLcy_closing_bal((rs.getBigDecimal("lcy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("lcy_closing_bal"));
				bb.setAcy_closing_bal((rs.getBigDecimal("acy_closing_bal") == null) ? new BigDecimal(0) : rs.getBigDecimal("acy_closing_bal"));
	        	llsp.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListLspBefore : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return llsp;
	}
	
//	public List getcontohbuku2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto, BigDecimal saldo) {
//		List lbb = new ArrayList();
//		Bukubesar bb = null;
//		Connection conn = null;
//		PreparedStatement stat = null;
//		ResultSet rs = null;
//		String sql = "";
//		try {
//			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			
//			sql = "SELECT inc, trn_dt, batch_no, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, " +
//			      "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount, rcc_no " +
//			      "FROM (select rownum as inc, a.trn_dt, a.batch_no, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " +
//			      "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
//			      "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
//				  "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
//		    	  ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' '|| (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
//				  "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
//				  "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
//				  "group by s.user_ref_no , c.cust_name) || ', ' || " +
//			      "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
//			      "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " + 
//                  "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
//				  "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
//				  //"(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
//				  //"and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
//                                    "(SELECT distinct y.txn_mis_1 FROM MITB_CLASS_MAPPING y WHERE y.UNIT_REF_NO = a.trn_ref_no AND a.ac_branch = y.branch_code) AS rcc_no " +
//			      "from ACVWS_ALL_AC_ENTRIES_HIST a, sttms_trn_code s, setb_event_log c  " +
//			      "where a.trn_code = s.trn_code " +
//			      "and a.trn_code = s.trn_code " +
//			      "and c.event_reference(+) = a.trn_ref_no " +
//			      "and c.esn(+) = a.event_sr_no " +
//				  "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
//				  "a.ac_no = ? and a.ac_branch = ? ";
//		    if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//		    	sql += " and a.ac_ccy = ? ";		    	
//		    }
//			if(!"1".equals(cbsrekoto)){
//				sql += " and a.ac_no not in (?) order by a.AC_ENTRY_SR_NO, a.trn_dt ";
//			}
//			sql += ") where inc >= ? and inc <= ? ";
//			int i = 1;
//			System.out.println("SQL BUKu Besar " + sql);
//			stat = conn.prepareStatement(sql);
//			stat.setString(i++, tgl1.trim());
//			stat.setString(i++, tgl2.trim());
//			stat.setString(i++, ac_no.trim());
//			stat.setString(i++, brnch.trim());
//			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//				stat.setString(i++, ccy.trim());		    	
//		    }
//			if(!"1".equals(cbsrekoto)){
//				stat.setString(i++, "195001006");
//			}
//			stat.setInt(i++, begin);
//			stat.setInt(i++, delta);
//			rs = stat.executeQuery();
//			totMutCb = new BigDecimal(0);
//			totMutDb = new BigDecimal(0);
//			BigDecimal mutDb = new BigDecimal(0);
//			BigDecimal mutCb = new BigDecimal(0);
//			BigDecimal mutDbEq = new BigDecimal(0);
//			BigDecimal mutCbEq = new BigDecimal(0);
//			while (rs.next()) {
//				bb = new Bukubesar();
//				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
//				bb.setTrn_dt(rs.getDate("trn_dt"));
//				bb.setValue_dt(rs.getDate("value_dt"));
//				bb.setDrcr(rs.getString("drcr_ind"));
//				if(rs.getString("batch_no") != null){
//					bb.setBatch_no(rs.getString("batch_no"));
//				}else{
//					bb.setBatch_no("");
//				}
//				bb.setAc_no(rs.getString("ac_no"));
//				bb.setMaker_id(rs.getString("user_id"));
//				bb.setAuth_id(rs.getString("auth_id"));
//				bb.setEvent(rs.getString("event"));
//				bb.setTrn_code(rs.getString("trn_code"));
//				bb.setKet(rs.getString("CF_ADDLTEXT"));
//				bb.setTrn_code(rs.getString("trn_code"));
//				bb.setAc_ccy(rs.getString("ac_ccy"));
//				bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
//				bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
//				if ("D".equals(bb.getDrcr().trim())) {
//					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
//						mutDb = bb.getLcy_amount();
//					} else {
//						mutDb = bb.getFcy_amount();
//						mutDbEq = bb.getLcy_amount();
//					}
//				} else {
//					mutDb = new BigDecimal(0);
//					mutDbEq =new BigDecimal(0);
//				}
//				totMutDb = totMutDb.subtract(mutDb);
//				bb.setAmt_db_eq(mutDbEq);
//				bb.setAmt_db(mutDb);
//				if ("C".equals(bb.getDrcr().trim())) {
//					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
//						mutCb = bb.getLcy_amount();
//					} else {
//						mutCb = bb.getFcy_amount();
//						mutCbEq = bb.getLcy_amount();
//					}
//				} else {
//					mutCb = new BigDecimal(0);
//					mutCbEq = new BigDecimal(0);
//				}
//			
//				bb.setAmt_cr_eq(mutCbEq);
//				bb.setAmt_cr(mutCb);
//				totMutCb = totMutCb.add(mutCb);
//				if ("C".equals(bb.getDrcr().trim())) {		
//					saldo = saldo.add(bb.getAmt_cr());
//				} else if ("D".equals(bb.getDrcr().trim())) {
//					saldo = saldo.subtract(bb.getAmt_db());
//				}
//				bb.setSaldo(saldo);
//				bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
//				lbb.add(bb);	
//				System.out.println("SQL getListBukubesar" + sql);
//				System.out.println("SQL ac_no" + ac_no);
//				System.out.println("SQL branch" + brnch);
//				System.out.println("SQL ac_ccy" + ccy);
//				System.out.println("SQL tgl1" + tgl1);
//				System.out.println("SQL tgl2" + tgl2);
//				System.out.println("SQL delta" + delta);
//				System.out.println("SQL begin" + begin);
//			}
//		} catch (SQLException ex) {
//			log.error(" getListBukubesar : " + ex.getMessage());
//		} finally {
//			closeConnDb(conn, stat, rs);
//		}
//		return lbb;
//	}

	//penambahan allcab-allval, AGUSTUS 2016	
	//	public List getcontohbuku2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto, BigDecimal saldo)
	public List getcontohbuku2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto, BigDecimal saldo, String allcab, String allval) {
		List lbb = new ArrayList();
		Bukubesar bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			//penambahan query untuk cabang
			sql = "SELECT inc, trn_dt, txn_dt_time, ac_branch, batch_no, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, " +
			      "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount, rcc_no " +
			      "FROM (select rownum as inc, a.trn_dt, TO_CHAR (d.txn_dt_time, 'YYYY-MM-DD HH24:MI:SS') AS txn_dt_time, a.batch_no, a.AC_BRANCH, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " +
			      "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
			      "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
				  "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
		    	  ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' '|| (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
				  "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
				  "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
				  "group by s.user_ref_no , c.cust_name) || ', ' || " +
			      "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
			      "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " + 
                  "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
				  "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
				  //"(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
				  //"and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
                                    "(SELECT distinct y.txn_mis_1 FROM MITB_CLASS_MAPPING y WHERE y.UNIT_REF_NO = a.trn_ref_no AND a.ac_branch = y.branch_code) AS rcc_no " +
			      "from ACVWS_ALL_AC_ENTRIES_HIST a, ACVW_ALL_AC_ENTRIES d, sttms_trn_code s, setb_event_log c  " +
			      "where a.trn_code = s.trn_code " +
			      "and a.trn_code = s.trn_code " +
			      "AND a.trn_code = d.trn_code " +
			      "AND a.ac_branch = d.ac_branch " + 
			      "AND a.ac_no = d.ac_no " +
			      "and a.ac_entry_sr_no = d.ac_entry_sr_no " +
			      "AND a.trn_ref_no = d.trn_ref_no " +
			      "AND a.trn_dt = d.trn_dt " +
			      "and c.event_reference(+) = a.trn_ref_no " +
			      "and c.esn(+) = a.event_sr_no " +
				  "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
//				  "a.ac_no = ? and a.ac_branch = ? ";
				  "a.ac_no = ?";
		    if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
		    } else {	    	
//		    	sql += " and a.ac_ccy = ? ";
		    	
		    	//penambahan allcab-allval, AGUSTUS 2016
		    	if("1".equals(allcab)) //cek semua cabang
				{
					if ("2".equals(allval)) //cek semua valuta
					{
						//tidak ada query
					}
					else //tidak cek semua valuta
					{
						sql += " AND a.ac_ccy = ? ";
					}
				}
				else //tidak cek semua cabang
				{
					if("2".equals(allval)) //cek semua valuta
					{
						//tidak ada query
					}
					else //tidak cek semua valuta
					{
						sql += " AND a.ac_ccy = ? ";
					}
					sql += " AND a.ac_branch = ? ";
				}
		    }
//
			if(!"1".equals(cbsrekoto)){
				sql += " and a.ac_no not in (?) order by a.AC_ENTRY_SR_NO, a.trn_dt ";
			}
			sql += ") where inc >= ? and inc <= ? ";
			int i = 1;
			
			System.out.println("SQL Buku besar :" + sql);
			System.out.println("tgl1 :" + tgl1);
			System.out.println("tgl2 :" + tgl2);
			System.out.println("ac_no :" + ac_no);
			stat = conn.prepareStatement(sql);
			
			stat.setString(i++, tgl1.trim());
			stat.setString(i++, tgl2.trim());
			stat.setString(i++, ac_no.trim());
			//stat.setString(i++, brnch.trim());
			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
		    } else {	    	
//				stat.setString(i++, ccy.trim());
		    	
		    	//penambahan allcab-allval, AGUSTUS 2016
				if("1".equals(allcab)) //cek semua cabang
				{
					if("2".equals(allval)) //cek semua valuta
					{
						//tidak ada query
					}
					else //tidak cek semua valuta
					{
						stat.setString(i++, ccy.trim());
					}
				}
				else //tidak cek semua cabang
				{
										
					if("2".equals(allval)) //cek semua valuta
					{
						//tidak ada query
					}
					else //tidak cek semua valuta
					{
						stat.setString(i++, ccy.trim());
					}
					stat.setString(i++, brnch.trim());
				}
		    }
//
			if(!"1".equals(cbsrekoto)){
				stat.setString(i++, "195001006");
			}
			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			System.out.println("ccy :" + ccy);
			System.out.println("brnch :" + brnch);
			System.out.println("cbsrekoto :" + cbsrekoto);
			System.out.println("begin :" + begin);
			System.out.println("delta :" + delta);
			rs = stat.executeQuery();
			totMutCb = new BigDecimal(0);
			totMutDb = new BigDecimal(0);
			BigDecimal mutDb = new BigDecimal(0);
			BigDecimal mutCb = new BigDecimal(0);
			BigDecimal mutDbEq = new BigDecimal(0);
			BigDecimal mutCbEq = new BigDecimal(0);
			
			while (rs.next()) {
				bb = new Bukubesar();
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				System.out.println("trn_ref_no :" + rs.getString("trn_ref_no"));
				bb.setTrn_dt(rs.getDate("trn_dt"));
				bb.setTxn_dt_time(rs.getString("txn_dt_time"));
//				bb.setJam(rs.getString("jam"));
				bb.setValue_dt(rs.getDate("value_dt"));
				//penambahan cabang
				bb.setAc_branch(rs.getString("ac_branch"));
				bb.setDrcr(rs.getString("drcr_ind"));
				if(rs.getString("batch_no") != null){
					bb.setBatch_no(rs.getString("batch_no"));
				}else{
					bb.setBatch_no("");
				}
				bb.setAc_no(rs.getString("ac_no"));
				bb.setMaker_id(rs.getString("user_id"));
				bb.setAuth_id(rs.getString("auth_id"));
				bb.setEvent(rs.getString("event"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setKet(rs.getString("CF_ADDLTEXT"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setAc_ccy(rs.getString("ac_ccy"));
				bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
				bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
				System.out.println("lcy amount :" + rs.getString("lcy_amount"));
				if ("D".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
						mutDb = bb.getLcy_amount();
					} else {
						mutDb = bb.getFcy_amount();
						mutDbEq = bb.getLcy_amount();
					}
				} else {
					mutDb = new BigDecimal(0);
					mutDbEq =new BigDecimal(0);
				}
				totMutDb = totMutDb.subtract(mutDb);
				bb.setAmt_db_eq(mutDbEq);
				bb.setAmt_db(mutDb);
				if ("C".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))) {
						mutCb = bb.getLcy_amount();
					} else {
						mutCb = bb.getFcy_amount();
						mutCbEq = bb.getLcy_amount();
					}
				} else {
					mutCb = new BigDecimal(0);
					mutCbEq = new BigDecimal(0);
				}
				
				bb.setAmt_cr_eq(mutCbEq);
				bb.setAmt_cr(mutCb);
				totMutCb = totMutCb.add(mutCb);
				if ("C".equals(bb.getDrcr().trim())) {		
					saldo = saldo.add(bb.getAmt_cr());
				} else if ("D".equals(bb.getDrcr().trim())) {
					saldo = saldo.subtract(bb.getAmt_db());
				}
				bb.setSaldo(saldo);
				System.out.println("saldo getcontohbuku2 (button OK): "+saldo);
				bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
				lbb.add(bb);				
			}
		} catch (SQLException ex) {
			log.error(" getcontohbuku2 : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
//=============================================================================================================================================================================================

	    //penambahan BukuBesarGL, Agustus 2016	
	    //public List getcontohbuku2(String tgl1, String tgl2, String ac_no, String brnch, String ccy, int begin, int delta, String cbsrekoto, BigDecimal saldo, String allcab, String allval) {
		public List getcontohbukuGL2(String tgl1, String tgl2, String ac_no, String ac_no2, String ac_no3, String ac_no4, String ac_no5,
				String ac_no6, String ac_no7, String ac_no8, String ac_no9, String ac_no10, int begin, int delta, BigDecimal saldo) {
		List lbb = new ArrayList();
		Bukubesar bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		System.out.println("TEST getcontohbukuGL2 before");
		try {
			System.out.println("TEST getcontohbukuGL2 before connect");
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			System.out.println("TEST getcontohbukuGL2 connect");
			//penambahan query untuk cabang
			sql = "SELECT inc, trn_dt, ac_branch, batch_no, ac_no, event, trn_ref_no, trn_code, VALUE_DT, AUTH_ID, user_id, " +
			      "CF_ADDLTEXT, drcr_ind, ac_ccy, fcy_amount, lcy_amount, rcc_no " +
			      "FROM (select rownum as inc, a.trn_dt, a.batch_no, a.AC_BRANCH, a.ac_no, a.event, a.trn_ref_no, a.trn_code, a.VALUE_DT,a.AUTH_ID, a.user_id, " +
			      "s.trn_desc || decode(a.related_account, '','' , ' For the Account No.'||a.related_account) ||':'|| " +
			      "(SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||', '||( " +
				  "select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no " +
		    	  ") || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' '|| (select s.user_ref_no || ' ' || c.cust_name from cstb_contract s, LCTB_PARTIES c " +
				  "where s.contract_ref_no = c.contract_ref_no and c.party_type = 'BEN' and s.contract_ref_no(+) = a.trn_ref_no " +
				  "and c.version_no = (select max(version_no) from LCTB_PARTIES where party_type = 'BEN' and contract_ref_no = a.trn_ref_no) " +
				  "group by s.user_ref_no , c.cust_name) || ', ' || " +
			      "(case when c.rel_reference_no is NULL THEN '' else 'Deal Reference: ' || " +
			      "(case when c.event_reference_type = 'P' THEN (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.rel_reference_no) " + 
                  "ELSE (select deal_reference_no from setb_deal_detail  where leg_reference_no = c.event_reference) end)  end) CF_ADDLTEXT, " +
				  "a.drcr_ind, a.ac_ccy, a.fcy_amount, a.lcy_amount, " +
				  //"(select y.txn_mis_1  from actb_history x, mitb_class_mapping y where x.trn_ref_no = y.unit_ref_no and x.ac_branch = y.branch_code " +
				  //"and x.ac_no = a.ac_no and x.trn_ref_no = a.trn_ref_no and x.drcr_ind = a.drcr_ind and x.ac_ccy = a.ac_ccy and x.ac_entry_sr_no = a.ac_entry_sr_no) as rcc_no " +
                  "(SELECT distinct y.txn_mis_1 FROM MITB_CLASS_MAPPING y WHERE y.UNIT_REF_NO = a.trn_ref_no AND a.ac_branch = y.branch_code) AS rcc_no " +
			      "from ACVWS_ALL_AC_ENTRIES_HIST a, sttms_trn_code s, setb_event_log c  " +
			      "where a.trn_code = s.trn_code " +
			      "and a.trn_code = s.trn_code " +
			      "and c.event_reference(+) = a.trn_ref_no " +
			      "and c.esn(+) = a.event_sr_no " +
				  "and a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and " +
//				  "a.ac_no = ? and a.ac_branch = ? ";
				  "a.ac_no in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			
//		    if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//		    	sql += " and a.ac_ccy = ? ";

			sql += ") where inc >= ? and inc <= ? ";
			int i = 1;
			
			System.out.println("TEST getcontohbukuGL2 before stat");
			stat = conn.prepareStatement(sql);
			System.out.println("TEST getcontohbukuGL2 stat");
			
			stat.setString(i++, tgl1.trim());
			stat.setString(i++, tgl2.trim());
			//penambahan BukuBesarGL, Agustus 2016
			stat.setString(i++, ac_no.trim()); stat.setString(i++, ac_no2.trim()); stat.setString(i++, ac_no3.trim()); stat.setString(i++, ac_no4.trim());
			stat.setString(i++, ac_no5.trim()); stat.setString(i++, ac_no6.trim()); stat.setString(i++, ac_no7.trim()); stat.setString(i++, ac_no8.trim());
			stat.setString(i++, ac_no9.trim()); stat.setString(i++, ac_no10.trim());
//			stat.setString(i++, brnch.trim());
//			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//				stat.setString(i++, ccy.trim());

			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			rs = stat.executeQuery();
			totMutCb = new BigDecimal(0);
			totMutDb = new BigDecimal(0);
			BigDecimal mutDb = new BigDecimal(0);
			BigDecimal mutCb = new BigDecimal(0);
			BigDecimal mutDbEq = new BigDecimal(0);
			BigDecimal mutCbEq = new BigDecimal(0);
			
			System.out.println("SQL getcontohbukuGL2 => " +sql);
			while (rs.next()) {
				bb = new Bukubesar();
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setTrn_dt(rs.getDate("trn_dt"));
				bb.setValue_dt(rs.getDate("value_dt"));
//				penambahan cabang
				bb.setAc_branch(rs.getString("ac_branch"));
//
				bb.setDrcr(rs.getString("drcr_ind"));
				if(rs.getString("batch_no") != null){
					bb.setBatch_no(rs.getString("batch_no"));
				}else{
					bb.setBatch_no("");
				}
				bb.setAc_no(rs.getString("ac_no"));
				bb.setMaker_id(rs.getString("user_id"));
				bb.setAuth_id(rs.getString("auth_id"));
				bb.setEvent(rs.getString("event"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setKet(rs.getString("CF_ADDLTEXT"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setAc_ccy(rs.getString("ac_ccy"));
				bb.setFcy_amount(rs.getBigDecimal("fcy_amount"));
				bb.setLcy_amount(rs.getBigDecimal("lcy_amount"));
				if ("D".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))
							//penambahan BukuBesarGL, Agustus 2016
							|| ("485052001".equals(ac_no2) || "585054001".equals(ac_no2)) || ("485052001".equals(ac_no3) || "585054001".equals(ac_no3))
							|| ("485052001".equals(ac_no4) || "585054001".equals(ac_no4)) || ("485052001".equals(ac_no5) || "585054001".equals(ac_no5))
							|| ("485052001".equals(ac_no6) || "585054001".equals(ac_no6)) || ("485052001".equals(ac_no7) || "585054001".equals(ac_no7))
							|| ("485052001".equals(ac_no8) || "585054001".equals(ac_no8)) || ("485052001".equals(ac_no9) || "585054001".equals(ac_no9))
							|| ("485052001".equals(ac_no10) || "585054001".equals(ac_no10)) ) {

						mutDb = bb.getLcy_amount();
					} else {
						mutDb = bb.getFcy_amount();
						mutDbEq = bb.getLcy_amount();
					}
				} else {
					mutDb = new BigDecimal(0);
					mutDbEq =new BigDecimal(0);
				}
				totMutDb = totMutDb.subtract(mutDb);
				bb.setAmt_db_eq(mutDbEq);
				bb.setAmt_db(mutDb);
				if ("C".equals(bb.getDrcr().trim())) {
					if ("IDR".equals(bb.getAc_ccy()) || ("485052001".equals(ac_no) || "585054001".equals(ac_no))
							//penambahan BukuBesarGL, Agustus 2016
							|| ("485052001".equals(ac_no2) || "585054001".equals(ac_no2)) || ("485052001".equals(ac_no3) || "585054001".equals(ac_no3))
							|| ("485052001".equals(ac_no4) || "585054001".equals(ac_no4)) || ("485052001".equals(ac_no5) || "585054001".equals(ac_no5))
							|| ("485052001".equals(ac_no6) || "585054001".equals(ac_no6)) || ("485052001".equals(ac_no7) || "585054001".equals(ac_no7))
							|| ("485052001".equals(ac_no8) || "585054001".equals(ac_no8)) || ("485052001".equals(ac_no9) || "585054001".equals(ac_no9))
							|| ("485052001".equals(ac_no10) || "585054001".equals(ac_no10)) ) {
//
						mutCb = bb.getLcy_amount();
					} else {
						mutCb = bb.getFcy_amount();
						mutCbEq = bb.getLcy_amount();
					}
				} else {
					mutCb = new BigDecimal(0);
					mutCbEq = new BigDecimal(0);
				}
			
				bb.setAmt_cr_eq(mutCbEq);
				bb.setAmt_cr(mutCb);
				totMutCb = totMutCb.add(mutCb);
				if ("C".equals(bb.getDrcr().trim())) {		
					saldo = saldo.add(bb.getAmt_cr());
				} else if ("D".equals(bb.getDrcr().trim())) {
					saldo = saldo.subtract(bb.getAmt_db());
				}
				bb.setSaldo(saldo);
				System.out.println("saldo getcontohbuku2 (button OK): "+saldo);
				bb.setRcc_no(rs.getString("rcc_no") == null ? "" : rs.getString("rcc_no"));
				lbb.add(bb);				
			}
		} catch (SQLException ex) {
			log.error("Eksepsi getcontohbuku2GL : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
//	public Bukubesar getnoOfRecords(String tgl1, String tgl2, String ac_no,
//			String brnch, String ccy, String cbsrekoto) {
//		Connection conn = null;
//		PreparedStatement stat = null;
//		ResultSet rs = null;
//		String sql = "";
//		Bukubesar bb = null;
//		try {
//			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			// sql = "select count(*) as totrownum " +
//			// "from ACVWS_ALL_AC_ENTRIES_HIST a " +
//			// "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
//			// +
//			// "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? and a.ac_no not in (?)";
//			sql = "select count(*) as totrow, sum(debet) as d, sum(kredit) as k from ( "
//					+ "select decode(drcr_ind, 'D', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) debet,"
//					+ " decode(drcr_ind, 'C', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) kredit "
//					+ "from ACVWS_ALL_AC_ENTRIES_HIST "
//					+ "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
//					+ "ac_no = ? and ac_branch = ? ";
//			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//		    	sql += " and ac_ccy = ? ";		    	
//		    }
//			if(!"1".equals(cbsrekoto)){
//				sql += " and ac_no not in (?) ";
//			}
//			sql += " ) ";
//			int i = 1;
//			stat = conn.prepareStatement(sql);
//			stat.setString(i++, tgl1.trim());
//			stat.setString(i++, tgl2.trim());
//			stat.setString(i++, ac_no.trim());
//			stat.setString(i++, brnch.trim());
//			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
//		    } else {	    	
//				stat.setString(i++, ccy.trim());	    	
//		    }
//			if(!"1".equals(cbsrekoto)){
//				stat.setString(i++, "195001006");
//			}
//			rs = stat.executeQuery();
//			if (rs.next()) {
//				bb = new Bukubesar();
//				bb.setTotrow(rs.getInt("totrow"));
//				BigDecimal x = (rs.getBigDecimal("d") == null) ? new BigDecimal(0) : rs.getBigDecimal("d");
//				bb.setTotdebet(x.multiply(new BigDecimal(-1)));
//				bb.setTotkredit(rs.getBigDecimal("k"));
//			}
//		} catch (SQLException ex) {
//			log.error(" getnoOfRecords : " + ex.getMessage());
//		} finally {
//			closeConnDb(conn, stat, rs);
//		}
//		return bb;
//	}
		
	//penambahan allcab-allval, AGUSTUS 2016
	//public Bukubesar getnoOfRecords(String tgl1, String tgl2, String ac_no,
	//String brnch, String ccy, String cbsrekoto) {
	public Bukubesar getnoOfRecords(String tgl1, String tgl2, String ac_no,
		String brnch, String ccy, String cbsrekoto, String allcab, String allval) {
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		Bukubesar bb = null;
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			// sql = "select count(*) as totrownum " +
			// "from ACVWS_ALL_AC_ENTRIES_HIST a " +
			// "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
			// +
			// "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? and a.ac_no not in (?)";
			sql = "select count(*) as totrow, sum(debet) as d, sum(kredit) as k from ( "
				+ "select decode(drcr_ind, 'D', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) debet,"
								+ " decode(drcr_ind, 'C', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) kredit "
				+ "from ACVWS_ALL_AC_ENTRIES_HIST "
				+ "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
//						+ "ac_no = ? and ac_branch = ? ";
				+ "ac_no = ? ";
			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
			} else {	    	
				//penambahan allcab-allval, AGUSTUS 2016
//						sql += " and ac_ccy = ? ";
				if ("2".equals(allval)) //cek semua valuta
				{
					//tidak ada query
					}
					else //tidak cek semua valuta
					{
						sql += " and ac_ccy = ? ";
					}
			}
			if("1".equals(allcab)) //cek semua cabang
			{
				//tidak ada query
			}
			else //tidak cek semua cabang
			{
				sql += " AND ac_branch = ? ";
			}
//								
			if(!"1".equals(cbsrekoto)){
				sql += " and ac_no not in (?) ";
			}
			sql += " ) ";
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, tgl1.trim());
			stat.setString(i++, tgl2.trim());
			stat.setString(i++, ac_no.trim());
//			stat.setString(i++, brnch.trim());
			if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
			} else {
			//penambahan allcab-allval, AGUSTUS 2016					
//			stat.setString(i++, ccy.trim());
				if ("2".equals(allval)) //cek semua valuta
				{
					//tidak ada query
				}
				else //tidak cek semua valuta
				{
					stat.setString(i++, ccy.trim());
				}
			}
			if("1".equals(allcab)) //cek semua cabang
			{
				//tidak ada query
			}
			else //tidak cek semua cabang
			{
				stat.setString(i++, brnch.trim());
			}
//
			if(!"1".equals(cbsrekoto)){
				stat.setString(i++, "195001006");
			}
			System.out.println("count buku besar :" + sql);
			rs = stat.executeQuery();
			if (rs.next()) {
			bb = new Bukubesar();
			bb.setTotrow(rs.getInt("totrow"));
			BigDecimal x = (rs.getBigDecimal("d") == null) ? new BigDecimal(0) : rs.getBigDecimal("d");
			bb.setTotdebet(x.multiply(new BigDecimal(-1)));
			bb.setTotkredit(rs.getBigDecimal("k"));
		}
		} catch (SQLException ex) {
			log.error(" getnoOfRecords : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
			
		//====================================================================================================================================================================================

	//penambahan BukuBesarGL, Agustus 2016	
	//public Bukubesar getnoOfRecords(String tgl1, String tgl2, String ac_no, String brnch, String ccy, String cbsrekoto, String allcab, String allval) {
	public Bukubesar getnoOfRecordsGL(String tgl1, String tgl2, String ac_no, String ac_no2, String ac_no3, String ac_no4, String ac_no5,
			String ac_no6, String ac_no7, String ac_no8, String ac_no9, String ac_no10) {
			Connection conn = null;
			PreparedStatement stat = null;
			ResultSet rs = null;
			String sql = "";
			Bukubesar bb = null;
			
			System.out.println("TEST getnoOfRecordsGL before");
			try {
				conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
				// sql = "select count(*) as totrownum " +
				// "from ACVWS_ALL_AC_ENTRIES_HIST a " +
				// "where a.trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and a.trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
				// +
				// "a.ac_no = ? and a.ac_branch = ? and a.ac_ccy = ? and a.ac_no not in (?)";
				sql = "select count(*) as totrow, sum(debet) as d, sum(kredit) as k from ( "
						+ "select decode(drcr_ind, 'D', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) debet,"
						+ " decode(drcr_ind, 'C', decode(ac_ccy, 'IDR', lcy_amount, fcy_amount), 0) kredit "
						+ "from ACVWS_ALL_AC_ENTRIES_HIST "
						+ "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and trn_dt <= TO_DATE(?, 'DD-MM-YYYY') and "
//								+ "ac_no = ? and ac_branch = ? ";
						+ "ac_no in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
/*				if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
			    } else {	    	
			    	sql += " and ac_ccy = ? ";
			    }
*/
				sql += " ) ";
				int i = 1;
				
				System.out.println("TEST getnoOfRecordsGL before stat");
				stat = conn.prepareStatement(sql);
				System.out.println("TEST getnoOfRecordsGL stat");
				
				stat.setString(i++, tgl1.trim());
				stat.setString(i++, tgl2.trim());
				//penambahan BukuBesarGL, Agustus 2016
				stat.setString(i++, ac_no.trim()); stat.setString(i++, ac_no2.trim()); stat.setString(i++, ac_no3.trim()); stat.setString(i++, ac_no4.trim());
				stat.setString(i++, ac_no5.trim()); stat.setString(i++, ac_no6.trim()); stat.setString(i++, ac_no7.trim()); stat.setString(i++, ac_no8.trim());
				stat.setString(i++, ac_no9.trim()); stat.setString(i++, ac_no10.trim());
/*				stat.setString(i++, brnch.trim());
				if("485052001".equals(ac_no) || "585054001".equals(ac_no)){	
			    } else {	    	
					stat.setString(i++, ccy.trim());
			    }
*/
				System.out.println("SQL getnoOfRecordsGL: "+sql);
				rs = stat.executeQuery();
				if (rs.next()) {
					bb = new Bukubesar();
					bb.setTotrow(rs.getInt("totrow"));
					BigDecimal x = (rs.getBigDecimal("d") == null) ? new BigDecimal(0) : rs.getBigDecimal("d");
					bb.setTotdebet(x.multiply(new BigDecimal(-1)));
					bb.setTotkredit(rs.getBigDecimal("k"));
				}
			} catch (SQLException ex) {
				log.error("Eksepsi getnoOfRecordsGL : " + ex.getMessage());
			} finally {
				closeConnDb(conn, stat, rs);
			}
			return bb;
		}

	//penambahan BukuBesarGL, Agustus 2016
	//public Gl getGl(String acc) {
	public Gl getGlALL(String acc, String acc2, String acc3, String acc4, String acc5, String acc6, String acc7, String acc8, String acc9, String acc10) {
		Gl gl = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = "select gl_code, gl_desc from gltm_glmaster where gl_code = ? ";
			sql = "select gl_code, gl_desc from gltm_glmaster where gl_code in (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			int i = 1;
			stat = conn.prepareStatement(sql);
			//penambahan BukuBesarGL, Agustus 2016
			stat.setString(i++, acc); stat.setString(i++, acc2); stat.setString(i++, acc3); stat.setString(i++, acc4); stat.setString(i++, acc5);
			stat.setString(i++, acc6); stat.setString(i++, acc7); stat.setString(i++, acc8); stat.setString(i++, acc9); stat.setString(i++, acc10);
			rs = stat.executeQuery();
			if (rs.next()) {
				gl = new Gl();
				gl.setGl_code(rs.getString("gl_code"));
				gl.setGl_desc(rs.getString("gl_desc"));
			}
		} catch (SQLException ex) {
			log.error("error getGlALL : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return gl;
	}
	
	public int getnoOfRecords2(String userId, String branch, String tgl1, 
			String tgl2, String mod) {
			Connection conn = null;
			PreparedStatement stat = null;
			ResultSet rs = null;
			String sql = "";
			int totrow = 0;
			try {
				conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
				sql = "select count(*) as total from acvw_all_ac_entries "
						+ "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and "
						+ "trn_dt <= TO_DATE(?, 'DD-MM-YYYY')  AND auth_stat=? ";
				if (!"".equals(userId) && userId != null) {
					sql += " and user_id = ? ";
				}
				if (!"".equals(branch) && branch != null) {
					sql += " and ac_branch = ? ";
				}
				if (!"".equals(mod) && mod != null) {
					sql += " and module = ? ";
				}
		
				int i = 1;
				stat = conn.prepareStatement(sql);
				stat.setString(i++, tgl1);
				stat.setString(i++, tgl2);
				stat.setString(i++, "A");
				if (!"".equals(userId) && userId != null) {
					stat.setString(i++, userId);
				}
				if (!"".equals(branch) && branch != null) {
					stat.setString(i++, branch);
				}
				if (!"".equals(mod) && mod != null) {
					stat.setString(i++, mod);
				}

				rs = stat.executeQuery();
				if (rs.next()) {
					totrow = rs.getInt("total");
				}
			} catch (SQLException ex) {
				log.error(" getnoOfRecords2 : " + ex.getMessage());
			} finally {
				closeConnDb(conn, stat, rs);
			}
			return totrow;
		}


	public List getListPCdIB(String type, String dt) {
		List lbb = new ArrayList();
		Pcode bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getIbDS().getConnection();
			sql = "select distinct status, count(*) as total, sum(amount) as nominal from "
					+ "(SELECT AMOUNT, (case when status = '11' then 'Success' else 'Failed' end) STATUS "
					+ "FROM IB_TRANSFERS_DOM where TRANSFER_TYPE= ? and trunc(DATE_TRX) = TO_DATE(?, 'dd-MM-yyyy')) "
					+ "group by status";
			stat = conn.prepareStatement(sql);
			stat.setString(1, type);
			stat.setString(2, dt);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Pcode();
				bb.setPcd("-");
				bb.setSts(rs.getString("status"));
				bb.setTot(rs.getInt("total"));
				bb.setNom(rs.getBigDecimal("nominal"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListPCdIB : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public List getListbatchht(String userId, String branch, String tgl1,
			String tgl2, String mod, int begin, int delta) {
		List lbb = new ArrayList();
		BatchMaster bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select inc, trn_dt,RELATED_ACCOUNT, trn_ref_no,ac_branch,ac_no,lcy_amount,ac_ccy,fcy_amount,"
					+ "user_id,auth_id,trn_code,drcr_ind,module,auth_stat from "
					+ "(select rownum as inc, trn_dt,RELATED_ACCOUNT, trn_ref_no,ac_branch,ac_no,lcy_amount,ac_ccy,fcy_amount,"
					+ "user_id,auth_id,trn_code,drcr_ind,module,auth_stat  from acvw_all_ac_entries "
					+ "where trn_dt >= TO_DATE(?, 'DD-MM-YYYY') and "
					+ "trn_dt <= TO_DATE(?, 'DD-MM-YYYY')  AND auth_stat='A'   ";
					
			if (!"".equals(userId) && userId != null) {
				sql += " and user_id = ? ";
			}
			if (!"".equals(branch) && branch != null) {
				sql += " and ac_branch = ? ";
			}
			if (!"".equals(mod) && mod != null) {
				sql += " and module = ? ";
			}

			sql+=") where inc >= ? and inc <= ? ";
			
			log.info("sql getListbatchht -->" + sql);
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, tgl1);
			stat.setString(i++, tgl2);
//			stat.setString(i++, "A");
//			stat.setInt(i++, delta);

			if (!"".equals(userId) && userId != null) {
				stat.setString(i++, userId);
			}
			if (!"".equals(branch) && branch != null) {
				stat.setString(i++, branch);
			}
			if (!"".equals(mod) && mod != null) {
				stat.setString(i++, mod);
			}
			
			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			log.info("sql getListbatchht -->" + begin +delta);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new BatchMaster();
				bb.setTrn_dt(rs.getDate("trn_dt"));
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setBranch_code(rs.getString("ac_branch"));
				bb.setAc_no(rs.getString("ac_no"));
				bb.setCr_ent_total(rs.getBigDecimal("lcy_amount"));
				bb.setAc_ccy(rs.getString("ac_ccy"));
				bb.setModule(rs.getString("module"));
				bb.setUser_id(rs.getString("user_id"));
				bb.setAuth_id(rs.getString("auth_id"));
				bb.setAuth_stat(rs.getString("auth_stat"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setDrcr_ind(rs.getString("drcr_ind"));
				bb.setRELATED_ACCOUNT(rs.getString("RELATED_ACCOUNT"));
				lbb.add(bb);

			}

		} catch (SQLException ex) {
			log.error(" getListbatch  : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	public List getListPCdCMS(String type, String dt) {
		List lbb = new ArrayList();
		Pcode bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getCmsDS().getConnection();
			sql = "select distinct status, count(*) as total, sum(amount) as nominal from "
					+ "(SELECT AMOUNT, decode(status, '3', 'Success', 'Fail') status "
					+ "FROM CB_TRANSFERS_DOM where transfer_type= ? and  trunc(DATE_TRX) = TO_DATE(?, 'dd-MM-yyyy')) "
					+ "group by status";
			stat = conn.prepareStatement(sql);
			stat.setString(1, type);
			stat.setString(2, dt);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Pcode();
				bb.setPcd("-");
				bb.setSts(rs.getString("status"));
				bb.setTot(rs.getInt("total"));
				bb.setNom(rs.getBigDecimal("nominal"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListPCdCMS : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getListCustom(String norek, String nama, String ktp, String lhr, int begin) {
		List lbb = new ArrayList();
		SttmCustAccount bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;
		
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "select cust_ac_no, ac_desc, cust_no, ccy, account_class, UNIQUE_ID_VALUE, DATE_OF_BIRTH from  " +
					"(SELECT cust_ac_no, ac_desc, cust_no, ccy, account_class, UNIQUE_ID_VALUE, DATE_OF_BIRTH  " +
					"FROM cust_account_master where ";
//			if((!"".equals(norek) && norek != null) && (!"".equals(nama) && nama != null)
//					&& (!"".equals(ktp) && ktp != null) && (!"".equals(lhr) && lhr != null)) {
//				sql += " ac_desc like ? and cust_ac_no = ? and UNIQUE_ID_VALUE = ? and DATE_OF_BIRTH =  '"+lhr+"' and ";
//			}  
			if (!"".equals(nama) && nama != null){
				sql += " ac_desc like ? and ";
			}  if (!"".equals(norek) && norek != null){
				sql += " cust_ac_no = ? and ";
			}  if (!"".equals(ktp) && ktp != null){
				sql += " UNIQUE_ID_VALUE = ? and ";
			}  if (!"".equals(lhr) && lhr != null){
				sql += "DATE_OF_BIRTH =  '"+lhr+"' and ";
			}
			
			total = sql.length();
			sql  =	sql.substring(0, total-4);
			
			sql += " order by cust_ac_no LIMIT 30 OFFSET ?) AS HASIL";
				
			int i = 1;
			stat = conn.prepareStatement(sql);
			  if (!"".equals(nama) && nama != null){
				stat.setString(i++, "%" + nama + "%".trim());
			} if (!"".equals(norek) && norek != null){
				stat.setString(i++, norek.trim());
			} if (!"".equals(ktp) && ktp != null){
				stat.setString(i++, ktp.trim());
			}
			stat.setInt(i++, begin);
			rs = stat.executeQuery();
					
			while (rs.next()) {
				bb = new SttmCustAccount();
				bb.setCust_ac_no(rs.getString("cust_ac_no"));
				bb.setAc_desc(rs.getString("ac_desc"));
				bb.setCust_no(rs.getString("cust_no"));
				bb.setCcy(rs.getString("ccy"));
				bb.setAccount_class(rs.getString("account_class"));
				bb.setUNIQUE_ID_VALUE(rs.getString("UNIQUE_ID_VALUE"));
				bb.setDATE_OF_BIRTH(rs.getString("DATE_OF_BIRTH"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListCustom : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public int getTotCust(String norek, String nama, String ktp, String lhr) {
		int tot = 0;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;
		
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "SELECT count(*) as total FROM cust_account_master where ";

			if (!"".equals(nama) && nama != null){
				sql += " ac_desc like ? and ";
			}  if (!"".equals(norek) && norek != null){
				sql += " cust_ac_no = ? and ";
			} if (!"".equals(ktp) && ktp != null){
				sql += "  UNIQUE_ID_VALUE= ? and ";
			} if (!"".equals(lhr) && lhr != null){
				sql += " DATE_OF_BIRTH = '"+lhr+"' and ";
			}
			
			total = sql.length();
			sql  =	sql.substring(0, total-4);
			
			int i = 1;
			stat = conn.prepareStatement(sql);

			if (!"".equals(nama) && nama != null){
				stat.setString(i++, "%" + nama + "%".trim());
			}  if (!"".equals(norek) && norek != null){
				stat.setString(i++, norek.trim());
			}  if (!"".equals(ktp) && ktp != null){
				stat.setString(i++, ktp.trim());
			}
			
			rs = stat.executeQuery();
			System.out.println("sql getTotCust" + sql);
			if (rs.next()) {
				tot = rs.getInt("total");
				System.out.println("total:" + tot);
			}
		} catch (SQLException ex) {
			log.error(" getTotCust : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public Saldo getSaldoRek(String acc, String dt1) {
		System.out.println("acc:" + acc + "dt:" + dt1);
		Saldo sld = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = "select cus.cust_ac_no as account,nvl(bal.acy_opening_bal,0) as acy_opening_bal,nvl(bal.lcy_opening_bal,0) as lcy_opening_bal,"
					+" nvl(decode(bal.acc_ccy,'IDR',bal.lcy_closing_bal,bal.acy_closing_bal),0) as lcy_closing_bal "
					+" from sttm_cust_account cus left join "
            +" (select * from actb_accbal_history bal where bkg_date = ( "
            +" select max(bkg_date) as bkg_date from actb_accbal_history "
            +" where account =? and bkg_date < to_date (?, 'dd-mm-yyyy')) "
            +" ) bal on    cus.cust_ac_no = bal.account "
            +" where cus.cust_ac_no = ? ";
			
//			sql = " select account,acy_opening_bal,lcy_opening_bal,decode(acc_ccy,'IDR',lcy_closing_bal,acy_closing_bal) as lcy_closing_bal"
//					 +" from actb_accbal_history where account = ? and bkg_date = ("
//					 +" select max(bkg_date) as bkg_date from actb_accbal_history where account =? and bkg_date < to_date (?, 'dd-mm-yyyy'))";
			
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc.trim());
			stat.setString(2, dt1.trim());
			stat.setString(3, acc.trim());
			
			System.out.println("sql saldo awal :" + sql);
			
			rs = stat.executeQuery();
			if (rs.next()) {
				sld = new Saldo();
				sld.setAccount(rs.getString("account"));
//				BigDecimal t = new BigDecimal(0);
//				if (rs.getBigDecimal("lcy_closing_bal") !=null){
//					t = rs.getBigDecimal("lcy_closing_bal");
//				}
//				else {
//					t = rs.getBigDecimal(0);
//				}
//				if(rs.getBigDecimal("saldo_awal") != null){
//					t = rs.getBigDecimal("saldo_awal");
//				}
//				BigDecimal e = new BigDecimal(0);
//				if(rs.getBigDecimal("ekivalen") != null){
//					e = rs.getBigDecimal("ekivalen");
//				}
//				sld.setSaldo_awal(t);
//				sld.setEkivalen(e);
				sld.setLcy_closing_bal(rs.getBigDecimal("lcy_closing_bal"));
				System.out.println("saldo awal:" + rs.getBigDecimal("lcy_closing_bal"));
			}
			if(sld==null){
				sld = new Saldo();
				sld.setAccount(acc);
				sld.setSaldo_awal(new BigDecimal(0));
				sld.setEkivalen(new BigDecimal(0));
			}
		} catch (SQLException ex) {
			log.error(" getSaldoRek : " + ex.getMessage());
		
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return sld;
	}
	
	public BigDecimal getPlafondPRK3String (Customer cust) {
		BigDecimal sld = new BigDecimal(0);
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " select b.cust_ac_no, a.limit_amount total_plafond,  (a.LINE_CODE || a.LINE_SERIAL) as liab, c.linked_ref_no " +
					"from GETM_FACILITY a, STTMS_CUST_ACCOUNT b, STTM_CUST_ACCOUNT_LINKAGES c " +
					"where a.LINE_CODE || a.LINE_SERIAL = c.linked_ref_no and b.cust_ac_no = c.cust_ac_no and " +
					"b.cust_no = c.customer_no and b.cust_no = a.LINE_CODE and " +
					"b.account_class = ? and b.cust_ac_no = ? and b.ccy=? and b.branch_code=? ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, cust.getCust_acc_clas());
			stat.setString(2, cust.getCust_acc());
			stat.setString(3, cust.getCust_ccy());
			stat.setString(4, cust.getBranch_cd());
			rs = stat.executeQuery();
			if (rs.next()) {
				sld = rs.getBigDecimal("total_plafond");
			}
		} catch (SQLException ex) {
			log.error(" getPlafondPRK3String : " + ex.getMessage());
		
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return sld;
	}
	
	public List getListHistTrans1 (String norek, String dt1, String dt2, BigDecimal sldawal) {
		System.out.println("masuk getListHistTrans1");
		List lbb = new ArrayList();
		HistTransRek ht = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			jmlItmD = 0;
//			jmlItmC = 0;
//			totD = new BigDecimal(0);
//			totC = new BigDecimal(0);
//			DateFormat formatter = null;
//	        Date convertedDate = new Date();
//	        Date convertedDate2 = new Date();
//			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            Date today = new Date();
////            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
//            
//			formatter = new SimpleDateFormat("dd-MM-yyyy");
//			Date date1 = dateFormatter.parse("05-07-2015");
//			convertedDate = (Date) formatter.parse(dt1);
//			convertedDate2 = (Date) formatter.parse(dt2);
//			today = dateFormatter.parse(dateFormatter.format(date1));
//			log.info("Date1 :" + date1);
//			log.info("convertdate :" + convertedDate);
//			log.info("ConvertDate2 :" + convertedDate2);
//			log.info("today :" + today);
            	System.out.println("query lama");
            	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " +
				 		"TO_CHAR(a.value_dt, 'DD-MM-YY') as value_dt, " +
			 		    "a.trn_code, a.event," +
						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
						"|| ' ' || " +
						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
						"|| ' ' || " +
						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
						"from cstb_contract s, " +
						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
						"from LCTBS_CONTRACT_MASTER a, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
						"where a.contract_ref_no = b.contract_ref_no " +
						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
						"from BCTB_CONTRACT_MASTER d, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
						"and d.event_seq_no = f.event_seq_no " +
						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
						"and s.contract_ref_no = a.trn_ref_no " +
						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat " +
//						", TO_CHAR (a.txn_dt_time, 'DD-MM-YY') AS value_dt " +
									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//							"FROM ACVW_ALL_AC_ENTRIES a , " +
						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
						"where a.ac_no = ? " +
						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
				
            
			    stat = conn.prepareStatement(sql);		
			    
			    stat.setString(1, norek);
				stat.setString(2, dt1);
				stat.setString(3, dt2);
				
				System.out.println("SQL GetListHistTrans " + sql);
				rs = stat.executeQuery();
				BigDecimal tempSld = sldawal;
				while (rs.next()) {
					ht = new HistTransRek();
					if ("3170003148".equals(norek.trim()) && 
							(
									("7018547131610003".equals(rs.getString("trn_ref_no")) && "2013-06-10".equals(rs.getString("trn_dt").substring(0,10))) || 
									("701IJ70131850001".equals(rs.getString("trn_ref_no")) && "2013-07-04".equals(rs.getString("trn_dt").substring(0,10)))
							)
						){
						continue;
					}
					if("ZZX".equals(rs.getString("trn_code").toUpperCase().trim()) && "CONVERSION - :,".equals(rs.getString("keterangan").toUpperCase().trim())){
						continue;
					}
					ht.setAccount(rs.getString("ac_no"));
					ht.setTrn_ref_no(rs.getString("trn_ref_no"));
					ht.setInstrument_code(rs.getString("instrument_code") == null ? "" : rs.getString("instrument_code"));
					ht.setTrn_dt(rs.getString("trn_dt"));
					ht.setVal_dt(rs.getString("value_dt"));
					ht.setTrn_code(rs.getString("trn_code"));
					ht.setDesc(rs.getString("keterangan"));
					ht.setDrcr(rs.getString("drcr_ind"));
//					ht.setTxn_dt_time(rs.getString("txn_dt_time"));
					ht.setNominal(rs.getBigDecimal("jumlah") == null ? new BigDecimal(0) : rs.getBigDecimal("jumlah"));
					BigDecimal t = new BigDecimal(0);
					if(rs.getBigDecimal("jumlah") != null){
						t = rs.getBigDecimal("jumlah");
					}
					if("D".equals(rs.getString("drcr_ind"))){
						tempSld = tempSld.subtract(t);
						jmlItmD++;
						totD = totD.add(ht.getNominal());
					}else if ("C".equals(rs.getString("drcr_ind"))){
						tempSld = tempSld.add(t);
						jmlItmC++;
						totC = totC.add(ht.getNominal());
					}				
					ht.setSaldo(tempSld);
					sld_akhir = ht.getSaldo();
					if(rs.getBigDecimal("ekivalen") != null){
						ht.setEkivalen(rs.getBigDecimal("ekivalen"));
					} else {
						ht.setEkivalen(new BigDecimal(0));
					}
					ht.setAuth_stat(rs.getString("auth_stat"));
//					lbb.add(ht);
				}
			} catch (Exception e) {
				log.error("getListHistTrans1 :" + e.getMessage());
//				e.printStackTrace();
			} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getListHistTrans(String norek, String dt1, String dt2, BigDecimal sldawal) {
		List lbb = new ArrayList();
		HistTransRek ht = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			
			jmlItmD = 0;
			jmlItmC = 0;
			totD = new BigDecimal(0);
			totC = new BigDecimal(0);
			DateFormat formatter = null;
	        Date convertedDate = new Date();
	        Date convertedDate2 = new Date();
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            Date today = new Date();
//            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
            
			formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date date1 = dateFormatter.parse("05-07-2015");
			convertedDate = (Date) formatter.parse(dt1);
			convertedDate2 = (Date) formatter.parse(dt2);
			today = dateFormatter.parse(dateFormatter.format(date1));
            	if (convertedDate.compareTo(convertedDate2) == 0){
            		if (convertedDate.compareTo(date1) < 0){
            	System.out.println("query lama masuk if");
            	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " +
				 		"TO_CHAR(a.value_dt, 'DD-MM-YY') as value_dt, " +
			 		    "a.trn_code, a.event," +
						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
						"|| ' ' || " +
						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
						"|| ' ' || " +
						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
						"from cstb_contract s, " +
						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
						"from LCTBS_CONTRACT_MASTER a, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
						"where a.contract_ref_no = b.contract_ref_no " +
						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
						"from BCTB_CONTRACT_MASTER d, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
						"and d.event_seq_no = f.event_seq_no " +
						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
						"and s.contract_ref_no = a.trn_ref_no " +
						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat " +
//						", TO_CHAR (a.txn_dt_time, 'DD-MM-YY') AS value_dt " +
									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//							"FROM ACVW_ALL_AC_ENTRIES a , " +
						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
						"where a.ac_no = ? " +
						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
            	} else if (convertedDate.compareTo(date1) > 0){
            		System.out.println("query baru else if");
//                	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, " +
////                		    "TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " + 
//                		    " to_char(a.trn_dt, 'DD-MM-YYYY') as trn_dt, " +
//    			 		    "a.trn_code, a.event," +
//    						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
//    						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
//    						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
//    						"|| ' ' || " +
//    						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
//    						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
//    						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
//    						"|| ' ' || " +
//    						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
//    						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
//    						"from cstb_contract s, " +
//    						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
//    						"from LCTBS_CONTRACT_MASTER a, " +
//    						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
//    						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
//    						"where a.contract_ref_no = b.contract_ref_no " +
//    						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
//    						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
//    						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
//    						"from BCTB_CONTRACT_MASTER d, " +
//    						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
//    						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
//    						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
//    						"and d.event_seq_no = f.event_seq_no " +
//    						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
//    						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
//    						"and s.contract_ref_no = a.trn_ref_no " +
//    						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
//    						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat," +
//    						"TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt " +
////    									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//    						"FROM ACVW_ALL_AC_ENTRIES a , " +
//    						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
//    						"where a.ac_no = ? " +
//    						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
//    						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
//    						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
////    						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
//    						"order by A.TXN_DT_TIME asc ";
            		sql = "SELECT * FROM (SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,a.trn_code, " +
                  		  " a.event,((SELECT TRIM (ADDL_TEXT)FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) " +
                  		  " ||' ' ||(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL " +
                  		  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN " +
                  		  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) " +
      					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) " +
      					  " || DECODE (a.module, 'CI', ' a.n ' || y.customer_name1, '')END) || ' ' || (  SELECT s.user_ref_no || ' ' || DECODE (s.module_code,'IB','Mtr_dt: ' " +
      					  " || TO_CHAR (u.maturity_date, 'YYYY-MM-DD') || ' ' || u.ket2,'Exp_dt: ' || TO_CHAR (t.expiry_date, 'YYYY-MM-DD') || ' ' || t.ket1) FROM cstb_contract s, "+
      					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '|| b.cust_name || ' & ' || 'BEN: ' || c.cust_name) ket1 " +
      					  "	FROM LCTBS_CONTRACT_MASTER a,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
      					  " (SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no " +
      					  " AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no AND a.event_seq_no = c.event_seq_no AND a.version_no = " +
      					  " (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t,(SELECT d.bcrefno,d.maturity_date,('DRAWEE: ' " +
      					  " || e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES " +
      					  " WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno " +
      					  " AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) " +
      					  " FROM BCTB_CONTRACT_MASTER  WHERE bcrefno = d.bcrefno)) u WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no " +
      					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date, t.ket1,u.ket2)) keterangan,a.drcr_ind, " +
      					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
      					  " FROM ACVW_ALL_AC_ENTRIES a,sttms_trn_code s,cltb_account_apps_master x,sttm_customer y "+
      					  " WHERE a.ac_no = ?  AND a.trn_dt >= TO_DATE (?,'DD-MM-YYYY') and a.trn_dt <= TO_DATE (?,'DD-MM-YYYY') "+
      					  " and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
      					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
//      					HANYA SEMENTARA KARENA DIPAKAI TIRZA  " AND trunc(a.txn_dt_time) - trn_dt  <= 5 " +
      					  " UNION ALL SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,"+
      					  " a.trn_code,a.event,((SELECT TRIM (ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) "+
      					  " || ' ' || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL "+
      					  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN "+
      					  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) "+
      					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) "+
      					  " ||DECODE(a.module,'CI',' a.n '||y.customer_name1, '') END)||' '||(SELECT s.user_ref_no ||' '||DECODE(s.module_code,'IB','Mtr_dt: ' "+
      					  " ||TO_CHAR(u.maturity_date, 'YYYY-MM-DD') || ' ' ||u.ket2,'Exp_dt: '||TO_CHAR (t.expiry_date, 'YYYY-MM-DD')|| ' ' || t.ket1) FROM cstb_contract s, "+
      					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '||b.cust_name|| ' & '|| 'BEN: '|| c.cust_name) ket1 FROM LCTBS_CONTRACT_MASTER a, "+
      					  " (SELECT contract_ref_no,event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP')b,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES "+
      					  " WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no "+
      					  " AND a.event_seq_no = c.event_seq_no AND a.version_no = (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t, "+
      					  " (SELECT d.bcrefno,d.maturity_date,('DRAWEE: '|| e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d, "+
      					  " (SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name "+
      					  " FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no "+
      					  " AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) FROM BCTB_CONTRACT_MASTER WHERE bcrefno = d.bcrefno)) u "+
      					  " WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no "+
      					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date,t.ket1,u.ket2))keterangan,a.drcr_ind, "+
      					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
      					  " FROM ACVW_ALL_AC_ENTRIES a, sttms_trn_code s, cltb_account_apps_master x, sttm_customer y "+
      					  " WHERE a.ac_no = ? and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
      					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
      					  " AND a.txn_dt_time >= TO_DATE (?,'DD-MM-YYYY') and a.txn_dt_time <= TO_DATE (?,'DD-MM-YYYY') "+
      					  " AND trunc(txn_dt_time)  -  a.trn_dt  > 5 ) order by TO_DATE(value_dt , 'DD-MM-YY HH24:MI:SS')";
            	}
            } else if (convertedDate.compareTo(date1) > 0) {
            	System.out.println("query baru");
//            	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, " +
////            		    "TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " + 
//            		    " to_char(a.trn_dt, 'DD-MM-YYYY') as trn_dt, " +
//			 		    "a.trn_code, a.event," +
//						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
//						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
//						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
//						"|| ' ' || " +
//						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
//						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
//						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
//						"|| ' ' || " +
//						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
//						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
//						"from cstb_contract s, " +
//						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
//						"from LCTBS_CONTRACT_MASTER a, " +
//						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
//						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
//						"where a.contract_ref_no = b.contract_ref_no " +
//						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
//						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
//						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
//						"from BCTB_CONTRACT_MASTER d, " +
//						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
//						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
//						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
//						"and d.event_seq_no = f.event_seq_no " +
//						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
//						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
//						"and s.contract_ref_no = a.trn_ref_no " +
//						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
//						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat," +
//						"TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt " +
////									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//						"FROM ACVW_ALL_AC_ENTRIES a , " +
//						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
//						"where a.ac_no = ? " +
//						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
//						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
//						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
////						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
//						"order by A.TXN_DT_TIME asc ";
            	sql = "SELECT * FROM (SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,a.trn_code, " +
              		  " a.event,((SELECT TRIM (ADDL_TEXT)FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) " +
              		  " ||' ' ||(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL " +
              		  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN " +
              		  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) " +
  					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) " +
  					  " || DECODE (a.module, 'CI', ' a.n ' || y.customer_name1, '')END) || ' ' || (  SELECT s.user_ref_no || ' ' || DECODE (s.module_code,'IB','Mtr_dt: ' " +
  					  " || TO_CHAR (u.maturity_date, 'YYYY-MM-DD') || ' ' || u.ket2,'Exp_dt: ' || TO_CHAR (t.expiry_date, 'YYYY-MM-DD') || ' ' || t.ket1) FROM cstb_contract s, "+
  					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '|| b.cust_name || ' & ' || 'BEN: ' || c.cust_name) ket1 " +
  					  "	FROM LCTBS_CONTRACT_MASTER a,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
  					  " (SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no " +
  					  " AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no AND a.event_seq_no = c.event_seq_no AND a.version_no = " +
  					  " (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t,(SELECT d.bcrefno,d.maturity_date,('DRAWEE: ' " +
  					  " || e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES " +
  					  " WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno " +
  					  " AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) " +
  					  " FROM BCTB_CONTRACT_MASTER  WHERE bcrefno = d.bcrefno)) u WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no " +
  					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date, t.ket1,u.ket2)) keterangan,a.drcr_ind, " +
  					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
  					  " FROM ACVW_ALL_AC_ENTRIES a,sttms_trn_code s,cltb_account_apps_master x,sttm_customer y "+
  					  " WHERE a.ac_no = ?  AND a.trn_dt >= TO_DATE (?,'DD-MM-YYYY') and a.trn_dt <= TO_DATE (?,'DD-MM-YYYY') "+
  					  " and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
  					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
//  					  " AND trunc(a.txn_dt_time) - trn_dt  <= 5 "+
  					  " UNION ALL SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,"+
  					  " a.trn_code,a.event,((SELECT TRIM (ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) "+
  					  " || ' ' || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL "+
  					  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN "+
  					  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) "+
  					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) "+
  					  " ||DECODE(a.module,'CI',' a.n '||y.customer_name1, '') END)||' '||(SELECT s.user_ref_no ||' '||DECODE(s.module_code,'IB','Mtr_dt: ' "+
  					  " ||TO_CHAR(u.maturity_date, 'YYYY-MM-DD') || ' ' ||u.ket2,'Exp_dt: '||TO_CHAR (t.expiry_date, 'YYYY-MM-DD')|| ' ' || t.ket1) FROM cstb_contract s, "+
  					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '||b.cust_name|| ' & '|| 'BEN: '|| c.cust_name) ket1 FROM LCTBS_CONTRACT_MASTER a, "+
  					  " (SELECT contract_ref_no,event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP')b,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES "+
  					  " WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no "+
  					  " AND a.event_seq_no = c.event_seq_no AND a.version_no = (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t, "+
  					  " (SELECT d.bcrefno,d.maturity_date,('DRAWEE: '|| e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d, "+
  					  " (SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name "+
  					  " FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no "+
  					  " AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) FROM BCTB_CONTRACT_MASTER WHERE bcrefno = d.bcrefno)) u "+
  					  " WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no "+
  					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date,t.ket1,u.ket2))keterangan,a.drcr_ind, "+
  					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
  					  " FROM ACVW_ALL_AC_ENTRIES a, sttms_trn_code s, cltb_account_apps_master x, sttm_customer y "+
  					  " WHERE a.ac_no = ? and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
  					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
  					  " AND a.txn_dt_time >= TO_DATE (?,'DD-MM-YYYY') and a.txn_dt_time <= TO_DATE (?,'DD-MM-YYYY') "+
  					  " AND trunc(txn_dt_time)  -  a.trn_dt  > 5 ) order by TO_DATE(value_dt , 'DD-MM-YY HH24:MI:SS')";
            } else if (convertedDate.compareTo(date1) < 0) {
            	System.out.println("query lama");
            	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " +
				 		"TO_CHAR(a.value_dt, 'DD-MM-YY') as value_dt, " +
			 		    "a.trn_code, a.event," +
						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
						"|| ' ' || " +
						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
						"|| ' ' || " +
						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
						"from cstb_contract s, " +
						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
						"from LCTBS_CONTRACT_MASTER a, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
						"where a.contract_ref_no = b.contract_ref_no " +
						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
						"from BCTB_CONTRACT_MASTER d, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
						"and d.event_seq_no = f.event_seq_no " +
						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
						"and s.contract_ref_no = a.trn_ref_no " +
						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat " +
//						", TO_CHAR (a.txn_dt_time, 'DD-MM-YY') AS value_dt " +
									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//							"FROM ACVW_ALL_AC_ENTRIES a , " +
						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
						"where a.ac_no = ? " +
						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
            }
			    stat = conn.prepareStatement(sql);		
			    
			    if (convertedDate.compareTo(convertedDate2) == 0){
            		if (convertedDate.compareTo(date1) < 0){
            			stat.setString(1, norek);
        				stat.setString(2, dt1);
        				stat.setString(3, dt2);
            		} else if (convertedDate.compareTo(date1) > 0){
            			stat.setString(1, norek);
        				stat.setString(2, dt1);
        				stat.setString(3, dt2);
        				stat.setString(4, norek);
        				stat.setString(5, dt1);
        				stat.setString(6, dt2);
            		}
			    }else if (convertedDate.compareTo(date1) > 0) {
			    	stat.setString(1, norek);
    				stat.setString(2, dt1);
    				stat.setString(3, dt2);
    				stat.setString(4, norek);
    				stat.setString(5, dt1);
    				stat.setString(6, dt2);
			    } else if (convertedDate.compareTo(date1) < 0) {
			    	stat.setString(1, norek);
    				stat.setString(2, dt1);
    				stat.setString(3, dt2);
			    }
			    
			    
//				stat.setString(1, norek);
//				stat.setString(2, dt1);
//				stat.setString(3, dt2);
				
				System.out.println("SQL GetListHistTrans " + sql);
				rs = stat.executeQuery();
				BigDecimal tempSld = sldawal;
				System.out.println("tempSld :" + tempSld + "saldo awal :" + sldawal);
				while (rs.next()) {
					ht = new HistTransRek();
					if ("3170003148".equals(norek.trim()) && 
							(
									("7018547131610003".equals(rs.getString("trn_ref_no")) && "2013-06-10".equals(rs.getString("trn_dt").substring(0,10))) || 
									("701IJ70131850001".equals(rs.getString("trn_ref_no")) && "2013-07-04".equals(rs.getString("trn_dt").substring(0,10)))
							)
						){
						continue;
					}
					if("ZZX".equals(rs.getString("trn_code").toUpperCase().trim()) && "CONVERSION - :,".equals(rs.getString("keterangan").toUpperCase().trim())){
						continue;
					}
					ht.setAccount(rs.getString("ac_no"));
					ht.setTrn_ref_no(rs.getString("trn_ref_no"));
					ht.setInstrument_code(rs.getString("instrument_code") == null ? "" : rs.getString("instrument_code"));
					ht.setTrn_dt(rs.getString("trn_dt"));
					ht.setVal_dt(rs.getString("value_dt"));
					ht.setTrn_code(rs.getString("trn_code"));
					ht.setDesc(rs.getString("keterangan"));
					ht.setDrcr(rs.getString("drcr_ind"));
//					ht.setAc_ccy(rs.getString("ac_ccy"));
//					System.out.println("ac no :" + rs.getString("ac_no"));
//					ht.setTxn_dt_time(rs.getString("txn_dt_time"));
					ht.setNominal(rs.getBigDecimal("jumlah") == null ? new BigDecimal(0) : rs.getBigDecimal("jumlah"));
					BigDecimal t = new BigDecimal(0);
					if(rs.getBigDecimal("jumlah") != null){
						t = rs.getBigDecimal("jumlah");
					}
//					System.out.println("ac no :" + rs.getString("jumlah"));
					if("D".equals(rs.getString("drcr_ind"))){
						tempSld = tempSld.subtract(t);
						jmlItmD++;
						totD = totD.add(ht.getNominal());
					}else if ("C".equals(rs.getString("drcr_ind"))){
						tempSld = tempSld.add(t);
						jmlItmC++;
						totC = totC.add(ht.getNominal());
					}				
					ht.setSaldo(tempSld);
					sld_akhir = ht.getSaldo();
					if(rs.getBigDecimal("ekivalen") != null){
						ht.setEkivalen(rs.getBigDecimal("ekivalen"));
					} else {
						ht.setEkivalen(new BigDecimal(0));
					}
					ht.setAuth_stat(rs.getString("auth_stat"));
					lbb.add(ht);
				}
			} catch (Exception e) {
				log.error("getListHistTrans :" + e.getMessage());
//				e.printStackTrace();
			} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getListHistTransCetakan(String norek, String dt1, String dt2, BigDecimal sldawal) {
		System.out.println("getListHistTransCetakan");
		List lbb = new ArrayList();
		HistTransRek ht = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			jmlItmD = 0;
			jmlItmC = 0;
			totD = new BigDecimal(0);
			totC = new BigDecimal(0);
			DateFormat formatter = null;
	        Date convertedDate = new Date();
	        Date convertedDate2 = new Date();
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            Date today = new Date();
//            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
            
			formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date date1 = dateFormatter.parse("05-07-2015");
			convertedDate = (Date) formatter.parse(dt1);
			convertedDate2 = (Date) formatter.parse(dt2);
			today = dateFormatter.parse(dateFormatter.format(date1));
			log.info("Date1 :" + date1);
			log.info("convertdate :" + convertedDate);
			log.info("ConvertDate2 :" + convertedDate2);
			log.info("today :" + today);
            	if (convertedDate.compareTo(convertedDate2) == 0){
            		if (convertedDate.compareTo(date1) < 0){
            	System.out.println("query lama masuk if");
            	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " +
				 		"TO_CHAR(a.value_dt, 'DD-MM-YY') as value_dt, " +
			 		    "a.trn_code, a.event," +
						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
						"|| ' ' || " +
						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
						"|| ' ' || " +
						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
						"from cstb_contract s, " +
						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
						"from LCTBS_CONTRACT_MASTER a, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
						"where a.contract_ref_no = b.contract_ref_no " +
						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
						"from BCTB_CONTRACT_MASTER d, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
						"and d.event_seq_no = f.event_seq_no " +
						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
						"and s.contract_ref_no = a.trn_ref_no " +
						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat " +
//						", TO_CHAR (a.txn_dt_time, 'DD-MM-YY') AS value_dt " +
									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//							"FROM ACVW_ALL_AC_ENTRIES a , " +
						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
						"where a.ac_no = ? " +
						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
            	} else if (convertedDate.compareTo(date1) > 0){
            		System.out.println("query baru else if");
//                	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, " +
////                		    "TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " + 
//                		    " to_char(a.trn_dt, 'DD-MM-YYYY') as trn_dt, " +
//    			 		    "a.trn_code, a.event," +
//    						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
//    						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
//    						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
//    						"|| ' ' || " +
//    						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
//    						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
//    						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
//    						"|| ' ' || " +
//    						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
//    						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
//    						"from cstb_contract s, " +
//    						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
//    						"from LCTBS_CONTRACT_MASTER a, " +
//    						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
//    						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
//    						"where a.contract_ref_no = b.contract_ref_no " +
//    						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
//    						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
//    						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
//    						"from BCTB_CONTRACT_MASTER d, " +
//    						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
//    						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
//    						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
//    						"and d.event_seq_no = f.event_seq_no " +
//    						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
//    						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
//    						"and s.contract_ref_no = a.trn_ref_no " +
//    						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
//    						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat," +
//    						"TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt " +
////    									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//    						"FROM ACVW_ALL_AC_ENTRIES a , " +
//    						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
//    						"where a.ac_no = ? " +
//    						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
//    						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
//    						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
////    						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
//    						"order by A.TXN_DT_TIME asc ";
            		sql = "SELECT * FROM (SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,a.trn_code, " +
                    		  " a.event,((SELECT TRIM (ADDL_TEXT)FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) " +
                    		  " ||' ' ||(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL " +
                    		  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN " +
                    		  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) " +
        					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) " +
        					  " || DECODE (a.module, 'CI', ' a.n ' || y.customer_name1, '')END) || ' ' || (  SELECT s.user_ref_no || ' ' || DECODE (s.module_code,'IB','Mtr_dt: ' " +
        					  " || TO_CHAR (u.maturity_date, 'YYYY-MM-DD') || ' ' || u.ket2,'Exp_dt: ' || TO_CHAR (t.expiry_date, 'YYYY-MM-DD') || ' ' || t.ket1) FROM cstb_contract s, "+
        					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '|| b.cust_name || ' & ' || 'BEN: ' || c.cust_name) ket1 " +
        					  "	FROM LCTBS_CONTRACT_MASTER a,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
        					  " (SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no " +
        					  " AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no AND a.event_seq_no = c.event_seq_no AND a.version_no = " +
        					  " (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t,(SELECT d.bcrefno,d.maturity_date,('DRAWEE: ' " +
        					  " || e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES " +
        					  " WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno " +
        					  " AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) " +
        					  " FROM BCTB_CONTRACT_MASTER  WHERE bcrefno = d.bcrefno)) u WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no " +
        					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date, t.ket1,u.ket2)) keterangan,a.drcr_ind, " +
        					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
        					  " FROM ACVW_ALL_AC_ENTRIES a,sttms_trn_code s,cltb_account_apps_master x,sttm_customer y "+
        					  " WHERE a.ac_no = ?  AND a.trn_dt >= TO_DATE (?,'DD-MM-YYYY') and a.trn_dt <= TO_DATE (?,'DD-MM-YYYY') "+
        					  " and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
        					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
        					  " AND trunc(a.txn_dt_time) - trn_dt  <= 5 UNION ALL SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,"+
        					  " a.trn_code,a.event,((SELECT TRIM (ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) "+
        					  " || ' ' || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL "+
        					  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN "+
        					  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) "+
        					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) "+
        					  " ||DECODE(a.module,'CI',' a.n '||y.customer_name1, '') END)||' '||(SELECT s.user_ref_no ||' '||DECODE(s.module_code,'IB','Mtr_dt: ' "+
        					  " ||TO_CHAR(u.maturity_date, 'YYYY-MM-DD') || ' ' ||u.ket2,'Exp_dt: '||TO_CHAR (t.expiry_date, 'YYYY-MM-DD')|| ' ' || t.ket1) FROM cstb_contract s, "+
        					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '||b.cust_name|| ' & '|| 'BEN: '|| c.cust_name) ket1 FROM LCTBS_CONTRACT_MASTER a, "+
        					  " (SELECT contract_ref_no,event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP')b,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES "+
        					  " WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no "+
        					  " AND a.event_seq_no = c.event_seq_no AND a.version_no = (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t, "+
        					  " (SELECT d.bcrefno,d.maturity_date,('DRAWEE: '|| e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d, "+
        					  " (SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name "+
        					  " FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no "+
        					  " AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) FROM BCTB_CONTRACT_MASTER WHERE bcrefno = d.bcrefno)) u "+
        					  " WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no "+
        					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date,t.ket1,u.ket2))keterangan,a.drcr_ind, "+
        					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
        					  " FROM ACVW_ALL_AC_ENTRIES a, sttms_trn_code s, cltb_account_apps_master x, sttm_customer y "+
        					  " WHERE a.ac_no = ? and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
        					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
        					  " AND a.txn_dt_time >= TO_DATE (?,'DD-MM-YYYY') and a.txn_dt_time <= TO_DATE (?,'DD-MM-YYYY') "+
        					  " AND trunc(txn_dt_time)  -  a.trn_dt  > 5 ) order by TO_DATE(value_dt , 'DD-MM-YY HH24:MI:SS')";
            	}
            } else if (convertedDate.compareTo(date1) > 0) {
            	System.out.println("query baru");
//            	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, " +
////            		    "TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " + 
//            		    " to_char(a.trn_dt, 'DD-MM-YYYY') as trn_dt, " +
//			 		    "a.trn_code, a.event," +
//						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
//						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
//						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
//						"|| ' ' || " +
//						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
//						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
//						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
//						"|| ' ' || " +
//						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
//						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
//						"from cstb_contract s, " +
//						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
//						"from LCTBS_CONTRACT_MASTER a, " +
//						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
//						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
//						"where a.contract_ref_no = b.contract_ref_no " +
//						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
//						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
//						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
//						"from BCTB_CONTRACT_MASTER d, " +
//						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
//						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
//						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
//						"and d.event_seq_no = f.event_seq_no " +
//						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
//						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
//						"and s.contract_ref_no = a.trn_ref_no " +
//						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
//						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat," +
//						"TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt " +
////									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//						"FROM ACVW_ALL_AC_ENTRIES a , " +
//						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
//						"where a.ac_no = ? " +
//						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
//						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
//						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
////						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
//						"order by A.TXN_DT_TIME asc ";
            	sql = "SELECT * FROM (SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,a.trn_code, " +
                		  " a.event,((SELECT TRIM (ADDL_TEXT)FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) " +
                		  " ||' ' ||(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL " +
                		  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN " +
                		  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) " +
    					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) " +
    					  " || DECODE (a.module, 'CI', ' a.n ' || y.customer_name1, '')END) || ' ' || (  SELECT s.user_ref_no || ' ' || DECODE (s.module_code,'IB','Mtr_dt: ' " +
    					  " || TO_CHAR (u.maturity_date, 'YYYY-MM-DD') || ' ' || u.ket2,'Exp_dt: ' || TO_CHAR (t.expiry_date, 'YYYY-MM-DD') || ' ' || t.ket1) FROM cstb_contract s, "+
    					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '|| b.cust_name || ' & ' || 'BEN: ' || c.cust_name) ket1 " +
    					  "	FROM LCTBS_CONTRACT_MASTER a,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
    					  " (SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no " +
    					  " AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no AND a.event_seq_no = c.event_seq_no AND a.version_no = " +
    					  " (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t,(SELECT d.bcrefno,d.maturity_date,('DRAWEE: ' " +
    					  " || e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES " +
    					  " WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno " +
    					  " AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) " +
    					  " FROM BCTB_CONTRACT_MASTER  WHERE bcrefno = d.bcrefno)) u WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no " +
    					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date, t.ket1,u.ket2)) keterangan,a.drcr_ind,a.ac_ccy, " +
    					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.fcy_amount,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
    					  " FROM ACVW_ALL_AC_ENTRIES a,sttms_trn_code s,cltb_account_apps_master x,sttm_customer y "+
    					  " WHERE a.ac_no = ?  AND a.trn_dt >= TO_DATE (?,'DD-MM-YYYY') and a.trn_dt <= TO_DATE (?,'DD-MM-YYYY') "+
    					  " and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
    					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
    					  " AND trunc(a.txn_dt_time) - trn_dt  <= 5 UNION ALL SELECT a.ac_no,a.trn_ref_no,a.instrument_code,TO_CHAR (a.trn_dt, 'DD-MM-YYYY') AS trn_dt,"+
    					  " a.trn_code,a.event,((SELECT TRIM (ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) "+
    					  " || ' ' || (SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || (SELECT DISTINCT ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL "+
    					  " WHERE reference_no = a.trn_ref_no AND serial_no = a.curr_no) || ' ' || s.trn_desc || ' ' || (CASE WHEN s.trn_desc LIKE 'SI %' THEN "+
    					  " (SELECT internal_remarks FROM SITB_CONTRACT_MASTER WHERE contract_ref_no = a.trn_ref_no AND version_no = (SELECT MAX (version_no) "+
    					  " FROM sitb_contract_master WHERE contract_ref_no = a.TRN_REF_NO)) ELSE DECODE (a.related_account,'', '',' For the Account No.' || a.related_account) "+
    					  " ||DECODE(a.module,'CI',' a.n '||y.customer_name1, '') END)||' '||(SELECT s.user_ref_no ||' '||DECODE(s.module_code,'IB','Mtr_dt: ' "+
    					  " ||TO_CHAR(u.maturity_date, 'YYYY-MM-DD') || ' ' ||u.ket2,'Exp_dt: '||TO_CHAR (t.expiry_date, 'YYYY-MM-DD')|| ' ' || t.ket1) FROM cstb_contract s, "+
    					  " (SELECT a.contract_ref_no,a.expiry_date,('APP: '||b.cust_name|| ' & '|| 'BEN: '|| c.cust_name) ket1 FROM LCTBS_CONTRACT_MASTER a, "+
    					  " (SELECT contract_ref_no,event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP')b,(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES "+
    					  " WHERE party_type = 'BEN') c WHERE a.contract_ref_no = b.contract_ref_no AND a.contract_ref_no = c.contract_ref_no AND a.event_seq_no = b.event_seq_no "+
    					  " AND a.event_seq_no = c.event_seq_no AND a.version_no = (SELECT MAX (version_no) FROM LCTBS_CONTRACT_MASTER WHERE contract_ref_no = a.contract_ref_no)) t, "+
    					  " (SELECT d.bcrefno,d.maturity_date,('DRAWEE: '|| e.party_name || ' & ' || 'DRAWER: ' || f.party_name) ket2 FROM BCTB_CONTRACT_MASTER d, "+
    					  " (SELECT bcrefno, event_seq_no, party_name FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e,(SELECT bcrefno, event_seq_no, party_name "+
    					  " FROM BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f WHERE d.bcrefno = e.bcrefno AND d.bcrefno = f.bcrefno AND d.event_seq_no = e.event_seq_no "+
    					  " AND d.event_seq_no = f.event_seq_no AND d.version_no = (SELECT MAX (version_no) FROM BCTB_CONTRACT_MASTER WHERE bcrefno = d.bcrefno)) u "+
    					  " WHERE s.contract_ref_no = t.contract_ref_no(+) AND s.contract_ref_no = u.bcrefno(+) AND s.contract_ref_no = a.trn_ref_no "+
    					  " GROUP BY s.user_ref_no,s.module_code,t.expiry_date,u.maturity_date,t.ket1,u.ket2))keterangan,a.drcr_ind,a.ac_ccy, "+
    					  " DECODE (a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) AS jumlah,a.lcy_amount AS ekivalen,a.fcy_amount,a.auth_stat,TO_CHAR (a.txn_dt_time, 'DD-MM-YY HH24:MI:SS') AS value_dt "+
    					  " FROM ACVW_ALL_AC_ENTRIES a, sttms_trn_code s, cltb_account_apps_master x, sttm_customer y "+
    					  " WHERE a.ac_no = ? and a.trn_code = s.trn_code and a.related_account = x.account_number(+) "+
    					  " and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) "+
    					  " AND a.txn_dt_time >= TO_DATE (?,'DD-MM-YYYY') and a.txn_dt_time <= TO_DATE (?,'DD-MM-YYYY') "+
    					  " AND trunc(txn_dt_time)  -  a.trn_dt  > 5 ) order by TO_DATE(value_dt , 'DD-MM-YY HH24:MI:SS')";
            } else if (convertedDate.compareTo(date1) < 0) {
            	System.out.println("query lama");
            	sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, " +
				 		"TO_CHAR(a.value_dt, 'DD-MM-YY') as value_dt, " +
			 		    "a.trn_code, a.event," +
						"((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
						"(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
						"(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
						"|| ' ' || " +
						"(case when s.trn_desc like 'SI %' Then (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
						"AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)) else " +
						"decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'') end)" +
						"|| ' ' || " +
						"(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2, " +
						"'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
						"from cstb_contract s, " +
						"(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
						"from LCTBS_CONTRACT_MASTER a, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
						"(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
						"where a.contract_ref_no = b.contract_ref_no " +
						"and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
						"and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
						"(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
						"from BCTB_CONTRACT_MASTER d, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
						"(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
						"where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
						"and d.event_seq_no = f.event_seq_no " +
						"and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
						"where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
						"and s.contract_ref_no = a.trn_ref_no " +
						"group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
						"a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat " +
//						", TO_CHAR (a.txn_dt_time, 'DD-MM-YY') AS value_dt " +
									"FROM ACVWS_ALL_AC_ENTRIES_HIST a , " +
//							"FROM ACVW_ALL_AC_ENTRIES a , " +
						"sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
						"where a.ac_no = ? " +
						"and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
						"and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
						"and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
						"order by a.AC_ENTRY_SR_NO, a.trn_dt ";
            }
				
				stat = conn.prepareStatement(sql);			
//				stat.setString(1, norek);
//				stat.setString(2, dt1);
//				stat.setString(3, dt2);
				
				System.out.println("SQL GetListHistTransCetakan :" + sql);
				
				if (convertedDate.compareTo(convertedDate2) == 0){
            		if (convertedDate.compareTo(date1) < 0){
            			stat.setString(1, norek);
        				stat.setString(2, dt1);
        				stat.setString(3, dt2);
            		} else if (convertedDate.compareTo(date1) > 0){
            			stat.setString(1, norek);
        				stat.setString(2, dt1);
        				stat.setString(3, dt2);
        				stat.setString(4, norek);
        				stat.setString(5, dt1);
        				stat.setString(6, dt2);
            		}
			    }else if (convertedDate.compareTo(date1) > 0) {
			    	stat.setString(1, norek);
    				stat.setString(2, dt1);
    				stat.setString(3, dt2);
    				stat.setString(4, norek);
    				stat.setString(5, dt1);
    				stat.setString(6, dt2);
			    } else if (convertedDate.compareTo(date1) < 0) {
			    	stat.setString(1, norek);
    				stat.setString(2, dt1);
    				stat.setString(3, dt2);
			    }
				
				rs = stat.executeQuery();
				BigDecimal tempSld = sldawal;
				while (rs.next()) {
					ht = new HistTransRek();
					if ("3170003148".equals(norek.trim()) && 
							(
									("7018547131610003".equals(rs.getString("trn_ref_no")) && "2013-06-10".equals(rs.getString("trn_dt").substring(0,10))) || 
									("701IJ70131850001".equals(rs.getString("trn_ref_no")) && "2013-07-04".equals(rs.getString("trn_dt").substring(0,10)))
							)
						){
						continue;
					}
					if("ZZX".equals(rs.getString("trn_code").toUpperCase().trim()) && "CONVERSION - :,".equals(rs.getString("keterangan").toUpperCase().trim())){
						continue;
					}
					ht.setAccount(rs.getString("ac_no"));
					ht.setTrn_ref_no(rs.getString("trn_ref_no"));
					ht.setInstrument_code(rs.getString("instrument_code") == null ? "" : rs.getString("instrument_code"));
					ht.setTrn_dt(rs.getString("trn_dt"));
//					System.out.println("trn dt :" + ht.getTrn_dt());
					ht.setVal_dt(rs.getString("value_dt"));
//					ht.setTxn_dt_time(rs.getString("txn_dt_time"));
					ht.setTrn_code(rs.getString("trn_code"));
					ht.setDesc(rs.getString("keterangan"));
					ht.setDrcr(rs.getString("drcr_ind"));
					ht.setNominal(rs.getBigDecimal("jumlah") == null ? new BigDecimal(0) : rs.getBigDecimal("jumlah"));
					BigDecimal t = new BigDecimal(0);
					if(rs.getBigDecimal("jumlah") != null){
						t = rs.getBigDecimal("jumlah");
					}
					if("D".equals(rs.getString("drcr_ind"))){
						tempSld = tempSld.subtract(t);
						jmlItmD++;
						totD = totD.add(ht.getNominal());
					}else if ("C".equals(rs.getString("drcr_ind"))){
						tempSld = tempSld.add(t);
						jmlItmC++;
						totC = totC.add(ht.getNominal());
					}				
					ht.setSaldo(tempSld);
					sld_akhir = ht.getSaldo();
					if(rs.getBigDecimal("ekivalen") != null){
						ht.setEkivalen(rs.getBigDecimal("ekivalen"));
					} else {
						ht.setEkivalen(new BigDecimal(0));
					}
					ht.setAuth_stat(rs.getString("auth_stat"));
					lbb.add(ht);
				}
			} catch (Exception e) {
				log.error("getListHistTransCetakan :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public SttmCustAccount getDetailCustom(String norek, String username) {
		SttmCustAccount bb = null;
		Connection conn = null;
//		Connection conn1 = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String sql2 = "";
		String sql3 = "";
		String sql4 = "";
		String sqlCustType = "";
		String sqlIndv = "";
		String sqlCorp= "";
		String sqlType = "";
		String noCif = "";	
		String branchCode = "";
		String sqlHold = "";
		String sqlMbanking = "";
		
		try {
			CetakPassbookFunction cpf = new CetakPassbookFunction();
			String rr = "";
			SMTB_USER_ROLE user_r = null;
			if ("".equals(username)){
				user_r = cpf.getUserHCO(username, "HCO");
			}
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " SELECT A.BRANCH_CODE, D.BRANCH_NAME, F.NATIONALITY, A.CUST_AC_NO, A.AC_DESC, A.CUST_NO, A.CCY, A.ACCOUNT_CLASS,"
					+ " ACCOUNT_TYPE, NVL (A.ACY_CURR_BALANCE, 0) AS ACY_CURR_BALANCE,"
					+ " NVL(A.ACY_BLOCKED_AMOUNT, 0) AS ACY_BLOCKED_AMOUNT, NVL(A.ACY_AVL_BAL, 0) AS ACY_AVL_BAL,"
					+ " NVL(B.MIN_BALANCE, 0) AS MIN_BALANCE, NVL(C.ACY_BAL, 0) AS ACY_BAL, F.CUSTOMER_TYPE, A.AC_STAT_DORMANT,"
					+ " A.AC_STAT_NO_DR, A.AC_STAT_NO_CR, A.AC_STAT_FROZEN, E.DESCRIPTION, TO_CHAR (A.AC_OPEN_DATE, 'DD/MM/YYYY') AS AC_OPEN_DATE,"
					+ " A.ALT_AC_NO, A.RECORD_STAT, TO_CHAR (A.CHECKER_DT_STAMP, 'DD/MM/YYYY') AS CHECKER_DT_STAMP,"
					+ " TO_CHAR (F.CIF_CREATION_DATE, 'DD/MM/YYYY') AS CIF_CREATION_DATE, TO_CHAR (A.MAKER_DT_STAMP, 'DD/MM/YYYY') MAKER_DT_STAMP"
					+ " FROM STTM_CUST_ACCOUNT A, STTM_ACCLS_CCY_BALANCES B,"
					+ " GLTB_AVGBAL C, STTM_BRANCH D, STTM_ACCOUNT_CLASS E, STTM_CUSTOMER F"
					+ " WHERE A.CUST_AC_NO = ? AND A.ACCOUNT_CLASS = B.ACCOUNT_CLASS AND F.CUSTOMER_NO = A.CUST_NO AND A.CUST_AC_NO = C.GL_CODE(+)"
					+ " AND A.BRANCH_CODE = D.BRANCH_CODE AND A.ACCOUNT_CLASS = E.ACCOUNT_CLASS";
				
			System.out.println("sql detail :" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, norek);
			rs = stat.executeQuery();
//			System.out.println("rs :" + rs.next());
			if (rs.next()) {
				bb = new SttmCustAccount();
				bb.setBranch_code(rs.getString("BRANCH_CODE"));
				bb.setBranch_name(rs.getString("BRANCH_NAME"));
				bb.setNationality(rs.getString("NATIONALITY"));
				bb.setCUST_AC_NO(rs.getString("CUST_AC_NO"));
				bb.setAcDesc(rs.getString("AC_DESC"));
				bb.setCustNo(rs.getString("CUST_NO"));
				bb.setCcy(rs.getString("CCY"));
				bb.setCustomer_type(rs.getString("CUSTOMER_TYPE"));
				bb.setDesc_acc(rs.getString("DESCRIPTION").toUpperCase().trim());
				//bb.setAC_OPEN_DATE(rs.getString("AC_OPEN_DATE").substring(0, 10));
				bb.setAC_OPEN_DATE(rs.getString("AC_OPEN_DATE"));
				bb.setCIF_CREATION_DATE(rs.getString("CIF_CREATION_DATE"));
				bb.setRecord_stat(rs.getString("RECORD_STAT"));
				System.out.println("Acc type :" + rs.getString("ACCOUNT_TYPE"));
				if ("C".equals(rs.getString("RECORD_STAT")) 
						&& "Y".equals(rs.getString("ACCOUNT_TYPE"))){
					bb.setAC_CLOSE_DATE(rs.getString("MAKER_DT_STAMP"));
				} else if ("C".equals(rs.getString("RECORD_STAT"))
						&& "S".equals(rs.getString("ACCOUNT_TYPE"))){
					bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP"));
				} else if ("C".equals(rs.getString("RECORD_STAT"))
						&& "U".equals(rs.getString("ACCOUNT_TYPE"))){
					bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP"));
				} else {
					bb.setAC_CLOSE_DATE(new String("-"));
				}
//				if (rs.getString("RECORD_STAT").equals("C")){
//					//bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP").substring(0, 10));
//					bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP"));
//				} else {
//					bb.setAC_CLOSE_DATE(new String("-"));
//				}
				bb.setAlt_ac_no(rs.getString("ALT_AC_NO"));
				bb.setAccountClass(rs.getString("ACCOUNT_CLASS"));
				bb.setStatDormant(rs.getString("AC_STAT_DORMANT"));
				bb.setStatCredit(rs.getString("AC_STAT_NO_CR"));
				bb.setStatDebet(rs.getString("AC_STAT_NO_DR"));
				bb.setStatFrozen(rs.getString("AC_STAT_FROZEN"));
				
				if (user_r != null) {
					rr = "";
				} else {
					if ("S18A".equals(rs.getString("ACCOUNT_CLASS")) || "S18B".equals(rs.getString("ACCOUNT_CLASS")) || "S90B".equals(rs.getString("ACCOUNT_CLASS"))){
//							|| "S09A".equals(rs.getString("ACCOUNT_CLASS")) || "S09B".equals(rs.getString("ACCOUNT_CLASS"))) {
						rr = "1";
					}
				}
//				if(rs.getString("ACCOUNT_CLASS").equals("S18A")||(rs.getString("ACCOUNT_CLASS").equals ("S18B"))){ 	
				if ("1".equals(rr)){  	
					bb.setAcyCurrBalance(new BigDecimal(999999999)); 				
					bb.setAcyBlockedAmount(new BigDecimal(999999999));	 			
					bb.setAcyAvlBal(new BigDecimal(999999999)); 			
					bb.setMinBalance(new BigDecimal(999999999)); 		
					bb.setAcy_bal(new BigDecimal(999999999));			
				} else {
					bb.setAcyCurrBalance(rs.getBigDecimal("acy_curr_balance"));	
					bb.setAcyBlockedAmount(rs.getBigDecimal("acy_blocked_amount"));	
					bb.setAcyAvlBal(rs.getBigDecimal("acy_avl_bal"));	
					bb.setMinBalance(rs.getBigDecimal("min_balance"));	
					bb.setAcy_bal(rs.getBigDecimal("acy_bal"));
					System.out.println("acy_curr_balance:" + rs.getBigDecimal("acy_curr_balance"));
					System.out.println("acy_blocked_amount:" + rs.getBigDecimal("acy_blocked_amount"));
					System.out.println("acy_avl_bal:" + rs.getBigDecimal("acy_avl_bal"));
					System.out.println("min_balance:" + rs.getBigDecimal("min_balance"));
					System.out.println("acy_bal:" + rs.getBigDecimal("acy_bal"));
				}				
				noCif = rs.getString("CUST_NO");
				System.out.println("noCif :" + noCif);
				branchCode = rs.getString("branch_code");
				System.out.println("Customer Type =>" + bb.getCustomer_type());
				if ("I".equals(bb.getCustomer_type())) {
					System.out.println("SQL Individual");
					sqlType = "SELECT A.CUST_AC_NO,A.CUST_NO,B.JALAN,B.RT_RW,B.KELURAHAN, "
							+" B.KECAMATAN,B.KOTA_KABUPATEN,B.PROVINSI,B.KODE_POS,B.TEMPAT_LAHIR AS TEMPAT"
							+" FROM STTM_CUST_ACCOUNT A,CSTM_FIELDS_STDCIFI9 B"
							+" WHERE A.CUST_AC_NO = ? AND A.CUST_NO = B.CUSTOMER_NO";
				} else if ("C".equals(bb.getCustomer_type())) {
					System.out.println("SQL Corporate");
					sqlType = "SELECT A.CUST_AC_NO,A.CUST_NO,B.JALAN,B.RT_RW,B.KELURAHAN, "
							+" B.KECAMATAN,B.KOTA_KABUPATEN,B.PROVINSI,B.KODE_POS,TEMPAT_PENDIRI AS TEMPAT"
							+" FROM STTM_CUST_ACCOUNT A,CSTM_FIELDS_STDCIFN9 B"
							+" WHERE A.CUST_AC_NO = ? AND A.CUST_NO = B.CUSTOMER_NO";
				} else if ("B".equals(bb.getCustomer_type())) {
					System.out.println("SQL Bank");
					sqlType = "SELECT A.CUST_AC_NO,A.CUST_NO,B.JALAN,B.RT_RW,B.KELURAHAN, "
							+" B.KECAMATAN,B.KOTA_KABUPATEN,B.PROVINSI,B.KODE_POS, '' AS TEMPAT"
							+" FROM STTM_CUST_ACCOUNT A,CSTM_FIELDS_STDCIFN9 B"
							+" WHERE A.CUST_AC_NO = ? AND A.CUST_NO = B.CUSTOMER_NO";
				}
				System.out.println("sql type-->" + sqlType);
				stat = conn.prepareStatement(sqlType);
				stat.setString(1, norek);
				rs = stat.executeQuery();
				if (rs.next()){
					bb.setJalan(rs.getString("JALAN") == null ? "-" : rs.getString("JALAN"));
					bb.setRt_rw(rs.getString("RT_RW") == null ? "-" : rs.getString("RT_RW"));
					bb.setKelurahan(rs.getString("KELURAHAN") == null ? "-" : rs.getString("KELURAHAN"));
					bb.setKecamatan(rs.getString("KECAMATAN") == null ? "-" : rs.getString("KECAMATAN"));
					bb.setKota_kabupaten(rs.getString("KOTA_KABUPATEN") == null ? "-" : rs.getString("KOTA_KABUPATEN"));
					bb.setProvinsi(rs.getString("PROVINSI") == null ? "-" : rs.getString("PROVINSI"));
					bb.setKode_pos(rs.getString("KODE_POS") == null ? "-" : rs.getString("KODE_POS"));
					bb.setTempat_lahir(rs.getString("TEMPAT") == null ? "-" : rs.getString("TEMPAT"));
				}
			}
			
			if (bb==null){				
				sql2 =  "SELECT A.BRANCH_CODE, D.BRANCH_NAME,A.CUST_AC_NO,A.AC_DESC,CUST_NO,A.CCY,A.ACCOUNT_CLASS,A.ACY_CURR_BALANCE, " +
					    "A.ACY_BLOCKED_AMOUNT, A.ACY_AVL_BAL, C.MIN_BALANCE, " +
					    "A.ADDRESS1,A.ADDRESS2,A.ADDRESS3, E.DESCRIPTION, TO_CHAR(A.AC_OPEN_DATE,'DD/MM/YYYY') AC_OPEN_DATE," +
					    "A.ALT_AC_NO,A.RECORD_STAT,A.CHECKER_DT_STAMP " +
						"FROM STTM_CUST_ACCOUNT A, STTM_ACCLS_CCY_BALANCES C, STTM_BRANCH D, STTM_ACCOUNT_CLASS E " +
					    "WHERE A.CUST_AC_NO=? AND A.BRANCH_CODE=D.BRANCH_CODE AND A.ACCOUNT_CLASS=E.ACCOUNT_CLASS "  ;		
				
				System.out.println("sql2-->" + sql2);
				stat = conn.prepareStatement(sql2);
				stat.setString(1, norek);
				rs = stat.executeQuery();				
				if(rs.next()) {
					bb = new SttmCustAccount();
					bb.setBranch_code(rs.getString("BRANCH_CODE"));
					bb.setBranch_name(rs.getString("BRANCH_NAME"));
					bb.setCUST_AC_NO(rs.getString("CUST_AC_NO"));
					bb.setAcDesc(rs.getString("AC_DESC"));
					bb.setCustNo(rs.getString("CUST_NO"));
					bb.setCcy(rs.getString("CCY"));
					bb.setAddress1(rs.getString("ADDRESS1"));
					bb.setAddress2(rs.getString("ADDRESS2"));
					bb.setAddress3(rs.getString("ADDRESS3"));
					bb.setDesc_acc(rs.getString("DESCRIPTION").toUpperCase().trim());
					//bb.setAC_OPEN_DATE(rs.getString("AC_OPEN_DATE").substring(0, 10));
					bb.setAC_OPEN_DATE(rs.getString("AC_OPEN_DATE"));
					bb.setRecord_stat(rs.getString("RECORD_STAT"));
					if (rs.getString("RECORD_STAT").equals("C")){
						//bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP").substring(0, 10));
						bb.setAC_CLOSE_DATE(rs.getString("CHECKER_DT_STAMP"));
					} else {
						bb.setAC_CLOSE_DATE(new String("-"));
					}
					bb.setAlt_ac_no(rs.getString("ALT_AC_NO"));					
					bb.setAccountClass(rs.getString("ACCOUNT_CLASS"));
					
					if (user_r != null) {
						rr = "";
					} else {
						if ("S18A".equals(rs.getString("ACCOUNT_CLASS")) || "S18B".equals(rs.getString("ACCOUNT_CLASS"))){
//								|| "S09A".equals(rs.getString("ACCOUNT_CLASS")) || "S09B".equals(rs.getString("ACCOUNT_CLASS"))) {
							rr = "1";
						}
					}
//					if(rs.getString("ACCOUNT_CLASS").equals("S18A")||(rs.getString("ACCOUNT_CLASS").equals ("S18B"))){ 
					if ("1".equals(rr)){ 	
						bb.setAcyCurrBalance(new BigDecimal(999999999)); 				
						bb.setAcyBlockedAmount(new BigDecimal(999999999));	 			
						bb.setAcyAvlBal(new BigDecimal(999999999)); 			
						bb.setMinBalance(new BigDecimal(999999999)); 		
						bb.setAcy_bal(new BigDecimal(999999999));			
					} else {
						bb.setAcyCurrBalance(rs.getBigDecimal("ACY_CURR_BALANCE"));	
						bb.setAcyBlockedAmount(rs.getBigDecimal("ACY_BLOCKED_AMOUNT"));	
						bb.setAcyAvlBal(rs.getBigDecimal("ACY_AVL_BAL"));	
						bb.setMinBalance(rs.getBigDecimal("MIN_BALANCE"));	
						bb.setAcy_bal(rs.getBigDecimal("-"));					
					}
				}				
			}	
			stat.setString(1,  branchCode+"~"+norek+ "~");
			sql3 = "select FIELD_VAL_1 AS KODE_MARKETING, (select lov_desc from udtm_lov where field_name = 'SUMBERDANA' and lov = a.FIELD_VAL_10) SUMBER_DANA, " +
					" (select lov_desc from udtm_lov where field_name = 'TUJUAN BUKA REKENING' and lov = a.FIELD_VAL_11) TUJUAN_BUKA_REK " +
					" FROM CSTM_FUNCTION_USERDEF_FIELDS a " +
//					"WHERE a.rec_key = ? and a.function_id in ('IADCUSAC','IADCUSTD')";	
					" WHERE a.rec_key = ? and a.function_id in ('IADCUSAC','IADCUSTD')";
			
			System.out.println("sql3 -->" + sql3);
			stat = conn.prepareStatement(sql3);
			stat.setString(1,  branchCode+"~"+norek+ "~");
			System.out.println("rec key -->" + branchCode+"~"+norek+ "~");
//			stat.setString(1,  norek);
			rs = stat.executeQuery();		
			if(rs.next()) {
				bb.setSumber_dana(rs.getString("SUMBER_DANA"));
				bb.setTujuan_buka_rek(rs.getString("TUJUAN_BUKA_REK"));	
//				bb.setKode_marketing(rs.getString("KODE_MARKETING"));
				bb.setMarketing(rs.getString("KODE_MARKETING") == null ? "-" : rs.getString("KODE_MARKETING"));
				bb.setKode_marketing(rs.getString("KODE_MARKETING") == null ? "-" : rs.getString("KODE_MARKETING"));
				System.out.println("marketing :" + rs.getString("KODE_MARKETING"));
			}	
			
			sql4 = "select UNIQUE_ID_NAME, UNIQUE_ID_VALUE, CUSTOMER_CATEGORY " +
					" from STTM_CUSTOMER where customer_no = ?";
			stat = conn.prepareStatement(sql4);
			
			stat.setString(1, noCif);
			rs = stat.executeQuery();
			if (rs.next()) {
				bb.setP_nationalid(rs.getString("UNIQUE_ID_NAME") == null ? "" : rs.getString("UNIQUE_ID_NAME"));
				bb.setUNIQUE_ID_VALUE(rs.getString("UNIQUE_ID_VALUE") == null ? "" : rs.getString("UNIQUE_ID_VALUE"));
				bb.setCUSTOMER_CATEGORY(rs.getString("CUSTOMER_CATEGORY"));
			}	
			
//			sqlHold = " SELECT ACCOUNT,AMOUNT,TO_CHAR (EXPIRY_DATE, 'dd-mm-yyyy') AS EXPIRY_DATE,REMARKS, "
//					+ " TO_CHAR (EFFECTIVE_DATE, 'dd-mm-yyyy') AS EFFECTIVE_DATE, MOD_NO, RECORD_STAT"
//					+ " FROM CATM_AMOUNT_BLOCKS WHERE ACCOUNT = ? AND RECORD_STAT = 'O' ";
//			
//			stat = conn.prepareStatement(sqlHold);
//			stat.setString(1, norek);
//			rs = stat.executeQuery();
//			if (rs.next()){
//				if (rs.getBigDecimal("AMOUNT") == null){
//					bb.setAmount(new BigDecimal(0));
//				} else {
//					bb.setAmount(rs.getBigDecimal("AMOUNT"));
//				}
//				System.out.println("amount :" + rs.getBigDecimal("AMOUNT"));
//				bb.setExpiryDate(rs.getString("EXPIRY_DATE"));
//				bb.setRemarks(rs.getString("REMARKS"));
//				bb.setEffectiveDate(rs.getString("EFFECTIVE_DATE"));
//				bb.setModNo(rs.getString("MOD_NO"));
//			}
			
			sqlCustType = "select customer_type FROM sttm_customer WHERE customer_no = ?";	
			stat = conn.prepareStatement(sqlCustType);
			stat.setString(1, noCif);
			System.out.println("noCif" + noCif);
			rs = stat.executeQuery();
			if(rs.next()) {
				bb.setCustomer_type(rs.getString("customer_type"));
				
				if ("I".equalsIgnoreCase(bb.getCustomer_type().toString())){
					System.out.println("customer_type INDIVIDU => " +bb.getCustomer_type().toString());
					
					sqlIndv = "select b.resident_status RESIDENT_STATUS, "+
//						    " a.cif_marketing_code MARKETING_CODE, " +
							" to_CHAR(a.expiry_date,'DD/MM/YYYY') AS EXPIRY_DATE,  to_CHAR(b.date_of_birth,'DD/MM/YYYY') AS DATE_OF_BIRTH, " +
							" a.nama_ibukandung NAMA_IBU, b.telephone TLP, b.e_mail EMAIL, b.mobile_number HP, b.SEX, " +
							" (select lov_desc from udtm_lov where field_Name = 'AGAMA' and lov = a.AGAMA) AGAMA, " +
							" (select lov_desc from udtm_lov where field_Name = 'PEKERJAAN' and lov = a.PEKERJAAN) PEKERJAAN, " +
							" (select lov_desc from udtm_lov where field_Name = 'BIDANG PEKERJAAN' and lov = a.BIDANG_PEKERJAAN) BIDANG_PEKERJAAN, " +
							" (select lov_desc from udtm_lov where field_Name = 'STATUS PERKAWINAN' and lov = a.STATUS_PERKAWINAN) STATUS_PERKAWINAN, " +
							" (select lov_desc from udtm_lov where field_Name = 'PENDIDIKAN' and lov = a.PENDIDIKAN) PENDIDIKAN, " +
							" (select lov_desc from udtm_lov where field_name = 'PENGHASILAN TETAP PERBULAN' and lov = a.PNGHSLN_TTP_PRBLN) PNGHSLN_TTP_PRBLN, " +
							" (select lov_desc from udtm_lov where field_name = 'PENGHASILAN TDK TETAP PERBULAN' and lov = a.PNGHSLN_TDK_TTP_PRBULN) PNGHSLN_TDK_TTP_PRBULN, " +
							" (select lov_desc from udtm_lov where field_name = 'PENGELUARAN TETAP PERBULAN' and lov = a.PENGELUARAN_TETAP_PERBULAN) PENGELUARAN_TETAP_PERBULAN, " +
							" (select lov_desc from udtm_lov where field_name = 'RATA2 PENGHASILAN TDK TETAP /BLN' and lov = a.RATA2_PNGHSLN_TDK_TTP_PRBLN) RATA2_PNGHSLN_TDK_TTP_PRBLN, " +
							" (select lov_desc from udtm_lov where field_name = 'INFO BGMN MEMPEROLEH HASIL TAMBAHAN' and lov = a.INFO_BGMN_MMPROLH_HSL_TMBHN) INFO_BGMN_MMPROLH_HSL_TMBHN " +
							  	"  FROM CSTM_FIELDS_STDCIFI9 a, STTM_CUST_PERSONAL b " +
							  " WHERE a.customer_no = ?" +
							  " and a.customer_no = b.customer_no ";	
					stat = conn.prepareStatement(sqlIndv);					
					stat.setString(1, noCif);
					rs = stat.executeQuery();					
					if (rs.next()) {
						bb.setResident_status(rs.getString("RESIDENT_STATUS"));	
//						bb.setKode_marketing(rs.getString("MARKETING_CODE"));
						bb.setTgl_kadaluarsa_id(rs.getString("EXPIRY_DATE"));
						bb.setDATE_OF_BIRTH(rs.getString("DATE_OF_BIRTH"));
						bb.setIbu_kandung(rs.getString("NAMA_IBU"));
						bb.setTelephone(rs.getString("TLP"));
						bb.setEmail(rs.getString("EMAIL"));
						bb.setMobile_number(rs.getString("HP"));
						bb.setFax("-");
						bb.setJenis_kelamin(rs.getString("SEX"));
						bb.setAgama(rs.getString("AGAMA"));
						bb.setPekerjaan(rs.getString("PEKERJAAN"));
						bb.setBidang_pekerjaan(rs.getString("BIDANG_PEKERJAAN"));
						bb.setSts_perkawinan(rs.getString("STATUS_PERKAWINAN"));
						bb.setPendidikan(rs.getString("PENDIDIKAN"));
						bb.setPenghsl_ttp(rs.getString("PNGHSLN_TTP_PRBLN"));
						bb.setPenghsl_tdk_ttp(rs.getString("PNGHSLN_TDK_TTP_PRBULN"));
						bb.setPengeluaran_ttp(rs.getString("PENGELUARAN_TETAP_PERBULAN"));
						bb.setRtrt_penghsl_tdk_ttp(rs.getString("RATA2_PNGHSLN_TDK_TTP_PRBLN"));
						bb.setInfo_mmprolh_hsl(rs.getString("INFO_BGMN_MMPROLH_HSL_TMBHN"));
					}
					
				} else {
					System.out.println("customer_type CORPORATE => " +bb.getCustomer_type().toString());
					
//					sqlCorp = "select a.stresident STRESIDENT, a.cif_marketing_code MARKETING_CODE, to_CHAR(a.tanggal_pendiri,'DD/MM/YYYY') AS TANGGAL_PENDIRI, " +
//							" b.telephone TLP, b.e_mail EMAIL, b.mobile_number FAX " +
//							  	" FROM CSTM_FIELDS_STDCIFN9 a, STTM_CUST_PERSONAL b " +
//							  " WHERE a.customer_no = ?" +
//							  " and a.customer_no = b.customer_no ";	
					
					sqlCorp = "SELECT STRESIDENT, CIF_MARKETING_CODE AS MARKETING_CODE, "
							+ " TO_CHAR (TANGGAL_PENDIRI,'DD/MM/YYYY') AS TANGGAL_PENDIRI,"
							+ " NO_TELP, NO_FAX, EMAIL"
							+ " FROM CSTM_FIELDS_STDCIFN9"
							+ " WHERE CUSTOMER_NO = ?" ;
					
					stat = conn.prepareStatement(sqlCorp);					
					stat.setString(1, noCif);					
					rs = stat.executeQuery();										
					if (rs.next()) {
						bb.setResident_status(rs.getString("STRESIDENT"));
						bb.setKode_marketing(rs.getString("MARKETING_CODE"));
						bb.setTgl_kadaluarsa_id("-");
						bb.setDATE_OF_BIRTH(rs.getString("TANGGAL_PENDIRI"));
						bb.setIbu_kandung("-");
						bb.setTelephone(rs.getString("NO_TELP"));
						bb.setEmail(rs.getString("EMAIL"));
						bb.setMobile_number("-");
						bb.setFax(rs.getString("NO_FAX"));
						bb.setJenis_kelamin("-");
						bb.setAgama("-");
						bb.setPekerjaan("-");
						bb.setBidang_pekerjaan("-");
						bb.setSts_perkawinan("-");
						bb.setPendidikan("-");
						bb.setPenghsl_ttp("-");
						bb.setPenghsl_tdk_ttp("-");
						bb.setPengeluaran_ttp("-");
						bb.setRtrt_penghsl_tdk_ttp("-");
						bb.setInfo_mmprolh_hsl("-");
					}
				}
			}
			
			
			System.out.println("SQL getDetailCustom =>" +sql);
			System.out.println("SQL2 getDetailCustom =>" +sql2);
			System.out.println("SQL3 getDetailCustom =>" +sql3);
			System.out.println("SQL4 getDetailCustom =>" +sql4);
			System.out.println("SQLCustType getDetailCustom =>" +sqlCustType);
			System.out.println("SQLIndv  getDetailCustom =>" +sqlIndv );
			System.out.println("SQLCorp getDetailCustom =>" +sqlCorp);
			System.out.println("SQLType getDetailCustom =>" +sqlType);
			System.out.println("SQLSaldoHold =>" + sqlHold);
			
		} catch (SQLException ex) {	
			log.error(" getDetailCustom : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs); 
		}
		return bb;
	
	}
	
	public List getSaldo (String acc){
		System.out.println("norek:" + acc);
		List lbb = new ArrayList();
		saldoHold bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " SELECT ACCOUNT,AMOUNT,TO_CHAR (EXPIRY_DATE, 'dd-mm-yyyy') AS EXPIRY_DATE,REMARKS, "
					+ " TO_CHAR (EFFECTIVE_DATE, 'dd-mm-yyyy') AS EFFECTIVE_DATE, MOD_NO, RECORD_STAT"
					+ " FROM CATM_AMOUNT_BLOCKS WHERE ACCOUNT = ? AND RECORD_STAT = 'O' ";
			System.out.println("sql:" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc);
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new saldoHold();
				bb.setAcc(rs.getString("ACCOUNT"));
				if (rs.getBigDecimal("AMOUNT") == null){
					bb.setAmount(new BigDecimal(0));
				} else {
					bb.setAmount(rs.getBigDecimal("AMOUNT"));
				}
				System.out.println("amount :" + rs.getBigDecimal("AMOUNT"));
				bb.setExpiryDate(rs.getString("EXPIRY_DATE"));
				System.out.println("expiry:" + rs.getString("EXPIRY_DATE"));
				bb.setRemarks(rs.getString("REMARKS"));
				System.out.println("ket:" + rs.getString("REMARKS"));
				bb.setEffectiveDate(rs.getString("EFFECTIVE_DATE"));
				System.out.println("efektif:" + rs.getString("EFFECTIVE_DATE"));
//				bb.setModNo(rs.getString("MOD_NO"));
				lbb.add(bb);
			}
		} catch (Exception e) {
			log.error(" getMbanking : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs); 
		}
		return lbb;
		
	}
	
//	public banking getMbanking (String norek){
//		System.out.println("norek -->" + norek);
//		banking bb = null;
//		Connection conn = null;
//		PreparedStatement stat = null;
//		ResultSet rs = null;
//		String sql = "";
//		
//		try {
//			conn = DatasourceEntry.getInstance().getmBankingDS().getConnection();
//			sql = " SELECT ACCT_NUM, LENGTH(PHONE_NUM),PHONE_NUM AS HANDPHONE, "
//					+ "CASE WHEN LENGTH(PHONE_NUM) = '11' THEN SUBSTR(PHONE_NUM,1,8)||'xxx'"
//					+ " WHEN LENGTH(PHONE_NUM) = '12' THEN SUBSTR(PHONE_NUM,1,9)||'xxx'"
//					+ " WHEN LENGTH(PHONE_NUM) = '13' THEN SUBSTR(PHONE_NUM,1,10)||'xxx' END AS PHONE,"
//					+ " USER_NAME, USER_STATUS"
//					+ " FROM MMB_USERS_REG WHERE ACCT_NUM = ?";
//			
//			System.out.println("mbanking :" + sql);
//			stat = conn.prepareStatement(sql);
//			stat.setString(1, norek);
//			rs = stat.executeQuery();
//			if (rs.next()){
//				bb = new banking();
//				bb.setAcc(rs.getString("ACCT_NUM"));
//				bb.setRegistrasi(rs.getString("PHONE"));
//				bb.setUserName(rs.getString("USER_NAME"));
//				bb.setUserStatus(rs.getString("USER_STATUS"));
////				bb.setNoHandphone(rs.getString("HANDPHONE"));
////				bb.setNoHandphone(rs.getString("PHONE"));
//				System.out.println("handphone -->" + rs.getString("PHONE"));
//			}
//		} catch (Exception e) {
//			log.error(" getMbanking : " + e.getMessage());
//		} finally {
//			closeConnDb(conn, stat, rs); 
//		}
//		return bb;
//		
//	}
	
	public List getSaldoHold (String acc){
		List lbb = new ArrayList();
		SttmCustAccount bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";	
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select account,amount,to_char(expiry_date,'dd-mm-yyyy') as expiry_date,remarks from CATM_AMOUNT_BLOCKS "
					+ "where account = ? and record_stat = 'O' order by expiry_date desc";
			
            stat = conn.prepareStatement(sql);
            stat.setString(1, acc);
            System.out.println("sql :" + sql);
            System.out.println("Acc :" + acc);
            
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new SttmCustAccount();
			    bb.setCUST_AC_NO(rs.getString("account"));
			    bb.setAmount(rs.getBigDecimal("amount"));
			    bb.setExpiryDate(rs.getString("expiry_date"));
			    bb.setRemarks(rs.getString("remarks"));
			    System.out.println("remarks :" + rs.getString("remarks"));
			    System.out.println("expiry_date :" + rs.getString("expiry_date"));
				lbb.add(bb);
				}
			} catch (SQLException ex) {
				log.error(" getListSaldoHold : " + ex.getMessage());
			} finally {
				closeConnDb(conn, stat, rs);
			}
			return lbb;
	}
	
	public Customer getCustomer(String acc) {
		System.out.println("acc customer :" + acc);
		Customer cust = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = " select b.branch_code, b.branch_name, b.branch_addr1 || '\n' || b.branch_addr2 || '\n' || b.branch_addr3 as alamat_cab, " +
//					"a.cust_ac_no, a.ac_desc, a.cust_no, a.account_class, (select description from sttm_account_class where account_class = a.account_class) as description, " +
//					"a.ccy, a.address1 || ' ' || a.address2 || ' ' || a.address3 || ' ' || a.address4 as alamat_nsbh, a.acy_uncollected, a.acy_blocked_amount, c.min_balance, a.acy_avl_bal," +
//					"(select e_mail from sttm_cust_personal where customer_no = a.cust_no) as email " +
//					"from sttm_cust_account a, sttm_branch b, STTM_ACCLS_CCY_BALANCES c " +
//					"where b.branch_code = a.branch_code and a.cust_ac_no = ? and a.account_class = c.account_class and a.ccy = c.ccy_code";
			sql = "select b.branch_code, b.branch_name, b.branch_addr1 || '\n' || b.branch_addr2 || '\n' || b.branch_addr3 as alamat_cab, " +
					"a.cust_ac_no, a.ac_desc, a.cust_no, a.account_class, (select description from sttm_account_class where account_class = a.account_class) as description," +
					"a.ccy, d.address_line1 || ' ' || d.address_line3 || ' ' || d.address_line2 || ' ' || d.address_line4 as alamat_nsbh," +
					"a.acy_uncollected, a.acy_blocked_amount, c.min_balance, a.acy_avl_bal, " +
					"(select e_mail from sttm_cust_personal where customer_no = a.cust_no) as email " +
					"from sttm_cust_account a, sttm_branch b, STTM_ACCLS_CCY_BALANCES c, sttm_customer d " +
					"where b.branch_code = a.branch_code and a.account_class = c.account_class and a.cust_no = d.customer_no and " +
					"a.cust_ac_no = ? and a.ccy = c.ccy_code ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc.trim());
			System.out.println("sql customer :" + sql);
			rs = stat.executeQuery();
			if (rs.next()) {
				cust = new Customer();
				cust.setBranch_cd(rs.getString("branch_code") == null ? "" : rs.getString("branch_code"));
				cust.setBranch_name(rs.getString("branch_name") == null ? "" : rs.getString("branch_name"));
				cust.setBranch_addrs(rs.getString("alamat_cab") == null ? "" : rs.getString("alamat_cab"));		
				cust.setCust_acc(rs.getString("cust_ac_no") == null ? "" : rs.getString("cust_ac_no"));
				cust.setCust_name(rs.getString("ac_desc") == null ? "" : rs.getString("ac_desc"));
				cust.setCust_no(rs.getString("cust_no") == null ? "" : rs.getString("cust_no"));
				cust.setCust_acc_clas(rs.getString("account_class") == null ? "" : rs.getString("account_class"));
				cust.setCust_acc_clas_desc(rs.getString("description") == null ? "" : rs.getString("description"));
				cust.setCust_ccy(rs.getString("ccy") == null ? "" : rs.getString("ccy"));
				cust.setCust_addrs(rs.getString("alamat_nsbh") == null ? "" : rs.getString("alamat_nsbh"));
				cust.setSld_hold(rs.getBigDecimal("acy_blocked_amount") == null ? new BigDecimal(0) : rs.getBigDecimal("acy_blocked_amount"));
				cust.setSld_float(rs.getBigDecimal("acy_uncollected") == null ? new BigDecimal(0) : rs.getBigDecimal("acy_uncollected"));
				cust.setSld_min(rs.getBigDecimal("min_balance") == null ? new BigDecimal(0) : rs.getBigDecimal("min_balance"));				
				cust.setCust_email(rs.getString("email") == null ? "" : rs.getString("email"));
			}
		} catch (SQLException ex) {
			log.error("Error in getCustomer : " + ex.getMessage());
			System.out.println("Error in getCustomer : " + ex.getMessage());
		
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return cust;
	}	
	
	public AvgBal getAvgBal (String acc, String period){
		AvgBal saldo = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			System.out.println("period :" + period);
			
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();
			sql = "select acc_no,acc_desc,product_code,period_code,ccy,lcy_bal,acy_bal,cust_no "
					+ "from(select avgbal.BRANCH_CODE,cust.BRANCH_NAME,avgbal.GL_CODE,cust.ACC_DESC, "
					+ "substr(avgbal.FIN_YEAR,3,6) || substr(avgbal.PERIOD_CODE,2,3) as period_code, "
					+ "avgbal.CCY,avgbal.LCY_BAL,avgbal.ACY_BAL,cust.ACC_NO,cust.cust_no,cust.product_code,cust.dw_row_ind "
					+ "from BMIDWH.GLTB_AVGBAL avgbal join BMIDWH.DIM_CUST_ACCOUNT cust "
					+ "on avgbal.BRANCH_CODE = cust.BRANCH_CODE and avgbal.GL_CODE = cust.ACC_NO)a "
					+ "where acc_no = ? and period_code = ? and product_code not in ('S18A','S18B') and dw_row_ind = 'Y'";
				 
				 System.out.println("sql getAvgBal :" + sql);
				 int i = 1;
				 stat = conn.prepareStatement(sql);
				 stat.setString(i++, acc.trim());
				 stat.setString(i++, period.trim());
				 rs = stat.executeQuery();
				 if (rs.next()){
					 saldo = new AvgBal();
					 saldo.setAcNo(rs.getString("acc_no"));
					 saldo.setAccDesc(rs.getString("acc_desc"));
					 saldo.setProductCode(rs.getString("product_code"));
					 saldo.setPeriodCode(rs.getString("period_code"));
					 saldo.setCcy(rs.getString("ccy"));
					 saldo.setAcyBal(rs.getBigDecimal("lcy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("lcy_bal"));
					 System.out.println("lcy Amount :" + rs.getString("lcy_bal"));
					 saldo.setAcyBal(rs.getBigDecimal("acy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("acy_bal"));
					 saldo.setCustNo(rs.getString("cust_no"));
				 }
				 
		} catch (Exception ex) {
			log.error("Error in getAvgBal : " + ex.getMessage());	
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return saldo;
	}
	
	public Gltb_avgbal getGltb_avgbal(String acc) {
		Gltb_avgbal cust = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select branch_code, gl_code, fin_year, period_code, ccy, acy_bal, lcy_bal, from_date, user_id, ytodacy_bal, ytodlcy_bal " +
					"from gltb_avgbal where gl_code = ? ";
			
			System.out.println("sql gltb avgbal :" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc.trim());
			rs = stat.executeQuery();
			if (rs.next()) {
				cust = new Gltb_avgbal();
				cust.setBranch_code(rs.getString("branch_code") == null ? "" : rs.getString("branch_code"));
				cust.setGl_code(rs.getString("gl_code") == null ? "" : rs.getString("gl_code"));		
				cust.setFin_year(rs.getString("fin_year") == null ? "" : rs.getString("fin_year"));
				cust.setPeriod_code(rs.getString("period_code") == null ? "" : rs.getString("period_code"));
				cust.setAcy_bal(rs.getBigDecimal("acy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("acy_bal"));
				System.out.println("acy bal :" + rs.getString("acy_bal"));
				cust.setLcy_bal(rs.getBigDecimal("lcy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("lcy_bal"));				
				cust.setFrom_date(rs.getDate("from_date"));
				cust.setUser_id(rs.getString("user_id") == null ? "" : rs.getString("user_id"));
				cust.setYtodacy_bal(rs.getBigDecimal("ytodacy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("ytodacy_bal"));
				cust.setYtodlcy_bal(rs.getBigDecimal("ytodlcy_bal") == null ? new BigDecimal(0) : rs.getBigDecimal("ytodlcy_bal"));
			}
		} catch (SQLException ex) {
			log.error("Error in getGltb_avgbal : " + ex.getMessage());		
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return cust;
	}
	
	public boolean getMenuStatus(List argGroup, String argAction) {
		boolean menu = false;
		Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        String subSQL = "(";
        System.out.println("argGroup" + argGroup);
        try {
		Iterator it = argGroup.iterator();
    	while (it.hasNext()) {
        	subSQL = subSQL +"'"+it.next()+"'"+",";
    	}
	    	subSQL = subSQL.substring(0,subSQL.length()-1)  + ")";	
	    	System.out.println("subSQL=" + subSQL);
	    	
	    	conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
//	    	Class.forName("org.postgresql.Driver");
//        	conn = DriverManager.getConnection("jdbc:postgresql://10.55.109.50:5432/REPORTSRV", "postgres", "postgres");
			sql = "select count(*) as jumlah from access_role a,menufunc b,menuhdr c"
				+" where a.func_id = b.func_id and b.hdr_id = c.hdr_id and a.group_id in "+ subSQL + " and b.func_act like '%"+ argAction+"%' ";
			System.out.println("sql role :" + sql);
	        stat = conn.prepareStatement(sql);
	        
	        rs = stat.executeQuery();
	        if (rs.next()) {
	        	int total = rs.getInt("jumlah");
	        	if (total > 0){
	        		menu = true;
	        		System.out.println("total:" + total + "menu:" + menu);
	        	}
	        }
        
		} catch (SQLException ex) {
	        log.error(" getMenuStatus :1: " + ex.getMessage());
	    } catch (Exception ex2) {
	        log.error(" getMenuStatus2 :2: " + ex2.getMessage());
	    } finally {
	    	closeConnDb(conn, stat, rs);
	    }
        return menu;
	}
	
	public List getRole2(List groupList) {
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        String subSQL = "(";
        ArrayList header = new ArrayList();
       
        try {
        	Iterator it = groupList.iterator();
        	while (it.hasNext()) {
            	subSQL = subSQL +"'"+it.next()+"'"+",";
        	}
        	subSQL = subSQL.substring(0,subSQL.length()-1)  + ")";	
        	System.out.println("subSQL=" + subSQL);
        	
        	conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
//        	conn = DatasourceEntry.getInstance().getNewMarsDS().getConnection();
        	
//        	Class.forName("org.postgresql.Driver");
//        	conn = DriverManager.getConnection("jdbc:postgresql://10.55.109.50:5432/REPORTSRV", "postgres", "postgres");
        	System.out.println("conn:" + conn);
			sql = "select distinct b.func_nm,b.func_act, c.hdr_nm, b.desc_func from access_role a,menufunc b,menuhdr c"
				+" where a.func_id = b.func_id and b.hdr_id = c.hdr_id and a.group_id in "+ subSQL +"order by c.hdr_nm,b.desc_func";
			
            stat = conn.prepareStatement(sql);
            System.out.println("sql role :" + sql);
            
            String headerName = "";
            String subHeaderName = "";
            
            Role rl = null;
            RoleMenu roleMenu = new RoleMenu();
            RoleSubMenu roleSubMenu = new RoleSubMenu();
            ArrayList subHeader = new ArrayList();
            ArrayList detail = new ArrayList();
            
            rs = stat.executeQuery();
            while (rs.next()) {
               rl = new Role();
//               rl.setGroup_id(rs.getString("group_id"));
               rl.setFunc_nm(rs.getString("func_nm"));
               rl.setFunc_act(rs.getString("func_act"));
               rl.setHdr_nm(rs.getString("hdr_nm"));
               rl.setDesc_func(rs.getString("desc_func"));
               if(!(subHeaderName.equals(rs.getString("desc_func"))) && !("".equals(subHeaderName))) {
            	   roleSubMenu = new RoleSubMenu();
            	   System.out.println("subHeaderName3:" + subHeaderName);
            	   roleSubMenu.setSubMenu(subHeaderName);
            	   roleSubMenu.setDetail(detail);
            	   subHeader.add(roleSubMenu);
            	   detail = new ArrayList();
               }
               detail.add(rl);
               if(!(headerName.equals(rs.getString("hdr_nm"))) && !("".equals(headerName))) {
            	   roleMenu = new RoleMenu();
            	   System.out.println("hdr_nm:" + headerName);
            	   System.out.println("subHeaderName:" + subHeaderName);
            	   roleMenu.setHeaderName(headerName);
            	   roleMenu.setSubSubMenu(subHeader);
            	   header.add(roleMenu);
            	   subHeader = new ArrayList();
            	   roleSubMenu = new RoleSubMenu();
               }
               headerName = rs.getString("hdr_nm");
               subHeaderName = rs.getString("desc_func");
            }
            if(!("".equals(headerName))){
            	roleSubMenu = new RoleSubMenu();
            	roleSubMenu.setDetail(detail);
            	roleSubMenu.setSubMenu(subHeaderName);
            	System.out.println("hdr_nm2:" + headerName);
            	System.out.println("subHeaderName2:" + subHeaderName);
            	subHeader.add(roleSubMenu);
            	roleMenu = new RoleMenu();
            	roleMenu.setHeaderName(headerName);
            	roleMenu.setSubSubMenu(subHeader);
            	header.add(roleMenu);
            }
        } catch (SQLException ex) {
            log.error(" getRole :1: " + ex.getMessage());
        } catch (Exception ex2) {
            log.error(" getRole2 :2: " + ex2.getMessage());
        } finally {
        	closeConnDb(conn, stat, rs);
        }
        return header;
    }
	
	public List getListJurnalDetail(String userId) {
		List lbb = new ArrayList();
		JurnalTodayDetail bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";	
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no,ac_desc  "
				 +" from(select c.user_id,substr(a.trn_ref_no, 1, 3) as kode_cabang,a.trn_code as kode_transaksi,a.instrument_code as nomor_warkat, "
				 +" to_char(a.txn_dt_time,'DD-MM-YYYY') as tanggal,to_char(a.txn_dt_time, 'HH24:MI') as jam,b.trn_desc || ':' || "
				 +" ACPKS_FCJ_BMIJRNAL.cf_addltextformula (a.trn_ref_no, a.event_sr_no) || ',' || (SELECT DISTINCT x.ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL x "
				 +" where x.reference_no = a.trn_ref_no and serial_no = nvl(a.curr_no, a.ENTRY_SEQ_NO))||''||(SELECT y.narrative FROM detbs_rtl_teller y "
				 +" where y.trn_ref_no = a.trn_ref_no) as ket_kode_transaksi,case when a.drcr_ind = 'D' then (case when a.trn_code = '100' then null "
				 +" else(case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as debet,case when a.drcr_ind = 'C' then (case "
				 +" when a.trn_code = '200' then null else (case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as kredit, "
				 +" a.auth_id AS user_approval,a.ac_no,case when length(a.ac_no)=10 then (select ac_desc from sttm_cust_account where cust_ac_no=a.ac_no) else (select gl_desc from gltm_glmaster where gl_code=a.ac_no) END AS ac_desc from actb_daily_log a, sttm_trn_code b,(select d.user_id from smtb_user d, smtb_user_role c "
				 +" where c.user_id = d.user_id and c.role_id IN ('TL_IN','TL_IN_JR','BO_IN','BO_IN_JR','BO_ALL_IN',"
				 +" 'TL_IN_JR_1','TL_IN_JR_2','TL_IN_JR_3','TL_IN_SEN_1','TL_IN_SEN_2','TL_IN_SEN_3','TL_IN_SEN_4','TL_IN_SEN_5',"
				 +" 'TL_IN_SEN_6','TL_IN_SEN_7','TL_IN_SEN_8','TL_MULIA_1','TL_MULIA_2','TL_MULIA_3','TLR_HYBRID','TLR_HYBRID_LSTP','FL_HYBRID')) c "
				 +" where a.trn_code = b.trn_code and a.user_id = c.user_id and a.delete_stat <> 'D' and a.ac_no NOT IN ('195002002', '695001006', '195001006') "
				 +" and a.ac_no NOT BETWEEN '101002000' AND '1010022099' and a.user_id NOT IN ('FLEXGW','FLEXSWITCH','SYSTEM') "
				 +" and a.auth_id NOT IN ('FLEXGW','FLEXSWITCH','SYSTEM')) a1 where user_id = ?"
				 +" group by user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no,ac_desc"
				 + " order by jam";
	                 
//	                 if (!"".equals(userId) && userId !=null){
//	                	 sql += " user_id = ? ";
//	                 } else if (!"".equals(kodecabang) && kodecabang !=null){
//	                	 sql += " kode_cabang = ? ";
//	                 }
//	                 
//	                 sql += " group by user_id,kode_cabang,kode_transaksi,nomor_warkat,tanggal,ket_kode_transaksi,"
//	                 		+ " jam,keterangan,user_approval,ac_no order by jam";
                 
                 stat = conn.prepareStatement(sql);
                 stat.setString(1, userId);
                 System.out.println("sql :" + sql);
 		         
//                 if (!"".equals(userId) && userId != null) {
//                	 stat.setString(1, userId);
//                 } else if (!"".equals(kodecabang) && kodecabang != null) {
//                	 stat.setString(1, kodecabang);
//                 }
		
				rs = stat.executeQuery();
				while (rs.next()) {
					bb = new JurnalTodayDetail();
				    bb.setUser_id(rs.getString("user_id"));
				    bb.setKode_cabang(rs.getString("kode_cabang"));
					bb.setTanggal(rs.getString("tanggal"));
					bb.setJam(rs.getString("jam"));
					bb.setKode_transaksi(rs.getString("kode_transaksi"));
					bb.setNomor_warkat(rs.getString("nomor_warkat") == null ? "-" : rs.getString("nomor_warkat"));
					bb.setKet_kode_transaksi(rs.getString("ket_kode_transaksi"));
					bb.setUser_approval(rs.getString("user_approval"));
					bb.setAc_no(rs.getString("ac_no"));
					bb.setAc_desc(rs.getString("ac_desc"));
					
					if (rs.getBigDecimal("debet") != null) {
						bb.setDebet(rs.getBigDecimal("debet"));
					} else {
						bb.setDebet(new BigDecimal(0));
					}
					
					if (rs.getBigDecimal("kredit") != null){
						bb.setKredit(rs.getBigDecimal("kredit"));
					} else {
						bb.setKredit(new BigDecimal(0));
					}
//					bb.setPeriksa(rs.getString("periksa"));
//					if (rs.getString("periksa").equals("Belum Diperiksa")){
//						
//					}
					lbb.add(bb);
//					System.out.println("acc :" + rs.getString("ac_no"));
					}
				} catch (SQLException ex) {
					log.error(" getListJurnalDetail : " + ex.getMessage());
				} finally {
					closeConnDb(conn, stat, rs);
				}
				return lbb;
	}
	
	public JurnalTodayDetail JurnalDetail(JurnalTodayDetail jurnal) {
//		List lbb = new ArrayList();
		JurnalTodayDetail bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";	
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no,periksa "
				 +" from(select c.user_id,substr(a.trn_ref_no, 1, 3) as kode_cabang,a.trn_code as kode_transaksi,a.instrument_code as nomor_warkat, "
				 +" to_char(a.txn_dt_time,'DD-MM-YYYY') as tanggal,to_char(a.txn_dt_time, 'HH24:MI') as jam,b.trn_desc || ':' || "
				 +" ACPKS_FCJ_BMIJRNAL.cf_addltextformula (a.trn_ref_no, a.event_sr_no) || ',' || (SELECT DISTINCT x.ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL x "
				 +" where x.reference_no = a.trn_ref_no and serial_no = nvl(a.curr_no, a.ENTRY_SEQ_NO))||''||(SELECT y.narrative FROM detbs_rtl_teller y "
				 +" where y.trn_ref_no = a.trn_ref_no) as ket_kode_transaksi,case when a.drcr_ind = 'D' then (case when a.trn_code = '100' then null "
				 +" else(case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as debet,case when a.drcr_ind = 'C' then (case "
				 +" when a.trn_code = '200' then null else (case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as kredit, "
				 +" a.auth_id AS user_approval,a.ac_no, 'Belum Diperiksa' as periksa from actb_daily_log a, sttm_trn_code b,(select d.user_id from smtb_user d, smtb_user_role c "
				 +" where c.user_id = d.user_id and c.role_id IN ('TL_IN','TL_IN_JR','BO_IN','BO_IN_JR','BO_ALL_IN','TL_IN_JR_1','TL_IN_JR_2','TL_IN_JR_3', "
				 +" 'TL_IN_SEN_1','TL_IN_SEN_2','TL_IN_SEN_3','TL_IN_SEN_4','TL_IN_SEN_5','TL_IN_SEN_6','TL_IN_SEN_7','TL_IN_SEN_8')) c "
				 +" where a.trn_code = b.trn_code and a.user_id = c.user_id and a.delete_stat <> 'D' and a.ac_no NOT IN ('195002002', '695001006', '195001006') "
				 +" and a.ac_no NOT BETWEEN '101002000' AND '1010022099' and a.user_id NOT IN ('FLEXGW', 'FLEXSWITCH', 'SYSTEM') "
				 +" and a.auth_id NOT IN ('FLEXGW', 'FLEXSWITCH', 'SYSTEM')) a1 where user_id = ? order by jam";
	                 
//	                 if (!"".equals(userId) && userId !=null){
//	                	 sql += " user_id = ? ";
//	                 } else if (!"".equals(kodecabang) && kodecabang !=null){
//	                	 sql += " kode_cabang = ? ";
//	                 }
//	                 
//	                 sql += " group by user_id,kode_cabang,kode_transaksi,nomor_warkat,tanggal,ket_kode_transaksi,"
//	                 		+ " jam,keterangan,user_approval,ac_no order by jam";
                 
                 stat = conn.prepareStatement(sql);
                 stat.setString(1, jurnal.getUser_id());
                 System.out.println("sql :" + sql);
 		         
//                 if (!"".equals(userId) && userId != null) {
//                	 stat.setString(1, userId);
//                 } else if (!"".equals(kodecabang) && kodecabang != null) {
//                	 stat.setString(1, kodecabang);
//                 }
		
				rs = stat.executeQuery();
				while (rs.next()) {
					bb = new JurnalTodayDetail();
				    bb.setUser_id(rs.getString("user_id"));
				    bb.setKode_cabang(rs.getString("kode_cabang"));
					bb.setTanggal(rs.getString("tanggal"));
					bb.setJam(rs.getString("jam"));
					bb.setKode_transaksi(rs.getString("kode_transaksi"));
					bb.setNomor_warkat(rs.getString("nomor_warkat") == null ? "-" : rs.getString("nomor_warkat"));
					bb.setKet_kode_transaksi(rs.getString("ket_kode_transaksi"));
					bb.setUser_approval(rs.getString("user_approval"));
					bb.setAc_no(rs.getString("ac_no"));
					
					if (rs.getBigDecimal("debet") != null) {
						bb.setDebet(rs.getBigDecimal("debet"));
					} else {
						bb.setDebet(new BigDecimal(0));
					}
					
					if (rs.getBigDecimal("kredit") != null){
						bb.setKredit(rs.getBigDecimal("kredit"));
					} else {
						bb.setKredit(new BigDecimal(0));
					}
					bb.setPeriksa(rs.getString("periksa"));
//					if (rs.getString("periksa").equals("Belum Diperiksa")){
//						
//					}
//					lbb.add(bb);
					}
				} catch (SQLException ex) {
					log.error(" getListJurnalDetail : " + ex.getMessage());
				} finally {
					closeConnDb(conn, stat, rs);
				}
				return bb;
	}
	
	public boolean saveMutasi (JurnalTodayDetail jurnal){
		boolean num = false;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			
			sql = "INSERT INTO mutasi_teller (user_id) values (?)";
//			sql = "INSERT INTO mutasi_teller values (?,?,?,?,?,?,?,?,?,?,?,'Sudah Di periksa')";
			stat = conn.prepareStatement(sql);
			
			int i = 1;
//			System.out.println("user");
			stat.setString(i++, jurnal.getUser_id());
			System.out.println("");
//			System.out.println("cabang");
//			stat.setString(i++, jurnal.getKode_cabang());
//			System.out.println("tanggal ");
//			stat.setString(i++, jurnal.getTanggal());
//			System.out.println("jam ");
//			stat.setString(i++, jurnal.getJam());
//			System.out.println("tran code ");
//			stat.setString(i++, jurnal.getKode_transaksi());
//			System.out.println("ac no ");
//			stat.setString(i++, jurnal.getAc_no());
//			System.out.println("warkat ");
//			stat.setString(i++, jurnal.getNomor_warkat());
//			System.out.println("ket ");
//			stat.setString(i++, jurnal.getKet_kode_transaksi());
//			System.out.println("debet ");
//			stat.setBigDecimal(i++, jurnal.getDebet());
//			System.out.println("kredit ");
//			stat.setBigDecimal(i++, jurnal.getKredit());
//			System.out.println("approval ");
//			stat.setString(i++, jurnal.getUser_approval());
//			System.out.println("periksa ");
//			stat.setString(i++, jurnal.getPeriksa());
			stat.executeQuery();
			num = true;
			System.out.println("insert :" + sql);
		} catch (Exception ex) {
			log.error(ex.getMessage());
		}
		finally {
			closeConnDb(conn, stat, rs);
		}
			return num;
	}
	
	public List getListJurnalDetailCetakan (String user, String cabang) {
		List lbb = new ArrayList();
		JurnalTodayDetail bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no "
					 +" from(select c.user_id,substr(a.trn_ref_no, 1, 3) as kode_cabang,a.trn_code as kode_transaksi,a.instrument_code as nomor_warkat, "
					 +" to_char(a.txn_dt_time,'DD-MM-YYYY') as tanggal,to_char(a.txn_dt_time, 'HH24:MI') as jam,b.trn_desc || ':' || "
					 +" ACPKS_FCJ_BMIJRNAL.cf_addltextformula (a.trn_ref_no, a.event_sr_no) || ',' || (SELECT DISTINCT x.ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL x "
					 +" where x.reference_no = a.trn_ref_no and serial_no = nvl(a.curr_no, a.ENTRY_SEQ_NO))||''||(SELECT y.narrative FROM detbs_rtl_teller y "
					 +" where y.trn_ref_no = a.trn_ref_no) as ket_kode_transaksi,case when a.drcr_ind = 'D' then (case when a.trn_code = '100' then null "
					 +" else(case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as debet,case when a.drcr_ind = 'C' then (case "
					 +" when a.trn_code = '200' then null else (case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as kredit, "
					 +" a.auth_id AS user_approval,a.ac_no from actb_daily_log a, sttm_trn_code b,(select d.user_id from smtb_user d, smtb_user_role c "
					 +" where c.user_id = d.user_id and c.role_id IN ('TL_IN','TL_IN_JR','BO_IN','BO_IN_JR','BO_ALL_IN','TL_IN_JR_1','TL_IN_JR_2','TL_IN_JR_3', "
					 +" 'TL_IN_SEN_1','TL_IN_SEN_2','TL_IN_SEN_3','TL_IN_SEN_4','TL_IN_SEN_5','TL_IN_SEN_6','TL_IN_SEN_7','TL_IN_SEN_8')) c "
					 +" where a.trn_code = b.trn_code and a.user_id = c.user_id and a.delete_stat <> 'D' and a.ac_no NOT IN ('195002002', '695001006', '195001006') "
					 +" and a.ac_no NOT BETWEEN '101002000' AND '1010022099' and a.user_id NOT IN ('FLEXSWITCH', 'SYSTEM') "
					 +" and a.auth_id NOT IN ('FLEXSWITCH', 'SYSTEM')) a1 where user_id = ? "
					 +" group by user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no"
					 +" order by jam";
			
//			if (!"".equals(user) && user != null){
//				sql += " user_id = ?";
//			} else if (!"".equals(cabang) && cabang != null){
//				sql += " kode_cabang = ?";
//			}
//			
//			sql += " group by user_id,kode_cabang,kode_transaksi,nomor_warkat,tanggal,ket_kode_transaksi,"
//             		+ " jam,keterangan,user_approval,ac_no order by jam";
			
			stat = conn.prepareStatement(sql);
			stat.setString(1, user);
			System.out.println("SQL getListJurnalDetailCetakan :" + sql);
//			
//			if (!"".equals(user) && user != null){
//				stat.setString(1, user);
//			} else if (!"".equals(cabang) && cabang != null) {
//				stat.setString(1, cabang);
//			}
			
			rs = stat.executeQuery();
			while(rs.next()) {
				bb = new JurnalTodayDetail();
				bb.setUser_id(rs.getString("user_id"));
				bb.setKode_cabang(rs.getString("kode_cabang"));
				bb.setTanggal(rs.getString("tanggal"));
				bb.setJam(rs.getString("jam"));
				bb.setKode_transaksi(rs.getString("kode_transaksi"));
				bb.setNomor_warkat(rs.getString("nomor_warkat") == null ? "-" : rs.getString("nomor_warkat"));
				bb.setKeterangan(rs.getString("keterangan"));
				bb.setKet_kode_transaksi(rs.getString("ket_kode_transaksi"));
				bb.setKredit(rs.getBigDecimal("kredit"));
				bb.setUser_approval(rs.getString("user_approval"));
				bb.setAc_no(rs.getString("ac_no"));
					
				if (rs.getBigDecimal("debet") != null) {
					bb.setDebet(rs.getBigDecimal("debet"));
				} else {
					bb.setDebet(new BigDecimal(0));
				}
				
				if (rs.getBigDecimal("kredit") != null){
					bb.setKredit(rs.getBigDecimal("kredit"));
				} else {
					bb.setKredit(new BigDecimal(0));
				}
				lbb.add(bb);
			}
		} catch (Exception ex) {
			log.error(" getListJurnalDetailCetakan : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public User getBranch (String branch){
		List lbb = new ArrayList();
		User bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select branch_code,branch_name,branch_addr1 from sttm_branch"
				 +" where branch_code = ?";
			
			int i = 1;
			System.out.println("SQL getBranch :" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(i++, branch.trim());
			rs = stat.executeQuery();
			while(rs.next()){
				bb = new User();
				bb.setBranch_code(rs.getString("branch_code"));
				bb.setBranch_name(rs.getString("branch_name"));
				bb.setAddress(rs.getString("branch_addr1"));
				lbb.add(bb);
			}
		} catch (Exception ex) {
			log.error(" getBranch : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
		
	public User getUser (String user) {
		List lbb = new ArrayList();
		User bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql =" select a.user_id,a.user_name,b.branch_code|| ' - '||b.branch_name as cabang,"
				+" b.branch_addr1 || '' || b.branch_addr2 || '' || b.branch_addr3 as alamat_cab"
				+" from smtb_user a,sttm_branch b where A.HOME_BRANCH = b.branch_code and a.user_id = ?";
			
			int i = 1;
			System.out.println("SQL getUser :" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(i++, user.trim());
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new User();
				bb.setUser_id(rs.getString("user_id"));
				bb.setUsername(rs.getString("user_name"));
				bb.setBranch_code(rs.getString("cabang"));
//				bb.setBranch_name(rs.getString("branch_name"));
				bb.setAddress(rs.getString("alamat_cab"));
				lbb.add(bb);
			}
		} catch (Exception ex) {
			log.error(" getUser : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public List getListUserAktifCabang (String kode) {
		List lbb = new ArrayList();
		UserAktifCabang bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select /*+ parallel (a1,4)*/ kode_cabang,cabang,tanggal,user_id"
				 +"	from(select a.user_id as user_id,b.branch_name as cabang,"
                 +" substr(a.trn_ref_no,1,3) as kode_cabang,"
                 +" to_char (a.txn_dt_time, 'DD-MM-YYYY') as tanggal"
                 +" from actb_daily_log a, sttm_branch b"
                 +" where substr(a.trn_ref_no,1,3)=b.branch_code and a.user_id in"
                 +" (select d.user_id from smtb_user d, smtb_user_role c where c.user_id=d.user_id"
                 +" and c.role_id in ('TL_IN','TL_IN_JR','BO_IN','BO_IN_JR','BO_ALL_IN','TL_IN_JR_1','TL_IN_JR_2',"
                 +" 'TL_IN_JR_3','TL_IN_SEN_1','TL_IN_SEN_2','TL_IN_SEN_3','TL_IN_SEN_4','TL_IN_SEN_5','TL_IN_SEN_6',"
                 +" 'TL_IN_SEN_7','TL_IN_SEN_8')and a.user_id = c.user_id) and a.delete_stat <> 'D'"
                 +" and a.ac_no not in ('195002002','695001006','195001006')"
                 +" and a.user_id not in ('FLEXGW','FLEXSWITCH','SYSTEM')"
                 +" and a.auth_id not in ('FLEXGW','FLEXSWITCH','SYSTEM'))a1"
                 +"	where kode_cabang = ? group by user_id,kode_cabang,cabang,tanggal";
			
			int i = 1;
		System.out.println("sql getListUserAktifCabang:" + sql);
		stat = conn.prepareStatement(sql);
		stat.setString(i++, kode.trim());
		
		rs = stat.executeQuery();
		while (rs.next()) {
			bb = new UserAktifCabang();
			bb.setKode_cabang(rs.getString("kode_cabang"));
			bb.setCabang(rs.getString("cabang"));
			bb.setTanggal(rs.getString("tanggal"));
			bb.setUser_id(rs.getString("user_id"));
			System.out.println("bb :" + bb);
			lbb.add(bb);
		}
		} catch (SQLException ex) {
			log.error(" getListUserAktifCabang : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List GetListLapSpvTeller (String user){
		List lbb = new ArrayList();
		LaporanSPVTeller bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			jmlSaldoC = 0;
			jmlSaldoD = 0;
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = "select /*+ parallel (a1,12)*/ branch,tanggal,jam,function_desc,trn_code,ac_no,"
				+" trn_ref_no,no_warkat,drcr_ind,sum(decode(drcr_ind,'C',lcy_amount,lcy_amount)) as nominal,user_id,auth_id"
				+" from(select substr(a.trn_ref_no,1,3) as branch,a.trn_ref_no,a.trn_code,a.ac_no,"
                +" a.instrument_code as no_warkat,to_char(a.txn_dt_time,'DD-MM-YYYY') as tanggal,"
                +" to_char(a.txn_dt_time,'HH24:MI:SS') as jam,a.drcr_ind,a.lcy_amount,"
                +" a.user_id,a.auth_id,b.trn_desc as function_desc"
                +" from actb_daily_log a, sttm_trn_code b"
                +" where a.trn_code = b.trn_code and a.user_id in(select d.user_id"
                +" from smtb_user d,smtb_user_role c where c.user_id = d.user_id"
                +" and c.role_id in ('TL_IN','TL_IN_JR','BO_IN','BO_IN_JR','BO_ALL_IN','TL_IN_JR_1','TL_IN_JR_2',"
                +" 'TL_IN_JR_3','TL_IN_SEN_1','TL_IN_SEN_2','TL_IN_SEN_3','TL_IN_SEN_4','TL_IN_SEN_5','TL_IN_SEN_6',"
                +" 'TL_IN_SEN_7','TL_IN_SEN_8')and a.user_id = c.user_id) and a.delete_stat <> 'D'"
                +" and a.ac_no not in ('195002002','695001006','195001006')"
                +" and a.user_id not in ('FLEXGW','FLEXSWITCH','SYSTEM')"
                +" and a.auth_id not in ('FLEXGW','FLEXSWITCH','SYSTEM') and a.user_id <> a.auth_id) a1 where auth_id =? "
                +" group by branch,tanggal,jam,function_desc,trn_code,ac_no,trn_ref_no,"
                +" drcr_ind,no_warkat,user_id,auth_id order by jam";
			
			System.out.println("sql :" + sql);
        	int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, user.trim());
			
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new LaporanSPVTeller();
				bb.setBranch(rs.getString("branch"));
				bb.setTanggal(rs.getString("tanggal"));
				bb.setJam(rs.getString("jam"));
				bb.setFunction_desc(rs.getString("function_desc"));
				bb.setTrn_code(rs.getString("trn_code"));
				bb.setAc_no(rs.getString("ac_no"));
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setNo_warkat(rs.getString("no_warkat") == null ? "-" : rs.getString("no_warkat"));
				bb.setDrcr_ind(rs.getString("drcr_ind"));
				bb.setNominal(rs.getBigDecimal("nominal"));
				bb.setUser_id(rs.getString("user_id"));
				bb.setAuth_id(rs.getString("auth_id"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error("GetListLapSpvTeller : "+ ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public int GetTotLapSpvCs(String user){
		int tot = 0;
		LaporanSpvCs bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " select count (*) as total from (select branch.branch_code,branch.branch_name,rec.key_id,rec.tanggal,rec.jam,rec.function_id,rec.maker_id,"
					+ " rec.checker_id,rec.modifikasi_ke from sttm_branch branch join"
					+ " (select branch_code as branch,tanggal,jam_transaksi as jam,key_id,function_id,maker_id,checker_id, modifikasi_ke "
				    + " from (select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,substr(key_id,16,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('STDCIFI9','STDCIFN9','STDCIF') and auth_stat = 'A' and maker_id <>'SYSTEM'"
				    + " and checker_id <>'SYSTEM' group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,substr(key_id,23,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('STDCIFIS') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
				    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,substr(key_id,24,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('IADCUSAC','IADCUSTD') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
				    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,substr(key_id,22,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('STDSTMNT','CADCHBOO') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
				    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,substr(key_id,25,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('CADCHKDT') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
				    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,CHAR_FLD_3 as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('ICDLIQAC') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
				    + " group by checker_dt_stamp,branch_code, CHAR_FLD_3,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,CHAR_FLD_1 key_id,function_id,maker_id,checker_id,max(mod_no)  as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('CADAMBLK') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
				    + " group by checker_dt_stamp,branch_code,CHAR_FLD_1,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,substr(key_id,13,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in ('ISDinSTR') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
				    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,substr(key_id,18,14) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in('IVDTXNAS','IVDTXNIR','IVDTXNOR','IVDTXNRO','IVDTXNRI') and auth_stat = 'A'"
				    + " and maker_id <>'SYSTEM'and checker_id <>'SYSTEM' group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
				    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
				    + " branch_code,char_fld_2 as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
				    + " from sttb_record_log where function_id in('IADREDMN') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'" 
				    + " group by checker_dt_stamp,branch_code,char_fld_2,function_id,maker_id,checker_id))rec on branch.branch_code = rec.branch"
				    + " where rec.checker_id =? order by branch_code,checker_id,jam)";
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, user.trim());
			rs = stat.executeQuery();
			while (rs.next()){
				tot = rs.getInt("total");
			}
		} catch (Exception e) {
			log.error("GetTotLapSpvCs : "+ e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public List GetLapSpvCsCetak(String user){
		List lbb = new ArrayList();
		LaporanSpvCs bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " select branch.branch_code,branch.branch_name,rec.key_id,rec.tanggal,rec.jam,rec.function_id,rec.maker_id,"
				+ " rec.checker_id,rec.modifikasi_ke from sttm_branch branch join"
				+ " (select branch_code as branch,tanggal,jam_transaksi as jam,key_id,function_id,maker_id,checker_id, modifikasi_ke "
			    + " from (select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,16,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('STDCIFI9','STDCIFN9','STDCIF') and auth_stat = 'A' and maker_id <>'SYSTEM'"
			    + " and checker_id <>'SYSTEM' group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,23,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('STDCIFIS') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,24,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('IADCUSAC','IADCUSTD') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,22,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('STDSTMNT','CADCHBOO') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,25,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('CADCHKDT') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,CHAR_FLD_3 as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('ICDLIQAC') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code, CHAR_FLD_3,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,CHAR_FLD_1 key_id,function_id,maker_id,checker_id,max(mod_no)  as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('CADAMBLK') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,CHAR_FLD_1,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,13,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('ISDinSTR') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,18,14) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in('IVDTXNAS','IVDTXNIR','IVDTXNOR','IVDTXNRO','IVDTXNRI') and auth_stat = 'A'"
			    + " and maker_id <>'SYSTEM'and checker_id <>'SYSTEM' group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,char_fld_2 as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in('IADREDMN') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'" 
			    + " group by checker_dt_stamp,branch_code,char_fld_2,function_id,maker_id,checker_id))rec on branch.branch_code = rec.branch"
			    + " where rec.checker_id =? order by jam";
			
/*				if (!"".equals(user) && user!=null) {
					if (!"".equals(branch) && branch!=null){
						sql += " checker_id = ? and branch_code = ? ";
					}
					else {
						sql += " checker_id = ? ";
					}	
				}
				else  {
					if (!"".equals(branch) && branch!=null){
						sql += " branch_code = ? ";
					}
				}
*/
				
//				sql += " order by branch_code,checker_id,jam";
				
				int i = 1;
				System.out.println("sql GetListLapSpvCSCetak: " +sql);
				
				
				stat = conn.prepareStatement(sql);
//				if (!"".equals(user) && user!=null) {
					/*if (!"".equals(branch) && branch!=null){
						stat.setString(i++, user.trim());
						stat.setString(i++, branch.trim());
					}
					else {*/
				stat.setString(i++, user.trim());
					//}
//				}
				/*else {
					stat.setString(i++, branch.trim());
				} */
			
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new LaporanSpvCs();
				bb.setBranch(rs.getString("branch_code"));
				bb.setBranch_name(rs.getString("branch_name"));
				bb.setTanggal(rs.getString("tanggal"));
				bb.setJam(rs.getString("jam"));
				bb.setKey_id(rs.getString("key_id"));
				bb.setMaker_id(rs.getString("maker_id"));
				bb.setChecker_id(rs.getString("checker_id"));
				bb.setFunction_id(rs.getString("function_id"));
				bb.setModifikasi_ke(rs.getString("modifikasi_ke"));
				lbb.add(bb);
			}
		}catch (SQLException ex){
			log.error("GetLapSpvCsCetak: " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List GetLapSpvCs(String user, int begin, int delta){
		List lbb = new ArrayList();
		LaporanSpvCs bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " select no,branch_code,branch_name,key_id,tanggal,jam,function_id,maker_id,checker_id,modifikasi_ke from "
				+ " (select rownum as no,branch.branch_code,branch.branch_name,rec.key_id,rec.tanggal,rec.jam,rec.function_id,rec.maker_id,"
				+ " rec.checker_id,rec.modifikasi_ke from sttm_branch branch join"
				+ " (select branch_code as branch,tanggal,jam_transaksi as jam,key_id,function_id,maker_id,checker_id, modifikasi_ke "
			    + " from (select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,16,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('STDCIFI9','STDCIFN9','STDCIF') and auth_stat = 'A' and maker_id <>'SYSTEM'"
			    + " and checker_id <>'SYSTEM' group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,23,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('STDCIFIS') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,24,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('IADCUSAC','IADCUSTD') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,22,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('STDSTMNT','CADCHBOO') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,25,10) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('CADCHKDT') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,CHAR_FLD_3 as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('ICDLIQAC') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code, CHAR_FLD_3,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,CHAR_FLD_1 key_id,function_id,maker_id,checker_id,max(mod_no)  as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('CADAMBLK') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,CHAR_FLD_1,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,13,9) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in ('ISDinSTR') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'"
			    + " group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,substr(key_id,18,14) as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in('IVDTXNAS','IVDTXNIR','IVDTXNOR','IVDTXNRO','IVDTXNRI') and auth_stat = 'A'"
			    + " and maker_id <>'SYSTEM'and checker_id <>'SYSTEM' group by checker_dt_stamp,branch_code,key_id,function_id,maker_id,checker_id"
			    + " union all select to_char(checker_dt_stamp,'DD-MM-YYYY') as tanggal,to_char(checker_dt_stamp,'HH24:MI:SS') as jam_transaksi,"
			    + " branch_code,char_fld_2 as key_id,function_id,maker_id,checker_id,max(mod_no) as modifikasi_ke"
			    + " from sttb_record_log where function_id in('IADREDMN') and auth_stat = 'A' and maker_id <>'SYSTEM' and checker_id <>'SYSTEM'" 
			    + " group by checker_dt_stamp,branch_code,char_fld_2,function_id,maker_id,checker_id))rec on branch.branch_code = rec.branch"
			    + " where rec.checker_id =?) where no >= ? and no <= ? order by jam";
			
/*				if (!"".equals(user) && user!=null) {
					if (!"".equals(branch) && branch!=null){
						sql += " checker_id = ? and branch_code = ? ";
					}
					else {
						sql += " checker_id = ? ";
					}	
				}
				else  {
					if (!"".equals(branch) && branch!=null){
						sql += " branch_code = ? ";
					}
				}
*/
				
//				sql += " order by branch_code,checker_id,jam";
				
				int i = 1;
				System.out.println("sql GetListLapSpvCS1: " +sql);
				
				
				stat = conn.prepareStatement(sql);
//				if (!"".equals(user) && user!=null) {
					/*if (!"".equals(branch) && branch!=null){
						stat.setString(i++, user.trim());
						stat.setString(i++, branch.trim());
					}
					else {*/
				stat.setString(i++, user.trim());
				stat.setInt(i++, begin);
				stat.setInt(i++, delta);
					//}
//				}
				/*else {
					stat.setString(i++, branch.trim());
				} */
			
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new LaporanSpvCs();
				bb.setBranch(rs.getString("branch_code"));
				bb.setBranch_name(rs.getString("branch_name"));
				bb.setTanggal(rs.getString("tanggal"));
				bb.setJam(rs.getString("jam"));
				bb.setKey_id(rs.getString("key_id"));
				bb.setMaker_id(rs.getString("maker_id"));
				bb.setChecker_id(rs.getString("checker_id"));
				bb.setFunction_id(rs.getString("function_id"));
				bb.setModifikasi_ke(rs.getString("modifikasi_ke"));
				lbb.add(bb);
			}
		}catch (SQLException ex){
			log.error("GetLapSpvCs: " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List GetcifIndividual(){
		List lbb = new ArrayList();
		cifIndividual bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		System.out.println("masuk sql");
		try{
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = " select profile_rule_group_no,profile_rule_group_name,profile_rule_no,profile_rule_desc"
					+ " from cif_individual";
			
			System.out.println("getCifIndividual" + sql);
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new cifIndividual();
				bb.setProfile_rule_group_no(rs.getString("profile_rule_group_no"));
				bb.setProfile_rule_group_name(rs.getString("profile_rule_group_name"));
				bb.setProfile_rule_no(rs.getString("profile_rule_no"));
				bb.setProfile_rule_desc(rs.getString("profile_rule_desc"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error("getcifIndividual :" + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getcifNonIndividual(){
		List lbb = new ArrayList();
		cifNonIndividual bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		System.out.println("masuk sql");
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = " select profile_rule_group_no,profile_rule_group_name,profile_rule_no,profile_rule_desc"
					+ " from cif_non_individual";
			
			System.out.println("getcifNonIndividual :" + sql);
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new cifNonIndividual();
				bb.setProfile_rule_group_no(rs.getString("profile_rule_group_no"));
				bb.setProfile_rule_group_name(rs.getString("profile_rule_group_name"));
				bb.setProfile_rule_no(rs.getString("profile_rule_no"));
				bb.setProfile_rule_desc(rs.getString("profile_rule_desc"));
				lbb.add(bb);
			}
		} catch (Exception ex) {
			log.error("getcifNonIndividual :" + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	} 
	
	public int getTotalSaldo (String acc, String nama){
		int tot = 0;
		Connection conn = null;
		SaldoRataRata saldo = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();
			
			sql = " select count(*) as total from (select branch_code,branch_name,acc_no,acc_desc,cust_no,currency,product_code,opening_date"
					+ " from BMIDWH.DIM_CUST_ACCOUNT where";
			
					if (!"".equals(acc) && acc != null){
						sql += " acc_no = ? " ;
					} if (!"".equals(nama) && nama != null){
						sql += " acc_desc like ? ";
					}
					
					sql += " and dw_row_ind = 'Y' and product_code not in ('S18A','S18B','S90B')) hasil";
					System.out.println("sql getSaldoRataRata:" + sql);
					
				int i = 1;
				stat = conn.prepareStatement(sql);
				if (!"".equals(acc) && acc !=null){
					stat.setString(i++, acc.trim());
				} if (!"".equals(nama) && nama !=null){
					stat.setString(i++, "%" + nama.toUpperCase() + "%" );
				}
				rs = stat.executeQuery();
				if (rs.next()){
					tot = rs.getInt("total");
					System.out.println("total:" + tot);
				}
			
		} catch (Exception e) {
			log.error("getTotalSaldo :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public List getListSaldoRata (String acc, String nama, int begin){
		List lbb = new ArrayList();
		Connection conn = null;
		SaldoRataRata saldo = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();
			
			sql = "select branch_code,branch_name,acc_no,acc_desc,cust_no,currency,product_code,opening_date"
					+ " from BMIDWH.DIM_CUST_ACCOUNT where ";
			
					if (!"".equals(acc) && acc != null){
						sql += " acc_no = ? " ;
					} if (!"".equals(nama) && nama != null){
						sql += " acc_desc like ? ";
					}
					
			sql += " and dw_row_ind = 'Y' and product_code not in ('S18A','S18B','S90B')"
					+ " order by branch_code limit  offset ?";
			
			System.out.println("List Saldo Rata2:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if (!"".equals(acc) && acc != null){
				stat.setString(i++, acc.trim());
			} if (!"".equals(nama) && nama != null){
				stat.setString(i++, "%" + nama.toUpperCase() + "%" );
			}
			stat.setInt(i++, begin);
			rs = stat.executeQuery();
			while (rs.next()){
				saldo = new SaldoRataRata();
				saldo.setBranch(rs.getString("branch_code"));
				saldo.setBranch_name(rs.getString("branch_name"));
				saldo.setAcc_no(rs.getString("acc_no"));
				saldo.setAcc_desc(rs.getString("acc_desc"));
				saldo.setCust_no(rs.getString("cust_no"));
				saldo.setCcy(rs.getString("currency"));
				saldo.setProduct_code(rs.getString("product_code"));
				System.out.println("product :" + saldo.getProduct_code());
				saldo.setOpening_date(rs.getString("opening_date"));
				lbb.add(saldo);
			}
		} catch (Exception e) {
			log.error("getListSaldoRata :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getSaldoRatarata(String acc, String nama, int begin){
		List lbb = new ArrayList();
		Connection conn = null;
		SaldoRataRata saldo = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();
			
			sql = "select branch_code,branch_name,"
				+ " acc_no,acc_desc,cust_no,currency,product_code,opening_date from BMIDWH.DIM_CUST_ACCOUNT where";
				if (!"".equals(acc) && acc != null){
					sql += " acc_no = ? " ;
				} if (!"".equals(nama) && nama != null){
					sql += " acc_desc like ? ";
				}
				sql += " and dw_row_ind = 'Y' and product_code not in ('S18A','S18B')"
						+ " limit 100 offset ?";
				System.out.println("sql getSaldoRataRata:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if (!"".equals(acc) && acc !=null){
				stat.setString(i++, acc.trim());
				System.out.println("nama:" + nama);
			} if (!"".equals(nama) && nama !=null){
				stat.setString(i++, "%" + nama.toUpperCase() + "%" );
			}
			stat.setInt(i++, begin);
			System.out.println("nama:" + nama);
			rs = stat.executeQuery();
			while(rs.next()){
				saldo = new SaldoRataRata();
				saldo.setBranch(rs.getString("branch_code"));
				saldo.setBranch_name(rs.getString("branch_name"));
				saldo.setAcc_no(rs.getString("acc_no"));
				saldo.setAcc_desc(rs.getString("acc_desc"));
				saldo.setCust_no(rs.getString("cust_no"));
				saldo.setCcy(rs.getString("currency"));
				saldo.setProduct_code(rs.getString("product_code"));
				saldo.setOpening_date(rs.getString("opening_date"));
				lbb.add(saldo);
			}
		} catch (Exception e) {
			log.error("getSaldoRatarata :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public int getTotSaldoDetail (String acc, String prd1, String prd2){
		int tot = 1;
		List detail = new ArrayList();
		SaldoRataRata saldoDetail = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();
			
			sql = "select count(*) as total from (select branch_code,branch_name,acc_no,acc_desc,period_code,ccy,lcy_bal,acy_bal,cust_no"
				+" from(select avgbal.BRANCH_CODE,cust.BRANCH_NAME,avgbal.GL_CODE,cust.ACC_DESC,"
				+" substr(avgbal.FIN_YEAR,3,6) || substr(avgbal.PERIOD_CODE,2,3) as period_code,"
				+" avgbal.CCY,avgbal.LCY_BAL,avgbal.ACY_BAL,cust.ACC_NO,cust.cust_no"
				+" from BMIDWH.GLTB_AVGBAL avgbal join BMIDWH.DIM_CUST_ACCOUNT cust"
				+" on avgbal.BRANCH_CODE = cust.BRANCH_CODE and avgbal.GL_CODE = cust.ACC_NO"
				+" group by avgbal.BRANCH_CODE,cust.BRANCH_NAME,avgbal.GL_CODE,cust.ACC_DESC, avgbal.FIN_YEAR,"
				+" avgbal.PERIOD_CODE,avgbal.CCY,avgbal.LCY_BAL,avgbal.ACY_BAL,cust.ACC_NO,cust.cust_no) a"
				+" where acc_no = ? and period_code >= ? and period_code <= ? order by period_code) hasil";
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, acc);
			stat.setString(i++, prd1);
			stat.setString(i++, prd2);
			rs = stat.executeQuery();
			
			while(rs.next()){
				tot = rs.getInt("total");
			}
			
		}catch (Exception e) {
			log.error("getTotSaldoDetail :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public List getSaldoDetail(String acc, String prd1, String prd2, int begin) {
		List detail = new ArrayList();
		SaldoRataRata saldoDetail = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();
			
			sql = "select branch_code,branch_name,acc_no,acc_desc,product_code,period_code,ccy,lcy_bal,acy_bal,cust_no"
				+" from(select avgbal.BRANCH_CODE,cust.BRANCH_NAME,avgbal.GL_CODE,cust.ACC_DESC,"
				+" substr(avgbal.FIN_YEAR,3,6) || substr(avgbal.PERIOD_CODE,2,3) as period_code,"
				+" avgbal.CCY,avgbal.LCY_BAL,avgbal.ACY_BAL,cust.ACC_NO,cust.cust_no,cust.product_code,cust.dw_row_ind"
				+" from BMIDWH.GLTB_AVGBAL avgbal join BMIDWH.DIM_CUST_ACCOUNT cust"
				+" on avgbal.BRANCH_CODE = cust.BRANCH_CODE and avgbal.GL_CODE = cust.ACC_NO)a"
				+" where acc_no = ? and product_code not in ('S18A','S18B') and dw_row_ind = 'Y' and period_code >= ? and period_code <= ? "
				+" order by period_code limit 120 offset ?";
			
			System.out.println("sql GetSaldoDetail :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, acc.trim());
			stat.setString(i++, prd1.trim());
			stat.setString(i++, prd2.trim());
			stat.setInt(i++, begin);
			System.out.println("begin :" + begin);
			rs = stat.executeQuery();
			while(rs.next()){
				saldoDetail = new SaldoRataRata();
				saldoDetail.setBranch(rs.getString("branch_code"));
				saldoDetail.setBranch_name(rs.getString("branch_name"));
				saldoDetail.setAcc_no(rs.getString("acc_no"));
				saldoDetail.setAcc_desc(rs.getString("acc_desc"));
				saldoDetail.setPeriod(rs.getString("period_code"));
				saldoDetail.setCcy(rs.getString("ccy"));
				saldoDetail.setLcy_bal(rs.getBigDecimal("lcy_bal"));
				saldoDetail.setAcy_bal(rs.getBigDecimal("acy_bal"));
				saldoDetail.setCust_no(rs.getString("cust_no"));
				saldoDetail.setProduct_code(rs.getString("product_code"));
				detail.add(saldoDetail);
			}
		} catch (Exception e) {
			log.error("getSaldoDetail :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return detail;
	}
	
	public int getTotLapPerubahanCif (String cif, String branch){
		int tot = 0;
		List lhistCif = new ArrayList();
		LapPerubahanDataCif lapDataCif = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select count (*) as total from (select cif.cif,cif.branch_code,cust.customer_name1 as nama_nasabah,cif.fastpath,cif.data_lama,cif.data_baru,"
				+" cif.modif_ke,cif.keterangan,cif.maker_date,cif.maker_time,cif.maker_id,cif.checker_id from(select a.cif,b.branch_code,a.fastpath,"
				+" a.data_lama,a.data_baru,a.modif_ke,a.keterangan,b.maker_date,b.maker_time,b.maker_id,b.checker_id from(select cif,fastpath,"
				+" data_lama,data_baru,modif_ke,keterangan from(select substr(key_id,16,9) as cif,function_id as fastpath,OLD_VALUE as DATA_LAMA,"
				+" NEW_VALUE as DATA_BARU,MOD_NO as MODIF_KE,FIELD_NAME as KETERANGAN from sttb_field_log"
				+" where function_id in('STDCIFI9','STDCIFN9','STDCIF') and MOD_NO <>'1' AND field_name NOT IN ('MOD_NO','CHECKER_ID',"
				+" 'MAKER_ID','CHECKER_DT_STAMP','AUTH_STAT','ACC','BRANCH','BRANCH_CODE','BRN','CCY','CHAR_FIELD1'))) a "
				+" join(select cif,branch_code,fastpath,maker_date,maker_time,checker_id,maker_id,mod_no from(select substr(key_id,16,9) as cif,branch_code,"
				+" to_char(maker_dt_stamp,'dd-mm-rrrr') as maker_date,to_char(maker_dt_stamp,'HH24:MI:SS') as maker_time,maker_id,checker_id, "
				+" function_id as fastpath,mod_no from sttb_record_log where function_id in('STDCIFI9','STDCIFN9','STDCIF') and MOD_NO <>'1' and auth_stat = 'A'))"
				+" b on a.cif = b.cif and a.modif_ke = b.mod_no ) cif join sttm_customer cust on cif.cif = cust.customer_no where";
			
			if (!"".equals(cif) && cif !=null){
				sql += " cif.cif = ?";
			} if (!"".equals(branch) && branch !=null){
				sql += " cif.branch_code = ?";
			}
			
			sql += " )";
			
			System.out.println("sql count cif " + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			
			if (!"".equals(cif) && cif !=null){
				stat.setString(i++, cif);
			}if (!"".equals(branch) && branch !=null){
				stat.setString(i++, branch);
			}
			
			rs = stat.executeQuery();
			if (rs.next()){
				tot = rs.getInt("total");
			}
		} catch (Exception e) {
			log.error("getTotLapPerubahanCif" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public int getTotLapPerubahanNorek (String branch, String acc){
		int tot = 0;
		List lhistNorek = new ArrayList();
		LapPerubahanDataCif lapDataRek = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = "select count (*) as count from (select rek.rek,rek.branch_code,cust.ac_desc AS nama_nasabah,rek.fastpath,rek.data_lama,rek.data_baru,rek.modif_ke,"
				+" rek.keterangan,rek.maker_date,rek.maker_time,rek.maker_id,rek.checker_id from(select a.rek,b.branch_code,a.fastpath,"
				+" a.data_lama,a.data_baru,a.modif_ke,a.keterangan,b.maker_date,b.maker_time,b.maker_id,b.checker_id from(select rek,fastpath,"
				+" data_lama,data_baru,modif_ke,keterangan from(select substr(key_id,24,10) as rek,function_id as fastpath,OLD_VALUE as DATA_LAMA,"
				+" NEW_VALUE as DATA_BARU,MOD_NO as MODIF_KE,FIELD_NAME as KETERANGAN from sttb_field_log"
				+" where MOD_NO <>'1' AND field_name NOT IN ('MOD_NO','CHECKER_ID',"
				+" 'MAKER_ID','CHECKER_DT_STAMP','AUTH_STAT','ACC','BRANCH','BRANCH_CODE','BRN','CCY','CHAR_FIELD1'))) a "
				+" join(select rek,branch_code,fastpath,maker_date,maker_time,checker_id,maker_id,mod_no from(select substr(key_id,24,10) as rek,branch_code,"
				+" to_char(maker_dt_stamp,'dd-mm-rrrr') as maker_date,to_char(maker_dt_stamp,'HH24:MI:SS') as maker_time,maker_id,checker_id, "
				+" function_id as fastpath,mod_no from sttb_record_log where MOD_NO <>'1' and auth_stat = 'A'))"
				+" b on a.rek = b.rek and a.modif_ke = b.mod_no) rek join sttm_cust_account cust on rek.rek = cust.cust_ac_no where ";
			
			if (!"".equals(branch) && branch != null){
				sql += "rek.branch_code = ?";
			} if (!"".equals(acc) && acc !=null){
				sql += "rek.rek = ?";
			}
			
			sql += " )";
			
			System.out.println("SQL COUNT LapPerubahanDataNorek -->" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			
			if (!"".equals(branch) && branch !=null){
				stat.setString(i++, branch);
			} if (!"".equals(acc) && acc !=null){
				stat.setString(i++, acc);
			}
			
			rs = stat.executeQuery();
			if (rs.next()){
				tot = rs.getInt("count");
			}
		} catch (Exception e) {
			log.error("getTotLapPerubahanNorek" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public List getLapPerubahanCif (String noCif, String branch, int begin, int delta){
		List lhistLapCif = new ArrayList();
		LapPerubahanDataCif lapDataCif = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select cif,branch_code,nama_nasabah,fastpath,data_lama,data_baru,modif_ke,keterangan,maker_date,maker_id,maker_time,checker_id from("
				+" select rownum as no,cif.cif,cif.branch_code,cust.customer_name1 as nama_nasabah,cif.fastpath,cif.data_lama,cif.data_baru,cif.modif_ke,"
				+" cif.keterangan,cif.maker_date,cif.maker_time,cif.maker_id,cif.checker_id from(select a.cif,b.branch_code,a.fastpath,"
				+" a.data_lama,a.data_baru,a.modif_ke,a.keterangan,b.maker_date,b.maker_time,b.maker_id,b.checker_id from(select cif,fastpath,"
				+" data_lama,data_baru,modif_ke,keterangan from(select substr(key_id,16,9) as cif,function_id as fastpath,OLD_VALUE as DATA_LAMA,"
				+" NEW_VALUE as DATA_BARU,MOD_NO as MODIF_KE,FIELD_NAME as KETERANGAN from sttb_field_log"
				+" where function_id in('STDCIFI9','STDCIFN9','STDCIF') and MOD_NO <>'1' AND field_name NOT IN ('MOD_NO','CHECKER_ID',"
				+" 'MAKER_ID','CHECKER_DT_STAMP','AUTH_STAT','ACC','BRANCH','BRANCH_CODE','BRN','CCY','CHAR_FIELD1'))) a "
				+" join(select cif,branch_code,fastpath,maker_date,maker_time,checker_id,maker_id,mod_no from(select substr(key_id,16,9) as cif,branch_code,"
				+" to_char(maker_dt_stamp,'dd-mm-rrrr') as maker_date,to_char(maker_dt_stamp,'HH24:MI:SS') as maker_time,maker_id,checker_id, "
				+" function_id as fastpath,mod_no from sttb_record_log where function_id in('STDCIFI9','STDCIFN9','STDCIF') and MOD_NO <>'1' and auth_stat = 'A'))"
				+" b on a.cif = b.cif and a.modif_ke = b.mod_no) cif join sttm_customer cust on cif.cif = cust.customer_no where";
				
				if (!"".equals(noCif) && noCif !=null){
					sql += " cif.cif = ?";
				}if (!"".equals(branch) && branch !=null){
					sql += " cif.branch_code = ?";
				}
				
				sql += " )where no >= ? and no <= ? order by maker_time";
			
			System.out.println("SQL LapPerubahanDataCif -->" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			
			if (!"".equals(noCif) && noCif !=null){
				stat.setString(i++, noCif);
			}if (!"".equals(branch) && branch !=null){
				stat.setString(i++, branch);
			}
			
			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			rs = stat.executeQuery();
			while (rs.next()){
				lapDataCif = new LapPerubahanDataCif();
				lapDataCif.setNoCif(rs.getString("cif"));
				lapDataCif.setBranch(rs.getString("branch_code"));
				lapDataCif.setNama_nasabah(rs.getString("nama_nasabah"));
				lapDataCif.setFastpath(rs.getString("fastpath"));
				lapDataCif.setData_lama(rs.getString("data_lama"));
				lapDataCif.setData_baru(rs.getString("data_baru"));
				lapDataCif.setModif_ke(rs.getString("modif_ke"));
				lapDataCif.setKeterangan(rs.getString("keterangan"));
				lapDataCif.setMaker_date(rs.getString("maker_date"));
				lapDataCif.setMaker_time(rs.getString("maker_time"));
				lapDataCif.setMaker_id(rs.getString("maker_id"));
				lapDataCif.setChecker_id(rs.getString("checker_id"));
				lhistLapCif.add(lapDataCif);
			}
		} catch (Exception e) {
			log.error("getLapPerubahanDataCif :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lhistLapCif;
	}
	
	public int getCountUser (String userRef){
		int tot = 0;
		List lbb = new ArrayList();
		Connection conn = null;
		cariTransaksiUserRef transaksi = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select count (*) as count from detb_jrnl_txn_detail where user_ref_no = ?";
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, userRef);
			rs = stat.executeQuery();
			System.out.println("sql count user :" + sql);
			if (rs.next()){
				tot = rs.getInt("count");
			}
		} catch (Exception e) {
			log.error("getCountUser" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public List getSearchUser (String userRef){
		
		List lbb = new ArrayList();
		Connection conn = null;
		cariTransaksiUserRef transaksi = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select a.trn_ref_no,b.user_ref_no,to_char(a.trn_dt,'dd-mm-yyyy') as trn_dt,b.branch_code,b.serial_no,"
					+" a.ac_no,a.trn_code,b.ccy,b.dr_cr,b.addl_text as description,b.exch_rate,b.amount from actb_history a, detb_jrnl_txn_detail b"
					+" where a.trn_ref_no = b.reference_no and a.ac_no = b.ac_gl_no and a.trn_code = b.txn_code and b.user_ref_no = ? "
					+" group by a.trn_ref_no,b.user_ref_no,a.trn_dt,b.branch_code,b.serial_no,a.ac_no,a.trn_code,b.ccy,b.dr_cr,b.addl_text,b.exch_rate,b.amount"
					+" order by a.trn_dt";
			
			int i = 1;
			
			System.out.println("sql getSearch User" + sql);
			
			stat = conn.prepareStatement(sql);
			stat.setString(i++, userRef);
			
			rs = stat.executeQuery();
			while (rs.next()) {
				transaksi = new cariTransaksiUserRef();
				transaksi.setAcNo(rs.getString("ac_no"));
				transaksi.setDesc(rs.getString("description"));
				transaksi.setAmount(rs.getBigDecimal("amount"));
				transaksi.setBranch(rs.getString("branch_code"));
				transaksi.setCcy(rs.getString("ccy"));
				transaksi.setDrcr(rs.getString("dr_cr"));
				transaksi.setRate(rs.getInt("exch_rate"));
				transaksi.setRefNo(rs.getString("trn_ref_no"));
				transaksi.setSerialNo(rs.getInt("serial_no"));
				transaksi.setTxnCode(rs.getString("trn_code"));
				transaksi.setUserRefNo(rs.getString("user_ref_no"));
				transaksi.setTrnDate(rs.getString("trn_dt"));
				lbb.add(transaksi);
			}
			
		} catch (Exception e) {
			log.error("getTransaksiUserRef :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
		
	}
	
	public List getLapPerubahanRek(String branch, String acc, int begin, int delta){
		List lbb = new ArrayList();
		Connection conn = null;
		LapPerubahanDataCif lapRek = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " select no,rek,branch_code,nama_nasabah,fastpath,data_lama,data_baru,modif_ke,keterangan,maker_date,maker_time,maker_id,checker_id from ("
				+ "select rownum as no,rek.rek,rek.branch_code,cust.ac_desc AS nama_nasabah,rek.fastpath,rek.data_lama,rek.data_baru,rek.modif_ke,"
				+" rek.keterangan,rek.maker_date,rek.maker_time,rek.maker_id,rek.checker_id from(select a.rek,b.branch_code,a.fastpath,"
				+" a.data_lama,a.data_baru,a.modif_ke,a.keterangan,b.maker_date,b.maker_time,b.maker_id,b.checker_id from(select rek,fastpath,"
				+" data_lama,data_baru,modif_ke,keterangan from(select substr(key_id,24,10) as rek,function_id as fastpath,OLD_VALUE as DATA_LAMA,"
				+" NEW_VALUE as DATA_BARU,MOD_NO as MODIF_KE,FIELD_NAME as KETERANGAN from sttb_field_log"
				+" where MOD_NO <>'1' AND field_name NOT IN ('MOD_NO','CHECKER_ID',"
				+" 'MAKER_ID','CHECKER_DT_STAMP','AUTH_STAT','ACC','BRANCH','BRANCH_CODE','BRN','CCY','CHAR_FIELD1'))) a "
				+" join(select rek,branch_code,fastpath,maker_date,maker_time,checker_id,maker_id,mod_no from(select substr(key_id,24,10) as rek,branch_code,"
				+" to_char(maker_dt_stamp,'dd-mm-rrrr') as maker_date,to_char(maker_dt_stamp,'HH24:MI:SS') as maker_time,maker_id,checker_id, "
				+" function_id as fastpath,mod_no from sttb_record_log where MOD_NO <>'1' and auth_stat = 'A'))"
				+" b on a.rek = b.rek and a.modif_ke = b.mod_no) rek join sttm_cust_account cust on rek.rek = cust.cust_ac_no where";
			
			if (!"".equals(branch) && branch !=null){
				sql += " rek.branch_code = ?";
			} if (!"".equals(acc) && acc !=null){
				sql += " rek.rek = ?";
			}
			
			sql += " )where no >= ? and no <= ? order by maker_time";
			
			System.out.println("SQL getLapPerubahanRek -->" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			
			if (!"".equals(branch) && branch !=null){
				stat.setString(i++, branch);
			} if (!"".equals(acc) && acc !=null){
				stat.setString(i++, acc);
			}
			
			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			System.out.println("NoRek -->" + branch);
			rs = stat.executeQuery();
			while (rs.next()){
				lapRek = new LapPerubahanDataCif();
				lapRek.setNo_rek(rs.getString("rek"));
				lapRek.setBranch(rs.getString("branch_code"));
				lapRek.setNama_nasabah(rs.getString("nama_nasabah"));
				lapRek.setFastpath(rs.getString("fastpath"));
				lapRek.setData_lama(rs.getString("data_lama"));
				lapRek.setData_baru(rs.getString("data_baru"));
				lapRek.setModif_ke(rs.getString("modif_ke"));
				lapRek.setKeterangan(rs.getString("keterangan"));
				lapRek.setMaker_date(rs.getString("maker_date"));
				lapRek.setMaker_time(rs.getString("maker_time"));
				lapRek.setMaker_id(rs.getString("maker_id"));
				lapRek.setChecker_id(rs.getString("checker_id"));
				lbb.add(lapRek);
			}
		} catch (Exception e) {
			log.error("getLapPerubahanRek :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public int getTotListNik (String nik){
		int tot = 0;
		List arrNIK = new ArrayList();
		Nik bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = "select count (*) as total from (select a.user_id AS nik, a.user_name user_name, a.home_branch AS home_branch, b.branch_name AS branch_name "
				+ "from SMTB_USER a, sttm_branch b where a.home_branch=b.branch_code and a.record_stat='O' and user_status in('E','D') and user_id like ?)";
			
			
			System.out.println("count user :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, "%" + nik.toUpperCase() + "%");
			rs = stat.executeQuery();
			
			while (rs.next()){
				tot = rs.getInt("total");
			}
			
		} catch (Exception e) {
			log.error("getTotListNik :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}
	
	public List getListNIK(String nik) {
		List arrNIK = new ArrayList();
		Nik bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select a.user_id AS nik, a.user_name user_name, a.home_branch AS home_branch, b.branch_name AS branch_name "
					+ "from SMTB_USER a, sttm_branch b where a.home_branch=b.branch_code and a.record_stat='O' and user_status in('E','D') and user_id like ?"; 
			int i = 1;
			System.out.println("query SQL: "+sql);
			stat = conn.prepareStatement(sql);
			stat.setString(i++, "%" + nik.toUpperCase() + "%");
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Nik();
				bb.setNik(rs.getString("nik"));
				System.out.println("nik :" + rs.getString("nik"));
				bb.setBranch(rs.getString("home_branch"));
				bb.setNama(rs.getString("user_name"));
				bb.setBranch_name(rs.getString("branch_name"));
				arrNIK.add(bb);
			}
		} catch (SQLException ex) {
			log.error("Kesalahan getListNIK : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return arrNIK;
	}
	

}
