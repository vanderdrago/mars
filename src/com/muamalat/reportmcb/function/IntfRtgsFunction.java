package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.bean.IntfRtgsOutBean;
import com.muamalat.reportmcb.entity.IntfRtgsOut;
import com.muamalat.reportmcb.entity.RtgsTtpn;
import com.muamalat.singleton.DatasourceEntry;

public class IntfRtgsFunction {
	private static Logger log = Logger.getLogger(IntfRtgsFunction.class);
	
	public void closeConnDb(Connection conn, PreparedStatement stat,
			ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public List getListIntfRtgsOut(IntfRtgsOutBean intfOut, int a, int b) {
		List lbb = new ArrayList();
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
			sql = "SELECT row, dataId, BOR, ValueDate, RTStatus, Status, RsnCde, ToMember, TRN, RelTRN, OriName, " +
					"UltimateBeneName, UltimateBeneAcc, Amount, FromAccName, ToAccName, PayDetails, SettleTime " +
					"FROM ( SELECT ROW_NUMBER() OVER(ORDER BY dataId asc) AS Row, dataId, BOR, ValueDate, RTStatus, Status, RsnCde, ToMember, TRN, RelTRN," +
			"OriName, UltimateBeneName, UltimateBeneAcc, Amount, FromAccName, ToAccName, PayDetails, SettleTime " +
			"FROM ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " dbo.T_RTIFTSOUT_HIS ";
			} else {
				sql += " dbo.T_RTIFTSOUT ";
			}
			sql += " where ValueDate = ? and";
			if ("".equals(intfOut.getKdcab()) && "".equals(intfOut.getAcc()) && "".equals(intfOut.getTxnsts()) && "".equals(intfOut.getTrn()) 
					&& "".equals(intfOut.getReltrn()) && "".equals(intfOut.getBsnsdt()) && "".equals(intfOut.getTmmbr()) && "".equals(intfOut.getAmtto()) 
					&& "".equals(intfOut.getAmtf())){	
			} else {
				if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
					sql += " RelTRN like '"+intfOut.getKdcab()+"%' and";
				}
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					sql += " UltimateBeneAcc = ? and";
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					sql += " Status = ? and";
				}
				if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
					sql += " TRN = ? and";
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					sql += " reltrn = ? and";
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					sql += " ToMember = ? and";
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					sql += " (CAST(Amount AS NUMERIC(20, 2)) / 100) >= ? and";
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					sql += "(CAST(Amount AS NUMERIC(20, 2)) / 100) <= ? and";
				}
			}
			sql = sql.substring(0, sql.length() - 3);
			sql += ")asdf where (Row BETWEEN ? AND ? );";
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setString(i++, intfOut.getBsnsdt());
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				stat.setString(i++, intfOut.getAcc());
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				stat.setString(i++, intfOut.getTxnsts());
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				stat.setString(i++, intfOut.getTrn());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				stat.setString(i++, intfOut.getTmmbr());
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
			}
			stat.setInt(i++, a);
			stat.setInt(i++, b);
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setDataId(rs.getString("dataId"));
				rtgsout.setBor(rs.getString("BOR"));
				rtgsout.setValuedate(rs.getString("ValueDate"));
				rtgsout.setRtstatus(rs.getString("RTStatus"));
				rtgsout.setStatus(rs.getString("Status"));
				rtgsout.setRsncde(rs.getString("RsnCde"));
				rtgsout.setTomember(rs.getString("ToMember"));
				rtgsout.setTrn(rs.getString("TRN"));
				rtgsout.setReltrn(rs.getString("RelTRN"));
				rtgsout.setOriname(rs.getString("OriName"));
				rtgsout.setUltimatebenename(rs.getString("UltimateBeneName"));
				rtgsout.setUltimatebeneacc(rs.getString("UltimateBeneAcc"));
				rtgsout.setAmount((new BigDecimal(rs.getString("Amount"))).divide(new BigDecimal(100)));
				rtgsout.setFromaccname(rs.getString("FromAccName"));
				rtgsout.setToaccname(rs.getString("ToAccName"));
				rtgsout.setPaydetails(rs.getString("PayDetails"));
				rtgsout.setSettletime(rs.getString("SettleTime"));
				lbb.add(rtgsout);
			}
		} catch (SQLException ex) {
			log.error(" getListIntfRtgsOut : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	

	public IntfRtgsOut getTotNomIntfRtgsOut(IntfRtgsOutBean intfOut) {
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
			sql = "SELECT count(1) AS tottrans, sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) as totnominal FROM ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " dbo.T_RTIFTSOUT_HIS ";
			} else {
				sql += " dbo.T_RTIFTSOUT ";
			}
			sql += " where ValueDate = ? and";
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				sql += " RelTRN like '"+intfOut.getKdcab()+"%' and";
			}
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				sql += " UltimateBeneAcc = ? and";
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				sql += " Status = ? and";
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				sql += " TRN = ? and";
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				sql += " reltrn = ? and";
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				sql += " ToMember = ? and";
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				sql += " (CAST(Amount AS NUMERIC(20, 2)) / 100) >= ? and";
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				sql += "(CAST(Amount AS NUMERIC(20, 2)) / 100) <= ? and";
			}
			sql = sql.substring(0, sql.length() - 3) + " ";
			int i=1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, intfOut.getBsnsdt());	
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				stat.setString(i++, intfOut.getAcc());
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				stat.setString(i++, intfOut.getTxnsts());
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				stat.setString(i++, intfOut.getTrn());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				stat.setString(i++, intfOut.getTmmbr());
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
			}
			rs = stat.executeQuery();
			if (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setTottrans(rs.getInt("tottrans"));
				rtgsout.setTotnominal(rs.getBigDecimal("totnominal"));
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomIntfRtgsOut : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return rtgsout;
	}
	
	public IntfRtgsOut getTotNomIntfRtgsIn(IntfRtgsOutBean intfOut) {
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
			sql = "SELECT count(1) AS tottrans, sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) as totnominal FROM ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " dbo.T_RTIFTSIN_HIS ";
			} else {
				sql += " dbo.T_RTIFTSIN ";
			}
			sql += " where ValueDate = ? and";
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				sql += " RelTRN like '"+intfOut.getKdcab()+"%' and";
			}
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				sql += " UltimateBeneAcc = ? and";
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				sql += " Status = ? and";
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				sql += " TRN = ? and";
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				sql += " reltrn = ? and";
			}
			if (!"".equals(intfOut.getFmmbr()) && intfOut.getFmmbr() != null){
				sql += " fromMember = ? and";
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				sql += " (CAST(Amount AS NUMERIC(20, 2)) / 100) >= ? and";
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				sql += "(CAST(Amount AS NUMERIC(20, 2)) / 100) <= ? and";
			}
			sql = sql.substring(0, sql.length() - 3) + " ";
			int i=1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, intfOut.getBsnsdt());				
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				stat.setString(i++, intfOut.getKdcab());				
			}
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				stat.setString(i++, intfOut.getAcc());
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				stat.setString(i++, intfOut.getTxnsts());
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				stat.setString(i++, intfOut.getTrn());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getFmmbr()) && intfOut.getFmmbr() != null){
				stat.setString(i++, intfOut.getFmmbr());
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
			}
			rs = stat.executeQuery();
			if (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setTottrans(rs.getInt("tottrans"));
				rtgsout.setTotnominal(rs.getBigDecimal("totnominal"));
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomIntfRtgsIn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return rtgsout;
	}
	
	public List getListIntfRtgsIn(IntfRtgsOutBean intfOut, int a, int b) {
		List lbb = new ArrayList();
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
			sql = "SELECT row, dataId, BOR, ValueDate, Status, RsnCde, FromMember, TRN, RelTRN, OriName, " +
					"UltimateBeneName, UltimateBeneAcc, Amount, FromAccName, ToAccName, PayDetails, SettleTime " +
					"FROM ( SELECT ROW_NUMBER() OVER(ORDER BY dataId asc) AS Row, dataId, BOR, ValueDate, Status, RsnCde, FromMember, TRN, RelTRN," +
			"OriName, UltimateBeneName, UltimateBeneAcc, Amount, FromAccName, ToAccName, PayDetails, SettleTime " +
			"FROM ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				sql += " dbo.T_RTIFTSIN_HIS ";
			} else {
				sql += " dbo.T_RTIFTSIN ";
			}
			sql += " where ValueDate = ? and";
			if ("".equals(intfOut.getKdcab()) && "".equals(intfOut.getAcc()) && "".equals(intfOut.getTxnsts()) && "".equals(intfOut.getTrn()) 
					&& "".equals(intfOut.getReltrn()) && "".equals(intfOut.getBsnsdt()) && "".equals(intfOut.getTmmbr()) && "".equals(intfOut.getAmtto()) 
					&& "".equals(intfOut.getAmtf())){	
			} else {
				if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
					sql += " RelTRN like '"+intfOut.getKdcab()+"%' and";
				}
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					sql += " UltimateBeneAcc = ? and";
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					sql += " Status = ? and";
				}
				if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
					sql += " TRN = ? and";
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					sql += " reltrn = ? and";
				}
				if (!"".equals(intfOut.getFmmbr()) && intfOut.getFmmbr() != null){
					sql += " FromMember = ? and";
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					sql += " (CAST(Amount AS NUMERIC(20, 2)) / 100) >= ? and";
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					sql += "(CAST(Amount AS NUMERIC(20, 2)) / 100) <= ? and";
				}
			}
			sql = sql.substring(0, sql.length() - 3);
			sql += ")asdf where (Row BETWEEN ? AND ? );";
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setString(i++, intfOut.getBsnsdt());
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				stat.setString(i++, intfOut.getKdcab());				
			}
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				stat.setString(i++, intfOut.getAcc());
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				stat.setString(i++, intfOut.getTxnsts());
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				stat.setString(i++, intfOut.getTrn());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getFmmbr()) && intfOut.getFmmbr() != null){
				stat.setString(i++, intfOut.getFmmbr());
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
			}
			stat.setInt(i++, a);
			stat.setInt(i++, b);
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setDataId(rs.getString("dataId"));
				rtgsout.setBor(rs.getString("BOR"));
				rtgsout.setValuedate(rs.getString("ValueDate"));
				rtgsout.setStatus(rs.getString("Status"));
				rtgsout.setRsncde(rs.getString("RsnCde"));
				rtgsout.setFomember(rs.getString("FromMember"));
				rtgsout.setTrn(rs.getString("TRN"));
				rtgsout.setReltrn(rs.getString("RelTRN"));
				rtgsout.setOriname(rs.getString("OriName"));
				rtgsout.setUltimatebenename(rs.getString("UltimateBeneName"));
				rtgsout.setUltimatebeneacc(rs.getString("UltimateBeneAcc"));
				rtgsout.setAmount((new BigDecimal(rs.getString("Amount"))).divide(new BigDecimal(100)));
				rtgsout.setFromaccname(rs.getString("FromAccName"));
				rtgsout.setToaccname(rs.getString("ToAccName"));
				rtgsout.setPaydetails(rs.getString("PayDetails"));
				rtgsout.setSettletime(rs.getString("SettleTime"));
				lbb.add(rtgsout);
			}
		} catch (SQLException ex) {
			log.error(" getListIntfRtgsIn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public IntfRtgsOut getTotNomRtgsOutTtpn(IntfRtgsOutBean intfOut) {
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMig2DS().getConnection();
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getTrn()) || intfOut.getTrn() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
				sql = "select count(*) as total, sum( CAST ( amount AS numeric(20,2) )) as nominal  FROM rtgsout_titipan ";				
			} else {
				sql = "select count(*) as total, sum( CAST ( amount AS numeric(20,2) )) as nominal  FROM rtgsout_titipan where ";
				if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
					sql += " Rel_TRN like '"+intfOut.getKdcab()+"%' and";
				}
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					sql += " ultimate_benef_acc = ? and";
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					sql += " rtgstmp_sts = ? and";
				}
				if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
					sql += " TRN = ? and";
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					sql += " sender_ref_no = ? and";
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					sql += " to_member = ? and";
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					sql += " CAST(amount AS numeric(20,2)) >= ? and";
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					sql += " CAST(amount AS numeric(20,2)) <= ? and";
				}
				sql = sql.substring(0, sql.length() - 3);
			}	
			
			int i=1;
			stat = conn.prepareStatement(sql);
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getTrn()) || intfOut.getTrn() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
								
			} else {
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					stat.setString(i++, intfOut.getAcc());
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					stat.setString(i++, intfOut.getTxnsts());
				}
				if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
					stat.setString(i++, intfOut.getTrn());
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					stat.setString(i++, intfOut.getReltrn());
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					stat.setString(i++, intfOut.getTmmbr());
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
				}
			}
			rs = stat.executeQuery();
			if (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setTottrans(rs.getInt("total"));
				rtgsout.setTotnominal(rs.getBigDecimal("nominal"));
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomRtgsOutTtpn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return rtgsout;
	}
	
	public List getListRtgsOutTtpn(IntfRtgsOutBean intfOut, int begin) {
		List lbb = new ArrayList();
		RtgsTtpn rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMig2DS().getConnection();			
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getTrn()) || intfOut.getTrn() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
				sql = "SELECT t.sender_ref_no, t.trn, t.to_member, t.ta_name, m.nama_member, t.amount, t.pay_detail, t.rtgstmp_sts, t.ori_name," +
						"t.ultimate_benef_acc, t.ultimate_benef_name, to_char(t.insert_dt, 'yyyy-mm-dd  HH24:MI:SS') as insert_dt" +
						" FROM rtgsout_titipan t inner join member_bank m on t.to_member = m.kode_member ";
			} else {
				sql = "SELECT t.sender_ref_no, t.trn, t.to_member, t.ta_name, m.nama_member, t.amount, t.pay_detail, t.rtgstmp_sts, t.ori_name, " +
						"t.ultimate_benef_acc, t.ultimate_benef_name, to_char(t.insert_dt, 'yyyy-mm-dd  HH24:MI:SS') insert_dt " +
						"FROM rtgsout_titipan t inner join member_bank m on t.to_member = m.kode_member where ";
				if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
					sql += " t.Rel_TRN like '"+intfOut.getKdcab()+"%' and";
				}
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					sql += " t.ultimate_benef_acc = ? and";
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					sql += " t.rtgstmp_sts = ? and";
				}
				if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
					sql += " t.TRN = ? and";
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					sql += " t.sender_ref_no = ? and";
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					sql += " t.to_member = ? and";
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					sql += " CAST(t.amount AS numeric(20,2)) >= ? and";
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					sql += " CAST(t.amount AS numeric(20,2)) <= ? and";
				}
				sql = sql.substring(0, sql.length() - 3);
			}	
			sql += " order by t.insert_dt asc LIMIT 10 OFFSET ? ";
			int i=1;
			stat = conn.prepareStatement(sql);	
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getTrn()) || intfOut.getTrn() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
				
			} else {
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					stat.setString(i++, intfOut.getAcc());
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					stat.setString(i++, intfOut.getTxnsts());
				}
				if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
					stat.setString(i++, intfOut.getTrn());
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					stat.setString(i++, intfOut.getReltrn());
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					stat.setString(i++, intfOut.getTmmbr());
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
				}				
			}
			stat.setInt(i++, begin);
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new RtgsTtpn();
				rtgsout.setSender_ref_no(rs.getString("sender_ref_no"));
				rtgsout.setTrn(rs.getString("trn"));
				rtgsout.setTo_member(rs.getString("to_member"));
				rtgsout.setTa_name(rs.getString("ta_name"));
				rtgsout.setNama_member(rs.getString("nama_member"));
				rtgsout.setAmount(new BigDecimal(rs.getString("amount")));
				rtgsout.setPay_detail(rs.getString("pay_detail"));
				rtgsout.setRtgstmp_sts(rs.getString("rtgstmp_sts"));
				rtgsout.setOri_name(rs.getString("ori_name"));
				rtgsout.setUltimate_benef_acc(rs.getString("ultimate_benef_acc"));
				rtgsout.setUltimate_benef_name(rs.getString("ultimate_benef_name"));
				rtgsout.setInsert_dt(rs.getString("insert_dt"));
				lbb.add(rtgsout);
			}
		} catch (SQLException ex) {
			log.error(" getListRtgsOutTtpn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	
	public List getTotNomRkpRtgsOutTtpn(String intrf) {
		List lttpn = new ArrayList();
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMig2DS().getConnection();
			if("SKN".equals(intrf)){
				sql = "select distinct skntmp_sts, count(*) as total, sum( CAST ( amount AS numeric(20,2) )) as nominal  " +
				"FROM sknout_titipan group by skntmp_sts order by skntmp_sts ";
				
			} else if("RTGS".equals(intrf)){
				sql = "select distinct rtgstmp_sts, count(*) as total, sum( CAST ( amount AS numeric(20,2) )) as nominal  " +
				"FROM rtgsout_titipan group by rtgstmp_sts order by rtgstmp_sts ";				
			}
			int i=1;
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new IntfRtgsOut();
				if("SKN".equals(intrf)){
					rtgsout.setStatus(rs.getString("skntmp_sts"));
				} else if("RTGS".equals(intrf)){ 
					rtgsout.setStatus(rs.getString("rtgstmp_sts"));
				}
				rtgsout.setTottrans(rs.getInt("total"));
				rtgsout.setTotnominal(rs.getBigDecimal("nominal"));
				lttpn.add(rtgsout);
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomRtgsOutTtpn/getTotNomSknOutTtpn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lttpn;
	}
	
	public IntfRtgsOut getTotNomSknOutTtpn(IntfRtgsOutBean intfOut) {
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMig2DS().getConnection();
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getChannel()) || intfOut.getChannel() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
				sql = "select count(*) as total, sum( CAST ( amount AS numeric(20,2) )) as nominal  FROM sknout_titipan ";				
			} else {
				sql = "select count(*) as total, sum( CAST ( amount AS numeric(20,2) )) as nominal  FROM sknout_titipan  where ";
				if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
					sql += " Rel_TRN like '"+intfOut.getKdcab()+"%' and";
				}
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					sql += " ultimate_benef_acc = ? and";
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					sql += " skntmp_sts = ? and";
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					sql += " sender_ref_no = ? and";
				}
				if (!"".equals(intfOut.getChannel()) && intfOut.getChannel() != null){
					if("MCB".equals(intfOut.getChannel())){
						sql += " ta_number = ? and";						
					} else if("IB".equals(intfOut.getChannel())){
						sql += " ta_number = ? and";						
					}
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					sql += " to_member = ? and";
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					sql += " CAST(amount AS numeric(20,2)) >= ? and";
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					sql += " CAST(amount AS numeric(20,2)) <= ? and";
				}
				sql = sql.substring(0, sql.length() - 3);
			}	
			
			int i=1;
			stat = conn.prepareStatement(sql);
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getChannel()) || intfOut.getChannel() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
								
			} else {
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					stat.setString(i++, intfOut.getAcc());
				}

				if (!"".equals(intfOut.getChannel()) && intfOut.getChannel() != null){
					if("MCB".equals(intfOut.getChannel())){
						stat.setString(i++, "");							
					} else if("IB".equals(intfOut.getChannel())){
						stat.setString(i++, intfOut.getChannel());			
					}
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					stat.setString(i++, intfOut.getTxnsts());
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					stat.setString(i++, intfOut.getReltrn());
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					stat.setString(i++, intfOut.getTmmbr());
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
				}
			}
			rs = stat.executeQuery();
			if (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setTottrans(rs.getInt("total"));
				rtgsout.setTotnominal(rs.getBigDecimal("nominal"));
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomSknOutTtpn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return rtgsout;
	}
	
	public List getListSknOutTtpn(IntfRtgsOutBean intfOut, int begin) {
		List lbb = new ArrayList();
		RtgsTtpn rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMig2DS().getConnection();			
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getChannel()) || intfOut.getChannel() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
				sql = "SELECT t.sender_ref_no, t.ta_number, t.to_member, t.ta_name, m.nama_member, t.amount, t.pay_detail, t.skntmp_sts, t.ori_name," +
						"t.ultimate_benef_acc, t.ultimate_benef_name, to_char(t.insert_dt, 'yyyy-mm-dd  HH24:MI:SS') as insert_dt" +
						" FROM sknout_titipan t inner join member_bank m on t.to_member = m.kode_member ";
			} else {
				sql = "SELECT t.sender_ref_no, t.ta_number, t.to_member, t.ta_name, m.nama_member, t.amount, t.pay_detail, t.skntmp_sts, t.ori_name, " +
						"t.ultimate_benef_acc, t.ultimate_benef_name, to_char(t.insert_dt, 'yyyy-mm-dd  HH24:MI:SS') insert_dt " +
						"FROM sknout_titipan t inner join member_bank m on t.to_member = m.kode_member where ";
				if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
					sql += " t.Rel_TRN like '"+intfOut.getKdcab()+"%' and";
				}
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					sql += " t.ultimate_benef_acc = ? and";
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					sql += " t.skntmp_sts = ? and";
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					sql += " t.sender_ref_no = ? and";
				}
				if (!"".equals(intfOut.getChannel()) && intfOut.getChannel() != null){
					if("MCB".equals(intfOut.getChannel())){
						sql += " t.ta_number = '' and";						
					} else if("IB".equals(intfOut.getChannel())){
						sql += " t.ta_number = ? and";						
					}
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					sql += " t.to_member = ? and";
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					sql += " CAST(t.amount AS numeric(20,2)) >= ? and";
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					sql += " CAST(t.amount AS numeric(20,2)) <= ? and";
				}
				sql = sql.substring(0, sql.length() - 3);
			}	
			sql += " order by t.insert_dt asc LIMIT 10 OFFSET ? ";
			int i=1;
			stat = conn.prepareStatement(sql);	
			if (("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null) && ("".equals(intfOut.getAcc()) || intfOut.getAcc() == null) && 
					("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null) && ("".equals(intfOut.getTrn()) || intfOut.getTrn() == null) && 
					("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null) && ("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null) &&
					("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null) && ("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null)){
				
			} else {
				if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
					stat.setString(i++, intfOut.getAcc());
				}
				if (!"".equals(intfOut.getChannel()) && intfOut.getChannel() != null){
					if("MCB".equals(intfOut.getChannel())){
						stat.setString(i++, "");				
					} else if("IB".equals(intfOut.getChannel())){
						stat.setString(i++, intfOut.getChannel());			
					}
				}
				if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
					stat.setString(i++, intfOut.getTxnsts());
				}
				if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
					stat.setString(i++, intfOut.getReltrn());
				}
				if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
					stat.setString(i++, intfOut.getTmmbr());
				}
				if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
				}
				if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
					stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
				}			
			}
			stat.setInt(i++, begin);
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new RtgsTtpn();
				rtgsout.setSender_ref_no(rs.getString("sender_ref_no"));
				rtgsout.setTo_member(rs.getString("to_member"));
				rtgsout.setTa_name(rs.getString("ta_name"));
				rtgsout.setNama_member(rs.getString("nama_member"));
				rtgsout.setAmount(new BigDecimal(rs.getString("amount")));
				rtgsout.setPay_detail(rs.getString("pay_detail"));
				rtgsout.setRtgstmp_sts(rs.getString("skntmp_sts"));
				rtgsout.setOri_name(rs.getString("ori_name"));
				rtgsout.setUltimate_benef_acc(rs.getString("ultimate_benef_acc"));
				rtgsout.setUltimate_benef_name(rs.getString("ultimate_benef_name"));
				rtgsout.setInsert_dt(rs.getString("insert_dt"));
				lbb.add(rtgsout);
			}
		} catch (SQLException ex) {
			log.error(" getListSknOutTtpn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getRkpStsIntfRtgs(IntfRtgsOutBean intfOut, String opt) {
		List lrkprtgs = new ArrayList();
		IntfRtgsOut rtgsout = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getRtgsDS().getConnection();
			sql = "SELECT DISTINCT status, count(1) AS total, sum(CAST(amount AS NUMERIC(20, 2)) / 100 ) as totnominal from ";
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
			if (Integer.parseInt(intfOut.getBsnsdt()) < Integer.parseInt(dateFormat.format(date))){
				if("out".equals(opt)){
					sql += " dbo.T_RTIFTSOUT_HIS ";					
				} else {
					sql += " dbo.T_RTIFTSIN_HIS ";					
				}
			} else {
				if("out".equals(opt)){
					sql += " dbo.T_RTIFTSOUT ";					
				} else {
					sql += " dbo.T_RTIFTSIN ";
				}
			}
			sql += " where ValueDate = ? and";
			if (!"".equals(intfOut.getKdcab()) && intfOut.getKdcab() != null){
				sql += " RelTRN like '"+intfOut.getKdcab()+"%' and";
			}
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				sql += " UltimateBeneAcc = ? and";
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				sql += " Status = ? and";
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				sql += " TRN = ? and";
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				sql += " reltrn = ? and";
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				sql += " ToMember = ? and";
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				sql += " (CAST(Amount AS NUMERIC(20, 2)) / 100) >= ? and";
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				sql += "(CAST(Amount AS NUMERIC(20, 2)) / 100) <= ? and";
			}
			sql = sql.substring(0, sql.length() - 3) + " GROUP BY Status ";
			int i=1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, intfOut.getBsnsdt());
			if (!"".equals(intfOut.getAcc()) && intfOut.getAcc() != null){
				stat.setString(i++, intfOut.getAcc());
			}
			if (!"".equals(intfOut.getTxnsts()) && intfOut.getTxnsts() != null){
				stat.setString(i++, intfOut.getTxnsts());
			}
			if (!"".equals(intfOut.getTrn()) && intfOut.getTrn() != null){
				stat.setString(i++, intfOut.getTrn());
			}
			if (!"".equals(intfOut.getReltrn()) && intfOut.getReltrn() != null){
				stat.setString(i++, intfOut.getReltrn());
			}
			if (!"".equals(intfOut.getTmmbr()) && intfOut.getTmmbr() != null){
				stat.setString(i++, intfOut.getTmmbr());
			}
			if (!"".equals(intfOut.getAmtf()) && intfOut.getAmtf() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtf().replace(",", "")));
			}
			if (!"".equals(intfOut.getAmtto()) && intfOut.getAmtto() != null){
				stat.setBigDecimal(i++, new BigDecimal(intfOut.getAmtto().replace(",", "")));
			}
			rs = stat.executeQuery();
			while (rs.next()) {
				rtgsout = new IntfRtgsOut();
				rtgsout.setStatus(rs.getString("status"));
				rtgsout.setTottrans(rs.getInt("total"));
				rtgsout.setTotnominal(rs.getBigDecimal("totnominal"));
				lrkprtgs.add(rtgsout);
			}
		} catch (SQLException ex) {
		
			log.error(" getRkpStsIntfRtgs : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lrkprtgs;
	}
}
