package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.singleton.DatasourceEntry;

import WSInvoker.FCUBSAccServiceInv;
import WSInvoker.Util.DB.Parameters;
import WSInvoker.Util.DB.RetVal.AccInfoRetVal;
import WSInvoker.Util.DB.RetVal.ParamRetVal;

public class AccountFunction {
	private static Logger log = Logger.getLogger(AccountFunction.class);

	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}

	public BigDecimal getSaldoRek(String norek) {
		BigDecimal nom = new BigDecimal(0);
		Connection connMcb = null;
		ParamRetVal paramRetVal;
		Parameters utilParam;
		String wsHost = null, wsPort = null;
		String errMsg;
		int i = 0;
		Connection connMig = null;
		try {
			connMcb = DatasourceEntry.getInstance().getMcbprod().getConnection();
		} catch (SQLException e) {
			this.log.error("Failed getSaldoRek : get connection mcb :1:" + e.getMessage());
			return nom;
		} catch (Exception e) {
			this.log.error("Failed getSaldoRek : get connection mcb :2:" + e.getMessage());
			return nom;
		}
		if (connMcb == null) {
			this.log.error("Failed getSaldoRek : get connection mcb proccessMCB :2:");
			return nom;
		}

		try {
			connMig = DatasourceEntry.getInstance().getMig2DS().getConnection();
		} catch (SQLException e) {
			this.log.error("Failed : get connection mig :1:" + e.getMessage());
		} catch (Exception e) {
			this.log.error("Failed : get connection mig :2:" + e.getMessage());
		}
		if (connMig == null) {
			this.log.error("Failed : get connection mig processSknMcb :2:");
			return nom;
		}

		utilParam = new Parameters();

		paramRetVal = utilParam.getParameters(connMig, "fcgw.ip,fcgw.port");

		if (paramRetVal == null) {
			errMsg = "Failed : paramRetVal null";
			log.error(errMsg);
			return nom;
		} else {
			if (!paramRetVal.isRespOk()) {
				errMsg = "Failed : " + paramRetVal.getErrCode() + " "
						+ paramRetVal.getErrMessage();
				log.error(errMsg);
				return nom;
			}
		}

		for (i = 0; i < paramRetVal.getParameters().size(); i++) {
			if (paramRetVal.getParameters().get(i).getParamName().equals("fcgw.ip")) {
				wsHost = paramRetVal.getParameters().get(i).getParamValue();
			} else if (paramRetVal.getParameters().get(i).getParamName().equals("fcgw.port")) {
				wsPort = paramRetVal.getParameters().get(i).getParamValue();
			}
		}

		if (wsHost == null || wsPort == null) {
			errMsg = "Failed : " + wsHost + ":" + wsPort;
			log.error(errMsg);
			return nom;
		}

		FCUBSAccServiceInv wsAcc = new FCUBSAccServiceInv(wsHost, wsPort, connMcb);
		AccInfoRetVal accRet = wsAcc.getCIF(norek);
		if (accRet != null) {
			if (accRet.isRespOk()) {
				nom = accRet.getAcct().getSaldo();
			}
		}
		if (connMcb != null) {
			try {
				connMcb.close();
				connMcb = null;
			} catch (SQLException ex) {
				this.log.error("getSaldoRek :4: " + ex);
			}
		}

		if (connMig != null) {
			try {
				connMig.close();
				connMig = null;
			} catch (SQLException ex) {
				this.log.error("getSaldoRek :4: " + ex);
			}
		}
		return nom;
	}
	
	public Reklist getAccMCB(String nomrek) {
		Reklist rek = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
//			conn = DatasourceEntry.getInstance().getMig2DS().getConnection();			
//			sql = "SELECT nomrek, altnomrek, rekname, kdcab, kdval, reksys, reksts FROM reklist where nomrek = ? or altnomrek = ? ";

			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select cust_ac_no as NOMREK, alt_ac_no as altnomrek, substr(ac_desc, 0, 70) as REKNAME, branch_code as KDCAB, ccy as KDVAL, 'M' as reksys," +
					"(case when record_stat = 'O' and auth_stat = 'A' then '1'  when record_stat = 'C' then '3' else '2' end) reksts " +
					"from STTM_CUST_ACCOUNT where cust_ac_no = ? or alt_ac_no = ?";
			stat = conn.prepareStatement(sql);
			stat.setString(1, nomrek.trim());
			stat.setString(2, nomrek.trim());
			rs = stat.executeQuery();
			if (rs.next()) {
				rek = new Reklist();
				rek.setNomrek(rs.getString("nomrek"));
				rek.setAltnomrek(rs.getString("altnomrek"));
				rek.setRekname(rs.getString("rekname"));
				rek.setKdcab(rs.getString("kdcab"));
				rek.setKdval(rs.getString("kdval"));
				rek.setReksts(rs.getString("reksts"));
			}
		} catch (SQLException ex) {
			log.error(" getReklist : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return rek;
	}
}
