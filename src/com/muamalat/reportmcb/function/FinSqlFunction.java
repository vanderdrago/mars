package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.muamalat.reportmcb.entity.DetailBatch;
import com.muamalat.reportmcb.entity.Djadwalangsur;
import com.muamalat.reportmcb.entity.EdcMcb;
import com.muamalat.reportmcb.entity.Event;
import com.muamalat.reportmcb.entity.GetmCollt;
import com.muamalat.reportmcb.entity.GetmFaclt;
import com.muamalat.reportmcb.entity.Instalment;
import com.muamalat.reportmcb.entity.Liability;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.entity.TRtiftsout;
import com.muamalat.reportmcb.entity.UdfGetColt;
import com.muamalat.reportmcb.entity.Udfvalue;
import com.muamalat.reportmcb.entity.salamMuamalat;
import com.muamalat.singleton.DatasourceEntry;

public class FinSqlFunction {

	 private static Logger log = Logger.getLogger(FinSqlFunction.class);
	    private BigDecimal totMutDb;
	    private BigDecimal totMutCb;

	    public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException ex) {
	                log.error("closeConnDb 1 : " + ex.getMessage());
	            }
	        }
	        if (rs != null) {
	            try {
	                rs.close();
	                rs = null;
	            } catch (SQLException ex) {
	                log.error("closeConnDb 2 : " + ex.getMessage());
	            }
	        }
	        if (stat != null) {
	            try {
	                stat.close();
	                stat = null;
	            } catch (SQLException ex) {
	                log.error("closeConnDb 3 : " + ex.getMessage());
	            }
	        }
	    }
	    
	    
    public List getcolltList(String liabno, String colcod, String amont_a, String amont_b) {
        List ltrn = new ArrayList();
        GetmCollt trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            sql =  "select id, liab_id, collateral_code, collateral_description, category_id, collateral_currency, " +
//            	   "collateral_value, limit_contribution, start_date, end_date, available_amount, haircut, " +
//            	   "lendable_margin, liab_branch, branch_code from getm_collat order by liab_id ";
            sql = "select distinct a.id, a.liab_id, b.pool_id,  d.liab_no, d.liab_name, a.collateral_code, a.collateral_description, " +
      	  		  "a.category_id, a.collateral_currency, a.collateral_value, a.limit_contribution, a.start_date, " +
      	  		  "a.end_date, a.available_amount, haircut, a.lendable_margin, a.liab_branch, a.branch_code, " +
      	  		  "(case when a.record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
      	  		  "(case when a.auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
      	  		  "from getm_collat a, getm_pool_coll_linkages b, getm_pool c, getm_liab d " +
      	  		  "where a.id = b.collateral_id " +
      	  		  "and b.pool_id = c.id and c.liab_id = d.id ";
            System.out.println("sql getcolltList :" + sql);
            
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new GetmCollt();
                trn.setLiab_no(rs.getString("liab_no"));
                trn.setLiab_name(rs.getString("liab_name"));
                trn.setLiab_id(rs.getString("liab_id"));
                trn.setPool_id(rs.getString("pool_id"));
                trn.setCollateral_code(rs.getString("collateral_code"));
                trn.setCollateral_description(rs.getString("collateral_description"));
                trn.setCollateral_currency(rs.getString("collateral_currency"));
                trn.setLiab_branch(rs.getString("liab_branch"));
                trn.setCollateral_value(rs.getString("collateral_value"));
                trn.setLendable_margin(rs.getString("lendable_margin")); 
                trn.setLimit_contribution(rs.getString("limit_contribution"));
                trn.setBranch_code(rs.getString("branch_code"));
              //  trn.setStart_date(rs.getString("start_date"));
                	if (rs.getString("start_date")!=null) {
                		trn.setStart_date(rs.getString("start_date").substring(0, 10));
                	} else {
                		trn.setStart_date(rs.getString("start_date"));
                	}
                	
                	if (rs.getString("end_date") != null) {
    					trn.setEnd_date(rs.getString("end_date").substring(0,10));
    				} else {
    					trn.setEnd_date(rs.getString("end_date"));
    				}
                 trn.setStatus_rcd(rs.getString("status_rcd"));
                 trn.setStatus_oto(rs.getString("status_oto"));
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public List sGetmcoltList(String liabno, String colcod,  String amont_a, String amont_b) {
   	 	List ltrn = new ArrayList();
   	 	GetmCollt trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
       	 if ((liabno==null ) && (colcod==null ) && (amont_a==null )&& (amont_b==null )){
       		 return ltrn;
       	 }
       	 conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
            int i = 1;
//            sql = "select id, liab_id, collateral_code, collateral_description, category_id, collateral_currency, " +
//     	   		  "collateral_value, limit_contribution, start_date, end_date, available_amount, haircut, " +
//     	   		  "lendable_margin, liab_branch, branch_code from getm_collat where collateral_code != '"+awal+"' ";
            sql = "select distinct a.id, a.liab_id, b.pool_Id, d.liab_no, d.liab_name, a.collateral_code, a.collateral_description, " +
            	  "a.category_id, a.collateral_currency, a.collateral_value, a.limit_contribution, a.start_date, " +
            	  "a.end_date, a.available_amount, haircut, a.lendable_margin, a.liab_branch, a.branch_code, " +
            	  "(case when a.record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
            	  "(case when a.auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
            	  "from getm_collat a, getm_pool_coll_linkages b, getm_pool c, getm_liab d " +
            	  "where a.id = b.collateral_id " +
            	  "and b.pool_id = c.id and c.liab_id = d.id ";
            if(liabno!=null && !"".equals(liabno)){
      	       	 sql += " and d.liab_no = ? ";
      	    }
            if(colcod!=null && !"".equals(colcod) ){
   	       	 sql += " and a.collateral_code = ? ";
   	        }
   	        if(amont_a!=null && !"".equals(amont_a)){
   	       	 sql += " and (CAST(a.collateral_value AS NUMERIC(20, 2))) >= ? ";
   	        }
   	        if(amont_b!=null && !"".equals(amont_b)){
   	       	 sql += " and (CAST(a.collateral_value AS NUMERIC(20, 2))) <= ? ";
   	        }
   	        stat = conn.prepareStatement(sql);
   	        if(liabno!=null && !"".equals(liabno)){
	            stat.setString(i++,liabno);
	        }
   	        if(colcod!=null && !"".equals(colcod)){
   	            stat.setString(i++,colcod);
   	        }
   	        if(amont_a!=null && !"".equals(amont_a)){
   	            stat.setString(i++,amont_a);
   	        }
   	        if(amont_b!=null && !"".equals(amont_b)){
   	            stat.setString(i++,amont_b);
   	        }
   	        
   	        System.out.println("sql sGetmcoltList :" + sql);
   	        
   	        rs = stat.executeQuery();
   	        while (rs.next()) {
   		       	 trn = new GetmCollt();
   		       	 trn.setLiab_no(rs.getString("liab_no"));
   		         trn.setLiab_name(rs.getString("liab_name"));
   		       	 trn.setLiab_id(rs.getString("liab_id"));
   		       	 trn.setPool_id(rs.getString("pool_id"));
	             trn.setCollateral_code(rs.getString("collateral_code"));
	             trn.setCollateral_description(rs.getString("collateral_description"));
	             trn.setCollateral_currency(rs.getString("collateral_currency"));
	             trn.setLiab_branch(rs.getString("liab_branch"));
	             trn.setCollateral_value(rs.getString("collateral_value"));
	             trn.setLendable_margin(rs.getString("lendable_margin")+ "%"); 
	             trn.setLimit_contribution(rs.getString("limit_contribution"));
		         trn.setBranch_code(rs.getString("branch_code"));
					if (rs.getString("start_date") != null) {
						trn.setStart_date(rs.getString("start_date").substring(0,10));
					} else {
						trn.setStart_date(rs.getString("start_date"));
					}
			         //trn.setStart_date(rs.getString("start_date").substring(0, 10));
					if (rs.getString("end_date") != null) {
						trn.setEnd_date(rs.getString("end_date").substring(0,10));
					} else {
						trn.setEnd_date(rs.getString("end_date"));
					}
				 trn.setStatus_rcd(rs.getString("status_rcd"));
	             trn.setStatus_oto(rs.getString("status_oto"));
   		        ltrn.add(trn);
   	        }
   	    } catch (SQLException ex) {
   	   	 	log.error(" get getListMCBMon : " + ex.getMessage());
   	    } finally {
   	        closeConnDb(conn, stat, rs);
   	    }
   	    return ltrn;
   	}
    
    public List getvalcol(String function_id, String colcod) {
    	List colrn = new ArrayList();
    	UdfGetColt colt = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select  UDF_VALUE_1, " +
            	  "(select LOV_DESC " +
            	  "from UDTM_LOV " +
            	  "where LOV = (select UDF_VALUE_2 " +
            	  				"from GETM_COLLAT " +
            	  				"where collateral_code = ? ) " +
            	  	"and FIELD_NAME = (select field_name " +
            	  						"from cstm_function_udf_fields_map " +
            	  						"where function_id  = 'GEDCOLLT' " +
            	  						"AND field_num = '2' " +
            	  						"AND AUTH_stat = 'A')) as UDF_VALUE_2, " +
            	  "UDF_VALUE_3, UDF_VALUE_4, UDF_VALUE_5, UDF_VALUE_6, UDF_VALUE_7, UDF_VALUE_8, " +
            	  "UDF_VALUE_9, UDF_VALUE_10, UDF_VALUE_11, UDF_VALUE_12, UDF_VALUE_13, UDF_VALUE_14, " +
            	  "UDF_VALUE_15, UDF_VALUE_16, UDF_VALUE_17, UDF_VALUE_18, UDF_VALUE_19, UDF_VALUE_20, " +
            	  "UDF_VALUE_21, UDF_VALUE_22, UDF_VALUE_23, UDF_VALUE_24, UDF_VALUE_25, UDF_VALUE_26, " +
            	  "UDF_VALUE_27, UDF_VALUE_28, UDF_VALUE_29, UDF_VALUE_30, " +
            	  "(select LOV_DESC " +
            	  "from UDTM_LOV " +
            	  "where LOV = (select UDF_VALUE_31 " +
            	  				"from GETM_COLLAT " +
            	  				"where collateral_code = ? ) " +
            	  "and FIELD_NAME = (select field_name " +
            	  					"from cstm_function_udf_fields_map " +
            	  					"where function_id  = 'GEDCOLLT' " +
            	  					"AND field_num = '1' " +
            	  					"AND AUTH_stat = 'A')) as UDF_VALUE_31 , " +
            	  "UDF_VALUE_32, UDF_VALUE_33, UDF_VALUE_34, UDF_VALUE_35, UDF_VALUE_36, UDF_VALUE_37, " +
            	  "UDF_VALUE_38, UDF_VALUE_39, UDF_VALUE_40, UDF_VALUE_41, UDF_VALUE_42, UDF_VALUE_43, " +
            	  "UDF_VALUE_44, UDF_VALUE_45, UDF_VALUE_46, UDF_VALUE_47, UDF_VALUE_48, UDF_VALUE_49, " +
            	  "UDF_VALUE_50 from getm_collat where collateral_code = ? "; 
            stat = conn.prepareStatement(sql);
            stat.setString(1, colcod);
            stat.setString(2, colcod);
            stat.setString(3, colcod);
            rs = stat.executeQuery();
            Udfvalue udf = new Udfvalue();
            if (rs.next()) {
            	udf.setUDF_VALUE_1(rs.getString("udf_value_1"));
            	udf.setUDF_VALUE_2(rs.getString("udf_value_2"));
            	udf.setUDF_VALUE_3(rs.getString("udf_value_3"));
            	udf.setUDF_VALUE_4(rs.getString("udf_value_4"));
            	udf.setUDF_VALUE_5(rs.getString("udf_value_5"));
            	udf.setUDF_VALUE_6(rs.getString("udf_value_6"));
            	udf.setUDF_VALUE_7(rs.getString("udf_value_7"));
            	udf.setUDF_VALUE_8(rs.getString("udf_value_8"));
            	udf.setUDF_VALUE_9(rs.getString("udf_value_9"));
            	udf.setUDF_VALUE_10(rs.getString("udf_value_10"));
            	udf.setUDF_VALUE_11(rs.getString("udf_value_11"));
            	udf.setUDF_VALUE_12(rs.getString("udf_value_12"));
            	udf.setUDF_VALUE_13(rs.getString("udf_value_13"));
            	udf.setUDF_VALUE_14(rs.getString("udf_value_14"));
            	udf.setUDF_VALUE_15(rs.getString("udf_value_15"));
            	udf.setUDF_VALUE_16(rs.getString("udf_value_16"));
            	udf.setUDF_VALUE_17(rs.getString("udf_value_17"));
            	udf.setUDF_VALUE_18(rs.getString("udf_value_18"));
            	udf.setUDF_VALUE_19(rs.getString("udf_value_19"));
            	udf.setUDF_VALUE_20(rs.getString("udf_value_20"));
            	udf.setUDF_VALUE_21(rs.getString("udf_value_21"));
            	udf.setUDF_VALUE_22(rs.getString("udf_value_22"));
            	udf.setUDF_VALUE_23(rs.getString("udf_value_23"));
            	udf.setUDF_VALUE_24(rs.getString("udf_value_24"));
            	udf.setUDF_VALUE_25(rs.getString("udf_value_25"));
            	udf.setUDF_VALUE_26(rs.getString("udf_value_26"));
            	udf.setUDF_VALUE_27(rs.getString("udf_value_27"));
            	udf.setUDF_VALUE_28(rs.getString("udf_value_28"));
            	udf.setUDF_VALUE_29(rs.getString("udf_value_29"));
            	udf.setUDF_VALUE_30(rs.getString("udf_value_30"));
            	udf.setUDF_VALUE_31(rs.getString("udf_value_31"));
            	udf.setUDF_VALUE_32(rs.getString("udf_value_32"));
            	udf.setUDF_VALUE_33(rs.getString("udf_value_33"));
            	udf.setUDF_VALUE_34(rs.getString("udf_value_34"));
            	udf.setUDF_VALUE_35(rs.getString("udf_value_35"));
            	udf.setUDF_VALUE_36(rs.getString("udf_value_36"));
            	udf.setUDF_VALUE_37(rs.getString("udf_value_37"));
            	udf.setUDF_VALUE_38(rs.getString("udf_value_38"));
            	udf.setUDF_VALUE_39(rs.getString("udf_value_39"));
            	udf.setUDF_VALUE_40(rs.getString("udf_value_40"));
            	udf.setUDF_VALUE_41(rs.getString("udf_value_41"));
            	udf.setUDF_VALUE_42(rs.getString("udf_value_42"));
            	udf.setUDF_VALUE_43(rs.getString("udf_value_43"));
            	udf.setUDF_VALUE_44(rs.getString("udf_value_44"));
            	udf.setUDF_VALUE_45(rs.getString("udf_value_45"));
            	udf.setUDF_VALUE_46(rs.getString("udf_value_46"));
            	udf.setUDF_VALUE_47(rs.getString("udf_value_47"));
            	udf.setUDF_VALUE_48(rs.getString("udf_value_48"));
            	udf.setUDF_VALUE_49(rs.getString("udf_value_49"));
            	udf.setUDF_VALUE_50(rs.getString("udf_value_50"));
//            	udf.setUDF_VALUE_51(rs.getString("udf_value_51"));
//            	udf.setUDF_VALUE_52(rs.getString("udf_value_52"));
//            	udf.setUDF_VALUE_53(rs.getString("udf_value_53"));
//            	udf.setUDF_VALUE_54(rs.getString("udf_value_54"));
//            	udf.setUDF_VALUE_55(rs.getString("udf_value_55"));
            	udf.setUDF_VALUE_51("-");
            	udf.setUDF_VALUE_52("-");
            	udf.setUDF_VALUE_53("-");
            	udf.setUDF_VALUE_54("-");
            	udf.setUDF_VALUE_55("-");
            	
           }
            
            sql =  "select function_id, field_name, field_num, auth_stat from cstm_function_udf_fields_map " +
		     	   "where function_id  = ? order by field_num";
		     stat = conn.prepareStatement(sql);
		     stat.setString(1, function_id);
		     rs = stat.executeQuery();
		     
		     while (rs.next()) {
		     	colt = new UdfGetColt();
		     	colt.setFunction_id(rs.getString("function_id"));
		     	colt.setField_name(rs.getString("field_name"));
		     	colt.setField_num(rs.getInt("field_num"));
		     	colt.setAuth_stat(rs.getString("auth_stat"));
		     	if(rs.getInt("field_num") == 1){
			     	colt.setValue(udf.getUDF_VALUE_1());		     		
		     	}
		     	if(rs.getInt("field_num") == 2){
			     	colt.setValue(udf.getUDF_VALUE_2());		     		
		     	}
		     	if(rs.getInt("field_num") == 3){
			     	colt.setValue(udf.getUDF_VALUE_3());		     		
		     	}
		     	if(rs.getInt("field_num") == 4){
			     	colt.setValue(udf.getUDF_VALUE_4());		     		
		     	}
		     	if(rs.getInt("field_num") == 5){
			     	colt.setValue(udf.getUDF_VALUE_5());		     		
		     	}
		     	if(rs.getInt("field_num") == 6){
			     	colt.setValue(udf.getUDF_VALUE_6());		     		
		     	}
		     	if(rs.getInt("field_num") == 7){
			     	colt.setValue(udf.getUDF_VALUE_7());		     		
		     	}
		     	if(rs.getInt("field_num") == 8){
			     	colt.setValue(udf.getUDF_VALUE_8());		     		
		     	}
		     	if(rs.getInt("field_num") == 9){
			     	colt.setValue(udf.getUDF_VALUE_9());		     		
		     	}
		     	if(rs.getInt("field_num") == 10){
			     	colt.setValue(udf.getUDF_VALUE_10());		     		
		     	}
		     	if(rs.getInt("field_num") == 11){
			     	colt.setValue(udf.getUDF_VALUE_11());		     		
		     	}
		     	if(rs.getInt("field_num") == 12){
			     	colt.setValue(udf.getUDF_VALUE_12());		     		
		     	}
		     	if(rs.getInt("field_num") == 13){
			     	colt.setValue(udf.getUDF_VALUE_13());		     		
		     	}
		     	if(rs.getInt("field_num") == 14){
			     	colt.setValue(udf.getUDF_VALUE_14());		     		
		     	}
		     	if(rs.getInt("field_num") == 15){
			     	colt.setValue(udf.getUDF_VALUE_15());		     		
		     	}
		     	if(rs.getInt("field_num") == 16){
			     	colt.setValue(udf.getUDF_VALUE_16());		     		
		     	}
		     	if(rs.getInt("field_num") == 17){
			     	colt.setValue(udf.getUDF_VALUE_17());		     		
		     	}
		     	if(rs.getInt("field_num") == 18){
			     	colt.setValue(udf.getUDF_VALUE_18());		     		
		     	}
		     	if(rs.getInt("field_num") == 19){
			     	colt.setValue(udf.getUDF_VALUE_19());		     		
		     	}
		     	if(rs.getInt("field_num") == 20){
			     	colt.setValue(udf.getUDF_VALUE_20());		     		
		     	}
		     	if(rs.getInt("field_num") == 21){
			     	colt.setValue(udf.getUDF_VALUE_21());		     		
		     	}
		     	if(rs.getInt("field_num") == 22){
			     	colt.setValue(udf.getUDF_VALUE_22());		     		
		     	}
		     	if(rs.getInt("field_num") == 23){
			     	colt.setValue(udf.getUDF_VALUE_23());		     		
		     	}
		     	if(rs.getInt("field_num") == 24){
			     	colt.setValue(udf.getUDF_VALUE_24());		     		
		     	}
		     	if(rs.getInt("field_num") == 25){
			     	colt.setValue(udf.getUDF_VALUE_25());		     		
		     	}
		     	if(rs.getInt("field_num") == 26){
			     	colt.setValue(udf.getUDF_VALUE_26());		     		
		     	}
		     	if(rs.getInt("field_num") == 27){
			     	colt.setValue(udf.getUDF_VALUE_27());		     		
		     	}
		     	if(rs.getInt("field_num") == 28){
			     	colt.setValue(udf.getUDF_VALUE_28());		     		
		     	}
		     	if(rs.getInt("field_num") == 29){
			     	colt.setValue(udf.getUDF_VALUE_29());		     		
		     	}
		     	if(rs.getInt("field_num") == 30){
			     	colt.setValue(udf.getUDF_VALUE_30());		     		
		     	}
		     	if(rs.getInt("field_num") == 31){
			     	colt.setValue(udf.getUDF_VALUE_31());		     		
		     	}
		     	if(rs.getInt("field_num") == 32){
			     	colt.setValue(udf.getUDF_VALUE_32());		     		
		     	}
		     	if(rs.getInt("field_num") == 33){
			     	colt.setValue(udf.getUDF_VALUE_33());		     		
		     	}
		     	if(rs.getInt("field_num") == 34){
			     	colt.setValue(udf.getUDF_VALUE_34());		     		
		     	}
		     	if(rs.getInt("field_num") == 35){
			     	colt.setValue(udf.getUDF_VALUE_35());		     		
		     	}
		     	if(rs.getInt("field_num") == 36){
			     	colt.setValue(udf.getUDF_VALUE_36());		     		
		     	}
		     	if(rs.getInt("field_num") == 37){
			     	colt.setValue(udf.getUDF_VALUE_37());		     		
		     	}
		     	if(rs.getInt("field_num") == 38){
			     	colt.setValue(udf.getUDF_VALUE_38());		     		
		     	}
		     	if(rs.getInt("field_num") == 39){
			     	colt.setValue(udf.getUDF_VALUE_39());		     		
		     	}
		     	if(rs.getInt("field_num") == 40){
			     	colt.setValue(udf.getUDF_VALUE_40());		     		
		     	}
		     	if(rs.getInt("field_num") == 41){
			     	colt.setValue(udf.getUDF_VALUE_41());		     		
		     	}
		     	if(rs.getInt("field_num") == 42){
			     	colt.setValue(udf.getUDF_VALUE_42());		     		
		     	}
		     	if(rs.getInt("field_num") == 43){
			     	colt.setValue(udf.getUDF_VALUE_43());		     		
		     	}
		     	if(rs.getInt("field_num") == 44){
			     	colt.setValue(udf.getUDF_VALUE_44());		     		
		     	}
		     	if(rs.getInt("field_num") == 45){
			     	colt.setValue(udf.getUDF_VALUE_45());		     		
		     	}
		     	if(rs.getInt("field_num") == 46){
			     	colt.setValue(udf.getUDF_VALUE_46());		     		
		     	}
		     	if(rs.getInt("field_num") == 47){
			     	colt.setValue(udf.getUDF_VALUE_47());		     		
		     	}
		     	if(rs.getInt("field_num") == 48){
			     	colt.setValue(udf.getUDF_VALUE_48());		     		
		     	}
		     	if(rs.getInt("field_num") == 49){
			     	colt.setValue(udf.getUDF_VALUE_49());		     		
		     	}
		     	if(rs.getInt("field_num") == 50){
			     	colt.setValue(udf.getUDF_VALUE_50());	
			    }
		     	if(rs.getInt("field_num") == 51){
			     	colt.setValue(udf.getUDF_VALUE_51());		     		
		     	}
		     	if(rs.getInt("field_num") == 52){
			     	colt.setValue(udf.getUDF_VALUE_52());		     		
		     	}
		     	if(rs.getInt("field_num") == 53){
			     	colt.setValue(udf.getUDF_VALUE_53());		     		
		     	}
		     	if(rs.getInt("field_num") == 54){
			     	colt.setValue(udf.getUDF_VALUE_54());		     		
		     	}
		     	if(rs.getInt("field_num") == 55){
			     	colt.setValue(udf.getUDF_VALUE_55());	
		     	}
		     	colrn.add(colt);
		     }
        } catch (SQLException ex) {
        	log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return colrn;
    }
    
    public List getinstlList(String accno, String cusid, String cusname) {
        List ltrn = new ArrayList();
        Instalment trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            sql =  "select customer_id, product_code, product_category, book_date, maturity_date, branch_code, currency, " +
//	  				"account_number, primary_applicant_id, primary_applicant_name, no_of_installments, total_sale_value, " +
//	  				"upfront_profit_booked, amount_disbursed,  maker_id, checker_id from  CLTB_ACCOUNT_MASTER " +
//	  				"order by product_code "; 
            sql =  "select a.customer_id, a.product_code, a.product_category, b.product_desc, a.book_date, " +
			  		"a.maturity_date, a.branch_code, a.currency, a.amount_financed, " +
			  		"a.account_number, a.primary_applicant_id, a.primary_applicant_name, a.no_of_installments, " +
			  		"a.total_sale_value, a.upfront_profit_booked, a.amount_disbursed,  a.maker_id, a.checker_id " +
			  		"from  CLTB_ACCOUNT_MASTER a, cltm_product b " +
			  		"where a.product_code = b.product_code ";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new Instalment();
                trn.setCustomer_id(rs.getString("customer_id"));
                trn.setProd_code(rs.getString("product_code"));
                trn.setProd_ctgry(rs.getString("product_category"));
                trn.setProd_desc(rs.getString("product_desc"));
                trn.setBook_date(rs.getString("book_date"));
                trn.setMatur_date(rs.getString("maturity_date"));
                trn.setAccount_number(rs.getString("account_number"));
                trn.setPrim_app_id(rs.getString("primary_applicant_id"));
                trn.setPrim_app_name(rs.getString("primary_applicant_name"));
                trn.setNo_of_installments(rs.getString("no_of_installments"));
                trn.setTotal_sale_value(rs.getBigDecimal("total_sale_value"));
                trn.setUpfront_profit_booked(rs.getBigDecimal("upfront_profit_booked"));
                trn.setAmount_disbursed(rs.getBigDecimal("amount_disbursed"));
                trn.setAmount_financed(rs.getBigDecimal("amount_financed"));
                              
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	log.error(" getInstalment : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public int getTotListInstlmnt (String accno, String cusid, String cusname, String status){
    	int tot = 0;
    	Instalment bb = null;
    	PreparedStatement stat = null;
    	Connection conn = null;
    	ResultSet rs = null;
        String sql = "";
        
        try {
			
        	conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
        	sql = "select count (*) as total from (select a.customer_id, a.product_code, a.product_category, b.product_desc, a.book_date, " +
  		          "a.maturity_date, a.branch_code, a.currency, a.account_number, a.primary_applicant_id, " +
  		          "(c.customer_name1) primary_applicant_name, decode(a.account_status, 'A', 'Active', 'L', 'Liquidate', 'H', 'Hold', 'V', 'Reverse') account_status, " +
  		          "a.no_of_installments, a.total_sale_value, " +
  		          "a.upfront_profit_booked, a.amount_disbursed, a.amount_financed, a.maker_id, a.checker_id " +
  		          "from  cltb_account_apps_master a, cltm_product b, sttm_customer c " +
  		          "where a.product_code = b.product_code " +
  		          "and a.customer_id = c.customer_no " +
  		          "and c.record_stat IN ('O','C') " +
  		          "and c.auth_stat = 'A' " +
  		          "and a.auth_stat = 'A' ";	
        	
  			if (!"".equals(accno) && accno != null){
  				if ("active".equals(status)){
  					sql += " and a.account_number = ? and a.account_status = 'A'";
  				}else if ("liquidate".equals(status)){
  					sql += " and a.account_number = ? and a.account_status = 'L'";
  				} else {
  					sql += " and a.account_number = ? ";
  				}
  			}if (!"".equals(cusid) && cusid != null){
  				if ("active".equals(status)){
  					sql += " and a.customer_id = ? and a.account_status = 'A'";
  				}else if ("liquidate".equals(status)){
  					sql += " and a.customer_id = ? and a.account_status = 'L'";
  				} else {
  					sql += " and a.customer_id = ? ";
  				}
  			}if (!"".equals(cusname) && cusname != null){
  				if ("active".equals(status)){
  					sql += " and c.customer_name1 like ? and a.account_status = 'A'";
  				}else if ("liquidate".equals(status)){
  					sql += " and c.customer_name1 like ? and a.account_status = 'L'";
  				} else {
  					sql += " c.customer_name1 like ? ";
  				}
  			}
  			
  			sql += " order by a.amount_financed desc) ";
  			System.out.println("SQL TOTAL :" + sql);
  			stat = conn.prepareStatement(sql);
  			int i = 1;
  			if (!"".equals(accno) && accno != null){
  				if ("active".equals(status)){
  					stat.setString(i++, accno);
  				} else {
  					stat.setString(i++, accno);
  				}
  			}if (!"".equals(cusid) && cusid != null){
  				if ("active".equals(status)){
  					stat.setString(i++, cusid);
  				} else {
  					stat.setString(i++, cusid);
  				}
  			}if (!"".equals(cusname) && cusname != null){
  				if ("active".equals(status)){
  					stat.setString(i++, "%" + cusname.toUpperCase() + "%");
  				} else {
  					stat.setString(i++, "%" + cusname.toUpperCase() + "%");
  				}
  				
  			} 
  			rs = stat.executeQuery();
  			if (rs.next()) {
				tot = rs.getInt("total");
				System.out.println("tot :" + tot);
			}
		} catch (Exception e) {
			log.error("getTotListInstlmnt :" + e.getMessage());
		}  finally {
			closeConnDb(conn, stat, rs);
		}
        return tot;
    }
    
    public List getListInstlmnt1(String accno, String cusid, String cusname, String active, String liquidate) {
		List lbb = new ArrayList();
		Instalment bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
	       	 if ((accno==null ) && (cusid==null ) && (cusname==null )){
	       		 return lbb;
	       	 }
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
		    //sql = "select a.customer_id, a.product_code, a.product_category, b.product_desc, a.book_date, " +
		    //	  "a.maturity_date, a.branch_code, a.currency, a.account_number, a.primary_applicant_id, " +
		    //	  "(c.customer_name1) primary_applicant_name, decode(d.account_status, 'A', 'Active', 'Liquidate') account_status, " +
		    //	  "a.no_of_installments, a.total_sale_value, " +
		    //	  "a.upfront_profit_booked, a.amount_disbursed, a.amount_financed, a.maker_id, a.checker_id " +
		    //	  "from  CLTB_ACCOUNT_MASTER a, cltm_product b, sttm_customer c, cltb_account_apps_master d " +
		    //	  "where a.product_code = b.product_code " +
		    //	  "and a.customer_id = c.customer_no " +
		    //	  "and a.account_number = d.account_number " +
		    //	  "and a.customer_id = d.customer_id " +
		    //	  "and a.branch_code = d.branch_code " +
		    //	  "and c.record_stat = 'O' " +
		    //	  "and c.auth_stat = 'A' and d.auth_stat = 'A' ";
		    sql = "select a.customer_id, a.product_code, a.product_category, b.product_desc, a.book_date, " +
		          "a.maturity_date, a.branch_code, a.currency, a.account_number, a.primary_applicant_id, " +
		          "(c.customer_name1) primary_applicant_name, decode(a.account_status, 'A', 'Active', 'L', 'Liquidate', 'H', 'Hold', 'V', 'Reverse') account_status, " +
		          "a.no_of_installments, a.total_sale_value, " +
		          "a.upfront_profit_booked, a.amount_disbursed, a.amount_financed, a.maker_id, a.checker_id " +
		          "from  cltb_account_apps_master a, cltm_product b, sttm_customer c " +
		          "where a.product_code = b.product_code " +
		          "and a.customer_id = c.customer_no " +
		          "and c.record_stat = 'O' " +
		          "and c.auth_stat = 'A' " +
		          "and a.auth_stat = 'A' ";	
		    if (!"".equals(accno) && accno != null){
				if ("1".equals(active)){
					sql += " and a.account_number = ? and a.account_status = 'A' ";
				} else {
					sql += " and a.account_number = ? and a.account_status = 'L' ";
				}
			}if (!"".equals(cusid) && cusid != null){
				if ("1".equals(active)){
					sql += " and a.customer_id = ? and a.account_status = 'A' ";
				} else {
					sql += " and a.customer_id = ? and a.account_status = 'L' ";
				}
			}if (!"".equals(cusname) && cusname != null){
				if ("1".equals(active)){
					sql += " and c.customer_name1 like ? and a.account_status = 'A' ";
				} else {
					sql += " and c.customer_name1 like ? and a.account_status = 'L' ";
				}
			}
//				sql += " and c.customer_name1 like ? ";
//			}if ("Active".equals(active)){
//				sql += " and a.account_status = 'A'";
//			}if ("Liquidate".equals(liquidate)){
//				sql += " and a.account_status = 'L'";
//			}
			sql += " order by amount_financed desc) where no >= ? and no <= ? ";
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			if (!"".equals(accno) && accno != null){
				if ("1".equals(active)){
					stat.setString(i++, accno.trim());
				} else {
					stat.setString(i++, accno.trim());
				}
			}
			if (!"".equals(cusid) && cusid != null){
				if ("1".equals(active)){
					stat.setString(i++, cusid.trim());
				} else {
					stat.setString(i++, cusid.trim());
				}
			}
			if (!"".equals(cusname) && cusname != null){
				if ("1".equals(active)){
					stat.setString(i++, "%" + cusname.toUpperCase() + "%");
				} else {
					stat.setString(i++, "%" + cusname.toUpperCase() + "%");
				}
			}
			//stat.setInt(i++, begin);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Instalment();
				bb.setCustomer_id(rs.getString("customer_id"));
				bb.setProd_code(rs.getString("product_code"));
				bb.setProd_ctgry(rs.getString("product_category"));
				bb.setProd_desc(rs.getString("product_desc"));
				bb.setAccount_status(rs.getString("account_status"));
				//bb.setBook_date(rs.getString("book_date"));
				if (rs.getString("book_date") != null) {
            		bb.setBook_date(rs.getString("book_date").substring(0,10));
				} else {
					bb.setBook_date(rs.getString("book_date"));
				}
				//bb.setMatur_date(rs.getString("maturity_date"));
				if (rs.getString("maturity_date") != null) {
            		bb.setMatur_date(rs.getString("maturity_date").substring(0,10));
				} else {
					bb.setMatur_date(rs.getString("maturity_date"));
				}
				bb.setAccount_number(rs.getString("account_number"));
				bb.setPrim_app_id(rs.getString("primary_applicant_id"));
				bb.setPrim_app_name(rs.getString("primary_applicant_name"));
				bb.setNo_of_installments(rs.getString("no_of_installments"));
				//bb.setTotal_sale_value(rs.getBigDecimal("total_sale_value"));
				if (rs.getBigDecimal("total_sale_value") == null){
					bb.setTotal_sale_value2(new BigDecimal(0));
				} else {
					bb.setTotal_sale_value2(rs.getBigDecimal("total_sale_value"));
				}
				//bb.setTotal_sale_value(rs.getBigDecimal("total_sale_value"));
				if (rs.getBigDecimal("total_sale_value") == null){
					bb.setTotal_sale_value(new BigDecimal(0));
				} else {
					bb.setTotal_sale_value(rs.getBigDecimal("total_sale_value"));
				}
				//-------
				if (rs.getBigDecimal("upfront_profit_booked") == null){
					bb.setUpfront_profit_booked(new BigDecimal(0));
				} else {
					bb.setUpfront_profit_booked(rs.getBigDecimal("upfront_profit_booked"));
				}
				//bb.setAmount_disbursed(rs.getBigDecimal("amount_disbursed"));
				if (rs.getBigDecimal("amount_disbursed") == null){
					bb.setAmount_disbursed(new BigDecimal(0));
				} else {
					bb.setAmount_disbursed(rs.getBigDecimal("amount_disbursed"));
				}
				if (rs.getBigDecimal("amount_financed") == null){
					bb.setAmount_financed(new BigDecimal(0));
				} else {
					bb.setAmount_financed(rs.getBigDecimal("amount_financed"));
				}
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListCustom : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
    
    public List getListInstlmnt(String accno, String cusid, String cusname, String status,int begin, int delta) {
    	System.out.println("status sql :" + status);
		List lbb = new ArrayList();
		Instalment bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
	       	 if ((accno==null ) && (cusid==null ) && (cusname==null )){
	       		 return lbb;
	       	 }
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
		    //sql = "select a.customer_id, a.product_code, a.product_category, b.product_desc, a.book_date, " +
		    //	  "a.maturity_date, a.branch_code, a.currency, a.account_number, a.primary_applicant_id, " +
		    //	  "(c.customer_name1) primary_applicant_name, decode(d.account_status, 'A', 'Active', 'Liquidate') account_status, " +
		    //	  "a.no_of_installments, a.total_sale_value, " +
		    //	  "a.upfront_profit_booked, a.amount_disbursed, a.amount_financed, a.maker_id, a.checker_id " +
		    //	  "from  CLTB_ACCOUNT_MASTER a, cltm_product b, sttm_customer c, cltb_account_apps_master d " +
		    //	  "where a.product_code = b.product_code " +
		    //	  "and a.customer_id = c.customer_no " +
		    //	  "and a.account_number = d.account_number " +
		    //	  "and a.customer_id = d.customer_id " +
		    //	  "and a.branch_code = d.branch_code " +
		    //	  "and c.record_stat = 'O' " +
		    //	  "and c.auth_stat = 'A' and d.auth_stat = 'A' ";
		    sql = "select * from (select rownum as no,a.customer_id, a.product_code, a.product_category, b.product_desc, a.book_date, " +
		          "a.maturity_date, a.branch_code, a.currency, a.account_number, a.primary_applicant_id, " +
		          "(c.customer_name1) primary_applicant_name, decode(a.account_status, 'A', 'Active', 'L', 'Liquidate', 'H', 'Hold', 'V', 'Reverse') account_status, " +
		          "a.no_of_installments, a.total_sale_value, " +
		          "a.upfront_profit_booked, a.amount_disbursed, a.amount_financed, a.maker_id, a.checker_id " +
		          "from  cltb_account_apps_master a, cltm_product b, sttm_customer c " +
		          "where a.product_code = b.product_code " +
		          "and a.customer_id = c.customer_no " +
		          "and c.record_stat IN ('O','C') " +
		          "and c.auth_stat IN ('A','U') " +
		          "and a.auth_stat = 'A' ";	
			if (!"".equals(accno) && accno != null){
				System.out.println("acc no");
				if ("active".equals(status)){
					System.out.println("if");
					sql += " and a.account_number = ? and a.account_status = 'A' ";
				} else if ("liquidate".equals(status)) {
					System.out.println("else");
					sql += " and a.account_number = ? and a.account_status = 'L' ";
				} else {
					sql += " and a.account_number = ? ";
				}
			}else if (!"".equals(cusid) && cusid != null){
				System.out.println("cusid");
				if ("active".equals(status)){
					System.out.println("if");
					sql += " and a.customer_id = ? and a.account_status = 'A' ";
				} else if ("liquidate".equals(status)) {
					System.out.println("else");
					sql += " and a.customer_id = ? and a.account_status = 'L' ";
				} else {
					sql += " and a.customer_id = ? ";
				}	
			}else if (!"".equals(cusname) && cusname != null){
				if ("active".equals(status)){
					System.out.println("if");
					sql += " and c.customer_name1 like ? and a.account_status = 'A' ";
				} else if ("liquidate".equals(status)) {
					System.out.println("else");
					sql += " and c.customer_name1 like ? and a.account_status = 'L' ";
				} else {
					sql += " and c.customer_name1 like ? ";
				}	
			}
//				sql += " and c.customer_name1 like ? ";
//			}if ("Active".equals(active)){
//				sql += " and a.account_status = 'A'";
//			}if ("Liquidate".equals(liquidate)){
//				sql += " and a.account_status = 'L'";
//			}
			sql += " order by amount_financed desc) where no >= ? and no <= ? ";
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			if (!"".equals(accno) && accno != null){
				if ("active".equals(status)){
					stat.setString(i++, accno.trim());
				} else if ("liquidate".equals(status)) {
					stat.setString(i++, accno.trim());
				} else {
					stat.setString(i++, accno.trim());
				}
			}
			if (!"".equals(cusid) && cusid != null){
				if ("active".equals(status)){
					stat.setString(i++, cusid.trim());
				} else if ("liquidate".equals(status)) {
					stat.setString(i++, cusid.trim());
				} else {
					stat.setString(i++, cusid.trim());
				}
			}
			if (!"".equals(cusname) && cusname != null){
				if ("active".equals(status)){
					stat.setString(i++, "%" + cusname.toUpperCase() + "%");
				} else if ("liquidate".equals(status)) {
					stat.setString(i++, "%" + cusname.toUpperCase() + "%");
				} else {
					stat.setString(i++, "%" + cusname.toUpperCase() + "%");
				}
			}
			System.out.println("sql instalment" + sql);
			stat.setInt(i++, begin);
			stat.setInt(i++, delta);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new Instalment();
				bb.setCustomer_id(rs.getString("customer_id"));
				bb.setProd_code(rs.getString("product_code"));
				bb.setProd_ctgry(rs.getString("product_category"));
				bb.setProd_desc(rs.getString("product_desc"));
				bb.setAccount_status(rs.getString("account_status"));
				//bb.setBook_date(rs.getString("book_date"));
				if (rs.getString("book_date") != null) {
            		bb.setBook_date(rs.getString("book_date").substring(0,10));
				} else {
					bb.setBook_date(rs.getString("book_date"));
				}
				//bb.setMatur_date(rs.getString("maturity_date"));
				if (rs.getString("maturity_date") != null) {
            		bb.setMatur_date(rs.getString("maturity_date").substring(0,10));
				} else {
					bb.setMatur_date(rs.getString("maturity_date"));
				}
				bb.setAccount_number(rs.getString("account_number"));
				bb.setPrim_app_id(rs.getString("primary_applicant_id"));
				bb.setPrim_app_name(rs.getString("primary_applicant_name"));
				bb.setNo_of_installments(rs.getString("no_of_installments"));
				//bb.setTotal_sale_value(rs.getBigDecimal("total_sale_value"));
				if (rs.getBigDecimal("total_sale_value") == null){
					bb.setTotal_sale_value2(new BigDecimal(0));
				} else {
					bb.setTotal_sale_value2(rs.getBigDecimal("total_sale_value"));
				}
				//bb.setTotal_sale_value(rs.getBigDecimal("total_sale_value"));
				if (rs.getBigDecimal("total_sale_value") == null){
					bb.setTotal_sale_value(new BigDecimal(0));
				} else {
					bb.setTotal_sale_value(rs.getBigDecimal("total_sale_value"));
				}
				//-------
				if (rs.getBigDecimal("upfront_profit_booked") == null){
					bb.setUpfront_profit_booked(new BigDecimal(0));
				} else {
					bb.setUpfront_profit_booked(rs.getBigDecimal("upfront_profit_booked"));
				}
				//bb.setAmount_disbursed(rs.getBigDecimal("amount_disbursed"));
				if (rs.getBigDecimal("amount_disbursed") == null){
					bb.setAmount_disbursed(new BigDecimal(0));
				} else {
					bb.setAmount_disbursed(rs.getBigDecimal("amount_disbursed"));
				}
				if (rs.getBigDecimal("amount_financed") == null){
					bb.setAmount_financed(new BigDecimal(0));
				} else {
					bb.setAmount_financed(rs.getBigDecimal("amount_financed"));
				}
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListCustom : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		System.out.println("lbb="+lbb);
		return lbb;
	}
    
    public List getdjadang(Instalment instl) {
//    	List jadrn = new ArrayList();
//    	Djadwalangsur jad = null;
//        Connection conn = null;
//        PreparedStatement stat = null;
//        ResultSet rs = null;
//        String sql = "";
//        
//        try {
//            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            sql = "select DISTINCT account_number, branch_code, Schedule_st_date, schedule_due_date, " +
//            	  "sum(principal) as principal, sum(profit) as profit, sum(amount_settled) as amount_setled, schedule_no " +
//            	  "from (select  account_number, branch_code, schedule_st_date, schedule_due_date, amount_settled, " +
//            	  "schedule_no, decode (component_name, 'PRINCIPAL', amount_due) as principal, " +
//            	  "decode (component_name, 'PROFIT', amount_due ) as profit " +
//            	  "from CLTB_ACCOUNT_SCHEDULES where account_number = ? " +
//            	  "and component_name in ('PRINCIPAL','PROFIT')) " +
//            	  "group by account_number, branch_code, Schedule_st_date, schedule_due_date, schedule_no " +
//            	  "order by schedule_no "; 
//            stat = conn.prepareStatement(sql);
//            stat.setString(1, instl.getAccount_number());
//            rs = stat.executeQuery();
//            BigDecimal tempTotPk = new BigDecimal(0);
//            BigDecimal tempTotMrgn = new BigDecimal(0);
//            BigDecimal tempPaid = new BigDecimal(0);
//            BigDecimal mont = new BigDecimal(12);
//            //BigDecimal tempSsPkk = instl.getAmount_disbursed();
//            BigDecimal tempSsPkk = instl.getAmount_financed();
//            while (rs.next()) {
//            	jad = new Djadwalangsur();
//            	jad.setSchedule_no(rs.getString("schedule_no"));
//            	if (rs.getString("Schedule_st_date") != null) {
//            		jad.setSchedule_st_date(rs.getString("Schedule_st_date").substring(0,10));
//				} else {
//					jad.setSchedule_st_date(rs.getString("Schedule_st_date"));
//				}
//            	if (rs.getString("schedule_due_date") != null) {
//            		jad.setSchedule_due_date(rs.getString("schedule_due_date").substring(0,10));
//				} else {
//					jad.setSchedule_due_date(rs.getString("schedule_due_date"));
//				}
//            	Djadwalangsur x = getnompaid(instl.getAccount_number(), jad.getSchedule_st_date(), jad.getSchedule_due_date());
//            	if(x!=null){
//            		jad.setNompaid(x.getNompaid());
//	            	//jad.setExec_dt(x.getExec_dt().substring(0,10));
//            	} else {
//            		jad.setNompaid(new BigDecimal(0));
//            	}
//            	jad.setPrincipal(rs.getBigDecimal("principal"));
//            	jad.setProfit(rs.getBigDecimal("profit")); 
//            	jad.setTot_angsuran(jad.getPrincipal().add(jad.getProfit()));
//            	jad.setAmount_settled(rs.getBigDecimal("amount_setled"));
//            	tempTotPk = tempTotPk.add(jad.getPrincipal());    
//            	tempPaid = tempPaid.add(jad.getNompaid());
//            	jad.setSisa_principal(instl.getAmount_financed().subtract(tempTotPk).subtract(tempPaid));
//            	tempTotMrgn = tempTotMrgn.add(jad.getProfit());
//            	jad.setSisa_profit(instl.getTempTotMrgnh().subtract(tempTotMrgn));            	
//            	jad.setSisa_angsuran(jad.getSisa_principal().add(jad.getSisa_profit()));
//            	int res;
//                res = jad.getSisa_principal().compareTo(new BigDecimal(0)); 
//                DecimalFormat myFormatter = new DecimalFormat("###,###.##");
//                BigDecimal t = new BigDecimal(0);
//            	if(res == 0){
//                	t = new BigDecimal(0);          		
//            	} else {
//	            	t = jad.getProfit().divide(tempSsPkk, MathContext.DECIMAL128);
//	            	t = t.multiply(mont).multiply(new BigDecimal(100));
//            	}
//            	tempSsPkk = jad.getSisa_principal();
//                String output = myFormatter.format(t);
//            	jad.setRate(output + "%");
//            	jadrn.add(jad);
//            }
//            
//        } catch (SQLException ex) {
//        	log.error(" getListdJadang : " + ex.getMessage());
//        } finally {
//            closeConnDb(conn, stat, rs);
//        }
//        return jadrn;
    	System.out.println("product code :" + instl.getProd_code());
    	List jadrn = new ArrayList();
    	Djadwalangsur jad = null;
    	
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            if ("D1T1".equals(instl.getProd_code()) || "D1T2".equals(instl.getProd_code())){
            	System.out.println("product code :" + instl.getProd_code());
            	System.out.println("if");
        	sql = " SELECT SCHEDULE_NO, ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, "
            		+ " SUM(PRINCIPAL) AS PRINCIPAL, SUM(PROFIT) AS PROFIT, SUM(AMOUNT_SETTLED) AS AMOUNT_SETLED, "
            		+ " NVL(SUM(UJROH),0) AS UJROH FROM (SELECT ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, "
            		+ " SCHEDULE_DUE_DATE, AMOUNT_SETTLED, SCHEDULE_NO, DECODE(COMPONENT_NAME,'PRINCIPAL', AMOUNT_DUE) AS PRINCIPAL, "
            		+ " DECODE (COMPONENT_NAME, 'PROFIT', AMOUNT_DUE ) AS PROFIT, DECODE (COMPONENT_NAME, 'UJROH', AMOUNT_DUE ) AS UJROH"
            		+ " FROM CLTB_ACCOUNT_SCHEDULES WHERE ACCOUNT_NUMBER = ? AND COMPONENT_NAME IN ('PRINCIPAL','PROFIT','UJROH')) "
            		+ " GROUP BY ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, SCHEDULE_NO ORDER BY SCHEDULE_DUE_DATE" ;
            } else {
            	System.out.println("product code :" + instl.getProd_code());
            	System.out.println("else");
        	sql = " SELECT SCHEDULE_NO, ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, "
            		+ " SUM(PRINCIPAL) AS PRINCIPAL, SUM(PROFIT) AS PROFIT, SUM(AMOUNT_SETTLED) AS AMOUNT_SETLED, "
            		+ " NVL(SUM(UJROH),0) AS UJROH FROM (SELECT ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, "
            		+ " SCHEDULE_DUE_DATE, AMOUNT_SETTLED, SCHEDULE_NO, DECODE(COMPONENT_NAME,'PRINCIPAL', AMOUNT_DUE) AS PRINCIPAL, "
            		+ " DECODE (COMPONENT_NAME, 'PROFIT', AMOUNT_DUE ) AS PROFIT, DECODE (COMPONENT_NAME, 'UJROH', AMOUNT_DUE ) AS UJROH"
            		+ " FROM CLTB_ACCOUNT_SCHEDULES WHERE ACCOUNT_NUMBER = ? AND COMPONENT_NAME IN ('PRINCIPAL','PROFIT','UJROH')) "
            		+ " GROUP BY ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, SCHEDULE_NO ORDER BY SCHEDULE_NO" ;
            }
            
            System.out.println("detail angsuran :" + sql);
            stat = conn.prepareStatement(sql);
            stat.setString(1, instl.getAccount_number());
            rs = stat.executeQuery();
            BigDecimal tempTotPk = new BigDecimal(0);
            BigDecimal tempTotMrgn = new BigDecimal(0);
            BigDecimal tempPaid = new BigDecimal(0);
            BigDecimal tempTotUjroh = new BigDecimal(0);
            BigDecimal mont = new BigDecimal(12);
            //BigDecimal tempSsPkk = instl.getAmount_disbursed();
            BigDecimal tempSsPkk = instl.getAmount_financed();
            while (rs.next()) {
            	jad = new Djadwalangsur();
            	jad.setSchedule_no(rs.getString("SCHEDULE_NO"));
            	if (rs.getString("SCHEDULE_ST_DATE") != null) {
            		jad.setSchedule_st_date(rs.getString("SCHEDULE_ST_DATE").substring(0,10));
				} else {
					jad.setSchedule_st_date(rs.getString("SCHEDULE_ST_DATE"));
				}
            	if (rs.getString("SCHEDULE_DUE_DATE") != null) {
            		jad.setSchedule_due_date(rs.getString("SCHEDULE_DUE_DATE").substring(0,10));
				} else {
					jad.setSchedule_due_date(rs.getString("SCHEDULE_DUE_DATE"));
				}
            	Djadwalangsur x = getnompaid(instl.getAccount_number(), jad.getSchedule_st_date(), jad.getSchedule_due_date());
            	if(x!=null){
            		jad.setNompaid(x.getNompaid());
	            	//jad.setExec_dt(x.getExec_dt().substring(0,10));
            	} else {
            		jad.setNompaid(new BigDecimal(0));
            	}
            	jad.setPrincipal(rs.getBigDecimal("PRINCIPAL"));
            	jad.setProfit(rs.getBigDecimal("PROFIT")); 
            	jad.setUjroh(rs.getBigDecimal("UJROH"));
            	jad.setTot_angsuran(jad.getPrincipal().add(jad.getProfit()));
            	jad.setAmount_settled(rs.getBigDecimal("AMOUNT_SETLED"));
            	tempTotPk = tempTotPk.add(jad.getPrincipal());    
            	tempPaid = tempPaid.add(jad.getNompaid());
            	tempTotUjroh = tempTotUjroh.add(jad.getUjroh());
            	jad.setSisa_principal(instl.getAmount_financed().subtract(tempTotPk).subtract(tempPaid));
            	tempTotMrgn = tempTotMrgn.add(jad.getProfit());
            	jad.setSisa_profit(instl.getTempTotMrgnh().subtract(tempTotMrgn));            	
            	jad.setSisa_angsuran(jad.getSisa_principal().add(jad.getSisa_profit()));
            	int res;
                res = jad.getSisa_principal().compareTo(new BigDecimal(0)); 
                DecimalFormat myFormatter = new DecimalFormat("###,###.##");
                BigDecimal t = new BigDecimal(0);
            	if(res == 0){
                	t = new BigDecimal(0);          		
            	} else {
	            	t = jad.getProfit().divide(tempSsPkk, MathContext.DECIMAL128);
	            	t = t.multiply(mont).multiply(new BigDecimal(100));
            	}
            	tempSsPkk = jad.getSisa_principal();
                String output = myFormatter.format(t);
            	jad.setRate(output + "%");
            	jadrn.add(jad);
            }
            
        } catch (SQLException ex) {
        	log.error(" getListdJadang : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return jadrn;
    }
  
    //tambahan
    public List getdjadangdenda(Instalment instl) {

    	System.out.println("product code :" + instl.getProd_code());
    	List jadrn = new ArrayList();
    	Djadwalangsur jad = null;
    	
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            if ("D1T1".equals(instl.getProd_code()) || "D1T2".equals(instl.getProd_code())){
            	System.out.println("product code :" + instl.getProd_code());
            	System.out.println("if");
            	sql = " SELECT SCHEDULE_NO, ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, "
                 		+ "  NVL(SUM(AMOUNT_SETTLED),0) AS AMOUNT_SETLED, "
                 		+ " SUM(DENDA) AS DENDA FROM (SELECT ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, "
                 		+ " SCHEDULE_DUE_DATE, AMOUNT_SETTLED, SCHEDULE_NO, "
                 		+ "  DECODE (COMPONENT_NAME, 'DENDA', AMOUNT_DUE ) AS DENDA"
                 		+ " FROM CLTB_ACCOUNT_SCHEDULES WHERE ACCOUNT_NUMBER = ? AND COMPONENT_NAME IN ('DENDA')) "
                 		+ " GROUP BY ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, SCHEDULE_NO ORDER BY SCHEDULE_DUE_DATE" ;          } else {
            	System.out.println("product code :" + instl.getProd_code());
            	System.out.println("else");
        	 sql = " SELECT SCHEDULE_NO, ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, "
             		+ "  NVL(SUM(AMOUNT_SETTLED),0) AS AMOUNT_SETLED, "
             		+ " SUM(DENDA) AS DENDA FROM (SELECT ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, "
             		+ " SCHEDULE_DUE_DATE, AMOUNT_SETTLED, SCHEDULE_NO, "
             		+ "  DECODE (COMPONENT_NAME, 'DENDA', AMOUNT_DUE ) AS DENDA"
             		+ " FROM CLTB_ACCOUNT_SCHEDULES WHERE ACCOUNT_NUMBER = ? AND COMPONENT_NAME IN ('DENDA')) "
             		+ " GROUP BY ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, SCHEDULE_NO ORDER BY SCHEDULE_NO" ;
            }
            
            System.out.println("detail angsuran :" + sql);
            stat = conn.prepareStatement(sql);
            stat.setString(1, instl.getAccount_number());
            rs = stat.executeQuery();
            
          //  BigDecimal tempTotBayar = new BigDecimal(0);
           BigDecimal tempTotDenda = new BigDecimal(0);
            
            
           while (rs.next()) {
            	jad = new Djadwalangsur();
            	jad.setSchedule_no(rs.getString("SCHEDULE_NO"));
            	//jad.setSchedule_no(rs.getString("DENDA"));
            	if (rs.getString("SCHEDULE_ST_DATE") != null) {
            		jad.setSchedule_st_date_2(rs.getString("SCHEDULE_ST_DATE").substring(0,10));
				} else {
					jad.setSchedule_st_date_2(rs.getString("SCHEDULE_ST_DATE"));
				}
            	if (rs.getString("SCHEDULE_DUE_DATE") != null) {
            		jad.setSchedule_due_date_2(rs.getString("SCHEDULE_DUE_DATE").substring(0,10));
				} else {
					jad.setSchedule_due_date_2(rs.getString("SCHEDULE_DUE_DATE"));
				}
            	
				jad.setDenda(rs.getBigDecimal("DENDA"));
          //  	jad.setTot_angsuran(jad.getPrincipal().add(jad.getProfit()));
            	jad.setAmount_settled_2(rs.getBigDecimal("AMOUNT_SETLED"));
				
            	//tempTotBayar = tempTotBayar.add(jad.getAmount_settled_2());    
            	tempTotDenda = tempTotDenda.add(jad.getDenda());
            	jadrn.add(jad);
            }
			         
        } catch (SQLException ex) {
        	log.error(" getListdJadang : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return jadrn;
    }
  
public Instalment gethedjadang(String acno, BigDecimal hrgpkk, BigDecimal mrgn, BigDecimal ujroh, BigDecimal denda ) {
    	
//    	Instalment hed = null;
//        Connection conn = null;
//        PreparedStatement stat = null;
//        ResultSet rs = null;
//        String sql = "";
//        
//        try {
//            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            sql = "select DISTINCT account_number, branch_code, Schedule_st_date, schedule_due_date, " +
//            	  "sum(principal) as principal, sum(profit) as profit, sum(amount_settled) as amount_setled, schedule_no " +
//            	  "from (select  account_number, branch_code, schedule_st_date, schedule_due_date, amount_settled, " +
//            	  "schedule_no, decode (component_name, 'PRINCIPAL', amount_due) as principal, " +
//            	  "decode (component_name, 'PROFIT', amount_due ) as profit " +
//            	  "from CLTB_ACCOUNT_SCHEDULES where account_number = ? " +
//            	  "and component_name in ('PRINCIPAL','PROFIT')) " +
//            	  "group by account_number, branch_code, Schedule_st_date, schedule_due_date, schedule_no " +
//            	  "order by schedule_no "; 
//            stat = conn.prepareStatement(sql);
//            stat.setString(1, acno);
//            rs = stat.executeQuery();
//            BigDecimal tempTotPkh = new BigDecimal(0);
//            BigDecimal tempTotMrgnh = new BigDecimal(0);
//            BigDecimal tempPaidh = new BigDecimal(0);
//        	hed = new Instalment();
//        	BigDecimal tt = new BigDecimal(0);
//            while (rs.next()) {
//            	if(rs.getBigDecimal("principal") == null){
//            		tt = new BigDecimal(0);
//            	} else {
//            		tt = rs.getBigDecimal("principal");
//            	}
//            	tempTotPkh = tempTotPkh.add(tt);
//            	if(rs.getBigDecimal("profit") == null){
//            		tt = new BigDecimal(0);
//            	} else {
//            		tt = rs.getBigDecimal("profit");
//            	}
//            	tempTotMrgnh = tempTotMrgnh.add(tt);
//            	tempPaidh = tempTotPkh.add(tempTotMrgnh);
//            	
//            }
//            hed.setTempTotPkh(tempTotPkh);
//            hed.setTempTotMrgnh(tempTotMrgnh);
//            hed.setTempPaidh(tempPaidh);
//            System.out.println("sql gethedjadang :" + sql);
//        } catch (SQLException ex) {
//        	log.error(" getListhedjadang : " + ex.getMessage());
//        } finally {
//            closeConnDb(conn, stat, rs);
//        }
//        return hed;
	
		Instalment hed = null;
	    Connection conn = null;
	    PreparedStatement stat = null;
	    ResultSet rs = null;
	    String sql = "";
	    
	    try {
	        conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
	        /*sql = "SELECT SCHEDULE_NO, ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, NVL(SUM(UJROH),0) AS UJROH,"
	        		+ " SUM(PRINCIPAL) AS PRINCIPAL,SUM(PROFIT) AS PROFIT, SUM(AMOUNT_SETTLED) AS AMOUNT_SETLED"
	        		+ " FROM (SELECT ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, AMOUNT_SETTLED,"
	        		+ " SCHEDULE_NO, DECODE (COMPONENT_NAME, 'PRINCIPAL', AMOUNT_DUE) AS PRINCIPAL,"
	        		+ " DECODE (COMPONENT_NAME, 'PROFIT', AMOUNT_DUE) AS PROFIT, DECODE(COMPONENT_NAME,'UJROH',AMOUNT_DUE) AS UJROH"
	        		+ " FROM CLTB_ACCOUNT_SCHEDULES WHERE ACCOUNT_NUMBER = ? AND COMPONENT_NAME IN ('PRINCIPAL','PROFIT','UJROH'))"
	        		+ " GROUP BY ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, SCHEDULE_NO ORDER BY SCHEDULE_NO ";
	        	*/	
	        
	        sql = "SELECT SCHEDULE_NO, ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, NVL(SUM(UJROH),0) AS UJROH,"
	        		+ " NVL(SUM(DENDA),0) AS DENDA,SUM(PRINCIPAL) AS PRINCIPAL,SUM(PROFIT) AS PROFIT, SUM(AMOUNT_SETTLED) AS AMOUNT_SETLED"
	        		+ " FROM (SELECT ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, AMOUNT_SETTLED,"
	        		+ " SCHEDULE_NO, DECODE (COMPONENT_NAME, 'PRINCIPAL', AMOUNT_DUE) AS PRINCIPAL,"
	        		+ " DECODE (COMPONENT_NAME, 'PROFIT', AMOUNT_DUE) AS PROFIT, DECODE(COMPONENT_NAME,'UJROH',AMOUNT_DUE) AS UJROH,DECODE (COMPONENT_NAME, 'DENDA', AMOUNT_DUE) AS DENDA"
	        		+ " FROM CLTB_ACCOUNT_SCHEDULES WHERE ACCOUNT_NUMBER = ? AND COMPONENT_NAME IN ('PRINCIPAL','PROFIT','UJROH','DENDA'))"
	        		+ " GROUP BY ACCOUNT_NUMBER, BRANCH_CODE, SCHEDULE_ST_DATE, SCHEDULE_DUE_DATE, SCHEDULE_NO ORDER BY SCHEDULE_NO ";
	        
	        System.out.println("sql gethedjadang :" + sql);
	        stat = conn.prepareStatement(sql);
	        stat.setString(1, acno);
	        rs = stat.executeQuery();
	        BigDecimal tempTotPkh = new BigDecimal(0);
	        BigDecimal tempTotMrgnh = new BigDecimal(0);
	        BigDecimal tempPaidh = new BigDecimal(0);
	        BigDecimal tempTotUjroh = new BigDecimal(0);
	        BigDecimal tempTotDenda = new BigDecimal(0);
	    	hed = new Instalment();
	    	BigDecimal tt = new BigDecimal(0);
	        while (rs.next()) {
	        	if(rs.getBigDecimal("PRINCIPAL") == null){
	        		tt = new BigDecimal(0);
	        	} else {
	        		tt = rs.getBigDecimal("PRINCIPAL");
	        	}
	        	tempTotPkh = tempTotPkh.add(tt);
	        	System.out.println("tempTotPkh:" + tempTotPkh);
	        	if(rs.getBigDecimal("PROFIT") == null){
	        		tt = new BigDecimal(0);
	        	} else {
	        		tt = rs.getBigDecimal("PROFIT");
	        	}
	        	tempTotMrgnh = tempTotMrgnh.add(tt);
	        	System.out.println("tempTotMrgnh:" + tempTotMrgnh);
	        	tempPaidh = tempTotPkh.add(tempTotMrgnh);
	        	System.out.println("tempPaidh:" + tempPaidh);
	        	if (rs.getBigDecimal("UJROH") == null){
	        		tt = new BigDecimal(0);
	        	} else {
	        		tt = rs.getBigDecimal("UJROH");
	        	}
	        	tempTotUjroh = tempTotUjroh.add(tt);
	        	System.out.println("tempTotUjroh:" + tempTotUjroh);
	        	if (rs.getBigDecimal("DENDA") == null){
	        		tt = new BigDecimal(0);
	        	} else {
	        		tt = rs.getBigDecimal("DENDA");
	        	}
	        	tempTotDenda = tempTotDenda.add(tt);
	        	System.out.println("tempTotDenda:" + tempTotDenda);
	        }
	        hed.setTempTotPkh(tempTotPkh);
	        hed.setTempTotMrgnh(tempTotMrgnh);
	        hed.setTempPaidh(tempPaidh);
	        hed.setTempTotUjroh(tempTotUjroh);
	        hed.setTempTotDenda(tempTotDenda);
	        System.out.println("sql gethedjadang :" + sql);
	    } catch (SQLException ex) {
	    	log.error(" getListhedjadang : " + ex.getMessage());
	    } finally {
	        closeConnDb(conn, stat, rs);
	    }
	    return hed;
    }
    
    
    public Djadwalangsur getnompaid(String nokartu, String tgawal, String tgakhir) {
    	Djadwalangsur jad = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            sql = "select a.account_number, a.branch_code, a.paid_date, sum(a.nominal) nominal " +
/*      	  "from (select account_number, branch_code,  exec_date, paid_date, SUM(amount_paid) as nominal " +
          "from cltb_amount_paid where account_number = ? " +
          "and paid_date > to_date(?, 'YYYY-MM-DD') " +
          "and exec_date <= to_date(?, 'YYYY-MM-DD') " +
          "and component_name = 'PRINCIPAL' and paid_status = 'P' " +
          "group by exec_date, paid_date, account_number, branch_code)a " +
          "group by a.paid_date, a.account_number, a.branch_code "*/;
            sql = "select a.account_number, a.branch_code, sum(a.nominal) nominal " +
            	  "from (select account_number, branch_code,  exec_date, paid_date, SUM(amount_paid) as nominal " +
            	          "from cltb_amount_paid where account_number = ? " +
            	          "and exec_date > to_date(?, 'YYYY-MM-DD') " +
            	          "and exec_date <= to_date(?, 'YYYY-MM-DD') " +
            	          "and component_name = 'PRINCIPAL' and paid_status = 'P' " +
            	          "group by exec_date, paid_date, account_number, branch_code)a " +
            	          "group by a.account_number, a.branch_code ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, nokartu);
            stat.setString(2, tgawal);
            stat.setString(3, tgakhir);
            rs = stat.executeQuery();
            while (rs.next()) {
            	jad = new Djadwalangsur();
            	if (rs.getBigDecimal("nominal")==null){
            		jad.setNompaid(new BigDecimal(0));
            	}else {
            		jad.setNompaid(rs.getBigDecimal("nominal"));
            	}
            	//jad.setExec_dt(rs.getString("exec_date"));
            }
            
        } catch (SQLException ex) {
        	log.error(" Djadwalangsur : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return jad;
    }
    
    public List getdtljadang(String account_no,String start,String end) {
   	 	List jdwl = new ArrayList();
   	    Djadwalangsur trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
             sql = "select account_number, branch_code, exec_date, sum(amount_paid) nominal " +
             	   "from cltb_amount_paid where account_number = ? " +
             	   "and paid_date >= to_date(?, 'YYYY-MM-DD') " +
             	   "and exec_date <= to_date(?, 'YYYY-MM-DD') " +
             	   "and component_name = 'PRINCIPAL' and paid_status = 'P' " +
             	   "group by exec_date, account_number, branch_code ";
            stat = conn.prepareStatement(sql);
            stat.setString(1, account_no);
            stat.setString(2, start);
            stat.setString(3, end);
   	        rs = stat.executeQuery();
   	        while (rs.next()) {
   		       	 trn = new Djadwalangsur();
   		       	 trn.setAccount_number(rs.getString("account_number"));
   		       	 trn.setBranch_code(rs.getString("branch_code"));
   		       	 if (rs.getString("exec_date") != null){
   		    	  		trn.setExec_date(rs.getString("exec_date").substring(0,10));
		       		}else{
		       			trn.setExec_date(rs.getString("exec_date"));;
		       		}
   		       	 if (rs.getString("nominal") != null){
   		       		 	trn.setNominal(rs.getString("nominal"));
   		       	 	}else{
   		       	 		trn.setNominal(rs.getString(0));;
   		       	 	}
   		      jdwl.add(trn);
   		    }
   	    } catch (SQLException ex) {
   	   	 	log.error(" get Djadwalangsur : " + ex.getMessage());
   	    } finally {
   	        closeConnDb(conn, stat, rs);
   	    }
   	    return jdwl;
   	}
    
    public List getFcltList(String liabno, String liabname, String linecode) {
        List ltrn = new ArrayList();
        GetmFaclt trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select  distinct a.id, a.liab_id, b.pool_id, d.liab_no, d.liab_name, a.line_code, a.line_serial, " +
	  		  	  "a.line_currency, a.line_start_date, a.line_expiry_date, a.limit_amount, a.collateral_contribution, " +
	  		  	  "a.available_amount, a.date_of_first_od, a.date_of_last_od, a.amount_utilised_today, " +
	  		  	  "a.amount_reinstated_today, a.utilisation, a.brn, a.transfer_amount, a.last_new_util_date, " +
	  		  	  "a.dsp_eff_line_amount, a.block_amount, a.uncollected_amount, a.category, " +
	  		  	  "a.maker_id, a.checker_id, " +
	  		  	  "(case when a.availability_flag ='Y' then 'AVAILABLE' else 'UNAVAILABLE' end) status_av, " +
	  		  	  "a.tanked_util, a.netting_amount, a.description, a.revolving_amt, a.approved_amt, " +
	  		  	  "(case when a.record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
	  		  	  "(case when a.auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
	  		  	  "from getm_facility a, getb_pool_link b, getm_pool c, getm_liab d " +
	  		  	  "where a.id = b.facility_id and b.pool_id = c.id " +
	  		  	  "and c.liab_id = d.id ";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                trn = new GetmFaclt();
                trn.setLiab_no(rs.getString("liab_no"));
  		         trn.setLiab_name(rs.getString("liab_name"));
  		       	 trn.setLiab_id(rs.getString("liab_id"));
  		       	 trn.setPool_id(rs.getString("pool_id"));
	             trn.setLine_code(rs.getString("line_code"));
	             trn.setLine_serial(rs.getString("line_serial"));
	             trn.setLine_currency(rs.getString("line_currency"));
	             //trn.setLine_start_date(rs.getString("line_start_date"));
	             if (rs.getString("line_start_date") != null) {
						trn.setLine_start_date(rs.getString("line_start_date").substring(0,10));
					} else {
						trn.setLine_start_date(rs.getString("line_start_date"));
					}
	             //trn.setLine_expiry_date(rs.getString("line_expiry_date"));
	             if (rs.getString("line_expiry_date") != null) {
						trn.setLine_expiry_date(rs.getString("line_expiry_date").substring(0,10));
					} else {
						trn.setLine_expiry_date(rs.getString("line_expiry_date"));
					}
	             trn.setLimit_amount(rs.getString("limit_amount")); 
	             trn.setCollateral_contribution(rs.getString("collateral_contribution"));
		         trn.setAvailable_amount(rs.getString("available_amount"));
		         //trn.setDate_of_first_od(rs.getString("date_of_first_od"));
		         if (rs.getString("date_of_first_od") != null) {
						trn.setDate_of_first_od(rs.getString("date_of_first_od").substring(0,10));
					} else {
						trn.setDate_of_first_od(rs.getString("date_of_first_od"));
					}
		         //trn.setDate_of_last_od(rs.getString("date_of_last_od"));
		         if (rs.getString("date_of_last_od") != null) {
						trn.setDate_of_last_od(rs.getString("date_of_last_od").substring(0,10));
					} else {
						trn.setDate_of_last_od(rs.getString("date_of_last_od"));
					}
		         trn.setAmount_utilised_today(rs.getString("amount_utilised_today"));
		         trn.setAmount_reinstated_today(rs.getString("amount_reinstated_today"));
		         trn.setUtilisation(rs.getString("utilisation"));
		         trn.setBrn(rs.getString("brn"));
		         trn.setDsp_eff_line_amount(rs.getString("dsp_eff_line_amount"));
		         trn.setBlock_amount(rs.getString("block_amount"));
		         trn.setTanked_util(rs.getString("tanked_util"));
		         trn.setNetting_amount(rs.getString("netting_amount"));
		         trn.setDescription(rs.getString("description"));
		         trn.setRevolving_amt(rs.getString("revolving_amt"));
		         trn.setDescription(rs.getString("description"));
		         trn.setApproved_amt(rs.getString("approved_amt"));
		         trn.setStatus_rcd(rs.getString("status_rcd"));
		         trn.setStatus_oto(rs.getString("status_oto"));
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public List sGetmFcltList(String liabno, String liabname, String linecode) {
   	 	List ltrn = new ArrayList();
   	 	GetmFaclt trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
       	 if ((liabno==null ) && (liabname==null ) && (linecode==null )){
       		 return ltrn;
       	 }
       	 conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
            int i = 1;
//            sql = "select  distinct a.id, a.liab_id, b.pool_id, d.liab_no, d.liab_name, a.line_code, a.line_serial, " +
//	  		      "a.line_currency, a.line_start_date, a.line_expiry_date, a.limit_amount, a.collateral_contribution, " +
//	  		      "a.available_amount, a.date_of_first_od, a.date_of_last_od, a.amount_utilised_today, " +
//	  		      "a.amount_reinstated_today, a.utilisation, a.brn, a.transfer_amount, a.last_new_util_date, " +
//	  		      "a.dsp_eff_line_amount, a.block_amount, a.uncollected_amount, a.category, " +
//	  		      "a.maker_id, a.checker_id, " +
//	  		      "(case when a.availability_flag ='Y' then 'AVAILABLE' else 'UNAVAILABLE' end) status_av, " +
//	  		      "a.tanked_util, a.netting_amount, a.description, a.revolving_amt, a.approved_amt, " +
//	  		      "(case when a.record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
//	  		      "(case when a.auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
//	  		      "from getm_facility a, getb_pool_link b, getm_pool c, getm_liab d " +
//	  		      "where a.id = b.facility_id and b.pool_id = c.id " +
//	  		      "and c.liab_id = d.id ";
             sql = "select  a.id, a.liab_id, b.pool_id, a.liab_no, a.liab_name, a.line_code, a.line_serial, " +
             	   "a.line_currency, a.line_start_date, a.line_expiry_date, a.limit_amount, a.collateral_contribution, " +
             	   "a.available_amount, a.date_of_first_od, a.date_of_last_od, a.amount_utilised_today, " +
             	   "a.amount_reinstated_today, a.utilisation, a.brn, a.transfer_amount, a.last_new_util_date, " +
             	   "a.dsp_eff_line_amount, a.block_amount, a.uncollected_amount, a.category, " +
             	   "a.maker_id, a.checker_id, " +
             	   "(case when a.availability_flag ='Y' then 'AVAILABLE' else 'UNAVAILABLE' end) status_av, " +
             	   "a.tanked_util, a.netting_amount, a.description, a.revolving_amt, a.approved_amt, " +
             	   "(case when a.record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
             	   "(case when a.auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
             	   "from ( select a.*,  d.liab_no, d.liab_name " +
             	   			"from getm_facility a, getm_liab d " +
             	   			"where a.liab_id = d.id) a , getb_pool_link b, getm_pool c " +
             	   "where a.id  = b.facility_id(+) " +
             	   "and b.pool_id = c.id(+) ";
             	   
            if(liabno!=null && !"".equals(liabno)){
      	      sql += " and a.liab_no = ? ";
      	    }
            if(liabname!=null && !"".equals(liabname) ){
   	       	  sql += " and a.liab_name = ? ";
   	        }
            if(linecode!=null && !"".equals(linecode) ){
      	      sql += " and a.line_code = ? ";
      	    }
   	        stat = conn.prepareStatement(sql);
   	        if(liabno!=null && !"".equals(liabno)){
	            stat.setString(i++,liabno);
	        }
   	        if(liabname!=null && !"".equals(liabname)){
   	            stat.setString(i++,liabname);
   	        }
   	        if(linecode!=null && !"".equals(linecode)){
   	            stat.setString(i++,linecode);
   	        }
   	        rs = stat.executeQuery();
   	        while (rs.next()) {
   		       	 trn = new GetmFaclt();
   		       	 trn.setLiab_no(rs.getString("liab_no"));
   		         trn.setLiab_name(rs.getString("liab_name"));
   		       	 trn.setLiab_id(rs.getString("liab_id"));
   		       	 trn.setPool_id(rs.getString("pool_id"));
	             trn.setLine_code(rs.getString("line_code"));
	             trn.setLine_serial(rs.getString("line_serial"));
	             trn.setLine_currency(rs.getString("line_currency"));
	             //trn.setLine_start_date(rs.getString("line_start_date"));
	             if (rs.getString("line_start_date") != null) {
						trn.setLine_start_date(rs.getString("line_start_date").substring(0,10));
					} else {
						trn.setLine_start_date(rs.getString("line_start_date"));
					}
	             //trn.setLine_expiry_date(rs.getString("line_expiry_date"));
	             if (rs.getString("line_expiry_date") != null) {
						trn.setLine_expiry_date(rs.getString("line_expiry_date").substring(0,10));
					} else {
						trn.setLine_expiry_date(rs.getString("line_expiry_date"));
					}
	             trn.setLimit_amount(rs.getString("limit_amount")); 
	             trn.setCollateral_contribution(rs.getString("collateral_contribution"));
		         trn.setAvailable_amount(rs.getString("available_amount"));
		         //trn.setDate_of_first_od(rs.getString("date_of_first_od"));
		         if (rs.getString("date_of_first_od") != null) {
						trn.setDate_of_first_od(rs.getString("date_of_first_od").substring(0,10));
					} else {
						trn.setDate_of_first_od(rs.getString("date_of_first_od"));
					}
		         //trn.setDate_of_last_od(rs.getString("date_of_last_od"));
		         if (rs.getString("date_of_last_od") != null) {
						trn.setDate_of_last_od(rs.getString("date_of_last_od").substring(0,10));
					} else {
						trn.setDate_of_last_od(rs.getString("date_of_last_od"));
					}
		         trn.setAmount_utilised_today(rs.getString("amount_utilised_today"));
		         trn.setAmount_reinstated_today(rs.getString("amount_reinstated_today"));
		         trn.setUtilisation(rs.getString("utilisation"));
		         trn.setBrn(rs.getString("brn"));
		         trn.setDsp_eff_line_amount(rs.getString("dsp_eff_line_amount"));
		         trn.setBlock_amount(rs.getString("block_amount"));
		         trn.setTanked_util(rs.getString("tanked_util"));
		         trn.setNetting_amount(rs.getString("netting_amount"));
		         trn.setDescription(rs.getString("description"));
		         trn.setRevolving_amt(rs.getString("revolving_amt"));
		         trn.setApproved_amt(rs.getString("approved_amt"));
		         trn.setStatus_rcd(rs.getString("status_rcd"));
		         trn.setStatus_oto(rs.getString("status_oto"));
		         trn.setMaker_id(rs.getString("maker_id"));
		         trn.setChecker_id(rs.getString("checker_id"));
				 ltrn.add(trn);
   	        }
   	    } catch (SQLException ex) {
   	   	 	log.error(" get getListMCBMon : " + ex.getMessage());
   	    } finally {
   	        closeConnDb(conn, stat, rs);
   	    }
   	    return ltrn;
   	}
    
    public GetmFaclt getvalfacil(String liabno, String linecode, String lineserial) {
   	 	List faclrn = new ArrayList();
   	 	GetmFaclt trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//            sql = "select  distinct a.id, a.liab_id, b.pool_id, d.liab_no, d.liab_name, a.line_code, a.line_serial, " +
//      	  		  "a.line_currency, a.line_start_date, a.line_expiry_date, a.limit_amount, a.collateral_contribution, " +
//      	          "a.available_amount, a.date_of_first_od, a.date_of_last_od, a.amount_utilised_today, " +
//      	          "a.amount_reinstated_today, a.utilisation, a.brn, a.transfer_amount, a.last_new_util_date, " +
//      	          "a.dsp_eff_line_amount, a.block_amount, a.uncollected_amount, a.category, " +
//      	          "a.maker_id, a.checker_id, " +
//      	          "c.pool_code, c.pool_description, c.pool_ccy, c.pool_amount, c.pool_util, " +
//      	          "b.facility_branch_code, b.percentage_of_pool, b.facility_amount_pool_ccy, b.facility_currency, " +
//      	          "b.facility_amount, b.type, b.percentage_of_contract, " +
//      	          "(case when a.availability_flag ='Y' then 'AVAILABLE' else 'UNAVAILABLE' end) status_av, " +
//      	          "a.tanked_util, a.netting_amount, a.description, a.revolving_amt, a.approved_amt, " +
//      	          "(case when a.record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
//      	          "(case when a.auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
//      	          "from getm_facility a, getb_pool_link b, getm_pool c, getm_liab d " +
//      	          "where a.id = b.facility_id and b.pool_id = c.id " +
//      	          "and c.liab_id = d.id and d.liab_no = ? and a.line_code = ? ";
            
             sql = "select  a.id, a.liab_id, b.pool_id, a.liab_no, a.liab_name, a.line_code, a.line_serial, " +
       	   		   "a.line_currency, a.line_start_date, a.line_expiry_date, a.limit_amount, a.collateral_contribution, " +
     	           "a.available_amount, a.date_of_first_od, a.date_of_last_od, a.amount_utilised_today, " +
     	           "a.amount_reinstated_today, a.utilisation, a.brn, a.transfer_amount, a.last_new_util_date, " +
     	           "a.dsp_eff_line_amount, a.block_amount, a.uncollected_amount, a.category, " +
     	           "a.maker_id, a.checker_id, " +
     	           "c.pool_code, c.pool_description, c.pool_ccy, c.pool_amount, c.pool_util, " +
     	           "b.facility_branch_code, b.percentage_of_pool, b.facility_amount_pool_ccy, b.facility_currency, " +
     	           "b.facility_amount, b.type, b.percentage_of_contract, " +
     	           "(case when a.availability_flag ='Y' then 'AVAILABLE' else 'UNAVAILABLE' end) status_av, " +
     	           "a.tanked_util, a.netting_amount, a.description, a.revolving_amt, a.approved_amt, " +
     	           "(case when a.record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
     	           "(case when a.auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
     	           "from ( select a.*,  d.liab_no, d.liab_name " +
     	   				"from getm_facility a, getm_liab d " +
     	   		   "where a.liab_id = d.id) a , getb_pool_link b, getm_pool c " +
     	   		   "where a.id  = b.facility_id(+) " +
     	   		   "and b.pool_id = c.id(+) " +
     	   		   "and a.liab_no = ? and a.line_code = ? and a.line_serial = ? ";
             System.out.println("SQL Detail liab :" + sql);
            stat = conn.prepareStatement(sql);
            stat.setString(1, liabno);
            stat.setString(2, linecode);
            stat.setString(3, lineserial);
   	        rs = stat.executeQuery();
   	        while (rs.next()) {
   		       	 trn = new GetmFaclt();
   		       	 trn.setLiab_no(rs.getString("liab_no"));
   		         trn.setLiab_name(rs.getString("liab_name"));
   		       	 trn.setLiab_id(rs.getString("liab_id"));
   		       	 trn.setPool_id(rs.getString("pool_id"));
	             trn.setLine_code(rs.getString("line_code"));
	             trn.setLine_serial(rs.getString("line_serial"));
	             trn.setLine_currency(rs.getString("line_currency"));
	             //trn.setLine_start_date(rs.getString("line_start_date"));
	             if (rs.getString("line_start_date") != null) {
						trn.setLine_start_date(rs.getString("line_start_date").substring(0,10));
					} else {
						trn.setLine_start_date(rs.getString("line_start_date"));
					}
	             //trn.setLine_expiry_date(rs.getString("line_expiry_date"));
	             if (rs.getString("line_expiry_date") != null) {
						trn.setLine_expiry_date(rs.getString("line_expiry_date").substring(0,10));
					} else {
						trn.setLine_expiry_date(rs.getString("line_expiry_date"));
					}
	             trn.setLimit_amount(rs.getString("limit_amount")); 
	             trn.setTransfer_amount(rs.getString("transfer_amount"));
	             trn.setUncollected_amount(rs.getString("uncollected_amount"));
	             trn.setCategory(rs.getString("category"));
	             trn.setStatus_av(rs.getString("status_av"));
	             //trn.setLast_new_util_date(rs.getString("last_new_util_date"));
	             if (rs.getString("last_new_util_date") != null) {
						trn.setLast_new_util_date(rs.getString("last_new_util_date").substring(0,10));
					} else {
						trn.setLast_new_util_date(rs.getString("last_new_util_date"));
					}
	             trn.setCollateral_contribution(rs.getString("collateral_contribution"));
		         trn.setAvailable_amount(rs.getString("available_amount"));
		         //trn.setDate_of_first_od(rs.getString("date_of_first_od"));
		         if (rs.getString("date_of_first_od") != null) {
						trn.setDate_of_first_od(rs.getString("date_of_first_od").substring(0,10));
					} else {
						trn.setDate_of_first_od(rs.getString("date_of_first_od"));
					}
		         //trn.setDate_of_last_od(rs.getString("date_of_last_od"));
		         if (rs.getString("date_of_last_od") != null) {
						trn.setDate_of_last_od(rs.getString("date_of_last_od").substring(0,10));
					} else {
						trn.setDate_of_last_od(rs.getString("date_of_last_od"));
					}
		         trn.setAmount_utilised_today(rs.getString("amount_utilised_today"));
		         trn.setAmount_reinstated_today(rs.getString("amount_reinstated_today"));
		         trn.setUtilisation(rs.getString("utilisation"));
		         trn.setBrn(rs.getString("brn"));
		         trn.setDsp_eff_line_amount(rs.getString("dsp_eff_line_amount"));
		         trn.setBlock_amount(rs.getString("block_amount"));
		         trn.setTanked_util(rs.getString("tanked_util"));
		         trn.setNetting_amount(rs.getString("netting_amount"));
		         trn.setDescription(rs.getString("description"));
		         trn.setRevolving_amt(rs.getString("revolving_amt"));
		         trn.setDescription(rs.getString("description"));
		         trn.setApproved_amt(rs.getString("approved_amt"));
		         trn.setStatus_rcd(rs.getString("status_rcd"));
		         trn.setStatus_oto(rs.getString("status_oto"));
		         trn.setPool_code(rs.getString("pool_code")); 
		         trn.setPool_description(rs.getString("pool_description"));
		         trn.setPool_ccy(rs.getString("pool_ccy")); 
		         trn.setPool_amount(rs.getString("pool_amount"));
		         trn.setPool_util(rs.getString("pool_util"));
		         trn.setFacility_branch_code(rs.getString("facility_branch_code"));
		         //trn.setPercentage_of_pool(rs.getString("percentage_of_pool"));
		         if (rs.getString("percentage_of_pool") != null) {
						trn.setPercentage_of_pool(rs.getString("percentage_of_pool")+" %");
					} else {
						trn.setPercentage_of_pool(rs.getString("percentage_of_pool"));
					}
		         trn.setFacility_amount_pool_ccy(rs.getString("facility_amount_pool_ccy"));
		         trn.setFacility_currency(rs.getString("facility_currency"));
		         trn.setFacility_amount(rs.getString("facility_amount"));
		         trn.setType(rs.getString("type"));
		         //trn.setPercentage_of_contract(rs.getString("percentage_of_contract"));
		         if (rs.getString("percentage_of_contract") != null) {
						trn.setPercentage_of_contract(rs.getString("percentage_of_contract")+" %");
					} else {
						trn.setPercentage_of_contract(rs.getString("percentage_of_contract"));
					}
		         //faclrn.add(trn);
   	        }
   	    } catch (SQLException ex) {
   	   	 	log.error(" get getListMCBMon : " + ex.getMessage());
   	    } finally {
   	        closeConnDb(conn, stat, rs);
   	    }
   	    return trn;
   	}
    
    
    
    public List getListLiab(String liabno, String liabname) {
        List ltrn = new ArrayList();
        Liability trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
            conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql = "select id, liab_no, liab_name, process_no, liab_branch, liab_ccy, util_amt, overall_limit, " +
            	  "category, maker_id, checker_id, " +
            	  "(case when record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
            	  "(case when auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
            	  "from getm_liab ";
            stat = conn.prepareStatement(sql);
            rs = stat.executeQuery();
            
            while (rs.next()) {
                 trn = new Liability();
                 trn.setId(rs.getString("id"));
  		         trn.setLiab_no(rs.getString("liab_no"));
  		       	 trn.setLiab_name(rs.getString("liab_name"));
  		       	 trn.setProcess_no(rs.getString("process_no"));
	             trn.setLiab_branch(rs.getString("liab_branch"));
	             trn.setLiab_ccy(rs.getString("liab_ccy"));
	             trn.setUtil_amt(rs.getString("util_amt"));
	             trn.setOverall_limit(rs.getString("overall_limit")); 
	             trn.setCategory(rs.getString("category"));
		         trn.setMaker_id(rs.getString("maker_id"));
		         trn.setChecker_id(rs.getString("checker_id"));
		         trn.setStatus_rcd(rs.getString("status_rcd"));
		         trn.setStatus_oto(rs.getString("status_oto"));
		         
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getListRTGSFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    public List sGetmLiabList(String liabno, String liabname) {
   	 	List ltrn = new ArrayList();
   	    Liability trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
        
        try {
       	 if ((liabno==null ) && (liabname==null )){
       		 return ltrn;
       	 }
       	 conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();  
            int i = 1;
            sql = "select id, liab_no, liab_name, process_no, liab_branch, liab_ccy, util_amt, overall_limit, " +
      	  		  "category, maker_id, checker_id, user_define_status, " +
      	  		  "(case when record_stat ='O' then 'OPEN' else 'CLOSE' end) status_rcd, " +
      	  		  "(case when auth_stat ='A' then 'AUTHORIZE' else 'UNAUTHORIZE' end) status_oto " +
      	  		  "from getm_liab where auth_stat ='A' ";
            if(liabno!=null && !"".equals(liabno)){
      	      sql += " and liab_no = ? ";
      	    }
            if(liabname!=null && !"".equals(liabname) ){
   	       	  sql += " and liab_name = ? ";
   	        }
            stat = conn.prepareStatement(sql);
   	        if(liabno!=null && !"".equals(liabno)){
	            stat.setString(i++,liabno);
	        }
   	        if(liabname!=null && !"".equals(liabname)){
   	            stat.setString(i++,liabname);
   	        }
   	        rs = stat.executeQuery();
   	        while (rs.next()) {
   	        	 trn = new Liability();
   	        	 trn.setId(rs.getString("id"));
 		         trn.setLiab_no(rs.getString("liab_no"));
 		       	 trn.setLiab_name(rs.getString("liab_name"));
 		       	 trn.setProcess_no(rs.getString("process_no"));
	             trn.setLiab_branch(rs.getString("liab_branch"));
	             trn.setLiab_ccy(rs.getString("liab_ccy"));
	             trn.setUtil_amt(rs.getString("util_amt"));
	             trn.setOverall_limit(rs.getString("overall_limit")); 
	             trn.setCategory(rs.getString("category"));
		         trn.setMaker_id(rs.getString("maker_id"));
		         trn.setUser_define_status(rs.getString("user_define_status"));
		         trn.setChecker_id(rs.getString("checker_id"));
		         trn.setStatus_rcd(rs.getString("status_rcd"));
		         trn.setStatus_oto(rs.getString("status_oto"));
				 ltrn.add(trn);
   	        }
   	    } catch (SQLException ex) {
   	   	 	log.error(" get getListMCBMon : " + ex.getMessage());
   	    } finally {
   	        closeConnDb(conn, stat, rs);
   	    }
   	    return ltrn;
   	}
    
    public int TotEvent(String accno){
    	int tot = 1;
    	List ltrn = new ArrayList();
		Event trn = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = "select count(*) as count from (SELECT  b.event_sr_no, a.event_seq_no, b.ac_entry_sr_no, a.event_entry_sr_no, b.module, " +
		    	  "b.ac_no, a.branch_code, a.product_code, a.value_date, a.schedule_due_date, a.ccy,a.account_number, a.event_code,a.amount, " +
		    	  "a.amount_settled, a.amount_tag, a.from_status,a.to_status, a.component_name, b.drcr_ind, " +
		    	  "DECODE(b.drcr_ind,'D',a.drtrnrefno, a.crtrnrefno) AS REFNO,DECODE (b.drcr_ind, 'D', a.dr_acc, a.cr_acc) AS ACC, " +
		    	  "DECODE(b.drcr_ind,'D',DECODE((SELECT GL_desc FROM gltm_glmaster " +
		    	   									"WHERE gl_code = a.dr_acc), '', (SELECT ac_desc " +
		    	   									"FROM sttm_cust_account WHERE cust_ac_no = a.dr_acc), " +
		    	   									"(SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.dr_acc)), " +
		    	   									"DECODE ((SELECT GL_desc FROM gltm_glmaster " +
		    	   									"WHERE gl_code = a.cr_acc), '', (SELECT ac_desc " +
		    	   									"FROM sttm_cust_account WHERE cust_ac_no = a.cr_acc), " +
		    	   									"(SELECT GL_desc FROM gltm_glmaster " +
		    	   									"WHERE gl_code = a.cr_acc))) AS AC_DESC, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_trn_code, a.cr_trn_code) AS TRN_CODE, " +
		    	   "DECODE (b.drcr_ind,'D', (SELECT trn_desc FROM sttm_trn_code WHERE dr_trn_code = trn_code), " +
		    	   					"(SELECT trn_desc FROM sttm_trn_code WHERE a.cr_trn_code = trn_code)) AS TRN_DESC, " +
		    	   					"DECODE (b.drcr_ind, 'D', a.dr_acc_role, a.cr_acc_role) AS ACC_ROLE " +
		    	   "FROM   cltb_event_entries a, actb_daily_log b " +
		    	   "WHERE   DELETE_STAT <> 'D' and (a.drtrnrefno = b.trn_ref_no OR a.crtrnrefno = b.trn_ref_no) " +
		    	   "AND b.module = 'CI' AND a.value_date = b.value_dt " +
		    	   "AND a.branch_code = b.ac_branch " +
		    	   "UNION ALL " +
		    	   "SELECT  b.event_sr_no, a.event_seq_no, b.ac_entry_sr_no, a.event_entry_sr_no, b.module, " +
		    	   "b.ac_no, a.branch_code, a.product_code, a.value_date, a.schedule_due_date, a.ccy, " +
		    	   "a.account_number, a.event_code, a.amount, a.amount_settled, a.amount_tag, a.from_status, " +
		    	   "a.to_status, a.component_name, b.drcr_ind, " +
		    	   "DECODE (b.drcr_ind, 'D', a.drtrnrefno, a.crtrnrefno) AS REFNO, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_acc, a.cr_acc) AS ACC, " +
		    	   "DECODE (b.drcr_ind, 'D', DECODE((SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.dr_acc), '', " +
		    	   			"(SELECT ac_desc FROM sttm_cust_account WHERE cust_ac_no = a.dr_acc), " +
		    	   			"(SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.dr_acc)), " +
		    	   			"DECODE((SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.cr_acc), '', " +
		    	   			"(SELECT ac_desc FROM sttm_cust_account WHERE cust_ac_no = a.cr_acc), " +
		    	   			"(SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.cr_acc))) AS AC_DESC, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_trn_code, a.cr_trn_code) AS TRN_CODE, " +
		    	   "DECODE (b.drcr_ind, 'D', (SELECT trn_desc FROM sttm_trn_code WHERE dr_trn_code = trn_code), " +
		    	   			"(SELECT trn_desc FROM sttm_trn_code WHERE a.cr_trn_code = trn_code)) AS TRN_DESC, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_acc_role, a.cr_acc_role) AS ACC_ROLE " +
		    	   "FROM cltb_event_entries a, actb_history b " +
		    	   "WHERE (a.drtrnrefno = b.trn_ref_no OR a.crtrnrefno = b.trn_ref_no) " +
		    	   "AND b.module = 'CI' AND a.value_date = b.value_dt " +
		    	   "AND a.branch_code = b.ac_branch) where account_number = ? " +
		    	   "ORDER BY value_date, event_seq_no, ac_entry_sr_no, event_entry_sr_no";
			
			int i = 1;
			System.out.println("count event :" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(i++, accno);
			rs = stat.executeQuery();
			
			while(rs.next()){
				tot = rs.getInt("count");
			}
		} catch (Exception e) {
			log.error("Error TotEvent" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
    }
    
    public List getevent(String accno,int begin,int delta) {
    	List ltrn = new ArrayList();
		Event trn = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
            sql =  "select * from (select rownum as no,event.* from " +
		     	   "(SELECT  b.event_sr_no, a.event_seq_no, b.ac_entry_sr_no, a.event_entry_sr_no, b.module, " +
		    	   "b.ac_no, a.branch_code, a.product_code, a.value_date, a.schedule_due_date, a.ccy, " +
		    	   "a.account_number, a.event_code, a.amount, a.amount_settled, a.amount_tag, a.from_status, " +
		    	   "a.to_status, a.component_name, b.drcr_ind, " +
		    	   "DECODE (b.drcr_ind, 'D', a.drtrnrefno, a.crtrnrefno) AS REFNO, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_acc, a.cr_acc) AS ACC, " +
		    	   "DECODE (b.drcr_ind, 'D', DECODE ((SELECT GL_desc FROM gltm_glmaster " +
		    	   									"WHERE gl_code = a.dr_acc), '', (SELECT ac_desc " +
		    	   									"FROM sttm_cust_account WHERE cust_ac_no = a.dr_acc), " +
		    	   									"(SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.dr_acc)), " +
		    	   									"DECODE ((SELECT GL_desc FROM gltm_glmaster " +
		    	   									"WHERE gl_code = a.cr_acc), '', (SELECT ac_desc " +
		    	   									"FROM sttm_cust_account WHERE cust_ac_no = a.cr_acc), " +
		    	   									"(SELECT GL_desc FROM gltm_glmaster " +
		    	   									"WHERE gl_code = a.cr_acc))) AS AC_DESC, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_trn_code, a.cr_trn_code) AS TRN_CODE, " +
		    	   "DECODE (b.drcr_ind,'D', (SELECT trn_desc FROM sttm_trn_code WHERE dr_trn_code = trn_code), " +
		    	   					"(SELECT trn_desc FROM sttm_trn_code WHERE a.cr_trn_code = trn_code)) AS TRN_DESC, " +
		    	   					"DECODE (b.drcr_ind, 'D', a.dr_acc_role, a.cr_acc_role) AS ACC_ROLE " +
		    	   "FROM   cltb_event_entries a, actb_daily_log b " +
		    	   "WHERE   DELETE_STAT <> 'D' and (a.drtrnrefno = b.trn_ref_no OR a.crtrnrefno = b.trn_ref_no) " +
//		    	   "AND a.account_number = ? "
		    	   "AND b.module = 'CI' AND a.value_date = b.value_dt " +
		    	   "AND a.branch_code = b.ac_branch " +
		    	   "UNION ALL " +
		    	   "SELECT  b.event_sr_no, a.event_seq_no, b.ac_entry_sr_no, a.event_entry_sr_no, b.module, " +
		    	   "b.ac_no, a.branch_code, a.product_code, a.value_date, a.schedule_due_date, a.ccy, " +
		    	   "a.account_number, a.event_code, a.amount, a.amount_settled, a.amount_tag, a.from_status, " +
		    	   "a.to_status, a.component_name, b.drcr_ind, " +
		    	   "DECODE (b.drcr_ind, 'D', a.drtrnrefno, a.crtrnrefno) AS REFNO, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_acc, a.cr_acc) AS ACC, " +
		    	   "DECODE (b.drcr_ind, 'D', DECODE((SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.dr_acc), '', " +
		    	   			"(SELECT ac_desc FROM sttm_cust_account WHERE cust_ac_no = a.dr_acc), " +
		    	   			"(SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.dr_acc)), " +
		    	   			"DECODE((SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.cr_acc), '', " +
		    	   			"(SELECT ac_desc FROM sttm_cust_account WHERE cust_ac_no = a.cr_acc), " +
		    	   			"(SELECT GL_desc FROM gltm_glmaster WHERE gl_code = a.cr_acc))) AS AC_DESC, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_trn_code, a.cr_trn_code) AS TRN_CODE, " +
		    	   "DECODE (b.drcr_ind, 'D', (SELECT trn_desc FROM sttm_trn_code WHERE dr_trn_code = trn_code), " +
		    	   			"(SELECT trn_desc FROM sttm_trn_code WHERE a.cr_trn_code = trn_code)) AS TRN_DESC, " +
		    	   "DECODE (b.drcr_ind, 'D', a.dr_acc_role, a.cr_acc_role) AS ACC_ROLE " +
		    	   "FROM cltb_event_entries a, actb_history b " +
		    	   "WHERE (a.drtrnrefno = b.trn_ref_no OR a.crtrnrefno = b.trn_ref_no) " +
//		    	   "AND a.account_number =  ? " +
		    	   "AND b.module = 'CI' AND a.value_date = b.value_dt " +
		    	   "AND a.branch_code = b.ac_branch) event where account_number = ? " +
		    	   "ORDER BY value_date, event_seq_no, ac_entry_sr_no, event_entry_sr_no) where no >= ? and no <= ? ";
            int i = 1;
            stat = conn.prepareStatement(sql);
            System.out.println("sql getevent :" + sql);
            stat.setString(i++, accno.trim());
            stat.setInt(i++, begin);
            stat.setInt(i++, delta);
			rs = stat.executeQuery();
			while (rs.next()) {
                trn = new Event();
                trn.setBranch_code(rs.getString("branch_code"));
                trn.setProduct_code(rs.getString("product_code"));
                if (rs.getString("value_date") != null) {
					trn.setValue_date(rs.getString("value_date").substring(0,10));
				} else {
					trn.setValue_date(rs.getString("value_date"));
				}
                if (rs.getString("schedule_due_date") != null) {
					trn.setSchedule_due_date(rs.getString("schedule_due_date").substring(0,10));
				} else {
					trn.setSchedule_due_date(rs.getString("schedule_due_date"));
				}
                trn.setCcy(rs.getString("ccy"));
                trn.setAccount_number(rs.getString("account_number"));
                trn.setEvent_code(rs.getString("event_code"));
                trn.setAmount(rs.getBigDecimal("amount"));
                trn.setAmount_settled(rs.getBigDecimal("amount_settled"));
                trn.setAmount_tag(rs.getString("amount_tag"));
                trn.setComponent_name(rs.getString("component_name"));
                trn.setDrcr_ind(rs.getString("drcr_ind"));
                trn.setRefno(rs.getString("REFNO"));
                trn.setAcc(rs.getString("ACC"));
                trn.setAc_desc(rs.getString("AC_DESC"));
                trn.setAcc_role(rs.getString("ACC_ROLE"));
                trn.setTrn_code(rs.getString("TRN_CODE"));
                trn.setTrn_desc(rs.getString("TRN_DESC"));
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
            log.error(" getEventFunc : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
    
    
    //edc mcb
    public List getListEdcMcb(String tgl_awal, String tgl_akhir, String branch_code,int begin, int delta) {
    	//System.out.println("status sql :" + status);
		List lbb = new ArrayList();
		EdcMcb bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
	       	 /*if ((accno==null ) && (cusid==null ) && (cusname==null )){
	       		 return lbb;
	       	 }*/
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
		    sql = "select * from (select rownum as no, A.branchcode, B.Trn_Ref_No, A.Functionid,A.wfinitdate, A.Makerid, A.Checkerid ,A.CHECKERDATESTAMP, " +
		          "A.txnactdet, A.txnccydet,A.txnamtdet, A.checkerremarks, a.makerremarks, b.edc_verify_status,c.ACCOUNT_CLASs,d.ATM_FACILITY" +
		          " FROM fbtb_txnlog_master A " +
		          "JOIN fcc114.iftb_edc_verification_custom B ON A.XREFID=B.XREF " +
		          "JOIN sttm_cust_account c ON a.txnactdet=c.cust_ac_NO " +
		          "JOIN sttm_account_CLASS d ON c.ACCOUNT_CLASS=d.ACCOUNT_CLASS " +
		          "where d.atm_facility <> 'N' AND A.wfinitdate > to_date(?, 'YYYY-MM-DD HH24:MI:SS') AND A.wfinitdate < to_date(?, 'YYYY-MM-DD HH24:MI:SS') AND A.branchcode=?) " +
		          "where no >= ? and no <= ?";	
			
			int i = 1;
			String tgl_awal_ =tgl_awal + " 00:00:00";
			String tgl_akhir_ =tgl_akhir + " 23:59:59";
			
			System.out.println(tgl_awal_ + tgl_akhir_+branch_code);
			stat = conn.prepareStatement(sql);
			stat.setString(1, tgl_awal_);
			stat.setString(2, tgl_akhir_);
			stat.setString(3, branch_code);
			stat.setInt(4, begin);
			stat.setInt(5, delta);
			System.out.println("sql instalment :" + sql);
			System.out.println("tes"+begin+delta);
			//stat.setInt(i++, begin);
			//stat.setInt(i++, delta);
			System.out.println("tes"+begin+delta);
			
			rs = stat.executeQuery();
			System.out.println("rs :"+rs );
			while (rs.next()) {
				bb = new EdcMcb();
				bb.setBranch_code(rs.getString("branchcode"));
				bb.setTrn_ref_no(rs.getString("trn_ref_no"));
				bb.setFunctionid(rs.getString("functionid"));
				bb.setWfinitdate(rs.getString("wfinitdate"));
				bb.setMakerid(rs.getString("makerid"));
				bb.setCheckerid(rs.getString("checkerid"));
				bb.setCheckerdatestamp(rs.getString("checkerdatestamp"));
				bb.setTxnactdet(rs.getString("txnactdet"));
				bb.setTxnccydet(rs.getString("txnccydet"));
				bb.setTxnamtdet(rs.getBigDecimal("txnamtdet"));
				if (rs.getBigDecimal("txnamtdet") == null){
					bb.setTxnamtdet(new BigDecimal(0));
				} else {
					bb.setTxnamtdet(rs.getBigDecimal("txnamtdet"));
				}
				bb.setCheckerremarks(rs.getString("checkerremarks"));
				bb.setMakerremarks(rs.getString("makerremarks"));
				bb.setEdc_verify_status(rs.getString("edc_verify_status"));
				bb.setAccount_class(rs.getString("account_class"));
				bb.setAtm_facility(rs.getString("atm_facility"));
				lbb.add(bb);
				System.out.println(lbb);
			}
		} catch (SQLException ex) {
			log.error(" getListCustom : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		
		return lbb;
	}
    
    public int getTotEdcMcb (String tgl_awal, String tgl_akhir, String branch_code){
    	int tot = 0;
    	EdcMcb bb = null;
    	PreparedStatement stat = null;
    	Connection conn = null;
    	ResultSet rs = null;
        String sql = "";
        
        try {
			
        	conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();

	  sql = "select count (*) as total from(SELECT A.branchcode, B.Trn_Ref_No, A.Functionid,A.wfinitdate, A.Makerid, A.Checkerid ,A.CHECKERDATESTAMP, " +
		          "A.txnactdet, A.txnccydet,A.txnamtdet, A.checkerremarks, a.makerremarks, b.edc_verify_status,c.ACCOUNT_CLASs,d.ATM_FACILITY " +
		          "FROM fbtb_txnlog_master A " +
		          "JOIN fcc114.iftb_edc_verification_custom B ON A.XREFID=B.XREF " +
		          "JOIN sttm_cust_account c ON a.txnactdet=c.cust_ac_NO " +
		          "JOIN sttm_account_CLASS d ON c.ACCOUNT_CLASS=d.ACCOUNT_CLASS " +
		          "where d.atm_facility <> 'N' AND A.wfinitdate > to_date(?, 'YYYY-MM-DD HH24:MI:SS') AND A.wfinitdate < to_date(?, 'YYYY-MM-DD HH24:MI:SS') AND A.branchcode=?) " ;	
        	
  			
  			System.out.println("SQL TOTAL :" + sql);
  			String tgl_awal_ =tgl_awal + " 00:00:00";
			String tgl_akhir_ =tgl_akhir + " 23:59:59";
			System.out.println(tgl_awal_ + tgl_akhir_+branch_code);
			
  			stat = conn.prepareStatement(sql);
  			stat.setString(1, tgl_awal_);
			stat.setString(2, tgl_akhir_);
			stat.setString(3, branch_code);
  			int i = 1;
  		
  				
  			 
  			rs = stat.executeQuery();
  			if (rs.next()) {
				tot = rs.getInt("total");
				System.out.println("tot :" + tot);
			}
		} catch (Exception e) {
			log.error("getTotListInstlmnt :" + e.getMessage());
		}  finally {
			closeConnDb(conn, stat, rs);
		}
        return tot;
    } 
    
}


