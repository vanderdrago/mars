package com.muamalat.reportmcb.function;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.muamalat.reportmcb.entity.KonversiRekening;
import com.muamalat.reportmcb.entity.RejectData;

public class ReadFileExcel {
	private List arrDataOk = new ArrayList();
	private List arrRejectData = new ArrayList();
	private int totTrns = 0;

	
	public List getArrDataOk() {
		return arrDataOk;
	}

	public void setArrDataOk(List arrDataOk) {
		this.arrDataOk = arrDataOk;
	}

	public List getArrRejectData() {
		return arrRejectData;
	}

	public void setArrRejectData(List arrRejectData) {
		this.arrRejectData = arrRejectData;
	}

	public int getTotTrns() {
		return totTrns;
	}

	public void setTotTrns(int totTrns) {
		this.totTrns = totTrns;
	}

	public void prosesReadExcl(String filepath, String filename) {
		try {
			File file = new File(filepath + filename);
			boolean exists = file.exists();
			if (exists) {
				KonversiRekening knvrek = null;
				RejectData reject = null;

				FileInputStream fileInputStream = new FileInputStream(filepath + filename);
				HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
				HSSFSheet worksheet = workbook.getSheet("rekening");

				int i = 1;
				for (int x = 1; x <= worksheet.getLastRowNum(); x++) {
					knvrek = new KonversiRekening();
					HSSFRow row1 = worksheet.getRow(x);					
					String rekasal = "";
					try {
						HSSFCell cellB1 = row1.getCell((short) 0);
						rekasal = cellB1.getStringCellValue();
						knvrek.setAlt_nm_rekening(rekasal);
					} catch (Exception e) {
						this.arrRejectData.add(getObjReject(i, rekasal + ", Nama Rekening tidak sesuai format."));
						knvrek = null;
						i++;
						continue;
					}
					try {
						HSSFCell cellB1 = row1.getCell((short) 1);
						rekasal = cellB1.getStringCellValue();
						if ((rekasal.length() > 10) || (rekasal.length() < 10)) {
							this.arrRejectData.add(getObjReject(i, rekasal + ", Panjang digit Rekening Asal tidak sesuai format, harus 10 karakter."));
							i++;
							knvrek = null;
							continue;
						}
						knvrek.setAlt_rekening(rekasal);
					} catch (Exception e) {
						this.arrRejectData.add(getObjReject(i, rekasal + ", Rekening asal tidak sesuai format."));
						knvrek = null;
						i++;
						continue;
					}
					if (knvrek != null) {
						this.totTrns += 1;
						this.arrDataOk.add(knvrek);
					}
					i++;
				}
				fileInputStream.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private RejectData getObjReject(int i, String teks) {
		RejectData reject = new RejectData();
		reject.setRow(i + 1);
		reject.setDesc(teks);
		return reject;
	}
}
