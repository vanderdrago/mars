package com.muamalat.reportmcb.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.entity.SknSectServer;
import com.muamalat.singleton.DatasourceEntry;


public class SknInterfaceFunction {
	
	private static Logger log = Logger.getLogger(SknInterfaceFunction.class);
	
	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public List getListSectServer() {
		List lbb = new ArrayList();
		SknSectServer ss = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getSknDS().getConnection();
			sql = "SELECT pc_code, pc_desc FROM bmi_skn_sectserver.dbo.postingcodes;";			
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				ss = new SknSectServer();
				ss.setPc_code(rs.getString("pc_code"));
				ss.setPc_desc(rs.getString("pc_desc"));
				lbb.add(ss);
			}
		} catch (SQLException ex) {		
			log.error(" getListSectServer : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
}
