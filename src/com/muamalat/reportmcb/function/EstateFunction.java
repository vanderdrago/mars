package com.muamalat.reportmcb.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
//import org.jboss.ws.tools.ant.SysProperty;

import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.Customer;
import com.muamalat.reportmcb.entity.E_stat_cust;
import com.muamalat.reportmcb.parameter.Parameter;
import com.muamalat.singleton.DatasourceEntry;

public class EstateFunction {
	private static Logger log = Logger.getLogger(EstateFunction.class);
	
	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public E_stat_cust getEStatCust(String accno, String tbl) {
		E_stat_cust bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String opt = "";
		DataFunction df = new DataFunction();
		try {
			conn = DatasourceEntry.getInstance().getEstatement().getConnection();
			if ("estatin".equals(tbl)){
				sql = "SELECT account, branch_code, cust_name, cust_type, cust_no, acc_class, acc_class_desc, sex, to_char(dt_birth, 'YYYY-MM-DD') as dt_birth, ibu_kndng, " +
						"address, s_text, user_email, email, email_cc, sts_proc, branch_reg, user_input, user_aut, dt_input, to_char(dt_input, 'YYYY-MM-DD') as dt_reg, dt_aut, sts_data, sts_data_desc " +
						"FROM e_stat_cust_in where account = ?;"; 
			} else if ("estat".equals(tbl)){
				
			}
			stat = conn.prepareStatement(sql);
			stat.setString(1, accno);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new E_stat_cust();;
				bb.setAccount((rs.getString("account") == null) ? "" : rs.getString("account"));
				bb.setBranch_code((rs.getString("branch_code") == null) ? "" : rs.getString("branch_code"));
				bb.setBranch_name(df.getBranchMaster(bb.getBranch_code().trim()).getBrnch_name());				
				bb.setCust_name((rs.getString("cust_name") == null) ? "" : rs.getString("cust_name"));
				bb.setCust_type((rs.getString("cust_type") == null) ? "" : rs.getString("cust_type"));
				bb.setCust_no((rs.getString("cust_no") == null) ? "" : rs.getString("cust_no"));
				bb.setAcc_class((rs.getString("acc_class") == null) ? "" : rs.getString("acc_class"));
				bb.setAcc_class_desc((rs.getString("acc_class_desc") == null) ? "" : rs.getString("acc_class_desc"));				
				bb.setSex((rs.getString("sex") == null) ? "" : rs.getString("sex"));
				bb.setDt_birth((rs.getString("dt_birth") == null) ? "" : rs.getString("dt_birth"));
				bb.setIbu_kndng((rs.getString("ibu_kndng") == null) ? "" : rs.getString("ibu_kndng"));
				bb.setCust_addrs((rs.getString("address") == null) ? "" : rs.getString("address"));
				bb.setS_text((rs.getString("s_text") == null) ? "" : rs.getString("s_text"));
				bb.setUser_email((rs.getString("user_email") == null) ? "" : rs.getString("user_email"));				
				bb.setEmail((rs.getString("email") == null) ? "" : rs.getString("email"));
				bb.setEmail_cc((rs.getString("email_cc") == null) ? "" : rs.getString("email_cc"));
				bb.setSts_proc((rs.getString("sts_proc") == null) ? "" : rs.getString("sts_proc"));
				bb.setBranch_reg((rs.getString("branch_reg") == null) ? "" : rs.getString("branch_reg"));
				bb.setBranch_reg_name(df.getBranchMaster(bb.getBranch_reg().trim()).getBrnch_name());
				bb.setUser_input((rs.getString("user_input") == null) ? "" : rs.getString("user_input"));
				bb.setUser_aut((rs.getString("user_aut") == null) ? "" : rs.getString("user_aut"));
				bb.setDt_input(rs.getTimestamp("dt_input"));
				bb.setTgl_input(rs.getString("dt_reg"));
				bb.setDt_aut(rs.getTimestamp("dt_aut"));
				bb.setSts_data((rs.getString("sts_data") == null) ? "" : rs.getString("sts_data"));
				bb.setSts_data_desc((rs.getString("sts_data_desc") == null) ? "" : rs.getString("sts_data_desc"));
				bb.setOption(opt);
			}
		} catch (SQLException ex) {
			log.error("Error in getEStatCust : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public E_stat_cust getCustomer(String acc) {
		E_stat_cust cust = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		DataFunction df = new DataFunction();

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select a.branch_code, a.cust_ac_no as account, a.record_stat, a.auth_stat, c.customer_name1 as cust_name, c.customer_no, c.customer_type as cust_type, " +
					"d.account_class, d.description, p.sex,(select field_val_1 from CSTM_FUNCTION_USERDEF_FIELDS where rec_key = c.customer_no || '~') as ibukandung, " +
					"substr (a.cust_ac_no, length(a.cust_ac_no) - 4) as s_text, p.e_mail as email, TO_CHAR(c.date_of_birth, 'YYYY-MM-DD') as dt_of_birth, " +
					"c.address_line1|| '<br />' || c.address_line3 || '<br />' || c.address_line2 || '<br />' || c.address_line4 as address " +
					"from sttm_cust_account a, sttm_cust_personal p, sttm_customer c, sttm_account_class d " +
					"where p.customer_no = a.cust_no and a.cust_no = c.customer_no and d.account_class = a.account_class and " +
					"a.cust_ac_no = ? ";
			System.out.println("sql customer :" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc.trim());
			rs = stat.executeQuery();
			if (rs.next()) {
				cust = new E_stat_cust();
				cust.setBranch_code(rs.getString("branch_code") == null ? "" : rs.getString("branch_code"));
				cust.setBranch_name(df.getBranchMaster(cust.getBranch_code().trim()).getBrnch_name());				
				cust.setAccount(rs.getString("account") == null ? "" : rs.getString("account"));
				System.out.println("account :" + rs.getString("account"));
				cust.setRecord_stat(rs.getString("record_stat") == null ? "" : rs.getString("record_stat"));
				cust.setAuth_stat(rs.getString("auth_stat") == null ? "" : rs.getString("auth_stat"));
				cust.setCust_name(rs.getString("cust_name") == null ? "" : rs.getString("cust_name"));
				cust.setCust_no(rs.getString("customer_no") == null ? "" : rs.getString("customer_no"));
				cust.setCust_type(rs.getString("cust_type") == null ? "" : rs.getString("cust_type"));
				cust.setAcc_class(rs.getString("account_class") == null ? "" : rs.getString("account_class"));
				cust.setAcc_class_desc(rs.getString("description") == null ? "" : rs.getString("description"));
				cust.setSex(rs.getString("sex") == null ? "" : rs.getString("sex"));
				cust.setIbu_kndng(rs.getString("ibukandung") == null ? "" : rs.getString("ibukandung"));
				cust.setS_text(rs.getString("s_text") == null ? "" : rs.getString("s_text"));
				cust.setUser_email("");
				cust.setEmail(rs.getString("email") == null ? "" : rs.getString("email"));
				cust.setDt_birth(rs.getString("dt_of_birth") == null ? "" : rs.getString("dt_of_birth"));
				cust.setCust_addrs(rs.getString("address") == null ? "" : rs.getString("address"));
				
			}
		} catch (SQLException ex) {
			log.error("Error in getCustomer e-statement : " + ex.getMessage());
		
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return cust;
	}	
	
	public List getListCustomer(String acc, String nsbh, Admin user, String sts_data, int awal, String rol_usr) {
		ArrayList lcust = new ArrayList();
		E_stat_cust bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String opt = "";
		String delimiter = ",";
		String lsCab = "";
		DataFunction df = new DataFunction();
		CetakPassbookFunction cbf = new CetakPassbookFunction();
		try {
			conn = DatasourceEntry.getInstance().getEstatement().getConnection();
			sql = "SELECT account, branch_code, cust_name, cust_type, cust_no, acc_class, acc_class_desc, sex, to_char(dt_birth, 'YYYY-MM-DD') as dt_birth, ibu_kndng, address, " +
					"s_text, user_email, email, email_cc, sts_proc, branch_reg, user_input, user_aut, dt_input, to_char(dt_input, 'YYYY-MM-DD') as dt_reg, dt_aut, sts_data, sts_data_desc " +
					"FROM e_stat_cust_in where";
			if("2".equals(rol_usr)){
				String alwdRole = cbf.getAllowedRoles(10);
				String[] temp = alwdRole.split(delimiter);
				String role_tmp = "";
				for(int i =0; i < temp.length ; i++){
					role_tmp += "\'" + temp[i] + "\',";
				}
				role_tmp = role_tmp.substring(0, role_tmp.length() - 1);
				lsCab = cbf.getListCabUser(user.getUsername(), role_tmp);
				sql += " branch_reg in ("+lsCab+") and";
//				if (!"".equals(kdcab)){
//					sql += " branch_reg = ? and";
//				}				
			}
			if (!"".equals(sts_data)){
				if("I".equals(sts_data)){
					sql += " sts_proc = ? and";
				} else {
					sql += " sts_proc = ? and sts_data = ? and";
				}
			}
			if (!"".equals(acc)){
				sql += " account = ? and";
			}
			if (!"".equals(nsbh)){
				sql += " cust_name like '%" + nsbh.toUpperCase() + "%' and";
			}
			sql = sql.substring(0, sql.length() - 3);
			sql += " order by account asc LIMIT 30 OFFSET ?";
			stat = conn.prepareStatement(sql);
			int i = 1;
			System.out.println("sql eStatement :" + sql);
			if("2".equals(rol_usr)){
//				if (!"".equals(kdcab)){
//					stat.setString(i++, kdcab.trim());
//				}			
			}
			if (!"".equals(sts_data)){
				if("I".equals(sts_data)){
					stat.setString(i++, sts_data.trim());
				} else {
					stat.setString(i++, "C");
					stat.setString(i++, sts_data.trim());
				}
			}
			if (!"".equals(acc)){
				stat.setString(i++, acc.trim());
			}
			stat.setInt(i++, awal);
			rs = stat.executeQuery();
			while (rs.next()) {				
				bb = new E_stat_cust();;
				bb.setAccount((rs.getString("account") == null) ? "" : rs.getString("account"));
				bb.setBranch_code((rs.getString("branch_code") == null) ? "" : rs.getString("branch_code"));
				bb.setBranch_name(df.getBranchMaster(bb.getBranch_code().trim()).getBrnch_name());
				bb.setCust_name((rs.getString("cust_name") == null) ? "" : rs.getString("cust_name"));
				bb.setCust_type((rs.getString("cust_type") == null) ? "" : rs.getString("cust_type"));
				if("I".equals(bb.getCust_type())){
					bb.setCust_type_desc("INDIVIDU");
				} else {
					bb.setCust_type_desc("NON INDIVIDU");
				}
				
				bb.setCust_no((rs.getString("cust_no") == null) ? "" : rs.getString("cust_no"));
				bb.setAcc_class((rs.getString("acc_class") == null) ? "" : rs.getString("acc_class"));
				bb.setAcc_class_desc((rs.getString("acc_class_desc") == null) ? "" : rs.getString("acc_class_desc"));				
				bb.setSex((rs.getString("sex") == null) ? "" : rs.getString("sex"));
				bb.setDt_birth((rs.getString("dt_birth") == null) ? "" : rs.getString("dt_birth"));
				bb.setIbu_kndng((rs.getString("ibu_kndng") == null) ? "" : rs.getString("ibu_kndng"));
				bb.setCust_addrs((rs.getString("address") == null) ? "" : rs.getString("address"));
				bb.setS_text((rs.getString("s_text") == null) ? "" : rs.getString("s_text"));
				bb.setUser_email((rs.getString("user_email") == null) ? "" : rs.getString("user_email"));				
				bb.setEmail((rs.getString("email") == null) ? "" : rs.getString("email"));
				bb.setEmail_cc((rs.getString("email_cc") == null) ? "" : rs.getString("email_cc"));
				bb.setSts_proc((rs.getString("sts_proc") == null) ? "" : rs.getString("sts_proc"));
				bb.setBranch_reg((rs.getString("branch_reg") == null) ? "" : rs.getString("branch_reg"));
				bb.setBranch_reg_name(df.getBranchMaster(bb.getBranch_reg().trim()).getBrnch_name());
				bb.setSts_proc_desc((rs.getString("sts_proc") == null) ? "" : rs.getString("sts_proc"));
				bb.setUser_input((rs.getString("user_input") == null) ? "" : rs.getString("user_input"));
				bb.setUser_aut((rs.getString("user_aut") == null) ? "" : rs.getString("user_aut"));
				bb.setDt_input(rs.getTimestamp("dt_input"));
				bb.setTgl_input(rs.getString("dt_reg"));
				bb.setDt_aut(rs.getTimestamp("dt_aut"));
				bb.setSts_data((rs.getString("sts_data") == null) ? "" : rs.getString("sts_data"));
				if("I".equals(bb.getSts_proc())){
					bb.setSts_data_desc("Menunggu Otorisasi");					
				} else if("C".equals(bb.getSts_proc())){
					bb.setSts_data_desc((rs.getString("sts_data_desc") == null) ? "" : rs.getString("sts_data_desc"));					
				}
				if("1".equals(rol_usr)){
					opt = "<a href='/ReportMCB/eStateUpdt.do?acc="+ bb.getAccount() + "&opt=e'> Edit </a> ";
					if("I".equals(bb.getSts_proc())){	
					} else if("C".equals(bb.getSts_proc())){	
						if("C".equals(bb.getSts_data().trim()) || "O".equals(bb.getSts_data().trim()) || "H".equals(bb.getSts_data().trim())){
							opt += "<a href='/ReportMCB/eStateUpdt.do?acc="+ bb.getAccount() + "&opt=u'> Update Sts </a>";
						}						
					}
				} 
				if("2".equals(rol_usr)){
					if("I".equals(bb.getSts_proc().trim())){
						opt = "<a href='/ReportMCB/eStateOto.do?acc="+ bb.getAccount() + "'> Otorisasi </a> ";						
					} 
				}
				bb.setOption(opt);
				opt="";
				if("2".equals(rol_usr)){
					if ("I".equals(bb.getSts_proc()) && (lsCab.indexOf(bb.getBranch_reg()) > -1 ? true : false)){
						lcust.add(bb);					
					} else if ("C".equals(bb.getSts_proc())) {
						lcust.add(bb);
					}
				} else if("1".equals(rol_usr)){
//					if ("I".equals(bb.getSts_proc()) && user.getKodecabang().equals(bb.getBranch_reg())){
					if ("I".equals(bb.getSts_proc())){
						lcust.add(bb);					
					} else if ("C".equals(bb.getSts_proc())) {
						lcust.add(bb);
					}					
				}
			}
		} catch (SQLException ex) {
			log.error("Error in getListCustomer : " + ex.getMessage());
		
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lcust;
	}	
	
	
	public int getTotCustomer(String acc, String nsbh, Admin user, String sts_data, String rol_usr) {
		int total = 0;
		ArrayList lcust = new ArrayList();
		E_stat_cust bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String opt = "";
		String delimiter = ",";
		DataFunction df = new DataFunction();
		CetakPassbookFunction cbf = new CetakPassbookFunction();
		try {
			conn = DatasourceEntry.getInstance().getEstatement().getConnection();
			sql = "SELECT account, branch_code, cust_name, cust_type, cust_no, acc_class, acc_class_desc, sex, to_char(dt_birth, 'YYYY-MM-DD') as dt_birth, ibu_kndng, address, " +
					"s_text, user_email, email, email_cc, sts_proc, branch_reg, user_input, user_aut, dt_input, to_char(dt_input, 'YYYY-MM-DD') as dt_reg, dt_aut, sts_data, sts_data_desc " +
					"FROM e_stat_cust_in where";
			if("2".equals(rol_usr)){
				String alwdRole = cbf.getAllowedRoles(10);
				String[] temp = alwdRole.split(delimiter);
				String role_tmp = "";
				for(int i =0; i < temp.length ; i++){
					role_tmp += "\'" + temp[i] + "\',";
				}
				role_tmp = role_tmp.substring(0, role_tmp.length() - 1);
				String lsCab = cbf.getListCabUser(user.getUsername(), role_tmp);
				sql += " branch_reg in ("+lsCab+") and";
//				if (!"".equals(kdcab)){
//					sql += " branch_reg = ? and";
//				}				
			}
			if (!"".equals(sts_data)){
				if("I".equals(sts_data)){
					sql += " sts_proc = ? and";
				} else {
					sql += " sts_proc = ? and sts_data = ? and";
				}
			}
			if (!"".equals(acc)){
				sql += " account = ? and";
			}
			if (!"".equals(nsbh)){
				sql += " cust_name like '%" + nsbh.toUpperCase() + "%' and";
			}
			sql = sql.substring(0, sql.length() - 3);
			sql += " order by account asc ";
			stat = conn.prepareStatement(sql);
			int i = 1;
			if("2".equals(rol_usr)){
//				if (!"".equals(kdcab)){
//					stat.setString(i++, kdcab.trim());
//				}			
			}
			if (!"".equals(sts_data)){
				if("I".equals(sts_data)){
					stat.setString(i++, sts_data.trim());
				} else {
					stat.setString(i++, "C");
					stat.setString(i++, sts_data.trim());
				}
			}
			if (!"".equals(acc)){
				stat.setString(i++, acc.trim());
			}
			rs = stat.executeQuery();
			while (rs.next()) {
				
				bb = new E_stat_cust();;
				bb.setAccount((rs.getString("account") == null) ? "" : rs.getString("account"));
				bb.setBranch_code((rs.getString("branch_code") == null) ? "" : rs.getString("branch_code"));
				bb.setBranch_name(df.getBranchMaster(bb.getBranch_code().trim()).getBrnch_name());
				bb.setCust_name((rs.getString("cust_name") == null) ? "" : rs.getString("cust_name"));
				bb.setCust_type((rs.getString("cust_type") == null) ? "" : rs.getString("cust_type"));
				if("I".equals(bb.getCust_type())){
					bb.setCust_type_desc("INDIVIDU");
				} else {
					bb.setCust_type_desc("NON INDIVIDU");
				}
				
				bb.setCust_no((rs.getString("cust_no") == null) ? "" : rs.getString("cust_no"));
				bb.setAcc_class((rs.getString("acc_class") == null) ? "" : rs.getString("acc_class"));
				bb.setAcc_class_desc((rs.getString("acc_class_desc") == null) ? "" : rs.getString("acc_class_desc"));				
				bb.setSex((rs.getString("sex") == null) ? "" : rs.getString("sex"));
				bb.setDt_birth((rs.getString("dt_birth") == null) ? "" : rs.getString("dt_birth"));
				bb.setIbu_kndng((rs.getString("ibu_kndng") == null) ? "" : rs.getString("ibu_kndng"));
				bb.setCust_addrs((rs.getString("address") == null) ? "" : rs.getString("address"));
				bb.setS_text((rs.getString("s_text") == null) ? "" : rs.getString("s_text"));
				bb.setUser_email((rs.getString("user_email") == null) ? "" : rs.getString("user_email"));				
				bb.setEmail((rs.getString("email") == null) ? "" : rs.getString("email"));
				bb.setEmail_cc((rs.getString("email_cc") == null) ? "" : rs.getString("email_cc"));
				bb.setSts_proc((rs.getString("sts_proc") == null) ? "" : rs.getString("sts_proc"));
				bb.setBranch_reg((rs.getString("branch_reg") == null) ? "" : rs.getString("branch_reg"));
				bb.setBranch_reg_name(df.getBranchMaster(bb.getBranch_reg().trim()).getBrnch_name());
				bb.setSts_proc_desc((rs.getString("sts_proc") == null) ? "" : rs.getString("sts_proc"));
				bb.setUser_input((rs.getString("user_input") == null) ? "" : rs.getString("user_input"));
				bb.setUser_aut((rs.getString("user_aut") == null) ? "" : rs.getString("user_aut"));
				bb.setDt_input(rs.getTimestamp("dt_input"));
				bb.setTgl_input(rs.getString("dt_reg"));
				bb.setDt_aut(rs.getTimestamp("dt_aut"));
				bb.setSts_data((rs.getString("sts_data") == null) ? "" : rs.getString("sts_data"));
				if("I".equals(bb.getSts_proc())){
					bb.setSts_data_desc("Menunggu Otorisasi");					
				} else if("C".equals(bb.getSts_proc())){
					bb.setSts_data_desc((rs.getString("sts_data_desc") == null) ? "" : rs.getString("sts_data_desc"));					
				}
				if("1".equals(rol_usr)){
					opt = "<a href='/ReportMCB/eStateUpdt.do?acc="+ bb.getAccount() + "&opt=e'> Edit </a> ";
					if("I".equals(bb.getSts_proc())){	
					} else if("C".equals(bb.getSts_proc())){	
						if("C".equals(bb.getSts_data().trim()) || "O".equals(bb.getSts_data().trim()) || "H".equals(bb.getSts_data().trim())){
							opt += "<a href='/ReportMCB/eStateUpdt.do?acc="+ bb.getAccount() + "&opt=u'> Update Sts </a>";
						}						
					}
				} 
				if("2".equals(rol_usr)){
					if("I".equals(bb.getSts_proc().trim())){
						opt = "<a href='/ReportMCB/eStateOto.do?acc="+ bb.getAccount() + "'> Otorisasi </a> ";						
					} 
				}
				bb.setOption(opt);
				opt="";
				if ("I".equals(bb.getSts_proc()) && user.getKodecabang().equals(bb.getBranch_reg())){
					lcust.add(bb);					
				} else if ("C".equals(bb.getSts_proc())) {
					lcust.add(bb);
				}
			}
			total = lcust.size();
		} catch (SQLException ex) {
			log.error("Error in getListCustomer : " + ex.getMessage());		
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return total;
	}
	
	public boolean insertEStateCust(E_stat_cust cust, String email_to, String email_cc, String sts_proc, String user_in, String user_cb, String sts_data, String sts_data_desc) {
		boolean ret = false;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getEstatement().getConnection();
			
			if(!"".equals(cust.getDt_birth()) && cust.getDt_birth() != null){
				sql = "INSERT INTO e_stat_cust_in(account, branch_code, cust_name, cust_type, cust_no, acc_class, acc_class_desc, sex, dt_birth, " +
				"ibu_kndng, address, s_text, user_email, email, email_cc, sts_proc, branch_reg, user_input, sts_data, sts_data_desc) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, to_date(?,'YYYY-MM-DD'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			} else {
				sql = "INSERT INTO e_stat_cust_in(account, branch_code, cust_name, cust_type, cust_no, acc_class, acc_class_desc, sex, " +
				"ibu_kndng, address, s_text, user_email, email, email_cc, sts_proc, branch_reg, user_input, sts_data, sts_data_desc) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			}
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, cust.getAccount());
			stat.setString(i++, cust.getBranch_code());
			stat.setString(i++, cust.getCust_name().toUpperCase());
			stat.setString(i++, cust.getCust_type());
			stat.setString(i++, cust.getCust_no());
			stat.setString(i++, cust.getAcc_class());
			stat.setString(i++, cust.getAcc_class_desc());			
			stat.setString(i++, cust.getSex());
			if(!"".equals(cust.getDt_birth()) && cust.getDt_birth() != null){
				stat.setString(i++, cust.getDt_birth());				
			} 
			stat.setString(i++, cust.getIbu_kndng());
			stat.setString(i++, cust.getCust_addrs());
			stat.setString(i++, cust.getS_text());
			stat.setString(i++, "");			
			stat.setString(i++, email_to);
			stat.setString(i++, email_cc);
			stat.setString(i++, sts_proc);
			stat.setString(i++, user_cb);
			stat.setString(i++, user_in);
			stat.setString(i++, sts_data);
			stat.setString(i++, sts_data_desc);
			stat.execute();
			ret = true;
		} catch (SQLException ex) {
			this.log.error(" insertEStateCust : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ret;
	}

	
	public boolean updateEStateCust(E_stat_cust cust) {
		boolean ret = false;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String opt = "";
		try {
			conn = DatasourceEntry.getInstance().getEstatement().getConnection(); 
			if(!"".equals(cust.getDt_birth()) && cust.getDt_birth() != null){
				sql = "UPDATE e_stat_cust_in " +
				"SET branch_code=?, cust_name=?, cust_type=?, cust_no=?, acc_class=?, acc_class_desc=?, sex=?, dt_birth = to_date(?,'YYYY-MM-DD'), ibu_kndng=?, " +
				"address=?, s_text=?, user_email=?, email=?, email_cc=?, sts_proc=?, branch_reg=?, user_input=?, user_aut=?, dt_input=?, dt_aut=?, sts_data=?, " +
				"sts_data_desc=?, sts_send_email=? WHERE account=?;";
			} else {
				sql = "UPDATE e_stat_cust_in " +
				"SET branch_code=?, cust_name=?, cust_type=?, cust_no=?, acc_class=?, acc_class_desc=?, sex=?, ibu_kndng=?, " +
				"address=?, s_text=?, user_email=?, email=?, email_cc=?, sts_proc=?, branch_reg=?, user_input=?, user_aut=?, dt_input=?, dt_aut=?, sts_data=?, " +
				"sts_data_desc=?, sts_send_email=? WHERE account=?;";
			}
			stat = conn.prepareStatement(sql);
			int i = 1;
			stat.setString(i++, cust.getBranch_code().trim());
			stat.setString(i++, cust.getCust_name().trim());
			stat.setString(i++, cust.getCust_type().trim());
			stat.setString(i++, cust.getCust_no().trim());
			stat.setString(i++, cust.getAcc_class().trim());
			stat.setString(i++, cust.getAcc_class_desc().trim());
			stat.setString(i++, cust.getSex().trim());
			if(!"".equals(cust.getDt_birth()) && cust.getDt_birth() != null){
				stat.setString(i++, cust.getDt_birth());				
			} 
			stat.setString(i++, cust.getIbu_kndng().trim());
			stat.setString(i++, cust.getCust_addrs().trim());
			stat.setString(i++, cust.getS_text().trim());
			stat.setString(i++, cust.getUser_email().trim());
			stat.setString(i++, cust.getEmail().trim());
			stat.setString(i++, cust.getEmail_cc().trim());
			stat.setString(i++, cust.getSts_proc().trim());
			stat.setString(i++, cust.getBranch_reg().trim());
			stat.setString(i++, cust.getUser_input().trim());
			stat.setString(i++, cust.getUser_aut().trim());
			stat.setTimestamp(i++, cust.getDt_input());
			stat.setTimestamp(i++, cust.getDt_aut());
			stat.setString(i++, cust.getSts_data());
			stat.setString(i++, cust.getSts_data_desc());
			stat.setBoolean(i++, cust.isSts_send_email());
			stat.setString(i++, cust.getAccount().trim());
//			stat.setString(i++, cust.getSts_data().trim());			
			stat.executeUpdate();
			ret = true;
		} catch (SQLException ex) {
			log.error("Error in updateEStateCust : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return ret;
	}
}
