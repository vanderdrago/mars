package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.entity.HistTransRek;
import com.muamalat.singleton.DatasourceEntry;

public class HistCorporateFunction {
	private static Logger log = Logger.getLogger(SqlFunction.class);
	private BigDecimal totMutDb;
	private BigDecimal totMutCb;
	private int jmlItmD;
	private int jmlItmC;
	private BigDecimal totD;
	private BigDecimal totC;
	private BigDecimal sld_akhir;
	
	public BigDecimal getTotMutDb() {
		return totMutDb;
	}
	public void setTotMutDb(BigDecimal totMutDb) {
		this.totMutDb = totMutDb;
	}
	public BigDecimal getTotMutCb() {
		return totMutCb;
	}
	public void setTotMutCb(BigDecimal totMutCb) {
		this.totMutCb = totMutCb;
	}
	public int getJmlItmD() {
		return jmlItmD;
	}
	public void setJmlItmD(int jmlItmD) {
		this.jmlItmD = jmlItmD;
	}
	public int getJmlItmC() {
		return jmlItmC;
	}
	public void setJmlItmC(int jmlItmC) {
		this.jmlItmC = jmlItmC;
	}
	public BigDecimal getTotD() {
		return totD;
	}
	public void setTotD(BigDecimal totD) {
		this.totD = totD;
	}
	public BigDecimal getTotC() {
		return totC;
	}
	public void setTotC(BigDecimal totC) {
		this.totC = totC;
	}
	public BigDecimal getSld_akhir() {
		return sld_akhir;
	}
	public void setSld_akhir(BigDecimal sld_akhir) {
		this.sld_akhir = sld_akhir;
	}
	
	public void closeConnDb(Connection conn, PreparedStatement stat,
			ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public List getListHistTrans(String norek1, String norek2, String dt1, String dt2, BigDecimal sldawal) {
		List lbb = new ArrayList();
		HistTransRek ht = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			jmlItmD = 0;
			jmlItmC = 0;
			totD = new BigDecimal(0);
			totC = new BigDecimal(0);
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select a.ac_no, a.trn_ref_no, a.instrument_code, TO_CHAR(a.trn_dt, 'DD-MM-YY') as trn_dt, TO_CHAR(a.value_dt, 'DD-MM-YY') as value_dt, a.trn_code, a.event, " +
				  "((SELECT trim(ADDL_TEXT) FROM CSTBS_ADDL_TEXT WHERE REFERENCE_NO = a.trn_ref_no AND EVNT_SEQ_NO = a.event_sr_no) ||' '|| " +
				  "(SELECT narrative FROM DETB_RTL_TELLER WHERE TRN_REF_NO = a.trn_ref_no) || ' ' || " +
				  "(select distinct ADDL_TEXT from DETB_JRNL_TXN_DETAIL where reference_no = a.trn_ref_no and serial_no = a.curr_no)||' '|| s.trn_desc " +
				  "|| ' ' || decode(s.trn_desc,'SI', (select internal_remarks from  SITB_CONTRACT_MASTER where contract_ref_no = a.trn_ref_no " +
				  "AND version_no  = (select max(version_no) from sitb_contract_master WHERE contract_ref_no  = a.TRN_REF_NO)), " +
				  "decode(a.related_account, '','' , ' For the Account No.'||a.related_account) || decode(a.module, 'CI', ' a.n '||y.customer_name1,'')) || ' ' ||  " +
				  "(select s.user_ref_no || ' ' || decode(s.module_code, 'IB', 'Mtr_dt: '||to_char(u.maturity_date,'YYYY-MM-DD') || ' '  || u.ket2,  " +
				  "'Exp_dt: '||to_char(t.expiry_date,'YYYY-MM-DD') || ' ' || t.ket1) " +
				  "from cstb_contract s, " +
				  "(select a.contract_ref_no, a.expiry_date, ('APP: ' || b.cust_name || ' & ' || 'BEN: '|| c.cust_name) ket1 " +
				  "from LCTBS_CONTRACT_MASTER a, " +
				  "(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'APP') b, " +
				  "(SELECT contract_ref_no, event_seq_no, cust_name FROM LCTB_PARTIES WHERE party_type = 'BEN') c " +
				  "where a.contract_ref_no = b.contract_ref_no " +
				  "and a.contract_ref_no = c.contract_ref_no and a.event_seq_no = b.event_seq_no and a.event_seq_no = c.event_seq_no " +
				  "and a.version_no = (select max(version_no) from LCTBS_CONTRACT_MASTER where contract_ref_no = a.contract_ref_no)) t, " +
				  "(select d.bcrefno, d.maturity_date, ('DRAWEE: ' || e.party_name || ' & ' || 'DRAWER: '|| f.party_name) ket2 " +
				  "from BCTB_CONTRACT_MASTER d, " +
				  "(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWEE') e, " +
				  "(select bcrefno, event_seq_no, party_name from BCTB_CONTRACT_PARTIES WHERE party_type = 'DRAWER') f " +
				  "where d.bcrefno = e.bcrefno and d.bcrefno = f.bcrefno and d.event_seq_no = e.event_seq_no " +
				  "and d.event_seq_no = f.event_seq_no " +
				  "and d.version_no = (select max(version_no) from BCTB_CONTRACT_MASTER where bcrefno = d.bcrefno)) u " +
				  "where s.contract_ref_no = t.contract_ref_no(+) and s.contract_ref_no = u.bcrefno(+) " +
				  "and s.contract_ref_no = a.trn_ref_no " +
				  "group by s.user_ref_no, s.module_code, t.expiry_date, u.maturity_date, t.ket1, u.ket2)) keterangan, " +
				  "a.drcr_ind, decode(a.ac_ccy, 'IDR', a.lcy_amount, a.fcy_amount) as jumlah, a.lcy_amount as ekivalen, a.auth_stat " +
				  "from ACVWS_ALL_AC_ENTRIES_HIST a , sttms_trn_code s , cltb_account_apps_master x, sttm_customer y " +
				  "where a.ac_no in (?, ?) " +
	          	  "and a.trn_dt >= to_date (?, 'DD-MM-YYYY') and a.trn_dt <= to_date (? , 'DD-MM-YYYY') " +
	          	  "and a.trn_code = s.trn_code and a.related_account = x.account_number(+) " +
	          	  "and a.ac_branch  = x.branch_code(+) and x.customer_id = y.customer_no(+) " +
	          	  "order by a.ac_no, a.AC_ENTRY_SR_NO, a.trn_dt";
			stat = conn.prepareStatement(sql);			
			stat.setString(1, norek1);		
			stat.setString(2, norek2);
			stat.setString(3, dt1);
			stat.setString(4, dt2);
			rs = stat.executeQuery();
			BigDecimal tempSld = sldawal;
			while (rs.next()) {
				ht = new HistTransRek();
				if ("3170003148".equals(norek1.trim()) && 
						(
								("7018547131610003".equals(rs.getString("trn_ref_no")) && "2013-06-10".equals(rs.getString("trn_dt").substring(0,10))) || 
								("701IJ70131850001".equals(rs.getString("trn_ref_no")) && "2013-07-04".equals(rs.getString("trn_dt").substring(0,10)))
						)
					){
					continue;
				}
				if("ZZX".equals(rs.getString("trn_code").toUpperCase().trim()) && "CONVERSION - :,".equals(rs.getString("keterangan").toUpperCase().trim())){
					continue;
				}
				ht.setAccount(rs.getString("ac_no"));
				ht.setTrn_ref_no(rs.getString("trn_ref_no"));
				ht.setInstrument_code(rs.getString("instrument_code") == null ? "" : rs.getString("instrument_code"));
				ht.setTrn_dt(rs.getString("trn_dt"));
				ht.setVal_dt(rs.getString("value_dt"));
				ht.setTrn_code(rs.getString("trn_code"));
				ht.setDesc(rs.getString("keterangan"));
				ht.setDrcr(rs.getString("drcr_ind"));
				ht.setNominal(rs.getBigDecimal("jumlah") == null ? new BigDecimal(0) : rs.getBigDecimal("jumlah"));
				BigDecimal t = new BigDecimal(0);
				if(rs.getBigDecimal("jumlah") != null){
					t = rs.getBigDecimal("jumlah");
				}
				if("D".equals(rs.getString("drcr_ind"))){
					tempSld = tempSld.subtract(t);
					jmlItmD++;
					totD = totD.add(ht.getNominal());
				}else if ("C".equals(rs.getString("drcr_ind"))){
					tempSld = tempSld.add(t);
					jmlItmC++;
					totC = totC.add(ht.getNominal());
				}				
				ht.setSaldo(tempSld);
				sld_akhir = ht.getSaldo();
				if(rs.getBigDecimal("ekivalen") != null){
					ht.setEkivalen(rs.getBigDecimal("ekivalen"));
				} else {
					ht.setEkivalen(new BigDecimal(0));
				}
				ht.setAuth_stat(rs.getString("auth_stat"));
				lbb.add(ht);
			}
		} catch (SQLException ex) {
			log.error(" getListCustom : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
}
