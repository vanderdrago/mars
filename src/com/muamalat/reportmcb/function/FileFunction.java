package com.muamalat.reportmcb.function;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.helpers.LogLog;

import com.muamalat.reportmcb.bean.IntfRtgsOutBean;
import com.muamalat.reportmcb.entity.Bukubesar;
import com.muamalat.reportmcb.entity.Gl;
import com.muamalat.reportmcb.entity.Jobtrans;
import com.muamalat.reportmcb.entity.Lsp;
import com.muamalat.reportmcb.entity.Mars_report_desc;
import com.muamalat.reportmcb.entity.ReportEom;
import com.muamalat.reportmcb.entity.ReportKoran;
import com.muamalat.reportmcb.entity.ReportNeraca;
import com.muamalat.reportmcb.entity.RtgsSknMcb;
import com.muamalat.reportmcb.entity.RtgsTtpn;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.singleton.DatasourceEntry;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

public class FileFunction {
	
	private static Logger log = Logger.getLogger(FinSqlFunction.class);
    private BigDecimal totMutDb;
    private BigDecimal totMutCb;

    public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                log.error("closeConnDb 1 : " + ex.getMessage());
            }
        }
        if (rs != null) {
            try {
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 2 : " + ex.getMessage());
            }
        }
        if (stat != null) {
            try {
                stat.close();
                stat = null;
            } catch (SQLException ex) {
                log.error("closeConnDb 3 : " + ex.getMessage());
            }
        }
    }
	
	public boolean createFileLaporanHarian(String pathfilename, String lineTransaksi) {
        boolean ret = false;
        File f;
        BufferedWriter bw = null;

        f = new File(pathfilename);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                //System.out.println("error in createLaporanHarian : " + ex);
            }
        }
        try {
            bw = new BufferedWriter(new FileWriter(pathfilename, true));
            bw.write(lineTransaksi);
            bw.newLine();
            bw.flush();
            ret = true;
        } catch (IOException ioe) {
            //System.out.println("error in createLaporanHarian ioe2 : " + ioe);
        } finally {                       // always close the file
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ioe2) {
                    //System.out.println("error in createLaporanHarian ioe2 : " + ioe2);
                }
            }
        }
        return ret;
    }
	
	public boolean deleteFile(String srFile) {
        boolean ret = false;
        try {

            File f1 = new File(srFile);
            boolean success = f1.delete();
            if (!success) {
                System.out.println("Deletion failed.");
            } else {
                ret = true;
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " in the specified directory.");
        }
        return ret;
    }
	
	public void saveFile(String filePath, byte[] fileData, String fileName) {
	    try {
	      cekFolder(filePath);
	      File fileToCreate = new File(filePath, fileName);
	      if (!fileToCreate.exists()) {
	        FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);
	        fileOutStream.write(fileData);
	        fileOutStream.flush();
	        fileOutStream.close();
	      }
	    }
	    catch (Exception ex)
	    {
	      log.error(ex.getMessage());
	    }
	  }
	private void cekFolder(String filePath) {
	    File folderData = new File(filePath);
	    if (folderData.exists()) {
	      if (!folderData.isDirectory())
	        folderData.mkdir();
	    }
	    else
	      folderData.mkdir();
	  }
	public String formatField(String stData, int intLength, String chPad,boolean bl) {
		if (stData == null){
			stData="";
		}
		int len = stData.length();
		if (len > intLength) {
			stData = stData.substring(0, intLength);
			len = intLength;
		}
		len = intLength - len;
		StringBuffer sb = new StringBuffer(stData);
		for (int i = 0; i < len; i++) {
			if (bl) {
				sb.insert(0, chPad);
			} else {
				sb.append(chPad);
			}
		}
		return sb.toString();
	}
	
//	public void createFile(String pathfile, Saldo sd, Gl gl, String acc, String ccy, String brnch, String tgl1, String tgl2, String cbsrekoto){
//		List lbb = new ArrayList();
//        SqlFunction f = new SqlFunction();
//        BigDecimal saldoakhir = new BigDecimal(0);
//    	
//        if ("IDR".equals(ccy)) {
//        	lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal());
//    	} else {
//    		lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal());
//    	}
//		   
//    	if ("IDR".equals(ccy)) {
//          saldoakhir = sd.getLcy_opening_bal().add(f.getTotMutDb()).add(f.getTotMutCb());
//    	} else {
//          saldoakhir = sd.getAcy_opening_bal().add(f.getTotMutDb()).add(f.getTotMutCb());
//    	}
//    	DecimalFormat myFormatter = new DecimalFormat("###,##0.00");
//		String header = "";
//		header = formatField("Nomor Rekening", 20, " ", false) + ": " + gl.getGl_code() + "/" + gl.getGl_desc() + "\r\n";
//    	header += formatField("Kantor/Cabang", 20, " ", false) + ": " + brnch + "\r\n";
//    	header += formatField("Valuta", 20, " ", false) + ": " + ccy + "\r\n";
//    	header += formatField("Saldo Awal", 20, " ", false) + ": " + myFormatter.format(sd.getAcy_opening_bal()) 
//    				+ " Saldo Eq " + myFormatter.format(sd.getAcy_opening_bal()) + "\r\n";
//    	header += formatField("Saldo Akhir", 20, " ", false) + ": "+ myFormatter.format(saldoakhir)+ "\r\n";
//        createFileLaporanHarian(pathfile, header);
//		String linetrans = "";           
//		if ("IDR".equals(ccy)) {
//	        linetrans = formatField("", 321, "-", false) + "\r\n";		
//		} else {			
//	        linetrans = formatField("", 386, "-", false) + "\r\n";
//		}
//        createFileLaporanHarian(pathfile, linetrans);
//        linetrans = formatField("TRN DT", 12, " ", false);
//        linetrans += formatField("VALUE DATE", 12, " ", false);
//        linetrans += formatField("REF NO", 18, " ", false);
//        linetrans += formatField("BATCH NO", 6, " ", false);
//        linetrans += formatField("RCC NO", 10, " ", false);
//        linetrans += formatField("ACCOUNT NO", 11, " ", false);
//        linetrans += formatField("CCY", 5, " ", false);
//        linetrans += formatField("EVENT", 6, " ", false);
//        linetrans += formatField("MAKER ID", 14, " ", false);
//        linetrans += formatField("AUTHORIZER ID", 14, " ", false);
//        linetrans += formatField("TRN CD", 5, " ", false);
//        linetrans += formatField("DESCRIPTION", 115, " ", false);
//        linetrans += formatField("MUTASI DR", 30, " ", true);
//        linetrans += formatField("MUTASI CR", 30, " ", true);
//        if (!"IDR".equals(ccy)) {
//            linetrans += formatField("EKIVALEN DR", 30, " ", true);
//            linetrans += formatField("EKIVALEN CR", 30, " ", true);        	
//        }
//        linetrans += formatField("SALDO", 30, " ", true) + "\r\n";
//        createFileLaporanHarian(pathfile, linetrans);
//        if ("IDR".equals(ccy)) {
//	        linetrans = formatField("", 321, "-", false) + "\r\n";			
//		} else {			
//	        linetrans = formatField("", 386, "-", false) + "\r\n";
//		}
//        createFileLaporanHarian(pathfile, linetrans);
//		if (lbb.size() > 0) {
//			for (Iterator<Bukubesar> i = lbb.iterator(); i.hasNext();) {
//				Bukubesar b = i.next();
//				linetrans = "";
//                linetrans += formatField(b.getTrn_dt().toString(), 12, " ", false);
//                linetrans += formatField(b.getValue_dt().toString(), 12, " ", false);
//                linetrans += formatField(b.getTrn_ref_no(), 18, " ", false);
//                linetrans += formatField(b.getBatch_no(), 6, " ", false);
//                linetrans += formatField(b.getRcc_no(), 10, " ", false);
//                linetrans += formatField(b.getAc_no(), 11, " ", false);
//                linetrans += formatField(b.getAc_ccy(), 5, " ", false);
//                linetrans += formatField(b.getEvent(), 6, " ", false);
//                linetrans += formatField(b.getMaker_id(), 14, " ", false);
//                linetrans += formatField(b.getAuth_id(), 14, " ", false);
//                linetrans += formatField(b.getTrn_code(), 5, " ", false);
//                linetrans += formatField(b.getKet(), 115, " ", false);
//                linetrans += formatField(myFormatter.format(b.getAmt_db()), 30, " ", true);
//                linetrans += formatField(myFormatter.format(b.getAmt_cr()), 30, " ", true); 
//                if (!"IDR".equals(ccy)) {
//                    linetrans += formatField(myFormatter.format(b.getAmt_db_eq()), 30, " ", true);
//                    linetrans += formatField(myFormatter.format(b.getAmt_cr_eq()), 30, " ", true);       	
//                }
//                linetrans += formatField(myFormatter.format(b.getSaldo()), 30, " ", true) + "\r";
//                createFileLaporanHarian(pathfile, linetrans);
//			}				
//		}
//		if ("IDR".equals(ccy)) {
//	        linetrans = formatField("", 321, "-", false) + "\r\n";		
//		} else {			
//	        linetrans = formatField("", 386, "-", false) + "\r\n";
//		}
////        createFileLaporanHarian(pathfile, linetrans);
////        linetrans = formatField("", 241, " ", false);
////        linetrans += formatField(myFormatter.format(f.getTotMutDb()), 30, " ", true);
////        linetrans += formatField(myFormatter.format(f.getTotMutCb()), 30, " ", true);
//        createFileLaporanHarian(pathfile, linetrans);
//	}
	
	//penambahan allcab-allval, AGUSTUS 2016
//	public void createFile(String pathfile, Saldo sd, Gl gl, String acc, String ccy, String brnch, String tgl1, String tgl2, String cbsrekoto){
public void createFile(String pathfile, Saldo sd, Gl gl, String acc, String ccy, String brnch, String tgl1, String tgl2, String cbsrekoto, String allcab, String allval){
	List lbb = new ArrayList();
    SqlFunction f = new SqlFunction();
    BigDecimal saldoakhir = new BigDecimal(0);
	
    if ("IDR".equals(ccy)) {
    	//lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal());
		lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal(), allcab, allval);
	} else {
		//lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal());
		lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal(), allcab, allval);
	}
	   
	if ("IDR".equals(ccy)) {
      saldoakhir = sd.getLcy_opening_bal().add(f.getTotMutDb()).add(f.getTotMutCb());
	} else {
      saldoakhir = sd.getAcy_opening_bal().add(f.getTotMutDb()).add(f.getTotMutCb());
	}
	DecimalFormat myFormatter = new DecimalFormat("###,##0.00");
	String header = "";
	header = formatField("Nomor Rekening", 20, " ", false) + ": " + gl.getGl_code() + "/" + gl.getGl_desc() + "\r\n";
	header += formatField("Kantor/Cabang", 20, " ", false) + ": " + brnch + "\r\n";
	header += formatField("Valuta", 20, " ", false) + ": " + ccy + "\r\n";
	header += formatField("Saldo Awal", 20, " ", false) + ": " + myFormatter.format(sd.getAcy_opening_bal()) 
				+ " Saldo Eq " + myFormatter.format(sd.getAcy_opening_bal()) + "\r\n";
	header += formatField("Saldo Akhir", 20, " ", false) + ": "+ myFormatter.format(saldoakhir)+ "\r\n";
    createFileLaporanHarian(pathfile, header);
	String linetrans = "";           
	if ("IDR".equals(ccy)) {
        linetrans = formatField("", 326, "-", false) + "\r\n";		
	} else {			
        linetrans = formatField("", 386, "-", false) + "\r\n";
	}
    createFileLaporanHarian(pathfile, linetrans);
    linetrans = formatField("TRN DT", 12, " ", false);
    linetrans += formatField("VALUE DATE", 12, " ", false);
    linetrans += formatField("TXN DT TIME", 20, " ", false);
//penambahan cabang
    linetrans += formatField("BRANCH", 10, " ", false);
//	
    linetrans += formatField("REF NO", 18, " ", false);
    linetrans += formatField("BATCH NO", 6, " ", false);
    linetrans += formatField("RCC NO", 10, " ", false);
    linetrans += formatField("ACCOUNT NO", 11, " ", false);
    linetrans += formatField("CCY", 5, " ", false);
    linetrans += formatField("EVENT", 6, " ", false);
    linetrans += formatField("MAKER ID", 14, " ", false);
    linetrans += formatField("AUTHORIZER ID", 14, " ", false);
    linetrans += formatField("TRN CD", 10, " ", false);
    linetrans += formatField("DESCRIPTION", 100, " ", false);
    linetrans += formatField("MUTASI DR", 25, " ", true);
    linetrans += formatField("MUTASI CR", 25, " ", true);
    if (!"IDR".equals(ccy)) {
        linetrans += formatField("EKIVALEN DR", 25, " ", true);
        linetrans += formatField("EKIVALEN CR", 25, " ", true);        	
    }
    linetrans += formatField("SALDO", 25, " ", true) + "\r\n";
    createFileLaporanHarian(pathfile, linetrans);
    if ("IDR".equals(ccy)) {
        linetrans = formatField("", 326, "-", false) + "\r\n";			
	} else {			
        linetrans = formatField("", 326, "-", false) + "\r\n";
	}
    createFileLaporanHarian(pathfile, linetrans);
	if (lbb.size() > 0) {
		for (Iterator<Bukubesar> i = lbb.iterator(); i.hasNext();) {
			Bukubesar b = i.next();
			linetrans = "";
            linetrans += formatField(b.getTrn_dt().toString(), 12, " ", false);
            linetrans += formatField(b.getValue_dt().toString(), 12, " ", false);
            linetrans += formatField(b.getTxn_dt_time().toString(), 20, " ", false);
//penambahan cabang
            linetrans += formatField(b.getAc_branch(), 10, " ", false);
//
            linetrans += formatField(b.getTrn_ref_no(), 18, " ", false);
            linetrans += formatField(b.getBatch_no(), 6, " ", false);
            linetrans += formatField(b.getRcc_no(), 10, " ", false);
            linetrans += formatField(b.getAc_no(), 11, " ", false);
            linetrans += formatField(b.getAc_ccy(), 5, " ", false);
            linetrans += formatField(b.getEvent(), 6, " ", false);
            linetrans += formatField(b.getMaker_id(), 14, " ", false);
            linetrans += formatField(b.getAuth_id(), 14, " ", false);
            linetrans += formatField(b.getTrn_code(), 10, " ", false);
            linetrans += formatField(b.getKet(), 100, " ", false);
            linetrans += formatField(myFormatter.format(b.getAmt_db()), 25, " ", true);
            linetrans += formatField(myFormatter.format(b.getAmt_cr()), 25, " ", true); 
            if (!"IDR".equals(ccy)) {
                linetrans += formatField(myFormatter.format(b.getAmt_db_eq()), 25, " ", true);
                linetrans += formatField(myFormatter.format(b.getAmt_cr_eq()), 25, " ", true);       	
            }
            linetrans += formatField(myFormatter.format(b.getSaldo()), 25, " ", true) + "\r";
            createFileLaporanHarian(pathfile, linetrans);
		}				
	}
	if ("IDR".equals(ccy)) {
        linetrans = formatField("", 326, "-", false) + "\r\n";		
	} else {			
        linetrans = formatField("", 326, "-", false) + "\r\n";
	}
//    createFileLaporanHarian(pathfile, linetrans);
//    linetrans = formatField("", 241, " ", false);
//    linetrans += formatField(myFormatter.format(f.getTotMutDb()), 30, " ", true);
//    linetrans += formatField(myFormatter.format(f.getTotMutCb()), 30, " ", true);
    createFileLaporanHarian(pathfile, linetrans);
}

//penambahan BukuBesarGL, Agustus 2016
//public void createFile(String pathfile, Saldo sd, Gl gl, String acc, String ccy, String brnch, String tgl1, String tgl2, String cbsrekoto, String allcab, String allval){
	public void createFileGL(String pathfile, Saldo sd, Gl gl, String acc, String acc2, String acc3, String acc4, String acc5,
			String acc6, String acc7, String acc8, String acc9, String acc10, String tgl1, String tgl2){
		List lbb = new ArrayList();
		SqlFunction f = new SqlFunction();
		BigDecimal saldoakhir = new BigDecimal(0);
			    	
/*		if ("IDR".equals(ccy)) {
			lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal(), allcab, allval);
		} else {
			lbb = f.getListBukubesar(tgl1, tgl2, acc, brnch, ccy, cbsrekoto, sd.getLcy_opening_bal(), allcab, allval);
		}
*/		lbb = f.getListBukubesarGL(tgl1, tgl2, acc, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, acc10, sd.getLcy_opening_bal());
		System.out.println("saldo akhir pertama: "+saldoakhir);
/*		if ("IDR".equals(ccy)) {
			saldoakhir = sd.getLcy_opening_bal().add(f.getTotMutDb()).add(f.getTotMutCb());
		} else {
			saldoakhir = sd.getAcy_opening_bal().add(f.getTotMutDb()).add(f.getTotMutCb());
		}
*/
		DecimalFormat myFormatter = new DecimalFormat("###,##0.00");
		String header = "";
		header = formatField("BUKU BESAR GL", 14, " ", false) + "\r\n";
/*		header = formatField("Nomor Rekening", 20, " ", false) + ": " + gl.getGl_code() + "/" + gl.getGl_desc() + "\r\n";
		header += formatField("Kantor/Cabang", 20, " ", false) + ": " + brnch + "\r\n";
		header += formatField("Valuta", 20, " ", false) + ": " + ccy + "\r\n";
		header += formatField("Saldo Awal", 20, " ", false) + ": " + myFormatter.format(sd.getAcy_opening_bal()) 
			+ " Saldo Eq " + myFormatter.format(sd.getAcy_opening_bal()) + "\r\n";
		header += formatField("Saldo Akhir", 20, " ", false) + ": "+ myFormatter.format(saldoakhir)+ "\r\n";
*/
		System.out.println("saldo akhir kedua: "+saldoakhir);
			    	
		createFileLaporanHarian(pathfile, header);
		String linetrans = "";
		
/*		if ("IDR".equals(ccy)) {
			linetrans = formatField("", 251, "-", false) + "\r\n";		
		} else {			
			linetrans = formatField("", 316, "-", false) + "\r\n";
		}
*/
		linetrans = formatField("", 283, "-", false);
		createFileLaporanHarian(pathfile, linetrans);
		
		linetrans = formatField("TRN DT", 15, " ", false);
		linetrans += formatField("VALUE DATE", 15, " ", false);
		//penambahan cabang
		linetrans += formatField("BRANCH", 10, " ", false);
		//
		linetrans += formatField("REF NO", 20, " ", false);
		linetrans += formatField("BATCH NO", 12, " ", false);
		linetrans += formatField("RCC NO", 10, " ", false);
		linetrans += formatField("ACCOUNT NO", 14, " ", false);
		linetrans += formatField("CCY", 7, " ", false);
		linetrans += formatField("EVENT", 8, " ", false);
		linetrans += formatField("MAKER ID", 14, " ", false);
		linetrans += formatField("AUTHORIZER ID", 16, " ", false);
		linetrans += formatField("TRN CD", 10, " ", false);
		linetrans += formatField("DESCRIPTION", 60, " ", false);
		linetrans += formatField("MUTASI DR", 24, " ", true);
		linetrans += formatField("MUTASI CR", 24, " ", true);
/*		if (!"IDR".equals(ccy)) {
			linetrans += formatField("EKIVALEN DR", 25, " ", true);
			linetrans += formatField("EKIVALEN CR", 25, " ", true);        	
		}
*/
		linetrans += formatField("SALDO", 23, " ", true);
//		createFileLaporanHarian(pathfile, linetrans);
/*		if ("IDR".equals(ccy)) {
			linetrans = formatField("", 251, "-", false) + "\r\n";			
		} else {			
			linetrans = formatField("", 316, "-", false) + "\r\n";
		}
*/
		createFileLaporanHarian(pathfile, linetrans);
		linetrans = formatField("", 283, "-", false) + "\r\n";
		createFileLaporanHarian(pathfile, linetrans);
		
		if (lbb.size() > 0) {
			for (Iterator<Bukubesar> i = lbb.iterator(); i.hasNext();) {
				Bukubesar b = i.next();
				linetrans = "";
			    linetrans += formatField(b.getTrn_dt().toString(), 15, " ", false);
			    linetrans += formatField(b.getValue_dt().toString(), 15, " ", false);
			    //penambahan cabang
			    linetrans += formatField(b.getAc_branch(), 10, " ", false);
			    //
			    linetrans += formatField(b.getTrn_ref_no(), 20, " ", false);
			    linetrans += formatField(b.getBatch_no(), 12, " ", false);
			    linetrans += formatField(b.getRcc_no(), 10, " ", false);
			    linetrans += formatField(b.getAc_no(), 14, " ", false);
			    linetrans += formatField(b.getAc_ccy(), 7, " ", false);
			    linetrans += formatField(b.getEvent(), 8, " ", false);
			    linetrans += formatField(b.getMaker_id(), 14, " ", false);
			    linetrans += formatField(b.getAuth_id(), 16, " ", false);
			    linetrans += formatField(b.getTrn_code(), 10, " ", false);
			    linetrans += formatField(b.getKet(), 60, " ", false);
			    linetrans += formatField(myFormatter.format(b.getAmt_db()), 24, " ", true);
			    linetrans += formatField(myFormatter.format(b.getAmt_cr()), 24, " ", true); 
/*			    if (!"IDR".equals(ccy)) {
			        linetrans += formatField(myFormatter.format(b.getAmt_db_eq()), 25, " ", true);
			        linetrans += formatField(myFormatter.format(b.getAmt_cr_eq()), 25, " ", true);       	
			    }
*/
			    linetrans += formatField(myFormatter.format(b.getSaldo()), 24, " ", true) + "\r";
			    createFileLaporanHarian(pathfile, linetrans);
			}				
		}
/*		if ("IDR".equals(ccy)) {
			linetrans = formatField("", 251, "-", false) + "\r\n";		
		} else {			
			linetrans = formatField("", 316, "-", false) + "\r\n";
		}
*/
//		createFileLaporanHarian(pathfile, linetrans);
//		linetrans = formatField("", 241, " ", false);
//		linetrans += formatField(myFormatter.format(f.getTotMutDb()), 30, " ", true);
//		linetrans += formatField(myFormatter.format(f.getTotMutCb()), 30, " ", true);
		createFileLaporanHarian(pathfile, linetrans);
		linetrans = formatField("", 283, "-", false);
		createFileLaporanHarian(pathfile, linetrans);
	}
	
	public void createFileLsp(String pathfile, String ssl1, String ssl2, String brnch, String val, List lsp){
		SqlFunction f = new SqlFunction(); 
    	DecimalFormat myFormatter = new DecimalFormat("###,##0.00");
		String header = "";
		header = formatField("Nomor SSL", 15, " ", false) + ": " + ssl1 + " - " + ssl2 + "\r\n";
    	header += formatField("Kantor/Cabang", 15, " ", false) + ": " + brnch + "\r\n";
    	header += formatField("Valuta", 15, " ", false) + ": " + val +"\r";
        createFileLaporanHarian(pathfile, header);
		String linetrans = "";            
        linetrans = formatField("", 87, "-", false)+"\r";
        createFileLaporanHarian(pathfile, linetrans);
        linetrans = formatField("NO SSL", 11, " ", false);
        linetrans += formatField("NAMA SSL", 30, " ", false);
        linetrans += formatField("CABANG", 8, " ", false);
        linetrans += formatField("VALUTA", 8, " ", false);
        linetrans += formatField("SALDO PERCOBAAN", 30, " ", true)+"\r";
        createFileLaporanHarian(pathfile, linetrans);
        linetrans = formatField("", 87, "-", false)+"\r";
        createFileLaporanHarian(pathfile, linetrans);
		if (lsp.size() > 0) {
			for (Iterator<Lsp> i = lsp.iterator(); i.hasNext();) {
				Lsp b = i.next();
				linetrans = "";
                linetrans += formatField(b.getGl_code(), 11, " ", false);
                linetrans += formatField(b.getGl_desc(), 30, " ", false);
                linetrans += formatField(b.getBranch_code(), 8, " ", false);
                linetrans += formatField(b.getCcy(), 8, " ", false);
                linetrans += formatField(myFormatter.format(b.getMov_lcy()), 30, " ", true) + "\r";
                createFileLaporanHarian(pathfile, linetrans);
			}				
		}
        linetrans = formatField("", 87, "-", false)+"\r";
        createFileLaporanHarian(pathfile, linetrans);
	}
	
	public List getFileList(String src){
        List lfl = new ArrayList();
        File folder = new File(src);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles.length > 0) {
        	ReportNeraca rn = null; 
        	LocalFunction lf = new LocalFunction();
            for (int i = 0; i < listOfFiles.length; i++) {
                String flnm = listOfFiles[i].getName();
                if(!"CSS".equals(flnm.substring(flnm.length() - 3, flnm.length()).toUpperCase())){
	                try {
	                    boolean isdirectory = new File(src + "/" + flnm).getCanonicalFile().isDirectory();
	                    if (!isdirectory) {       
	                    	Mars_report_desc rpt = lf.stsDisplay(flnm.substring(0, flnm.length() - 17));
	                    	if (rpt == null){          
	                            rn = new ReportNeraca(); 
	                            rn.setNamefile(flnm); 
		                        rn.setDescription("BELUM DIDEFINISIKAN");   
		                        lfl.add(rn);                 		
	                    	} else {
		                    	if (rpt.isSts_display()){
		                            rn = new ReportNeraca(); 
		                            rn.setNamefile(flnm); 
			                        rn.setDescription(rpt.getDesc());
			                        lfl.add(rn);
		                    	}
	                    	}
	                    }
	
	                } catch (IOException e) {
	                    //System.out.println("Error :: " + e.getMessage());
	                	e.printStackTrace();
	                }
                }
            }
        }
        return lfl;
    }
	
	public List getFileListCognos(String src){
        List lfl = new ArrayList();
        File folder = new File(src);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles.length > 0) {
        	ReportNeraca rn = null; 
        	LocalFunction lf = new LocalFunction();
            for (int i = 0; i < listOfFiles.length; i++) {
                String flnm = listOfFiles[i].getName();
                if(!"CSS".equals(flnm.substring(flnm.length() - 3, flnm.length()).toUpperCase())){
	                try {
	                    boolean isdirectory = new File(src + "/" + flnm).getCanonicalFile().isDirectory();
	                    if (!isdirectory) {         
	                    	
	                    	System.out.println("Substring File Cognos = "+flnm.substring(26, flnm.length() - 4));
	                    	Mars_report_desc rpt = lf.stsDisplay(flnm.substring(26, flnm.length() - 4));
	                    	
	                    	if (rpt == null){          
	                            rn = new ReportNeraca(); 
	                            rn.setNamefile(flnm); 
		                        rn.setDescription("BELUM DIDEFINISIKAN");   
		                        lfl.add(rn);                 		
	                    	} else {
		                    	if (rpt.isSts_display()){
		                            rn = new ReportNeraca(); 
		                            rn.setNamefile(flnm); 
			                        rn.setDescription(rpt.getDesc());
			                        lfl.add(rn);
		                    	}
	                    	}
	                    }
	
	                } catch (IOException e) {
	                    //System.out.println("Error :: " + e.getMessage());
	                }
                }
            }
        }
        return lfl;
    }
	
	/*
	public String getdescription(String fd) {
		String desc = "";
		String temp = fd.substring(0, fd.length() - 17);
		if("LABARUGI_RINCI_PER_CAB".equals(temp)){
			desc = "LAPORAN LABA RUGI RINCI PER CABANG";
		}else if("LABARUGI_SINGKAT_PER_CAB".equals(temp)){
			desc = "LAPORAN LABA RUGI SINGKAT PER CABANG";
		}else if("NERACA_RINCI_PER_CAB".equals(temp)){
			desc = "LAPORAN NERACA RINCI PER CABANG";
		}else if("NERACA_RINCI_KONSOL".equals(temp)){
			desc = "LAPORAN NERACA RINCI KONSOLIDASI";
		}else if("NERACA_SINGKAT_PER_CAB".equals(temp)){
			desc = "LAPORAN NERACA SINGKAT PER CABANG";
		}else if("NERACA_SINGKAT_KONSOL".equals(temp)){
			desc = "LAPORAN NERACA SINGKAT KONSOLIDASI";
		}else if("TRIAL_BAL_PER_CAB".equals(temp)){
			desc = "LAPORAN TRIAL BALANCE PER CABANG";
		}else if("TRIAL_BAL_KONSOL".equals(temp)){
			desc = "LAPORAN TRIAL BALANCE KONSOLIDASI";
		}else if("MCB_DATA_N".equals(temp)){
			desc = "LAPORAN DATA N MCB";
		}else if("MCB_DATA_N_VALUELESS_ZERO".equals(temp)){
			desc = "LAPORAN DATA N MCB TANPA NILAI NOL";
		}else if("FIX_ASSETS_PER_CAB".equals(temp)){
			desc = "LAPORAN FIX ASSETS PER CABANG";
		}else if("LAPORAN_BDDC".equals(temp)){
			desc = "LAPORAN BIAYA DIBAYAR DIMUKA KONSOLIDASI";
		}else if("LAPORAN_BDD".equals(temp)){
			desc = "LAPORAN BIAYA DIBAYAR DIMUKA CABANG";
		}else if("LAPORAN_FCCL".equals(temp)){
			desc = "LAPORAN FCCL PER CABANG";
		}else if("LAPORAN_FOREIGN_CURRENCY".equals(temp)){
			desc = "LAPORAN FOREIGN CURRENCY CABANG";
		}else if("LAPORAN_ABC".equals(temp)){
			desc = "LAPORAN ABC PER CABANG";
		}else if("LAPORAN_ABCC".equals(temp)){
			desc = "LAPORAN ABC KONSOLIDASI";
		}else if("SALDOKASATM".equals(temp)){
			desc = "LAPORAN SALDO KAS ATM";
		}else if("LABARUGI_RINCI_KONSOL".equals(temp)){
			desc = "LAPORAN LABA RUGI RINCI KONSOLIDASI";
		}else if("LABARUGI_SINGKAT_KONSOL".equals(temp)){
			desc = "LAPORAN LABA RUGI SINGKAT KONSOLIDASI";
		}else if("LPNOM".equals(temp)){
			desc = "LAPORAN LPNOM PER CABANG";
		}else if("LPNOM_KONSOLIDASI".equals(temp)){
			desc = "LAPORAN LPNOM KONSOLIDASI";
		}else if("SSLNIHIL".equals(temp)){
			desc = "LAPORAN SSL HARUS NIHIL";
		}else if("DATAN_VALAS_ADHOC".equals(temp)){
			desc = "LAPORAN DATAN VALAS ADHOC";
		}else if("LAPORAN_FIX_ASSETS_KONS".equals(temp)){
			desc = "LAPORAN FIX ASSETS KONSOLIDASI";
		}else if("REPORT_TRAVAL".equals(temp)){
			desc = "LAPORAN TRAVAL";
		}else if("LAPORAN_DP3_PRKC".equals(temp)){
			desc = "LAPORAN DP3 PRK KONSOLIDASI";
		}else if("LAPORAN_TD_BAL".equals(temp)){
			desc = "LAPORAN TERM DEPOSIT BALANCE";
		}else if("GIRO_BAL".equals(temp)){
			desc = "LAPORAN GIRO BALANCE";
		}else if("CASA_BAL".equals(temp)){
			desc = "LAPORAN CASA BALANCE";
		}else {
			desc = "BELUM DIDEFINISIKAN";
		}
		return desc;
		
	}
	*/
	public List getRekonList(String src){
        List lfl = new ArrayList();
        File folder = new File(src);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles.length > 0) {
        	ReportNeraca rn = null; 
        	LocalFunction lf = new LocalFunction();
            for (int i = 0; i < listOfFiles.length; i++) {
                String flnm = listOfFiles[i].getName();
                try {
                    boolean isdirectory = new File(src + "/" + flnm).getCanonicalFile().isDirectory();
                    if (!isdirectory) {
                    	Mars_report_desc rpt = lf.stsDisplay(flnm.substring(0, flnm.length() - 13));
                    	if (rpt == null){          
                            rn = new ReportNeraca(); 
                            rn.setNamefile(flnm); 
	                        rn.setDescription("BELUM DIDEFINISIKAN");   
	                        lfl.add(rn);                 		
                    	} else {
	                    	if (rpt.isSts_display()){
	                            rn = new ReportNeraca(); 	
	                            rn.setNamefile(flnm); 
		                        rn.setDescription(rpt.getDesc());
		                        lfl.add(rn); 
	                    	}
                    	} 
                    }

                } catch (IOException e) {
                    //System.out.println("Error :: " + e.getMessage());
                }
            }
        }
        return lfl;
    }
	
	public List getEomList(String src){
        List lfl = new ArrayList();
        File folder = new File(src);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles.length > 0) {
        	ReportEom re = null; 
        	LocalFunction lf = new LocalFunction();
            for (int i = 0; i < listOfFiles.length; i++) {
                String flnm = listOfFiles[i].getName();
                try {
                    boolean isdirectory = new File(src + "/" + flnm).getCanonicalFile().isDirectory();
                    if (!isdirectory) {
                    	Mars_report_desc rpt = lf.stsDisplay(flnm.substring(0, flnm.length() - 15));
                    	if (rpt == null){          
                    		re = new ReportEom();  
                    		re.setNamefile(flnm); 
                    		re.setDescription("BELUM DIDEFINISIKAN");   
	                        lfl.add(re);                 		
                    	} else {
	                    	if (rpt.isSts_display()){
	                    		re = new ReportEom(); 	
	                    		re.setNamefile(flnm); 
	                    		re.setDescription(rpt.getDesc());
		                        lfl.add(re);
	                    	}
                    	}
                    }

                } catch (IOException e) {
                    //System.out.println("Error :: " + e.getMessage());
                }
            }
        }
        return lfl;
    }
	
	public List getKoranList(String src, String acc){
        List lkl = new ArrayList();
        File folder = new File(src);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles.length > 0) {
        	ReportKoran rk = null; 
        	LocalFunction lf = new LocalFunction();
            for (int i = 0; i < listOfFiles.length; i++) {
                String flnm = listOfFiles[i].getName();
                try {
                    boolean isdirectory = new File(src + "/" + flnm).getCanonicalFile().isDirectory();
                    if (!isdirectory) {
            	        if (!"".equals(acc)){
            	        	if (acc.equals(flnm.substring(18, 18 + acc.length()))){
            	        		Mars_report_desc rpt = lf.stsDisplay(flnm.substring(0, flnm.length() - 15));
    	                    	if (rpt == null){          
    	                    		rk = new ReportKoran();  
    	                    		rk.setNamefile(flnm); 
    	                    		rk.setDescription("BELUM DIDEFINISIKAN");   
    	                    		lkl.add(rk);                 		
    	                    	} else {
    		                    	if (rpt.isSts_display()){
    		                    		rk = new ReportKoran(); 	
    		                    		rk.setNamefile(flnm); 
    		                    		rk.setDescription(rpt.getDesc());
    		                    		lkl.add(rk);
    		                    	}
    	                    	}
            	        	}
            	        } else {
	                    	Mars_report_desc rpt = lf.stsDisplay(flnm.substring(0, flnm.length() - 15));
	                    	if (rpt == null){          
	                    		rk = new ReportKoran();  
	                    		rk.setNamefile(flnm); 
	                    		rk.setDescription("BELUM DIDEFINISIKAN");   
	                    		lkl.add(rk);                 		
	                    	} else {
		                    	if (rpt.isSts_display()){
		                    		rk = new ReportKoran(); 	
		                    		rk.setNamefile(flnm); 
		                    		rk.setDescription(rpt.getDesc());
		                    		lkl.add(rk);
		                    	}
	                    	}
            	        }
                    }

                } catch (IOException e) {
                    //System.out.println("Error :: " + e.getMessage());
                }
            }
        }
        return lkl;
    }
	
	/*
	public String getdescriptionrec(String fd) {
		String desc = "";
		String temp = fd.substring(0, fd.length() - 13);
		if("LAPORAN_HARIAN_GARUDA".equals(temp)){
			desc = "LAPORAN PEMBAYARAN VIA OLP  GARUDA INDONESIA";
		}else if("LAPORAN_HARIAN_INDOSAT".equals(temp)){
				desc = "LAPORAN HARIAN PEMBAYARAN PT INDOSAT";
		}else if("LAPORAN_HARIAN_TELKOM".equals(temp)){
			desc = "LAPORAN HARIAN PEMBAYARAN PT TELKOM";
		}else if("LAPORAN_HARIAN_TELKOMSEL".equals(temp)){
			desc = "LAPORAN HARIAN PEMBAYARAN PT TELKOMSEL";
		}else if("LAPORAN_POS_REKON".equals(temp)){
			desc = "LAPORAN HARIAN PEMBAYARAN PT POS INDONESIA";
		}else if("LAPORAN_HARIAN_PLN".equals(temp)){
			desc = "LAPORAN HARIAN PEMBAYARAN PT PLN";
		}else if("LAPORAN_HARIAN_XLINK".equals(temp)){
			desc = "LAPORAN HARIAN PEMBAYARAN XLINK CHANNEL";
		} else {
			desc = "BELUM DIDEFINISIKAN";
		}
		return desc;
	}
	*/
	
	public List getFileList2(String src){
        List lfl = new ArrayList();
        File folder = new File(src);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles.length > 0) {
        	ReportNeraca rn = null; 
        	LocalFunction lf = new LocalFunction();
            for (int i = 0; i < listOfFiles.length; i++) {
                String flnm = listOfFiles[i].getName();
                try {
                    boolean isdirectory = new File(src + "/" + flnm).getCanonicalFile().isDirectory();
                    if (!isdirectory) {
                    	Mars_report_desc rpt = lf.stsDisplay(flnm.substring(0, flnm.length() - 26));
                    	if (rpt == null){          
                            rn = new ReportNeraca(); 
                            rn.setNamefile(flnm); 
	                        rn.setDescription("BELUM DIDEFINISIKAN");   
	                        lfl.add(rn);                 		
                    	} else {
	                    	if (rpt.isSts_display()){
	                            rn = new ReportNeraca(); 
	                            rn.setNamefile(flnm); 
		                        rn.setDescription(rpt.getDesc());
		                        lfl.add(rn);
	                    	}
                    	}
                    }

                } catch (IOException e) {
                    //System.out.println("Error :: " + e.getMessage());
                }
            }
        }
        return lfl;
    }
	
	public List getjobdaily(String day) {
		List ltrn = new ArrayList();
        Jobtrans trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
	        
        try {
            conn = DatasourceEntry.getInstance().getRefdwhDS().getConnection();
            sql = "select day, job_code, job_name, module, (CASE WHEN runjob_flag = 0 THEN 'Daily' when runjob_flag  = '1' THEN 'Working Day' ELSE 'Monthly'  END) Runjob_flag, " +
            	  "start_date, end_date, duration, status, description from job_status_v " +
            	  "where day = ? and runjob_flag = '0' order by JOB_CODE asc";
        	stat =  conn.prepareStatement(sql);
	    	stat.setString(1, day);
	        rs = stat.executeQuery();
	        while (rs.next()) {
                trn = new Jobtrans();
                trn.setDay(rs.getString("day"));
                trn.setJob_code(rs.getString("job_code"));
                trn.setJob_name(rs.getString("job_name"));
                trn.setRunjob_flag(rs.getString("runjob_flag"));
                trn.setModule(rs.getString("module"));
                trn.setStart_date(rs.getString("start_date"));
                trn.setEnd_date(rs.getString("end_date"));
                trn.setDuration(rs.getString("duration"));
                trn.setStatus(rs.getString("status"));
                trn.setDescription(rs.getString("description"));
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	LogLog.error(" getJobtrans : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
	
	public List getjob(String day) {
		List ltrn = new ArrayList();
        Jobtrans trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
	        
        try {
           /*conn = DatasourceEntry.getInstance().getStgDS().getConnection();
        	sql =  "select day, job_code, job_name, status, to_char(start_date,'YYYY/MM/DD HH24:MI:SS') start_date, " +
        		   "to_char(end_date,'YYYY/MM/DD HH24:MI:SS') end_date, FLOOR(24*60*60*(end_date-start_date)/3600)||':'|| " +
        		   "FLOOR(round(mod(24*60*60*(end_date-start_date),3600))/60)||':'|| " +
        		   "mod(round(mod(24*60*60*(end_date-start_date),3600)),60) duration, description " +
        		   "from job_status  where day = ? order by start_date, JOB_CODE";*/
        	conn = DatasourceEntry.getInstance().getRefdwhDS().getConnection();
            sql = "select day, job_code, job_name, module, (CASE WHEN runjob_flag = 0 THEN 'Daily' when runjob_flag  = '1' THEN 'Working Day' ELSE 'Monthly'  END) Runjob_flag, " +
      	          "start_date, end_date, duration, status, description from job_status_v " +
    	          "where day = ? and runjob_flag = '1' order by JOB_CODE asc";
        	stat =  conn.prepareStatement(sql);
	    	stat.setString(1, day);
	        rs = stat.executeQuery();
	        while (rs.next()) {
                trn = new Jobtrans();
                trn.setDay(rs.getString("day"));
                trn.setJob_code(rs.getString("job_code"));
                trn.setJob_name(rs.getString("job_name"));
                trn.setModule(rs.getString("module"));
                trn.setRunjob_flag(rs.getString("Runjob_flag"));
                trn.setStart_date(rs.getString("start_date"));
                trn.setEnd_date(rs.getString("end_date"));
                trn.setDuration(rs.getString("duration"));
                trn.setStatus(rs.getString("status"));
                trn.setDescription(rs.getString("description"));
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	LogLog.error(" getJobtrans : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }
	
	public List getjobeom(String day) {
		List ltrn = new ArrayList();
        Jobtrans trn = null;
        Connection conn = null;
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "";
	        
        try {
            conn = DatasourceEntry.getInstance().getRefdwhDS().getConnection();
            sql = "select day, job_code, job_name, module, (CASE WHEN runjob_flag = 0 THEN 'Daily' when runjob_flag  = '1' THEN 'Working Day' ELSE 'Monthly'  END) Runjob_flag, " +
            	  "start_date, end_date, duration, status, description from job_status_v " +
            	  "where day = ? and runjob_flag = '2' order by JOB_CODE asc";
        	stat =  conn.prepareStatement(sql);
	    	stat.setString(1, day);
	        rs = stat.executeQuery();
	        while (rs.next()) {
                trn = new Jobtrans();
                trn.setDay(rs.getString("day"));
                trn.setJob_code(rs.getString("job_code"));
                trn.setJob_name(rs.getString("job_name"));
                trn.setModule(rs.getString("module"));
                trn.setRunjob_flag(rs.getString("Runjob_flag"));
                trn.setStart_date(rs.getString("start_date"));
                trn.setEnd_date(rs.getString("end_date"));
                trn.setDuration(rs.getString("duration"));
                trn.setStatus(rs.getString("status"));
                trn.setDescription(rs.getString("description"));
                ltrn.add(trn);
            }
        } catch (SQLException ex) {
        	LogLog.error(" getJobtrans : " + ex.getMessage());
        } finally {
            closeConnDb(conn, stat, rs);
        }
        return ltrn;
    }

	public void createFileSknMssl(IntfRtgsOutBean intfOut, int tot, String pathfile){
		SknRtgsMsslFunction f = new SknRtgsMsslFunction();
        List ltrnsknmsl = f.getListSknMssl(intfOut, tot, "D"); 
    	DecimalFormat myFormatter = new DecimalFormat("###,##0.00");
		String linetrans = "";            
        linetrans = formatField("TRNS DT", 10, " ", false)+ "|";
        linetrans += formatField("BRANCH", 6, " ", false)+ "|";
        linetrans += formatField("REF NO", 16, " ", false)+ "|";
        linetrans += formatField("FROM ACCOUNT", 12, " ", false)+ "|";
        linetrans += formatField("NAME", 40, " ", false)+ "|";
        linetrans += formatField("CLASS", 5, " ", false)+ "|";
        linetrans += formatField("TO ACCOUNT", 20, " ", false)+ "|";
        linetrans += formatField("NAME ACCOUNT", 140, " ", false)+ "|";
        linetrans += formatField("NOMINAL", 28, " ", true) + "|";
        linetrans += formatField("DESCRIPTION", 96, " ", false)+ "|";
        linetrans += formatField("BUSINESS", 8, " ", false)+ "|";
        linetrans += formatField("TO MEMBER",9, " ", false)+ "|";
        linetrans += formatField("MEMBER NAME", 52, " ", false)+ "|";
        linetrans += formatField("MAKER ID", 12, " ", false)+ "|";
        linetrans += formatField("AUTHORIZER ID", 12, " ", false)+ "|";
        linetrans += formatField("AUTH DT", 20, " ", false)+ "|";
        linetrans += formatField("FILENAME", 60, " ", false)+ "|";
        linetrans += formatField("STS DATA", 12, " ", false)+ "|";
        linetrans += formatField("AUTHORIZED", 12, " ", false)+ "|";
        linetrans += formatField("DESCRIPTION", 58, " ", false)+ "|" + "\r";
        createFileLaporanHarian(pathfile, linetrans);
        linetrans = formatField("", 648, "=", false) + "\r";
        createFileLaporanHarian(pathfile, linetrans);
		if (ltrnsknmsl.size() > 0) {
			for (Iterator<RtgsTtpn> i = ltrnsknmsl.iterator(); i.hasNext();) {
				RtgsTtpn b = i.next();
				linetrans = "";
                linetrans += formatField(b.getTrx_dt().toString(), 10, " ", false)+ "|";
                linetrans += formatField(b.getBranch_cd().toString(), 6, " ", false)+ "|";
                linetrans += formatField(b.getSender_ref_no(), 16, " ", false)+ "|";
                linetrans += formatField(b.getFr_acc(), 12, " ", false)+ "|";
                linetrans += formatField(b.getName_from(), 40, " ", false)+ "|";
                linetrans += formatField(b.getClass_acc(), 5, " ", false)+ "|";
                linetrans += formatField(b.getTo_acc(), 20, " ", false)+ "|";
                linetrans += formatField(b.getUltimate_benef_name(), 140, " ", false)+ "|";
                linetrans += formatField(myFormatter.format(b.getAmount()), 28, " ", true) + "|";;
                linetrans += formatField(b.getPay_detail(), 96, " ", false)+ "|";
                linetrans += formatField(b.getSandi_usaha(), 8, " ", false)+ "|";
                linetrans += formatField(b.getTo_member(), 9, " ", false)+ "|";
                linetrans += formatField(b.getNama_member(), 52, " ", false)+ "|";
                linetrans += formatField(b.getMaker_id(), 12, " ", false)+ "|";
                linetrans += formatField(b.getChecker_id(), 12, " ", false)+ "|";
                linetrans += formatField(b.getTgl_oto().toString(), 20, " ", false)+ "|";
                linetrans += formatField(b.getFilename(), 60, " ", false)+ "|";
                linetrans += formatField(b.getSts_data(), 12, " ", false)+ "|";
                linetrans += formatField(b.getSts_trns(), 12, " ", false)+ "|";
                linetrans += formatField(b.getErr_desc_mcb(), 58, " ", false) + "|" + "\r";
                createFileLaporanHarian(pathfile, linetrans);
			}				
		}
	}
	
	public void downloadRtgsMcbXls(List ltrans, HttpServletRequest request, HttpServletResponse response, IntfRtgsOutBean intfOut, String network){
		String filename = "";
        String formatFile = "xls";
        HashMap params = new HashMap();
        params.put("tgltrns", intfOut.getBsnsdt());
        if("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null){
            params.put("cab", "");        	
        } else {
            params.put("cab", intfOut.getKdcab());        	        	
        }
        if("".equals(intfOut.getTxnsts()) || intfOut.getTxnsts() == null){
            params.put("sts", "");        	
        } else {
            params.put("sts", intfOut.getTxnsts());        	        	
        }
        if("".equals(intfOut.getContractno()) || intfOut.getContractno() == null){
            params.put("contractno", "");        	
        } else {
            params.put("contractno", intfOut.getContractno());        	        	
        }
        if("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null){
            params.put("noref", "");
        } else {
            params.put("noref", intfOut.getReltrn());        	        	
        }
        if("".equals(intfOut.getFracc()) || intfOut.getFracc() == null){
            params.put("fracc", "");
        } else {
            params.put("fracc", intfOut.getFracc());        	        	
        }
        if("".equals(intfOut.getTmmbr()) || intfOut.getTmmbr() == null){
            params.put("bankcd", "");
        } else {
            params.put("bankcd", intfOut.getTmmbr());        	        	
        }
        if("".equals(intfOut.getToacc()) || intfOut.getToacc() == null){
            params.put("toacc", "");
        } else {
            params.put("toacc", intfOut.getToacc());        	        	
        }
        if("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null){
            params.put("amtfr", new BigDecimal(0));
        } else {
            params.put("amtfr", new BigDecimal(intfOut.getAmtf().replace(",", "")));        	        	
        }
        if("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null){
            params.put("amtto", new BigDecimal(0));
        } else {
            params.put("amtto", new BigDecimal(intfOut.getAmtto().replace(",", "")));        	        	
        }
        if("RTGSBI".equals(network)){
            filename = "RTGS_MCB.xls";        	
        } else if("SKNBI".equals(network)){
            filename = "SKN_MCB.xls";        	
        }
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try {
            ArrayList arrListLrpp = moveListToHashMap(ltrans);
            byte[] bytes = null;
            if (!arrListLrpp.isEmpty()) {
                JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrListLrpp);
                jasperReport = (JasperReport) JRLoader.loadObjectFromLocation("com/muamalat/reportmcb/report/RtgsMcb.jasper");
                jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);
                if (formatFile.equals("pdf")) {
                	response.reset();
                    OutputStream ouputStream = response.getOutputStream();
                    JRExporter exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    response.setContentType("application/pdf");
                    filename = "RTGS_MCB.pdf";
                    response.setHeader("Content-disposition", "attachment;filename="+filename);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                    exporter.exportReport();
                    bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                    
                    ouputStream.write(bytes, 0, bytes.length);
                    ouputStream.flush();
                    ouputStream.close();
                } else if (formatFile.equals("xls")) {
                    OutputStream ouputStream = response.getOutputStream();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                    JRXlsExporter exporterXls = new JRXlsExporter();
                    exporterXls.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    exporterXls.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);

                    exporterXls.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
                    //exporterXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);

                    exporterXls.exportReport();
                    byte[] encData2 = byteArrayOutputStream.toByteArray();

                    response.setContentType("application/octet-stream");
                    response.setContentType("application/xls");
//                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-disposition", "attachment;filename=" + filename);

                    ouputStream.write(encData2);
                    ouputStream.flush();
                    ouputStream.close();
                }
            }
        } catch (IOException ex) {
        	System.out.println("errr : " + ex.getMessage());
            request.setAttribute("message", "Print failed ! <br/>Exception");
            log.error(ex.getMessage());
        } catch (JRException ex) {
        	System.out.println("errr 2: " + ex.getMessage());
            request.setAttribute("message", "Print failed ! <br/>Exception");
            log.error(ex.getMessage());
        }
	}
	
	public void downloadRtgsMcbIncXls(List ltrans, HttpServletRequest request, HttpServletResponse response, IntfRtgsOutBean intfOut, String network){
		String filename = "";
        String formatFile = "xls";
        HashMap params = new HashMap();
        params.put("tgltrns", intfOut.getBsnsdt());
        if("".equals(intfOut.getKdcab()) || intfOut.getKdcab() == null){
            params.put("cab", "");        	
        } else {
            params.put("cab", intfOut.getKdcab());        	        	
        }
        if("".equals(intfOut.getReltrn()) || intfOut.getReltrn() == null){
            params.put("noref", "");
        } else {
            params.put("noref", intfOut.getReltrn());        	        	
        }
        if("".equals(intfOut.getCust_ac_no()) || intfOut.getCust_ac_no() == null){
            params.put("custacno_h", "");
        } else {
            params.put("custacno_h", intfOut.getCust_ac_no());        	        	
        }
        if("".equals(intfOut.getType_trns()) || intfOut.getType_trns() == null){
            params.put("type", "");
        } else {
        	if ("411".equals(intfOut.getType_trns())){
                params.put("type", "RTGS INCOMING");         		
        	} else if ("4D1".equals(intfOut.getType_trns())){
                params.put("type", "RTGS RETUR");         		
        	} else if ("4E1".equals(intfOut.getType_trns())){
                params.put("type", "RTGS TITIPAN");         		
        	} else if ("410".equals(intfOut.getType_trns())){
                params.put("type", "SKN INCOMING");         		
        	} else if ("4D0".equals(intfOut.getType_trns())){
                params.put("type", "SKN RETUR");         		
        	} else if ("4E0".equals(intfOut.getType_trns())){
                params.put("type", "SKN TITIPAN");         		
        	}      	        	
        }
        if("".equals(intfOut.getAmtf()) || intfOut.getAmtf() == null){
            params.put("amtfr", new BigDecimal(0));
        } else {
            params.put("amtfr", new BigDecimal(intfOut.getAmtf().replace(",", "")));        	        	
        }
        if("".equals(intfOut.getAmtto()) || intfOut.getAmtto() == null){
            params.put("amtto", new BigDecimal(0));
        } else {
            params.put("amtto", new BigDecimal(intfOut.getAmtto().replace(",", "")));        	        	
        }
        if ("SKNBI".equals(network)){
            filename = "SKNMCB_INC.xls";      	
        } else if ("RTGSBI".equals(network)){
            filename = "RTGS_MCB_INC.xls";   	
        }
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try {
            ArrayList arrListLrpp = moveListToHashMapRtgsInc(ltrans);
            byte[] bytes = null;
            if (!arrListLrpp.isEmpty()) {
                JRMapCollectionDataSource ds = new JRMapCollectionDataSource(arrListLrpp);
                jasperReport = (JasperReport) JRLoader.loadObjectFromLocation("com/muamalat/reportmcb/report/RtgsIncMCB.jasper");
                jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);
                if (formatFile.equals("pdf")) {
                	response.reset();
                    OutputStream ouputStream = response.getOutputStream();
                    JRExporter exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    response.setContentType("application/pdf");
                    response.setHeader("Content-disposition", "attachment;filename="+filename);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                    exporter.exportReport();
                    bytes = JasperExportManager.exportReportToPdf(jasperPrint);
                    
                    ouputStream.write(bytes, 0, bytes.length);
                    ouputStream.flush();
                    ouputStream.close();
                } else if (formatFile.equals("xls")) {
                    OutputStream ouputStream = response.getOutputStream();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                    JRXlsExporter exporterXls = new JRXlsExporter();
                    exporterXls.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    exporterXls.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);

                    exporterXls.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);

                    exporterXls.exportReport();
                    byte[] encData2 = byteArrayOutputStream.toByteArray();

                    response.setContentType("application/octet-stream");
                    response.setContentType("application/xls");
//                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-disposition", "attachment;filename=" + filename);

                    ouputStream.write(encData2);
                    ouputStream.flush();
                    ouputStream.close();
                }
            }
        } catch (IOException ex) {
        	System.out.println("errr : " + ex.getMessage());
            request.setAttribute("message", "Print failed ! <br/>Exception");
            log.error(ex.getMessage());
        } catch (JRException ex) {
        	System.out.println("errr 2: " + ex.getMessage());
            request.setAttribute("message", "Print failed ! <br/>Exception");
            log.error(ex.getMessage());
        }
	}
	
	public ArrayList moveListToHashMap(List ls) {
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        for (Iterator<RtgsSknMcb> i = ls.iterator(); i.hasNext();) {
        	RtgsSknMcb rs = i.next();
            rowMap = new HashMap();
            rowMap.put("bkdt", rs.getBookdt());
            rowMap.put("inidt", rs.getInit());
            rowMap.put("activdt", rs.getActiv());
            rowMap.put("brnch", rs.getBranch_code());
            rowMap.put("contractno", rs.getContract_ref_no());
            rowMap.put("noref", rs.getAc_entry_ref_no());
            rowMap.put("accno", rs.getCust_ac_no());
            rowMap.put("accname", rs.getCust_name());
            rowMap.put("bnkcd", rs.getCpty_bankcode());
            rowMap.put("toacc", rs.getCpty_ac_no());
            rowMap.put("toaccname", rs.getCpty_name());
            rowMap.put("detail", rs.getRemarks());
            rowMap.put("amount", rs.getTxn_amount());
            detail.add(rowMap);
        }
        return detail;
    }
	
	public ArrayList moveListToHashMapRtgsInc(List ls) {
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        for (Iterator<RtgsSknMcb> i = ls.iterator(); i.hasNext();) {
        	RtgsSknMcb rs = i.next();
            rowMap = new HashMap();
            rowMap.put("bkdt", rs.getBookdt());
            rowMap.put("brnch", rs.getBranch_code());
            rowMap.put("noref", rs.getAc_entry_ref_no());
            rowMap.put("custacno", rs.getCust_ac_no());
            rowMap.put("type", rs.getTrn_desc());
            rowMap.put("amount", rs.getTxn_amount());
            rowMap.put("detail", rs.getRemarks());
            rowMap.put("userid", rs.getMaker_id());
            rowMap.put("authid", rs.getChecker_id());
            detail.add(rowMap);
        }
        return detail;
    }
	
	public void createFileRTgsMssl(IntfRtgsOutBean intfOut, int tot, String pathfile){
		SknRtgsMsslFunction f = new SknRtgsMsslFunction();
        List ltrnrtgsmsl = f.getListRtgsMssl(intfOut, tot, "D"); 
    	DecimalFormat myFormatter = new DecimalFormat("###,###.##");
		String header = "";
		String linetrans = "";            
        linetrans = formatField("TRNS DT", 10, " ", false)+ "|";
        linetrans += formatField("BRANCH", 6, " ", false)+ "|";
        linetrans += formatField("REF NO", 16, " ", false)+ "|";
        linetrans += formatField("FROM ACCOUNT", 12, " ", false)+ "|";
        linetrans += formatField("NAME", 40, " ", false)+ "|";
        linetrans += formatField("CLASS", 5, " ", false)+ "|";
        linetrans += formatField("TO ACCOUNT", 22, " ", false)+ "|";
        linetrans += formatField("NAME ACCOUNT", 140, " ", false) + "|";    
        linetrans += formatField("TO MEMBER",9, " ", false)+ "|";    
        linetrans += formatField("NOMINAL", 28, " ", true) + "|";
        linetrans += formatField("DESCRIPTION", 96, " ", false)+ "|" + "\r";
//        linetrans += formatField("MEMBER NAME",52, " ", false)+ "|";
        createFileLaporanHarian(pathfile, linetrans);
        linetrans = formatField("", 395, "=", false) + "\r";
        createFileLaporanHarian(pathfile, linetrans);
		if (ltrnrtgsmsl.size() > 0) {
			for (Iterator<RtgsTtpn> i = ltrnrtgsmsl.iterator(); i.hasNext();) {
				RtgsTtpn b = i.next();
				linetrans = "";
                linetrans += formatField(b.getTrx_dt().toString(), 10, " ", false) + "|";
                linetrans += formatField(b.getBranch_cd().toString(), 6, " ", false) + "|";
                linetrans += formatField(b.getSender_ref_no(), 16, " ", false)+ "|";
                linetrans += formatField(b.getFr_acc(), 12, " ", false)+ "|";
                linetrans += formatField(b.getName_from(), 40, " ", false)+ "|";
                linetrans += formatField(b.getClass_acc(), 5, " ", false)+ "|";
                linetrans += formatField(b.getTo_acc(), 22, " ", false)+ "|";
                linetrans += formatField(b.getUltimate_benef_name(), 140, " ", false)+ "|";
                linetrans += formatField(b.getTo_member(), 9, " ", false)+ "|";
                linetrans += formatField(myFormatter.format(b.getAmount()), 28, " ", true) + "|";;
                linetrans += formatField(b.getPay_detail(), 96, " ", false)+ "|" + "\r";
//                linetrans += formatField(b.getNama_member(), 52, " ", false)+ "|";
                createFileLaporanHarian(pathfile, linetrans);
			}				
		}
	}	
	public boolean containsWhitespace(String str) {
        if (!hasLength(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public boolean hasLength(String str) {
        return (str != null && str.length() > 0);
    }
}
