package com.muamalat.reportmcb.function;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.muamalat.reportmcb.entity.BranchMaster;
import com.muamalat.reportmcb.entity.HI;
import com.muamalat.reportmcb.entity.Instruction;
import com.muamalat.reportmcb.entity.JurnalTodayDetail;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.entity.User;
import com.muamalat.reportmcb.entity.account;
import com.muamalat.reportmcb.entity.accountEntity;
import com.muamalat.reportmcb.entity.detailUserPending;
import com.muamalat.reportmcb.entity.facility;
import com.muamalat.reportmcb.entity.fbti;
import com.muamalat.reportmcb.entity.haji;
import com.muamalat.reportmcb.entity.jadangFbti;
import com.muamalat.reportmcb.entity.kurs;
import com.muamalat.reportmcb.entity.merchantName;
import com.muamalat.reportmcb.entity.merge;
import com.muamalat.reportmcb.entity.pendingTransaksi;
import com.muamalat.reportmcb.entity.pinpad;
import com.muamalat.reportmcb.entity.prks;
import com.muamalat.reportmcb.entity.quickCif;
import com.muamalat.reportmcb.entity.quickCifPeriode;
import com.muamalat.reportmcb.entity.tes;
import com.muamalat.singleton.DatasourceEntry;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

public class SqlFunctionNew {

	private static Logger log = Logger.getLogger(SqlFunctionNew.class);

	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				log.error("closeConnDb 1 : " + e.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (Exception e) {
				log.error("closeConnDb 2 : " + e.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (Exception e) {
				log.error("closeConnDb 3 : " + e.getMessage());
			}
		}
	}

	public List getStandingInstruction(String no, String accDebit, String accCredit) {
		List lbb = new ArrayList();
		Instruction instr = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {

			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " SELECT a.INSTRUCTION_NO, a.dr_account,a.cr_account, b.product_code,  "
					+ " a.SI_AMT_CCY as ccy,a.DR_ACC_BR, c.PRODUCT_DESCRIPTION, case when a.EVENT_CODE ='CLOS' "
					+ " then 'CLOSE' when a.EVENT_CODE ='OPEN' then 'OPEN' else 'OTHER' end as EVENT_CODE "
					+ " FROM SITB_CONTRACT_MASTER a, sitb_instruction b, cstm_product c"
					+ " WHERE a.INSTRUCTION_NO = b.INSTRUCTION_NO and b.product_code = c.product_code";

			if (!"".equals(no) && no != null) {
				sql += " and a.INSTRUCTION_NO = ?";
			} else if (!"".equals(accDebit) && accDebit != null) {
				sql += " and a.dr_account = ?";
			} else if (!"".equals(accCredit) && accCredit != null) {
				sql += " and a.cr_account = ?";
			}

			sql += " group by a.INSTRUCTION_NO, a.dr_account,a.cr_account, b.product_code, "
					+ " c.PRODUCT_DESCRIPTION,a.SI_AMT_CCY,a.DR_ACC_BR,a.EVENT_CODE";

			System.out.println("sql :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			// stat.setString(i++, no);
			if (!"".equals(no) && no != null) {
				stat.setString(i++, no);
			} else if (!"".equals(accDebit) && accDebit != null) {
				stat.setString(i++, accDebit);
			} else if (!"".equals(accCredit) && accCredit != null) {
				stat.setString(i++, accCredit);
			}
			rs = stat.executeQuery();
			while (rs.next()) {
				instr = new Instruction();
				instr.setInstructionNo(rs.getString("INSTRUCTION_NO"));
				instr.setDrAccount(rs.getString("dr_account"));
				instr.setCrAccount(rs.getString("cr_account"));
				instr.setProductCode(rs.getString("product_code"));
				instr.setProductDesc(rs.getString("PRODUCT_DESCRIPTION"));
				instr.setCcy(rs.getString("ccy"));
				instr.setDrBranch(rs.getString("DR_ACC_BR"));
				instr.setEventCode(rs.getString("EVENT_CODE"));
				lbb.add(instr);
			}
		} catch (Exception e) {
			log.error("getStandingInstruction :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public Instruction getInstruction(String no) {
		Instruction instr = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();

			sql = "SELECT MASTER.INSTRUCTION_NO,MASTER.CONTRACT_REF_NO,CSTM.PRODUCT_REMARKS,CSTM.PRODUCT_CODE, MASTER.USER_REF_NUMBER,"
					+ " CSTM.PRODUCT_DESCRIPTION,TO_CHAR (SITB.FIRST_EXEC_DATE,'dd-mm-yyyy') AS FIRST_EXEC_VALUE_DATE, "
					+ " TO_CHAR (SITB.FIRST_VALUE_DATE,'dd-mm-yyyy') AS FIRST_EXEC_DUE_DATE, "
					+ " TO_CHAR (SITB.NEXT_VALUE_DATE,'dd-mm-yyyy') AS NEXT_EXEC_VALUE_DATE, "
					+ " TO_CHAR (SITB.NEXT_EXEC_DATE,'dd-mm-yyyy') AS NEXT_EXEC_DUE_DATE,"
					+ " TO_CHAR (MASTER.SI_EXPIRY_DATE, 'dd-mm-yyyy') AS EXPIRY_DATE, "
					+ " MASTER.SI_AMT_CCY AS CCY,MASTER.SI_AMT,MASTER.DR_ACC_BR AS DEBET_BRANCH,MASTER.DR_ACCOUNT, "
					+ " MASTER.DR_ACC_CCY,MASTER.CR_ACC_BR,MASTER.CR_ACCOUNT, "
					+ " MASTER.CR_ACC_CCY,MASTER.INTERNAL_REMARKS "
					+ " FROM SITB_CONTRACT_MASTER MASTER,SITB_INSTRUCTION SITB,CSTM_PRODUCT CSTM "
					+ " WHERE MASTER.INSTRUCTION_NO = SITB.INSTRUCTION_NO AND SITB.PRODUCT_CODE = CSTM.PRODUCT_CODE "
					+ " AND MASTER.INSTRUCTION_NO = ? "
					+ " GROUP BY MASTER.INSTRUCTION_NO,MASTER.CONTRACT_REF_NO,CSTM.PRODUCT_REMARKS,CSTM.PRODUCT_CODE, "
					+ " CSTM.PRODUCT_DESCRIPTION,TO_CHAR (SITB.FIRST_EXEC_DATE, 'dd-mm-yyyy'), "
					+ " TO_CHAR (SITB.NEXT_EXEC_DATE, 'dd-mm-yyyy'),TO_CHAR (SITB.FIRST_VALUE_DATE, 'dd-mm-yyyy'), "
					+ " TO_CHAR (SITB.NEXT_VALUE_DATE, 'dd-mm-yyyy'),MASTER.SI_EXPIRY_DATE,MASTER.SI_AMT_CCY, "
					+ " MASTER.SI_AMT,MASTER.DR_ACC_BR,MASTER.DR_ACCOUNT,MASTER.DR_ACC_CCY,MASTER.CR_ACC_BR, "
					+ " MASTER.CR_ACCOUNT,MASTER.CR_ACC_CCY,MASTER.INTERNAL_REMARKS,MASTER.USER_REF_NUMBER";
			System.out.println("sql SI -->" + sql);
			System.out.println("no SI sql:" + no);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, no.trim());
			rs = stat.executeQuery();
			if (rs.next()) {
				instr = new Instruction();
				instr.setInstructionNo(rs.getString("INSTRUCTION_NO"));
				instr.setUserRefNumber(rs.getString("USER_REF_NUMBER"));
				instr.setContractRefNo(rs.getString("CONTRACT_REF_NO"));
				instr.setProductCode(rs.getString("PRODUCT_CODE"));
				instr.setProductDesc(rs.getString("PRODUCT_DESCRIPTION"));
				instr.setProductRemarks(rs.getString("PRODUCT_REMARKS"));
				instr.setFirstExecDueDate(rs.getString("FIRST_EXEC_DUE_DATE"));
				instr.setFirstExecValueDate(rs.getString("FIRST_EXEC_VALUE_DATE"));
				instr.setNextExecDueDate(rs.getString("NEXT_EXEC_DUE_DATE"));
				instr.setNextExecValueDate(rs.getString("NEXT_EXEC_VALUE_DATE"));
				instr.setExpiryDate(rs.getString("EXPIRY_DATE"));
				instr.setCcy(rs.getString("CCY"));
				instr.setAmount(rs.getBigDecimal("SI_AMT"));
				instr.setDrBranch(rs.getString("DEBET_BRANCH"));
				instr.setDrAccount(rs.getString("DR_ACCOUNT"));
				instr.setCrBranch(rs.getString("CR_ACC_BR"));
				instr.setCrAccount(rs.getString("CR_ACCOUNT"));
				instr.setRemarks(rs.getString("INTERNAL_REMARKS"));
			}

		} catch (Exception e) {
			log.error(" getInstruction : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return instr;
	}

	public int getTotSI(String no, String accDebit, String accCredit) {
		List lbb = new ArrayList();
		Instruction instr = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;

		try {

			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select count (*) as total from (SELECT a.INSTRUCTION_NO, a.dr_account, "
					+ " a.cr_account,b.product_code,a.SI_AMT_CCY,a.DR_ACC_BR "
					+ " FROM SITB_CONTRACT_MASTER a, sitb_instruction b"
					+ " WHERE a.INSTRUCTION_NO = b.INSTRUCTION_NO ";
			if (!"".equals(no) && no != null) {
				sql += " and a.INSTRUCTION_NO = ? ";
			} else if (!"".equals(accDebit) && accDebit != null) {
				sql += " and a.dr_account = ? ";
			} else if (!"".equals(accCredit) && accCredit != null) {
				sql += " and a.cr_account = ? ";
			}

			sql += " group by a.INSTRUCTION_NO, a.dr_account,a.cr_account, "
					+ " b.product_code,a.SI_AMT_CCY,a.DR_ACC_BR)";

			System.out.println("sql total:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if (!"".equals(no) && no != null) {
				stat.setString(i++, no.trim());
			} else if (!"".equals(accDebit) && accDebit != null) {
				stat.setString(i++, accDebit.trim());
			} else if (!"".equals(accCredit) && accCredit != null) {
				stat.setString(i++, accCredit.trim());
			}
			rs = stat.executeQuery();

			if (rs.next()) {
				total = rs.getInt("total");
			}
		} catch (Exception e) {
			log.error("getTotSI :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return total;

	}

	public int getTotKurs(String branch) {
		List lbb = new ArrayList();
		Instruction instr = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;

		try {

			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "select count (*) as total from (select CCY1, CCY2,  RATE_TYPE, BUY_SPREAD, SALE_SPREAD, "
					+ " BUY_RATE, SALE_RATE, to_date(RATE_DATE,'dd-mm-yyyy') as RATE_DATE "
					+ " from cytm_rates where branch_code =? and to_char (RATE_DATE, 'dd-mm-yyyy') = to_char(sysdate,'dd-mm-yyyy') "
					+ " AND RATE_TYPE IN ('STANDARD','TT','KURSKPNO','REVAL','BANKNOTE'))";

			System.out.println("sql getTotKurs:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, branch);
			rs = stat.executeQuery();

			if (rs.next()) {
				total = rs.getInt("total");
				System.out.println(total);
			}
		} catch (Exception e) {
			log.error("getTotKurs :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return total;
	}

	public List getKurs() {
		List lbb = new ArrayList();
		kurs entityKurs = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();

			sql = "select CCY1 as fcy_amount, CCY2 as lcy_amount, RATE_TYPE, BUY_SPREAD, SALE_SPREAD, BUY_RATE, SALE_RATE, "
					+ " to_char(RATE_DATE,'dd-mm-yyyy') as RATE_DATE from cytm_rates where "
					+ " to_char (RATE_DATE, 'dd-mm-yyyy') = to_char(sysdate,'dd-mm-yyyy') "
					+ " and RATE_TYPE IN ('STANDARD', 'TT', 'KURSKPNO', 'REVAL', 'BANKNOTE') "
					+ " group by CCY1,CCY2,RATE_TYPE,BUY_SPREAD,SALE_SPREAD,BUY_RATE,SALE_RATE,TO_CHAR (RATE_DATE, 'dd-mm-yyyy') "
					+ " ORDER BY CCY1 asc";

			System.out.println("sql kurs :" + sql);

			int i = 1;
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				entityKurs = new kurs();
				entityKurs.setFcyAmount(rs.getString("fcy_amount"));
				entityKurs.setLcyAmount(rs.getString("lcy_amount"));
				entityKurs.setRateType(rs.getString("RATE_TYPE"));
				entityKurs.setBuySpread(rs.getBigDecimal("BUY_SPREAD"));
				entityKurs.setSaleSpread(rs.getBigDecimal("SALE_SPREAD"));
				entityKurs.setBuyRate(rs.getBigDecimal("BUY_RATE"));
				entityKurs.setSaleRate(rs.getBigDecimal("SALE_RATE"));
				entityKurs.setRateDate(rs.getString("RATE_DATE"));
				System.out.println("Rate type :" + rs.getString("RATE_TYPE"));
				lbb.add(entityKurs);
			}
		} catch (Exception e) {
			log.error("getTotKurs :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}

		return lbb;
	}

	public List getHI() {
		List lbb = new ArrayList();
		HI hi = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "SELECT gl_class,lccy_code,fccy_code,HI1000_VAL_ACT as kode_idr, HI1000_VAL_USD_ACT kode_usd, "
					+ " to_char(RUN_DATE ,'dd-mm-yyyy') as RUN_DATE FROM gltm_bmi_hi1000 "
					+ " where gl_class = 'HI-1000'" + " order by to_date(RUN_DATE,'dd-mm-yyyy') desc";

			System.out.println("sql HI :" + sql);
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				hi = new HI();
				hi.setIdr(rs.getBigDecimal("kode_idr"));
				hi.setUsd(rs.getBigDecimal("kode_usd"));
				hi.setDate(rs.getString("RUN_DATE"));
				hi.setFccyCode(rs.getString("fccy_code"));
				hi.setLccyCode(rs.getString("lccy_code"));
				hi.setGlClass(rs.getString("gl_class"));
				lbb.add(hi);
			}

		} catch (Exception e) {
			log.error("getHI :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public int getTotCustAcc(String norek) {
		int tot = 0;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;

		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "SELECT count(*) as total FROM cust_account_master where cust_ac_no = ?";

			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, norek.trim());

			rs = stat.executeQuery();
			if (rs.next()) {
				tot = rs.getInt("total");
				System.out.println("total:" + tot);
			}
		} catch (SQLException ex) {
			log.error(" getTotCust : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return tot;
	}

	public List getListCustomAcc(String norek, int begin) {
		List lbb = new ArrayList();
		account bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int total = 0;

		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "select cust_ac_no, ac_desc, cust_no, ccy, account_class, UNIQUE_ID_VALUE, DATE_OF_BIRTH from  "
					+ "(SELECT cust_ac_no, ac_desc, cust_no, ccy, account_class, UNIQUE_ID_VALUE, DATE_OF_BIRTH  "
					+ "FROM cust_account_master where cust_ac_no = ? order by cust_ac_no LIMIT 30 OFFSET ?) as hasil";

			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, norek.trim());
			stat.setInt(i++, begin);
			rs = stat.executeQuery();

			while (rs.next()) {
				bb = new account();
				bb.setCust_ac_no(rs.getString("cust_ac_no"));
				bb.setAc_desc(rs.getString("ac_desc"));
				bb.setCust_no(rs.getString("cust_no"));
				bb.setCcy(rs.getString("ccy"));
				bb.setAccount_class(rs.getString("account_class"));
				bb.setUNIQUE_ID_VALUE(rs.getString("UNIQUE_ID_VALUE"));
				bb.setDATE_OF_BIRTH(rs.getString("DATE_OF_BIRTH"));
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" getListCustomAcc : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public accountEntity getDetailAccount(String acc) {
		accountEntity entity = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();

			sql = "select a.BRANCH_CODE,d.branch_name,f.nationality, a.CUST_AC_NO,a.AC_DESC,CUST_NO,a.CCY,a.ACCOUNT_CLASS,a.acy_curr_balance, "
					+ "a.acy_blocked_amount, a.acy_avl_bal, c.min_balance,b.acy_bal, f.CUSTOMER_TYPE, "
					+ "e.DESCRIPTION, to_CHAR(a.ac_open_date,'DD/MM/YYYY') ac_open_date,"
					+ "a.alt_ac_no,a.record_stat,to_CHAR(a.CHECKER_DT_STAMP,'DD/MM/YYYY') CHECKER_DT_STAMP,to_CHAR(f.cif_creation_date,'DD/MM/YYYY') cif_creation_date "
					+ "FROM Sttm_Cust_Account a, STTM_ACCLS_CCY_BALANCES c, GLTB_AVGBAL b,sttm_branch d, sttm_account_class e, sttm_Customer f "
					+ "WHERE a.CUST_aC_NO= ? and a.account_class = c.account_class and f.customer_no = a.cust_no and a.CUST_aC_NO=b.gl_code(+) "
					+ "and a.BRANCH_CODE=d.BRANCH_CODE and a.account_class=e.account_class";

			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, acc.trim());
			rs = stat.executeQuery();
			if (rs.next()) {
				entity = new accountEntity();
				entity.setBranchCode(rs.getString("BRANCH_CODE"));
				entity.setBranchName(rs.getString("branch_name"));
				entity.setNoCif(rs.getString("CUST_NO"));
				entity.setNoRek(rs.getString("CUST_AC_NO"));
				entity.setNama(rs.getString("AC_DESC"));
				entity.setAccountClass(rs.getString("ACCOUNT_CLASS"));
				entity.setCcy(rs.getString("CCY"));
				entity.setRecordStat(rs.getString("record_stat"));
				entity.setAcOpenDate(rs.getString("ac_open_date"));

			}

		} catch (Exception e) {
			log.error("getDetailAccount : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return entity;
	}

	public List GetTransaksiSimpati(String id) {
		List lbb = new ArrayList();
		JurnalTodayDetail bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();

			sql = " select user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no "
					+ " from(select c.user_id,substr(a.trn_ref_no, 1, 3) as kode_cabang,a.trn_code as kode_transaksi,a.instrument_code as nomor_warkat, "
					+ " to_char(a.txn_dt_time,'DD-MM-YYYY') as tanggal,to_char(a.txn_dt_time, 'HH24:MI') as jam,b.trn_desc || ':' || "
					+ " ACPKS_FCJ_BMIJRNAL.cf_addltextformula (a.trn_ref_no, a.event_sr_no) || ',' || (SELECT DISTINCT x.ADDL_TEXT FROM DETB_JRNL_TXN_DETAIL x "
					+ " where x.reference_no = a.trn_ref_no and serial_no = nvl(a.curr_no, a.ENTRY_SEQ_NO))||''||(SELECT y.narrative FROM detbs_rtl_teller y "
					+ " where y.trn_ref_no = a.trn_ref_no) as ket_kode_transaksi,case when a.drcr_ind = 'D' then (case when a.trn_code = '100' then null "
					+ " else(case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as debet,case when a.drcr_ind = 'C' then (case "
					+ " when a.trn_code = '200' then null else (case when a.ac_ccy = 'IDR' then a.lcy_amount else a.fcy_amount end)end)end as kredit, "
					+ " a.auth_id AS user_approval,a.ac_no from actb_daily_log a, sttm_trn_code b,(select d.user_id from smtb_user d, smtb_user_role c "
					+ " where c.user_id = d.user_id and c.role_id IN ('TL_IN','TL_IN_JR','BO_IN','BO_IN_JR','BO_ALL_IN','TL_IN_JR_1','TL_IN_JR_2','TL_IN_JR_3', "
					+ " 'TL_IN_SEN_1','TL_IN_SEN_2','TL_IN_SEN_3','TL_IN_SEN_4','TL_IN_SEN_5','TL_IN_SEN_6','TL_IN_SEN_7','TL_IN_SEN_8')) c "
					+ " where a.trn_code = b.trn_code and a.user_id = c.user_id and a.delete_stat <> 'D' and a.ac_no NOT IN ('195002002', '695001006', '195001006') "
					+ " and a.ac_no NOT BETWEEN '101002000' AND '1010022099' and a.user_id NOT IN ('FLEXSWITCH','SYSTEM') "
					+ " and a.auth_id NOT IN ('FLEXSWITCH','SYSTEM')) a1 where user_id = ?"
					+ " group by user_id,kode_cabang,tanggal,jam,kode_transaksi,nomor_warkat,ket_kode_transaksi,debet,kredit,user_approval,ac_no"
					+ " order by jam";

			stat = conn.prepareStatement(sql);
			stat.setString(1, id);
			rs = stat.executeQuery();
			while (rs.next()) {
				bb = new JurnalTodayDetail();
				bb.setUser_id(rs.getString("user_id"));
				bb.setKode_cabang(rs.getString("kode_cabang"));
				bb.setTanggal(rs.getString("tanggal"));
				bb.setJam(rs.getString("jam"));
				bb.setKode_transaksi(rs.getString("kode_transaksi"));
				bb.setNomor_warkat(rs.getString("nomor_warkat") == null ? "-" : rs.getString("nomor_warkat"));
				bb.setKet_kode_transaksi(rs.getString("ket_kode_transaksi"));
				bb.setUser_approval(rs.getString("user_approval"));
				bb.setAc_no(rs.getString("ac_no"));

				if (rs.getBigDecimal("debet") != null) {
					bb.setDebet(rs.getBigDecimal("debet"));
				} else {
					bb.setDebet(new BigDecimal(0));
				}

				if (rs.getBigDecimal("kredit") != null) {
					bb.setKredit(rs.getBigDecimal("kredit"));
				} else {
					bb.setKredit(new BigDecimal(0));
				}
				lbb.add(bb);
			}
		} catch (SQLException ex) {
			log.error(" GetTransaksiSimpati : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public List GetJadangFbti(String instNo, String custId, String name) {
		List lbb = new ArrayList();
		jadangFbti fbti = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getDb2BmiprodDS().getConnection();

			sql = " SELECT INSTALLMENT_NO,CUSTOMER_ID,CUSTOMER_NAME,PRODUCT_CODE,PRODUCT_CATEGORY,COUNT (TENOR) AS TENOR,CCY,STATUS"
					+ " FROM (SELECT MASTER.MASTER_REF AS INSTALLMENT_NO,MASTER.NPRCUSTMNM AS CUSTOMER_ID,MASTER.PRINAME_L1 AS CUSTOMER_NAME, "
					+ " CASE WHEN SUBSTR(MASTER.MASTER_REF,7,3) = 'BYF' THEN 'BYF'"
					+ " WHEN SUBSTR(MASTER.MASTER_REF,7,3) = 'ARF' THEN 'ARF' ELSE 'OTHER' END AS PRODUCT_CODE, "
					+ " CASE WHEN SUBSTR(MASTER.MASTER_REF,7,3) = 'BYF' THEN 'MURABAHAH' "
					+ " WHEN SUBSTR(MASTER.MASTER_REF,7,3) = 'ARF' THEN 'QARDH' ELSE 'OTHER' END AS PRODUCT_CATEGORY, "
					+ " MASTER.CTRCT_DATE AS BOOK_DATE,EXTMASTERINS.INSDT AS MATURITY_DATE,"
					+ " ROW_NUMBER () OVER () AS TENOR,EXTMASTERINS.CCY,"
					// + " EXTMASTERINS.CCY,EXTMASTERINS.TENOR, "
					+ " EXTMASTERINS.TOTINSMN AS AMOUNT_PRINCIPAL, EXTMASTERINS.INTRST AS AMOUNT_MARGIN, "
					+ " EXTMASTERINS.TOTINSMN + EXTMASTERINS.INTRST AS AMOUNT_TOTAL, CASE WHEN MASTER.ACTIVE ='Y' THEN 'ACTIVE' "
					+ " ELSE 'NO ACTIVE' END AS STATUS FROM tradein1.MASTER MASTER, tradein1.EXTMASTER EXTMASTER,tradein1.EXTMASTERINS EXTMASTERINS "
					+ " WHERE EXTMASTERINS.FK_MASTER = EXTMASTER.KEY29 AND EXTMASTER.MASTER = MASTER.KEY97 AND MASTER.ACTIVE ='Y') WHERE ";

			if (!"".equals(instNo) && instNo != null) {
				sql += " INSTALLMENT_NO = ? ";
			}
			if (!"".equals(custId) && custId != null) {
				sql += " CUSTOMER_ID = ? ";
			}
			if (!"".equals(name) && name != null) {
				sql += " CUSTOMER_NAME like ? ";
			}

			sql += " GROUP BY INSTALLMENT_NO,CUSTOMER_ID,CUSTOMER_NAME,PRODUCT_CODE,PRODUCT_CATEGORY,CCY,STATUS";

			System.out.println("sql GetJadangFbti:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if (!"".equals(instNo) && instNo != null) {
				stat.setString(i++, instNo.trim());
			}
			if (!"".equals(custId) && custId != null) {
				stat.setString(i++, custId.trim());
			}
			if (!"".equals(name) && name != null) {
				stat.setString(i++, "%" + name.toUpperCase() + "%");
			}

			rs = stat.executeQuery();
			while (rs.next()) {
				fbti = new jadangFbti();
				fbti.setInstallmentNo(rs.getString("INSTALLMENT_NO"));
				fbti.setCustomerId(rs.getString("CUSTOMER_ID"));
				fbti.setCustomerName(rs.getString("CUSTOMER_NAME"));
				fbti.setProductCode(rs.getString("PRODUCT_CODE"));
				fbti.setProductCategory(rs.getString("PRODUCT_CATEGORY"));
				fbti.setCcy(rs.getString("CCY"));
				fbti.setTenor(rs.getInt("TENOR"));
				fbti.setStatus(rs.getString("STATUS"));
				lbb.add(fbti);
			}
		} catch (Exception ex) {
			log.error(" getJadangFbti : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}

	public jadangFbti getFBTI(String instNo) {
		jadangFbti fbtiDetail = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();

			sql = "SELECT to_char(download_date,'DD-MM-YYYY') as TANGGAl,CUSTOMER_ID,CUSTOMER_NAME,ACCOUNT_NUMBER NO_KARTU,BRANCH_CODE, "
					+ " A.PRODUCT_CODE,case when A.PRODUCT_CODE = 'BYF' then 'Buyer Financing' when A.PRODUCT_CODE = 'ARF' "
					+ " then 'Account Receiveable' else 'Other' end as Produk_Type,to_char(loan_due_date,'dd-mm-yyyy') as due_date, "
					+ " CASE WHEN A.PRODUCT_CODE = 'BYF' THEN 'MURABAHAH' WHEN A.PRODUCT_CODE = 'ARF' THEN 'QARDH'"
					+ " ELSE 'OTHER' END AS PRODUCT_CATEGORY, TENOR,PPAP_ON_LCY PPAP,b.CTRCT_DATE TGL_DROPING,INTEREST_RATE RATE,PLAFOND, "
					+ " TOTAL_MARGIN as MARGIN,PLAFOND + TOTAL_MARGIN as HARGA_JUAL,nvl (TOTAL_MARGIN / TENOR,0) as PROFIT_MARGIN_AMOUNT, "
					+ " LOAN_START_DATE TGL_AWAL_ANGSURAN,next_payment_date TGL_ANGSUR_SELANJUTNYA,outstanding as OS_POKOK, "
					+ " TUNGGAKAN_POKOK,INTRST OS_MARGIN,TUNGGAKAN_MARGIN,currency,C01.LOV_DESC AS KATEGORI_USAHA, "
					+ " C02.LOV_DESC as	KATEGORI_USAHA_PORTFOLIO,C03.LOV_DESC as JENIS_PIUTANG,C04.LOV_DESC as SIFAT_PIUTANG,C05.LOV_DESC as ORIENTASI_PENGGUNAAN, "
					+ " C06.LOV_DESC as SEKTOR_EKONOMI_USAHA,C07.LOV_DESC as LOKASI_PROYEK,C.FINMARKE FINANCING_MARKETING_CODE,substr(C09.LOV_DESC,4,52) AS SEGMENTASI_ACCOUNT, "
					+ " C10.LOV_DESC as	PROGRAM_PEMBIAYAAN,C11.LOV_DESC as PEMBIAYAAN_BACK_TO_BACK,c.BACKACCT NOMOR_REK_BACK_TO_BACK,C13.LOV_DESC as JENIS_KARTU,"
					+ " SUBSTR(C14.LOV_DESC,4,45) AS UPDATE_SEGMENTASI,SUBSTR(C15.LOV_DESC,4,45) as PRODUK_SEGMENTASI,SUBSTR(C16.LOV_DESC,5,16) AS MODEL_KERJA,INTEREST_RATE,"
					+ " BI_COLLECTABILITY KOL_ORI, INTRST * exchange_rate OS_MARGIN_LCY,outstanding * exchange_rate OS_POKOK_LCY,a.account_status"
					+ " FROM STGDWH.TF_MASTER_ACCOUNT_EDW_STG a JOIN refdwh.ref_tradeinovasi_product PROD ON A.PRODUCT_CODE = PROD.PRODUCT_CODE "
					+ " LEFT JOIN STGDWH.TF_MASTER b ON A.ACCOUNT_NUMBER = b.MASTER_REF LEFT JOIN STGDWH.TF_EXTMASTER c ON C.MASTER = B.KEY97"
					+ " LEFT JOIN (SELECT  MASTER.MASTER_REF, NVL(SUM(EXTMASTERINS.INTRST),0) INTRST "
					+ " FROM STGDWH.TF_EXTMASTERINS EXTMASTERINS, STGDWH.TF_MASTER MASTER, STGDWH.TF_EXTMASTER EXTMASTER,STGDWH.TF_C8PF C8PF "
					+ " WHERE EXTMASTERINS.FK_MASTER = EXTMASTER.KEY29 AND EXTMASTER.MASTER = MASTER.KEY97 AND EXTMASTERINS.CCY = C8PF.C8CCY "
					+ " AND EXTMASTERINS.TOTREP<= 0 GROUP BY MASTER.MASTER_REF) d ON d.MASTER_REF = A.ACCOUNT_NUMBER "
					+ " LEFT JOIN (SELECT  MASTER.MASTER_REF,POSTING.ACC_TYPE GL_PRINCIPAL,baseevent.finished "
					+ " FROM STGDWH.TF_MASTER MASTER, STGDWH.TF_FNCEMASTER FNCEMASTER, STGDWH.TF_BASEEVENT BASEEVENT, STGDWH.TF_INITFINCE INITFINCE, "
					+ " STGDWH.TF_RELITEM RELITEM ,STGDWH.TF_POSTING POSTING,STGDWH.TF_EXEMPL30 EXEMPL30 WHERE MASTER.KEY97 = FNCEMASTER.KEY97 "
					+ " AND BASEEVENT.MASTER_KEY = MASTER.KEY97 AND BASEEVENT.KEY97=INITFINCE.KEY97 AND RELITEM.EVENT_KEY = BASEEVENT.KEY97 "
					+ " AND RELITEM.KEY97=POSTING.KEY97 AND RELITEM.TYPEFLAG=14812 AND MASTER.EXEMPLAR = EXEMPL30.KEY97 "
					+ " AND ((EXEMPL30.CODE79='FSA' AND POSTING.SP_CODE IN ('125013','111021','111022','111023','115013','115011','111063')) "
					+ " OR (EXEMPL30.CODE79='FIL' AND POSTING.SP_CODE IN ('114013','114012','114011','181281','115013','115011','210101','125012')) "
					+ " OR (EXEMPL30.CODE79='FEL' AND POSTING.SP_CODE IN ('111021','213021','111022','213023','123011','123013','210101','125011','125013')) "
					+ " OR (EXEMPL30.CODE79='FOC' AND POSTING.SP_CODE IN ('125013')))) e ON e.MASTER_REF = A.ACCOUNT_NUMBER "
					+ " LEFT JOIN STGDWH.TF_FNCEMASTER F ON F.key97 = B.KEY97 LEFT JOIN STGDWH.TF_INTERE81 G ON G.KEY97 = F.INTACC_KEY "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -2 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C01_KATEGORI_USAHA' ) C01 ON C01.CODE =  C.KAT_USHA"
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -2 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C02_KATEGORI_PORTOFOLIO_KONSUMSI') C02 ON C02.CODE =  C. PRTFLUSH "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -2 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C03_JENIS_PIUTANG_USAHA' ) C03 ON C03.CODE =  C.JNSPIUTN "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -2 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C04_SIFAT_PIUTANG') C04 ON C04.CODE =  C.SFTPIUTN "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -2 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C05_ORIENTASI_PENGGUNAAN') C05 ON C05.CODE =  C.ORIENTAS "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -2 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C06_SEKTOR_EKONOMI_USAHA') C06 ON C06.CODE =  C.SEKTUSHA "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -2 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C07_LOKASI_PROYEK') C07 ON C07.CODE =  C.PROJLOC "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'_') -1 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C09_SEGMENTASI_ACCOUNT' ) C09 ON C09.CODE =  C.SEGMENT "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'_') -1 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C10_PROGRAM_PEMBIAYAAN') C10 ON C10.CODE =  C.PROGBIAY "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'_') -1 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C11_PEMBIAYAAN_BACK_TO_BACK') C11 ON C11.CODE =  C.BIAYABCK "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -1 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C13_JENIS_KARTU') C13 ON C13.CODE =  C.JENKARTU "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'_') -1 ))  CODE,    LOV,LOV_DESC FROM  STGDWH.udtm_lov WHERE field_name = 'C14_UPDATE_SEGMENTASI_ACCOUNT') C14 ON C14.CODE =  C.UPDSEGME "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'_') -1 ))  CODE,    LOV,LOV_DESC  FROM  STGDWH.udtm_lov WHERE field_name = 'C15_PRODUK_SEGMENTASI_ACCOUNT') C15 ON C15.CODE =  C.PRDSEGME "
					+ " LEFT JOIN (SELECT field_name, TRIM(SUBSTR(LOV, 1, INSTR(LOV,'-') -1 ))  CODE,    LOV ,LOV_DESC FROM  STGDWH.udtm_lov WHERE field_name = 'C16_JENIS_PENGGUNAAN') C16 ON C16.CODE =  C.JNPGNA "
					+ " LEFT JOIN (SELECT * FROM bmidwh.DIM_SEGMENT WHERE dw_row_ind = 'Y') H ON    C09.LOV = H.SEGMENTASI_ACCOUNT "
					+ " WHERE NO_KARTU= ? AND account_status <>'L' order by download_date";

			System.out.println("sql getFBTI:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, instNo.trim());
			rs = stat.executeQuery();
			fbtiDetail = new jadangFbti();
			while (rs.next()) {
				fbtiDetail.setCustomerId(rs.getString("CUSTOMER_ID"));
				fbtiDetail.setCustomerName(rs.getString("customer_name"));
				fbtiDetail.setInstallmentNo(rs.getString("NO_KARTU"));
				fbtiDetail.setBranch(rs.getString("BRANCH_CODE"));
				fbtiDetail.setProductCode(rs.getString("PRODUCT_CODE"));
				fbtiDetail.setProductCategory(rs.getString("PRODUCT_CATEGORY"));
				fbtiDetail.setProductType(rs.getString("Produk_Type"));
				fbtiDetail.setDueDate(rs.getString("due_date"));
				fbtiDetail.setTenor(rs.getInt("TENOR"));
				fbtiDetail.setRate(rs.getDouble("RATE"));
//				fbtiDetail.setRate(rs.getString("RATE"));
				fbtiDetail.setCcy(rs.getString("currency"));
				fbtiDetail.setPlafond(rs.getBigDecimal("PLAFOND"));
				fbtiDetail.setKategoriKonsumsiUsaha(rs.getString("KATEGORI_USAHA"));
				fbtiDetail.setKategoriPortofolioKonsumsiUsaha(rs.getString("KATEGORI_USAHA_PORTFOLIO"));
				fbtiDetail.setJenisPiutang(rs.getString("JENIS_PIUTANG"));
				fbtiDetail.setSifatPiutang(rs.getString("SIFAT_PIUTANG"));
				fbtiDetail.setOrientasiPenggunaan(rs.getString("ORIENTASI_PENGGUNAAN"));
				fbtiDetail.setSektorEkonomiUsaha(rs.getString("SEKTOR_EKONOMI_USAHA"));
				fbtiDetail.setLokasiProyek(rs.getString("LOKASI_PROYEK"));
				fbtiDetail.setKodeMarketing(rs.getString("FINANCING_MARKETING_CODE"));
				fbtiDetail.setSegmentAccount(rs.getString("SEGMENTASI_ACCOUNT"));
				fbtiDetail.setProgramPembiayaan(rs.getString("PROGRAM_PEMBIAYAAN"));
				fbtiDetail.setPembiayaanBackToBack(rs.getString("PEMBIAYAAN_BACK_TO_BACK") == null ? "-" : rs.getString("PEMBIAYAAN_BACK_TO_BACK"));
				
//				ht.setInstrument_code(rs.getString("instrument_code") == null ? "" : rs.getString("instrument_code"));
//				fbtiDetail.setPembiayaanBackToBack(rs.getString("PEMBIAYAAN_BACK_TO_BACK"));
				fbtiDetail.setNoRekBackToBack(rs.getString("NOMOR_REK_BACK_TO_BACK") == null ? "-" : rs.getString("NOMOR_REK_BACK_TO_BACK"));
				fbtiDetail.setJenisKartu(rs.getString("JENIS_KARTU"));
				fbtiDetail.setUpdateSegmentasi(rs.getString("UPDATE_SEGMENTASI"));
				fbtiDetail.setProdukSegmentasi(rs.getString("PRODUK_SEGMENTASI"));
				fbtiDetail.setModelKerja(rs.getString("MODEL_KERJA"));
				fbtiDetail.setHargaJual(rs.getBigDecimal("HARGA_JUAL"));
				fbtiDetail.setMarginAwal(rs.getBigDecimal("MARGIN"));
				// lbb.add(fbtiDetail);
			}

		} catch (Exception e) {
			log.error(" getFBTI : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return fbtiDetail;
	}

	public List getDetailJadangFbti(String instNo) {
		List lbb = new ArrayList();
		fbti detailJadang = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getNetezzaDS().getConnection();

			sql = "SELECT MASTER.MASTER_REF AS INSTALLMENT_NO,MASTER.NPRCUSTMNM AS CUSTOMER_ID,MASTER.PRINAME_L1 AS CUSTOMER_NAME, "
					+ " TO_CHAR(EXTMASTERINS.INSDT,'DD-MM-YYYY') AS MATURITY_DATE,EXTMASTERINS.CCY,EXTMASTERINS.TENOR,EXTMASTERINS.TOTINSMN AS AMOUNT_PRINCIPAL, "
					+ " EXTMASTERINS.INTRST AS AMOUNT_MARGIN,EXTMASTERINS.AMT AS AMOUNT,EXTMASTERINS.TOTREP AS TOTAL_REPAYMENT,"
					+ " EXTMASTERINS.PRGMGNRT AS RATE,CASE WHEN MASTER.ACTIVE ='Y' THEN 'YES' ELSE 'NO' END AS ACC_STATUS "
					+ " FROM STGDWH.TF_EXTMASTERINS EXTMASTERINS, STGDWH.TF_MASTER MASTER, STGDWH.TF_EXTMASTER EXTMASTER "
					+ " WHERE EXTMASTERINS.FK_MASTER = EXTMASTER.KEY29 AND EXTMASTER.MASTER = MASTER.KEY97 AND MASTER.MASTER_REF = ? "
					+ " ORDER BY EXTMASTERINS.INSDT ASC";
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, instNo.trim());
			System.out.println("sql getDetailJadangFbti " + sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				detailJadang = new fbti();
				detailJadang.setInstallmentNo(rs.getString("INSTALLMENT_NO"));
				detailJadang.setTanggal(rs.getString("MATURITY_DATE"));
				detailJadang.setAmountPrincipal(rs.getBigDecimal("AMOUNT_PRINCIPAL"));
				detailJadang.setPrincipalRepayment(rs.getBigDecimal("AMOUNT"));
				detailJadang.setTenor(rs.getInt("TENOR"));
				detailJadang.setProfitMargin(rs.getBigDecimal("AMOUNT_MARGIN"));
				detailJadang.setRate(rs.getDouble("RATE"));
				detailJadang.setTotalRepayment(rs.getBigDecimal("TOTAL_REPAYMENT"));
				lbb.add(detailJadang);
			}

		} catch (Exception e) {
			log.error(" getDetailJadangFbti : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getFacility (String NoFasilitas, String Name, String status){
		List lbb = new ArrayList();
		facility fac = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "SELECT DISTINCT LIAB.LIAB_NO, FAC.LINE_CODE AS NO_FASILITAS, FAC.LINE_SERIAL, FAC.LINE_CODE || FAC.LINE_SERIAL AS FASILITAS_KE,"
					+ " LIAB.LIAB_NAME AS NAMA_NASABAH, FAC.LINE_CURRENCY AS CCY, FAC.LIMIT_AMOUNT, FAC.AVAILABLE_AMOUNT,"
					+ " FAC.DESCRIPTION AS NAMA_FASILITAS,FAC.BLOCK_AMOUNT,FAC.UTILISATION, TO_CHAR (FAC.LINE_START_DATE, 'DD-MM-RRRR') AS LINE_START_DATE,"
					+ " TO_CHAR (FAC.LINE_EXPIRY_DATE, 'DD-MM-RRRR') AS LINE_EXPIRY_DATE, "
					+ " CASE WHEN FAC.RECORD_STAT = 'O' THEN 'OPEN' ELSE 'CLOSE' END AS RECORD_STAT, CLTB.LINKED_REFERENCE_NO "
					+ " FROM GETM_LIAB LIAB JOIN GETM_FACILITY FAC ON FAC.LIAB_ID = LIAB.ID"
					+ " JOIN (SELECT ACCOUNT_NUMBER, LINKED_REFERENCE_NO, SUBSTR (LINKED_REFERENCE_NO, 1, 9) AS CUSTOMER_NO"
					+ " FROM CLTB_ACC_COLL_LINK_DTLS) CLTB ON LIAB.LIAB_NO || FAC.LINE_SERIAL = CLTB.LINKED_REFERENCE_NO"
					+ " WHERE ";
					
					if (!"".equals(NoFasilitas)) {
						if ("Open".equals(status)){
							sql += " LIAB.LIAB_NO = ? AND FAC.RECORD_STAT = 'O'";
						} else if ("Close".equals(status)){
							sql += " LIAB.LIAB_NO = ? AND FAC.RECORD_STAT = 'C'";
						} else {
							sql += " LIAB.LIAB_NO = ?";
						}
					}else if (!"".equals(Name)){
						if ("Open".equals(status)){
							sql += " LIAB.LIAB_NAME LIKE ? AND FAC.RECORD_STAT = 'O'";
						} else if ("Close".equals(status)){
							sql += " LIAB.LIAB_NAME LIKE ? AND FAC.RECORD_STAT = 'C'";
						} else {
							sql += " LIAB.LIAB_NAME LIKE ? ";
						}
					}
					 sql += " ORDER BY FAC.LINE_SERIAL ASC";
					
					System.out.println("sql getFacility :" + sql);
					int i = 1;
					stat = conn.prepareStatement(sql);
					if (!"".equals(NoFasilitas)){
						if ("Open".equals(status)){
							stat.setString(i++, NoFasilitas);
						} else if ("Close".equals(status)){
							stat.setString(i++, NoFasilitas);
						} else {
							stat.setString(i++, NoFasilitas);
						}
					} else if (!"".equals(Name)){
						if ("Open".equals(status)){
							stat.setString(i++, "%" + Name.toUpperCase() + "%");
						} else if ("Close".equals(status)){
							stat.setString(i++, "%" + Name.toUpperCase() + "%");
						} else {
							stat.setString(i++, "%" + Name.toUpperCase() + "%");
						}
					}
					rs = stat.executeQuery();
					while (rs.next()){
						fac = new facility();
						fac.setNamaNasabah(rs.getString("NAMA_NASABAH"));
						fac.setNoFasilitas(rs.getString("NO_FASILITAS"));
						fac.setNamaFasilitas(rs.getString("NAMA_FASILITAS"));
						fac.setFasilitasKe(rs.getString("FASILITAS_KE"));
						fac.setLineSerial(rs.getInt("LINE_SERIAL"));
						fac.setCcy(rs.getString("CCY"));
						fac.setLimitAmount(rs.getBigDecimal("LIMIT_AMOUNT"));
						fac.setAvailableAmount(rs.getBigDecimal("AVAILABLE_AMOUNT"));
						fac.setBlockAmount(rs.getBigDecimal("BLOCK_AMOUNT"));
						fac.setUtilization(rs.getBigDecimal("UTILISATION"));
						fac.setStartDate(rs.getString("LINE_START_DATE"));
						fac.setExpiryDate(rs.getString("LINE_EXPIRY_DATE"));
						fac.setRecordStat(rs.getString("RECORD_STAT"));
						lbb.add(fac);
					}
			
		} catch (Exception e) {
			log.error(" getFacility : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getdetailFasilitas(String fasilitasKe, String status){
		System.out.println("status sql :" + status);
		List lbb = new ArrayList();
		facility fac = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
//			sql = " SELECT CLTB.ACCOUNT_NUMBER,CLTB.CUSTOMER_ID,FAC.NO_REKENING_ANGSURAN,FAC.NAMA_NASABAH, "
//					+" CLTB.PRODUCT_CODE,CLTB.PRODUCT_CATEGORY,TO_CHAR(CLTB.VALUE_DATE,'DD-MM-RRRR') AS TANGGAL_PENCAIRAN, "
//					+" TO_CHAR(CLTB.MATURITY_DATE,'DD-MM-RRRR') AS TANGGAL_JATUH_TEMPO,CASE WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' "
//					+" WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE' WHEN CLTB.ACCOUNT_STATUS = 'V' THEN 'REVESAL' "
//					+" ELSE 'HOLD' END STATUS_KARTU FROM CLTB_ACCOUNT_MASTER CLTB JOIN(SELECT ACCOUNT_NUMBER, "
//					+" NO_REKENING_ANGSURAN,NAMA_NASABAH,CUSTOMER_ID,NO_FACILITAS_KE "
//					+" FROM (SELECT FAC.LINE_CODE AS CUSTOMER_ID,CUST.CUSTOMER_NAME1 AS NAMA_NASABAH, "
//					+" FAC.LINE_SERIAL,FAC.LINE_CODE || FAC.LINE_SERIAL AS NO_FACILITAS_KE,CLTB.ACCOUNT_NUMBER, "
//					+" CLTB.LINKED_REFERENCE_NO,COMP.NO_REKENING_ANGSURAN FROM GETM_FACILITY FAC JOIN ( "
//					+" SELECT ACCOUNT_NUMBER,LINKED_REFERENCE_NO,SUBSTR(LINKED_REFERENCE_NO,1,9) AS CUSTOMER_NO "
//					+" FROM CLTB_ACC_COLL_LINK_DTLS) CLTB ON  FAC.LINE_CODE || FAC.LINE_SERIAL = CLTB.LINKED_REFERENCE_NO "
//					+" JOIN STTM_CUSTOMER CUST ON FAC.LINE_CODE = CUST.CUSTOMER_NO JOIN (SELECT ACCOUNT_NUMBER,COMPONENT_NAME, "
//					+" DR_PROD_AC AS NO_REKENING_ANGSURAN FROM CLTB_ACCOUNT_COMPONENTS WHERE COMPONENT_NAME = 'PRINCIPAL') COMP "
//					+" ON CLTB.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER) FAC) FAC ON CLTB.ACCOUNT_NUMBER = FAC.ACCOUNT_NUMBER "
//					+" AND CLTB.CUSTOMER_ID = FAC.CUSTOMER_ID WHERE FAC.NO_FACILITAS_KE = ? ";
			
			if ("Open".equals(status)){
				System.out.println("if");
				sql =" SELECT COL.FASILITAS_KE, COL.ACCOUNT_NUMBER, COL.NO_REKENING_ANGSURAN, COL.NAMA_NASABAH, "
						+ " CLTB.PRODUCT_CODE,CLTB.PRODUCT_CATEGORY, TO_CHAR (CLTB.VALUE_DATE, 'DD-MM-RRRR') AS TANGGAL_PENCAIRAN,"
						+ " TO_CHAR (CLTB.MATURITY_DATE, 'DD-MM-RRRR') AS TANGGAL_JATUH_TEMPO, "
						+ " CASE WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE'"
						+ " WHEN CLTB.ACCOUNT_STATUS = 'V' THEN 'REVESAL' ELSE 'HOLD' END STATUS_ACCOUNT"
						+ " FROM CLTB_ACCOUNT_MASTER CLTB JOIN (SELECT COL.FASILITAS_KE, COL.ACCOUNT_NUMBER,"
						+ " COL.NO_REKENING_ANGSURAN, LIAB.LIAB_NAME AS NAMA_NASABAH "
						+ " FROM GETM_LIAB LIAB JOIN GETM_FACILITY FAC ON FAC.LIAB_ID = LIAB.ID"
						+ " JOIN (SELECT DTLS.ACCOUNT_NUMBER, DTLS.LINKED_REFERENCE_NO AS FASILITAS_KE,"
						+ " COMP.NO_REKENING_ANGSURAN FROM CLTB_ACC_COLL_LINK_DTLS DTLS"
						+ " JOIN (SELECT ACCOUNT_NUMBER, COMPONENT_NAME, DR_PROD_AC AS NO_REKENING_ANGSURAN"
						+ " FROM CLTB_ACCOUNT_COMPONENTS WHERE COMPONENT_NAME = 'PRINCIPAL') COMP"
						+ " ON DTLS.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER) COL "
						+ " ON FAC.LINE_CODE||FAC.LINE_SERIAL = COL.FASILITAS_KE) COL"
						+ " ON CLTB.ACCOUNT_NUMBER = COL.ACCOUNT_NUMBER WHERE FASILITAS_KE = ? AND CLTB.ACCOUNT_STATUS = 'A' "
						+ " ORDER BY TO_CHAR (CLTB.VALUE_DATE, 'DD-MM-RRRR') DESC";
			} else if ("Close".equals(status)) {
				System.out.println("else if");
				sql =" SELECT COL.FASILITAS_KE, COL.ACCOUNT_NUMBER, COL.NO_REKENING_ANGSURAN, COL.NAMA_NASABAH, "
						+ " CLTB.PRODUCT_CODE,CLTB.PRODUCT_CATEGORY, TO_CHAR (CLTB.VALUE_DATE, 'DD-MM-RRRR') AS TANGGAL_PENCAIRAN,"
						+ " TO_CHAR (CLTB.MATURITY_DATE, 'DD-MM-RRRR') AS TANGGAL_JATUH_TEMPO, "
						+ " CASE WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE'"
						+ " WHEN CLTB.ACCOUNT_STATUS = 'V' THEN 'REVESAL' ELSE 'HOLD' END STATUS_ACCOUNT"
						+ " FROM CLTB_ACCOUNT_MASTER CLTB JOIN (SELECT COL.FASILITAS_KE, COL.ACCOUNT_NUMBER,"
						+ " COL.NO_REKENING_ANGSURAN, LIAB.LIAB_NAME AS NAMA_NASABAH "
						+ " FROM GETM_LIAB LIAB JOIN GETM_FACILITY FAC ON FAC.LIAB_ID = LIAB.ID"
						+ " JOIN (SELECT DTLS.ACCOUNT_NUMBER, DTLS.LINKED_REFERENCE_NO AS FASILITAS_KE,"
						+ " COMP.NO_REKENING_ANGSURAN FROM CLTB_ACC_COLL_LINK_DTLS DTLS"
						+ " JOIN (SELECT ACCOUNT_NUMBER, COMPONENT_NAME, DR_PROD_AC AS NO_REKENING_ANGSURAN"
						+ " FROM CLTB_ACCOUNT_COMPONENTS WHERE COMPONENT_NAME = 'PRINCIPAL') COMP"
						+ " ON DTLS.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER) COL "
						+ " ON FAC.LINE_CODE||FAC.LINE_SERIAL = COL.FASILITAS_KE) COL"
						+ " ON CLTB.ACCOUNT_NUMBER = COL.ACCOUNT_NUMBER WHERE FASILITAS_KE = ? AND CLTB.ACCOUNT_STATUS = 'L' "
						+ " ORDER BY TO_CHAR (CLTB.VALUE_DATE, 'DD-MM-RRRR') DESC";
			} else {
				System.out.println("else");
				sql =" SELECT COL.FASILITAS_KE, COL.ACCOUNT_NUMBER, COL.NO_REKENING_ANGSURAN, COL.NAMA_NASABAH, "
						+ " CLTB.PRODUCT_CODE,CLTB.PRODUCT_CATEGORY, TO_CHAR (CLTB.VALUE_DATE, 'DD-MM-RRRR') AS TANGGAL_PENCAIRAN,"
						+ " TO_CHAR (CLTB.MATURITY_DATE, 'DD-MM-RRRR') AS TANGGAL_JATUH_TEMPO, "
						+ " CASE WHEN CLTB.ACCOUNT_STATUS = 'A' THEN 'ACTIVE' WHEN CLTB.ACCOUNT_STATUS = 'L' THEN 'LIQUIDATE'"
						+ " WHEN CLTB.ACCOUNT_STATUS = 'V' THEN 'REVESAL' ELSE 'HOLD' END STATUS_ACCOUNT"
						+ " FROM CLTB_ACCOUNT_MASTER CLTB JOIN (SELECT COL.FASILITAS_KE, COL.ACCOUNT_NUMBER,"
						+ " COL.NO_REKENING_ANGSURAN, LIAB.LIAB_NAME AS NAMA_NASABAH "
						+ " FROM GETM_LIAB LIAB JOIN GETM_FACILITY FAC ON FAC.LIAB_ID = LIAB.ID"
						+ " JOIN (SELECT DTLS.ACCOUNT_NUMBER, DTLS.LINKED_REFERENCE_NO AS FASILITAS_KE,"
						+ " COMP.NO_REKENING_ANGSURAN FROM CLTB_ACC_COLL_LINK_DTLS DTLS"
						+ " JOIN (SELECT ACCOUNT_NUMBER, COMPONENT_NAME, DR_PROD_AC AS NO_REKENING_ANGSURAN"
						+ " FROM CLTB_ACCOUNT_COMPONENTS WHERE COMPONENT_NAME = 'PRINCIPAL') COMP"
						+ " ON DTLS.ACCOUNT_NUMBER = COMP.ACCOUNT_NUMBER) COL "
						+ " ON FAC.LINE_CODE||FAC.LINE_SERIAL = COL.FASILITAS_KE) COL"
						+ " ON CLTB.ACCOUNT_NUMBER = COL.ACCOUNT_NUMBER WHERE FASILITAS_KE = ? "
						+ " ORDER BY TO_CHAR (CLTB.VALUE_DATE, 'DD-MM-RRRR') DESC";
			}
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			System.out.println("sql detailFasilitas -->" + sql);
			stat.setString(i++, fasilitasKe.trim());
			rs = stat.executeQuery();
			while(rs.next()){
				fac = new facility();
				fac.setNoKartu(rs.getString("ACCOUNT_NUMBER"));
				fac.setFasilitasKe(rs.getString("FASILITAS_KE"));
				fac.setNoRekAngsuran(rs.getString("NO_REKENING_ANGSURAN"));
				fac.setNamaNasabah(rs.getString("NAMA_NASABAH"));
				fac.setProdCode(rs.getString("PRODUCT_CODE"));
				fac.setProdCategory(rs.getString("PRODUCT_CATEGORY"));
				fac.setTglPencairan(rs.getString("TANGGAL_PENCAIRAN"));
				fac.setTglJatuhTempo(rs.getString("TANGGAL_JATUH_TEMPO"));
				fac.setStatusKartu(rs.getString("STATUS_ACCOUNT"));
				lbb.add(fac);
			}
		} catch (Exception e) {
			log.error(" detailFasilitas : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public BranchMaster getBranch (String branch) {
		List lbb = new ArrayList();
		BranchMaster bb = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql =" select branch_code,branch_name"
				+" from sttm_branch where branch_code = ?";
			
			int i = 1;
			System.out.println("SQL getBranch :" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(i++, branch.trim());
			rs = stat.executeQuery();
			while (rs.next()){
				bb = new BranchMaster();
				bb.setBrnch_cd(rs.getString("branch_code"));
				lbb.add(bb);
			}
		} catch (Exception ex) {
			log.error(" getUser : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public List getCetakQuickCif (String branch, String chkBranch, String StartDate, String EndDate) {
		List lbb = new ArrayList();
		quickCif Quick = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.LOCAL_BRANCH, BRN.BRANCH_NAME, CUST.MOD_NO,"
					+ " TO_CHAR(CUST.CIF_CREATION_DATE,'DD-MM-RRRR') AS CIF_CREATION_DATE,"
					+ " FIRST.FUNCTION_ID, FIRST.MAKER_ID AS FIRST_MAKER_ID,"
					+ " CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9' THEN '-' ELSE CUST.MAKER_ID"
					+ " END AS LAST_MAKER_ID, CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9' THEN '-'"
					+ " ELSE CUST.CHECKER_ID END AS LAST_OTO, CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9'"
					+ " THEN '-' ELSE TO_CHAR(CUST.MAKER_DT_STAMP,'DD-MM-RRRR') END AS TANGGAL_LAST_UPDATE,"
					+ " CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9' THEN 'NO UPDATE' ELSE 'UPDATE' END AS STATUS"
					+ " FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, REC.MOD_NO, "
					+ " TO_DATE(CUST.CIF_CREATION_DATE, 'DD-MM-RRRR') AS CIF_CREATION_DATE, "
					+ " CUST.LOCAL_BRANCH, CUST.MAKER_ID,TO_DATE(CUST.MAKER_DT_STAMP,'DD-MM-RRRR') AS MAKER_DT_STAMP, "
					+ " CUST.CHECKER_ID, TO_DATE(CUST.CHECKER_DT_STAMP,'DD-MM-RRRR') AS CHECKER_DT_STAMP"
					+ " FROM STTM_CUSTOMER CUST JOIN (SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO, BRANCH_CODE, FUNCTION_ID"
					+ " FROM STTB_RECORD_LOG WHERE FUNCTION_ID IN('STDCIFQ9','STDCIFI9')) REC ON CUST.CUSTOMER_NO = REC.CIF AND CUST.MOD_NO = REC.MOD_NO"
					+ " WHERE TO_CHAR(CUST.MAKER_DT_STAMP, 'DD-MM-RRRR') = TO_CHAR(SYSDATE,'DD-MM-RRRR')) CUST"
					+ " JOIN (SELECT FIRST.CIF, MIN.MOD_NO, FIRST.FUNCTION_ID, FIRST.MAKER_ID "
					+ " FROM (SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO, BRANCH_CODE, FUNCTION_ID, MAKER_ID"
					+ " FROM STTB_RECORD_LOG WHERE FUNCTION_ID = 'STDCIFQ9' UNION ALL"
					+ " SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO, BRANCH_CODE, FUNCTION_ID, MAKER_ID"
					+ " FROM STTB_RECORD_LOG_HIST WHERE FUNCTION_ID = 'STDCIFQ9') FIRST"
					+ " JOIN (SELECT CIF, MIN(MOD_NO) AS MOD_NO "
					+ " FROM (SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO"
					+ " FROM STTB_RECORD_LOG WHERE FUNCTION_ID = 'STDCIFQ9' UNION ALL"
					+ " SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO"
					+ " FROM STTB_RECORD_LOG_HIST WHERE FUNCTION_ID = 'STDCIFQ9') GROUP BY CIF) MIN"
					+ " ON FIRST.CIF = MIN.CIF AND FIRST.MOD_NO = MIN.MOD_NO) FIRST"
					+ " ON CUST.CUSTOMER_NO = FIRST.CIF JOIN STTM_BRANCH BRN ON BRN.BRANCH_CODE = CUST.LOCAL_BRANCH ";
			
					if ("all".equals(chkBranch)){
						sql += " ORDER BY CUST.MAKER_DT_STAMP DESC";
					} else {
						sql +=" WHERE CUST.LOCAL_BRANCH = ? ORDER BY CUST.MAKER_DT_STAMP DESC ";
					}
			
			System.out.println("sql QuickCif :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if ("all".equals(chkBranch)){
				
			} else {
				stat.setString (i++, branch.trim());
			}
			rs = stat.executeQuery();
			while (rs.next()){
				Quick = new quickCif();
				Quick.setCustomerNo(rs.getString("CUSTOMER_NO"));
				Quick.setCustomerName(rs.getString("CUSTOMER_NAME1"));
				Quick.setBranchCode(rs.getString("LOCAL_BRANCH"));
				Quick.setBranchName(rs.getString("BRANCH_NAME"));
				Quick.setFunctionId(rs.getString("FUNCTION_ID"));
				Quick.setCifCreationDate(rs.getString("CIF_CREATION_DATE"));
				Quick.setFirstMakerId(rs.getString("FIRST_MAKER_ID"));
				Quick.setLastMakerId(rs.getString("LAST_MAKER_ID"));
				Quick.setLastOtoId(rs.getString("LAST_OTO"));
				Quick.setLastUpdate(rs.getString("TANGGAL_LAST_UPDATE"));
				Quick.setStatus(rs.getString("STATUS"));
				lbb.add(Quick);
			}
		} catch (Exception e) {
			log.error(" getCetakQuickCif : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public int getCountQuickCif (String branch, String chkCab, String StartDate, String EndDate) {
		int total = 0;
		List lbb = new ArrayList();
		quickCif Quick = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			sql = " SELECT COUNT (*) AS TOTAL FROM (SELECT TO_CHAR(CUST.CIF_CREATION_DATE,'DD-MM-RRRR') AS TANGGAL_LAPORAN,"
					+ " CUST.CUSTOMER_NO,CUST.CUSTOMER_NAME1 AS CUSTOMER_NAME,CUST.CUST_AC_NO,BRANCH.BRANCH_CODE,BRANCH.BRANCH_NAME,"
					+ " BRANCH.PARENT_BRANCH,BRANCH.PARENT_NAME,STTB.FUNCTION_ID,STTB.MOD_NO,STTB.MAKER_ID,"
					+ " STTB.CHECKER_ID,TO_CHAR(STTB.MAKER_DT_STAMP,'DD-MM-RRRR') AS TANGGAL_UPDATE_CIF,"
					+ " TO_CHAR(STTB.CHECKER_DT_STAMP,'DD-MM-RRRR') AS CHECKER_DT_STAMP"
					+ " FROM (SELECT CUST.CUSTOMER_NO,CUST.CUSTOMER_NAME1,ACC.CUST_AC_NO,CUST.CIF_CREATION_DATE"
					+ " FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.CIF_CREATION_DATE "
					+ " FROM STTM_CUSTOMER CUST) CUST JOIN STTM_CUST_ACCOUNT ACC ON CUST.CUSTOMER_NO = ACC.CUST_NO) CUST"
					+ " JOIN (SELECT*FROM(SELECT SUBSTR(KEY_ID,16,9) AS KEY_ID,FUNCTION_ID,MOD_NO,BRANCH_CODE,MAKER_ID,"
					+ " CHECKER_ID,RECORD_STAT,NULL AS MAKER_DT_STAMP,TO_DATE(CHECKER_DT_STAMP,'DD/MM/RRRR') AS CHECKER_DT_STAMP"
					+ " FROM STTB_RECORD_LOG WHERE FUNCTION_ID IN ('STDCIFQ9') UNION ALL SELECT SUBSTR(KEY_ID,16,9) AS KEY_ID,"
					+ " FUNCTION_ID,MOD_NO,BRANCH_CODE,MAKER_ID,CHECKER_ID,RECORD_STAT,TO_DATE(MAKER_DT_STAMP,'DD/MM/RRRR') AS MAKER_DT_STAMP,"
					+ " TO_DATE(CHECKER_DT_STAMP,'DD/MM/RRRR') AS CHECKER_DT_STAMP FROM STTB_RECORD_LOG "
					+ " WHERE FUNCTION_ID IN ('STDCIF','STDCIFI9')) STTB ) STTB  ON CUST.CUSTOMER_NO = STTB.KEY_ID"
					+ " JOIN (SELECT A.BRANCH_CODE,A.BRANCH_NAME,B.PARENT_BRANCH,B.PARENT_NAME FROM STTM_BRANCH A "
					+ " JOIN (SELECT A.PARENT_BRANCH,B.BRANCH_NAME AS PARENT_NAME FROM (SELECT BRANCH_CODE,PARENT_BRANCH"
					+ " FROM STTM_BRANCH) A JOIN (SELECT BRANCH_NAME,BRANCH_CODE FROM STTM_BRANCH) B"
					+ " ON A.PARENT_BRANCH = B.BRANCH_CODE GROUP BY A.PARENT_BRANCH, B.BRANCH_NAME) B"
					+ " ON A.PARENT_BRANCH = B.PARENT_BRANCH) BRANCH ON STTB.BRANCH_CODE = BRANCH.BRANCH_CODE WHERE";
					
					if ("all".equals(chkCab)){
						sql += " TO_CHAR (CUST.CIF_CREATION_DATE, 'DD-MM-YYYY') BETWEEN ? AND ? ";
					} else {
						System.out.println("masuk else");
						sql += " BRANCH.BRANCH_CODE = ? AND "
								+ " TO_CHAR (CUST.CIF_CREATION_DATE, 'DD-MM-YYYY') BETWEEN ? AND ?";
					}
					sql += " ORDER BY BRANCH_CODE,CUST_AC_NO,MOD_NO ASC)";
			
			System.out.println("sql getCountQuickCif :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			
					if ("all".equals(chkCab)){
						stat.setString(i++, StartDate);
						stat.setString(i++, EndDate);
					} else {
						stat.setString (i++, branch.trim());
						stat.setString(i++, StartDate);
						stat.setString(i++, EndDate);
					}
			
			rs = stat.executeQuery();
			while (rs.next()){
				total = rs.getInt("TOTAL");
				System.out.println("tot :" + total);
			}
		} catch (Exception e) {
			log.error(" getCountQuickCif : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return total;
	}
	
	public List getQuickCif (String branch, String chkCab, String StartDate, String EndDate){
		List lbb = new ArrayList();
		quickCif Quick = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		System.out.println("branch -->" + branch);
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
//			sql = " SELECT TO_CHAR(CUST.CIF_CREATION_DATE,'DD-MM-RRRR') AS TANGGAL_LAPORAN,"
//					+ " CUST.CUSTOMER_NO,CUST.CUSTOMER_NAME1 AS CUSTOMER_NAME,CUST.CUST_AC_NO,BRANCH.BRANCH_CODE,BRANCH.BRANCH_NAME,"
//					+ " BRANCH.PARENT_BRANCH,BRANCH.PARENT_NAME,STTB.FUNCTION_ID,STTB.MOD_NO,STTB.MAKER_ID,"
//					+ " STTB.CHECKER_ID,TO_CHAR (STTB.MAKER_DT_STAMP, 'DD-MM-RRRR') AS TANGGAL_UPDATE_CIF,"
//					+ " TO_CHAR (STTB.CHECKER_DT_STAMP,'DD-MM-RRRR') AS CHECKER_DT_STAMP"
//					+ " FROM (SELECT CUST.CUSTOMER_NO,CUST.CUSTOMER_NAME1,ACC.CUST_AC_NO,CUST.CIF_CREATION_DATE"
//					+ " FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.CIF_CREATION_DATE "
//					+ " FROM STTM_CUSTOMER CUST) CUST JOIN STTM_CUST_ACCOUNT ACC ON CUST.CUSTOMER_NO = ACC.CUST_NO) CUST"
//					+ " JOIN (SELECT*FROM(SELECT SUBSTR(KEY_ID,16,9) AS KEY_ID,FUNCTION_ID,MOD_NO,BRANCH_CODE,MAKER_ID,"
//					+ " CHECKER_ID,RECORD_STAT,NULL AS MAKER_DT_STAMP,TO_DATE(CHECKER_DT_STAMP,'DD/MM/RRRR') AS CHECKER_DT_STAMP"
//					+ " FROM STTB_RECORD_LOG WHERE FUNCTION_ID IN ('STDCIFQ9') UNION ALL SELECT SUBSTR(KEY_ID,16,9) AS KEY_ID,"
//					+ " FUNCTION_ID,MOD_NO,BRANCH_CODE,MAKER_ID,CHECKER_ID,RECORD_STAT,TO_DATE(MAKER_DT_STAMP,'DD/MM/RRRR') AS MAKER_DT_STAMP,"
//					+ " TO_DATE(CHECKER_DT_STAMP,'DD/MM/RRRR') AS CHECKER_DT_STAMP FROM STTB_RECORD_LOG "
//					+ " WHERE FUNCTION_ID IN ('STDCIF','STDCIFI9')) STTB ) STTB  ON CUST.CUSTOMER_NO = STTB.KEY_ID"
//					+ " JOIN (SELECT A.BRANCH_CODE,A.BRANCH_NAME,B.PARENT_BRANCH,B.PARENT_NAME FROM STTM_BRANCH A "
//					+ " JOIN (SELECT A.PARENT_BRANCH,B.BRANCH_NAME AS PARENT_NAME FROM (SELECT BRANCH_CODE,PARENT_BRANCH"
//					+ " FROM STTM_BRANCH) A JOIN (SELECT BRANCH_NAME,BRANCH_CODE FROM STTM_BRANCH) B"
//					+ " ON A.PARENT_BRANCH = B.BRANCH_CODE GROUP BY A.PARENT_BRANCH, B.BRANCH_NAME) B"
//					+ " ON A.PARENT_BRANCH = B.PARENT_BRANCH) BRANCH ON STTB.BRANCH_CODE = BRANCH.BRANCH_CODE WHERE ";
//					
//					if ("all".equals(chkCab)){
//						sql += " TO_CHAR(CUST.CIF_CREATION_DATE, 'DD-MM-YYYY') BETWEEN ? AND ?";
//					} else {
//						sql += " BRANCH.BRANCH_CODE = ? "
//								+ " AND TO_CHAR(CUST.CIF_CREATION_DATE, 'DD-MM-YYYY') BETWEEN ? AND ? ";
//					}
//					sql += " ORDER BY BRANCH_CODE,CUST_AC_NO,MOD_NO ASC";
			
			sql = "SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.LOCAL_BRANCH, BRN.BRANCH_NAME, CUST.MOD_NO, CUST.FUNCTION_ID,"
					+ " TO_CHAR (CUST.CIF_CREATION_DATE, 'DD/MM/RRRR') AS CIF_CREATION_DATE, FIRST.MAKER_ID AS FIRST_MAKER_ID,"
					+ " CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9' THEN '-' ELSE CUST.MAKER_ID END AS LAST_MAKER_ID,"
					+ " CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9' THEN '-' ELSE CUST.CHECKER_ID END AS LAST_OTO,"
					+ " CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9' THEN '-' ELSE TO_CHAR (CUST.MAKER_DT_STAMP, 'DD/MM/RRRR') END AS TANGGAL_LAST_UPDATE,"
					+ " CASE WHEN CUST.MOD_NO = '1' AND FIRST.FUNCTION_ID = 'STDCIFQ9' THEN  'NO UPDATE' ELSE 'UPDATE' END AS STATUS"
					+ " FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, REC.MOD_NO, REC.FUNCTION_ID, TO_DATE (CUST.CIF_CREATION_DATE, 'DD-MM-RRRR') "
					+ " AS CIF_CREATION_DATE, CUST.LOCAL_BRANCH, CUST.MAKER_ID, TO_DATE (CUST.MAKER_DT_STAMP, 'DD-MM-RRRR') AS MAKER_DT_STAMP,"
					+ " CUST.CHECKER_ID, TO_DATE (CUST.CHECKER_DT_STAMP, 'DD-MM-RRRR') AS CHECKER_DT_STAMP FROM STTM_CUSTOMER CUST"
					+ " JOIN (SELECT SUBSTR (KEY_ID, 16, 9) AS CIF, MOD_NO, BRANCH_CODE, FUNCTION_ID FROM STTB_RECORD_LOG"
					+ " WHERE FUNCTION_ID IN ('STDCIFI9','STDCIFQ9') AND MOD_NO IN ('1','2')) REC ON CUST.CUSTOMER_NO = REC.CIF AND CUST.MOD_NO = REC.MOD_NO"
					+ " WHERE TO_DATE (CUST.MAKER_DT_STAMP, 'DD-MM-RRRR') = TO_DATE (SYSDATE, 'DD-MM-RRRR') "
					+ " AND TO_DATE (CUST.CIF_CREATION_DATE, 'DD-MM-RRRR') = TO_DATE (SYSDATE, 'DD-MM-RRRR')) CUST"
					+ " JOIN (SELECT FIRST.CIF, MIN.MOD_NO, FIRST.FUNCTION_ID, FIRST.MAKER_ID FROM (SELECT SUBSTR(KEY_ID,16,9) AS CIF, "
					+ " MOD_NO, BRANCH_CODE, FUNCTION_ID, MAKER_ID FROM STTB_RECORD_LOG WHERE FUNCTION_ID = 'STDCIFQ9' UNION ALL"
					+ " SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO, BRANCH_CODE, FUNCTION_ID, MAKER_ID FROM STTB_RECORD_LOG_HIST WHERE FUNCTION_ID = 'STDCIFQ9') FIRST"
					+ " JOIN (SELECT CIF, MIN(MOD_NO) AS MOD_NO FROM (SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO FROM STTB_RECORD_LOG WHERE FUNCTION_ID = 'STDCIFQ9' "
					+ " UNION ALL SELECT SUBSTR(KEY_ID,16,9) AS CIF, MOD_NO FROM STTB_RECORD_LOG_HIST WHERE FUNCTION_ID = 'STDCIFQ9') GROUP BY CIF) MIN"
					+ " ON FIRST.CIF = MIN.CIF AND FIRST.MOD_NO = MIN.MOD_NO) FIRST ON CUST.CUSTOMER_NO = FIRST.CIF "
					+ " JOIN STTM_BRANCH BRN ON BRN.BRANCH_CODE = CUST.LOCAL_BRANCH";
			
					if ("all".equals(chkCab)){
						sql += " ORDER BY CUST.MAKER_DT_STAMP DESC";
					} else {
						sql +=" WHERE CUST.LOCAL_BRANCH = ? ORDER BY CUST.MAKER_DT_STAMP DESC ";
					}
			
			System.out.println("sql QuickCif :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if ("all".equals(chkCab)){
				
			} else {
				stat.setString (i++, branch.trim());
			}
			rs = stat.executeQuery();
			while (rs.next()){
				Quick = new quickCif();
				Quick.setCustomerNo(rs.getString("CUSTOMER_NO"));
				Quick.setCustomerName(rs.getString("CUSTOMER_NAME1"));
				Quick.setBranchCode(rs.getString("LOCAL_BRANCH"));
				Quick.setBranchName(rs.getString("BRANCH_NAME"));
				Quick.setFunctionId(rs.getString("FUNCTION_ID"));
				Quick.setCifCreationDate(rs.getString("CIF_CREATION_DATE"));
				Quick.setFirstMakerId(rs.getString("FIRST_MAKER_ID"));
				Quick.setLastMakerId(rs.getString("LAST_MAKER_ID"));
				Quick.setLastOtoId(rs.getString("LAST_OTO"));
				Quick.setLastUpdate(rs.getString("TANGGAL_LAST_UPDATE"));
				Quick.setStatus(rs.getString("STATUS"));
				lbb.add(Quick);
			}
		} catch (Exception e) {
			log.error(" getQuickCif : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getQuickCifTes (String branch, String chkCab, String StartDate, String EndDate){	
		List lbb = new ArrayList();
		quickCifPeriode Quick = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
				                  
			sql = " SELECT DISTINCT CUSTOMER_NO, CUSTOMER_NAME1, LOCAL_BRANCH, BRANCH_NAME, MOD_NO, FUNCTION_ID, JAM,"
					+ " MAKER_ID, TO_CHAR(CIF_CREATION_DATE,'DD-MM-RRRR') AS CIF_CREATION_DATE, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-'"
					+ " ELSE LAST_MAKER END AS MOD2_MAKER, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-'"
					+ " ELSE MAKER_DT_STAMP || ' ' || JAM END AS MOD2_MAKER_DT, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-'"
					+ " ELSE CHECKER_ID END AS MOD2_OTO, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-' ELSE CHECKER_DT_STAMP || ' ' || JAM1"
					+ " END AS MOD2_OTO_DT, CASE WHEN CHECKER_ID IS NULL THEN 'NO UPDATE' WHEN MOD_NO = '1' THEN 'NO UPDATE' ELSE 'UPDATE' END AS STATUS"
					+ " FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.LOCAL_BRANCH, CUST.BRANCH_NAME,CUST.CIF_CREATION_DATE,"
					+ " B.MOD_NO, A.FUNCTION_ID, C.MAKER_ID, A.MAKER_ID AS LAST_MAKER, TO_DATE (A.MAKER_DT_STAMP, 'DD/MM/RRRR') AS MAKER_DT_STAMP,"
					+ " TO_CHAR (A.MAKER_DT_STAMP, 'HH24:MI:SS') AS JAM, A.CHECKER_ID, TO_DATE (A.CHECKER_DT_STAMP, 'DD/MM/RRRR') AS CHECKER_DT_STAMP,"
					+ " TO_CHAR (A.CHECKER_DT_STAMP, 'HH24:MI:SS') AS JAM1 FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.LOCAL_BRANCH,"
					+ " BRN.BRANCH_NAME, CUST.MOD_NO, CUST.MAKER_ID, TO_DATE (CUST.CIF_CREATION_DATE, 'DD-MM-RRRR') AS CIF_CREATION_DATE"
					+ " FROM STTM_CUSTOMER CUST JOIN STTM_BRANCH BRN ON CUST.LOCAL_BRANCH = BRN.BRANCH_CODE) CUST JOIN (SELECT SUBSTR (KEY_ID, 16, 9) AS KEY_ID,"
					+ " MOD_NO, FUNCTION_ID, MAKER_ID, TO_DATE ( TO_CHAR (MAKER_DT_STAMP, 'DD/MM/RRRR HH24:MI:SS'), 'DD/MM/RRRR HH24:MI:SS')"
					+ " AS MAKER_DT_STAMP, CHECKER_ID, TO_DATE (TO_CHAR (CHECKER_DT_STAMP, 'DD/MM/RRRR HH24:MI:SS'), 'DD/MM/RRRR HH24:MI:SS')"
					+ " AS CHECKER_DT_STAMP FROM STTB_RECORD_LOG_HIST) A ON CUST.CUSTOMER_NO = A.KEY_ID JOIN (SELECT SUBSTR (KEY_ID, 16, 9) AS KEY_ID,"
					+ " MAX (MOD_NO) AS MOD_NO FROM STTB_RECORD_LOG_HIST WHERE MOD_NO IN ('1', '2') GROUP BY SUBSTR (KEY_ID, 16, 9)) B"
					+ " ON CUST.CUSTOMER_NO = B.KEY_ID AND A.MOD_NO = B.MOD_NO JOIN (SELECT SUBSTR (KEY_ID, 16, 9) AS KEY_ID, MOD_NO, FUNCTION_ID,"
					+ " MAKER_ID FROM STTB_RECORD_LOG_HIST WHERE FUNCTION_ID = 'STDCIFQ9') C"
					+ " ON CUST.CUSTOMER_NO = C.KEY_ID) ";    
			
					if ("all".equals(chkCab)){
						System.out.println("if");
						sql += " WHERE CIF_CREATION_DATE BETWEEN TO_DATE(?,'DD-MM-RRRR')"
								+ " AND TO_DATE (?,'DD-MM-RRRR') ORDER BY CIF_CREATION_DATE, MOD2_MAKER_DT, JAM DESC";
					} else {
						System.out.println("else");
						sql +=" WHERE CIF_CREATION_DATE BETWEEN TO_DATE(?,'DD-MM-RRRR') "
							+ " AND TO_DATE (?,'DD-MM-RRRR') AND LOCAL_BRANCH = ? ORDER BY CIF_CREATION_DATE, MOD2_MAKER_DT, JAM DESC";
					}
			System.out.println("sql tes:" + sql);		
			int i = 1;
			stat = conn.prepareStatement(sql);
			if ("all".equals(chkCab)){
				stat.setString(i++, StartDate);
				stat.setString(i++, EndDate);
				System.out.println("start -->" + StartDate + "end -->" + EndDate);
			} else {
				stat.setString(i++, StartDate);
				stat.setString(i++, EndDate);
				stat.setString (i++, branch.trim());
			}
			rs = stat.executeQuery();
			while (rs.next()){
				Quick = new quickCifPeriode();
				Quick.setCustomerNo(rs.getString("CUSTOMER_NO"));
				Quick.setCustomerName(rs.getString("CUSTOMER_NAME1"));
				Quick.setBranchCode(rs.getString("LOCAL_BRANCH"));
				Quick.setBranchName(rs.getString("BRANCH_NAME"));
				Quick.setmakerDate(rs.getString("MOD2_MAKER_DT"));
				Quick.setCifCreationDate(rs.getString("CIF_CREATION_DATE"));
				Quick.setFirstMakerId(rs.getString("MAKER_ID"));
				Quick.setLastMakerId(rs.getString("MOD2_MAKER"));
				Quick.setLastOtoId(rs.getString("MOD2_OTO"));
				Quick.setLastUpdate(rs.getString("MOD2_OTO_DT"));
				Quick.setStatus(rs.getString("STATUS"));
				lbb.add(Quick);
			}
		} catch (Exception e) {
			log.error(" getQuickCif : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getQuickPeriode (String branch, String chkCab, String StartDate, String EndDate){
		List lbb = new ArrayList();
		quickCifPeriode period = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
	
			sql = " SELECT DISTINCT CUSTOMER_NO, CUSTOMER_NAME1, LOCAL_BRANCH, BRANCH_NAME, MOD_NO, FUNCTION_ID,"
					+ " MAKER_ID, CIF_CREATION_DATE, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-'"
					+ " ELSE LAST_MAKER END AS MOD2_MAKER, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-'"
					+ " ELSE MAKER_DT_STAMP || ' ' || JAM END AS MOD2_MAKER_DT, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-'"
					+ " ELSE CHECKER_ID END AS MOD2_OTO, CASE WHEN MOD_NO = '1' AND FUNCTION_ID = 'STDCIFQ9' THEN '-' ELSE CHECKER_DT_STAMP || ' ' || JAM1"
					+ " END AS MOD2_OTO_DT, CASE WHEN CHECKER_ID IS NULL THEN 'NO UPDATE' WHEN MOD_NO = '1' THEN 'NO UPDATE' ELSE 'UPDATE' END AS STATUS"
					+ " FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.LOCAL_BRANCH, CUST.BRANCH_NAME,CUST.CIF_CREATION_DATE,"
					+ " B.MOD_NO, A.FUNCTION_ID, C.MAKER_ID, A.MAKER_ID AS LAST_MAKER, TO_DATE (A.MAKER_DT_STAMP, 'DD/MM/RRRR') AS MAKER_DT_STAMP,"
					+ " TO_CHAR (A.MAKER_DT_STAMP, 'HH24:MI:SS') AS JAM, A.CHECKER_ID, TO_DATE (A.CHECKER_DT_STAMP, 'DD/MM/RRRR') AS CHECKER_DT_STAMP,"
					+ " TO_CHAR (A.CHECKER_DT_STAMP, 'HH24:MI:SS') AS JAM1 FROM (SELECT CUST.CUSTOMER_NO, CUST.CUSTOMER_NAME1, CUST.LOCAL_BRANCH,"
					+ " BRN.BRANCH_NAME, CUST.MOD_NO, CUST.MAKER_ID, TO_DATE (CUST.CIF_CREATION_DATE, 'DD-MM-RRRR') AS CIF_CREATION_DATE"
					+ " FROM    STTM_CUSTOMER CUST JOIN STTM_BRANCH BRN ON CUST.LOCAL_BRANCH = BRN.BRANCH_CODE) CUST JOIN (SELECT SUBSTR (KEY_ID, 16, 9) AS KEY_ID,"
					+ " MOD_NO, FUNCTION_ID, MAKER_ID, TO_DATE ( TO_CHAR (MAKER_DT_STAMP, 'DD/MM/RRRR HH24:MI:SS'), 'DD/MM/RRRR HH24:MI:SS')"
					+ " AS MAKER_DT_STAMP, CHECKER_ID, TO_DATE (TO_CHAR (CHECKER_DT_STAMP, 'DD/MM/RRRR HH24:MI:SS'), 'DD/MM/RRRR HH24:MI:SS')"
					+ " AS CHECKER_DT_STAMP FROM STTB_RECORD_LOG_HIST) A ON CUST.CUSTOMER_NO = A.KEY_ID JOIN (SELECT SUBSTR (KEY_ID, 16, 9) AS KEY_ID,"
					+ " MAX (MOD_NO) AS MOD_NO FROM STTB_RECORD_LOG_HIST WHERE MOD_NO IN ('1', '2') GROUP BY SUBSTR (KEY_ID, 16, 9)) B"
					+ " ON CUST.CUSTOMER_NO = B.KEY_ID AND A.MOD_NO = B.MOD_NO JOIN (SELECT SUBSTR (KEY_ID, 16, 9) AS KEY_ID, MOD_NO, FUNCTION_ID,"
					+ " MAKER_ID FROM STTB_RECORD_LOG_HIST WHERE FUNCTION_ID = 'STDCIFQ9') C"
					+ " ON CUST.CUSTOMER_NO = C.KEY_ID) WHERE MAKER_DT_STAMP BETWEEN TO_DATE (?,'DD-MM-RRRR') AND TO_DATE (?,'DD-MM-RRRR') ";
			
					if ("all".equals(chkCab)){
						sql += " WHERE MAKER_DT_STAMP BETWEEN TO_DATE(?,'DD-MM-RRRR')"
								+ " AND TO_DATE (?,'DD-MM-RRRR') ORDER BY MAKER_DT_STAMP";
					} else {
						sql +=" WHERE MAKER_DT_STAMP BETWEEN TO_DATE(?,'DD-MM-RRRR') "
							+ " AND TO_DATE (?,'DD-MM-RRRR') AND LOCAL_BRANCH = ? ORDER BY MAKER_DT_STAMP ";
					}
			
			System.out.println("sql QuickCif :" + sql);
			System.out.println("StartDate :" + StartDate + "EndDate :" + EndDate + "branch:" + branch);
			int i = 1;
			stat = conn.prepareStatement(sql);
			if ("all".equals(chkCab)){
				stat.setString(i++, StartDate);
				stat.setString(i++, EndDate);
			} else {
				stat.setString(i++, StartDate);
				stat.setString(i++, EndDate);
				stat.setString (i++, branch.trim());
			}
			rs = stat.executeQuery();
			while (rs.next()){
				period = new quickCifPeriode();
				period.setCustomerNo(rs.getString("CUSTOMER_NO"));
				period.setCustomerName(rs.getString("CUSTOMER_NAME1"));
				period.setBranchCode(rs.getString("LOCAL_BRANCH"));
				period.setBranchName(rs.getString("BRANCH_NAME"));
//				merge = new merge();
//				merge.setRefNo(rs.getString("REF_NO"));
//				merge.setCustNo(rs.getString("CUST_NO"));
//				merge.setCustName(rs.getString("CUST_NAME"));
//				merge.setBranchCode(rs.getInt("BRANCH_CODE"));
//				merge.setBranchName(rs.getString("BRANCH_NAME"));
//				merge.setMergeMakerDt(rs.getString("MERGE_MAKER_DT"));
//				merge.setMergeMaker(rs.getString("MERGE_MAKER_ID"));
//				merge.setMergeChecker(rs.getString("MERGE_CHECKER"));
//				merge.setCloseMaker(rs.getString("CLOSE_MAKER_ID"));
//				merge.setCloseChecker(rs.getString("CLOSE_CHECKER"));
//				merge.setLastMakerDt(rs.getString("LAST_MAKER_DT"));
//				merge.setStatus(rs.getString("STATUS"));
				lbb.add(period);
			}
		} catch (Exception e) {
			log.error(" getQuickCif : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getMergeCif (String branch, String chkBranch){
		System.out.println("branch :" + branch + "check:" + chkBranch);
		List lbb = new ArrayList();
		merge merge = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " SELECT CHG.REF_NO, CHG.CUST_NO, CHG.CUST_NAME, CHG.BRANCH_CODE, BRN.BRANCH_NAME,"
					+ " TO_CHAR (MAKER_DT, 'DD/MM/RRRR') AS MERGE_MAKER_DT, CHG.MAKER_ID AS MERGE_MAKER_ID,"
					+ " CASE WHEN CHG.CHECKER_ID IS NULL THEN '-' ELSE CHG.CHECKER_ID END AS MERGE_CHECKER,"
					+ " CASE WHEN CUST.RECORD_STAT = 'C' THEN CUST.MAKER_ID ELSE '-' END AS CLOSE_MAKER_ID,"
					+ " CASE WHEN CUST.RECORD_STAT = 'C' THEN CUST.CHECKER_ID END AS CLOSE_CHECKER, CUST.AUTH_STAT,"
					+ " TO_CHAR (CUST.MAKER_DT_STAMP, 'DD/MM/RRRR') AS LAST_MAKER_DT,"
					+ " CASE WHEN CUST.RECORD_STAT = 'C' AND CUST.AUTH_STAT = 'U' THEN 'OPEN'"
					+ " WHEN CUST.RECORD_STAT = 'C' AND CUST.AUTH_STAT = 'A' THEN 'CLOSE' ELSE 'OPEN' END AS STATUS,"
					+ " CUST.RECORD_STAT FROM STTM_CUSTOMER CUST"
					+ " JOIN (SELECT TRANSFER_REFERENCE_NO AS REF_NO, OLD_CUSTOMER_NO AS CUST_NO,"
					+ " OLD_CUSTOMER_DESC AS CUST_NAME, OLD_BRANCH AS BRANCH_CODE, MAKER_ID, CHECKER_ID,"
					+ " TO_DATE (MAKER_DT_STAMP, 'DD-MM-RRRR') AS MAKER_DT, AUTH_STAT"
					+ " FROM FCC114.STTM_CUST_CHG_OTH_MASTER"
					+ " WHERE TO_CHAR(MAKER_DT_STAMP, 'DD-MM-RRRR') = TO_CHAR(SYSDATE,'DD-MM-RRRR')) CHG"
					+ " ON CUST.CUSTOMER_NO = CHG.CUST_NO JOIN STTM_BRANCH BRN ON CHG.BRANCH_CODE = BRN.BRANCH_CODE";
//			sql = " SELECT REF_NO,CUST_NO, CUST_NAME, BRANCH_CODE, BRANCH_NAME, TO_CHAR(MERGE_MAKER_DT,'DD-MM-RRRR') AS MERGE_MAKER_DT,"
//					+ " MERGE_MAKER_ID, MERGE_CHECKER, CLOSE_MAKER_ID, NVL(CLOSE_CHECKER,'-') CLOSE_CHECKER,"
//					+ " TO_CHAR(LAST_MAKER_DT,'DD-MM-RRRR') AS LAST_MAKER_DT, STATUS"
//					+ " FROM (SELECT CHG.REF_NO, CHG.CUST_NO, CHG.CUST_NAME, CHG.BRANCH_CODE, BRN.BRANCH_NAME,"
//					+ " TO_DATE(CHG.MAKER_DT,'DD-MM-RRRR') AS MERGE_MAKER_DT, CHG.MAKER_ID AS MERGE_MAKER_ID,"
//					+ " CASE WHEN CHG.CHECKER_ID IS NULL THEN '-' ELSE CHG.CHECKER_ID END AS MERGE_CHECKER,"
//					+ " CASE WHEN CUST.RECORD_STAT = 'C' THEN CUST.MAKER_ID ELSE '-' END AS CLOSE_MAKER_ID, "
//					+ " CASE WHEN CUST.RECORD_STAT = 'C' THEN CUST.CHECKER_ID END AS CLOSE_CHECKER,"
//					+ " CUST.AUTH_STAT, TO_DATE(CUST.MAKER_DT_STAMP,'DD-MM-RRRR') AS LAST_MAKER_DT,"
//					+ " CASE WHEN CUST.RECORD_STAT = 'C' AND CUST.AUTH_STAT = 'U' THEN 'OPEN'"
//					+ " WHEN CUST.RECORD_STAT = 'C' AND CUST.AUTH_STAT = 'A' THEN 'CLOSE' ELSE 'OPEN' "
//					+ " END AS STATUS, CUST.RECORD_STAT FROM STTM_CUSTOMER CUST"
//					+ " JOIN (SELECT TRANSFER_REFERENCE_NO AS REF_NO, OLD_CUSTOMER_NO AS CUST_NO, OLD_CUSTOMER_DESC AS CUST_NAME,"
//					+ " OLD_BRANCH AS BRANCH_CODE, MAKER_ID, CHECKER_ID, TO_DATE(MAKER_DT_STAMP,'DD/MM/RRRR') AS MAKER_DT,"
//					+ " AUTH_STAT FROM FCC114.STTM_CUST_CHG_OTH_MASTER) CHG"
//					+ " ON CUST.CUSTOMER_NO = CHG.CUST_NO JOIN STTM_BRANCH BRN ON CHG.BRANCH_CODE = BRN.BRANCH_CODE"
//					+ " WHERE TO_CHAR(CHG.MAKER_DT,'DD-MM-RRRR') = '05-10-2017'"
//					+ " ORDER BY TO_DATE(CHG.MAKER_DT,'DD-MM-RRRR'))";
			
				if ("all".equals(chkBranch)){
					sql += " ORDER BY TO_DATE (CHG.MAKER_DT, 'DD-MM-RRRR')";
				} else {
					sql += " WHERE CHG.BRANCH_CODE = ? ORDER BY TO_DATE (CHG.MAKER_DT, 'DD-MM-RRRR')";
				}
				System.out.println("Sql Merge -->" + sql);
				int i = 1;
				stat = conn.prepareStatement(sql);
				if ("all".equals(chkBranch)){
					
				} else {
					stat.setString (i++, branch.trim());
				}
				rs = stat.executeQuery();
				while (rs.next()){
					merge = new merge();
					merge.setRefNo(rs.getString("REF_NO"));
					merge.setCustNo(rs.getString("CUST_NO"));
					merge.setCustName(rs.getString("CUST_NAME"));
					merge.setBranchCode(rs.getInt("BRANCH_CODE"));
					merge.setBranchName(rs.getString("BRANCH_NAME"));
					merge.setMergeMakerDt(rs.getString("MERGE_MAKER_DT"));
					merge.setMergeMaker(rs.getString("MERGE_MAKER_ID"));
					merge.setMergeChecker(rs.getString("MERGE_CHECKER"));
					merge.setCloseMaker(rs.getString("CLOSE_MAKER_ID"));
					merge.setCloseChecker(rs.getString("CLOSE_CHECKER"));
					merge.setLastMakerDt(rs.getString("LAST_MAKER_DT"));
					merge.setStatus(rs.getString("STATUS"));
					lbb.add(merge);
				}
				
		} catch (Exception e) {
			log.error(" getMergeCif : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}	
	
	public List getPendingTransaksi (String brn, String area){
		System.out.println("branch " + brn + "area :" + area);
		List lbb = new ArrayList();
		pendingTransaksi pending = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			
			if ("303".equals(brn)){
				System.out.println("kcu 303");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER) USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('303','336','388','408','386','389','391','269','463')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("308".equals(brn)) {
				System.out.println("kcu 308");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('308','418','492') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("310".equals(brn)) {
				System.out.println("kcu 310");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('310','315') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("312".equals(brn)) {
				System.out.println("kcu 308");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('311','312','440') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("313".equals(brn)) {
				System.out.println("kcu 313");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '313' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("316".equals(brn)) {
				System.out.println("kcu 316");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '316' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("319".equals(brn)) {
				System.out.println("kcu 319");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '319' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("325".equals(brn)) {
				System.out.println("kcu 325");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('325','385','266') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("326".equals(brn)) {
				System.out.println("kcu 326");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '326' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("327".equals(brn)) {
				System.out.println("kcu 327");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '327' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("330".equals(brn)) {
				System.out.println("kcu 330");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '330' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("333".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('333','493') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("301".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '301' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("302".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('302','374') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("304".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('304','332','350','390','428') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("307".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('307','331','339','342','380') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("314".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '314' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("324".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('324','375','384') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("328".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '328' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("340".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '340' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("121".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('121','123','124','125','126') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("323".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('323','454','455','345','378') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("322".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('322','382') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("305".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('305','334','335','346','347','369','458','348','367') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("309".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('309','306','358') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("317".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '317' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("320".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('320','435') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("321".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '321' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("318".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('318','360','394') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("329".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('329','342') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("341".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('341','203') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("101".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JABAR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('101','102','103','104','105','106','108','111','117','118','120','146','112','113','114','119','147','130')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("131".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JABAR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('131','132','134','135','136','137','139','264') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("141".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JABAR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('141','143','144') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("151".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JABAR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('151','154','152','155','156') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("161".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JABAR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('161','162','164','273') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("501".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('501','502','504','508','509','548','549','475') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("506".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('506','507') AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("511".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('511','514','515','516','562') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("512".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '512' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("521".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('521','522','523','524','526','527','530') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("531".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('531','531','532','567','533','534','535','537','538','539','208','561') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("541".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('541','542','544','546','563') "
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("701".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '701' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("702".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '702' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("711".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '711' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("721".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '721' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("731".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '731' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("741".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '741' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("751".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '751' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("761".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '761' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("771".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '771' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("601".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KALIMANTAN' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '601' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("602".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KALIMANTAN' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '602' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("611".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KALIMANTAN' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '611' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("621".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KALIMANTAN' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '621' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("631".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KALIMANTAN' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '631' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("841".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '841' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("851".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '851' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("871".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '871' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("881".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '881' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("801".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '801' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("831".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '831' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("811".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '811' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("821".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '821' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("891".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '891' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("861".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '861' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("351".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGSEL' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '351' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("361".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGSEL' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '361' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("371".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGSEL' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '371' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("431".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGSEL' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '431' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("421".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGSEL' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '421' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("441".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGSEL' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '441' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("211".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '211' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("231".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '231' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("241".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '241' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("251".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '251' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("261".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '261' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("482".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '482' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("411".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '411' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("211".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '211' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("451".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '451' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("901".equals(brn)) {
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KUALA LUMPUR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '901' AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("JAKARTA 1".equals(area)){
				System.out.println("area jakarta 1");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 1' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('303','336','388','408','386','389','391','269','463','312','311','440',"
						+ " '310','315','313','316','319','326','327','325','385','266','330','308','418','492','333','493')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("JAKARTA 2".equals(area)) {
				System.out.println("area jakarta 2");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 2' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('301','302','374','304','332','390','350','428','307','331',"
						+ " '339','342','380','314','324','375','384','328','340','121','123','124','125','126')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("JAKARTA 3".equals(area)) {
				System.out.println("area jakarta 3");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JAKARTA 3' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('323','454','455','345','378','322','382','305','334','335',"
						+ " '346','347','369','458','348','367','309','306','358','317','320','435','321',"
						+ " '318','360','394','329','349','341','203')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("JABAR".equals(area)) {
				System.out.println("area jabar");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JABAR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('101','102','103','104','105','106','108','111','117','118',"
						+ " '120','146','112','113','114','119','147','130','131','132','134','137','139',"
						+ " '135','136','264','141','143','144','151','154','152','155','156','161','162',"
						+ " '164','273')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("JATENG & DIY".equals(area)) {
				System.out.println("area jateng");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATENG DAN DIY' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('501','502','504','508','509','548','549','475','506','507','511',"
						+ " '514','515','516','562','512','521','522','523','524','526','527','530','531','532',"
						+ " '567','533','534','535','537','538','539','208','561','541','542','544','546','563')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("JATIMBALNUSRA".equals(area)) {
				System.out.println("area JATIMBALNUSRA");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'JATIMBALNUSRA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('701','702','711','721','731','741','751','761','771')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("KALIMANTAN".equals(area)) {
				System.out.println("area kalimantan");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KALIMANTAN' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('601','602','611','621','631')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("SULAMPUA".equals(area)) {
				System.out.println("area SULAMPUA");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SULAMPUA' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('841','851','871','881','801','811','821','831','861','891')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("SUMBAGSEL".equals(area)) {
				System.out.println("area SUMBAGSEL");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGSEL' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('351','361','371','431','421','441')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else if ("SUMBAGUT".equals(area)) {
				System.out.println("area SUMBAGUT");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'SUMBAGUT' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR IN ('211','231','241','251','261','482','221','411','451')"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			} else {
				System.out.println("area KL");
				sql = " SELECT DISTINCT BRN.BRANCH_CODE, BRN.BRANCH_NAME, 'KUALA LUMPUR' AS REGIONAL, VW.MD AS MODULE,"
						+ " VW.RN AS REFERENCE_NO, VW.ID AS USER_ID, USR.USER_NAME"
						+ " FROM STTM_BRANCH BRN JOIN EIVW_PENDING_ITEMS VW ON BRN.BRANCH_CODE = VW.BR"
						+ " JOIN (SELECT USER_ID, USER_NAME FROM SMTB_USER)USR ON VW.ID = USR.USER_ID"
						+ " WHERE VW.BR = '901"
						+ " AND VW.ID NOT IN ('SYSTEM','FLEXGW','FLEXCUBE') ORDER BY BRN.BRANCH_CODE";
			}
				System.out.println("SQL getBranch:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()){
				pending = new pendingTransaksi();
				pending.setBranch(rs.getString("BRANCH_CODE"));
				pending.setBranchName(rs.getString("BRANCH_NAME"));
				pending.setArea(rs.getString("REGIONAL"));
				pending.setModule(rs.getString("MODULE"));
				pending.setRefNo(rs.getString("REFERENCE_NO"));
				pending.setUserId(rs.getString("USER_ID"));
				pending.setUserName(rs.getString("USER_NAME"));
				lbb.add(pending);
			}
		} catch (Exception e) {
			log.error(" getBranch : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public List getPrks (String acc){
		List lis = new ArrayList();
		prks entity = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = "SELECT ICTM.ACC AS ACC_PRK, CUST.BRANCH_CODE, CUST.AC_DESC, CUST.ACCOUNT_CLASS,"
					+ " TO_CHAR(SER.LINE_START_DATE,'DD/MM/RRRR') AS LINE_START_DATE, "
					+ " TO_CHAR(SER.LINE_EXPIRY_DATE,'DD/MM/RRRR') AS LINE_EXPIRY_DATE,"
					+ " SER.LIMIT_AMOUNT AS PLAFOND FROM ICTM_ACC ICTM"
					+ " LEFT JOIN STTM_CUST_ACCOUNT CUST ON ICTM.ACC = CUST.CUST_AC_NO LEFT JOIN (SELECT SERIAL.BRANCH_CODE,"
					+ " SERIAL.CUST_AC_NO, SERIAL.CUSTOMER_NO, SERIAL.LINKED_REF_NO, SUBSTR (SERIAL.LINKED_REF_NO, 10, 10) AS LINE_SERIAL,"
					+ " AMOUNT.LINE_START_DATE, AMOUNT.LINE_EXPIRY_DATE, AMOUNT.LIMIT_AMOUNT FROM STTM_CUST_ACCOUNT_LINKAGES SERIAL"
					+ " JOIN GETM_FACILITY AMOUNT ON SERIAL.CUSTOMER_NO = AMOUNT.LINE_CODE "
					+ " AND SUBSTR (SERIAL.LINKED_REF_NO, 10, 10) = AMOUNT.LINE_SERIAL) SER ON ICTM.ACC = SER.CUST_AC_NO WHERE ICTM.ACC = ?";
			
				System.out.println("sql prks:" + sql);
				stat = conn.prepareStatement(sql);
				stat.setString(1, acc);
				rs = stat.executeQuery();
				while(rs.next()){
					entity = new prks();
					entity.setAcc(rs.getString("ACC_PRK"));
					entity.setBrn(rs.getString("BRANCH_CODE"));
					entity.setCustName(rs.getString("AC_DESC"));
					entity.setAccClass(rs.getString("ACCOUNT_CLASS") == null ? "-" : rs.getString("ACCOUNT_CLASS"));
					entity.setStartDt(rs.getString("LINE_START_DATE") == null ? "-" : rs.getString("LINE_START_DATE"));
					entity.setExpiryDt(rs.getString("LINE_EXPIRY_DATE") == null ? "-" : rs.getString("LINE_EXPIRY_DATE"));
					entity.setPlafond(rs.getBigDecimal("PLAFOND"));
					lis.add(entity);
				}
		} catch (Exception e) {
			log.error(" getPrks : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lis;
	}
	
	public List getDetailPRKS (String acc, String prd1, String prd2){
		System.out.println("prd1:" + prd1 + "prd2:" + prd2);
		List lbb = new ArrayList();
		prks detail = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String sql1 = "";
		String accBaghas = "";
		String brn = "";
		String accClass = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql =" SELECT CUST.CUST_AC_NO, ICC.BOOK_ACC, CUST.BRANCH_CODE, CUST.ACCOUNT_CLASS  "
					+ " FROM STTM_CUST_ACCOUNT CUST LEFT JOIN ICTM_ACC ICC ON CUST.CUST_AC_NO = ICC.ACC"
					+ " WHERE CUST_AC_NO = ? ";
			stat = conn.prepareStatement(sql);
			stat.setString(1, acc);
			rs = stat.executeQuery();
			if (rs.next()){
				detail = new prks();
				detail.setAccBaghas(rs.getString("BOOK_ACC"));
				detail.setBrn(rs.getString("BRANCH_CODE"));
				detail.setAccClass(rs.getString("ACCOUNT_CLASS"));
			}
			
			accBaghas = detail.getAccBaghas();
			brn = detail.getBrn();
			accClass = detail.getAccClass();
			System.out.println("branch:" + brn + "acc bagHas:" + accBaghas);
			
			if ("PRK3".equals(rs.getString("ACCOUNT_CLASS"))){
				System.out.println("PRK3");
				sql1 = " SELECT PBH.ACC, PBH.BOOK_ACC, TO_CHAR (PBH.ENT_DT, 'DD/MM/RRRR') AS PERIODE, PBH.BULAN_TAGIHAN,"
						+ " NVL(RBH.BULAN_BAYAR, '-') AS BULAN_BAYAR, NVL (SALDO.SALDO, '0') AS SALDO_RATA_RATA,"
						+ " NVL(RBH.RBH, '0') AS RBH, PBH.PBH, CASE WHEN RBH.RBH IS NOT NULL THEN 'PAID' ELSE 'UNPAID' END AS STATUS"
						+ " FROM (SELECT ICTM.ACC, ICTM.BOOK_ACC, PBH.ENT_DT, PBH.BULAN_TAGIHAN, PBH.PBH FROM ICTM_ACC ICTM"
						+ " LEFT JOIN (SELECT * FROM (SELECT ACC, BRN, PROD, ENT_DT, TO_CHAR (ADD_MONTHS (ENT_DT, 1), 'MM/RRRR') AS BULAN_TAGIHAN,"
						+ " AMT AS PBH, CASE WHEN AMT = 0 THEN NULL ELSE AMT END AS FLAG FROM ICTB_ENTRIES_HISTORY "
						+ " WHERE ACC = ? AND BRN = ?) WHERE FLAG IS NOT NULL ORDER BY ENT_DT) PBH ON ICTM.ACC = PBH.ACC) PBH"
						+ " LEFT JOIN (SELECT ICTM.ACC, ICTM.BOOK_ACC, TO_CHAR (RBH.TRN_DT, 'MM/RRRR') AS BULAN_BAYAR,"
						+ " RBH.LCY_AMOUNT AS RBH FROM ICTM_ACC ICTM LEFT JOIN (SELECT AC_NO, RELATED_ACCOUNT, TRN_DT, LCY_AMOUNT"
						+ " FROM ACVW_ALL_AC_ENTRIES WHERE AC_NO = ? AND EVENT = 'INIT' AND TRN_CODE IN ('O15', 'N09','O82') AND DRCR_IND = 'C')"
						+ " RBH ON ICTM.BOOK_ACC = RBH.AC_NO WHERE ICTM.ACC = ?) RBH ON PBH.ACC = RBH.ACC AND PBH.BULAN_TAGIHAN = RBH.BULAN_BAYAR"
						+ " LEFT JOIN (SELECT DISTINCT STMT.ACC,TO_CHAR(STMT.D$,'DD/MM/RRRR') AS TANGGAL, TO_CHAR(STMT.D$,'MM/RRRR') AS BULAN, "
						+ " REGEXP_REPLACE(STMT.COND_TS, '[^0-9]', '') AS SALDO FROM ICTB_ICALC_STMT STMT JOIN (SELECT TANGGAL, BULAN, ACC, BRN FROM ("
						+ " SELECT MAX(D$) AS TANGGAL, TO_CHAR(D$,'MM/RRRR') AS BULAN, ACC, BRN FROM ICTB_ICALC_STMT WHERE PROD='PRK3' AND FRM_NO = '3' "
						+ " AND ACC = ? AND BRN = ? GROUP BY TO_CHAR(D$,'MM/RRRR'), ACC, BRN)) TGL ON STMT.ACC = TGL.ACC "
						+ " AND TO_CHAR(STMT.D$,'DD/MM/RRRR') = TO_CHAR(TGL.TANGGAL,'DD/MM/RRRR') WHERE STMT.ACC = ? AND STMT.BRN  = ? AND STMT.FRM_NO = '3') SALDO "
						+ " ON PBH.ACC = SALDO.ACC AND TO_CHAR(PBH.ENT_DT,'MM/RRRR') = TO_CHAR(SALDO.BULAN) WHERE PBH.ACC = ? "
						+ " AND PBH.ENT_DT BETWEEN TO_DATE (?, 'DD/MM/RRRR') AND TO_DATE (?, 'DD/MM/RRRR') ORDER BY ENT_DT";
			} else {
				System.out.println("PRK7");
				/*sql1 = " SELECT PBH.ACC, PBH.BOOK_ACC, TO_CHAR (PBH.ENT_DT, 'DD/MM/RRRR') AS PERIODE, PBH.BULAN_TAGIHAN, "
						+ " NVL (RBH.BULAN_BAYAR,'-') AS BULAN_BAYAR, NVL (SALDO.SALDO,'0') AS SALDO_RATA_RATA, NVL (RBH.RBH,'0') AS RBH, "
						+ " PBH.PBH, CASE WHEN RBH.RBH IS NOT NULL THEN 'PAID' ELSE 'UNPAID' END AS STATUS "
						+ " FROM (SELECT ICTM.ACC, ICTM.BOOK_ACC, PBH.ENT_DT, PBH.BULAN_TAGIHAN, PBH.PBH FROM ICTM_ACC ICTM"
						+ " LEFT JOIN (SELECT * FROM (SELECT ACC, BRN, PROD, ENT_DT, TO_CHAR (ADD_MONTHS (ENT_DT, 1), 'MM/RRRR') AS BULAN_TAGIHAN,"
						+ " AMT AS PBH, CASE WHEN AMT = 0 THEN NULL ELSE AMT END AS FLAG FROM ICTB_ENTRIES_HISTORY WHERE ACC = ? AND BRN = ?)"
						+ " WHERE FLAG IS NOT NULL ORDER BY ENT_DT) PBH ON ICTM.ACC = PBH.ACC WHERE ICTM.ACC = ? ) PBH LEFT JOIN (SELECT ICTM.ACC,"
						+ " ICTM.BOOK_ACC, TO_CHAR (RBH.TRN_DT, 'MM/RRRR') AS BULAN_BAYAR, RBH.LCY_AMOUNT AS RBH FROM ICTM_ACC ICTM"
						+ " LEFT JOIN (SELECT AC_NO, RELATED_ACCOUNT, TRN_DT, LCY_AMOUNT FROM ACVW_ALL_AC_ENTRIES WHERE AC_NO = ? AND EVENT = 'INIT'"
						+ " AND TRN_CODE IN ('O82') AND DRCR_IND = 'D') RBH ON ICTM.BOOK_ACC = RBH.AC_NO WHERE ICTM.ACC = ?) RBH"
						+ " ON PBH.ACC = RBH.ACC AND PBH.BULAN_TAGIHAN = RBH.BULAN_BAYAR LEFT JOIN (SELECT DISTINCT STMT.ACC, "
						+ " TO_CHAR(STMT.D$,'DD/MM/RRRR') AS TANGGAL, TO_CHAR(STMT.D$,'MM/RRRR') AS BULAN, REGEXP_REPLACE(STMT.COND_TS, '[^0-9]','') AS SALDO"
						+ " FROM ICTB_ICALC_STMT STMT JOIN (SELECT TANGGAL, BULAN, ACC, BRN FROM (SELECT MAX(D$) AS TANGGAL, TO_CHAR(D$,'MM/RRRR') AS BULAN, "
						+ " ACC, BRN FROM ICTB_ICALC_STMT WHERE PROD = 'PRK7' AND FRM_NO = '1' AND ACC = ? AND BRN = ? "
						+ " GROUP BY TO_CHAR(D$,'MM/RRRR'), ACC, BRN)) TGL ON STMT.ACC = TGL.ACC "
						+ " AND TO_CHAR(STMT.D$,'DD/MM/RRRR') = TO_CHAR(TGL.TANGGAL,'DD/MM/RRRR') WHERE STMT.ACC = ? AND STMT.BRN = ? AND STMT.FRM_NO = '1') SALDO"
						+ " ON PBH.ACC = SALDO.ACC AND TO_CHAR(PBH.ENT_DT,'MM/RRRR') = TO_CHAR(SALDO.BULAN) WHERE PBH.ACC = ?"
						+ " AND PBH.ENT_DT BETWEEN TO_DATE (?, 'DD/MM/RRRR') AND TO_DATE (?, 'DD/MM/RRRR') ORDER BY ENT_DT";
			*/
				sql1 = " SELECT PBH.ACC, PBH.BOOK_ACC, TO_CHAR (PBH.ENT_DT, 'DD/MM/RRRR') AS PERIODE, PBH.BULAN_TAGIHAN, "
						+ " NVL (RBH.BULAN_BAYAR,'-') AS BULAN_BAYAR, NVL (SALDO.SALDO,'0') AS SALDO_RATA_RATA, NVL (RBH.RBH,'0') AS RBH, "
						+ " PBH.PBH, CASE WHEN RBH.RBH IS NOT NULL THEN 'PAID' ELSE 'UNPAID' END AS STATUS "
						+ " FROM (SELECT ICTM.ACC, ICTM.BOOK_ACC, PBH.ENT_DT, PBH.BULAN_TAGIHAN, PBH.PBH FROM ICTM_ACC ICTM"
						+ " LEFT JOIN (SELECT * FROM (SELECT ACC, BRN, PROD, ENT_DT, TO_CHAR (ADD_MONTHS (ENT_DT, 1), 'MM/RRRR') AS BULAN_TAGIHAN,"
						+ " AMT AS PBH, CASE WHEN AMT = 0 THEN NULL ELSE AMT END AS FLAG FROM ICTB_ENTRIES_HISTORY WHERE ACC = ? AND BRN = ?)"
						+ " WHERE FLAG IS NOT NULL ORDER BY ENT_DT) PBH ON ICTM.ACC = PBH.ACC WHERE ICTM.ACC = ? ) PBH LEFT JOIN (SELECT ICTM.ACC,"
						+ " ICTM.BOOK_ACC, TO_CHAR (RBH.TRN_DT, 'MM/RRRR') AS BULAN_BAYAR, RBH.LCY_AMOUNT AS RBH FROM ICTM_ACC ICTM"
						+ " LEFT JOIN (SELECT AC_NO, RELATED_ACCOUNT, TRN_DT, LCY_AMOUNT FROM ACVW_ALL_AC_ENTRIES WHERE AC_NO = ? AND EVENT IN ('ILIQ','INIT')"
						+ " AND TRN_CODE IN ('O82') AND DRCR_IND = 'D') RBH ON ICTM.BOOK_ACC = RBH.AC_NO WHERE ICTM.ACC = ?) RBH"
						+ " ON PBH.ACC = RBH.ACC AND PBH.BULAN_TAGIHAN = RBH.BULAN_BAYAR LEFT JOIN (SELECT DISTINCT STMT.ACC, "
						+ " TO_CHAR(STMT.D$,'DD/MM/RRRR') AS TANGGAL, TO_CHAR(STMT.D$,'MM/RRRR') AS BULAN, REGEXP_REPLACE(STMT.COND_TS, '[^0-9]','') AS SALDO"
						+ " FROM ICTB_ICALC_STMT STMT JOIN (SELECT TANGGAL, BULAN, ACC, BRN FROM (SELECT MAX(D$) AS TANGGAL, TO_CHAR(D$,'MM/RRRR') AS BULAN, "
						+ " ACC, BRN FROM ICTB_ICALC_STMT WHERE PROD = 'PRK7' AND FRM_NO = '1' AND ACC = ? AND BRN = ? "
						+ " GROUP BY TO_CHAR(D$,'MM/RRRR'), ACC, BRN)) TGL ON STMT.ACC = TGL.ACC "
						+ " AND TO_CHAR(STMT.D$,'DD/MM/RRRR') = TO_CHAR(TGL.TANGGAL,'DD/MM/RRRR') WHERE STMT.ACC = ? AND STMT.BRN = ? AND STMT.FRM_NO = '1') SALDO"
						+ " ON PBH.ACC = SALDO.ACC AND TO_CHAR(PBH.ENT_DT,'MM/RRRR') = TO_CHAR(SALDO.BULAN) WHERE PBH.ACC = ?"
						+ " AND PBH.ENT_DT BETWEEN TO_DATE (?, 'DD/MM/RRRR') AND TO_DATE (?, 'DD/MM/RRRR') ORDER BY ENT_DT";
				
			}
			
			System.out.println("sql prks:" + sql1);
			int i = 1;
			stat = conn.prepareStatement(sql1);
			if ("PRK3".equals(rs.getString("ACCOUNT_CLASS"))){
				stat.setString(i++, acc);
				stat.setString(i++, brn);
				stat.setString(i++, accBaghas);
				stat.setString(i++, acc);
				stat.setString(i++, acc);
				stat.setString(i++, brn);
				stat.setString(i++, acc);
				stat.setString(i++, brn);
				stat.setString(i++, acc);
				stat.setString(i++, prd1);
				stat.setString(i++, prd2);
			}else {
				stat.setString(i++, acc);
				stat.setString(i++, brn);
				stat.setString(i++, acc);
				stat.setString(i++, accBaghas);
				stat.setString(i++, acc);
				stat.setString(i++, acc);
				stat.setString(i++, brn);
				stat.setString(i++, acc);
				stat.setString(i++, brn);
				stat.setString(i++, acc);
				stat.setString(i++, prd1);
				stat.setString(i++, prd2);
			}
			rs = stat.executeQuery();
			while (rs.next()){
				detail = new prks();
				detail.setPeriode(rs.getString("PERIODE"));
				detail.setBulanBayar(rs.getString("BULAN_BAYAR"));
				detail.setSaldoRata(rs.getBigDecimal("SALDO_RATA_RATA"));
				detail.setBulanTagihan(rs.getString("BULAN_TAGIHAN"));
				detail.setPbh(rs.getBigDecimal("PBH"));
				detail.setRbh(rs.getBigDecimal("RBH"));
				detail.setStatus(rs.getString("STATUS"));
				lbb.add(detail);
			//	System.out.println("detail:" + detail);
			}
		} catch (Exception e) {
			log.error(" getDetailPRKS : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public prks getHeadPrks (String acc) {
		prks head = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			/*asli
			sql = "SELECT ICTM.ACC AS ACC_PRK, ICTM.BOOK_ACC AS ACC_BAGHAS, CUST.AC_DESC, CUST.ACCOUNT_CLASS, "
					+ " TO_CHAR(SER.LINE_START_DATE,'DD/MM/RRRR') AS LINE_START_DATE, "
					+ " TO_CHAR(SER.LINE_EXPIRY_DATE,'DD/MM/RRRR') AS LINE_EXPIRY_DATE,"
					+ " SER.LIMIT_AMOUNT AS PLAFOND, CUST.LCY_CURR_BALANCE AS OS,"
					+ " CASE WHEN SUBSTR (CUST.LCY_CURR_BALANCE, 1, 1) = '-' THEN NVL(SER.LIMIT_AMOUNT + CUST.LCY_CURR_BALANCE, '0')"
					+ " ELSE SER.LIMIT_AMOUNT END AS FCU, UDE.UDE_VALUE AS RATE FROM ICTM_ACC ICTM LEFT JOIN STTM_CUST_ACCOUNT CUST"
					+ " ON ICTM.ACC = CUST.CUST_AC_NO LEFT JOIN ("
					+ ") UDE"
					+ " ON ICTM.ACC = UDE.ACC LEFT JOIN (SELECT SERIAL.BRANCH_CODE, SERIAL.CUST_AC_NO, SERIAL.CUSTOMER_NO, SERIAL.LINKED_REF_NO,"
					+ " SUBSTR (SERIAL.LINKED_REF_NO, 10, 10) AS LINE_SERIAL, AMOUNT.LINE_START_DATE, AMOUNT.LINE_EXPIRY_DATE, AMOUNT.LIMIT_AMOUNT"
					+ " FROM STTM_CUST_ACCOUNT_LINKAGES SERIAL JOIN GETM_FACILITY AMOUNT ON SERIAL.CUSTOMER_NO = AMOUNT.LINE_CODE"
					+ " AND SUBSTR (SERIAL.LINKED_REF_NO, 10, 10) = AMOUNT.LINE_SERIAL) SER ON ICTM.ACC = SER.CUST_AC_NO WHERE ICTM.ACC = ?";
			*/
			
			sql = "SELECT ICTM.ACC AS ACC_PRK, ICTM.BOOK_ACC AS ACC_BAGHAS, CUST.AC_DESC, CUST.ACCOUNT_CLASS, "
					+ " TO_CHAR(SER.LINE_START_DATE,'DD/MM/RRRR') AS LINE_START_DATE, "
					+ " TO_CHAR(SER.LINE_EXPIRY_DATE,'DD/MM/RRRR') AS LINE_EXPIRY_DATE,"
					+ " SER.LIMIT_AMOUNT AS PLAFOND, CUST.LCY_CURR_BALANCE AS OS,"
					+ " CASE WHEN SUBSTR (CUST.LCY_CURR_BALANCE, 1, 1) = '-' THEN NVL(SER.LIMIT_AMOUNT + CUST.LCY_CURR_BALANCE, '0')"
					+ " ELSE SER.LIMIT_AMOUNT END AS FCU, Ictm_Acc_Udevals.Ude_Value As Rate FROM ICTM_ACC ICTM LEFT JOIN STTM_CUST_ACCOUNT CUST"
					+ " ON ICTM.ACC = CUST.CUST_AC_NO LEFT JOIN"
					+ " Ictm_Acc_Udevals ON ICTM.ACC = Ictm_Acc_Udevals.ACC LEFT JOIN (SELECT SERIAL.BRANCH_CODE, SERIAL.CUST_AC_NO, SERIAL.CUSTOMER_NO, SERIAL.LINKED_REF_NO,"
					+ " SUBSTR (SERIAL.LINKED_REF_NO, 10, 10) AS LINE_SERIAL, AMOUNT.LINE_START_DATE, AMOUNT.LINE_EXPIRY_DATE, AMOUNT.LIMIT_AMOUNT"
					+ " FROM STTM_CUST_ACCOUNT_LINKAGES SERIAL JOIN GETM_FACILITY AMOUNT ON SERIAL.CUSTOMER_NO = AMOUNT.LINE_CODE"
					+ " AND SUBSTR (SERIAL.LINKED_REF_NO, 10, 10) = AMOUNT.LINE_SERIAL) SER ON ICTM.ACC = SER.CUST_AC_NO WHERE ICTM.ACC = ? AND Ictm_Acc_Udevals.UDE_ID ='PRKS_RATE'";
			
			System.out.println("sql head:" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString(i++, acc);
			rs = stat.executeQuery();
			if (rs.next()){
				head = new prks();
				head.setAcc(rs.getString("ACC_PRK"));
				head.setAccBaghas(rs.getString("ACC_BAGHAS"));
				head.setCustName(rs.getString("AC_DESC"));
				head.setAccClass(rs.getString("ACCOUNT_CLASS"));
				head.setStartDt(rs.getString("LINE_START_DATE"));
				head.setExpiryDt(rs.getString("LINE_EXPIRY_DATE"));
				head.setPlafond(rs.getBigDecimal("PLAFOND"));
				head.setOs(rs.getBigDecimal("OS"));
				head.setFcu(rs.getBigDecimal("FCU"));
				head.setRate(rs.getInt("RATE"));
			}
		} catch (Exception e) {
			log.error(" getHeadPrks : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return head;
	}
	
	public List getPinpad (String termId){
		List lbb = new ArrayList();
		pinpad detail = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getWay4DS().getConnection();
			sql = " SELECT DISTINCT EDC.TRML_ID, ACNT.CONTRACT_NAME, EDC.MERCHANT_ID,"
					+ " CASE WHEN EDC.TRX_TYPE = 'CH_PIN' THEN 'PIN VERIFICATION' ELSE 'OTHER'"
					+ " END AS STATUS FROM OPT_EDC_TRX EDC LEFT JOIN(SELECT RBS_NUMBER AS AC_NO,"
					+ " CONTRACT_NUMBER, CONTRACT_NAME FROM ACNT_CONTRACT WHERE CONTRACT_NAME LIKE '%EDC%'"
					+ " ) ACNT ON EDC.TRML_ID = ACNT.CONTRACT_NUMBER WHERE ACNT.CONTRACT_NUMBER = ?";
					
			System.out.println("sql pinpad -->" + sql); 
			System.out.println("termId:" + termId);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString (i++, termId);
			rs = stat.executeQuery();
			while (rs.next()){
				detail = new pinpad();
				detail.setTerminalId(rs.getInt("TRML_ID"));
				detail.setIdMerc(rs.getString("MERCHANT_ID"));
				detail.setMercName(rs.getString("CONTRACT_NAME"));
				detail.setTrnType(rs.getString("STATUS"));
				lbb.add(detail);
			}
		} catch (Exception e) {
			log.error(" getPinpad : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public List getSearchPinpad (String termId, String tgl1, String tgl2){
		List lbb = new ArrayList();
		pinpad download = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getWay4DS().getConnection();
			sql = "SELECT DOC.SOURCE_NUMBER AS TERMINAL_ID, DOC.MERCHANT_ID, DOC.TRANS_DETAILS, TO_CHAR(DOC.TRANS_DATE,'DD-MM-RRRR HH24:MI:SS') AS TRANS_DATE,"
					+ " SUBSTR(ACNT.RBS_NUMBER,1,3) AS BRANCH, ACNT.RBS_NUMBER AS AC_NO, ACNT.CONTRACT_NAME AS AD_DESC, DOC.RET_REF_NUMBER, DOC.TARGET_NUMBER,"
					+ " DOC.TRANS_TYPE, RES.RESP_TEXT AS RETURN_CODE FROM DOC DOC LEFT JOIN RESP_CODE RES"
					+ " ON DOC.RETURN_CODE = RES.RESP_CODE LEFT JOIN ACNT_CONTRACT ACNT ON DOC.TARGET_NUMBER = ACNT.CONTRACT_NUMBER"
					+ " WHERE DOC.SOURCE_NUMBER = ? AND DOC.TRANS_DATE BETWEEN TO_DATE (?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY') + 1"
					+ " AND DOC.AMND_STATE = 'A' AND DOC.TRANS_TYPE = '33336' "
					+ " ORDER BY DOC.TRANS_DATE";
			
			System.out.println("sql getSearchPinpad :" + sql);
			System.out.println("Terminal id:" + termId + "tgl1:" + tgl1  + "tgl2:" + tgl2);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString (i++, termId);
			stat.setString (i++, tgl1);
			stat.setString (i++, tgl2);
			rs = stat.executeQuery();
			while (rs.next()){
				download = new pinpad();
				download.setTrnDt(rs.getString("TRANS_DATE"));
				download.setTrnRefNo(rs.getString("RET_REF_NUMBER") == null ? "-" : rs.getString("RET_REF_NUMBER"));
				download.setAccNo(rs.getString("TARGET_NUMBER"));
				download.setCustAcNo(rs.getString("AC_NO") == null ? "-" : rs.getString("AC_NO"));
				download.setTermId(rs.getString("TERMINAL_ID"));
				download.setIdMerc(rs.getString("MERCHANT_ID"));
				download.setTrnType(rs.getString("RETURN_CODE"));
				download.setBranch(rs.getString("BRANCH") == null ? "-" : rs.getString("BRANCH"));
				download.setMercName(rs.getString("TRANS_DETAILS") == null ? "NO DATA" : rs.getString("TRANS_DETAILS"));
				lbb.add(download);
			}
		} catch (Exception e) {
			log.error(" getDownloadPinpad : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public List getDownloadPinpad (String termId, String tgl1, String tgl2){
		List lbb = new ArrayList();
		pinpad download = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getWay4DS().getConnection();
			sql = "SELECT DOC.SOURCE_NUMBER AS TERMINAL_ID, DOC.MERCHANT_ID, DOC.TRANS_DETAILS, TO_CHAR(DOC.TRANS_DATE,'DD-MM-RRRR HH24:MI:SS') AS TRANS_DATE,"
					+ " SUBSTR(ACNT.RBS_NUMBER,1,3) AS BRANCH, ACNT.RBS_NUMBER AS AC_NO, ACNT.CONTRACT_NAME AS AD_DESC, DOC.RET_REF_NUMBER, DOC.TARGET_NUMBER,"
					+ " DOC.TRANS_TYPE, RES.RESP_TEXT AS RETURN_CODE FROM DOC DOC LEFT JOIN RESP_CODE RES"
					+ " ON DOC.RETURN_CODE = RES.RESP_CODE LEFT JOIN ACNT_CONTRACT ACNT ON DOC.TARGET_NUMBER = ACNT.CONTRACT_NUMBER"
					+ " WHERE DOC.SOURCE_NUMBER = ? AND DOC.TRANS_DATE BETWEEN TO_DATE (?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY') + 1"
					+ " AND DOC.AMND_STATE = 'A' AND DOC.TRANS_TYPE = '33336' "
					+ " ORDER BY DOC.TRANS_DATE";
			
			System.out.println("sql getDownloadPinpad :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString (i++, termId);
			stat.setString (i++, tgl1);
			stat.setString (i++, tgl2);
			rs = stat.executeQuery();
			while (rs.next()){
				download = new pinpad();
				download.setTrnDt(rs.getString("TRANS_DATE"));
				download.setTrnRefNo(rs.getString("RET_REF_NUMBER") == null ? "-" : rs.getString("RET_REF_NUMBER"));
				download.setAccNo(rs.getString("TARGET_NUMBER"));
				download.setCustAcNo(rs.getString("AC_NO") == null ? "-" : rs.getString("AC_NO"));
				download.setTermId(rs.getString("TERMINAL_ID"));
				download.setIdMerc(rs.getString("MERCHANT_ID"));
				download.setTrnType(rs.getString("RETURN_CODE"));
				download.setBranch(rs.getString("BRANCH") == null ? "-" : rs.getString("BRANCH"));
				download.setMercName(rs.getString("TRANS_DETAILS") == null ? "NO DATA" : rs.getString("TRANS_DETAILS"));
				lbb.add(download);
			}
		} catch (Exception e) {
			log.error(" getDownloadPinpad : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public List getMerchantName () {
		List lbb = new ArrayList();
		merchantName edc = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = " SELECT TERMINAL_ID, TERMINAL_ID || ' - ' || MERCHANT_NAME AS NAME, MERCHANT_NAME BRANCH_CODE FROM EDC"
					+ " ORDER BY BRANCH_CODE, PARENT_CODE, TERMINAL_ID ";
			
			System.out.println("sql getMerchantName :" + sql);
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()){
				edc = new merchantName();
				edc.setTerminalId(rs.getString("TERMINAL_ID"));
				edc.setMerchantName(rs.getString("MERCHANT_NAME"));
				edc.setName(rs.getString("NAME"));
				lbb.add(edc);
			}
		} catch (Exception e) {
			log.error(" getMerchantName : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public List getDataEdc (){
		List lbb = new ArrayList();
		merchantName data = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = "SELECT * FROM EDC ORDER BY BRANCH_CODE, PARENT_CODE, TERMINAL_ID";
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()){
				data = new merchantName();
				data.setParent(rs.getString("PARENT_CODE"));
				data.setTerminalId(rs.getString("TERMINAL_ID"));
				data.setMerchantName(rs.getString("MERCHANT_NAME"));
				data.setBranch(rs.getString("BRANCH_CODE"));
				lbb.add(data);
			}
		} catch (Exception e) {
			log.error(" getDataEdc : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		} return lbb;
	}
	
	public List getHaji (String noPorsi){
		List lbb = new ArrayList();
		haji bagiHasil = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		System.out.println("noPorsi -->" + noPorsi);
		
		try {
			conn = DatasourceEntry.getInstance().getLocpgDS().getConnection();
			sql = " SELECT * FROM HAJI WHERE NO_PORSI = ?";
			System.out.println("sql getHaji :" + sql);
			
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString (i++, noPorsi);
			rs = stat.executeQuery();
			while (rs.next()){
				bagiHasil =  new haji();
				bagiHasil.setNoVa(rs.getString("NO_VA"));
				bagiHasil.setNoPorsi(rs.getString("NO_PORSI"));
				bagiHasil.setWilayah(rs.getString("KODE_WILAYAH"));
				bagiHasil.setNama(rs.getString("NAMA"));
				bagiHasil.setAyah(rs.getString("NAMA_AYAH"));
				bagiHasil.setTglLahir(rs.getString("TGL_LAHIR"));
				bagiHasil.setSetoran(rs.getBigDecimal("JUMLAH_SETORAN"));
				bagiHasil.setNilai(rs.getBigDecimal("BAGI_HASIL"));
				lbb.add(bagiHasil);
			}
		} catch (Exception e) {
			log.error(" getHaji : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	public List getDetailUser (String userId){
		List lbb = new ArrayList();
		detailUserPending detail = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		System.out.println("userId -->" + userId);
		try {
			
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
		
			sql = " SELECT CUST.AC_DESC, P_ADDRESS1 ||' ' || P_ADDRESS3 || ' ' ||  P_ADDRESS3 AS ALAMAT,"
					+ " PERSONAL.MOBILE_NUMBER AS HP FROM STTM_CUST_ACCOUNT CUST "
					+ " JOIN STTM_CUST_PERSONAL PERSONAL ON CUST.CUST_NO = PERSONAL.CUSTOMER_NO "
					+ " JOIN SMTB_USER USR ON CUST.AC_DESC = USR.USER_NAME"
					+ " WHERE USER_ID = ?";
			
			System.out.println("sql getDetailUser :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			stat.setString (i++, userId);
			rs = stat.executeQuery();
			while (rs.next()){
				detail = new detailUserPending();
				detail.setUserName(rs.getString("AC_DESC"));
				detail.setMobileNo(rs.getString("HP"));
				detail.setAlamat(rs.getString("ALAMAT"));
				lbb.add(detail);
			}
		} catch (Exception e) {
			log.error(" getDetailUser : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	} 
	
	public List getInsertTes (){
		List lbb = new ArrayList();
		tes insert = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		try {
			
//			Class.forName("org.postgresql.Driver");
//        	conn = DriverManager.getConnection("jdbc:postgresql://10.55.109.50:5432/REPORTSRV", "postgres", "postgres");
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			sql = " SELECT TRN_REF_NO,AC_BRANCH,AC_NO,AC_CCY,DRCR_IND,TRN_CODE,FCY_AMOUNT,EXCH_RATE,LCY_AMOUNT,0 as FCY_BALANCE,0 as LCY_BALANCE,"
					+ " TRN_DT,VALUE_DT,FINANCIAL_CYCLE,PERIOD_CODE,CUST_GL,MODULE,AC_ENTRY_SR_NO,USER_ID,AUTH_ID,PRODUCT,ENTRY_SEQ_NO,TXN_DT_TIME FROM ACTB_HISTORY"
					+ " WHERE TRN_DT = TO_DATE('2013-05-01', 'yyyy-mm-dd')";
			
			System.out.println("sql getDetailUser :" + sql);
			int i = 1;
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()){
				insert = new tes();
				insert.setTrnRefNo(rs.getString("TRN_REF_NO"));
				insert.setAcNo(rs.getString("AC_NO"));
				insert.setBranch(rs.getString("AC_BRANCH"));
				insert.setCcy(rs.getString("AC_CCY"));
				insert.setDrcr(rs.getString("DRCR_IND"));
				insert.setTrnCode(rs.getString("TRN_CODE"));
				insert.setFcyAmount(rs.getBigDecimal("FCY_AMOUNT"));
				insert.setExchRate(rs.getString("EXCH_RATE"));
				insert.setFcyBalance(rs.getBigDecimal("FCY_BALANCE"));
				insert.setLcyAmount(rs.getString("DRCR_IND"));
				insert.setTrnDate(rs.getString("TRN_DT"));
				insert.setValueDt(rs.getString("DRCR_IND"));
				insert.setFinancialCyle(rs.getString("TRN_DT"));
				insert.setPeriodCode(rs.getString("TRN_DT"));
				insert.setCustGl(rs.getString("TRN_DT"));
				insert.setModule(rs.getString("MODULE"));
				insert.setAcEntry(rs.getString("AC_ENTRY_SR_NO"));
				insert.setUserId(rs.getString("USER_ID"));
				insert.setAuthId(rs.getString("AUTH_ID"));
				insert.setProduct(rs.getString("PRODUCT"));
				insert.setEntrySeq(rs.getString("ENTRY_SEQ_NO"));
				insert.setTxnDtTime(rs.getString("TXN_DT_TIME"));
				lbb.add(insert);
			}
		} catch (Exception e) {
			log.error(" getDetailUser : " + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	} 
	
}
