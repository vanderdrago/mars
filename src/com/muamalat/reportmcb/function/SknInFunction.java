package com.muamalat.reportmcb.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.bean.SknInRepRetTtpnBean;
import com.muamalat.reportmcb.entity.SknIn;
import com.muamalat.singleton.DatasourceEntry;

public class SknInFunction {
	private static Logger log = Logger.getLogger(SknInFunction.class);

	public void closeConnDb(Connection conn, PreparedStatement stat, ResultSet rs) {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 1 : " + ex.getMessage());
			}
		}
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 2 : " + ex.getMessage());
			}
		}
		if (stat != null) {
			try {
				stat.close();
				stat = null;
			} catch (SQLException ex) {
				log.error("closeConnDb 3 : " + ex.getMessage());
			}
		}
	}
	
	public SknIn getTotNomSknInRepRetTtpn(SknInRepRetTtpnBean inptskn) {
		SknIn skn = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getSknDS().getConnection();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);
	        
	        sql = "SELECT count(*) AS total, sum(id_amount) AS nominal " +
	        		"FROM bmi_skn_smiw"+inptskn.getBsnsdt().replace("-", "")+".dbo.increditnotes " +
	        		"WHERE id_posting_code = ? and id_alt_no_1 = ?";
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setString(1, inptskn.getRpt());
			stat.setString(2, inptskn.getKdcab());
			rs = stat.executeQuery();
			while (rs.next()) {
				skn = new SknIn();
				skn.setTottrns(rs.getInt("total"));
				skn.setNominal(rs.getBigDecimal("nominal"));
			}
		} catch (SQLException ex) {
		
			log.error(" getTotNomSknInRepRetTtpn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return skn;
	}
	
	public List getSknInRepRetTtpn(SknInRepRetTtpnBean inptskn) {
		List lttpn = new ArrayList();
		SknIn sknin = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";		
		try {
			conn = DatasourceEntry.getInstance().getSknDS().getConnection();
	        sql = "SELECT i.id_ref_no AS sendrefno, i.id_sor AS sor, i.id_sender_clearing_code AS sandikliringasal, i.id_sender_name AS namapengirim, " +
	        		"i.id_benef_name AS namapenerima, i.id_benef_account_name AS namapenerima_bmi, i.id_benef_account AS norekpenerima, i.id_alt_no_1 AS cabangssl," +
	        		"i.id_amount AS amount, i.id_remark AS keterangan, i.id_status4," +
	        		"(CASE WHEN i.id_status_host = 1 THEN 'S' WHEN i.id_status_host = 2 THEN 'F' end) AS sts, i.id_reject_host, " +
	        		"i.id_posting_code, p.pc_desc, i.id_reject_desc " +
	        		"FROM bmi_skn_smiw"+inptskn.getBsnsdt().replace("-", "")+".dbo.increditnotes i INNER JOIN bmi_skn_sectserver.dbo.postingcodes p " +
	        		"ON i.id_posting_code = p.pc_code WHERE p.pc_code = ? and i.id_alt_no_1 = ? ";
	        
			int i=1;
			stat = conn.prepareStatement(sql);	
			stat.setString(1, inptskn.getRpt());
			stat.setString(2, inptskn.getKdcab());
			rs = stat.executeQuery();
			while (rs.next()) {
				sknin = new SknIn();
				sknin.setNo(i++);
				sknin.setNoref(rs.getString("sendrefno"));
				sknin.setSor(rs.getString("sor"));
				sknin.setSbp(rs.getString("sandikliringasal"));
				sknin.setNm_pengirim(rs.getString("namapengirim"));
				sknin.setNm_penerima(rs.getString("namapenerima"));
				sknin.setNm_penerima_core(rs.getString("namapenerima_bmi"));
				sknin.setRek_tujuan(rs.getString("norekpenerima"));
				sknin.setNominal(rs.getBigDecimal("amount"));
				sknin.setKet(rs.getString("keterangan"));	
				sknin.setSts(rs.getString("sts"));
				lttpn.add(sknin);
			}
		} catch (SQLException ex) {		
			log.error(" error in getSknInRepRetTtpn: " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lttpn;
	}
	
	public List getRekapknInRepRetTtpn() {
		List lttpn = new ArrayList();
		SknIn skn = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";

		try {
			conn = DatasourceEntry.getInstance().getSknDS().getConnection();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        dateFormat.format(date);	        
	        sql += "SELECT DISTINCT i.id_alt_no_1, p.pc_code, p.pc_desc, count(*) AS total_trans, sum(i.id_amount) AS total_nominal " +
	        		"FROM bmi_skn_smiw"+dateFormat.format(date)+".dbo.increditnotes i INNER JOIN bmi_skn_sectserver.dbo.postingcodes p " +
	        		"ON i.id_posting_code = p.pc_code WHERE p.pc_code in ('05', '03') " +
	        		"GROUP BY i.id_alt_no_1, p.pc_code, p.pc_desc " +
	        		"ORDER BY i.id_alt_no_1 ASC";
			int i=1;
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();
			while (rs.next()) {
				skn = new SknIn();
				skn.setNo(i++);
				skn.setCab_ssl(rs.getString("id_alt_no_1"));
				skn.setP_code(rs.getString("pc_code"));
				skn.setDesc_rep(rs.getString("pc_desc"));
				skn.setTottrns(rs.getInt("total_trans"));
				skn.setNominal(rs.getBigDecimal("total_nominal"));
				lttpn.add(skn);
			}
		} catch (SQLException ex) {		
			log.error(" getRekapknInRepRetTtpn : " + ex.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lttpn;
	}
}
