package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.Instruction;
import com.muamalat.reportmcb.entity.fbti;
import com.muamalat.reportmcb.entity.jadangFbti;
import com.muamalat.reportmcb.function.SqlFunctionNew;

public class detailInstNoFwd extends org.apache.struts.action.Action{

	private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(detailInstNoFwd.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
			return mapping.findForward("loginpage");
		}
		
		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			
			List lInstl = new ArrayList();
			String instNo = (String) request.getParameter("instNo");
			lInstl = (List) request.getSession(true).getAttribute("llsp");
			System.out.println("instNo : " + instNo);
			jadangFbti headFbti = null;
			if (lInstl.size() > 0){
				for (Iterator<jadangFbti> i = lInstl.iterator(); i.hasNext();){
					jadangFbti jmcb = i.next();
					if (instNo != jmcb.getInstallmentNo()){
						headFbti = jmcb;
						System.out.println("instNo dalam if: " + instNo);
						System.out.println("headFbti dalam if: " + headFbti.getInstallmentNo());
						break;
					}
					
				}
			}
			System.out.println("headFbti luar if: " + headFbti.getInstallmentNo());
			SqlFunctionNew sql = new SqlFunctionNew();
			jadangFbti head = sql.getFBTI(instNo);
			System.out.println("headFbti :" + headFbti);
			if (headFbti != null){
				List detail = sql.getDetailJadangFbti(instNo);
				request.setAttribute("detailJadang", detail);
				request.setAttribute("headFbti", head);
			}
//			fbti jadang = sql.getFBTI(instNo);
//			if (jadang != null){
//				fbti detailJadang = sql.getDetailJadangFbti(instNo);
//				if (detailJadang != null){
//					request.setAttribute("detailJadang", detailJadang);
//				}
//				request.setAttribute("detail", jadang);
//				return mapping.findForward(SUCCESS);
//			} else {
//				request.setAttribute("message", "NO DATA!");
//				request.setAttribute("message_error", "ERROR NO DATA!");
//				return mapping.findForward("msgpage");
			return mapping.findForward(SUCCESS);
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return mapping.findForward("msgpage");
		}
	}
}
