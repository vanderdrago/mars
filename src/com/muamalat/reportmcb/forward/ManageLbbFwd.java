/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.forward;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Bukubesar;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.function.SqlFunction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;

/**
 *
 * @author Utis
 */
public class ManageLbbFwd extends org.apache.struts.action.Action {

    private static Logger log = Logger.getLogger(ManageLbbFwd.class);

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int level;
        int validLevel = 5;
        
	 	

        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            
          

            return mapping.findForward("loginpage");
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			 	proseBb(request);
			 	String val = (String) request.getSession(true).getAttribute("val");
			 	request.setAttribute("viewccy", val);
		        return mapping.findForward(SUCCESS);
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return mapping.findForward("msgpage");
	        }
    }
    
    public void proseBb(HttpServletRequest request){
        List lbukbes = new ArrayList();
        SqlFunction f = new SqlFunction();
    	String acc = "";
    	String brnch = "";
    	String ccy= "";
    	String tgl1 = "";
    	String tgl2 = "";
    	String title = "";
    	String cbsrekoto = "";
    	//penambahan allcab-allval BukuBesar, Agustus 2016
    	String allcab = "";
    	String allval = "";

    	int sawal = 1;
        int sakhir = 10;
        int recordsPerPage = 30;
        
        title = (String) request.getSession(true).getAttribute("title");
        cbsrekoto = (String) request.getSession(true).getAttribute("cbsrekoto");
        //penambahan allcab-allval BukuBesar, Agustus 2016
        allcab = (String) request.getSession(true).getAttribute("allcab");
        allval = (String) request.getSession(true).getAttribute("allval");

        acc = (String) request.getSession(true).getAttribute("acount");
        brnch = (String) request.getSession(true).getAttribute("kdcab");
        ccy = (String) request.getSession(true).getAttribute("val");
        tgl1 = (String) request.getSession(true).getAttribute("tgl1");
        tgl2 = (String) request.getSession(true).getAttribute("tgl2");
    
        int page = Integer.parseInt(request.getParameter("page"));
        int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("noOfRecords").toString());
        int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPages").toString());
        BigDecimal sdawal = (BigDecimal) request.getSession(true).getAttribute("sdawal");
        BigDecimal saldoakhir = (BigDecimal) request.getSession(true).getAttribute("saldoakhir");
        Bukubesar bukbes = (Bukubesar) request.getSession(true).getAttribute("bukbes");
 
        if (page == 1){
       
        	//Saldo sd = f.getSaldo(acc, brnch, ccy, tgl1);
			Saldo sd = f.getSaldo(acc, brnch, ccy, tgl1, allcab, allval);
        	//lbukbes = f.getcontohbuku2(tgl1, tgl2, acc, brnch, ccy, 1, 30, cbsrekoto, sdawal);
			lbukbes = f.getcontohbuku2(tgl1, tgl2, acc, brnch, ccy, 1, 30, cbsrekoto, sdawal, allcab, allval);
        	sawal = page;
        	if (noOfPages < 10){
        		sakhir = noOfPages;
        	} else {
        		sakhir = 10;
        	}
        } else {   
        	
        	sawal = page;
        	int tempsakhir = 10 + (page-1);
        	if (noOfPages < tempsakhir){
        		sakhir = noOfPages;
        	} else {
        		sakhir = tempsakhir;
        	}
//			BigDecimal tsldo = sdawal.add(f.getSaldo2(tgl1, tgl2, acc, brnch, ccy, 1, ((page -1) * recordsPerPage), cbsrekoto));
			BigDecimal tsldo = sdawal.add(f.getSaldo2(tgl1, tgl2, acc, brnch, ccy, 1, ((page -1) * recordsPerPage), cbsrekoto, allcab, allval));
//			lbukbes = f.getcontohbuku2(tgl1, tgl2, acc, brnch, ccy, ((page -1) * recordsPerPage) + 1, page * recordsPerPage, cbsrekoto, tsldo);
			lbukbes = f.getcontohbuku2(tgl1, tgl2, acc, brnch, ccy, ((page -1) * recordsPerPage) + 1, page * recordsPerPage, cbsrekoto, tsldo, allcab, allval);
        }
        request.setAttribute("title", title);
        request.setAttribute("sawal", sawal);
        request.setAttribute("sakhir", sakhir);
        request.setAttribute("cbsrekoto", cbsrekoto); //tambahan
        request.setAttribute("currentPage", page);
        request.setAttribute("lbukbes", lbukbes);
        request.setAttribute("bukbes", bukbes);
        request.setAttribute("noOfRecords", noOfRecords);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("saldoakhir", saldoakhir);
        //penambahan allcab-allval BukuBesar, Agustus 2016
        request.setAttribute("allcab", allcab);
        request.setAttribute("allval", allval);

      	title = "BUKU BESAR <br /> " + tgl1 + " - " + tgl2;
        request.setAttribute("title", title);
        request.setAttribute("view", "data");
    }
}
