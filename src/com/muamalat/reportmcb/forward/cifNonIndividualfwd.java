package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.SqlFunction;

public class cifNonIndividualfwd extends org.apache.struts.action.Action{
	private static Logger log = Logger.getLogger(cifNonIndividualfwd.class);
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		System.out.println("masuk awal");
		SUCCESS = isValidUser (request);
		System.out.println("sukses");
		if (!SUCCESS.equals("success")){
			return mapping.findForward(SUCCESS);
		}
		try {
			System.out.println("masuk cif non");
			List lphNon = new ArrayList();
			SqlFunction sql = new SqlFunction();
			lphNon = sql.getcifNonIndividual();
			if (lphNon.size() > 0 ) {
				System.out.println("size" + lphNon.size());
				request.setAttribute("lphNon", lphNon);
				request.getSession(true).setAttribute("lphNon", lphNon);
			} else {
				request.setAttribute("dataresponse", "Tidak ditemukan transaksi hari ini");
                request.setAttribute("konfirmasi", "err");
			}
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			System.out.println("error");
			request.setAttribute("errpage", "Error : " + e.getMessage());
			return mapping.findForward("failed");
		}
	}
	
	private String isValidUser(HttpServletRequest request) {
		if (request.getSession(true).getAttribute(StaticParameter.USER_ENTITY) == null) {
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		if (sql.getMenuStatus(lrole, "cifNonIndividual.do")) {
			List lph = (List) request.getSession(true).getAttribute("lph");
        	request.setAttribute("lph", lph);
        	return SUCCESS;
		} else {
			request.setAttribute("message", "Unauthorized Access!");
            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            //request.setAttribute("error_code", "Error_timeout");
            return "msgpage";
		}
	}
	
}
