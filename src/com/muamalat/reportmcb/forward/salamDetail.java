package com.muamalat.reportmcb.forward;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.salamMuamalat;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.salamMuamalatSql;

public class salamDetail extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(salamDetail.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
		}
		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			salamMuamalatSql sql = new salamMuamalatSql();
//			SqlFunction sql = new SqlFunction();
			String no_kartu = (String) request.getSession(true).getAttribute("no_kartu");
			String prd1 = (String) request.getParameter("prd1");
			salamMuamalat salam = sql.getDetailSalam(no_kartu);
			
			System.out.println("no kartu :" + no_kartu);
			System.out.println("period :" + prd1);
			if (salam != null){
				salamMuamalat total = sql.getAngsuran(no_kartu, prd1);
				if (total != null){
					request.setAttribute("total", total);
				}
//				salamMuamalat tunggakan = sql.getTunggakan(no_kartu, prd1);
//				if (tunggakan != null){
//					request.setAttribute("tunggakan", tunggakan);
//				}
				request.setAttribute("salam", salam);
				return mapping.findForward(SUCCESS);
			} else {
				request.setAttribute("message", "NO DATA!");
				request.setAttribute("message_error", "ERROR NO DATA!");
				return mapping.findForward("msgpage");
			}
		}else {
	    	 request.setAttribute("message", "Unauthorized Access!");
	    	 request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	         return mapping.findForward("msgpage");
		}
	}
}
