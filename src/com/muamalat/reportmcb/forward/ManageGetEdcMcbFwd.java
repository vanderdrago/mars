package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.FinSqlFunction;

public class ManageGetEdcMcbFwd extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(ManageGetEdcMcbFwd.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
//		         List lbb = (List) request.getSession(true).getAttribute("ltrn");
//		         request.setAttribute("ltrn", lbb);
			 	prosesBb(request);
		        return mapping.findForward(SUCCESS);
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return mapping.findForward("msgpage");
	        }
    }

	private void prosesBb(HttpServletRequest request) {
		  
		List RptEdcMcb = new ArrayList(); 
        FinSqlFunction sql = new FinSqlFunction();
      
		String tgl_awal="";
		String tgl_akhir="";
		String branch="";
		
		tgl_awal = (String) request.getSession(true).getAttribute("tgl_awal");
		tgl_akhir = (String) request.getSession(true).getAttribute("tgl_akhir");
		branch = (String) request.getSession(true).getAttribute("branch");
		
		int sawal = 0;
		int sakhir = 0;
		int recordsPerPage = 100;
		
		
		
		

		
		int page = Integer.parseInt(request.getParameter("page"));
		int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("recordsAngsuran").toString());
		int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPagesAngsuran").toString());
		
		if (page == 1){
			RptEdcMcb = sql.getListEdcMcb(tgl_awal, tgl_akhir, branch, 1, 1000);
			sawal = page;
			if (noOfPages < 10) {
				sakhir = noOfPages;
			} else {
				sakhir = 10;
			}
		} else {
			sawal = page;
			int tempsakhir = 10 + (page -1);
			if (noOfPages < tempsakhir){
				sakhir = noOfPages;
        	} else {
        		sakhir = tempsakhir;
        	}
			RptEdcMcb = sql.getListEdcMcb(tgl_awal, tgl_akhir, branch, ((page -1) * recordsPerPage) + 1, page * recordsPerPage);
		}
		request.setAttribute("sawalAngsuran", sawal);
        request.setAttribute("sakhirAngsuran", sakhir);
        request.setAttribute("RptEdcMcb", RptEdcMcb);
        request.setAttribute("noOfRecordsAngsuran", noOfRecords);
        request.setAttribute("noOfPagesAngsuran", noOfPages);
        request.setAttribute("currentPageAngsuran", page);
	}
}
