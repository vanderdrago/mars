package com.muamalat.reportmcb.forward;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.SqlFunction;

public class eomRepFwd extends org.apache.struts.action.Action {

    private static Logger log = Logger.getLogger(eomRepFwd.class);

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }
        List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
        SqlFunction sql = new SqlFunction();
        if (sql.getMenuStatus(lrole, "eomRepFwd.do")){
			 request.getSession(true).setAttribute("noOfRecords", null); 
		        request.getSession(true).setAttribute("noOfPages", null); 
	            request.getSession(true).setAttribute("llsp", null);	
		        return mapping.findForward(SUCCESS);
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return mapping.findForward("msgpage");
	        }
    }

}
