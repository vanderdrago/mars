package com.muamalat.reportmcb.forward;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.function.CetakPassbookFunction;

public class VPeragaanHistD extends org.apache.struts.action.Action {

    private static Logger log = Logger.getLogger(VPeragaanHistD.class);

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			 //tutup dari sini
			 
			 String norek = (String) request.getParameter("ac");
			 String nama = (String) request.getParameter("nm");
			 List llsp = (List) request.getSession(true).getAttribute("llsp");
			 request.setAttribute("norek", norek);
			 request.setAttribute("nama", nama);
			 request.setAttribute("lph", llsp);
			 request.getSession(true).setAttribute("norek", norek);
			 request.getSession(true).setAttribute("nama", nama);
             request.getSession(true).setAttribute("llsp", llsp);			 
		     return mapping.findForward(SUCCESS); //sampe sini
		     
		     /*
			 CetakPassbookFunction cpf = new CetakPassbookFunction();
			 
			 Admin adm = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
			 
			 if ("000".equals(adm.getKodecabang())){
				 
			 } else {			 
				 List lroles = cpf.getRolesUser(adm.getUsername());
				 int ada = 0;
				if (lroles.size() > 0) {
					for (Iterator<String> it = lroles.iterator(); it.hasNext();) {
						String rl = it.next();
						String role = cpf.getAllowedStatement(12, rl.toUpperCase());
						if (!"".equals(role) && role!=null){
							ada++;
						}
					}						
					if (ada > 0) {
						String norek = (String) request.getParameter("ac");
						String nama = (String) request.getParameter("nm");
						List llsp = (List) request.getSession(true).getAttribute( "llsp");
						request.setAttribute("norek", norek);
						request.setAttribute("nama", nama);
						request.getSession(true).setAttribute("norek", norek);
						request.getSession(true).setAttribute("nama", nama);
						request.getSession(true).setAttribute("llsp", llsp);
						return mapping.findForward(SUCCESS);
					} else {
						request.setAttribute("message", "Unauthorized Access!");
						request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
						return mapping.findForward("msgpage");				
					}
				} else {
					request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
					return mapping.findForward("msgpage");
				}
			 }
			/*
			System.out.println("user : " + adm.getUsername());
			 System.out.println("roles : " + roles);
//			 String role = cpf.getAllowedPrintPassbook(12, user.getRole_id());
			 String role = cpf.getAllowedStatement(12, roles);
			 System.out.println("role : " + role);
			 if ("".equals(role) || role == null){
				 request.setAttribute("message", "Unauthorized Access!");
				 request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
				 return mapping.findForward("msgpage");
			 } else {
				 if((role.indexOf(roles.toUpperCase()) > -1 ? true : false)){
					 String norek = (String) request.getParameter("ac");
					 String nama = (String) request.getParameter("nm");
					 List llsp = (List) request.getSession(true).getAttribute("llsp");
					 request.setAttribute("norek", norek);
					 request.setAttribute("nama", nama);
					 request.getSession(true).setAttribute("norek", norek);
					 request.getSession(true).setAttribute("nama", nama);
		             request.getSession(true).setAttribute("llsp", llsp);			 
				     return mapping.findForward(SUCCESS);				
				 } else {	
					 request.setAttribute("message", "Unauthorized Access!");
					 request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
					 return mapping.findForward("msgpage");						
				 }
			 }
			 */
			 
	     } else {
	    	 request.setAttribute("message", "Unauthorized Access!");
	    	 request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	         return mapping.findForward("msgpage");
	     }
    }
}
