package com.muamalat.reportmcb.forward;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class VPeragaanCustDMKT extends org.apache.struts.action.Action {

	private static Logger log = Logger.getLogger(VPeragaanCustDMKT.class);

	/* forward name="success" path="" */
	private static final String SUCCESS = "success";

	/**
	 * This is the action called from the Struts framework.
	 * 
	 * @param mapping
	 *            The ActionMapping used to select this instance.
	 * @param form
	 *            The optional ActionForm bean for this request.
	 * @param request
	 *            The HTTP Request we are processing.
	 * @param response
	 *            The HTTP Response we are processing.
	 * @throws java.lang.Exception
	 * @return
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
			return mapping.findForward("loginpage");
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			SqlFunction f = new SqlFunction();
			String norek = (String) request.getParameter("norek");			
			SttmCustAccount DetailCustom = f.getDetailCustom(norek, user.getUsername().trim());
			if (DetailCustom != null) {
				request.setAttribute("DetailCustom", DetailCustom);
				return mapping.findForward(SUCCESS);
			} else {
				request.setAttribute("message", "NO DATA!");
				request.setAttribute("message_error", "ERROR NO DATA!");
				return mapping.findForward("msgpage");
			}
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			return mapping.findForward("msgpage");
		}
	}

}
