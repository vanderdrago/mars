package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.SqlFunction;

public class manageLphCsFwd extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(manageLphCsFwd.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }
        
        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			proseBb(request);
		    return mapping.findForward(SUCCESS);
	    } else {
            request.setAttribute("message", "Unauthorized Access!");
            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
            return mapping.findForward("msgpage");
	    }
	}
	
	private void proseBb(HttpServletRequest request) {		
		List lphCs = new ArrayList();
		SqlFunction sql = new SqlFunction();
		String userId = "";
		
		int sawal = 0;
		int sakhir = 0;
		int recordsPerPage = 30; 
		
		userId = (String) request.getSession(true).getAttribute("userId");
		int page = Integer.parseInt(request.getParameter("page"));
		int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("recordsCs").toString());
		int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("OfPagesCs").toString());
		
		if (page == 1){
			lphCs = sql.GetLapSpvCs(userId, sawal, recordsPerPage);
			sawal = page;
			if (noOfPages < 10) {
				sakhir = noOfPages;
			} else {
				sakhir = 10;
			}
		} else {
			sawal = page;
			int tempsakhir = 10 + (page -1);
			if (noOfPages < tempsakhir){
				sakhir = noOfPages;
        	} else {
        		sakhir = tempsakhir;
        	}
			lphCs = sql.GetLapSpvCs(userId, ((page -1) * recordsPerPage) + 1, page * recordsPerPage);
		}
		request.setAttribute("sawalCs", sawal);
        request.setAttribute("sakhirCs", sakhir);
        request.setAttribute("lphCs", lphCs);
        request.setAttribute("noOfRecordsCs", noOfRecords);
        request.setAttribute("noOfPagesCs", noOfPages);
        request.setAttribute("currentPageCs", page);
        
        System.out.println("sawal :" + sawal);
        System.out.println("sakhir :" + sakhir);
        System.out.println("currentPageCs :" + page);
        System.out.println("noOfRecordsCs :" + noOfRecords);
        System.out.println("noOfPagesCs :" + noOfPages);
        System.out.println("recordsPerPage :" + recordsPerPage);
        System.out.println("lphCs :" + lphCs.size());
        System.out.println("userId :" + userId);
        System.out.println("begin :" + ((page -1) * recordsPerPage) + 1);
        System.out.println("delta :" + page * recordsPerPage);
	}
}
