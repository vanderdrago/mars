package com.muamalat.reportmcb.forward;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.BatchMaster;
import com.muamalat.reportmcb.entity.Bukubesar;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.function.SqlFunction;

public class ManagelBatch1 extends org.apache.struts.action.Action {
	
	 private static Logger log = Logger.getLogger(ManagelBatch1.class);

	    /* forward name="success" path="" */
	    private static final String SUCCESS = "success";

	    /**
	     * This is the action called from the Struts framework.
	     * @param mapping The ActionMapping used to select this instance.
	     * @param form The optional ActionForm bean for this request.
	     * @param request The HTTP Request we are processing.
	     * @param response The HTTP Response we are processing.
	     * @throws java.lang.Exception
	     * @return
	     */
	    @Override
	    public ActionForward execute(ActionMapping mapping, ActionForm form,
	            HttpServletRequest request, HttpServletResponse response)
	            throws Exception {
	        int level;
	        int validLevel = 5;
	        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
	            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
	            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
	            return mapping.findForward("loginpage");
	        }
	        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
			 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
				 System.out.println("req"+(String)request.getSession(true).getAttribute("user"));	           
				// proseBb(request);
//				 List lBatchM = (List) request.getSession(true).getAttribute("lBatchM");
//			       			      
//			        request.setAttribute("lBatchM", lBatchM);
			      		  
			        return mapping.findForward(SUCCESS);
		        } else {
		            request.setAttribute("message", "Unauthorized Access!");
		            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
		            //request.setAttribute("error_code", "Error_timeout");
		            return mapping.findForward("msgpage");
		        }
	    }


	  /*  
	    public void proseBb(HttpServletRequest request){
	        List lBatchM = new ArrayList();
	        SqlFunction f = new SqlFunction();
	        String user_id = "";
	        String brnch ="";
	        String tgl1 = "";
	       // String tgl2 = "";
	        String mod = "";
	    	String title = "";
	    	int sawal = 1;
	        int sakhir = 10;
	        int recordsPerPage = 30;
	        System.out.println("req"+(String)request.getSession(true).getAttribute("user"));	     
	        title = (String) request.getSession(true).getAttribute("title");
	        user_id = (String)request.getSession(true).getAttribute("user");
	        brnch = (String) request.getSession(true).getAttribute("kdcab");
	        tgl1 = (String) request.getSession(true).getAttribute("tgl1");
	       // tgl2 = (String) request.getSession(true).getAttribute("tgl2");
	       // mod = (String) request.getParameter("mod").trim();
	       // int page = Integer.parseInt(request.getParameter("page"));
	        int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("noOfRecords").toString());
	        int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPages").toString());
//	        BigDecimal saldoakhir = (BigDecimal) request.getSession(true).getAttribute("saldoakhir");
	        BatchMaster bb = (BatchMaster) request.getSession(true).getAttribute("bb");  
	        System.out.println("req"+page);	       
	        if (page == 1){
	        	//lBatchM = f.getListbatchht(user_id,brnch,tgl1,tgl2,mod, 1, 30);
	        	//lBatchM = f.getListbatchh(user_id,brnch,tgl1, 1, 30);
	        	sawal = page;
	        	if (noOfPages < 10){
	        		sakhir = noOfPages;
	        	} else {
	        		sakhir = 10;
	        	}
	        } else {        	
	        	sawal = page;
	        	int tempsakhir = 10 + (page-1);
	        	if (noOfPages < tempsakhir){
	        		sakhir = noOfPages;
	        	} else {
	        		sakhir = tempsakhir;
	        	}
	      //  	lBatchM = f.getListbatchht(user_id,brnch,tgl1,tgl2,mod, ((page -1) * recordsPerPage) + 1, page * recordsPerPage);
	        //	lBatchM = f.getListbatchh(user_id,brnch,tgl1, ((page -1) * recordsPerPage) + 1, page * recordsPerPage);
	        }
	        request.setAttribute("title", title);
	        request.setAttribute("sawal", sawal);
	        request.setAttribute("sakhir", sakhir);
	        request.setAttribute("currentPage", page);
	        request.setAttribute("lBatchM", lBatchM);
	        request.setAttribute("bb", bb);
	        request.setAttribute("noOfRecords", noOfRecords);
	        request.setAttribute("noOfPages", noOfPages);
	        request.setAttribute("currentPage", page);
//	        request.setAttribute("saldoakhir", saldoakhir); 
	    }*/

	    
	    
}
