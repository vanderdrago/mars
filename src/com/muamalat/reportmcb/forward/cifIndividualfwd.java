package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ibm.db2.jcc.am.lp;
import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.SqlFunction;

public class cifIndividualfwd extends org.apache.struts.action.Action{
	private static Logger log = Logger.getLogger(cifIndividualfwd.class);
	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SUCCESS = isValidUser (request);
		if (!SUCCESS.equals("success")){
			return mapping.findForward(SUCCESS);
		}
		try {
			List lphIndividual = new ArrayList();
			SqlFunction sql = new SqlFunction();
			lphIndividual = sql.GetcifIndividual();
			if (lphIndividual.size() > 0 ) {
				request.setAttribute("lphIndividual", lphIndividual);
				request.getSession(true).setAttribute("lphIndividual", lphIndividual);
			} else {
				request.setAttribute("dataresponse", "Tidak ditemukan transaksi hari ini");
                request.setAttribute("konfirmasi", "err");
			}
			return mapping.findForward(SUCCESS);
		} catch (Exception e) {
			request.setAttribute("errpage", "Error : " + e.getMessage());
			return mapping.findForward("failed");
		}
	}


	private String isValidUser(HttpServletRequest request) {
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return "loginpage";
        }
        List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
        SqlFunction sql = new SqlFunction();
        if (sql.getMenuStatus(lrole, "cifIndividual.do")){
        	List lphIndividual = (List) request.getSession(true).getAttribute("lphIndividual");
        	request.setAttribute("lphIndividual", lphIndividual);
        	return SUCCESS;
        	}
        else {
        	request.setAttribute("message", "Unauthorized Access!");
            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
            //request.setAttribute("error_code", "Error_timeout");
            return "msgpage";
        }
	}
}
