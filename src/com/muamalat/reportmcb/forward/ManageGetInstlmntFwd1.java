/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.Instalment;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.FinSqlFunction;
import com.muamalat.reportmcb.function.SqlFunction;

/**
 *
 * @author Utis
 */
public class ManageGetInstlmntFwd1 extends org.apache.struts.action.Action {

    private static Logger log = Logger.getLogger(ManageGetInstlmntFwd1.class);
    private static final String SUCCESS = "success";
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
//    	int level;
//        int validLevel = 5;
//        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
//            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
//            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
//            return mapping.findForward("loginpage");
//        }
//        
//        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
//			proseBb(request);
//		    return mapping.findForward(SUCCESS);
//	    } else {
//            request.setAttribute("message", "Unauthorized Access!");
//            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//            return mapping.findForward("msgpage");
//	    }
//	}
//    
//	private void proseBb(HttpServletRequest request) {
//		List lInstl = new ArrayList();
//		FinSqlFunction f = new FinSqlFunction();
//		String instNo = "";
//		
//		int sawal = 0;
//		int sakhir = 0;
//		int recordsPerPage = 30;
//		instNo = (String) request.getSession(true).getAttribute("instNo");
//		lInstl = (List) request.getSession(true).getAttribute("ltrnAngsuran");
//		System.out.println("noInstl :"  + instNo);
//		Instalment mstrInstl = null;
//		if(lInstl.size() > 0){
//			for (Iterator<Instalment> i = lInstl.iterator(); i.hasNext();) {
//				Instalment jmcb = i.next();
//				if(instNo.equals(jmcb.getAccount_number())){
//					mstrInstl = jmcb;
//					break;
//				}
//			}
//		}
//		
//		Instalment heds = f.gethedjadang(mstrInstl.getAccount_number(), mstrInstl.getAmount_disbursed(), mstrInstl.getUpfront_profit_booked());
//		mstrInstl.setTempTotPkh(heds.getTempTotPkh());
//		mstrInstl.setTempTotMrgnh(heds.getTempTotMrgnh());
//		mstrInstl.setTempPaidh(heds.getTempPaidh());
//		if(mstrInstl!=null){
//			List angs = f.getdjadang(mstrInstl);
//			request.setAttribute("angs", angs);
//			request.setAttribute("mstrInstl", mstrInstl);
//		}
//		int page = Integer.parseInt(request.getParameter("pageAngsuran"));
//		int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("noOfRecordsAngsuran").toString());
//		int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPagesAngsuran").toString());
//		
//		if (page == 1){
//			if(mstrInstl!=null){
//				List angs = f.getdjadang(mstrInstl);
//			}
//			sawal = page;
//			if (noOfPages < 10) {
//				sakhir = noOfPages;
//			} else {
//				sakhir = 10;
//			}
//		} else {
//			sawal = page;
//			int tempsakhir = 10 + (page -1);
//			if (noOfPages < tempsakhir){
//				sakhir = noOfPages;
//        	} else {
//        		sakhir = tempsakhir;
//        	}
//			lphSaldo = sql.getListSaldoRata(acc, nama.toUpperCase(), (((page-1) * recordsPerPage) - 1));
//		}
//		
//	}
    	
    	int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }
        List lrole =  (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
        System.out.println("lrole" + lrole);
        SqlFunction sql = new SqlFunction();
        if (sql.getMenuStatus(lrole, "MonGetInstlForward1.do")){
//	            request.getSession(true).setAttribute("ltrn", null);
	           return mapping.findForward(SUCCESS);
        }else {
			 	request.setAttribute("message", "Unauthorized Access!");
				request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
				//request.setAttribute("error_code", "Error_timeout");
				return mapping.findForward("msgpage");
		 }
    }
}
