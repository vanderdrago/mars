package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.entity.banking;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class detailPinpad extends org.apache.struts.action.Action {

	private static Logger log = Logger.getLogger(detailPinpad.class);

	/* forward name="success" path="" */
	private static final String SUCCESS = "success";

	/**
	 * This is the action called from the Struts framework.
	 * 
	 * @param mapping
	 *            The ActionMapping used to select this instance.
	 * @param form
	 *            The optional ActionForm bean for this request.
	 * @param request
	 *            The HTTP Request we are processing.
	 * @param response
	 *            The HTTP Response we are processing.
	 * @throws java.lang.Exception
	 * @return
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
			return mapping.findForward("loginpage");
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			String termId = (String) request.getParameter("termId");	
			String mercName = (String) request.getParameter("mercName");
			List llsp = (List) request.getSession(true).getAttribute("edc");
			System.out.println("termId:" + termId + "merc Name:" + mercName);
			request.setAttribute(termId, "termId");
			request.setAttribute("mercName", mercName);
			request.setAttribute("lph", llsp);
			request.getSession(true).setAttribute("termId", termId);
			request.getSession(true).setAttribute("mercName", mercName);
			request.getSession(true).setAttribute("llsp", llsp);
			return mapping.findForward(SUCCESS);
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return mapping.findForward("msgpage");
		}
	}

}
