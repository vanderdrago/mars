package com.muamalat.reportmcb.forward;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;

public class salamMuamalatPeriod extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(salamMuamalatPeriod.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
		}
		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			String no_kartu = (String) request.getParameter("no_kartu");
			String nama = (String) request.getParameter("nama");
			List lhist = (List) request.getSession(true).getAttribute("lhist");
			request.setAttribute(no_kartu, "no_kartu");
			request.setAttribute(nama, "nama");
			request.getSession(true).setAttribute("no_kartu", no_kartu);
			request.getSession(true).setAttribute("nama", nama);
			request.getSession(true).setAttribute("lhist", lhist);
			return mapping.findForward(SUCCESS);
		}else {
	    	 request.setAttribute("message", "Unauthorized Access!");
	    	 request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	         return mapping.findForward("msgpage");
	    }
	}
	
}
