package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.reportmcb.function.SqlFunctionNew;

public class pinpad extends org.apache.struts.action.Action{
	
	private static Logger log = Logger.getLogger(pinpad.class);
    private static final String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }
        List lrole =  (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
        List merchant = new ArrayList();
        SqlFunction sql = new SqlFunction();
        SqlFunctionNew sql1 = new SqlFunctionNew();
        if (sql.getMenuStatus(lrole, "pinpad.do")){
        	merchant = sql1.getMerchantName();
        	request.getSession(true).setAttribute("name", merchant);
	        return mapping.findForward(SUCCESS);
        }else {
		 	request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
			//request.setAttribute("error_code", "Error_timeout");
			return mapping.findForward("msgpage");
		 }
    }
}
