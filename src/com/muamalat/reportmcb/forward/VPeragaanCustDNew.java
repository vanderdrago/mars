package com.muamalat.reportmcb.forward;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.SMTB_USER_ROLE;
import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class VPeragaanCustDNew extends org.apache.struts.action.Action {

	private static Logger log = Logger.getLogger(VPeragaanCustDNew.class);

	/* forward name="success" path="" */
	private static final String SUCCESS = "success";

	/**
	 * This is the action called from the Struts framework.
	 * 
	 * @param mapping
	 *            The ActionMapping used to select this instance.
	 * @param form
	 *            The optional ActionForm bean for this request.
	 * @param request
	 *            The HTTP Request we are processing.
	 * @param response
	 *            The HTTP Response we are processing.
	 * @throws java.lang.Exception
	 * @return
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
			return mapping.findForward("loginpage");
		}

		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		Admin user = (Admin) request.getSession(true).getAttribute(StaticParameter.USER_ENTITY);
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			SqlFunction f = new SqlFunction();
			String norek = (String) request.getParameter("norek");			
			SttmCustAccount DetailCustom = f.getDetailCustom(norek, user.getUsername().trim());
			if (DetailCustom != null) {
				List<HashMap> MapResult = new ArrayList<HashMap>();
				DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
				DecimalFormatSymbols format = new DecimalFormatSymbols();
				format.setMonetaryDecimalSeparator(',');
				format.setGroupingSeparator('.');
				df.setDecimalFormatSymbols(format);
				
				HashMap map = new HashMap();
				BigDecimal saldoMinimum = new BigDecimal(0);
				BigDecimal saldoMemo = new BigDecimal(0);
				BigDecimal saldoHold = new BigDecimal(0);
				BigDecimal saldoEfektif = new BigDecimal(0);
				BigDecimal saldoRata2 = new BigDecimal(0);
				BigDecimal amount = new BigDecimal(0);
				
				map.put("saldoMinimum",df.format(DetailCustom.getMinBalance()));
				map.put("saldoMemo",df.format(DetailCustom.getAcyCurrBalance()));
				map.put("saldoHold",df.format(DetailCustom.getAcyBlockedAmount()));
				map.put("saldoEfektif",df.format(DetailCustom.getAcyAvlBal()));
				map.put("saldoRata2",df.format(DetailCustom.getAcy_bal()));
//				map.put("amount", df.format(DetailCustom.getAmount()));
								
				request.setAttribute("DetailCustom", DetailCustom);
				request.setAttribute("saldo", MapResult);
				System.out.println("saldoMinimum" + map.get("saldoMinimum") + "saldoMemo" + map.get("saldoMemo") + "saldoHold" + map.get("saldoHold") + "saldoEfektif" + map.get("saldoEfektif") + "saldoRata2" + map.get("saldoRata2"));
				MapResult.add(map);
				return mapping.findForward(SUCCESS);
			} else {
				request.setAttribute("message", "NO DATA!");
				request.setAttribute("message_error", "ERROR NO DATA!");
				return mapping.findForward("msgpage");
			}
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
			// request.setAttribute("error_code", "Error_timeout");
			return mapping.findForward("msgpage");
		}
	}

}
