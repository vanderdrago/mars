package com.muamalat.reportmcb.forward;

import java.util.List;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Admin;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.SqlFunction;

public class LapSpvTellerfwd extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    private static String SUCCESS = "success";
    private static Logger log = Logger.getLogger(LapSpvTellerfwd.class);

    /** Action called on save button click
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
	        int validLevel = 5;
	        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
	            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
	            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
	            return mapping.findForward("loginpage");
	        }
	        List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
	        SqlFunction sql = new SqlFunction();
	        if (sql.getMenuStatus(lrole, "LapSpvTeller.do")){
	        	return mapping.findForward(SUCCESS);
			 } else {
				 	request.setAttribute("message", "Unauthorized Access!");
					request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
					//request.setAttribute("error_code", "Error_timeout");
					return mapping.findForward("msgpage");
			 }
    }
}
