package com.muamalat.reportmcb.forward;


import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import WSInvoker.FCUBSAccServiceInv;
import WSInvoker.Util.DB.RetVal.AccInfoRetVal;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.DataFunction;
import com.muamalat.reportmcb.function.SqlFunction;
import com.muamalat.singleton.DatasourceEntry;
import com.ofss.fcubs.gw.ws.types.FCUBSAccService;

public class RtgsIntfOutFwd extends org.apache.struts.action.Action {

	private static Logger log = Logger.getLogger(RtgsIntfOutFwd.class);

	/* forward name="success" path="" */
	private static final String SUCCESS = "success";

	/**
	 * This is the action called from the Struts framework.
	 * 
	 * @param mapping
	 *            The ActionMapping used to select this instance.
	 * @param form
	 *            The optional ActionForm bean for this request.
	 * @param request
	 *            The HTTP Request we are processing.
	 * @param response
	 *            The HTTP Response we are processing.
	 * @throws java.lang.Exception
	 * @return
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL,validLevel);
			request.setAttribute("message","Login Session Expired, silahkan login kembali!");
			return mapping.findForward("loginpage");
		}
		List lrole = (List) request.getSession(true).getAttribute(StaticParameter.MENU_STATUS);
		SqlFunction sql = new SqlFunction();
		List lmmbrbnk = new DataFunction().getMemberBank("RTGS");
		request.setAttribute("lmmbrbnk", lmmbrbnk);
		if (sql.getMenuStatus(lrole, "rtgsIntfOut.do")) {
			return mapping.findForward(SUCCESS);
			/*
			Connection conn = null;
			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			FCUBSAccServiceInv wsAcc = new FCUBSAccServiceInv("10.55.60.67", "7001", conn);
			AccInfoRetVal acc = wsAcc.getCIF("3810002477");
			System.out.println(acc.getAcct().getAccName());
			System.out.println(acc.getAcct().getJalan());
			System.out.println(acc.getAcct().getNPWP());
			System.out.println(acc.getAcct().getEmail());
			System.out.println(acc.getAcct().getIbuKandung());
			if (conn != null){
				try {
					conn.close();
					conn = null;
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			*/
		} else {
			request.setAttribute("message", "Unauthorized Access!");
			request.setAttribute("message_error","Anda tidak memiliki akses ke halaman yang diminta, Silahkan Login kembali!");
			// request.setAttribute("error_code", "Error_timeout");
			return mapping.findForward("msgpage");
		}
	}

}
