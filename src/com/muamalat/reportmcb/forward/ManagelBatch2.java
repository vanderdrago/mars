package com.muamalat.reportmcb.forward;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Saldo;
import com.muamalat.reportmcb.function.SqlFunction;

public class ManagelBatch2 extends org.apache.struts.action.Action {
	
	 private static Logger log = Logger.getLogger(ManagelBatch2.class);

	    /* forward name="success" path="" */
	    private static final String SUCCESS = "success";

	    /**
	     * This is the action called from the Struts framework.
	     * @param mapping The ActionMapping used to select this instance.
	     * @param form The optional ActionForm bean for this request.
	     * @param request The HTTP Request we are processing.
	     * @param response The HTTP Response we are processing.
	     * @throws java.lang.Exception
	     * @return
	     */
	    @Override
	    public ActionForward execute(ActionMapping mapping, ActionForm form,
	            HttpServletRequest request, HttpServletResponse response)
	            throws Exception {
	        int level;
	        int validLevel = 5;
	        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
	            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
	            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
	            return mapping.findForward("loginpage");
	        }
	        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
//	        if (level != validLevel) {
//	            request.setAttribute("message", "Unauthorized Access!");
//	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
//	            //request.setAttribute("error_code", "Error_timeout");
//	            return mapping.findForward("msgpage");
//	        }
			 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			        
//				 List lBatchM = (List) request.getSession(true).getAttribute("lBatchM");
			        SqlFunction f = new SqlFunction();
			        
			        String batch_no = (String)request.getSession(true).getAttribute("batch_no");
			        String userId= (String) request.getSession(true).getAttribute("userId");
			        String kdcab = (String) request.getSession(true).getAttribute("kdcab");			        
			        List DetailBatch = f.getDetailBatchh(batch_no, userId, kdcab, "");
			        if(DetailBatch.size() > 0){
			        	request.setAttribute("lbd", DetailBatch);
			        	request.setAttribute("batch_no", batch_no);
			            request.getSession(true).setAttribute("lbd", DetailBatch);
			        }
			       			      
			        request.setAttribute("lBatchM", DetailBatch);
			      			       
			        return mapping.findForward(SUCCESS);
		        } else {
		            request.setAttribute("message", "Unauthorized Access!");
		            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
		            //request.setAttribute("error_code", "Error_timeout");
		            return mapping.findForward("msgpage");
		        }
	    }


}
