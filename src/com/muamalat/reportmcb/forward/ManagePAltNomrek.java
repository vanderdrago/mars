package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.MonSqlFunction;
import com.muamalat.reportmcb.function.salamMuamalatSql;

public class ManagePAltNomrek extends org.apache.struts.action.Action {
	
	 private static Logger log = Logger.getLogger(ManagePAltNomrek.class);

	    /* forward name="success" path="" */
	    private static final String SUCCESS = "success";

	    /**
	     * This is the action called from the Struts framework.
	     * @param mapping The ActionMapping used to select this instance.
	     * @param form The optional ActionForm bean for this request.
	     * @param request The HTTP Request we are processing.
	     * @param response The HTTP Response we are processing.
	     * @throws java.lang.Exception
	     * @return
	     */
	    @Override
	    public ActionForward execute(ActionMapping mapping, ActionForm form,
	            HttpServletRequest request, HttpServletResponse response)
	            throws Exception {
	        int level;
	        int validLevel = 5;
	        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
	            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
	            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
	            return mapping.findForward("loginpage");
	        }
	        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
			if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {			      
//			        List alt =  (List) request.getSession(true).getAttribute("alt");			       
//			        request.setAttribute("alt", alt);
				 prosesBb(request);
			     return mapping.findForward(SUCCESS);
		    } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            return mapping.findForward("msgpage");
	        }
	    }

		private void prosesBb(HttpServletRequest request) {
			List alt = new ArrayList();
			MonSqlFunction sql = new MonSqlFunction();
			String accno = "";
			String name = "";
			
			int sawal = 0;
			int sakhir = 0;
			int recordsPerPage = 30;
			
			accno = (String) request.getSession(true).getAttribute("accno");
			name = (String) request.getSession(true).getAttribute("name");
			
			int page = Integer.parseInt(request.getParameter("page"));
			int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("recordsAlt").toString());
			int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPagesAlt").toString());
			if (page == 1){
				alt = sql.getListAltacc(accno, name.toUpperCase(), sawal);
				sawal = page;
				if (noOfPages < 10) {
					sakhir = noOfPages;
				} else {
					sakhir = 10;
				}
			} else {
				sawal = page;
				int tempsakhir = 10 + (page -1);
				if (noOfPages < tempsakhir){
					sakhir = noOfPages;
	        	} else {
	        		sakhir = tempsakhir;
	        	}
				alt = sql.getListAltacc(accno, name.toUpperCase(), (((page-1) * recordsPerPage) - 1));
			}
			request.setAttribute("sawalAlt", sawal);
	        request.setAttribute("sakhirAlt", sakhir);
	        request.setAttribute("alt", alt);
	        request.setAttribute("noOfRecordsAlt", noOfRecords);
	        request.setAttribute("noOfPagesAlt", noOfPages);
	        request.setAttribute("currentPageAlt", page);
		}
}
