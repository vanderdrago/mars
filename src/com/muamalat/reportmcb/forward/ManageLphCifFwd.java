package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.SqlFunction;

public class ManageLphCifFwd extends org.apache.struts.action.Action {
	private static String SUCCESS = "success";
	private static Logger log = Logger.getLogger(ManageLphCifFwd.class);
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }
        
        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			proseBb(request);
		    return mapping.findForward(SUCCESS);
	    } else {
            request.setAttribute("message", "Unauthorized Access!");
            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
            return mapping.findForward("msgpage");
	    }
	}
	
	private void proseBb(HttpServletRequest request) {
		List lphLapCif = new ArrayList();
		SqlFunction sql = new SqlFunction();
		String cif = "";
		String branch = "";
		int sawal = 0;
		int sakhir = 0;
		int recordsPerPage = 30;
		
		cif = (String) request.getSession(true).getAttribute("cif");
		branch = (String) request.getSession(true).getAttribute("branch");
		int page = Integer.parseInt(request.getParameter("page"));
		int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("recordsCif").toString());
		int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPagesCif").toString());
		
		if (page == 1){
			lphLapCif = sql.getLapPerubahanCif(cif,branch, 1, 30);
			sawal = page;
			if (noOfPages < 10) {
				sakhir = noOfPages;
			} else {
				sakhir = 10;
			} 
		} else {
			sawal = page;
			int tempsakhir = 10 + (page -1);
			if (noOfPages < tempsakhir){
				sakhir = noOfPages;
        	} else {
        		sakhir = tempsakhir;
        	}
			lphLapCif = sql.getLapPerubahanCif(cif,branch, ((page -1) * recordsPerPage) + 1, page * recordsPerPage);
		}
		request.setAttribute("sawalCif", sawal);
        request.setAttribute("sakhirCif", sakhir);
        request.setAttribute("lphLapCif", lphLapCif);
        request.setAttribute("noOfRecordsCif", noOfRecords);
        request.setAttribute("noOfPagesCif", noOfPages);
        request.setAttribute("currentPageCif", page);

	}
	
}
