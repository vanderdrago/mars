/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muamalat.reportmcb.forward;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.function.FinSqlFunction;
import com.muamalat.reportmcb.function.MonSqlFunction;

/**
 *
 * @author Utis
 */
public class ManageGetEventPgg extends org.apache.struts.action.Action {

    private static Logger log = Logger.getLogger(ManageGetEventPgg.class);

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";


    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
//		         List lbb = (List) request.getSession(true).getAttribute("ltrn");
//		         request.setAttribute("ltrn", lbb);
			 	prosesBb(request);
		        return mapping.findForward(SUCCESS);
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return mapping.findForward("msgpage");
	        }
    }

	private void prosesBb(HttpServletRequest request) {
		List ltrn = new ArrayList();
		FinSqlFunction sql = new FinSqlFunction();
		String accno = "";
		
		int sawal = 0;
		int sakhir = 0;
		int recordsPerPage = 30;
		
		accno = (String) request.getSession(true).getAttribute("accno");
		
		int page = Integer.parseInt(request.getParameter("page"));
		int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("recordsEvent").toString());
		int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPagesEvent").toString());
		if (page == 1){
			ltrn = sql.getevent(accno, 1,30);
			sawal = page;
			if (noOfPages < 10) {
				sakhir = noOfPages;
			} else {
				sakhir = 10;
			}
		} else {
			sawal = page;
			int tempsakhir = 10 + (page -1);
			if (noOfPages < tempsakhir){
				sakhir = noOfPages;
        	} else {
        		sakhir = tempsakhir;
        	}
			ltrn = sql.getevent(accno, ((page -1) * recordsPerPage) + 1, page * recordsPerPage);
		}
		request.setAttribute("sawalEvent", sawal);
        request.setAttribute("sakhirEvent", sakhir);
        request.setAttribute("ltrn", ltrn);
        request.setAttribute("noOfRecordsEvent", noOfRecords);
        request.setAttribute("noOfPagesEvent", noOfPages);
        request.setAttribute("currentPageEvent", page);
	}
}
