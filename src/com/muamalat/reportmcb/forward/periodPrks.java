package com.muamalat.reportmcb.forward;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;

public class periodPrks extends org.apache.struts.action.Action{

	private static String SUCCESS = "success";
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int level;
		int validLevel = 5;
		if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
			request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
			request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
		}
		
		level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			String acc = (String) request.getParameter("accNo");
			String brn = (String) request.getParameter("brn");
			String nama = (String) request.getParameter("custName");
			System.out.println("acc:" + acc + "brn:" + brn + "cust:" + nama);
			request.setAttribute(acc, "accNo");
			request.setAttribute(brn, "brn");
			request.setAttribute(nama, "nama");
			request.getSession(true).setAttribute("accNo", acc);
			request.getSession(true).setAttribute("brn", brn);
			request.getSession(true).setAttribute("nama", nama);
			return mapping.findForward(SUCCESS);
		}else {
	   	 request.setAttribute("message", "Unauthorized Access!");
	   	 request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	     return mapping.findForward("msgpage");
	   }
	}
}
