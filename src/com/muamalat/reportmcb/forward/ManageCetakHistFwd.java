package com.muamalat.reportmcb.forward;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.muamalat.parameter.StaticParameter;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.perantara.PerantaraRpt;

public class ManageCetakHistFwd extends org.apache.struts.action.Action {

    private static Logger log = Logger.getLogger(ManageCetakHistFwd.class);

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int level;
        int validLevel = 5;
        if (request.getSession(true).getAttribute(StaticParameter.SESSION_USER) == null) {
            request.getSession(true).setAttribute(StaticParameter.USER_LEVEL, validLevel);
            request.setAttribute("message", "Login Session Expired, silahkan login kembali!");
            return mapping.findForward("loginpage");
        }

        level = Integer.valueOf(request.getSession(true).getAttribute(StaticParameter.USER_LEVEL).toString());
		 if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6 || level == 7) {
			 	mngePg(request);
		        return mapping.findForward(SUCCESS);
	        } else {
	            request.setAttribute("message", "Unauthorized Access!");
	            request.setAttribute("message_error", "Anda tidak memiliki akses ke halaman yang diminta!");
	            //request.setAttribute("error_code", "Error_timeout");
	            return mapping.findForward("msgpage");
	        }
    }
    
    public void mngePg(HttpServletRequest request){
        int page = Integer.parseInt(request.getParameter("page"));
        int noOfRecords = Integer.parseInt(request.getSession(true).getAttribute("noOfRecords").toString());
        int noOfPages = Integer.parseInt(request.getSession(true).getAttribute("noOfPages").toString());
        String title = request.getSession(true).getAttribute("noOfRecords").toString();
        List ltrnprinthist = (List) request.getSession(true).getAttribute("ltrnprinthist");
        Reklist cust_acc = (Reklist) request.getSession(true).getAttribute("cust_acc");
    	
    	int sawal = 0;
        int sakhir = 10;
        int recordsPerPage = 30;

		PerantaraRpt p = new PerantaraRpt();
        List lctkhst = null;
        if (page == 1){
        	lctkhst = p.getAcc_lineHist(ltrnprinthist, 1, recordsPerPage);
        	sawal = page;
        	if (noOfPages < 10){
        		sakhir = noOfPages;
        	} else {
        		sakhir = 10;
        	}
        } else {        	
        	sawal = page;
        	int tempsakhir = 10 + (page-1);
        	if (noOfPages < tempsakhir){
        		sakhir = noOfPages;
        	} else {
        		sakhir = tempsakhir;
        	}
        	lctkhst = p.getSknInRepRetTtpn(ltrnprinthist, ((page - 1) * recordsPerPage) + 1, page * recordsPerPage);
        }
        
        request.setAttribute("sawal", sawal);
        request.setAttribute("sakhir", sakhir);
        request.setAttribute("lctkhst", lctkhst);
        request.setAttribute("noOfRecords", noOfRecords);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);
        request.setAttribute("data", "viewdata");
    }

}
