package com.muamalat.reportmcb.entity;

public class Gl {
	private String gl_code;
	private String gl_desc;
	
	public String getGl_code() {
		return gl_code;
	}
	public void setGl_code(String glCode) {
		gl_code = glCode;
	}
	public String getGl_desc() {
		return gl_desc;
	}
	public void setGl_desc(String glDesc) {
		gl_desc = glDesc;
	}
	
}
