package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class TRTGSmcbin {
	
	public String trn_dt;
	
	public String trn_ref_no;
	
	public String event;
	
	public String module;
	
	public String ac_branch;
	
	public String trn_code;
	
	public String batch_no;
	
	public String user_id;
	
	public String auth_id;
	
	public String auth_stat;
	
	public String ac_no;
	
	public String ac_desc;

	public String drcr_ind;
	
	public BigDecimal lcy_amount;
	
	public TRTGSmcbin() {
    }

	public String getTrn_dt() {
		return trn_dt;
	}

	public void setTrn_dt(String trnDt) {
		trn_dt = trnDt;
	}

	public String getTrn_ref_no() {
		return trn_ref_no;
	}

	public void setTrn_ref_no(String trnRefNo) {
		trn_ref_no = trnRefNo;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getAc_branch() {
		return ac_branch;
	}

	public void setAc_branch(String acBranch) {
		ac_branch = acBranch;
	}

	public String getTrn_code() {
		return trn_code;
	}

	public void setTrn_code(String trnCode) {
		trn_code = trnCode;
	}

	public String getBatch_no() {
		return batch_no;
	}

	public void setBatch_no(String batchNo) {
		batch_no = batchNo;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String userId) {
		user_id = userId;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String authId) {
		auth_id = authId;
	}

	public String getAuth_stat() {
		return auth_stat;
	}

	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}

	public String getAc_no() {
		return ac_no;
	}

	public void setAc_no(String acNo) {
		ac_no = acNo;
	}

	public String getAc_desc() {
		return ac_desc;
	}

	public void setAc_desc(String acDesc) {
		ac_desc = acDesc;
	}

	public String getDrcr_ind() {
		return drcr_ind;
	}

	public void setDrcr_ind(String drcrInd) {
		drcr_ind = drcrInd;
	}

	public BigDecimal getLcy_amount() {
		return lcy_amount;
	}

	public void setLcy_amount(BigDecimal lcyAmount) {
		lcy_amount = lcyAmount;
	}
	
}
