package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class RtgsTtpn {
	private Date trx_dt;
	private String branch_cd; 
	private String fr_acc; 
	private String name_from;
	private String to_acc; 
	private String class_acc; 
	private String to_member; 
	private String trn; 
	private String sender_ref_no; 
	private String nama_member;
	private BigDecimal amount; 
	private String sandi_usaha; 
	private String ta_name; 
	private String pay_detail; 
	private String rtgstmp_sts; 
	private String ori_name; 
	private String ultimate_benef_acc; 
	private String ultimate_benef_name; 
	private String maker_id;
	private String checker_id;
	private String insert_dt;
	private String tgl_oto; 
	private BigDecimal fee;
	private String filename;
	private String sts_data;
	private String sts_trns;
	private String err_cd_mcb;
	private String err_desc_mcb;
	
	
	public Date getTrx_dt() {
		return trx_dt;
	}
	public void setTrx_dt(Date trx_dt) {
		this.trx_dt = trx_dt;
	}
	public String getBranch_cd() {
		return branch_cd;
	}
	public void setBranch_cd(String branch_cd) {
		this.branch_cd = branch_cd;
	}
	public String getFr_acc() {
		return fr_acc;
	}
	public void setFr_acc(String fr_acc) {
		this.fr_acc = fr_acc;
	}
	public String getTo_acc() {
		return to_acc;
	}
	public void setTo_acc(String to_acc) {
		this.to_acc = to_acc;
	}
	public String getName_from() {
		return name_from;
	}
	public void setName_from(String name_from) {
		this.name_from = name_from;
	}
	public String getClass_acc() {
		return class_acc;
	}
	public void setClass_acc(String class_acc) {
		this.class_acc = class_acc;
	}
	public String getTo_member() {
		return to_member;
	}
	public void setTo_member(String to_member) {
		this.to_member = to_member;
	}
	public String getNama_member() {
		return nama_member;
	}
	public void setNama_member(String nama_member) {
		this.nama_member = nama_member;
	}
	public String getSandi_usaha() {
		return sandi_usaha;
	}
	public void setSandi_usaha(String sandi_usaha) {
		this.sandi_usaha = sandi_usaha;
	}
	public String getTrn() {
		return trn;
	}
	public void setTrn(String trn) {
		this.trn = trn;
	}
	public String getSender_ref_no() {
		return sender_ref_no;
	}
	public void setSender_ref_no(String sender_ref_no) {
		this.sender_ref_no = sender_ref_no;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getTa_name() {
		return ta_name;
	}
	public void setTa_name(String ta_name) {
		this.ta_name = ta_name;
	}
	public String getPay_detail() {
		return pay_detail;
	}
	public void setPay_detail(String pay_detail) {
		this.pay_detail = pay_detail;
	}
	public String getRtgstmp_sts() {
		return rtgstmp_sts;
	}
	public void setRtgstmp_sts(String rtgstmp_sts) {
		this.rtgstmp_sts = rtgstmp_sts;
	}
	public String getOri_name() {
		return ori_name;
	}
	public void setOri_name(String ori_name) {
		this.ori_name = ori_name;
	}
	public String getUltimate_benef_acc() {
		return ultimate_benef_acc;
	}
	public void setUltimate_benef_acc(String ultimate_benef_acc) {
		this.ultimate_benef_acc = ultimate_benef_acc;
	}
	public String getUltimate_benef_name() {
		return ultimate_benef_name;
	}
	public void setUltimate_benef_name(String ultimate_benef_name) {
		this.ultimate_benef_name = ultimate_benef_name;
	}
	public String getMaker_id() {
		return maker_id;
	}
	public void setMaker_id(String maker_id) {
		this.maker_id = maker_id;
	}
	public String getChecker_id() {
		return checker_id;
	}
	public void setChecker_id(String checker_id) {
		this.checker_id = checker_id;
	}
	public String getInsert_dt() {
		return insert_dt;
	}
	public void setInsert_dt(String insert_dt) {
		this.insert_dt = insert_dt;
	}
	public String getTgl_oto() {
		return tgl_oto;
	}
	public void setTgl_oto(String tgl_oto) {
		this.tgl_oto = tgl_oto;
	}
	public BigDecimal getFee() {
		return fee;
	}
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getSts_data() {
		return sts_data;
	}
	public void setSts_data(String sts_data) {
		this.sts_data = sts_data;
	}
	public String getSts_trns() {
		return sts_trns;
	}
	public void setSts_trns(String sts_trns) {
		this.sts_trns = sts_trns;
	}
	public String getErr_cd_mcb() {
		return err_cd_mcb;
	}
	public void setErr_cd_mcb(String err_cd_mcb) {
		this.err_cd_mcb = err_cd_mcb;
	}
	public String getErr_desc_mcb() {
		return err_desc_mcb;
	}
	public void setErr_desc_mcb(String err_desc_mcb) {
		this.err_desc_mcb = err_desc_mcb;
	}
	
	
}
