package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class E_stat_cust {
	private BigDecimal id;
	private String branch_code;
	private String branch_name;
	private String account;
	private String cust_name;
	private String cust_addrs;
	private String cust_type;
	private String cust_type_desc;
	private String record_stat;
	private String auth_stat;
	private String sex;
	private String ibu_kndng;
	private String dt_birth;
	private String cust_no;
	private String acc_class;
	private String acc_class_desc;
	private String s_text;
	private String user_email;
	private String email;
	private String email_cc;
	private String sts_proc;
	private String sts_proc_desc;
	private String user_input;
	private String user_aut;
	private String branch_reg;
	private String branch_reg_name;
	private Timestamp dt_input;
	private Timestamp dt_aut;
	private String sts_data;
	private String sts_data_desc;
	private boolean sts_send_email;
	private String option;
	private String tgl_input;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getCust_addrs() {
		return cust_addrs;
	}
	public void setCust_addrs(String cust_addrs) {
		this.cust_addrs = cust_addrs;
	}
	public String getCust_type() {
		return cust_type;
	}
	public void setCust_type(String cust_type) {
		this.cust_type = cust_type;
	}
	public String getCust_type_desc() {
		return cust_type_desc;
	}
	public void setCust_type_desc(String cust_type_desc) {
		this.cust_type_desc = cust_type_desc;
	}
	public String getRecord_stat() {
		return record_stat;
	}
	public void setRecord_stat(String record_stat) {
		this.record_stat = record_stat;
	}
	public String getAuth_stat() {
		return auth_stat;
	}
	public void setAuth_stat(String auth_stat) {
		this.auth_stat = auth_stat;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getIbu_kndng() {
		return ibu_kndng;
	}
	public void setIbu_kndng(String ibu_kndng) {
		this.ibu_kndng = ibu_kndng;
	}
	public String getDt_birth() {
		return dt_birth;
	}
	public void setDt_birth(String dt_birth) {
		this.dt_birth = dt_birth;
	}
	public String getAcc_class() {
		return acc_class;
	}
	public void setAcc_class(String acc_class) {
		this.acc_class = acc_class;
	}
	public String getAcc_class_desc() {
		return acc_class_desc;
	}
	public void setAcc_class_desc(String acc_class_desc) {
		this.acc_class_desc = acc_class_desc;
	}
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}
	public String getS_text() {
		return s_text;
	}
	public void setS_text(String s_text) {
		this.s_text = s_text;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail_cc() {
		return email_cc;
	}
	public void setEmail_cc(String email_cc) {
		this.email_cc = email_cc;
	}
	public String getSts_proc() {
		return sts_proc;
	}
	public void setSts_proc(String sts_proc) {
		this.sts_proc = sts_proc;
	}
	public String getSts_proc_desc() {
		return sts_proc_desc;
	}
	public void setSts_proc_desc(String sts_proc_desc) {
		this.sts_proc_desc = sts_proc_desc;
	}
	public String getUser_input() {
		return user_input;
	}
	public void setUser_input(String user_input) {
		this.user_input = user_input;
	}
	public String getUser_aut() {
		return user_aut;
	}
	public void setUser_aut(String user_aut) {
		this.user_aut = user_aut;
	}
	public String getBranch_reg() {
		return branch_reg;
	}
	public void setBranch_reg(String branch_reg) {
		this.branch_reg = branch_reg;
	}
	public String getBranch_reg_name() {
		return branch_reg_name;
	}
	public void setBranch_reg_name(String branch_reg_name) {
		this.branch_reg_name = branch_reg_name;
	}
	public Timestamp getDt_input() {
		return dt_input;
	}
	public void setDt_input(Timestamp dt_input) {
		this.dt_input = dt_input;
	}
	public Timestamp getDt_aut() {
		return dt_aut;
	}
	public void setDt_aut(Timestamp dt_aut) {
		this.dt_aut = dt_aut;
	}
	public String getSts_data() {
		return sts_data;
	}
	public void setSts_data(String sts_data) {
		this.sts_data = sts_data;
	}
	public String getSts_data_desc() {
		return sts_data_desc;
	}
	public void setSts_data_desc(String sts_data_desc) {
		this.sts_data_desc = sts_data_desc;
	}
	public boolean isSts_send_email() {
		return sts_send_email;
	}
	public void setSts_send_email(boolean sts_send_email) {
		this.sts_send_email = sts_send_email;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	public String getTgl_input() {
		return tgl_input;
	}
	public void setTgl_input(String tgl_input) {
		this.tgl_input = tgl_input;
	}
	
}
