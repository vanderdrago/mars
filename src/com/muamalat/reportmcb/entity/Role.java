package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Role {
	private String role_id;
	private String group_id;
	private String func_id;
	private String func_nm;
	private String func_act;
	private String hdr_nm;
	private String desc_func;
	private String hdr1;
	private String hdr2;
	private String menusub_name;
	private String menusub_id;
	private String menusub_hdrId;
	
	public String getMenusub_hdrId() {
		return menusub_hdrId;
	}
	public void setMenusub_hdrId(String menusub_hdrId) {
		this.menusub_hdrId = menusub_hdrId;
	}
	public String getMenusub_id() {
		return menusub_id;
	}
	public void setMenusub_id(String menusub_id) {
		this.menusub_id = menusub_id;
	}
	public String getMenusub_name() {
		return menusub_name;
	}
	public void setMenusub_name(String menusub_name) {
		this.menusub_name = menusub_name;
	}
	public String getHdr1() {
		return hdr1;
	}
	public void setHdr1(String hdr1) {
		this.hdr1 = hdr1;
	}
	public String getHdr2() {
		return hdr2;
	}
	public void setHdr2(String hdr2) {
		this.hdr2 = hdr2;
	}
	
	public String getDesc_func() {
		return desc_func;
	}
	public void setDesc_func(String desc_func) {
		this.desc_func = desc_func;
	}
	public String getHdr_nm() {
		return hdr_nm;
	}
	public void setHdr_nm(String hdr_nm) {
		this.hdr_nm = hdr_nm;
	}
	public String getRole_id() {
		return role_id;
	}
	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	public String getFunc_id() {
		return func_id;
	}
	public void setFunc_id(String func_id) {
		this.func_id = func_id;
	}
	public String getFunc_nm() {
		return func_nm;
	}
	public void setFunc_nm(String func_nm) {
		this.func_nm = func_nm;
	}
	public String getFunc_act() {
		return func_act;
	}
	public void setFunc_act(String func_act) {
		this.func_act = func_act;
	}
}
