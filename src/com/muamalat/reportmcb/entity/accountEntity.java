package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.util.Date;

public class accountEntity {

	private String noCif;
	private String noRek;
	private String nama;
	private String accountClass;
	private String ccy;
	private String drcr;
	private String trnCode;
	private String maturityDate;
	private String nextmaturityDate;
	private BigDecimal lcyAmount;
	
	
	private String branchCode;
	private String branchName;
	private String CustomerType;
	private String AcOpenDate;
	private String CifCreationDate;
	private String RecordStat;
	
	public String getAcOpenDate() {
		return AcOpenDate;
	}
	public void setAcOpenDate(String acOpenDate) {
		AcOpenDate = acOpenDate;
	}
	public String getCifCreationDate() {
		return CifCreationDate;
	}
	public void setCifCreationDate(String cifCreationDate) {
		CifCreationDate = cifCreationDate;
	}
	public String getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	public String getNextmaturityDate() {
		return nextmaturityDate;
	}
	public void setNextmaturityDate(String nextmaturityDate) {
		this.nextmaturityDate = nextmaturityDate;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getCustomerType() {
		return CustomerType;
	}
	public void setCustomerType(String customerType) {
		CustomerType = customerType;
	}
	
	public String getRecordStat() {
		return RecordStat;
	}
	public void setRecordStat(String recordStat) {
		RecordStat = recordStat;
	}
	public String getNoCif() {
		return noCif;
	}
	public void setNoCif(String noCif) {
		this.noCif = noCif;
	}
	public String getNoRek() {
		return noRek;
	}
	public void setNoRek(String noRek) {
		this.noRek = noRek;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getAccountClass() {
		return accountClass;
	}
	public void setAccountClass(String accountClass) {
		this.accountClass = accountClass;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getDrcr() {
		return drcr;
	}
	public void setDrcr(String drcr) {
		this.drcr = drcr;
	}
	public String getTrnCode() {
		return trnCode;
	}
	public void setTrnCode(String trnCode) {
		this.trnCode = trnCode;
	}
	
	public BigDecimal getLcyAmount() {
		return lcyAmount;
	}
	public void setLcyAmount(BigDecimal lcyAmount) {
		this.lcyAmount = lcyAmount;
	}
	
	
}
