package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class HistTransRek {
    private String trn_dt;
    private String val_dt;
    private String trn_code;
    private String desc;
    private String drcr;
    private String trn_ref_no; 
    private BigDecimal nominal;
    private BigDecimal saldo;
    private BigDecimal ekivalen;
    private String auth_stat;
    private String instrument_code;
    private String account;
    private String txn_dt_time;
    private String jam;
    private String ac_ccy;
    
	public String getAc_ccy() {
		return ac_ccy;
	}
	public void setAc_ccy(String ac_ccy) {
		this.ac_ccy = ac_ccy;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getTxn_dt_time() {
		return txn_dt_time;
	}
	public void setTxn_dt_time(String txn_dt_time) {
		this.txn_dt_time = txn_dt_time;
	}
	public String getTrn_dt() {
		return trn_dt;
	}
	public void setTrn_dt(String trnDt) {
		trn_dt = trnDt;
	}
	public String getVal_dt() {
		return val_dt;
	}
	public void setVal_dt(String valDt) {
		val_dt = valDt;
	}
	public String getTrn_code() {
		return trn_code;
	}
	public void setTrn_code(String trnCode) {
		trn_code = trnCode;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getDrcr() {
		return drcr;
	}
	public void setDrcr(String drcr) {
		this.drcr = drcr;
	}
	public BigDecimal getNominal() {
		return nominal;
	}
	public void setNominal(BigDecimal nominal) {
		if(nominal == null){
			this.nominal = new BigDecimal(0);
		}else {
			this.nominal = nominal;
		}
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		if(saldo == null){
			this.saldo = new BigDecimal(0);
		}else {
			this.saldo = saldo;
		}
	}
	public String getAuth_stat() {
		return auth_stat;
	}
	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}
	public String getTrn_ref_no() {
		return trn_ref_no;
	}
	public void setTrn_ref_no(String trnRefNo) {
		trn_ref_no = trnRefNo;
	}
	public BigDecimal getEkivalen() {
		return ekivalen;
	}
	public void setEkivalen(BigDecimal ekivalen) {
		this.ekivalen = ekivalen;
	}
	public String getInstrument_code() {
		return instrument_code;
	}
	public void setInstrument_code(String instrument_code) {
		this.instrument_code = instrument_code;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}    
    
}
