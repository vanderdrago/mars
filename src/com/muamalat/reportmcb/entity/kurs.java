package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class kurs {

	private String fcyAmount;
	private String lcyAmount;
	private String rateType;
	private BigDecimal buySpread;
	private BigDecimal saleSpread;
	private BigDecimal buyRate;
	private BigDecimal saleRate;
	private String rateDate;
	private String branch;
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getFcyAmount() {
		return fcyAmount;
	}
	public void setFcyAmount(String fcyAmount) {
		this.fcyAmount = fcyAmount;
	}
	public String getLcyAmount() {
		return lcyAmount;
	}
	public void setLcyAmount(String lcyAmount) {
		this.lcyAmount = lcyAmount;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public BigDecimal getBuySpread() {
		return buySpread;
	}
	public void setBuySpread(BigDecimal buySpread) {
		this.buySpread = buySpread;
	}
	public BigDecimal getSaleSpread() {
		return saleSpread;
	}
	public void setSaleSpread(BigDecimal saleSpread) {
		this.saleSpread = saleSpread;
	}
	public BigDecimal getBuyRate() {
		return buyRate;
	}
	public void setBuyRate(BigDecimal buyRate) {
		this.buyRate = buyRate;
	}
	public BigDecimal getSaleRate() {
		return saleRate;
	}
	public void setSaleRate(BigDecimal saleRate) {
		this.saleRate = saleRate;
	}
	public String getRateDate() {
		return rateDate;
	}
	public void setRateDate(String rateDate) {
		this.rateDate = rateDate;
	}
	
	
}
