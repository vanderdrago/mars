package com.muamalat.reportmcb.entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PCTB_CONTRACT_MASTER")
public class JoinTrnRTGSIBMaster implements Serializable {
	@Id
	@Column(name="CONTRACT_REF_NO")
	private String contractRefNo;

	@Column(name="PROD_REF_NO")
	private String prodRefNo;

	private String network;

	@Column(name="CUST_BANKCODE")
	private String custBankcode;

	@Column(name="CUST_AC_NO")
	private String custAcNo;
	
	@Column(name="CUST_NAME")
	private String custName;

	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;

	@Column(name="CPTY_AC_NO")
	private String cptyAcNo;

	@Column(name="CPTY_NAME")
	private String cptyName;

	@Column(name="BOOKING_DT")
	private Date bookingDt;

	@Column(name="EXCEPTION_QUEUE")
	private String exceptionQueue;

	@Column(name="MAKER_ID")
	private String makerId;

	@Column(name="CHECKER_ID")
	private String checkerId;

	@Column(name="DISPATCH_REF_NO")
	private String dispatchRefNo;

	@Column(name="UDF_2")
	private String udf_2;
	
	@Column(name="PayDetails")
	private String payDetails;
	
	private static final long serialVersionUID = 1L;

	@Column(name="ACCOUNT_NO")
	private String accountNo;

	private BigDecimal amount;

	@Column(name="BANK_ID")
	private String bankId;

	@Column(name="BANK_REF_NO")
	private String bankRefNo;

	private BigDecimal charge;

    @Temporal( TemporalType.DATE)
	@Column(name="DATE_TRX")
	private Date dateTrx;

	private String message;

	@Column(name="REF_ID")
	private String refId;

	@Column(name="RESPONSE_CODE")
	private String responseCode;

	private String status;

	@Column(name="TO_ACCOUNT_NO")
	private String toAccountNo;

	@Column(name="TO_BANK_NAME")
	private String toBankName;

	@Column(name="TO_NAME")
	private String toName;

	@Column(name="TRANSFER_TYPE")
	private String transferType;

	@Column(name="USER_ID")
	private String userId;
	
	@Column(name="REF_NO")
	private String refNo;
	
	@Column(name="NAME_E")
	private String nameE;
		
	public JoinTrnRTGSIBMaster() {
		super();
	}

	public String getContractRefNo() {
		return contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public String getProdRefNo() {
		return prodRefNo;
	}

	public void setProdRefNo(String prodRefNo) {
		this.prodRefNo = prodRefNo;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getCustBankcode() {
		return custBankcode;
	}

	public void setCustBankcode(String custBankcode) {
		this.custBankcode = custBankcode;
	}

	public String getCustAcNo() {
		return custAcNo;
	}

	public void setCustAcNo(String custAcNo) {
		this.custAcNo = custAcNo;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getCptyAcNo() {
		return cptyAcNo;
	}

	public void setCptyAcNo(String cptyAcNo) {
		this.cptyAcNo = cptyAcNo;
	}

	public String getCptyName() {
		return cptyName;
	}

	public void setCptyName(String cptyName) {
		this.cptyName = cptyName;
	}

	public Date getBookingDt() {
		return bookingDt;
	}

	public void setBookingDt(Date bookingDt) {
		this.bookingDt = bookingDt;
	}

	public String getExceptionQueue() {
		return exceptionQueue;
	}

	public void setExceptionQueue(String exceptionQueue) {
		this.exceptionQueue = exceptionQueue;
	}

	public String getMakerId() {
		return makerId;
	}

	public void setMakerId(String makerId) {
		this.makerId = makerId;
	}

	public String getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(String checkerId) {
		this.checkerId = checkerId;
	}

	public String getDispatchRefNo() {
		return dispatchRefNo;
	}

	public void setDispatchRefNo(String dispatchRefNo) {
		this.dispatchRefNo = dispatchRefNo;
	}

	public String getUdf_2() {
		return udf_2;
	}

	public void setUdf_2(String udf_2) {
		this.udf_2 = udf_2;
	}
	
	public String getPayDetails() {
		return payDetails;
	}

	public void setPayDetails(String payDetails) {
		this.payDetails = payDetails;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankRefNo() {
		return bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public BigDecimal getCharge() {
		return charge;
	}

	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}

	public Date getDateTrx() {
		return dateTrx;
	}

	public void setDateTrx(Date dateTrx) {
		this.dateTrx = dateTrx;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getToAccountNo() {
		return toAccountNo;
	}

	public void setToAccountNo(String toAccountNo) {
		this.toAccountNo = toAccountNo;
	}

	public String getToBankName() {
		return toBankName;
	}

	public void setToBankName(String toBankName) {
		this.toBankName = toBankName;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getNameE() {
		return nameE;
	}

	public void setNameE(String nameE) {
		this.nameE = nameE;
	}

}
