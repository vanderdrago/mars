package com.muamalat.reportmcb.entity;

public class banking {
	
	private String noHandphone;
	private String registrasi;
	private String acc;
	private String userName;
	private String userStatus;
	
	public String getRegistrasi() {
		return registrasi;
	}

	public void setRegistrasi(String registrasi) {
		this.registrasi = registrasi;
	}

	public String getAcc() {
		return acc;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getNoHandphone() {
		return noHandphone;
	}

	public void setNoHandphone(String noHandphone) {
		this.noHandphone = noHandphone;
	}
	
	

}
