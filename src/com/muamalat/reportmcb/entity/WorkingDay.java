/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.entity;

import java.sql.Date;

/**
 *
 * @author Utis
 */
public class WorkingDay {
    private Date today;
    private Date prev_wk;
    private Date next_wk;

    public Date getNext_wk() {
        return next_wk;
    }

    public void setNext_wk(Date next_wk) {
        this.next_wk = next_wk;
    }

    public Date getPrev_wk() {
        return prev_wk;
    }

    public void setPrev_wk(Date prev_wk) {
        this.prev_wk = prev_wk;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }
    
}
