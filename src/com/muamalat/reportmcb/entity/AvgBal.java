package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class AvgBal {
	
	private String acNo;
	private String accDesc;
	private String productCode;
	private String periodCode;
	private String ccy;
	private BigDecimal lcyBal;
	private BigDecimal acyBal;
	private String custNo;
	
	
	public BigDecimal getLcyBal() {
		return lcyBal;
	}
	public void setLcyBal(BigDecimal lcyBal) {
		this.lcyBal = lcyBal;
	}
	public BigDecimal getAcyBal() {
		return acyBal;
	}
	public void setAcyBal(BigDecimal acyBal) {
		this.acyBal = acyBal;
	}
	public String getAcNo() {
		return acNo;
	}
	public void setAcNo(String acNo) {
		this.acNo = acNo;
	}
	public String getAccDesc() {
		return accDesc;
	}
	public void setAccDesc(String accDesc) {
		this.accDesc = accDesc;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getPeriodCode() {
		return periodCode;
	}
	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	
	

}
