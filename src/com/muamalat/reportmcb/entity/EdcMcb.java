package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class EdcMcb {
	
	public String branch_code;
	public String trn_ref_no;
	public String functionid;
	public String wfinitdate;
	public String makerid;
	public String checkerid;
	public String checkerdatestamp;
	public String txnactdet;
	public String txnccydet;
	public BigDecimal txnamtdet;
	public String checkerremarks;
	public String makerremarks;
	public String edc_verify_status;
	public String account_class;
	public String atm_facility;
	
	
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getTrn_ref_no() {
		return trn_ref_no;
	}
	public void setTrn_ref_no(String trn_ref_no) {
		this.trn_ref_no = trn_ref_no;
	}
	public String getFunctionid() {
		return functionid;
	}
	public void setFunctionid(String functionid) {
		this.functionid = functionid;
	}
	public String getWfinitdate() {
		return wfinitdate;
	}
	public void setWfinitdate(String wfinitdate) {
		this.wfinitdate = wfinitdate;
	}
	public String getMakerid() {
		return makerid;
	}
	public void setMakerid(String makerid) {
		this.makerid = makerid;
	}
	public String getCheckerid() {
		return checkerid;
	}
	public void setCheckerid(String checkerid) {
		this.checkerid = checkerid;
	}
	public String getCheckerdatestamp() {
		return checkerdatestamp;
	}
	public void setCheckerdatestamp(String checkerdatestamp) {
		this.checkerdatestamp = checkerdatestamp;
	}
	public String getTxnactdet() {
		return txnactdet;
	}
	public void setTxnactdet(String txnactdet) {
		this.txnactdet = txnactdet;
	}
	public String getTxnccydet() {
		return txnccydet;
	}
	public void setTxnccydet(String txnccydet) {
		this.txnccydet = txnccydet;
	}
	public BigDecimal getTxnamtdet() {
		return txnamtdet;
	}
	public void setTxnamtdet(BigDecimal txnamtdet) {
		this.txnamtdet = txnamtdet;
	}
	public String getCheckerremarks() {
		return checkerremarks;
	}
	public void setCheckerremarks(String checkerremarks) {
		this.checkerremarks = checkerremarks;
	}
	public String getMakerremarks() {
		return makerremarks;
	}
	public void setMakerremarks(String makerremarks) {
		this.makerremarks = makerremarks;
	}
	public String getEdc_verify_status() {
		return edc_verify_status;
	}
	public void setEdc_verify_status(String edc_verify_status) {
		this.edc_verify_status = edc_verify_status;
	}
	public String getAccount_class() {
		return account_class;
	}
	public void setAccount_class(String account_class) {
		this.account_class = account_class;
	}
	public String getAtm_facility() {
		return atm_facility;
	}
	public void setAtm_facility(String atm_facility) {
		this.atm_facility = atm_facility;
	}
	
	
		

	
	
}
