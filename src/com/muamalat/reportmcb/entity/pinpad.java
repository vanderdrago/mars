package com.muamalat.reportmcb.entity;

public class pinpad {
	
	private String accNo;
	private int terminalId;
	private int merchantId;
	private String trnDt;
	private String trnType;
	private String desc;
	private String idMerc;
	private String custAcNo;
	private String trnRefNo;
	private String termId;
	private String branch;
	private String mercName;
	
	public String getMercName() {
		return mercName;
	}
	public void setMercName(String mercName) {
		this.mercName = mercName;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public String getTrnRefNo() {
		return trnRefNo;
	}
	public void setTrnRefNo(String trnRefNo) {
		this.trnRefNo = trnRefNo;
	}
	public String getCustAcNo() {
		return custAcNo;
	}
	public void setCustAcNo(String custAcNo) {
		this.custAcNo = custAcNo;
	}
	public String getIdMerc() {
		return idMerc;
	}
	public void setIdMerc(String idMerc) {
		this.idMerc = idMerc;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public int getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(int terminalId) {
		this.terminalId = terminalId;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}
	public String getTrnDt() {
		return trnDt;
	}
	public void setTrnDt(String trnDt) {
		this.trnDt = trnDt;
	}
	public String getTrnType() {
		return trnType;
	}
	public void setTrnType(String trnType) {
		this.trnType = trnType;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
