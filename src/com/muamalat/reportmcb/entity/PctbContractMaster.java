package com.muamalat.reportmcb.entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PCTB_CONTRACT_MASTER")
public class PctbContractMaster implements Serializable {
	@Id
	@Column(name="CONTRACT_REF_NO")
	private String contractRefNo;

	@Column(name="BRANCH_OF_INPUT")
	private String branchOfInput;

	@Column(name="BRANCH_CODE")
	private String branchCode;

	@Column(name="SOURCE_CODE")
	private String sourceCode;

	@Column(name="SOURCE_REF")
	private String sourceRef;

	@Column(name="INTERFACE_REF")
	private String interfaceRef;

	@Column(name="AC_ENTRY_REF_NO")
	private String acEntryRefNo;

	@Column(name="PROD_REF_NO")
	private String prodRefNo;

	@Column(name="THEIR_REF_NO")
	private String theirRefNo;

	@Column(name="CUSTOM_REF_NO")
	private String customRefNo;

	@Column(name="STATION_ID")
	private String stationId;

	private String network;

	@Column(name="BATCH_NO")
	private BigDecimal batchNo;

	@Column(name="CURR_NO")
	private BigDecimal currNo;

	@Column(name="PRODUCT_CATEGORY")
	private String productCategory;

	@Column(name="PRODUCT_CODE")
	private String productCode;

	@Column(name="PRODUCT_TYPE")
	private String productType;

	private BigDecimal priority;

	@Column(name="CUST_NO")
	private String custNo;

	@Column(name="CUST_BANKCODE")
	private String custBankcode;

	@Column(name="CUST_AC_LCF")
	private String custAcLcf;

	@Column(name="CUST_AC_BRN")
	private String custAcBrn;

	@Column(name="CUST_AC_NO")
	private String custAcNo;

	@Column(name="CUST_AC_CCY")
	private String custAcCcy;

	@Column(name="OLD_CUST_NO")
	private String oldCustNo;

	@Column(name="OLD_AC_BRN")
	private String oldAcBrn;

	@Column(name="OLD_AC_NO")
	private String oldAcNo;

	@Column(name="OLD_AC_CCY")
	private String oldAcCcy;

	@Column(name="FCY_AMOUNT")
	private BigDecimal fcyAmount;

	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;

	@Column(name="EXCH_RATE")
	private BigDecimal exchRate;

	@Column(name="CPTY_BANKCODE")
	private String cptyBankcode;

	@Column(name="OLD_BANKCODE")
	private String oldBankcode;

	@Column(name="CPTY_AC_NO")
	private String cptyAcNo;

	@Column(name="CPTY_NAME")
	private String cptyName;

	@Column(name="OLD_ACTIVATION_DT")
	private Date oldActivationDt;

	@Column(name="OLD_EXCH_RATE")
	private BigDecimal oldExchRate;

	@Column(name="CUTOFF_STAT")
	private String cutoffStat;

	@Column(name="BOOKING_DT")
	private Date bookingDt;

	@Column(name="INITIATION_DT")
	private Date initiationDt;

	@Column(name="ACTIVATION_DT")
	private Date activationDt;

	@Column(name="CUST_ENTRY_DT")
	private Date custEntryDt;

	@Column(name="CUST_ENTRY_VAL_DT")
	private Date custEntryValDt;

	@Column(name="CPTY_ENTRY_DT")
	private Date cptyEntryDt;

	@Column(name="CPTY_ENTRY_VAL_DT")
	private Date cptyEntryValDt;

	@Column(name="DISPATCH_DT")
	private Date dispatchDt;

	@Column(name="RECEIVE_DATE")
	private Date receiveDate;

	@Column(name="CONSOL_REQD")
	private String consolReqd;

	@Column(name="CONSOL_REF")
	private String consolRef;

	@Column(name="CREDIT_EXCPT")
	private String creditExcpt;

	@Column(name="GEN_ADVICE")
	private String genAdvice;

	private String dispatch;

	@Column(name="EXCEPTION_QUEUE")
	private String exceptionQueue;

	@Column(name="RELATED_TXN")
	private String relatedTxn;

	private String wip;

	@Column(name="LAST_EVENT_CODE")
	private String lastEventCode;

	@Column(name="NEXT_EVENT_CODE")
	private String nextEventCode;

	@Column(name="NEXT_EVENT_DATE")
	private Date nextEventDate;

	@Column(name="NEXT_EVENT_BRANCH")
	private String nextEventBranch;

	@Column(name="CONTRACT_STATUS")
	private String contractStatus;

	@Column(name="AUTH_STATUS")
	private String authStatus;

	@Column(name="AUTO_MANUAL_FLAG")
	private String autoManualFlag;

	@Column(name="DEFERRED_EVENTS")
	private String deferredEvents;

	@Column(name="AC_REDIRECT")
	private String acRedirect;

	@Column(name="BANK_REDIRECT")
	private String bankRedirect;

	@Column(name="ACTIVATION_DT_FWD")
	private String activationDtFwd;

	@Column(name="CREX_CRFWD")
	private String crexCrfwd;

	@Column(name="CONSOL_STATUS")
	private String consolStatus;

	@Column(name="DUP_RESOLUTION_LIST")
	private String dupResolutionList;

	@Column(name="ADV_PROCESSED")
	private String advProcessed;

	@Column(name="MAKER_ID")
	private String makerId;

	@Column(name="CHECKER_ID")
	private String checkerId;

	@Column(name="MAKER_DT_STAMP")
	private Date makerDtStamp;

	@Column(name="CHECKER_DT_STAMP")
	private Date checkerDtStamp;

	@Column(name="INIT_DT_STAMP")
	private Date initDtStamp;

	@Column(name="DCLG_DT_STAMP")
	private Date dclgDtStamp;

	@Column(name="DRLQ_DT_STAMP")
	private Date drlqDtStamp;

	@Column(name="CRLQ_DT_STAMP")
	private Date crlqDtStamp;

	@Column(name="DCLG_PROCESSED")
	private String dclgProcessed;

	@Column(name="DRAG_PROCESSED")
	private String dragProcessed;

	@Column(name="UDF_1")
	private String udf1;

	@Column(name="UDF_2")
	private String udf2;

	@Column(name="UDF_3")
	private String udf3;

	@Column(name="UDF_4")
	private String udf4;

	@Column(name="UDF_5")
	private String udf5;

	@Column(name="UDF_6")
	private String udf6;

	@Column(name="UDF_7")
	private String udf7;

	@Column(name="UDF_8")
	private String udf8;

	@Column(name="UDF_9")
	private String udf9;

	@Column(name="UDF_10")
	private String udf10;

	@Column(name="UDF_11")
	private String udf11;

	@Column(name="UDF_12")
	private String udf12;

	@Column(name="UDF_13")
	private String udf13;

	@Column(name="UDF_14")
	private String udf14;

	@Column(name="UDF_15")
	private String udf15;

	@Column(name="UDF_16")
	private String udf16;

	@Column(name="UDF_17")
	private String udf17;

	@Column(name="UDF_18")
	private String udf18;

	@Column(name="UDF_19")
	private String udf19;

	@Column(name="UDF_20")
	private String udf20;

	@Column(name="UDF_21")
	private String udf21;

	@Column(name="UDF_22")
	private String udf22;

	@Column(name="UDF_23")
	private String udf23;

	@Column(name="UDF_24")
	private String udf24;

	@Column(name="UDF_25")
	private String udf25;

	@Column(name="UDF_26")
	private String udf26;

	@Column(name="UDF_27")
	private String udf27;

	@Column(name="UDF_28")
	private String udf28;

	@Column(name="UDF_29")
	private String udf29;

	@Column(name="UDF_30")
	private String udf30;

	@Column(name="DISPATCH_REF_NO")
	private String dispatchRefNo;

	@Column(name="COLLECTION_TYPE")
	private String collectionType;

	@Column(name="RFD_TYPE")
	private String rfdType;

	@Column(name="COLLECTION_STATUS")
	private String collectionStatus;

	@Column(name="DEBTOR_CATEGORY")
	private String debtorCategory;

	@Column(name="INVOICE_SPLIT_REQD")
	private String invoiceSplitReqd;

	@Column(name="MAX_TXN_AMT")
	private BigDecimal maxTxnAmt;

	@Column(name="SPLIT_INDICATOR")
	private String splitIndicator;

	@Column(name="SPLIT_NUMBER")
	private BigDecimal splitNumber;

	@Column(name="SPLIT_PARENT_REF_NO")
	private String splitParentRefNo;

	@Column(name="AUTO_RESPONSE")
	private String autoResponse;

	@Column(name="RESPONSE_DATE")
	private Date responseDate;

	@Column(name="RESPONSE_ADVICE_REQD")
	private String responseAdviceReqd;

	@Column(name="RESPONSE_ADVICE_BASIS")
	private String responseAdviceBasis;

	@Column(name="RESPONSE_ADVICE_DATE")
	private Date responseAdviceDate;

	@Column(name="RESPONSE_ADVICE_SENT")
	private String responseAdviceSent;

	@Column(name="REDISPATCH_REQD")
	private String redispatchReqd;

	@Column(name="AUTO_REDISPATCH")
	private String autoRedispatch;

	@Column(name="MAX_REDISPATCH_NO")
	private BigDecimal maxRedispatchNo;

	@Column(name="REDISPATCH_INDICATOR")
	private String redispatchIndicator;

	@Column(name="REDISPATCH_DATE")
	private Date redispatchDate;

	@Column(name="REDISPATCH_NO")
	private BigDecimal redispatchNo;

	@Column(name="REDISPATCH_PARENT_REF_NO")
	private String redispatchParentRefNo;

	private String redispatched;

	@Column(name="MAX_RECALL_DATE")
	private Date maxRecallDate;

	@Column(name="APPR_DT_STAMP")
	private Date apprDtStamp;

	@Column(name="REJT_DT_STAMP")
	private Date rejtDtStamp;

	@Column(name="CLOS_DT_STAMP")
	private Date closDtStamp;

	@Column(name="RDSP_DT_STAMP")
	private Date rdspDtStamp;

	@Column(name="RECL_DT_STAMP")
	private Date reclDtStamp;

	@Column(name="XREF_DT_STAMP")
	private Date xrefDtStamp;

	@Column(name="RADV_DT_STAMP")
	private Date radvDtStamp;

	@Column(name="CREDITOR_ID")
	private String creditorId;

	@Column(name="AGREEMENT_ID")
	private String agreementId;

	@Column(name="REJECT_CODE")
	private String rejectCode;

	@Column(name="REJECT_DETAIL")
	private String rejectDetail;

	@Column(name="INTEREST_AMOUNT")
	private BigDecimal interestAmount;

	@Column(name="ORIGINAL_COLL_REF_NO")
	private String originalCollRefNo;

	@Column(name="COLLECTED_AMOUNT")
	private BigDecimal collectedAmount;

	@Column(name="CHARGE_WAIVER")
	private String chargeWaiver;

	@Column(name="CHARGE_AC_STATISTICS")
	private String chargeAcStatistics;

	@Column(name="CHARGE_CUST_STATISTICS")
	private String chargeCustStatistics;

	@Column(name="PREV_MTH_AC_TXN_COUNT")
	private BigDecimal prevMthAcTxnCount;

	@Column(name="PREV_MTH_AC_TXN_AMOUNT")
	private BigDecimal prevMthAcTxnAmount;

	@Column(name="PREV_MTH_CUST_TXN_COUNT")
	private BigDecimal prevMthCustTxnCount;

	@Column(name="PREV_MTH_CUST_TXN_AMOUNT")
	private BigDecimal prevMthCustTxnAmount;

	@Column(name="ASCII_HANDOFF_REQD")
	private String asciiHandoffReqd;

	@Column(name="REJECT_DUE_TO_FUNDS")
	private String rejectDueToFunds;

	@Column(name="XREF_PROCESSED")
	private String xrefProcessed;

	@Column(name="COLLECT_VOLUME_STATISTICS")
	private String collectVolumeStatistics;

	@Column(name="CHARGE_CATEGORY")
	private String chargeCategory;

	@Column(name="REDISPATCH_CHILD_REF_NO")
	private String redispatchChildRefNo;

	@Column(name="MAX_INT_AMT")
	private BigDecimal maxIntAmt;

	private String remarks;

	@Column(name="CUST_ADDRESS_1")
	private String custAddress1;

	@Column(name="CUST_ADDRESS_2")
	private String custAddress2;

	@Column(name="CUST_ADDRESS_3")
	private String custAddress3;

	@Column(name="CUST_ADDRESS_4")
	private String custAddress4;

	@Column(name="CPTY_ADDRESS_1")
	private String cptyAddress1;

	@Column(name="CPTY_ADDRESS_2")
	private String cptyAddress2;

	@Column(name="CPTY_ADDRESS_3")
	private String cptyAddress3;

	@Column(name="CPTY_ADDRESS_4")
	private String cptyAddress4;

	@Column(name="CUST_NAME")
	private String custName;

	@Column(name="CUST_AVLBL_BAL")
	private BigDecimal custAvlblBal;

	@Column(name="MSG_STATUS")
	private String msgStatus;

	@Column(name="LATEST_EVENT_SEQ_NO")
	private BigDecimal latestEventSeqNo;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	@Column(name="MISC_PROCESSED")
	private String miscProcessed;

	@Column(name="CLEARING_BRN")
	private String clearingBrn;

	@Column(name="AUTH_REJ_REMARKS")
	private String authRejRemarks;

	@Column(name="PREV_MSG_STATUS")
	private String prevMsgStatus;

	@Column(name="OUT_PMT_WORKFLOW")
	private String outPmtWorkflow;

	@Column(name="AUTO_DISPATCH")
	private String autoDispatch;

	@Column(name="DISP_FILE_NAME")
	private String dispFileName;

	@Column(name="CUST_REFERENCE")
	private String custReference;

	@Column(name="PAYMENT_DETAILS_1")
	private String paymentDetails1;

	@Column(name="PAYMENT_DETAILS_2")
	private String paymentDetails2;

	@Column(name="PAYMENT_DETAILS_3")
	private String paymentDetails3;

	@Column(name="PAYMENT_DETAILS_4")
	private String paymentDetails4;

	@Column(name="ACTUAL_AMOUNT")
	private BigDecimal actualAmount;

	@Column(name="CHARGE_MODE")
	private String chargeMode;

	@Column(name="CUST_ADDRESS_5")
	private String custAddress5;

	@Column(name="CPTY_ADDRESS_5")
	private String cptyAddress5;

	@Column(name="CUST_INFO_1")
	private String custInfo1;

	@Column(name="CUST_INFO_2")
	private String custInfo2;

	@Column(name="CUST_INFO_3")
	private String custInfo3;

	@Column(name="CUST_INFO_4")
	private String custInfo4;

	@Column(name="CPTY_INFO_1")
	private String cptyInfo1;

	@Column(name="CPTY_INFO_2")
	private String cptyInfo2;

	@Column(name="CPTY_INFO_3")
	private String cptyInfo3;

	@Column(name="CPTY_INFO_4")
	private String cptyInfo4;

	@Column(name="UPLOAD_STATUS")
	private String uploadStatus;

	@Column(name="DISPATCH_MEDIA")
	private String dispatchMedia;

	@Column(name="DISPATCH_AC_REF_NO")
	private String dispatchAcRefNo;

	@Column(name="XREF_ESN")
	private BigDecimal xrefEsn;

	@Column(name="XREF_AMOUNT_TAG")
	private String xrefAmountTag;

	@Column(name="POST_ACCOUNTING")
	private String postAccounting;

	@Column(name="DIRECT_PARTICIPANT")
	private String directParticipant;

	@Column(name="COVER_REQUIRED")
	private String coverRequired;

	@Column(name="ACK_STATUS")
	private String ackStatus;

	@Column(name="TXN_CCY")
	private String txnCcy;

	@Column(name="LCY_EQUIV_AMT")
	private BigDecimal lcyEquivAmt;

	@Column(name="FROM_EX_RATE_QUEUE")
	private String fromExRateQueue;

	@Column(name="SERV_LEVEL_CODE")
	private String servLevelCode;

	@Column(name="CHARGE_BEARER")
	private String chargeBearer;

	@Column(name="PYMT_REJECT_DATE")
	private Date pymtRejectDate;

	@Column(name="STLMNT_DATE")
	private Date stlmntDate;

	@Column(name="INSTRUCTION_DATE")
	private Date instructionDate;

	@Column(name="INITPARTY_NAME")
	private String initpartyName;

	@Column(name="INITPARTY_ADDRESS_1")
	private String initpartyAddress1;

	@Column(name="INITPARTY_ADDRESS_2")
	private String initpartyAddress2;

	@Column(name="INITPARTY_CTRY")
	private String initpartyCtry;

	@Column(name="INITPARTY_TYPE")
	private String initpartyType;

	@Column(name="INITPARTY_IDTYPE")
	private String initpartyIdtype;

	@Column(name="INITPARTY_IDVALUE")
	private String initpartyIdvalue;

	@Column(name="INITPARTY_OTHERID_TYPE")
	private String initpartyOtheridType;

	@Column(name="INITPARTY_IDISSUER")
	private String initpartyIdissuer;

	@Column(name="INITPARTY_DOBCITY")
	private String initpartyDobcity;

	@Column(name="INITPARTY_DOBCTRY")
	private String initpartyDobctry;

	@Column(name="CUST_CTRY")
	private String custCtry;

	@Column(name="CUST_TYPE")
	private String custType;

	@Column(name="CUST_IDTYPE")
	private String custIdtype;

	@Column(name="CUST_IDVALUE")
	private String custIdvalue;

	@Column(name="CUST_OTHERID_TYPE")
	private String custOtheridType;

	@Column(name="CUST_IDISSUER")
	private String custIdissuer;

	@Column(name="CUST_DOBCITY")
	private String custDobcity;

	@Column(name="CUST_DOBCTRY")
	private String custDobctry;

	@Column(name="DEBTOR_REFPARTY")
	private String debtorRefparty;

	@Column(name="CREDITOR_REFPARTY")
	private String creditorRefparty;

	@Column(name="CPTY_AC_CCY")
	private String cptyAcCcy;

	@Column(name="CPTY_CTRY")
	private String cptyCtry;

	@Column(name="CPTY_TYPE")
	private String cptyType;

	@Column(name="CPTY_IDTYPE")
	private String cptyIdtype;

	@Column(name="CPTY_IDVALUE")
	private String cptyIdvalue;

	@Column(name="CPTY_OTHERID_TYPE")
	private String cptyOtheridType;

	@Column(name="CPTY_IDISSUER")
	private String cptyIdissuer;

	@Column(name="CPTY_DOBCITY")
	private String cptyDobcity;

	@Column(name="CPTY_DOBCTRY")
	private String cptyDobctry;

	@Column(name="ORG_MSG_NAME")
	private String orgMsgName;

	@Column(name="ORG_MSG_REF_NO")
	private String orgMsgRefNo;

	@Column(name="REJT_ORG_NAME")
	private String rejtOrgName;

	@Column(name="REJT_ORG_BANK")
	private String rejtOrgBank;

	@Column(name="REJT_REF_NO")
	private String rejtRefNo;

	@Column(name="REJECT_CODE_ADD")
	private String rejectCodeAdd;

	@Column(name="ORG_SOURCE_REF")
	private String orgSourceRef;

	@Column(name="ORG_STLMNT_DT")
	private Date orgStlmntDt;

	@Column(name="ORG_STLMNT_AMT")
	private BigDecimal orgStlmntAmt;

	@Column(name="ORG_STLMNT_CCY")
	private String orgStlmntCcy;

	@Column(name="IN_FILE_REF_NO")
	private String inFileRefNo;

	@Column(name="IN_MSG_REF_NO")
	private String inMsgRefNo;

	@Column(name="IN_MSG_NAME")
	private String inMsgName;

	@Column(name="IN_MSG_CREATION_DT")
	private Date inMsgCreationDt;

	@Column(name="IN_INSTG_BANK")
	private String inInstgBank;

	@Column(name="IN_INSTD_BANK")
	private String inInstdBank;

	@Column(name="IN_STLMNT_METHOD")
	private String inStlmntMethod;

	@Column(name="IN_CLRG_SYS_ID")
	private String inClrgSysId;

	@Column(name="IPCUST_NAME")
	private String ipcustName;

	@Column(name="IPCUST_ADDRESS_1")
	private String ipcustAddress1;

	@Column(name="IPCUST_ADDRESS_2")
	private String ipcustAddress2;

	@Column(name="IPCUST_CTRY")
	private String ipcustCtry;

	@Column(name="IPCUST_TYPE")
	private String ipcustType;

	@Column(name="IPCUST_IDTYPE")
	private String ipcustIdtype;

	@Column(name="IPCUST_IDVALUE")
	private String ipcustIdvalue;

	@Column(name="IPCUST_OTHERID_TYPE")
	private String ipcustOtheridType;

	@Column(name="IPCUST_IDISSUER")
	private String ipcustIdissuer;

	@Column(name="IPCUST_DOBCITY")
	private String ipcustDobcity;

	@Column(name="IPCUST_DOBCTRY")
	private String ipcustDobctry;

	@Column(name="IPCUST_AC_NO")
	private String ipcustAcNo;

	@Column(name="IPCUST_AC_CCY")
	private String ipcustAcCcy;

	@Column(name="IPCUST_BANKCODE")
	private String ipcustBankcode;

	@Column(name="DD_SEQ_TYPE")
	private String ddSeqType;

	@Column(name="MANDATE_ID")
	private String mandateId;

	@Column(name="MAND_SIGN_DATE")
	private Date mandSignDate;

	@Column(name="CREDITOR_SCH_ID")
	private String creditorSchId;

	@Column(name="MAND_AMEND_IND")
	private String mandAmendInd;

	@Column(name="MAND_AMEND_TYPE")
	private String mandAmendType;

	@Column(name="ORG_MANDATE_ID")
	private String orgMandateId;

	@Column(name="ORG_CREDITOR_SCH_ID")
	private String orgCreditorSchId;

	@Column(name="ORG_CREDITOR_NAME")
	private String orgCreditorName;

	@Column(name="ORG_DEBTOR_ACC")
	private String orgDebtorAcc;

	@Column(name="ORG_DEBTOR_BANK")
	private String orgDebtorBank;

	@Column(name="OUT_FILE_REF_NO")
	private String outFileRefNo;

	@Column(name="OUT_MSG_REF_NO")
	private String outMsgRefNo;

	@Column(name="OUT_MSG_NAME")
	private String outMsgName;

	@Column(name="OUT_MSG_CREATION_DT")
	private Date outMsgCreationDt;

	@Column(name="SUBSYSTEM_STAT")
	private String subsystemStat;

	@Column(name="GEN_REMIT_SLIP_IN_INIT")
	private String genRemitSlipInInit;

	@Column(name="CHARGE_ACCOUNT")
	private String chargeAccount;

	@Column(name="CONSOL_DEBIT_REFERENCE_NO")
	private String consolDebitReferenceNo;

	@Column(name="ULT_DEBTOR_ID")
	private String ultDebtorId;

	@Column(name="ULT_DEBTOR_ID_TYPE")
	private String ultDebtorIdType;

	@Column(name="ULT_DEBTOR_ID_VALUE")
	private String ultDebtorIdValue;

	@Column(name="ULT_DEBTOR_OTHER_ID_TYPE")
	private String ultDebtorOtherIdType;

	@Column(name="ULT_DEBTOR_ID_ISSUER")
	private String ultDebtorIdIssuer;

	@Column(name="ULT_CREDITOR_ID")
	private String ultCreditorId;

	@Column(name="ULT_CREDITOR_ID_TYPE")
	private String ultCreditorIdType;

	@Column(name="ULT_CREDITOR_ID_VALUE")
	private String ultCreditorIdValue;

	@Column(name="ULT_CREDITOR_OTHER_ID_TYPE")
	private String ultCreditorOtherIdType;

	@Column(name="ULT_CREDITOR_ID_ISSUER")
	private String ultCreditorIdIssuer;

	@Column(name="CATEGORY_PURPOSE")
	private String categoryPurpose;

	@Column(name="PURPOSE_TYPE")
	private String purposeType;

	@Column(name="PURPOSE_VALUE")
	private String purposeValue;

	@Column(name="LOCAL_INSTRUMENT_TYPE")
	private String localInstrumentType;

	@Column(name="LOCAL_INSTRUMENT_VALUE")
	private String localInstrumentValue;

	@Column(name="COMP_CCY")
	private String compCcy;

	@Column(name="COMP_AMT")
	private BigDecimal compAmt;

	@Column(name="ELEC_SIG")
	private String elecSig;

	@Column(name="ULT_DEBTOR_BIRTH_CITY")
	private String ultDebtorBirthCity;

	@Column(name="ULT_DEBTOR_BIRTH_COUNTRY")
	private String ultDebtorBirthCountry;

	@Column(name="ULT_CREDITOR_BIRTH_CITY")
	private String ultCreditorBirthCity;

	@Column(name="ULT_CREDITOR_BIRTH_COUNTRY")
	private String ultCreditorBirthCountry;

	@Column(name="CREDITOR_SCH_TYPE")
	private String creditorSchType;

	@Column(name="CREDITOR_SCH_ID_VALUE")
	private String creditorSchIdValue;

	@Column(name="CREDITOR_SCH_ID_TYPE")
	private String creditorSchIdType;

	@Column(name="ORG_CREDITOR_SCH_TYPE")
	private String orgCreditorSchType;

	@Column(name="ORG_CREDITOR_SCH_ID_VALUE")
	private String orgCreditorSchIdValue;

	@Column(name="ORG_CREDITOR_SCH_ID_TYPE")
	private String orgCreditorSchIdType;

	@Column(name="CUST_ADDRESS_6")
	private String custAddress6;

	@Column(name="CPTY_ADDRESS_6")
	private String cptyAddress6;

	@Column(name="USER_REF_NO")
	private String userRefNo;

	@Column(name="JOB_NO")
	private BigDecimal jobNo;

	@Column(name="MSG_SEQ_NO")
	private String msgSeqNo;

	@Column(name="INSTRUMENT_NO")
	private String instrumentNo;

	@Column(name="INSTRUMENT_DT")
	private Date instrumentDt;

	@Column(name="AUTO_RELEASE")
	private String autoRelease;

	@Column(name="SNDR_RECV_7495")
	private String sndrRecv7495;

	@Column(name="SNDR_RECV_7495_1")
	private String sndrRecv74951;

	@Column(name="SNDR_RECV_7495_2")
	private String sndrRecv74952;

	@Column(name="SNDR_RECV_7495_3")
	private String sndrRecv74953;

	@Column(name="SNDR_RECV_7495_4")
	private String sndrRecv74954;

	@Column(name="SNDR_RECV_7495_5")
	private String sndrRecv74955;

	@Column(name="CUST_ACC_TYPE")
	private BigDecimal custAccType;

	@Column(name="DEST_MICR_CODE")
	private String destMicrCode;

	@Column(name="COMMUNICATION_MODE")
	private String communicationMode;

	@Column(name="MOBILENO_EMAILID")
	private String mobilenoEmailid;

	private static final long serialVersionUID = 1L;

	public PctbContractMaster() {
		super();
	}

	public String getContractRefNo() {
		return this.contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public String getBranchOfInput() {
		return this.branchOfInput;
	}

	public void setBranchOfInput(String branchOfInput) {
		this.branchOfInput = branchOfInput;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getSourceCode() {
		return this.sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSourceRef() {
		return this.sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	public String getInterfaceRef() {
		return this.interfaceRef;
	}

	public void setInterfaceRef(String interfaceRef) {
		this.interfaceRef = interfaceRef;
	}

	public String getAcEntryRefNo() {
		return this.acEntryRefNo;
	}

	public void setAcEntryRefNo(String acEntryRefNo) {
		this.acEntryRefNo = acEntryRefNo;
	}

	public String getProdRefNo() {
		return this.prodRefNo;
	}

	public void setProdRefNo(String prodRefNo) {
		this.prodRefNo = prodRefNo;
	}

	public String getTheirRefNo() {
		return this.theirRefNo;
	}

	public void setTheirRefNo(String theirRefNo) {
		this.theirRefNo = theirRefNo;
	}

	public String getCustomRefNo() {
		return this.customRefNo;
	}

	public void setCustomRefNo(String customRefNo) {
		this.customRefNo = customRefNo;
	}

	public String getStationId() {
		return this.stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getNetwork() {
		return this.network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public BigDecimal getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(BigDecimal batchNo) {
		this.batchNo = batchNo;
	}

	public BigDecimal getCurrNo() {
		return this.currNo;
	}

	public void setCurrNo(BigDecimal currNo) {
		this.currNo = currNo;
	}

	public String getProductCategory() {
		return this.productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public BigDecimal getPriority() {
		return this.priority;
	}

	public void setPriority(BigDecimal priority) {
		this.priority = priority;
	}

	public String getCustNo() {
		return this.custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

	public String getCustBankcode() {
		return this.custBankcode;
	}

	public void setCustBankcode(String custBankcode) {
		this.custBankcode = custBankcode;
	}

	public String getCustAcLcf() {
		return this.custAcLcf;
	}

	public void setCustAcLcf(String custAcLcf) {
		this.custAcLcf = custAcLcf;
	}

	public String getCustAcBrn() {
		return this.custAcBrn;
	}

	public void setCustAcBrn(String custAcBrn) {
		this.custAcBrn = custAcBrn;
	}

	public String getCustAcNo() {
		return this.custAcNo;
	}

	public void setCustAcNo(String custAcNo) {
		this.custAcNo = custAcNo;
	}

	public String getCustAcCcy() {
		return this.custAcCcy;
	}

	public void setCustAcCcy(String custAcCcy) {
		this.custAcCcy = custAcCcy;
	}

	public String getOldCustNo() {
		return this.oldCustNo;
	}

	public void setOldCustNo(String oldCustNo) {
		this.oldCustNo = oldCustNo;
	}

	public String getOldAcBrn() {
		return this.oldAcBrn;
	}

	public void setOldAcBrn(String oldAcBrn) {
		this.oldAcBrn = oldAcBrn;
	}

	public String getOldAcNo() {
		return this.oldAcNo;
	}

	public void setOldAcNo(String oldAcNo) {
		this.oldAcNo = oldAcNo;
	}

	public String getOldAcCcy() {
		return this.oldAcCcy;
	}

	public void setOldAcCcy(String oldAcCcy) {
		this.oldAcCcy = oldAcCcy;
	}

	public BigDecimal getFcyAmount() {
		return this.fcyAmount;
	}

	public void setFcyAmount(BigDecimal fcyAmount) {
		this.fcyAmount = fcyAmount;
	}

	public BigDecimal getTxnAmount() {
		return this.txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public BigDecimal getExchRate() {
		return this.exchRate;
	}

	public void setExchRate(BigDecimal exchRate) {
		this.exchRate = exchRate;
	}

	public String getCptyBankcode() {
		return this.cptyBankcode;
	}

	public void setCptyBankcode(String cptyBankcode) {
		this.cptyBankcode = cptyBankcode;
	}

	public String getOldBankcode() {
		return this.oldBankcode;
	}

	public void setOldBankcode(String oldBankcode) {
		this.oldBankcode = oldBankcode;
	}

	public String getCptyAcNo() {
		return this.cptyAcNo;
	}

	public void setCptyAcNo(String cptyAcNo) {
		this.cptyAcNo = cptyAcNo;
	}

	public String getCptyName() {
		return this.cptyName;
	}

	public void setCptyName(String cptyName) {
		this.cptyName = cptyName;
	}

	public Date getOldActivationDt() {
		return this.oldActivationDt;
	}

	public void setOldActivationDt(Date oldActivationDt) {
		this.oldActivationDt = oldActivationDt;
	}

	public BigDecimal getOldExchRate() {
		return this.oldExchRate;
	}

	public void setOldExchRate(BigDecimal oldExchRate) {
		this.oldExchRate = oldExchRate;
	}

	public String getCutoffStat() {
		return this.cutoffStat;
	}

	public void setCutoffStat(String cutoffStat) {
		this.cutoffStat = cutoffStat;
	}

	public Date getBookingDt() {
		return this.bookingDt;
	}

	public void setBookingDt(Date bookingDt) {
		this.bookingDt = bookingDt;
	}

	public Date getInitiationDt() {
		return this.initiationDt;
	}

	public void setInitiationDt(Date initiationDt) {
		this.initiationDt = initiationDt;
	}

	public Date getActivationDt() {
		return this.activationDt;
	}

	public void setActivationDt(Date activationDt) {
		this.activationDt = activationDt;
	}

	public Date getCustEntryDt() {
		return this.custEntryDt;
	}

	public void setCustEntryDt(Date custEntryDt) {
		this.custEntryDt = custEntryDt;
	}

	public Date getCustEntryValDt() {
		return this.custEntryValDt;
	}

	public void setCustEntryValDt(Date custEntryValDt) {
		this.custEntryValDt = custEntryValDt;
	}

	public Date getCptyEntryDt() {
		return this.cptyEntryDt;
	}

	public void setCptyEntryDt(Date cptyEntryDt) {
		this.cptyEntryDt = cptyEntryDt;
	}

	public Date getCptyEntryValDt() {
		return this.cptyEntryValDt;
	}

	public void setCptyEntryValDt(Date cptyEntryValDt) {
		this.cptyEntryValDt = cptyEntryValDt;
	}

	public Date getDispatchDt() {
		return this.dispatchDt;
	}

	public void setDispatchDt(Date dispatchDt) {
		this.dispatchDt = dispatchDt;
	}

	public Date getReceiveDate() {
		return this.receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getConsolReqd() {
		return this.consolReqd;
	}

	public void setConsolReqd(String consolReqd) {
		this.consolReqd = consolReqd;
	}

	public String getConsolRef() {
		return this.consolRef;
	}

	public void setConsolRef(String consolRef) {
		this.consolRef = consolRef;
	}

	public String getCreditExcpt() {
		return this.creditExcpt;
	}

	public void setCreditExcpt(String creditExcpt) {
		this.creditExcpt = creditExcpt;
	}

	public String getGenAdvice() {
		return this.genAdvice;
	}

	public void setGenAdvice(String genAdvice) {
		this.genAdvice = genAdvice;
	}

	public String getDispatch() {
		return this.dispatch;
	}

	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}

	public String getExceptionQueue() {
		return this.exceptionQueue;
	}

	public void setExceptionQueue(String exceptionQueue) {
		this.exceptionQueue = exceptionQueue;
	}

	public String getRelatedTxn() {
		return this.relatedTxn;
	}

	public void setRelatedTxn(String relatedTxn) {
		this.relatedTxn = relatedTxn;
	}

	public String getWip() {
		return this.wip;
	}

	public void setWip(String wip) {
		this.wip = wip;
	}

	public String getLastEventCode() {
		return this.lastEventCode;
	}

	public void setLastEventCode(String lastEventCode) {
		this.lastEventCode = lastEventCode;
	}

	public String getNextEventCode() {
		return this.nextEventCode;
	}

	public void setNextEventCode(String nextEventCode) {
		this.nextEventCode = nextEventCode;
	}

	public Date getNextEventDate() {
		return this.nextEventDate;
	}

	public void setNextEventDate(Date nextEventDate) {
		this.nextEventDate = nextEventDate;
	}

	public String getNextEventBranch() {
		return this.nextEventBranch;
	}

	public void setNextEventBranch(String nextEventBranch) {
		this.nextEventBranch = nextEventBranch;
	}

	public String getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getAuthStatus() {
		return this.authStatus;
	}

	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	public String getAutoManualFlag() {
		return this.autoManualFlag;
	}

	public void setAutoManualFlag(String autoManualFlag) {
		this.autoManualFlag = autoManualFlag;
	}

	public String getDeferredEvents() {
		return this.deferredEvents;
	}

	public void setDeferredEvents(String deferredEvents) {
		this.deferredEvents = deferredEvents;
	}

	public String getAcRedirect() {
		return this.acRedirect;
	}

	public void setAcRedirect(String acRedirect) {
		this.acRedirect = acRedirect;
	}

	public String getBankRedirect() {
		return this.bankRedirect;
	}

	public void setBankRedirect(String bankRedirect) {
		this.bankRedirect = bankRedirect;
	}

	public String getActivationDtFwd() {
		return this.activationDtFwd;
	}

	public void setActivationDtFwd(String activationDtFwd) {
		this.activationDtFwd = activationDtFwd;
	}

	public String getCrexCrfwd() {
		return this.crexCrfwd;
	}

	public void setCrexCrfwd(String crexCrfwd) {
		this.crexCrfwd = crexCrfwd;
	}

	public String getConsolStatus() {
		return this.consolStatus;
	}

	public void setConsolStatus(String consolStatus) {
		this.consolStatus = consolStatus;
	}

	public String getDupResolutionList() {
		return this.dupResolutionList;
	}

	public void setDupResolutionList(String dupResolutionList) {
		this.dupResolutionList = dupResolutionList;
	}

	public String getAdvProcessed() {
		return this.advProcessed;
	}

	public void setAdvProcessed(String advProcessed) {
		this.advProcessed = advProcessed;
	}

	public String getMakerId() {
		return this.makerId;
	}

	public void setMakerId(String makerId) {
		this.makerId = makerId;
	}

	public String getCheckerId() {
		return this.checkerId;
	}

	public void setCheckerId(String checkerId) {
		this.checkerId = checkerId;
	}

	public Date getMakerDtStamp() {
		return this.makerDtStamp;
	}

	public void setMakerDtStamp(Date makerDtStamp) {
		this.makerDtStamp = makerDtStamp;
	}

	public Date getCheckerDtStamp() {
		return this.checkerDtStamp;
	}

	public void setCheckerDtStamp(Date checkerDtStamp) {
		this.checkerDtStamp = checkerDtStamp;
	}

	public Date getInitDtStamp() {
		return this.initDtStamp;
	}

	public void setInitDtStamp(Date initDtStamp) {
		this.initDtStamp = initDtStamp;
	}

	public Date getDclgDtStamp() {
		return this.dclgDtStamp;
	}

	public void setDclgDtStamp(Date dclgDtStamp) {
		this.dclgDtStamp = dclgDtStamp;
	}

	public Date getDrlqDtStamp() {
		return this.drlqDtStamp;
	}

	public void setDrlqDtStamp(Date drlqDtStamp) {
		this.drlqDtStamp = drlqDtStamp;
	}

	public Date getCrlqDtStamp() {
		return this.crlqDtStamp;
	}

	public void setCrlqDtStamp(Date crlqDtStamp) {
		this.crlqDtStamp = crlqDtStamp;
	}

	public String getDclgProcessed() {
		return this.dclgProcessed;
	}

	public void setDclgProcessed(String dclgProcessed) {
		this.dclgProcessed = dclgProcessed;
	}

	public String getDragProcessed() {
		return this.dragProcessed;
	}

	public void setDragProcessed(String dragProcessed) {
		this.dragProcessed = dragProcessed;
	}

	public String getUdf1() {
		return this.udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return this.udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return this.udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return this.udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return this.udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getUdf6() {
		return this.udf6;
	}

	public void setUdf6(String udf6) {
		this.udf6 = udf6;
	}

	public String getUdf7() {
		return this.udf7;
	}

	public void setUdf7(String udf7) {
		this.udf7 = udf7;
	}

	public String getUdf8() {
		return this.udf8;
	}

	public void setUdf8(String udf8) {
		this.udf8 = udf8;
	}

	public String getUdf9() {
		return this.udf9;
	}

	public void setUdf9(String udf9) {
		this.udf9 = udf9;
	}

	public String getUdf10() {
		return this.udf10;
	}

	public void setUdf10(String udf10) {
		this.udf10 = udf10;
	}

	public String getUdf11() {
		return this.udf11;
	}

	public void setUdf11(String udf11) {
		this.udf11 = udf11;
	}

	public String getUdf12() {
		return this.udf12;
	}

	public void setUdf12(String udf12) {
		this.udf12 = udf12;
	}

	public String getUdf13() {
		return this.udf13;
	}

	public void setUdf13(String udf13) {
		this.udf13 = udf13;
	}

	public String getUdf14() {
		return this.udf14;
	}

	public void setUdf14(String udf14) {
		this.udf14 = udf14;
	}

	public String getUdf15() {
		return this.udf15;
	}

	public void setUdf15(String udf15) {
		this.udf15 = udf15;
	}

	public String getUdf16() {
		return this.udf16;
	}

	public void setUdf16(String udf16) {
		this.udf16 = udf16;
	}

	public String getUdf17() {
		return this.udf17;
	}

	public void setUdf17(String udf17) {
		this.udf17 = udf17;
	}

	public String getUdf18() {
		return this.udf18;
	}

	public void setUdf18(String udf18) {
		this.udf18 = udf18;
	}

	public String getUdf19() {
		return this.udf19;
	}

	public void setUdf19(String udf19) {
		this.udf19 = udf19;
	}

	public String getUdf20() {
		return this.udf20;
	}

	public void setUdf20(String udf20) {
		this.udf20 = udf20;
	}

	public String getUdf21() {
		return this.udf21;
	}

	public void setUdf21(String udf21) {
		this.udf21 = udf21;
	}

	public String getUdf22() {
		return this.udf22;
	}

	public void setUdf22(String udf22) {
		this.udf22 = udf22;
	}

	public String getUdf23() {
		return this.udf23;
	}

	public void setUdf23(String udf23) {
		this.udf23 = udf23;
	}

	public String getUdf24() {
		return this.udf24;
	}

	public void setUdf24(String udf24) {
		this.udf24 = udf24;
	}

	public String getUdf25() {
		return this.udf25;
	}

	public void setUdf25(String udf25) {
		this.udf25 = udf25;
	}

	public String getUdf26() {
		return this.udf26;
	}

	public void setUdf26(String udf26) {
		this.udf26 = udf26;
	}

	public String getUdf27() {
		return this.udf27;
	}

	public void setUdf27(String udf27) {
		this.udf27 = udf27;
	}

	public String getUdf28() {
		return this.udf28;
	}

	public void setUdf28(String udf28) {
		this.udf28 = udf28;
	}

	public String getUdf29() {
		return this.udf29;
	}

	public void setUdf29(String udf29) {
		this.udf29 = udf29;
	}

	public String getUdf30() {
		return this.udf30;
	}

	public void setUdf30(String udf30) {
		this.udf30 = udf30;
	}

	public String getDispatchRefNo() {
		return this.dispatchRefNo;
	}

	public void setDispatchRefNo(String dispatchRefNo) {
		this.dispatchRefNo = dispatchRefNo;
	}

	public String getCollectionType() {
		return this.collectionType;
	}

	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}

	public String getRfdType() {
		return this.rfdType;
	}

	public void setRfdType(String rfdType) {
		this.rfdType = rfdType;
	}

	public String getCollectionStatus() {
		return this.collectionStatus;
	}

	public void setCollectionStatus(String collectionStatus) {
		this.collectionStatus = collectionStatus;
	}

	public String getDebtorCategory() {
		return this.debtorCategory;
	}

	public void setDebtorCategory(String debtorCategory) {
		this.debtorCategory = debtorCategory;
	}

	public String getInvoiceSplitReqd() {
		return this.invoiceSplitReqd;
	}

	public void setInvoiceSplitReqd(String invoiceSplitReqd) {
		this.invoiceSplitReqd = invoiceSplitReqd;
	}

	public BigDecimal getMaxTxnAmt() {
		return this.maxTxnAmt;
	}

	public void setMaxTxnAmt(BigDecimal maxTxnAmt) {
		this.maxTxnAmt = maxTxnAmt;
	}

	public String getSplitIndicator() {
		return this.splitIndicator;
	}

	public void setSplitIndicator(String splitIndicator) {
		this.splitIndicator = splitIndicator;
	}

	public BigDecimal getSplitNumber() {
		return this.splitNumber;
	}

	public void setSplitNumber(BigDecimal splitNumber) {
		this.splitNumber = splitNumber;
	}

	public String getSplitParentRefNo() {
		return this.splitParentRefNo;
	}

	public void setSplitParentRefNo(String splitParentRefNo) {
		this.splitParentRefNo = splitParentRefNo;
	}

	public String getAutoResponse() {
		return this.autoResponse;
	}

	public void setAutoResponse(String autoResponse) {
		this.autoResponse = autoResponse;
	}

	public Date getResponseDate() {
		return this.responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public String getResponseAdviceReqd() {
		return this.responseAdviceReqd;
	}

	public void setResponseAdviceReqd(String responseAdviceReqd) {
		this.responseAdviceReqd = responseAdviceReqd;
	}

	public String getResponseAdviceBasis() {
		return this.responseAdviceBasis;
	}

	public void setResponseAdviceBasis(String responseAdviceBasis) {
		this.responseAdviceBasis = responseAdviceBasis;
	}

	public Date getResponseAdviceDate() {
		return this.responseAdviceDate;
	}

	public void setResponseAdviceDate(Date responseAdviceDate) {
		this.responseAdviceDate = responseAdviceDate;
	}

	public String getResponseAdviceSent() {
		return this.responseAdviceSent;
	}

	public void setResponseAdviceSent(String responseAdviceSent) {
		this.responseAdviceSent = responseAdviceSent;
	}

	public String getRedispatchReqd() {
		return this.redispatchReqd;
	}

	public void setRedispatchReqd(String redispatchReqd) {
		this.redispatchReqd = redispatchReqd;
	}

	public String getAutoRedispatch() {
		return this.autoRedispatch;
	}

	public void setAutoRedispatch(String autoRedispatch) {
		this.autoRedispatch = autoRedispatch;
	}

	public BigDecimal getMaxRedispatchNo() {
		return this.maxRedispatchNo;
	}

	public void setMaxRedispatchNo(BigDecimal maxRedispatchNo) {
		this.maxRedispatchNo = maxRedispatchNo;
	}

	public String getRedispatchIndicator() {
		return this.redispatchIndicator;
	}

	public void setRedispatchIndicator(String redispatchIndicator) {
		this.redispatchIndicator = redispatchIndicator;
	}

	public Date getRedispatchDate() {
		return this.redispatchDate;
	}

	public void setRedispatchDate(Date redispatchDate) {
		this.redispatchDate = redispatchDate;
	}

	public BigDecimal getRedispatchNo() {
		return this.redispatchNo;
	}

	public void setRedispatchNo(BigDecimal redispatchNo) {
		this.redispatchNo = redispatchNo;
	}

	public String getRedispatchParentRefNo() {
		return this.redispatchParentRefNo;
	}

	public void setRedispatchParentRefNo(String redispatchParentRefNo) {
		this.redispatchParentRefNo = redispatchParentRefNo;
	}

	public String getRedispatched() {
		return this.redispatched;
	}

	public void setRedispatched(String redispatched) {
		this.redispatched = redispatched;
	}

	public Date getMaxRecallDate() {
		return this.maxRecallDate;
	}

	public void setMaxRecallDate(Date maxRecallDate) {
		this.maxRecallDate = maxRecallDate;
	}

	public Date getApprDtStamp() {
		return this.apprDtStamp;
	}

	public void setApprDtStamp(Date apprDtStamp) {
		this.apprDtStamp = apprDtStamp;
	}

	public Date getRejtDtStamp() {
		return this.rejtDtStamp;
	}

	public void setRejtDtStamp(Date rejtDtStamp) {
		this.rejtDtStamp = rejtDtStamp;
	}

	public Date getClosDtStamp() {
		return this.closDtStamp;
	}

	public void setClosDtStamp(Date closDtStamp) {
		this.closDtStamp = closDtStamp;
	}

	public Date getRdspDtStamp() {
		return this.rdspDtStamp;
	}

	public void setRdspDtStamp(Date rdspDtStamp) {
		this.rdspDtStamp = rdspDtStamp;
	}

	public Date getReclDtStamp() {
		return this.reclDtStamp;
	}

	public void setReclDtStamp(Date reclDtStamp) {
		this.reclDtStamp = reclDtStamp;
	}

	public Date getXrefDtStamp() {
		return this.xrefDtStamp;
	}

	public void setXrefDtStamp(Date xrefDtStamp) {
		this.xrefDtStamp = xrefDtStamp;
	}

	public Date getRadvDtStamp() {
		return this.radvDtStamp;
	}

	public void setRadvDtStamp(Date radvDtStamp) {
		this.radvDtStamp = radvDtStamp;
	}

	public String getCreditorId() {
		return this.creditorId;
	}

	public void setCreditorId(String creditorId) {
		this.creditorId = creditorId;
	}

	public String getAgreementId() {
		return this.agreementId;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}

	public String getRejectCode() {
		return this.rejectCode;
	}

	public void setRejectCode(String rejectCode) {
		this.rejectCode = rejectCode;
	}

	public String getRejectDetail() {
		return this.rejectDetail;
	}

	public void setRejectDetail(String rejectDetail) {
		this.rejectDetail = rejectDetail;
	}

	public BigDecimal getInterestAmount() {
		return this.interestAmount;
	}

	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}

	public String getOriginalCollRefNo() {
		return this.originalCollRefNo;
	}

	public void setOriginalCollRefNo(String originalCollRefNo) {
		this.originalCollRefNo = originalCollRefNo;
	}

	public BigDecimal getCollectedAmount() {
		return this.collectedAmount;
	}

	public void setCollectedAmount(BigDecimal collectedAmount) {
		this.collectedAmount = collectedAmount;
	}

	public String getChargeWaiver() {
		return this.chargeWaiver;
	}

	public void setChargeWaiver(String chargeWaiver) {
		this.chargeWaiver = chargeWaiver;
	}

	public String getChargeAcStatistics() {
		return this.chargeAcStatistics;
	}

	public void setChargeAcStatistics(String chargeAcStatistics) {
		this.chargeAcStatistics = chargeAcStatistics;
	}

	public String getChargeCustStatistics() {
		return this.chargeCustStatistics;
	}

	public void setChargeCustStatistics(String chargeCustStatistics) {
		this.chargeCustStatistics = chargeCustStatistics;
	}

	public BigDecimal getPrevMthAcTxnCount() {
		return this.prevMthAcTxnCount;
	}

	public void setPrevMthAcTxnCount(BigDecimal prevMthAcTxnCount) {
		this.prevMthAcTxnCount = prevMthAcTxnCount;
	}

	public BigDecimal getPrevMthAcTxnAmount() {
		return this.prevMthAcTxnAmount;
	}

	public void setPrevMthAcTxnAmount(BigDecimal prevMthAcTxnAmount) {
		this.prevMthAcTxnAmount = prevMthAcTxnAmount;
	}

	public BigDecimal getPrevMthCustTxnCount() {
		return this.prevMthCustTxnCount;
	}

	public void setPrevMthCustTxnCount(BigDecimal prevMthCustTxnCount) {
		this.prevMthCustTxnCount = prevMthCustTxnCount;
	}

	public BigDecimal getPrevMthCustTxnAmount() {
		return this.prevMthCustTxnAmount;
	}

	public void setPrevMthCustTxnAmount(BigDecimal prevMthCustTxnAmount) {
		this.prevMthCustTxnAmount = prevMthCustTxnAmount;
	}

	public String getAsciiHandoffReqd() {
		return this.asciiHandoffReqd;
	}

	public void setAsciiHandoffReqd(String asciiHandoffReqd) {
		this.asciiHandoffReqd = asciiHandoffReqd;
	}

	public String getRejectDueToFunds() {
		return this.rejectDueToFunds;
	}

	public void setRejectDueToFunds(String rejectDueToFunds) {
		this.rejectDueToFunds = rejectDueToFunds;
	}

	public String getXrefProcessed() {
		return this.xrefProcessed;
	}

	public void setXrefProcessed(String xrefProcessed) {
		this.xrefProcessed = xrefProcessed;
	}

	public String getCollectVolumeStatistics() {
		return this.collectVolumeStatistics;
	}

	public void setCollectVolumeStatistics(String collectVolumeStatistics) {
		this.collectVolumeStatistics = collectVolumeStatistics;
	}

	public String getChargeCategory() {
		return this.chargeCategory;
	}

	public void setChargeCategory(String chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

	public String getRedispatchChildRefNo() {
		return this.redispatchChildRefNo;
	}

	public void setRedispatchChildRefNo(String redispatchChildRefNo) {
		this.redispatchChildRefNo = redispatchChildRefNo;
	}

	public BigDecimal getMaxIntAmt() {
		return this.maxIntAmt;
	}

	public void setMaxIntAmt(BigDecimal maxIntAmt) {
		this.maxIntAmt = maxIntAmt;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCustAddress1() {
		return this.custAddress1;
	}

	public void setCustAddress1(String custAddress1) {
		this.custAddress1 = custAddress1;
	}

	public String getCustAddress2() {
		return this.custAddress2;
	}

	public void setCustAddress2(String custAddress2) {
		this.custAddress2 = custAddress2;
	}

	public String getCustAddress3() {
		return this.custAddress3;
	}

	public void setCustAddress3(String custAddress3) {
		this.custAddress3 = custAddress3;
	}

	public String getCustAddress4() {
		return this.custAddress4;
	}

	public void setCustAddress4(String custAddress4) {
		this.custAddress4 = custAddress4;
	}

	public String getCptyAddress1() {
		return this.cptyAddress1;
	}

	public void setCptyAddress1(String cptyAddress1) {
		this.cptyAddress1 = cptyAddress1;
	}

	public String getCptyAddress2() {
		return this.cptyAddress2;
	}

	public void setCptyAddress2(String cptyAddress2) {
		this.cptyAddress2 = cptyAddress2;
	}

	public String getCptyAddress3() {
		return this.cptyAddress3;
	}

	public void setCptyAddress3(String cptyAddress3) {
		this.cptyAddress3 = cptyAddress3;
	}

	public String getCptyAddress4() {
		return this.cptyAddress4;
	}

	public void setCptyAddress4(String cptyAddress4) {
		this.cptyAddress4 = cptyAddress4;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public BigDecimal getCustAvlblBal() {
		return this.custAvlblBal;
	}

	public void setCustAvlblBal(BigDecimal custAvlblBal) {
		this.custAvlblBal = custAvlblBal;
	}

	public String getMsgStatus() {
		return this.msgStatus;
	}

	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}

	public BigDecimal getLatestEventSeqNo() {
		return this.latestEventSeqNo;
	}

	public void setLatestEventSeqNo(BigDecimal latestEventSeqNo) {
		this.latestEventSeqNo = latestEventSeqNo;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public String getMiscProcessed() {
		return this.miscProcessed;
	}

	public void setMiscProcessed(String miscProcessed) {
		this.miscProcessed = miscProcessed;
	}

	public String getClearingBrn() {
		return this.clearingBrn;
	}

	public void setClearingBrn(String clearingBrn) {
		this.clearingBrn = clearingBrn;
	}

	public String getAuthRejRemarks() {
		return this.authRejRemarks;
	}

	public void setAuthRejRemarks(String authRejRemarks) {
		this.authRejRemarks = authRejRemarks;
	}

	public String getPrevMsgStatus() {
		return this.prevMsgStatus;
	}

	public void setPrevMsgStatus(String prevMsgStatus) {
		this.prevMsgStatus = prevMsgStatus;
	}

	public String getOutPmtWorkflow() {
		return this.outPmtWorkflow;
	}

	public void setOutPmtWorkflow(String outPmtWorkflow) {
		this.outPmtWorkflow = outPmtWorkflow;
	}

	public String getAutoDispatch() {
		return this.autoDispatch;
	}

	public void setAutoDispatch(String autoDispatch) {
		this.autoDispatch = autoDispatch;
	}

	public String getDispFileName() {
		return this.dispFileName;
	}

	public void setDispFileName(String dispFileName) {
		this.dispFileName = dispFileName;
	}

	public String getCustReference() {
		return this.custReference;
	}

	public void setCustReference(String custReference) {
		this.custReference = custReference;
	}

	public String getPaymentDetails1() {
		return this.paymentDetails1;
	}

	public void setPaymentDetails1(String paymentDetails1) {
		this.paymentDetails1 = paymentDetails1;
	}

	public String getPaymentDetails2() {
		return this.paymentDetails2;
	}

	public void setPaymentDetails2(String paymentDetails2) {
		this.paymentDetails2 = paymentDetails2;
	}

	public String getPaymentDetails3() {
		return this.paymentDetails3;
	}

	public void setPaymentDetails3(String paymentDetails3) {
		this.paymentDetails3 = paymentDetails3;
	}

	public String getPaymentDetails4() {
		return this.paymentDetails4;
	}

	public void setPaymentDetails4(String paymentDetails4) {
		this.paymentDetails4 = paymentDetails4;
	}

	public BigDecimal getActualAmount() {
		return this.actualAmount;
	}

	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}

	public String getChargeMode() {
		return this.chargeMode;
	}

	public void setChargeMode(String chargeMode) {
		this.chargeMode = chargeMode;
	}

	public String getCustAddress5() {
		return this.custAddress5;
	}

	public void setCustAddress5(String custAddress5) {
		this.custAddress5 = custAddress5;
	}

	public String getCptyAddress5() {
		return this.cptyAddress5;
	}

	public void setCptyAddress5(String cptyAddress5) {
		this.cptyAddress5 = cptyAddress5;
	}

	public String getCustInfo1() {
		return this.custInfo1;
	}

	public void setCustInfo1(String custInfo1) {
		this.custInfo1 = custInfo1;
	}

	public String getCustInfo2() {
		return this.custInfo2;
	}

	public void setCustInfo2(String custInfo2) {
		this.custInfo2 = custInfo2;
	}

	public String getCustInfo3() {
		return this.custInfo3;
	}

	public void setCustInfo3(String custInfo3) {
		this.custInfo3 = custInfo3;
	}

	public String getCustInfo4() {
		return this.custInfo4;
	}

	public void setCustInfo4(String custInfo4) {
		this.custInfo4 = custInfo4;
	}

	public String getCptyInfo1() {
		return this.cptyInfo1;
	}

	public void setCptyInfo1(String cptyInfo1) {
		this.cptyInfo1 = cptyInfo1;
	}

	public String getCptyInfo2() {
		return this.cptyInfo2;
	}

	public void setCptyInfo2(String cptyInfo2) {
		this.cptyInfo2 = cptyInfo2;
	}

	public String getCptyInfo3() {
		return this.cptyInfo3;
	}

	public void setCptyInfo3(String cptyInfo3) {
		this.cptyInfo3 = cptyInfo3;
	}

	public String getCptyInfo4() {
		return this.cptyInfo4;
	}

	public void setCptyInfo4(String cptyInfo4) {
		this.cptyInfo4 = cptyInfo4;
	}

	public String getUploadStatus() {
		return this.uploadStatus;
	}

	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}

	public String getDispatchMedia() {
		return this.dispatchMedia;
	}

	public void setDispatchMedia(String dispatchMedia) {
		this.dispatchMedia = dispatchMedia;
	}

	public String getDispatchAcRefNo() {
		return this.dispatchAcRefNo;
	}

	public void setDispatchAcRefNo(String dispatchAcRefNo) {
		this.dispatchAcRefNo = dispatchAcRefNo;
	}

	public BigDecimal getXrefEsn() {
		return this.xrefEsn;
	}

	public void setXrefEsn(BigDecimal xrefEsn) {
		this.xrefEsn = xrefEsn;
	}

	public String getXrefAmountTag() {
		return this.xrefAmountTag;
	}

	public void setXrefAmountTag(String xrefAmountTag) {
		this.xrefAmountTag = xrefAmountTag;
	}

	public String getPostAccounting() {
		return this.postAccounting;
	}

	public void setPostAccounting(String postAccounting) {
		this.postAccounting = postAccounting;
	}

	public String getDirectParticipant() {
		return this.directParticipant;
	}

	public void setDirectParticipant(String directParticipant) {
		this.directParticipant = directParticipant;
	}

	public String getCoverRequired() {
		return this.coverRequired;
	}

	public void setCoverRequired(String coverRequired) {
		this.coverRequired = coverRequired;
	}

	public String getAckStatus() {
		return this.ackStatus;
	}

	public void setAckStatus(String ackStatus) {
		this.ackStatus = ackStatus;
	}

	public String getTxnCcy() {
		return this.txnCcy;
	}

	public void setTxnCcy(String txnCcy) {
		this.txnCcy = txnCcy;
	}

	public BigDecimal getLcyEquivAmt() {
		return this.lcyEquivAmt;
	}

	public void setLcyEquivAmt(BigDecimal lcyEquivAmt) {
		this.lcyEquivAmt = lcyEquivAmt;
	}

	public String getFromExRateQueue() {
		return this.fromExRateQueue;
	}

	public void setFromExRateQueue(String fromExRateQueue) {
		this.fromExRateQueue = fromExRateQueue;
	}

	public String getServLevelCode() {
		return this.servLevelCode;
	}

	public void setServLevelCode(String servLevelCode) {
		this.servLevelCode = servLevelCode;
	}

	public String getChargeBearer() {
		return this.chargeBearer;
	}

	public void setChargeBearer(String chargeBearer) {
		this.chargeBearer = chargeBearer;
	}

	public Date getPymtRejectDate() {
		return this.pymtRejectDate;
	}

	public void setPymtRejectDate(Date pymtRejectDate) {
		this.pymtRejectDate = pymtRejectDate;
	}

	public Date getStlmntDate() {
		return this.stlmntDate;
	}

	public void setStlmntDate(Date stlmntDate) {
		this.stlmntDate = stlmntDate;
	}

	public Date getInstructionDate() {
		return this.instructionDate;
	}

	public void setInstructionDate(Date instructionDate) {
		this.instructionDate = instructionDate;
	}

	public String getInitpartyName() {
		return this.initpartyName;
	}

	public void setInitpartyName(String initpartyName) {
		this.initpartyName = initpartyName;
	}

	public String getInitpartyAddress1() {
		return this.initpartyAddress1;
	}

	public void setInitpartyAddress1(String initpartyAddress1) {
		this.initpartyAddress1 = initpartyAddress1;
	}

	public String getInitpartyAddress2() {
		return this.initpartyAddress2;
	}

	public void setInitpartyAddress2(String initpartyAddress2) {
		this.initpartyAddress2 = initpartyAddress2;
	}

	public String getInitpartyCtry() {
		return this.initpartyCtry;
	}

	public void setInitpartyCtry(String initpartyCtry) {
		this.initpartyCtry = initpartyCtry;
	}

	public String getInitpartyType() {
		return this.initpartyType;
	}

	public void setInitpartyType(String initpartyType) {
		this.initpartyType = initpartyType;
	}

	public String getInitpartyIdtype() {
		return this.initpartyIdtype;
	}

	public void setInitpartyIdtype(String initpartyIdtype) {
		this.initpartyIdtype = initpartyIdtype;
	}

	public String getInitpartyIdvalue() {
		return this.initpartyIdvalue;
	}

	public void setInitpartyIdvalue(String initpartyIdvalue) {
		this.initpartyIdvalue = initpartyIdvalue;
	}

	public String getInitpartyOtheridType() {
		return this.initpartyOtheridType;
	}

	public void setInitpartyOtheridType(String initpartyOtheridType) {
		this.initpartyOtheridType = initpartyOtheridType;
	}

	public String getInitpartyIdissuer() {
		return this.initpartyIdissuer;
	}

	public void setInitpartyIdissuer(String initpartyIdissuer) {
		this.initpartyIdissuer = initpartyIdissuer;
	}

	public String getInitpartyDobcity() {
		return this.initpartyDobcity;
	}

	public void setInitpartyDobcity(String initpartyDobcity) {
		this.initpartyDobcity = initpartyDobcity;
	}

	public String getInitpartyDobctry() {
		return this.initpartyDobctry;
	}

	public void setInitpartyDobctry(String initpartyDobctry) {
		this.initpartyDobctry = initpartyDobctry;
	}

	public String getCustCtry() {
		return this.custCtry;
	}

	public void setCustCtry(String custCtry) {
		this.custCtry = custCtry;
	}

	public String getCustType() {
		return this.custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getCustIdtype() {
		return this.custIdtype;
	}

	public void setCustIdtype(String custIdtype) {
		this.custIdtype = custIdtype;
	}

	public String getCustIdvalue() {
		return this.custIdvalue;
	}

	public void setCustIdvalue(String custIdvalue) {
		this.custIdvalue = custIdvalue;
	}

	public String getCustOtheridType() {
		return this.custOtheridType;
	}

	public void setCustOtheridType(String custOtheridType) {
		this.custOtheridType = custOtheridType;
	}

	public String getCustIdissuer() {
		return this.custIdissuer;
	}

	public void setCustIdissuer(String custIdissuer) {
		this.custIdissuer = custIdissuer;
	}

	public String getCustDobcity() {
		return this.custDobcity;
	}

	public void setCustDobcity(String custDobcity) {
		this.custDobcity = custDobcity;
	}

	public String getCustDobctry() {
		return this.custDobctry;
	}

	public void setCustDobctry(String custDobctry) {
		this.custDobctry = custDobctry;
	}

	public String getDebtorRefparty() {
		return this.debtorRefparty;
	}

	public void setDebtorRefparty(String debtorRefparty) {
		this.debtorRefparty = debtorRefparty;
	}

	public String getCreditorRefparty() {
		return this.creditorRefparty;
	}

	public void setCreditorRefparty(String creditorRefparty) {
		this.creditorRefparty = creditorRefparty;
	}

	public String getCptyAcCcy() {
		return this.cptyAcCcy;
	}

	public void setCptyAcCcy(String cptyAcCcy) {
		this.cptyAcCcy = cptyAcCcy;
	}

	public String getCptyCtry() {
		return this.cptyCtry;
	}

	public void setCptyCtry(String cptyCtry) {
		this.cptyCtry = cptyCtry;
	}

	public String getCptyType() {
		return this.cptyType;
	}

	public void setCptyType(String cptyType) {
		this.cptyType = cptyType;
	}

	public String getCptyIdtype() {
		return this.cptyIdtype;
	}

	public void setCptyIdtype(String cptyIdtype) {
		this.cptyIdtype = cptyIdtype;
	}

	public String getCptyIdvalue() {
		return this.cptyIdvalue;
	}

	public void setCptyIdvalue(String cptyIdvalue) {
		this.cptyIdvalue = cptyIdvalue;
	}

	public String getCptyOtheridType() {
		return this.cptyOtheridType;
	}

	public void setCptyOtheridType(String cptyOtheridType) {
		this.cptyOtheridType = cptyOtheridType;
	}

	public String getCptyIdissuer() {
		return this.cptyIdissuer;
	}

	public void setCptyIdissuer(String cptyIdissuer) {
		this.cptyIdissuer = cptyIdissuer;
	}

	public String getCptyDobcity() {
		return this.cptyDobcity;
	}

	public void setCptyDobcity(String cptyDobcity) {
		this.cptyDobcity = cptyDobcity;
	}

	public String getCptyDobctry() {
		return this.cptyDobctry;
	}

	public void setCptyDobctry(String cptyDobctry) {
		this.cptyDobctry = cptyDobctry;
	}

	public String getOrgMsgName() {
		return this.orgMsgName;
	}

	public void setOrgMsgName(String orgMsgName) {
		this.orgMsgName = orgMsgName;
	}

	public String getOrgMsgRefNo() {
		return this.orgMsgRefNo;
	}

	public void setOrgMsgRefNo(String orgMsgRefNo) {
		this.orgMsgRefNo = orgMsgRefNo;
	}

	public String getRejtOrgName() {
		return this.rejtOrgName;
	}

	public void setRejtOrgName(String rejtOrgName) {
		this.rejtOrgName = rejtOrgName;
	}

	public String getRejtOrgBank() {
		return this.rejtOrgBank;
	}

	public void setRejtOrgBank(String rejtOrgBank) {
		this.rejtOrgBank = rejtOrgBank;
	}

	public String getRejtRefNo() {
		return this.rejtRefNo;
	}

	public void setRejtRefNo(String rejtRefNo) {
		this.rejtRefNo = rejtRefNo;
	}

	public String getRejectCodeAdd() {
		return this.rejectCodeAdd;
	}

	public void setRejectCodeAdd(String rejectCodeAdd) {
		this.rejectCodeAdd = rejectCodeAdd;
	}

	public String getOrgSourceRef() {
		return this.orgSourceRef;
	}

	public void setOrgSourceRef(String orgSourceRef) {
		this.orgSourceRef = orgSourceRef;
	}

	public Date getOrgStlmntDt() {
		return this.orgStlmntDt;
	}

	public void setOrgStlmntDt(Date orgStlmntDt) {
		this.orgStlmntDt = orgStlmntDt;
	}

	public BigDecimal getOrgStlmntAmt() {
		return this.orgStlmntAmt;
	}

	public void setOrgStlmntAmt(BigDecimal orgStlmntAmt) {
		this.orgStlmntAmt = orgStlmntAmt;
	}

	public String getOrgStlmntCcy() {
		return this.orgStlmntCcy;
	}

	public void setOrgStlmntCcy(String orgStlmntCcy) {
		this.orgStlmntCcy = orgStlmntCcy;
	}

	public String getInFileRefNo() {
		return this.inFileRefNo;
	}

	public void setInFileRefNo(String inFileRefNo) {
		this.inFileRefNo = inFileRefNo;
	}

	public String getInMsgRefNo() {
		return this.inMsgRefNo;
	}

	public void setInMsgRefNo(String inMsgRefNo) {
		this.inMsgRefNo = inMsgRefNo;
	}

	public String getInMsgName() {
		return this.inMsgName;
	}

	public void setInMsgName(String inMsgName) {
		this.inMsgName = inMsgName;
	}

	public Date getInMsgCreationDt() {
		return this.inMsgCreationDt;
	}

	public void setInMsgCreationDt(Date inMsgCreationDt) {
		this.inMsgCreationDt = inMsgCreationDt;
	}

	public String getInInstgBank() {
		return this.inInstgBank;
	}

	public void setInInstgBank(String inInstgBank) {
		this.inInstgBank = inInstgBank;
	}

	public String getInInstdBank() {
		return this.inInstdBank;
	}

	public void setInInstdBank(String inInstdBank) {
		this.inInstdBank = inInstdBank;
	}

	public String getInStlmntMethod() {
		return this.inStlmntMethod;
	}

	public void setInStlmntMethod(String inStlmntMethod) {
		this.inStlmntMethod = inStlmntMethod;
	}

	public String getInClrgSysId() {
		return this.inClrgSysId;
	}

	public void setInClrgSysId(String inClrgSysId) {
		this.inClrgSysId = inClrgSysId;
	}

	public String getIpcustName() {
		return this.ipcustName;
	}

	public void setIpcustName(String ipcustName) {
		this.ipcustName = ipcustName;
	}

	public String getIpcustAddress1() {
		return this.ipcustAddress1;
	}

	public void setIpcustAddress1(String ipcustAddress1) {
		this.ipcustAddress1 = ipcustAddress1;
	}

	public String getIpcustAddress2() {
		return this.ipcustAddress2;
	}

	public void setIpcustAddress2(String ipcustAddress2) {
		this.ipcustAddress2 = ipcustAddress2;
	}

	public String getIpcustCtry() {
		return this.ipcustCtry;
	}

	public void setIpcustCtry(String ipcustCtry) {
		this.ipcustCtry = ipcustCtry;
	}

	public String getIpcustType() {
		return this.ipcustType;
	}

	public void setIpcustType(String ipcustType) {
		this.ipcustType = ipcustType;
	}

	public String getIpcustIdtype() {
		return this.ipcustIdtype;
	}

	public void setIpcustIdtype(String ipcustIdtype) {
		this.ipcustIdtype = ipcustIdtype;
	}

	public String getIpcustIdvalue() {
		return this.ipcustIdvalue;
	}

	public void setIpcustIdvalue(String ipcustIdvalue) {
		this.ipcustIdvalue = ipcustIdvalue;
	}

	public String getIpcustOtheridType() {
		return this.ipcustOtheridType;
	}

	public void setIpcustOtheridType(String ipcustOtheridType) {
		this.ipcustOtheridType = ipcustOtheridType;
	}

	public String getIpcustIdissuer() {
		return this.ipcustIdissuer;
	}

	public void setIpcustIdissuer(String ipcustIdissuer) {
		this.ipcustIdissuer = ipcustIdissuer;
	}

	public String getIpcustDobcity() {
		return this.ipcustDobcity;
	}

	public void setIpcustDobcity(String ipcustDobcity) {
		this.ipcustDobcity = ipcustDobcity;
	}

	public String getIpcustDobctry() {
		return this.ipcustDobctry;
	}

	public void setIpcustDobctry(String ipcustDobctry) {
		this.ipcustDobctry = ipcustDobctry;
	}

	public String getIpcustAcNo() {
		return this.ipcustAcNo;
	}

	public void setIpcustAcNo(String ipcustAcNo) {
		this.ipcustAcNo = ipcustAcNo;
	}

	public String getIpcustAcCcy() {
		return this.ipcustAcCcy;
	}

	public void setIpcustAcCcy(String ipcustAcCcy) {
		this.ipcustAcCcy = ipcustAcCcy;
	}

	public String getIpcustBankcode() {
		return this.ipcustBankcode;
	}

	public void setIpcustBankcode(String ipcustBankcode) {
		this.ipcustBankcode = ipcustBankcode;
	}

	public String getDdSeqType() {
		return this.ddSeqType;
	}

	public void setDdSeqType(String ddSeqType) {
		this.ddSeqType = ddSeqType;
	}

	public String getMandateId() {
		return this.mandateId;
	}

	public void setMandateId(String mandateId) {
		this.mandateId = mandateId;
	}

	public Date getMandSignDate() {
		return this.mandSignDate;
	}

	public void setMandSignDate(Date mandSignDate) {
		this.mandSignDate = mandSignDate;
	}

	public String getCreditorSchId() {
		return this.creditorSchId;
	}

	public void setCreditorSchId(String creditorSchId) {
		this.creditorSchId = creditorSchId;
	}

	public String getMandAmendInd() {
		return this.mandAmendInd;
	}

	public void setMandAmendInd(String mandAmendInd) {
		this.mandAmendInd = mandAmendInd;
	}

	public String getMandAmendType() {
		return this.mandAmendType;
	}

	public void setMandAmendType(String mandAmendType) {
		this.mandAmendType = mandAmendType;
	}

	public String getOrgMandateId() {
		return this.orgMandateId;
	}

	public void setOrgMandateId(String orgMandateId) {
		this.orgMandateId = orgMandateId;
	}

	public String getOrgCreditorSchId() {
		return this.orgCreditorSchId;
	}

	public void setOrgCreditorSchId(String orgCreditorSchId) {
		this.orgCreditorSchId = orgCreditorSchId;
	}

	public String getOrgCreditorName() {
		return this.orgCreditorName;
	}

	public void setOrgCreditorName(String orgCreditorName) {
		this.orgCreditorName = orgCreditorName;
	}

	public String getOrgDebtorAcc() {
		return this.orgDebtorAcc;
	}

	public void setOrgDebtorAcc(String orgDebtorAcc) {
		this.orgDebtorAcc = orgDebtorAcc;
	}

	public String getOrgDebtorBank() {
		return this.orgDebtorBank;
	}

	public void setOrgDebtorBank(String orgDebtorBank) {
		this.orgDebtorBank = orgDebtorBank;
	}

	public String getOutFileRefNo() {
		return this.outFileRefNo;
	}

	public void setOutFileRefNo(String outFileRefNo) {
		this.outFileRefNo = outFileRefNo;
	}

	public String getOutMsgRefNo() {
		return this.outMsgRefNo;
	}

	public void setOutMsgRefNo(String outMsgRefNo) {
		this.outMsgRefNo = outMsgRefNo;
	}

	public String getOutMsgName() {
		return this.outMsgName;
	}

	public void setOutMsgName(String outMsgName) {
		this.outMsgName = outMsgName;
	}

	public Date getOutMsgCreationDt() {
		return this.outMsgCreationDt;
	}

	public void setOutMsgCreationDt(Date outMsgCreationDt) {
		this.outMsgCreationDt = outMsgCreationDt;
	}

	public String getSubsystemStat() {
		return this.subsystemStat;
	}

	public void setSubsystemStat(String subsystemStat) {
		this.subsystemStat = subsystemStat;
	}

	public String getGenRemitSlipInInit() {
		return this.genRemitSlipInInit;
	}

	public void setGenRemitSlipInInit(String genRemitSlipInInit) {
		this.genRemitSlipInInit = genRemitSlipInInit;
	}

	public String getChargeAccount() {
		return this.chargeAccount;
	}

	public void setChargeAccount(String chargeAccount) {
		this.chargeAccount = chargeAccount;
	}

	public String getConsolDebitReferenceNo() {
		return this.consolDebitReferenceNo;
	}

	public void setConsolDebitReferenceNo(String consolDebitReferenceNo) {
		this.consolDebitReferenceNo = consolDebitReferenceNo;
	}

	public String getUltDebtorId() {
		return this.ultDebtorId;
	}

	public void setUltDebtorId(String ultDebtorId) {
		this.ultDebtorId = ultDebtorId;
	}

	public String getUltDebtorIdType() {
		return this.ultDebtorIdType;
	}

	public void setUltDebtorIdType(String ultDebtorIdType) {
		this.ultDebtorIdType = ultDebtorIdType;
	}

	public String getUltDebtorIdValue() {
		return this.ultDebtorIdValue;
	}

	public void setUltDebtorIdValue(String ultDebtorIdValue) {
		this.ultDebtorIdValue = ultDebtorIdValue;
	}

	public String getUltDebtorOtherIdType() {
		return this.ultDebtorOtherIdType;
	}

	public void setUltDebtorOtherIdType(String ultDebtorOtherIdType) {
		this.ultDebtorOtherIdType = ultDebtorOtherIdType;
	}

	public String getUltDebtorIdIssuer() {
		return this.ultDebtorIdIssuer;
	}

	public void setUltDebtorIdIssuer(String ultDebtorIdIssuer) {
		this.ultDebtorIdIssuer = ultDebtorIdIssuer;
	}

	public String getUltCreditorId() {
		return this.ultCreditorId;
	}

	public void setUltCreditorId(String ultCreditorId) {
		this.ultCreditorId = ultCreditorId;
	}

	public String getUltCreditorIdType() {
		return this.ultCreditorIdType;
	}

	public void setUltCreditorIdType(String ultCreditorIdType) {
		this.ultCreditorIdType = ultCreditorIdType;
	}

	public String getUltCreditorIdValue() {
		return this.ultCreditorIdValue;
	}

	public void setUltCreditorIdValue(String ultCreditorIdValue) {
		this.ultCreditorIdValue = ultCreditorIdValue;
	}

	public String getUltCreditorOtherIdType() {
		return this.ultCreditorOtherIdType;
	}

	public void setUltCreditorOtherIdType(String ultCreditorOtherIdType) {
		this.ultCreditorOtherIdType = ultCreditorOtherIdType;
	}

	public String getUltCreditorIdIssuer() {
		return this.ultCreditorIdIssuer;
	}

	public void setUltCreditorIdIssuer(String ultCreditorIdIssuer) {
		this.ultCreditorIdIssuer = ultCreditorIdIssuer;
	}

	public String getCategoryPurpose() {
		return this.categoryPurpose;
	}

	public void setCategoryPurpose(String categoryPurpose) {
		this.categoryPurpose = categoryPurpose;
	}

	public String getPurposeType() {
		return this.purposeType;
	}

	public void setPurposeType(String purposeType) {
		this.purposeType = purposeType;
	}

	public String getPurposeValue() {
		return this.purposeValue;
	}

	public void setPurposeValue(String purposeValue) {
		this.purposeValue = purposeValue;
	}

	public String getLocalInstrumentType() {
		return this.localInstrumentType;
	}

	public void setLocalInstrumentType(String localInstrumentType) {
		this.localInstrumentType = localInstrumentType;
	}

	public String getLocalInstrumentValue() {
		return this.localInstrumentValue;
	}

	public void setLocalInstrumentValue(String localInstrumentValue) {
		this.localInstrumentValue = localInstrumentValue;
	}

	public String getCompCcy() {
		return this.compCcy;
	}

	public void setCompCcy(String compCcy) {
		this.compCcy = compCcy;
	}

	public BigDecimal getCompAmt() {
		return this.compAmt;
	}

	public void setCompAmt(BigDecimal compAmt) {
		this.compAmt = compAmt;
	}

	public String getElecSig() {
		return this.elecSig;
	}

	public void setElecSig(String elecSig) {
		this.elecSig = elecSig;
	}

	public String getUltDebtorBirthCity() {
		return this.ultDebtorBirthCity;
	}

	public void setUltDebtorBirthCity(String ultDebtorBirthCity) {
		this.ultDebtorBirthCity = ultDebtorBirthCity;
	}

	public String getUltDebtorBirthCountry() {
		return this.ultDebtorBirthCountry;
	}

	public void setUltDebtorBirthCountry(String ultDebtorBirthCountry) {
		this.ultDebtorBirthCountry = ultDebtorBirthCountry;
	}

	public String getUltCreditorBirthCity() {
		return this.ultCreditorBirthCity;
	}

	public void setUltCreditorBirthCity(String ultCreditorBirthCity) {
		this.ultCreditorBirthCity = ultCreditorBirthCity;
	}

	public String getUltCreditorBirthCountry() {
		return this.ultCreditorBirthCountry;
	}

	public void setUltCreditorBirthCountry(String ultCreditorBirthCountry) {
		this.ultCreditorBirthCountry = ultCreditorBirthCountry;
	}

	public String getCreditorSchType() {
		return this.creditorSchType;
	}

	public void setCreditorSchType(String creditorSchType) {
		this.creditorSchType = creditorSchType;
	}

	public String getCreditorSchIdValue() {
		return this.creditorSchIdValue;
	}

	public void setCreditorSchIdValue(String creditorSchIdValue) {
		this.creditorSchIdValue = creditorSchIdValue;
	}

	public String getCreditorSchIdType() {
		return this.creditorSchIdType;
	}

	public void setCreditorSchIdType(String creditorSchIdType) {
		this.creditorSchIdType = creditorSchIdType;
	}

	public String getOrgCreditorSchType() {
		return this.orgCreditorSchType;
	}

	public void setOrgCreditorSchType(String orgCreditorSchType) {
		this.orgCreditorSchType = orgCreditorSchType;
	}

	public String getOrgCreditorSchIdValue() {
		return this.orgCreditorSchIdValue;
	}

	public void setOrgCreditorSchIdValue(String orgCreditorSchIdValue) {
		this.orgCreditorSchIdValue = orgCreditorSchIdValue;
	}

	public String getOrgCreditorSchIdType() {
		return this.orgCreditorSchIdType;
	}

	public void setOrgCreditorSchIdType(String orgCreditorSchIdType) {
		this.orgCreditorSchIdType = orgCreditorSchIdType;
	}

	public String getCustAddress6() {
		return this.custAddress6;
	}

	public void setCustAddress6(String custAddress6) {
		this.custAddress6 = custAddress6;
	}

	public String getCptyAddress6() {
		return this.cptyAddress6;
	}

	public void setCptyAddress6(String cptyAddress6) {
		this.cptyAddress6 = cptyAddress6;
	}

	public String getUserRefNo() {
		return this.userRefNo;
	}

	public void setUserRefNo(String userRefNo) {
		this.userRefNo = userRefNo;
	}

	public BigDecimal getJobNo() {
		return this.jobNo;
	}

	public void setJobNo(BigDecimal jobNo) {
		this.jobNo = jobNo;
	}

	public String getMsgSeqNo() {
		return this.msgSeqNo;
	}

	public void setMsgSeqNo(String msgSeqNo) {
		this.msgSeqNo = msgSeqNo;
	}

	public String getInstrumentNo() {
		return this.instrumentNo;
	}

	public void setInstrumentNo(String instrumentNo) {
		this.instrumentNo = instrumentNo;
	}

	public Date getInstrumentDt() {
		return this.instrumentDt;
	}

	public void setInstrumentDt(Date instrumentDt) {
		this.instrumentDt = instrumentDt;
	}

	public String getAutoRelease() {
		return this.autoRelease;
	}

	public void setAutoRelease(String autoRelease) {
		this.autoRelease = autoRelease;
	}

	public String getSndrRecv7495() {
		return this.sndrRecv7495;
	}

	public void setSndrRecv7495(String sndrRecv7495) {
		this.sndrRecv7495 = sndrRecv7495;
	}

	public String getSndrRecv74951() {
		return this.sndrRecv74951;
	}

	public void setSndrRecv74951(String sndrRecv74951) {
		this.sndrRecv74951 = sndrRecv74951;
	}

	public String getSndrRecv74952() {
		return this.sndrRecv74952;
	}

	public void setSndrRecv74952(String sndrRecv74952) {
		this.sndrRecv74952 = sndrRecv74952;
	}

	public String getSndrRecv74953() {
		return this.sndrRecv74953;
	}

	public void setSndrRecv74953(String sndrRecv74953) {
		this.sndrRecv74953 = sndrRecv74953;
	}

	public String getSndrRecv74954() {
		return this.sndrRecv74954;
	}

	public void setSndrRecv74954(String sndrRecv74954) {
		this.sndrRecv74954 = sndrRecv74954;
	}

	public String getSndrRecv74955() {
		return this.sndrRecv74955;
	}

	public void setSndrRecv74955(String sndrRecv74955) {
		this.sndrRecv74955 = sndrRecv74955;
	}

	public BigDecimal getCustAccType() {
		return this.custAccType;
	}

	public void setCustAccType(BigDecimal custAccType) {
		this.custAccType = custAccType;
	}

	public String getDestMicrCode() {
		return this.destMicrCode;
	}

	public void setDestMicrCode(String destMicrCode) {
		this.destMicrCode = destMicrCode;
	}

	public String getCommunicationMode() {
		return this.communicationMode;
	}

	public void setCommunicationMode(String communicationMode) {
		this.communicationMode = communicationMode;
	}

	public String getMobilenoEmailid() {
		return this.mobilenoEmailid;
	}

	public void setMobilenoEmailid(String mobilenoEmailid) {
		this.mobilenoEmailid = mobilenoEmailid;
	}

}
