package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Reklist {
	private String nomrek;
	private String altnomrek;
	private String rekname;
	private String kdcab;
	private String kdval;
	private String acc_clas;
	private String reksys;
	private String reksts;
	private BigDecimal ac_entry_sr_no;
	private String address; 
	private String passbook_number;
	
	public String getNomrek() {
		return nomrek;
	}
	public void setNomrek(String nomrek) {
		this.nomrek = nomrek;
	}
	public String getAltnomrek() {
		return altnomrek;
	}
	public void setAltnomrek(String altnomrek) {
		this.altnomrek = altnomrek;
	}
	public String getRekname() {
		return rekname;
	}
	public void setRekname(String rekname) {
		this.rekname = rekname;
	}
	public String getKdcab() {
		return kdcab;
	}
	public void setKdcab(String kdcab) {
		this.kdcab = kdcab;
	}
	public String getAcc_clas() {
		return acc_clas;
	}
	public void setAcc_clas(String acc_clas) {
		this.acc_clas = acc_clas;
	}
	public String getKdval() {
		return kdval;
	}
	public void setKdval(String kdval) {
		this.kdval = kdval;
	}
	public String getReksys() {
		return reksys;
	}
	public void setReksys(String reksys) {
		this.reksys = reksys;
	}
	public String getReksts() {
		return reksts;
	}
	public void setReksts(String reksts) {
		this.reksts = reksts;
	}
	public BigDecimal getAc_entry_sr_no() {
		return ac_entry_sr_no;
	}
	public void setAc_entry_sr_no(BigDecimal ac_entry_sr_no) {
		this.ac_entry_sr_no = ac_entry_sr_no;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPassbook_number() {
		return passbook_number;
	}
	public void setPassbook_number(String passbook_number) {
		this.passbook_number = passbook_number;
	}
	
}
