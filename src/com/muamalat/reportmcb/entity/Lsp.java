package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.util.Comparator;

public class Lsp {
	
	private String gl_code;
	private String gl_desc;
	private String branch_code;
	private String ccy;
	private BigDecimal dr_mov_lcy ;
	private BigDecimal cr_mov_lcy ;
	private BigDecimal lcy_closing_bal ;
	private BigDecimal acy_closing_bal ;
	private BigDecimal mov_lcy ;
	private BigDecimal mov_eq_lcy ;
	private BigDecimal debitgl ;
	private BigDecimal creditgl ;
	private BigDecimal sld_prcobaan ;
	
	public static class OrderByBranch implements Comparator<Lsp> {

        @Override
        public int compare(Lsp o1, Lsp o2) {
            return o1.branch_code.compareTo(o2.branch_code);
        }
    }
	
	public Lsp() {
    }
	
	public String getGl_code() {
		return gl_code;
	}
	public void setGl_code(String glCode) {
		gl_code = glCode;
	}
	public String getGl_desc() {
		return gl_desc;
	}
	public void setGl_desc(String glDesc) {
		gl_desc = glDesc;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branchCode) {
		branch_code = branchCode;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getDr_mov_lcy() {
		return dr_mov_lcy;
	}
	public void setDr_mov_lcy(BigDecimal drMovLcy) {
		dr_mov_lcy = drMovLcy;
	}
	public BigDecimal getCr_mov_lcy() {
		return cr_mov_lcy;
	}
	public void setCr_mov_lcy(BigDecimal crMovLcy) {
		cr_mov_lcy = crMovLcy;
	}
	public BigDecimal getMov_lcy() {
		return mov_lcy;
	}
	
	public void setMov_lcy(BigDecimal movLcy) {
		mov_lcy = movLcy;
	}

	public BigDecimal getMov_eq_lcy() {
		return mov_eq_lcy;
	}

	public void setMov_eq_lcy(BigDecimal movEqLcy) {
		mov_eq_lcy = movEqLcy;
	}

	public BigDecimal getDebitgl() {
		return debitgl;
	}

	public void setDebitgl(BigDecimal debitgl) {
		this.debitgl = debitgl;
	}

	public BigDecimal getCreditgl() {
		return creditgl;
	}

	public void setCreditgl(BigDecimal creditgl) {
		this.creditgl = creditgl;
	}
	public BigDecimal getLcy_closing_bal() {
		return lcy_closing_bal;
	}
	public void setLcy_closing_bal(BigDecimal lcy_closing_bal) {
		this.lcy_closing_bal = lcy_closing_bal;
	}
	public BigDecimal getAcy_closing_bal() {
		return acy_closing_bal;
	}
	public void setAcy_closing_bal(BigDecimal acy_closing_bal) {
		this.acy_closing_bal = acy_closing_bal;
	}

	public BigDecimal getSld_prcobaan() {
		return sld_prcobaan;
	}

	public void setSld_prcobaan(BigDecimal sld_prcobaan) {
		this.sld_prcobaan = sld_prcobaan;
	}
	
}
