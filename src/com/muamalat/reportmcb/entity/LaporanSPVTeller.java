package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class LaporanSPVTeller {
	private String branch;
	private String tanggal;
	private String jam;
	private String function_desc;
	private String trn_code;
	private String ac_no;
	private String trn_ref_no;
	private String no_warkat;
	private String user_id;
	private String auth_id;
	private BigDecimal debet;
	private BigDecimal kredit;
	private BigDecimal nominal;
	private String drcr_ind;
	
	public BigDecimal getNominal() {
		return nominal;
	}
	public void setNominal(BigDecimal nominal) {
		this.nominal = nominal;
	}
	
	public String getDrcr_ind() {
		return drcr_ind;
	}
	public void setDrcr_ind(String drcr_ind) {
		this.drcr_ind = drcr_ind;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getFunction_desc() {
		return function_desc;
	}
	public void setFunction_desc(String function_desc) {
		this.function_desc = function_desc;
	}
	public String getTrn_code() {
		return trn_code;
	}
	public void setTrn_code(String trn_code) {
		this.trn_code = trn_code;
	}
	public String getAc_no() {
		return ac_no;
	}
	public void setAc_no(String ac_no) {
		this.ac_no = ac_no;
	}
	public String getTrn_ref_no() {
		return trn_ref_no;
	}
	public void setTrn_ref_no(String trn_ref_no) {
		this.trn_ref_no = trn_ref_no;
	}
	public String getNo_warkat() {
		return no_warkat;
	}
	public void setNo_warkat(String no_warkat) {
		this.no_warkat = no_warkat;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAuth_id() {
		return auth_id;
	}
	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}
	public BigDecimal getDebet() {
		return debet;
	}
	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}
	public BigDecimal getKredit() {
		return kredit;
	}
	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}
}
