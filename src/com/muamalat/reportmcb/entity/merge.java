package com.muamalat.reportmcb.entity;

public class merge {
	
	private String custNo;
	private String custName;
	private int branchCode;
	private String branchName;
	private String mergeMakerDt;
	private String mergeMaker;
	private String mergeChecker;
	private String closeMaker;
	private String closeChecker;
	private String lastMakerDt;
	private String status;
	private String refNo;
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public int getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(int branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getMergeMakerDt() {
		return mergeMakerDt;
	}
	public void setMergeMakerDt(String mergeMakerDt) {
		this.mergeMakerDt = mergeMakerDt;
	}
	public String getMergeMaker() {
		return mergeMaker;
	}
	public void setMergeMaker(String mergeMaker) {
		this.mergeMaker = mergeMaker;
	}
	public String getMergeChecker() {
		return mergeChecker;
	}
	public void setMergeChecker(String mergeChecker) {
		this.mergeChecker = mergeChecker;
	}
	public String getCloseMaker() {
		return closeMaker;
	}
	public void setCloseMaker(String closeMaker) {
		this.closeMaker = closeMaker;
	}
	public String getCloseChecker() {
		return closeChecker;
	}
	public void setCloseChecker(String closeChecker) {
		this.closeChecker = closeChecker;
	}
	public String getLastMakerDt() {
		return lastMakerDt;
	}
	public void setLastMakerDt(String lastMakerDt) {
		this.lastMakerDt = lastMakerDt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
