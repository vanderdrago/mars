package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class JoinTrnRTGSMCBInMstr {

	public String id_upload1_time;
		
	private String valuedate;
	
	private String relTRN;
	
	private String TRN;
	
	private String transcode;
	
	private String bor;
	
	private String osn;
	
	private String fromaccnum;
	
	private String fromaccname;
	
	private String toaccnum;
	
	private String toaccname;
	
	private String ultimatebeneacc;
	
	private String ultimatebenename;
	
	private String amount;
	
	private String payDetails;
	
	private String status;
	
	private String saktinum;
	
	private String frommember;
	
	private String tomember;
	
	private BigDecimal nominal;
	
public String trn_dt;
	
	public String trn_ref_no;
	
	public String event;
	
	public String module;
	
	public String ac_branch;
	
	public String trn_code;
	
	public String batch_no;
	
	public String user_id;
	
	public String auth_id;
	
	public String auth_stat;
	
	public String ac_no;
	
	public String ac_desc;

	public String drcr_ind;
	
	public BigDecimal lcy_amount;

	public String getId_upload1_time() {
		return id_upload1_time;
	}

	public void setId_upload1_time(String idUpload1Time) {
		id_upload1_time = idUpload1Time;
	}

	public String getValuedate() {
		return valuedate;
	}

	public void setValuedate(String valuedate) {
		this.valuedate = valuedate;
	}

	public String getRelTRN() {
		return relTRN;
	}

	public void setRelTRN(String relTRN) {
		this.relTRN = relTRN;
	}

	public String getTRN() {
		return TRN;
	}

	public void setTRN(String tRN) {
		TRN = tRN;
	}

	public String getTranscode() {
		return transcode;
	}

	public void setTranscode(String transcode) {
		this.transcode = transcode;
	}

	public String getBor() {
		return bor;
	}

	public void setBor(String bor) {
		this.bor = bor;
	}

	public String getOsn() {
		return osn;
	}

	public void setOsn(String osn) {
		this.osn = osn;
	}

	public String getFromaccnum() {
		return fromaccnum;
	}

	public void setFromaccnum(String fromaccnum) {
		this.fromaccnum = fromaccnum;
	}

	public String getFromaccname() {
		return fromaccname;
	}

	public void setFromaccname(String fromaccname) {
		this.fromaccname = fromaccname;
	}

	public String getToaccnum() {
		return toaccnum;
	}

	public void setToaccnum(String toaccnum) {
		this.toaccnum = toaccnum;
	}

	public String getToaccname() {
		return toaccname;
	}

	public void setToaccname(String toaccname) {
		this.toaccname = toaccname;
	}

	public String getUltimatebeneacc() {
		return ultimatebeneacc;
	}

	public void setUltimatebeneacc(String ultimatebeneacc) {
		this.ultimatebeneacc = ultimatebeneacc;
	}

	public String getUltimatebenename() {
		return ultimatebenename;
	}

	public void setUltimatebenename(String ultimatebenename) {
		this.ultimatebenename = ultimatebenename;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPayDetails() {
		return payDetails;
	}

	public void setPayDetails(String payDetails) {
		this.payDetails = payDetails;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSaktinum() {
		return saktinum;
	}

	public void setSaktinum(String saktinum) {
		this.saktinum = saktinum;
	}

	public String getFrommember() {
		return frommember;
	}

	public void setFrommember(String frommember) {
		this.frommember = frommember;
	}

	public String getTomember() {
		return tomember;
	}

	public void setTomember(String tomember) {
		this.tomember = tomember;
	}

	public BigDecimal getNominal() {
		return nominal;
	}

	public void setNominal(BigDecimal nominal) {
		this.nominal = nominal;
	}

	public String getTrn_dt() {
		return trn_dt;
	}

	public void setTrn_dt(String trnDt) {
		trn_dt = trnDt;
	}

	public String getTrn_ref_no() {
		return trn_ref_no;
	}

	public void setTrn_ref_no(String trnRefNo) {
		trn_ref_no = trnRefNo;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getAc_branch() {
		return ac_branch;
	}

	public void setAc_branch(String acBranch) {
		ac_branch = acBranch;
	}

	public String getTrn_code() {
		return trn_code;
	}

	public void setTrn_code(String trnCode) {
		trn_code = trnCode;
	}

	public String getBatch_no() {
		return batch_no;
	}

	public void setBatch_no(String batchNo) {
		batch_no = batchNo;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String userId) {
		user_id = userId;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String authId) {
		auth_id = authId;
	}

	public String getAuth_stat() {
		return auth_stat;
	}

	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}

	public String getAc_no() {
		return ac_no;
	}

	public void setAc_no(String acNo) {
		ac_no = acNo;
	}

	public String getAc_desc() {
		return ac_desc;
	}

	public void setAc_desc(String acDesc) {
		ac_desc = acDesc;
	}

	public String getDrcr_ind() {
		return drcr_ind;
	}

	public void setDrcr_ind(String drcrInd) {
		drcr_ind = drcrInd;
	}

	public BigDecimal getLcy_amount() {
		return lcy_amount;
	}

	public void setLcy_amount(BigDecimal lcyAmount) {
		lcy_amount = lcyAmount;
	}
	
}
