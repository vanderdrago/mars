package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class DetailJadang {
	
	private String InstallmentDate;
	private BigDecimal amountPrincipal;
	private BigDecimal marginAmount;
	private BigDecimal principalRepayment;
	private BigDecimal totalRepayment;
	private int rate;
	private int tenor;
	
	
	public String getInstallmentDate() {
		return InstallmentDate;
	}
	public void setInstallmentDate(String installmentDate) {
		InstallmentDate = installmentDate;
	}
	public BigDecimal getAmountPrincipal() {
		return amountPrincipal;
	}
	public void setAmountPrincipal(BigDecimal amountPrincipal) {
		this.amountPrincipal = amountPrincipal;
	}
	public BigDecimal getMarginAmount() {
		return marginAmount;
	}
	public void setMarginAmount(BigDecimal marginAmount) {
		this.marginAmount = marginAmount;
	}
	public BigDecimal getPrincipalRepayment() {
		return principalRepayment;
	}
	public void setPrincipalRepayment(BigDecimal principalRepayment) {
		this.principalRepayment = principalRepayment;
	}
	public BigDecimal getTotalRepayment() {
		return totalRepayment;
	}
	public void setTotalRepayment(BigDecimal totalRepayment) {
		this.totalRepayment = totalRepayment;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public int getTenor() {
		return tenor;
	}
	public void setTenor(int tenor) {
		this.tenor = tenor;
	}
	
	

}
