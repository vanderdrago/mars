package com.muamalat.reportmcb.entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CB_TRANSFERS_DOM")
public class CbTransfersDom implements Serializable {
	@Id
	@Column(name="TRX_ID")
	private long trxId;

	@Column(name="REF_ID")
	private String refId;

	@Column(name="BANK_REF_NO")
	private String bankRefNo;

	@Column(name="BANK_ID")
	private BigDecimal bankId;

	@Column(name="COMPANY_ID")
	private BigDecimal companyId;

	@Column(name="ACCOUNT_NO")
	private String accountNo;

	@Column(name="TO_ACCOUNT_NO")
	private String toAccountNo;

	@Column(name="TO_NAME")
	private String toName;

	@Column(name="TO_BANK_NAME")
	private String toBankName;

	private String message;

	private BigDecimal amount;

	@Column(name="TO_ADDRESS_1")
	private String toAddress1;

	@Column(name="TO_ADDRESS_2")
	private String toAddress2;

	@Column(name="TO_ADDRESS_3")
	private String toAddress3;

	@Column(name="TO_ADDRESS_4")
	private String toAddress4;

	@Column(name="TO_POSTAL_CODE")
	private String toPostalCode;

	@Column(name="TO_EMAIL")
	private String toEmail;

	private BigDecimal residence;

	private BigDecimal nationality;

	private String status;

	@Column(name="RESPONSE_CODE")
	private String responseCode;

	@Column(name="TRANSFER_TYPE")
	private String transferType;

	@Column(name="DATE_TRX")
	private Date dateTrx;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

	private String stan;

	@Column(name="BULK_DETAIL_ID")
	private BigDecimal bulkDetailId;

	@Column(name="BNF_TRANSFER_ID")
	private BigDecimal bnfTransferId;

	private BigDecimal charge;
	
	private String companyCode;
	
	private String name ;
	
	private String name_e ;
	


	private static final long serialVersionUID = 1L;

	public CbTransfersDom() {
		super();
	}

	public long getTrxId() {
		return this.trxId;
	}

	public void setTrxId(long trxId) {
		this.trxId = trxId;
	}

	public String getRefId() {
		return this.refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getBankRefNo() {
		return this.bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public BigDecimal getBankId() {
		return this.bankId;
	}

	public void setBankId(BigDecimal bankId) {
		this.bankId = bankId;
	}

	public BigDecimal getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}

	public String getAccountNo() {
		return this.accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getToAccountNo() {
		return this.toAccountNo;
	}

	public void setToAccountNo(String toAccountNo) {
		this.toAccountNo = toAccountNo;
	}

	public String getToName() {
		return this.toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getToBankName() {
		return this.toBankName;
	}

	public void setToBankName(String toBankName) {
		this.toBankName = toBankName;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getToAddress1() {
		return this.toAddress1;
	}

	public void setToAddress1(String toAddress1) {
		this.toAddress1 = toAddress1;
	}

	public String getToAddress2() {
		return this.toAddress2;
	}

	public void setToAddress2(String toAddress2) {
		this.toAddress2 = toAddress2;
	}

	public String getToAddress3() {
		return this.toAddress3;
	}

	public void setToAddress3(String toAddress3) {
		this.toAddress3 = toAddress3;
	}

	public String getToAddress4() {
		return this.toAddress4;
	}

	public void setToAddress4(String toAddress4) {
		this.toAddress4 = toAddress4;
	}

	public String getToPostalCode() {
		return this.toPostalCode;
	}

	public void setToPostalCode(String toPostalCode) {
		this.toPostalCode = toPostalCode;
	}

	public String getToEmail() {
		return this.toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public BigDecimal getResidence() {
		return this.residence;
	}

	public void setResidence(BigDecimal residence) {
		this.residence = residence;
	}

	public BigDecimal getNationality() {
		return this.nationality;
	}

	public void setNationality(BigDecimal nationality) {
		this.nationality = nationality;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getTransferType() {
		return this.transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public Date getDateTrx() {
		return this.dateTrx;
	}

	public void setDateTrx(Date dateTrx) {
		this.dateTrx = dateTrx;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getStan() {
		return this.stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public BigDecimal getBulkDetailId() {
		return this.bulkDetailId;
	}

	public void setBulkDetailId(BigDecimal bulkDetailId) {
		this.bulkDetailId = bulkDetailId;
	}

	public BigDecimal getBnfTransferId() {
		return this.bnfTransferId;
	}

	public void setBnfTransferId(BigDecimal bnfTransferId) {
		this.bnfTransferId = bnfTransferId;
	}

	public BigDecimal getCharge() {
		return this.charge;
	}

	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName_e() {
		return name_e;
	}

	public void setName_e(String name_e) {
		this.name_e = name_e;
	}

	

}
