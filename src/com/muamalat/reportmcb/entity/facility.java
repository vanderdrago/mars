package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class facility {

	private String noFasilitas;
	private String namaNasabah;
	private String namaFasilitas;
	private String fasilitasKe;
	private String ccy;
	private BigDecimal limitAmount;
	private BigDecimal availableAmount;
	private BigDecimal utilization;
	private String startDate;
	private String expiryDate;
	private String recordStat;
	
	private String noKartu;
	private String prodCode;
	private String prodCategory;
	private String tglPencairan;
	private String tglJatuhTempo;
	private String statusKartu;
	private String noRekAngsuran;
	private int lineSerial;
	private BigDecimal blockAmount;
	
	public int getLineSerial() {
		return lineSerial;
	}
	public void setLineSerial(int lineSerial) {
		this.lineSerial = lineSerial;
	}
	public String getNoRekAngsuran() {
		return noRekAngsuran;
	}
	public void setNoRekAngsuran(String noRekAngsuran) {
		this.noRekAngsuran = noRekAngsuran;
	}
	public String getNoKartu() {
		return noKartu;
	}
	public void setNoKartu(String noKartu) {
		this.noKartu = noKartu;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	public String getProdCategory() {
		return prodCategory;
	}
	public void setProdCategory(String prodCategory) {
		this.prodCategory = prodCategory;
	}
	public String getTglPencairan() {
		return tglPencairan;
	}
	public void setTglPencairan(String tglPencairan) {
		this.tglPencairan = tglPencairan;
	}
	public String getTglJatuhTempo() {
		return tglJatuhTempo;
	}
	public void setTglJatuhTempo(String tglJatuhTempo) {
		this.tglJatuhTempo = tglJatuhTempo;
	}
	public String getStatusKartu() {
		return statusKartu;
	}
	public void setStatusKartu(String statusKartu) {
		this.statusKartu = statusKartu;
	}
	public String getNoFasilitas() {
		return noFasilitas;
	}
	public void setNoFasilitas(String noFasilitas) {
		this.noFasilitas = noFasilitas;
	}
	public String getNamaNasabah() {
		return namaNasabah;
	}
	public void setNamaNasabah(String namaNasabah) {
		this.namaNasabah = namaNasabah;
	}
	public String getNamaFasilitas() {
		return namaFasilitas;
	}
	public void setNamaFasilitas(String namaFasilitas) {
		this.namaFasilitas = namaFasilitas;
	}
	public String getFasilitasKe() {
		return fasilitasKe;
	}
	public void setFasilitasKe(String fasilitasKe) {
		this.fasilitasKe = fasilitasKe;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getLimitAmount() {
		return limitAmount;
	}
	public void setLimitAmount(BigDecimal limitAmount) {
		this.limitAmount = limitAmount;
	}
	public BigDecimal getAvailableAmount() {
		return availableAmount;
	}
	public void setAvailableAmount(BigDecimal availableAmount) {
		this.availableAmount = availableAmount;
	}
	public BigDecimal getUtilization() {
		return utilization;
	}
	public void setUtilization(BigDecimal utilization) {
		this.utilization = utilization;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getRecordStat() {
		return recordStat;
	}
	public void setRecordStat(String recordStat) {
		this.recordStat = recordStat;
	}
	public BigDecimal getBlockAmount() {
		return blockAmount;
	}
	public void setBlockAmount(BigDecimal blockAmount) {
		this.blockAmount = blockAmount;
	}
	
	
}
