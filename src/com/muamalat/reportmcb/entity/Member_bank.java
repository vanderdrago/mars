package com.muamalat.reportmcb.entity;

public class Member_bank {
	private String kode_member;
	private String nama_member;
	private String codename;
	public String getKode_member() {
		return kode_member;
	}
	public void setKode_member(String kode_member) {
		this.kode_member = kode_member;
	}
	public String getNama_member() {
		return nama_member;
	}
	public void setNama_member(String nama_member) {
		this.nama_member = nama_member;
	}
	public String getCodename() {
		return codename;
	}
	public void setCodename(String codename) {
		this.codename = codename;
	}
}
