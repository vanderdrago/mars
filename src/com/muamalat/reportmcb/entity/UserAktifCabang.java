package com.muamalat.reportmcb.entity;

import java.sql.Date;

public class UserAktifCabang {
	private String kode_cabang;
	private String tanggal;
	private String user_id;
	private int totrow;
	private String cabang;
	private String jam;
	
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getCabang() {
		return cabang;
	}
	public void setCabang(String cabang) {
		this.cabang = cabang;
	}
	public int getTotrow() {
		return totrow;
	}
	public void setTotrow(int totrow) {
		this.totrow = totrow;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getKode_cabang() {
		return kode_cabang;
	}
	public void setKode_cabang(String kode_cabang) {
		this.kode_cabang = kode_cabang;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
}
