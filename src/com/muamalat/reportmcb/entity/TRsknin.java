package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class TRsknin {
	
	public String id_upload1_time;
	
	public String id_ref_no;
	
	public String id_sender_name;
	
	public String id_benef_account;
	
	public String id_benef_name;
	
	public String id_remark;
	
	public String id_amount;
	
	public String id_sender_region_code;
	
	public String id_benef_region_code;
	
	public String id_sender_clearing_code;
	
	public String id_sor;
	
	public String id_benef_account_name;
	
	public String id_br_code;
	
	public String id_reject_desc; 
	
	public String id_status_host;
	
	public String id_verify_user;
	
	public String id_auth_user;

	public String getId_upload1_time() {
		return id_upload1_time;
	}

	public void setId_upload1_time(String idUpload1Time) {
		id_upload1_time = idUpload1Time;
	}

	public String getId_ref_no() {
		return id_ref_no;
	}
	
	public void setId_ref_no(String idRefNo) {
		id_ref_no = idRefNo;
	}

	public String getId_sender_name() {
		return id_sender_name;
	}

	public void setId_sender_name(String idSenderName) {
		id_sender_name = idSenderName;
	}

	public String getId_benef_account() {
		return id_benef_account;
	}

	public void setId_benef_account(String idBenefAccount) {
		id_benef_account = idBenefAccount;
	}

	public String getId_benef_name() {
		return id_benef_name;
	}

	public void setId_benef_name(String idBenefName) {
		id_benef_name = idBenefName;
	}

	public String getId_remark() {
		return id_remark;
	}

	public void setId_remark(String idRemark) {
		id_remark = idRemark;
	}

	public String getId_amount() {
		return id_amount;
	}

	public void setId_amount(String idAmount) {
		id_amount = idAmount;
	}

	public String getId_sender_region_code() {
		return id_sender_region_code;
	}

	public void setId_sender_region_code(String idSenderRegionCode) {
		id_sender_region_code = idSenderRegionCode;
	}

	public String getId_benef_region_code() {
		return id_benef_region_code;
	}

	public void setId_benef_region_code(String idBenefRegionCode) {
		id_benef_region_code = idBenefRegionCode;
	}

	public String getId_sender_clearing_code() {
		return id_sender_clearing_code;
	}

	public void setId_sender_clearing_code(String idSenderClearingCode) {
		id_sender_clearing_code = idSenderClearingCode;
	}

	public String getId_sor() {
		return id_sor;
	}

	public void setId_sor(String idSor) {
		id_sor = idSor;
	}

	public String getId_benef_account_name() {
		return id_benef_account_name;
	}

	public void setId_benef_account_name(String idBenefAccountName) {
		id_benef_account_name = idBenefAccountName;
	}

	public String getId_br_code() {
		return id_br_code;
	}

	public void setId_br_code(String idBrCode) {
		id_br_code = idBrCode;
	}

	public String getId_reject_desc() {
		return id_reject_desc;
	}

	public void setId_reject_desc(String idRejectDesc) {
		id_reject_desc = idRejectDesc;
	}

	public String getId_status_host() {
		return id_status_host;
	}

	public void setId_status_host(String idStatusHost) {
		id_status_host = idStatusHost;
	}

	public String getId_verify_user() {
		return id_verify_user;
	}

	public void setId_verify_user(String idVerifyUser) {
		id_verify_user = idVerifyUser;
	}

	public String getId_auth_user() {
		return id_auth_user;
	}

	public void setId_auth_user(String idAuthUser) {
		id_auth_user = idAuthUser;
	}

}
