package com.muamalat.reportmcb.entity;

import java.sql.Date;

public class tes {

	private String trnRefNo;
	private String branch;
	private String acNo;
	private String ccy;
	private String drcr;
	private String trnCode;
	private Number fcyAmount;
	private String exchRate;
	private String lcyAmount;
	private Number fcyBalance;
	private String trnDate;
	private String valueDt;
	private String financialCyle;
	private String periodCode;
	private String custGl;
	private String module;
	private String acEntry;
	private String userId;
	private String authId;
	private String product;
	private String entrySeq;
	private String txnDtTime;
	public String getTrnRefNo() {
		return trnRefNo;
	}
	public void setTrnRefNo(String trnRefNo) {
		this.trnRefNo = trnRefNo;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAcNo() {
		return acNo;
	}
	public void setAcNo(String acNo) {
		this.acNo = acNo;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getDrcr() {
		return drcr;
	}
	public void setDrcr(String drcr) {
		this.drcr = drcr;
	}
	public String getTrnCode() {
		return trnCode;
	}
	public void setTrnCode(String trnCode) {
		this.trnCode = trnCode;
	}
	public Number getFcyAmount() {
		return fcyAmount;
	}
	public void setFcyAmount(Number fcyAmount) {
		this.fcyAmount = fcyAmount;
	}
	public String getExchRate() {
		return exchRate;
	}
	public void setExchRate(String exchRate) {
		this.exchRate = exchRate;
	}
	public String getLcyAmount() {
		return lcyAmount;
	}
	public void setLcyAmount(String lcyAmount) {
		this.lcyAmount = lcyAmount;
	}
	public Number getFcyBalance() {
		return fcyBalance;
	}
	public void setFcyBalance(Number fcyBalance) {
		this.fcyBalance = fcyBalance;
	}
	public String getTrnDate() {
		return trnDate;
	}
	public void setTrnDate(String trnDate) {
		this.trnDate = trnDate;
	}
	public String getValueDt() {
		return valueDt;
	}
	public void setValueDt(String valueDt) {
		this.valueDt = valueDt;
	}
	public String getFinancialCyle() {
		return financialCyle;
	}
	public void setFinancialCyle(String financialCyle) {
		this.financialCyle = financialCyle;
	}
	public String getPeriodCode() {
		return periodCode;
	}
	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}
	public String getCustGl() {
		return custGl;
	}
	public void setCustGl(String custGl) {
		this.custGl = custGl;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getAcEntry() {
		return acEntry;
	}
	public void setAcEntry(String acEntry) {
		this.acEntry = acEntry;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAuthId() {
		return authId;
	}
	public void setAuthId(String authId) {
		this.authId = authId;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getEntrySeq() {
		return entrySeq;
	}
	public void setEntrySeq(String entrySeq) {
		this.entrySeq = entrySeq;
	}
	public String getTxnDtTime() {
		return txnDtTime;
	}
	public void setTxnDtTime(String txnDtTime) {
		this.txnDtTime = txnDtTime;
	}
	
	
}
