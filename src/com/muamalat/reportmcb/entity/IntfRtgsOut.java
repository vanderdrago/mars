package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class IntfRtgsOut {
	private String dataId;
	private String bor;
	private String valuedate;
	private String rtstatus;
	private String status;
	private String rsncde;
	private String tomember;
	private String fomember;
	private String trn;
	private String reltrn;
	private String oriname; 
	private String ultimatebenename;
	private String ultimatebeneacc;
	private BigDecimal amount;
	private String fromaccname;
	private String toaccname;
	private String paydetails;
	private String settletime;	
	private int tottrans;
	private BigDecimal totnominal;
	
	
	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public String getBor() {
		return bor;
	}
	public void setBor(String bor) {
		this.bor = bor;
	}
	public String getValuedate() {
		return valuedate;
	}
	public void setValuedate(String valuedate) {
		this.valuedate = valuedate;
	}
	public String getRtstatus() {
		return rtstatus;
	}
	public void setRtstatus(String rtstatus) {
		this.rtstatus = rtstatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRsncde() {
		return rsncde;
	}
	public void setRsncde(String rsncde) {
		this.rsncde = rsncde;
	}
	public String getTomember() {
		return tomember;
	}
	public void setTomember(String tomember) {
		this.tomember = tomember;
	}
	public String getFomember() {
		return fomember;
	}
	public void setFomember(String fomember) {
		this.fomember = fomember;
	}
	public String getTrn() {
		return trn;
	}
	public void setTrn(String trn) {
		this.trn = trn;
	}
	public String getReltrn() {
		return reltrn;
	}
	public void setReltrn(String reltrn) {
		this.reltrn = reltrn;
	}
	public String getOriname() {
		return oriname;
	}
	public void setOriname(String oriname) {
		this.oriname = oriname;
	}
	public String getUltimatebenename() {
		return ultimatebenename;
	}
	public void setUltimatebenename(String ultimatebenename) {
		this.ultimatebenename = ultimatebenename;
	}
	public String getUltimatebeneacc() {
		return ultimatebeneacc;
	}
	public void setUltimatebeneacc(String ultimatebeneacc) {
		this.ultimatebeneacc = ultimatebeneacc;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getFromaccname() {
		return fromaccname;
	}
	public void setFromaccname(String fromaccname) {
		this.fromaccname = fromaccname;
	}
	public String getToaccname() {
		return toaccname;
	}
	public void setToaccname(String toaccname) {
		this.toaccname = toaccname;
	}
	public String getPaydetails() {
		return paydetails;
	}
	public void setPaydetails(String paydetails) {
		this.paydetails = paydetails;
	}
	public String getSettletime() {
		return settletime;
	}
	public void setSettletime(String settletime) {
		this.settletime = settletime;
	}
	public int getTottrans() {
		return tottrans;
	}
	public void setTottrans(int tottrans) {
		this.tottrans = tottrans;
	}
	public BigDecimal getTotnominal() {
		return totnominal;
	}
	public void setTotnominal(BigDecimal totnominal) {
		this.totnominal = totnominal;
	}
	
}
