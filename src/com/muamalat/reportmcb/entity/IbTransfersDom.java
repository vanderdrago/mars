package com.muamalat.reportmcb.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the IB_TRANSFERS_DOM database table.
 * 
 */
@Entity
@Table(name="IB_TRANSFERS_DOM")
public class IbTransfersDom implements Serializable {
	private static final long serialVersionUID = 1L;

//	@Id
//	@Column(name="TRX_ID")
//	private long trxId;

	@Column(name="ACCOUNT_NO")
	private String accountNo;

	private BigDecimal amount;

	@Column(name="BANK_ID")
	private String bankId;

	@Column(name="BANK_REF_NO")
	private String bankRefNo;

	private BigDecimal charge;

    @Temporal( TemporalType.DATE)
	@Column(name="DATE_TRX")
	private Date dateTrx;

	private String message;

	@Column(name="REF_ID")
	private String refId;

	@Column(name="RESPONSE_CODE")
	private String responseCode;

	private String status;

	@Column(name="TO_ACCOUNT_NO")
	private String toAccountNo;

	@Column(name="TO_BANK_NAME")
	private String toBankName;

	@Column(name="TO_NAME")
	private String toName;

	@Column(name="TRANSFER_TYPE")
	private String transferType;

	@Column(name="USER_ID")
	private String userId;
	
	@Column(name="REF_NO")
	private String refNo;
	
	@Column(name="NAME_E")
	private String nameE;
	
	public IbTransfersDom() {
    }

//	public long getTrxId() {
//		return trxId;
//	}
//
//	public void setTrxId(long trxId) {
//		this.trxId = trxId;
//	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankRefNo() {
		return bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public BigDecimal getCharge() {
		return charge;
	}

	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}

	public Date getDateTrx() {
		return dateTrx;
	}

	public void setDateTrx(Date dateTrx) {
		this.dateTrx = dateTrx;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getToAccountNo() {
		return toAccountNo;
	}

	public void setToAccountNo(String toAccountNo) {
		this.toAccountNo = toAccountNo;
	}

	public String getToBankName() {
		return toBankName;
	}

	public void setToBankName(String toBankName) {
		this.toBankName = toBankName;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getNameE() {
		return nameE;
	}

	public void setNameE(String nameE) {
		this.nameE = nameE;
	}

}