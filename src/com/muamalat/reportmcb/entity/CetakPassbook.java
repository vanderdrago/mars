package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class CetakPassbook {
	private int no;
	private String no_s;
	private BigDecimal mutasi;
	private String mutasi_s;
	private BigDecimal lcy_amount;
	private BigDecimal fcy_amount;
	private BigDecimal saldo;
	private String saldo_s;
	private Date trn_dt;
	private String trn_dt_s;
	private String value_dt;
	private String drcr_ind;
	private String trn_ref_no;
	private String event;
	private String detail;
	private String trn_code;
	private BigDecimal ac_entry_sr_no;
	private String auth_id;
	private String rbtn;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getNo_s() {
		return no_s;
	}
	public void setNo_s(String no_s) {
		this.no_s = no_s;
	}
	public BigDecimal getMutasi() {
		return mutasi;
	}
	public void setMutasi(BigDecimal mutasi) {
		this.mutasi = mutasi;
	}
	public String getMutasi_s() {
		return mutasi_s;
	}
	public void setMutasi_s(String mutasi_s) {
		this.mutasi_s = mutasi_s;
	}
	public BigDecimal getLcy_amount() {
		return lcy_amount;
	}
	public void setLcy_amount(BigDecimal lcy_amount) {
		this.lcy_amount = lcy_amount;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public String getSaldo_s() {
		return saldo_s;
	}
	public void setSaldo_s(String saldo_s) {
		this.saldo_s = saldo_s;
	}
	public BigDecimal getFcy_amount() {
		return fcy_amount;
	}
	public void setFcy_amount(BigDecimal fcy_amount) {
		this.fcy_amount = fcy_amount;
	}
	public Date getTrn_dt() {
		return trn_dt;
	}
	public void setTrn_dt(Date trn_dt) {
		this.trn_dt = trn_dt;
	}
	public String getTrn_dt_s() {
		return trn_dt_s;
	}
	public void setTrn_dt_s(String trn_dt_s) {
		this.trn_dt_s = trn_dt_s;
	}
	public String getValue_dt() {
		return value_dt;
	}
	public void setValue_dt(String value_dt) {
		this.value_dt = value_dt;
	}
	public String getDrcr_ind() {
		return drcr_ind;
	}
	public void setDrcr_ind(String drcr_ind) {
		this.drcr_ind = drcr_ind;
	}
	public String getTrn_ref_no() {
		return trn_ref_no;
	}
	public void setTrn_ref_no(String trn_ref_no) {
		this.trn_ref_no = trn_ref_no;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getTrn_code() {
		return trn_code;
	}
	public void setTrn_code(String trn_code) {
		this.trn_code = trn_code;
	}
	public BigDecimal getAc_entry_sr_no() {
		return ac_entry_sr_no;
	}
	public void setAc_entry_sr_no(BigDecimal ac_entry_sr_no) {
		this.ac_entry_sr_no = ac_entry_sr_no;
	}
	public String getAuth_id() {
		return auth_id;
	}
	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getRbtn() {
		return rbtn;
	}
	public void setRbtn(String rbtn) {
		this.rbtn = rbtn;
	}
		

}
