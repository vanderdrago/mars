package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Customer {
	private String branch_cd;
	private String branch_name;
	private String branch_addrs;
	private String cust_acc;
	private String cust_name;
	private String cust_no;
	private String record_stat;
	private String auth_stat;
	private String cust_acc_clas;
	private String cust_acc_clas_desc;
	private String cust_ccy;
	private String cust_addrs;
	private String cust_email;
	private String cust_email_cc;
	private String cust_type;
	private String sex;
	private String ibu_kndng;
	private String s_text;
	private String dt_of_birth;
	private String user_input;
	private String user_aut;
	private BigDecimal sld_hold;
	private BigDecimal sld_float;
	private BigDecimal sld_avg;
	private BigDecimal sld_min;
	
	public String getBranch_cd() {
		return branch_cd;
	}
	public void setBranch_cd(String branch_cd) {
		this.branch_cd = branch_cd;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getBranch_addrs() {
		return branch_addrs;
	}
	public void setBranch_addrs(String branch_addrs) {
		this.branch_addrs = branch_addrs;
	}
	public String getCust_acc() {
		return cust_acc;
	}
	public void setCust_acc(String cust_acc) {
		this.cust_acc = cust_acc;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}
	public String getRecord_stat() {
		return record_stat;
	}
	public void setRecord_stat(String record_stat) {
		this.record_stat = record_stat;
	}
	public String getAuth_stat() {
		return auth_stat;
	}
	public void setAuth_stat(String auth_stat) {
		this.auth_stat = auth_stat;
	}
	public String getCust_acc_clas() {
		return cust_acc_clas;
	}
	public void setCust_acc_clas(String cust_acc_clas) {
		this.cust_acc_clas = cust_acc_clas;
	}
	public String getCust_acc_clas_desc() {
		return cust_acc_clas_desc;
	}
	public void setCust_acc_clas_desc(String cust_acc_clas_desc) {
		this.cust_acc_clas_desc = cust_acc_clas_desc;
	}
	public String getCust_ccy() {
		return cust_ccy;
	}
	public void setCust_ccy(String cust_ccy) {
		this.cust_ccy = cust_ccy;
	}
	public String getCust_addrs() {
		return cust_addrs;
	}
	public void setCust_addrs(String cust_addrs) {
		this.cust_addrs = cust_addrs;
	}
	public String getCust_email() {
		return cust_email;
	}
	public void setCust_email(String cust_email) {
		this.cust_email = cust_email;
	}
	public String getCust_email_cc() {
		return cust_email_cc;
	}
	public void setCust_email_cc(String cust_email_cc) {
		this.cust_email_cc = cust_email_cc;
	}
	public String getCust_type() {
		return cust_type;
	}
	public void setCust_type(String cust_type) {
		this.cust_type = cust_type;
	}
	public BigDecimal getSld_hold() {
		return sld_hold;
	}
	public void setSld_hold(BigDecimal sld_hold) {
		this.sld_hold = sld_hold;
	}
	public String getSex() {
		return sex;
	}
	public String getIbu_kndng() {
		return ibu_kndng;
	}
	public void setIbu_kndng(String ibu_kndng) {
		this.ibu_kndng = ibu_kndng;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getS_text() {
		return s_text;
	}
	public void setS_text(String s_text) {
		this.s_text = s_text;
	}
	public String getDt_of_birth() {
		return dt_of_birth;
	}
	public void setDt_of_birth(String dt_of_birth) {
		this.dt_of_birth = dt_of_birth;
	}
	public String getUser_input() {
		return user_input;
	}
	public void setUser_input(String user_input) {
		this.user_input = user_input;
	}
	public String getUser_aut() {
		return user_aut;
	}
	public void setUser_aut(String user_aut) {
		this.user_aut = user_aut;
	}
	public BigDecimal getSld_float() {
		return sld_float;
	}
	public void setSld_float(BigDecimal sld_float) {
		this.sld_float = sld_float;
	}
	public BigDecimal getSld_avg() {
		return sld_avg;
	}
	public void setSld_avg(BigDecimal sld_avg) {
		this.sld_avg = sld_avg;
	}
	public BigDecimal getSld_min() {
		return sld_min;
	}
	public void setSld_min(BigDecimal sld_min) {
		this.sld_min = sld_min;
	}
	
	
}
