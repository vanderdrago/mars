package com.muamalat.reportmcb.entity;

public class quickCifPeriode {

	private String tglLaporan;
	private String customerNo;
	private String customerName;
	private String branchCode;
	private String branchName;
	private String functionId;
	private int modNo;
	private String firstMakerId;
	private String cifCreationDate;
	private String lastMakerId;
	private String lastOtoId;
	private String lastUpdate;
	private String status;
	private String makerDate;
		
	public String getmakerDate() {
		return makerDate;
	}
	public void setmakerDate(String makerDate) {
		this.makerDate = makerDate;
	}
	public String getFirstMakerId() {
		return firstMakerId;
	}
	public void setFirstMakerId(String firstMakerId) {
		this.firstMakerId = firstMakerId;
	}
	public String getCifCreationDate() {
		return cifCreationDate;
	}
	public void setCifCreationDate(String cifCreationDate) {
		this.cifCreationDate = cifCreationDate;
	}
	public String getLastMakerId() {
		return lastMakerId;
	}
	public void setLastMakerId(String lastMakerId) {
		this.lastMakerId = lastMakerId;
	}
	public String getLastOtoId() {
		return lastOtoId;
	}
	public void setLastOtoId(String lastOtoId) {
		this.lastOtoId = lastOtoId;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTglLaporan() {
		return tglLaporan;
	}
	public void setTglLaporan(String tglLaporan) {
		this.tglLaporan = tglLaporan;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	public int getModNo() {
		return modNo;
	}
	public void setModNo(int modNo) {
		this.modNo = modNo;
	}
	
}
