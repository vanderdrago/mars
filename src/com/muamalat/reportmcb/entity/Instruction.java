package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Instruction {

	private String instructionNo;
	private String userRefNumber;
	private String contractRefNo;
	private String productRemarks;
	private String productCode;
	private String productDesc;
	private String firstExecValueDate;
	private String firstExecDueDate;
	private String nextExecValueDate;
	private String nextExecDueDate;
	private String expiryDate;
	private BigDecimal amount;
	private String drBranch;
	private String drAccount;
	private String ccy;
	private String crBranch;
	private String crAccount;
	private String remarks;
	private String eventCode;
	
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public String getInstructionNo() {
		return instructionNo;
	}
	public void setInstructionNo(String instructionNo) {
		this.instructionNo = instructionNo;
	}
	public String getUserRefNumber() {
		return userRefNumber;
	}
	public void setUserRefNumber(String userRefNumber) {
		this.userRefNumber = userRefNumber;
	}
	public String getContractRefNo() {
		return contractRefNo;
	}
	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}
	public String getProductRemarks() {
		return productRemarks;
	}
	public void setProductRemarks(String productRemarks) {
		this.productRemarks = productRemarks;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductDesc() {
		return productDesc;
	}
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	public String getFirstExecValueDate() {
		return firstExecValueDate;
	}
	public void setFirstExecValueDate(String firstExecValueDate) {
		this.firstExecValueDate = firstExecValueDate;
	}
	public String getFirstExecDueDate() {
		return firstExecDueDate;
	}
	public void setFirstExecDueDate(String firstExecDueDate) {
		this.firstExecDueDate = firstExecDueDate;
	}
	public String getNextExecValueDate() {
		return nextExecValueDate;
	}
	public void setNextExecValueDate(String nextExecValueDate) {
		this.nextExecValueDate = nextExecValueDate;
	}
	public String getNextExecDueDate() {
		return nextExecDueDate;
	}
	public void setNextExecDueDate(String nextExecDueDate) {
		this.nextExecDueDate = nextExecDueDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getDrBranch() {
		return drBranch;
	}
	public void setDrBranch(String drBranch) {
		this.drBranch = drBranch;
	}
	public String getDrAccount() {
		return drAccount;
	}
	public void setDrAccount(String drAccount) {
		this.drAccount = drAccount;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getCrBranch() {
		return crBranch;
	}
	public void setCrBranch(String crBranch) {
		this.crBranch = crBranch;
	}
	public String getCrAccount() {
		return crAccount;
	}
	public void setCrAccount(String crAccount) {
		this.crAccount = crAccount;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
}
