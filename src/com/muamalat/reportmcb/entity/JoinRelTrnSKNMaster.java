package com.muamalat.reportmcb.entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PCTB_CONTRACT_MASTER")
public class JoinRelTrnSKNMaster implements Serializable {
	@Id
	@Column(name="CONTRACT_REF_NO")
	private String contractRefNo;

	@Column(name="PROD_REF_NO")
	private String prodRefNo;

	private String network;

	@Column(name="CUST_BANKCODE")
	private String custBankcode;
	
	@Column(name="PayDetails")
	private String payDetails;

	@Column(name="CUST_AC_NO")
	private String custAcNo;
	
	@Column(name="CUST_NAME")
	private String custName;

	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;

	@Column(name="CPTY_AC_NO")
	private String cptyAcNo;

	@Column(name="CPTY_NAME")
	private String cptyName;

	@Column(name="BOOKING_DT")
	private Date bookingDt;

	@Column(name="EXCEPTION_QUEUE")
	private String exceptionQueue;

	@Column(name="MAKER_ID")
	private String makerId;

	@Column(name="CHECKER_ID")
	private String checkerId;

	@Column(name="DISPATCH_REF_NO")
	private String dispatchRefNo;

	@Column(name="UDF_2")
	private String udf_2;
	
	private static final long serialVersionUID = 1L;

	public String getContractRefNo() {
		return contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public String getProdRefNo() {
		return prodRefNo;
	}

	public void setProdRefNo(String prodRefNo) {
		this.prodRefNo = prodRefNo;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getCustBankcode() {
		return custBankcode;
	}

	public void setCustBankcode(String custBankcode) {
		this.custBankcode = custBankcode;
	}

	public String getCustAcNo() {
		return custAcNo;
	}

	public void setCustAcNo(String custAcNo) {
		this.custAcNo = custAcNo;
	}
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getCptyAcNo() {
		return cptyAcNo;
	}

	public void setCptyAcNo(String cptyAcNo) {
		this.cptyAcNo = cptyAcNo;
	}

	public String getCptyName() {
		return cptyName;
	}

	public void setCptyName(String cptyName) {
		this.cptyName = cptyName;
	}

	public Date getBookingDt() {
		return bookingDt;
	}

	public void setBookingDt(Date bookingDt) {
		this.bookingDt = bookingDt;
	}

	public String getExceptionQueue() {
		return exceptionQueue;
	}

	public void setExceptionQueue(String exceptionQueue) {
		this.exceptionQueue = exceptionQueue;
	}

	public String getMakerId() {
		return makerId;
	}

	public void setMakerId(String makerId) {
		this.makerId = makerId;
	}

	public String getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(String checkerId) {
		this.checkerId = checkerId;
	}

	public String getDispatchRefNo() {
		return dispatchRefNo;
	}

	public void setDispatchRefNo(String dispatchRefNo) {
		this.dispatchRefNo = dispatchRefNo;
	}

	public String getUdf_2() {
		return udf_2;
	}

	public void setUdf_2(String udf_2) {
		this.udf_2 = udf_2;
	}

	public String getPayDetails() {
		return payDetails;
	}

	public void setPayDetails(String payDetails) {
		this.payDetails = payDetails;
	}

	public JoinRelTrnSKNMaster() {
		super();
	}

	@Column(name="Od_ref_no")
	private String od_ref_no;
	
	@Column(name="Od_bankno")
	private String od_bankno;
	
	@Column(name="Od_op_name")
	private String od_op_name;
	
	@Column(name="Od_accno")
	private String od_accno;
	
	@Column(name="Od_cu_name")
	private String od_cu_name;
	
	@Column(name="Od_desc")
	private String od_desc;
	
	@Column(name="Od_amount")
//	private String od_amount;
	private BigDecimal od_amount;
	
	@Column(name="Od_status1")
	private String od_status1;
	
	@Column(name="Od_status2")
	private String od_status2;

	public String getOd_ref_no() {
		return od_ref_no;
	}

	public void setOd_ref_no(String odRefNo) {
		od_ref_no = odRefNo;
	}

	public String getOd_bankno() {
		return od_bankno;
	}

	public void setOd_bankno(String odBankno) {
		od_bankno = odBankno;
	}

	public String getOd_op_name() {
		return od_op_name;
	}

	public void setOd_op_name(String odOpName) {
		od_op_name = odOpName;
	}

	public String getOd_accno() {
		return od_accno;
	}

	public void setOd_accno(String odAccno) {
		od_accno = odAccno;
	}

	public String getOd_cu_name() {
		return od_cu_name;
	}

	public void setOd_cu_name(String odCuName) {
		od_cu_name = odCuName;
	}

	public String getOd_desc() {
		return od_desc;
	}

	public void setOd_desc(String odDesc) {
		od_desc = odDesc;
	}

	public BigDecimal getOd_amount() {
		return od_amount;
	}

	public void setOd_amount(BigDecimal odAmount) {
		od_amount = odAmount;
	}

	public String getOd_status1() {
		return od_status1;
	}

	public void setOd_status1(String odStatus1) {
		od_status1 = odStatus1;
	}

	public String getOd_status2() {
		return od_status2;
	}

	public void setOd_status2(String odStatus2) {
		od_status2 = odStatus2;
	}
}
