package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Instalment {
	
	public String customer_id;
	public String prod_code;
	public String prod_ctgry;
	public String prod_desc;
	public String book_date;
	public String matur_date;
	public String account_number;
	public String prim_app_id;
	public String prim_app_name;
	public String no_of_installments;
	public BigDecimal total_sale_value;
	public BigDecimal total_sale_value2;
	public BigDecimal tempTotPkh;
	public BigDecimal upfront_profit_booked;
	public BigDecimal tempTotMrgnh;
	public BigDecimal amount_disbursed;
	public BigDecimal tempPaidh;
	public String account_status;
	public BigDecimal amount_financed;
	public BigDecimal tempTotUjroh;
	public BigDecimal ujroh;
	public BigDecimal denda;
	public BigDecimal tempTotDenda;
		
	public BigDecimal getUjroh() {
		return ujroh;
	}

	public void setUjroh(BigDecimal ujroh) {
		this.ujroh = ujroh;
	}

	public BigDecimal getTempTotUjroh() {
		return tempTotUjroh;
	}

	public void setTempTotUjroh(BigDecimal tempTotUjroh) {
		this.tempTotUjroh = tempTotUjroh;
	}

	public String getAccount_status() {
		return account_status;
	}

	public void setAccount_status(String account_status) {
		this.account_status = account_status;
	}

	public String getPrim_app_name() {
		return prim_app_name;
	}

	public void setPrim_app_name(String primAppName) {
		prim_app_name = primAppName;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customerId) {
		customer_id = customerId;
	}

	public String getProd_code() {
		return prod_code;
	}

	public void setProd_code(String prodCode) {
		prod_code = prodCode;
	}

	public String getProd_ctgry() {
		return prod_ctgry;
	}

	public void setProd_ctgry(String prodCtgry) {
		prod_ctgry = prodCtgry;
	}

	public String getBook_date() {
		return book_date;
	}

	public void setBook_date(String bookDate) {
		book_date = bookDate;
	}

	public String getMatur_date() {
		return matur_date;
	}

	public void setMatur_date(String maturDate) {
		matur_date = maturDate;
	}
	
	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String accountNumber) {
		account_number = accountNumber;
	}

	public String getPrim_app_id() {
		return prim_app_id;
	}

	public void setPrim_app_id(String primAppId) {
		prim_app_id = primAppId;
	}

	public String getNo_of_installments() {
		return no_of_installments;
	}

	public void setNo_of_installments(String noOfInstallments) {
		no_of_installments = noOfInstallments;
	}
	
	public String getProd_desc() {
		return prod_desc;
	}

	public void setProd_desc(String prodDesc) {
		prod_desc = prodDesc;
	}

	public BigDecimal getTotal_sale_value2() {
		return total_sale_value2;
	}

	public void setTotal_sale_value2(BigDecimal totalSaleValue2) {
		if(totalSaleValue2 == null){
			totalSaleValue2 = new BigDecimal(0);
		} else {
			total_sale_value2 = totalSaleValue2;
		}
	}
	
	public BigDecimal getTotal_sale_value() {
		return total_sale_value;
	}
	
	public void setTotal_sale_value(BigDecimal totalSaleValue) {
		if(totalSaleValue == null){
			totalSaleValue = new BigDecimal(0);
		} else {
			total_sale_value = totalSaleValue;
		}
	}

	public BigDecimal getUpfront_profit_booked() {
		return upfront_profit_booked;
	}

	public void setUpfront_profit_booked(BigDecimal upfrontProfitBooked) {
		if(upfrontProfitBooked == null){
			upfrontProfitBooked = new BigDecimal(0);
		} else {
			upfront_profit_booked = upfrontProfitBooked;
		}
	}

	public BigDecimal getAmount_disbursed() {
		return amount_disbursed;
	}

	public void setAmount_disbursed(BigDecimal amountDisbursed) {
		if(amountDisbursed == null){
			amountDisbursed = new BigDecimal(0);
		} else {
			amount_disbursed = amountDisbursed;
		}
	}
	
	
	
	public BigDecimal getAmount_financed() {
		return amount_financed;
	}

	public void setAmount_financed(BigDecimal amountFinanced) {
		if(amountFinanced == null){
			amountFinanced = new BigDecimal(0);
		} else {
			amount_financed = amountFinanced;
		}
	}

	public BigDecimal getTempTotPkh() {
		return tempTotPkh;
	}

	public void setTempTotPkh(BigDecimal tempTotPkh) {
		if(tempTotPkh == null){
			tempTotPkh = new BigDecimal(0);
		} else {
			this.tempTotPkh = tempTotPkh;
		}
	}

	public BigDecimal getTempTotMrgnh() {
		return tempTotMrgnh;
	}

	public void setTempTotMrgnh(BigDecimal tempTotMrgnh) {
		if(tempTotMrgnh == null){
			tempTotMrgnh = new BigDecimal(0);
		} else {
			this.tempTotMrgnh = tempTotMrgnh;
		}
	}

	public BigDecimal getTempPaidh() {
		return tempPaidh;
	}

	public void setTempPaidh(BigDecimal tempPaidh) {
		if(tempPaidh == null){
			tempPaidh = new BigDecimal(0);
		} else {
			this.tempPaidh = tempPaidh;
		}
	}

	public BigDecimal getDenda() {
		return denda;
	}

	public void setDenda(BigDecimal denda) {
		this.denda = denda;
	}

	public BigDecimal getTempTotDenda() {
		return tempTotDenda;
	}

	public void setTempTotDenda(BigDecimal tempTotDenda) {
		if(tempTotDenda == null){
			tempTotDenda = new BigDecimal(0);
		} else {
		this.tempTotDenda = tempTotDenda;
		}
	}
	
	
	
}
