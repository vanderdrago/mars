package com.muamalat.reportmcb.entity;

public class Admin {
	
    private String username;
    private String password;
    private int level;
    private String kodecabang;
    private String namacabang;
    
	public String getNamacabang() {
		return namacabang;
	}
	public void setNamacabang(String namacabang) {
		this.namacabang = namacabang;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getKodecabang() {
		return kodecabang;
	}
	public void setKodecabang(String kodecabang) {
		this.kodecabang = kodecabang;
	}

    
    
    
}
