package com.muamalat.reportmcb.entity;

public class LmtTrnsPssbk {
	private String product_cd;
	private int limit_trans;
	private int limit_per_page;
	private int limit_max_time;
	
	public String getProduct_cd() {
		return product_cd;
	}
	public void setProduct_cd(String product_cd) {
		this.product_cd = product_cd;
	}
	public int getLimit_trans() {
		return limit_trans;
	}
	public void setLimit_trans(int limit_trans) {
		this.limit_trans = limit_trans;
	}
	public int getLimit_per_page() {
		return limit_per_page;
	}
	public void setLimit_per_page(int limit_per_page) {
		this.limit_per_page = limit_per_page;
	}
	public int getLimit_max_time() {
		return limit_max_time;
	}
	public void setLimit_max_time(int limit_max_time) {
		this.limit_max_time = limit_max_time;
	}
	
}
