package com.muamalat.reportmcb.entity;

public class LaporanSpvCs {
	private String branch;
	private String tanggal;
	private String jam;
	private String key_id;
	private String maker_id;
	private String checker_id;
	private String function_id;
	private String modifikasi_ke;
	private String cust_ac_no;
	private String branch_name;
	
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getCust_ac_no() {
		return cust_ac_no;
	}
	public void setCust_ac_no(String cust_ac_no) {
		this.cust_ac_no = cust_ac_no;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getKey_id() {
		return key_id;
	}
	public void setKey_id(String key_id) {
		this.key_id = key_id;
	}
	public String getMaker_id() {
		return maker_id;
	}
	public void setMaker_id(String maker_id) {
		this.maker_id = maker_id;
	}
	public String getChecker_id() {
		return checker_id;
	}
	public void setChecker_id(String checker_id) {
		this.checker_id = checker_id;
	}
	public String getFunction_id() {
		return function_id;
	}
	public void setFunction_id(String function_id) {
		this.function_id = function_id;
	}
	public String getModifikasi_ke() {
		return modifikasi_ke;
	}
	public void setModifikasi_ke(String modifikasi_ke) {
		this.modifikasi_ke = modifikasi_ke;
	}
}
