package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Djadwalangsur {
	
	public String schedule_no;
	public String schedule_st_date;
    public String schedule_due_date;
    public String schedule_st_date_2;
    public String schedule_due_date_2;
    public BigDecimal principal;
    public BigDecimal profit;
    public BigDecimal tot_angsuran;
    public BigDecimal nompaid;
    public BigDecimal sisa_principal;
    public BigDecimal sisa_profit;
    public BigDecimal sisa_angsuran;
    public BigDecimal amount_settled;
    public BigDecimal amount_settled_2;
    public String rate;
    public String exec_dt;
    public String account_number;
    public String branch_code;
    public String exec_date;
    public String paid_date;
    public String nominal;
    public BigDecimal ujroh;
    public BigDecimal denda;
    
    public BigDecimal getAmount_settled_2() {
		return amount_settled_2;
	}

	public void setAmount_settled_2(BigDecimal amount_settled_2) {
		this.amount_settled_2 = amount_settled_2;
	}

	public String getSchedule_st_date_2() {
		return schedule_st_date_2;
	}

	public void setSchedule_st_date_2(String schedule_st_date_2) {
		this.schedule_st_date_2 = schedule_st_date_2;
	}

	public String getSchedule_due_date_2() {
		return schedule_due_date_2;
	}

	public void setSchedule_due_date_2(String schedule_due_date_2) {
		this.schedule_due_date_2 = schedule_due_date_2;
	}

	public BigDecimal getUjroh() {
		return ujroh;
	}

	public void setUjroh(BigDecimal ujroh) {
		this.ujroh = ujroh;
	}

	public String getSchedule_no() {
		return schedule_no;
	}

	public void setSchedule_no(String scheduleNo) {
		schedule_no = scheduleNo;
	}

	public String getSchedule_st_date() {
		return schedule_st_date;
	}

	public void setSchedule_st_date(String scheduleStDate) {
		schedule_st_date = scheduleStDate;
	}

	public String getSchedule_due_date() {
		return schedule_due_date;
	}

	public void setSchedule_due_date(String scheduleDueDate) {
		schedule_due_date = scheduleDueDate;
	}

	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		if (principal == null){
			this.principal = new BigDecimal(0);
		} else {
			this.principal = principal;
		}
	}
	
	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		if (profit == null){
			this.profit = new BigDecimal(0);
		} else {
			this.profit = profit;
		}
	}

	public BigDecimal getTot_angsuran() {
		return tot_angsuran;
	}

	public void setTot_angsuran(BigDecimal totAngsuran) {
		tot_angsuran = totAngsuran;
	}

	public BigDecimal getSisa_principal() {
		return sisa_principal;
	}

	public void setSisa_principal(BigDecimal sisaPrincipal) {
		sisa_principal = sisaPrincipal;
	}

	public BigDecimal getSisa_profit() {
		return sisa_profit;
	}

	public void setSisa_profit(BigDecimal sisaProfit) {
		sisa_profit = sisaProfit;
	}

	public BigDecimal getSisa_angsuran() {
		return sisa_angsuran;
	}

	public void setSisa_angsuran(BigDecimal sisaAngsuran) {
		sisa_angsuran = sisaAngsuran;
	}

	public BigDecimal getAmount_settled() {
		return amount_settled;
	}

	public void setAmount_settled(BigDecimal amountSettled) {
		if(amountSettled == null){
			amount_settled = new BigDecimal(0);
		} else {
			amount_settled = amountSettled;
		}
	}

	public BigDecimal getNompaid() {
		return nompaid;
	}

	public void setNompaid(BigDecimal nompaid) {
		if(nompaid == null){
			this.nompaid = new BigDecimal(0);
		} else {
			this.nompaid = nompaid;
		}
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getExec_dt() {
		return exec_dt;
	}

	public void setExec_dt(String execDt) {
		exec_dt = execDt;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String accountNumber) {
		account_number = accountNumber;
	}

	public String getBranch_code() {
		return branch_code;
	}

	public void setBranch_code(String branchCode) {
		branch_code = branchCode;
	}

	public String getExec_date() {
		return exec_date;
	}

	public void setExec_date(String execDate) {
		exec_date = execDate;
	}

	public String getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(String paidDate) {
		paid_date = paidDate;
	}

	public String getNominal() {
		return nominal;
	}

	public void setNominal(String nominal) {
		this.nominal = nominal;
	}

	public BigDecimal getDenda() {
		return denda;
	}

	public void setDenda(BigDecimal denda) {
		this.denda = denda;
	}

}
