package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class haji {
	private String noVa;
	private String noPorsi;
	private String wilayah;
	private String nama;
	private String ayah;
	private String tglLahir;
	private BigDecimal setoran;
	private BigDecimal nilai;
	public String getNoVa() {
		return noVa;
	}
	public void setNoVa(String noVa) {
		this.noVa = noVa;
	}
	public String getNoPorsi() {
		return noPorsi;
	}
	public void setNoPorsi(String noPorsi) {
		this.noPorsi = noPorsi;
	}
	public String getWilayah() {
		return wilayah;
	}
	public void setWilayah(String wilayah) {
		this.wilayah = wilayah;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getAyah() {
		return ayah;
	}
	public void setAyah(String ayah) {
		this.ayah = ayah;
	}
	public String getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(String tglLahir) {
		this.tglLahir = tglLahir;
	}
	public BigDecimal getSetoran() {
		return setoran;
	}
	public void setSetoran(BigDecimal setoran) {
		this.setoran = setoran;
	}
	public BigDecimal getNilai() {
		return nilai;
	}
	public void setNilai(BigDecimal nilai) {
		this.nilai = nilai;
	}
	
	
}
