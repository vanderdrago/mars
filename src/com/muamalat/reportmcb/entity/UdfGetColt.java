package com.muamalat.reportmcb.entity;

public class UdfGetColt {

	public String function_id;
	public String field_name;
	public int field_num;
	public String auth_stat;
	public String value;

	public String getFunction_id() {
		return function_id;
	}

	public void setFunction_id(String functionId) {
		function_id = functionId;
	}

	public String getField_name() {
		return field_name;
	}

	public void setField_name(String fieldName) {
		field_name = fieldName;
	}

	public int getField_num() {
		return field_num;
	}

	public void setField_num(int fieldNum) {
		field_num = fieldNum;
	}

	public String getAuth_stat() {
		return auth_stat;
	}

	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
