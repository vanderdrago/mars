package com.muamalat.reportmcb.entity;

public class LapPerubahanDataCif {
	private String branch;
	private String noCif;
	private String nama_nasabah;
	private String data_lama;
	private String modif_ke;
	private String fastpath;
	private String keterangan;
	private String maker_date;
	private String maker_id;
	private String checker_id;
	private String maker_time;
	private String data_baru;
	private String no_rek;
	
	public String getNo_rek() {
		return no_rek;
	}
	public void setNo_rek(String no_rek) {
		this.no_rek = no_rek;
	}
	public String getData_baru() {
		return data_baru;
	}
	public void setData_baru(String data_baru) {
		this.data_baru = data_baru;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getNoCif() {
		return noCif;
	}
	public void setNoCif(String noCif) {
		this.noCif = noCif;
	}
	public String getMaker_time() {
		return maker_time;
	}
	public void setMaker_time(String maker_time) {
		this.maker_time = maker_time;
	}
	public String getNama_nasabah() {
		return nama_nasabah;
	}
	public void setNama_nasabah(String nama_nasabah) {
		this.nama_nasabah = nama_nasabah;
	}
	public String getData_lama() {
		return data_lama;
	}
	public void setData_lama(String data_lama) {
		this.data_lama = data_lama;
	}
	public String getModif_ke() {
		return modif_ke;
	}
	public void setModif_ke(String modif_ke) {
		this.modif_ke = modif_ke;
	}
	public String getFastpath() {
		return fastpath;
	}
	public void setFastpath(String fastpath) {
		this.fastpath = fastpath;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getMaker_date() {
		return maker_date;
	}
	public void setMaker_date(String maker_date) {
		this.maker_date = maker_date;
	}
	public String getMaker_id() {
		return maker_id;
	}
	public void setMaker_id(String maker_id) {
		this.maker_id = maker_id;
	}
	public String getChecker_id() {
		return checker_id;
	}
	public void setChecker_id(String checker_id) {
		this.checker_id = checker_id;
	}
}
