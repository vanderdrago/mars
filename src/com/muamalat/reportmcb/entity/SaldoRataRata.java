package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class SaldoRataRata {
	private String branch;
	private String branch_name; 
	private String acc_no;
	private String acc_desc;
	private String period;
	private String ccy;
	private BigDecimal lcy_bal;
	private BigDecimal acy_bal;
	private String cust_no;
	private String product_code;
	private String opening_date;
	
	public String getOpening_date() {
		return opening_date;
	}
	public void setOpening_date(String opening_date) {
		this.opening_date = opening_date;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	
	public BigDecimal getLcy_bal() {
		return lcy_bal;
	}
	public void setLcy_bal(BigDecimal lcy_bal) {
		this.lcy_bal = lcy_bal;
	}
	public BigDecimal getAcy_bal() {
		return acy_bal;
	}
	public void setAcy_bal(BigDecimal acy_bal) {
		this.acy_bal = acy_bal;
	}
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAcc_no() {
		return acc_no;
	}
	public void setAcc_no(String acc_no) {
		this.acc_no = acc_no;
	}
	public String getAcc_desc() {
		return acc_desc;
	}
	public void setAcc_desc(String acc_desc) {
		this.acc_desc = acc_desc;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	
}
