package com.muamalat.reportmcb.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


/**
 * The persistent class for the T_RTIFTSOUT database table.
 * 
 */
@Entity
@Table(name="T_RTIFTSOUT")
public class TRtiftsout implements Serializable {
	private static final long serialVersionUID = 1L;

	
	
	@Column(name="Amount")
	private String amount;

	@Column(name="Approve")
	private String approve;

	@Column(name="BOR")
	private String bor;

	@Column(name="BusinessDate")
	private String businessDate;

	private String CBSBatchRef;

	@Column(name="Currency")
	private String currency;

	@Column(name="DataID")
	private String dataID;

	@Column(name="DealStockCode")
	private String dealStockCode;

	@Column(name="ExchangeRate")
	private String exchangeRate;

	@Column(name="FromAccName")
	private String fromAccName;

	@Column(name="FromAccNum")
	private String fromAccNum;

	@Column(name="FromMember")
	private String fromMember;

	@Column(name="Inputter")
	private String inputter;

	@Column(name="InterestRate")
	private String interestRate;

	@Column(name="ISN")
	private String isn;

	@Column(name="MemInfo")
	private String memInfo;

	@Column(name="OriName")
	private String oriName;
	
	@Column(name="Nominal")
	private BigDecimal nominal;

	@Column(name="PayDetails")
	private String payDetails;

	@Column(name="Periode")
	private String periode;

	@Column(name="RcvReffNo")
	private String rcvReffNo;

	@Column(name="RecType")
	private String recType;

	@Column(name="RelTRN")
	private String relTRN;

	@Column(name="RsnCde")
	private String rsnCde;

	private String RTStatus;

	@Column(name="SaktiNum")
	private String saktiNum;

	@Column(name="SenderReffNo")
	private String senderReffNo;

	@Column(name="SettleTime")
	private String settleTime;

	@Column(name="Status")
	private String status;

	@Column(name="SubBrCode")
	private String subBrCode;

	@Column(name="TimeApprove")
	private String timeApprove;

	@Column(name="TimeInput")
	private String timeInput;

	@Column(name="ToAccName")
	private String toAccName;

	@Column(name="ToAccNum")
	private String toAccNum;

	@Column(name="ToMember")
	private String toMember;

	@Column(name="TransCode")
	private String transCode;

	@Column(name="TRN")
	private String trn;

	@Column(name="UltimateBeneAcc")
	private String ultimateBeneAcc;

	@Column(name="UltimateBeneName")
	private String ultimateBeneName;

	@Column(name="ValueDate")
	private String valueDate;

    public TRtiftsout() {
    }

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getApprove() {
		return this.approve;
	}

	public void setApprove(String approve) {
		this.approve = approve;
	}

	public String getBor() {
		return this.bor;
	}

	public void setBor(String bor) {
		this.bor = bor;
	}

	public String getBusinessDate() {
		return this.businessDate;
	}

	public void setBusinessDate(String businessDate) {
		this.businessDate = businessDate;
	}

	public String getCBSBatchRef() {
		return this.CBSBatchRef;
	}

	public void setCBSBatchRef(String CBSBatchRef) {
		this.CBSBatchRef = CBSBatchRef;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDataID() {
		return this.dataID;
	}

	public void setDataID(String dataID) {
		this.dataID = dataID;
	}

	public String getDealStockCode() {
		return this.dealStockCode;
	}

	public void setDealStockCode(String dealStockCode) {
		this.dealStockCode = dealStockCode;
	}

	public String getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getFromAccName() {
		return this.fromAccName;
	}

	public void setFromAccName(String fromAccName) {
		this.fromAccName = fromAccName;
	}

	public String getFromAccNum() {
		return this.fromAccNum;
	}

	public void setFromAccNum(String fromAccNum) {
		this.fromAccNum = fromAccNum;
	}

	public String getFromMember() {
		return this.fromMember;
	}

	public void setFromMember(String fromMember) {
		this.fromMember = fromMember;
	}

	public String getInputter() {
		return this.inputter;
	}

	public void setInputter(String inputter) {
		this.inputter = inputter;
	}

	public String getInterestRate() {
		return this.interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}

	public String getIsn() {
		return this.isn;
	}

	public void setIsn(String isn) {
		this.isn = isn;
	}

	public String getMemInfo() {
		return this.memInfo;
	}

	public void setMemInfo(String memInfo) {
		this.memInfo = memInfo;
	}

	public String getOriName() {
		return this.oriName;
	}

	public void setOriName(String oriName) {
		this.oriName = oriName;
	}
	
	public BigDecimal getNominal() {
		return nominal;
	}

	public void setNominal(BigDecimal nominal) {
		this.nominal = nominal;
	}

	public String getPayDetails() {
		return this.payDetails;
	}

	public void setPayDetails(String payDetails) {
		this.payDetails = payDetails;
	}

	public String getPeriode() {
		return this.periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	public String getRcvReffNo() {
		return this.rcvReffNo;
	}

	public void setRcvReffNo(String rcvReffNo) {
		this.rcvReffNo = rcvReffNo;
	}

	public String getRecType() {
		return this.recType;
	}

	public void setRecType(String recType) {
		this.recType = recType;
	}

	public String getRelTRN() {
		return this.relTRN;
	}

	public void setRelTRN(String relTRN) {
		this.relTRN = relTRN;
	}

	public String getRsnCde() {
		return this.rsnCde;
	}

	public void setRsnCde(String rsnCde) {
		this.rsnCde = rsnCde;
	}

	public String getRTStatus() {
		return this.RTStatus;
	}

	public void setRTStatus(String RTStatus) {
		this.RTStatus = RTStatus;
	}

	public String getSaktiNum() {
		return this.saktiNum;
	}

	public void setSaktiNum(String saktiNum) {
		this.saktiNum = saktiNum;
	}

	public String getSenderReffNo() {
		return this.senderReffNo;
	}

	public void setSenderReffNo(String senderReffNo) {
		this.senderReffNo = senderReffNo;
	}

	public String getSettleTime() {
		return this.settleTime;
	}

	public void setSettleTime(String settleTime) {
		this.settleTime = settleTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubBrCode() {
		return this.subBrCode;
	}

	public void setSubBrCode(String subBrCode) {
		this.subBrCode = subBrCode;
	}

	public String getTimeApprove() {
		return this.timeApprove;
	}

	public void setTimeApprove(String timeApprove) {
		this.timeApprove = timeApprove;
	}

	public String getTimeInput() {
		return this.timeInput;
	}

	public void setTimeInput(String timeInput) {
		this.timeInput = timeInput;
	}

	public String getToAccName() {
		return this.toAccName;
	}

	public void setToAccName(String toAccName) {
		this.toAccName = toAccName;
	}

	public String getToAccNum() {
		return this.toAccNum;
	}

	public void setToAccNum(String toAccNum) {
		this.toAccNum = toAccNum;
	}

	public String getToMember() {
		return this.toMember;
	}

	public void setToMember(String toMember) {
		this.toMember = toMember;
	}

	public String getTransCode() {
		return this.transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}

	public String getTrn() {
		return this.trn;
	}

	public void setTrn(String trn) {
		this.trn = trn;
	}

	public String getUltimateBeneAcc() {
		return this.ultimateBeneAcc;
	}

	public void setUltimateBeneAcc(String ultimateBeneAcc) {
		this.ultimateBeneAcc = ultimateBeneAcc;
	}

	public String getUltimateBeneName() {
		return this.ultimateBeneName;
	}

	public void setUltimateBeneName(String ultimateBeneName) {
		this.ultimateBeneName = ultimateBeneName;
	}

	public String getValueDate() {
		return this.valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

}