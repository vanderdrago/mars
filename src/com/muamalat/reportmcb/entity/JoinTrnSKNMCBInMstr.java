package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class JoinTrnSKNMCBInMstr {

	public String trn_dt;
		
	public String id_upload1_time;
	
	public String id_ref_no;
	
	public String id_sender_name;
	
	public String id_benef_account;
	
	public String id_benef_name;
	
	public String id_remark;
	
	public String id_amount;
	
	public String id_sender_region_code;
	
	public String id_benef_region_code;
	
	public String id_sender_clearing_code;
	
	public String id_sor;
	
	public String id_benef_account_name;
	
	public String id_br_code;
	
	public String id_reject_desc; 
	
	public String id_status_host;
	
	public String id_verify_user;
	
	public String id_auth_user;

	public String trn_ref_no;
	
	public String event;
	
	public String module;
	
	public String ac_branch;
	
	public String trn_code;
	
	public String batch_no;
	
	public String user_id;
	
	public String auth_id;
	
	public String auth_stat;
	
	public String ac_no;
	
	public String ac_desc;

	public String drcr_ind;
	
	public BigDecimal lcy_amount;

	public String getTrn_dt() {
		return trn_dt;
	}

	public void setTrn_dt(String trnDt) {
		trn_dt = trnDt;
	}

	public String getId_upload1_time() {
		return id_upload1_time;
	}

	public void setId_upload1_time(String idUpload1Time) {
		id_upload1_time = idUpload1Time;
	}

	public String getId_ref_no() {
		return id_ref_no;
	}

	public void setId_ref_no(String idRefNo) {
		id_ref_no = idRefNo;
	}

	public String getId_sender_name() {
		return id_sender_name;
	}

	public void setId_sender_name(String idSenderName) {
		id_sender_name = idSenderName;
	}

	public String getId_benef_account() {
		return id_benef_account;
	}

	public void setId_benef_account(String idBenefAccount) {
		id_benef_account = idBenefAccount;
	}

	public String getId_benef_name() {
		return id_benef_name;
	}

	public void setId_benef_name(String idBenefName) {
		id_benef_name = idBenefName;
	}

	public String getId_remark() {
		return id_remark;
	}

	public void setId_remark(String idRemark) {
		id_remark = idRemark;
	}

	public String getId_amount() {
		return id_amount;
	}

	public void setId_amount(String idAmount) {
		id_amount = idAmount;
	}

	public String getId_sender_region_code() {
		return id_sender_region_code;
	}

	public void setId_sender_region_code(String idSenderRegionCode) {
		id_sender_region_code = idSenderRegionCode;
	}

	public String getId_benef_region_code() {
		return id_benef_region_code;
	}

	public void setId_benef_region_code(String idBenefRegionCode) {
		id_benef_region_code = idBenefRegionCode;
	}

	public String getId_sender_clearing_code() {
		return id_sender_clearing_code;
	}

	public void setId_sender_clearing_code(String idSenderClearingCode) {
		id_sender_clearing_code = idSenderClearingCode;
	}

	public String getId_sor() {
		return id_sor;
	}

	public void setId_sor(String idSor) {
		id_sor = idSor;
	}

	public String getId_benef_account_name() {
		return id_benef_account_name;
	}

	public void setId_benef_account_name(String idBenefAccountName) {
		id_benef_account_name = idBenefAccountName;
	}

	public String getId_br_code() {
		return id_br_code;
	}

	public void setId_br_code(String idBrCode) {
		id_br_code = idBrCode;
	}

	public String getId_reject_desc() {
		return id_reject_desc;
	}

	public void setId_reject_desc(String idRejectDesc) {
		id_reject_desc = idRejectDesc;
	}

	public String getId_status_host() {
		return id_status_host;
	}

	public void setId_status_host(String idStatusHost) {
		id_status_host = idStatusHost;
	}

	public String getId_verify_user() {
		return id_verify_user;
	}

	public void setId_verify_user(String idVerifyUser) {
		id_verify_user = idVerifyUser;
	}

	public String getId_auth_user() {
		return id_auth_user;
	}

	public void setId_auth_user(String idAuthUser) {
		id_auth_user = idAuthUser;
	}

	public String getTrn_ref_no() {
		return trn_ref_no;
	}

	public void setTrn_ref_no(String trnRefNo) {
		trn_ref_no = trnRefNo;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getAc_branch() {
		return ac_branch;
	}

	public void setAc_branch(String acBranch) {
		ac_branch = acBranch;
	}

	public String getTrn_code() {
		return trn_code;
	}

	public void setTrn_code(String trnCode) {
		trn_code = trnCode;
	}

	public String getBatch_no() {
		return batch_no;
	}

	public void setBatch_no(String batchNo) {
		batch_no = batchNo;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String userId) {
		user_id = userId;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String authId) {
		auth_id = authId;
	}

	public String getAuth_stat() {
		return auth_stat;
	}

	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}

	public String getAc_no() {
		return ac_no;
	}

	public void setAc_no(String acNo) {
		ac_no = acNo;
	}

	public String getAc_desc() {
		return ac_desc;
	}

	public void setAc_desc(String acDesc) {
		ac_desc = acDesc;
	}

	public String getDrcr_ind() {
		return drcr_ind;
	}

	public void setDrcr_ind(String drcrInd) {
		drcr_ind = drcrInd;
	}

	public BigDecimal getLcy_amount() {
		return lcy_amount;
	}

	public void setLcy_amount(BigDecimal lcyAmount) {
		lcy_amount = lcyAmount;
	}
}
