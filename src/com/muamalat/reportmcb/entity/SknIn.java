package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class SknIn {
	private int no;
	private String tgltrns;
	private String noref;
	private String sor;
	private String sbp;
	private String nm_pengirim;
	private String nm_penerima;
	private String nm_penerima_core;
	private String rek_tujuan;
	private BigDecimal nominal;
	private String ket;
	private int tottrns;
	private String sts;
	private String cab_ssl;
	private String desc_rep;
	private String p_code;
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getTgltrns() {
		return tgltrns;
	}
	public void setTgltrns(String tgltrns) {
		this.tgltrns = tgltrns;
	}
	public String getNoref() {
		return noref;
	}
	public void setNoref(String noref) {
		this.noref = noref;
	}
	public String getSor() {
		return sor;
	}
	public void setSor(String sor) {
		this.sor = sor;
	}
	public String getSbp() {
		return sbp;
	}
	public void setSbp(String sbp) {
		this.sbp = sbp;
	}
	public String getNm_pengirim() {
		return nm_pengirim;
	}
	public void setNm_pengirim(String nm_pengirim) {
		this.nm_pengirim = nm_pengirim;
	}
	public String getNm_penerima() {
		return nm_penerima;
	}
	public void setNm_penerima(String nm_penerima) {
		this.nm_penerima = nm_penerima;
	}
	public String getNm_penerima_core() {
		return nm_penerima_core;
	}
	public void setNm_penerima_core(String nm_penerima_core) {
		this.nm_penerima_core = nm_penerima_core;
	}
	public String getRek_tujuan() {
		return rek_tujuan;
	}
	public void setRek_tujuan(String rek_tujuan) {
		this.rek_tujuan = rek_tujuan;
	}
	public BigDecimal getNominal() {
		return nominal;
	}
	public void setNominal(BigDecimal nominal) {
		this.nominal = nominal;
	}
	public String getKet() {
		return ket;
	}
	public void setKet(String ket) {
		this.ket = ket;
	}
	public int getTottrns() {
		return tottrns;
	}
	public void setTottrns(int tottrns) {
		this.tottrns = tottrns;
	}
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
	public String getCab_ssl() {
		return cab_ssl;
	}
	public void setCab_ssl(String cab_ssl) {
		this.cab_ssl = cab_ssl;
	}
	public String getDesc_rep() {
		return desc_rep;
	}
	public void setDesc_rep(String desc_rep) {
		this.desc_rep = desc_rep;
	}
	public String getP_code() {
		return p_code;
	}
	public void setP_code(String p_code) {
		this.p_code = p_code;
	}
	
	
}
