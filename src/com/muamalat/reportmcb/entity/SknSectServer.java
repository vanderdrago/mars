package com.muamalat.reportmcb.entity;

public class SknSectServer {
	private String pc_code;
	private String pc_desc;
	
	public String getPc_code() {
		return pc_code;
	}
	public void setPc_code(String pc_code) {
		this.pc_code = pc_code;
	}
	public String getPc_desc() {
		return pc_desc;
	}
	public void setPc_desc(String pc_desc) {
		this.pc_desc = pc_desc;
	}
	
	
}
