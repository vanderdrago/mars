package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class jadangFbti {

	private String installmentNo;
	private String customerId;
	private String customerName;
	private String productCode;
	private String productCategory;
	private String productType;
	private String bookDate;
	private String maturityDate;
	private String ccy;
	private int tenor;
	private BigDecimal amountPrincipal;
	private BigDecimal amountMargin;
	private BigDecimal amountTotal;
	private String status;
	private double rate;
	private String dueDate;
	private BigDecimal plafond;
	private String KategoriKonsumsiUsaha;
	private String kategoriPortofolioKonsumsiUsaha;
	private String sifatPiutang;
	private String orientasiPenggunaan;
	private String sektorEkonomiUsaha;
	private String lokasiProyek;
	private String kodeMarketing;
	private String segmentAccount;
	private String programPembiayaan;
	private String pembiayaanBackToBack;
	private String jenisKartu;
	private String branch;
	private String jenisPiutang;
	private String noRekBackToBack;
	private String updateSegmentasi;
	private String produkSegmentasi;
	private String modelKerja;
	private BigDecimal marginAwal;
	private BigDecimal hargaJual;
	
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public BigDecimal getMarginAwal() {
		return marginAwal;
	}
	public void setMarginAwal(BigDecimal marginAwal) {
		this.marginAwal = marginAwal;
	}
	public BigDecimal getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(BigDecimal hargaJual) {
		this.hargaJual = hargaJual;
	}
	public String getJenisPiutang() {
		return jenisPiutang;
	}
	public void setJenisPiutang(String jenisPiutang) {
		this.jenisPiutang = jenisPiutang;
	}
	public String getNoRekBackToBack() {
		return noRekBackToBack;
	}
	public void setNoRekBackToBack(String noRekBackToBack) {
		this.noRekBackToBack = noRekBackToBack;
	}
	public String getUpdateSegmentasi() {
		return updateSegmentasi;
	}
	public void setUpdateSegmentasi(String updateSegmentasi) {
		this.updateSegmentasi = updateSegmentasi;
	}
	public String getProdukSegmentasi() {
		return produkSegmentasi;
	}
	public void setProdukSegmentasi(String produkSegmentasi) {
		this.produkSegmentasi = produkSegmentasi;
	}
	public String getModelKerja() {
		return modelKerja;
	}
	public void setModelKerja(String modelKerja) {
		this.modelKerja = modelKerja;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getPlafond() {
		return plafond;
	}
	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}
	public String getKategoriKonsumsiUsaha() {
		return KategoriKonsumsiUsaha;
	}
	public void setKategoriKonsumsiUsaha(String kategoriKonsumsiUsaha) {
		KategoriKonsumsiUsaha = kategoriKonsumsiUsaha;
	}
	public String getKategoriPortofolioKonsumsiUsaha() {
		return kategoriPortofolioKonsumsiUsaha;
	}
	public void setKategoriPortofolioKonsumsiUsaha(String kategoriPortofolioKonsumsiUsaha) {
		this.kategoriPortofolioKonsumsiUsaha = kategoriPortofolioKonsumsiUsaha;
	}
	public String getSifatPiutang() {
		return sifatPiutang;
	}
	public void setSifatPiutang(String sifatPiutang) {
		this.sifatPiutang = sifatPiutang;
	}
	public String getOrientasiPenggunaan() {
		return orientasiPenggunaan;
	}
	public void setOrientasiPenggunaan(String orientasiPenggunaan) {
		this.orientasiPenggunaan = orientasiPenggunaan;
	}
	public String getSektorEkonomiUsaha() {
		return sektorEkonomiUsaha;
	}
	public void setSektorEkonomiUsaha(String sektorEkonomiUsaha) {
		this.sektorEkonomiUsaha = sektorEkonomiUsaha;
	}
	public String getLokasiProyek() {
		return lokasiProyek;
	}
	public void setLokasiProyek(String lokasiProyek) {
		this.lokasiProyek = lokasiProyek;
	}
	public String getKodeMarketing() {
		return kodeMarketing;
	}
	public void setKodeMarketing(String kodeMarketing) {
		this.kodeMarketing = kodeMarketing;
	}
	public String getSegmentAccount() {
		return segmentAccount;
	}
	public void setSegmentAccount(String segmentAccount) {
		this.segmentAccount = segmentAccount;
	}
	public String getProgramPembiayaan() {
		return programPembiayaan;
	}
	public void setProgramPembiayaan(String programPembiayaan) {
		this.programPembiayaan = programPembiayaan;
	}
	public String getPembiayaanBackToBack() {
		return pembiayaanBackToBack;
	}
	public void setPembiayaanBackToBack(String pembiayaanBackToBack) {
		this.pembiayaanBackToBack = pembiayaanBackToBack;
	}
	public String getJenisKartu() {
		return jenisKartu;
	}
	public void setJenisKartu(String jenisKartu) {
		this.jenisKartu = jenisKartu;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public BigDecimal getAmountTotal() {
		return amountTotal;
	}
	public void setAmountTotal(BigDecimal amountTotal) {
		this.amountTotal = amountTotal;
	}
	public String getInstallmentNo() {
		return installmentNo;
	}
	public void setInstallmentNo(String installmentNo) {
		this.installmentNo = installmentNo;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getBookDate() {
		return bookDate;
	}
	public void setBookDate(String bookDate) {
		this.bookDate = bookDate;
	}
	public String getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public int getTenor() {
		return tenor;
	}
	public void setTenor(int tenor) {
		this.tenor = tenor;
	}
	public BigDecimal getAmountPrincipal() {
		return amountPrincipal;
	}
	public void setAmountPrincipal(BigDecimal amountPrincipal) {
		this.amountPrincipal = amountPrincipal;
	}
	public BigDecimal getAmountMargin() {
		return amountMargin;
	}
	public void setAmountMargin(BigDecimal amountMargin) {
		this.amountMargin = amountMargin;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
