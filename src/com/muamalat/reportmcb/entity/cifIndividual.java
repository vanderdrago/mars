package com.muamalat.reportmcb.entity;

public class cifIndividual {
	private String profile_rule_group_no;
	private String profile_rule_group_name;
	private String profile_rule_no;
	private String profile_rule_desc;
	
	public String getProfile_rule_group_no() {
		return profile_rule_group_no;
	}
	public void setProfile_rule_group_no(String profile_rule_group_no) {
		this.profile_rule_group_no = profile_rule_group_no;
	}
	public String getProfile_rule_group_name() {
		return profile_rule_group_name;
	}
	public void setProfile_rule_group_name(String profile_rule_group_name) {
		this.profile_rule_group_name = profile_rule_group_name;
	}
	public String getProfile_rule_no() {
		return profile_rule_no;
	}
	public void setProfile_rule_no(String profile_rule_no) {
		this.profile_rule_no = profile_rule_no;
	}
	public String getProfile_rule_desc() {
		return profile_rule_desc;
	}
	public void setProfile_rule_desc(String profile_rule_desc) {
		this.profile_rule_desc = profile_rule_desc;
	}
}
