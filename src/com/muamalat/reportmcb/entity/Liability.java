package com.muamalat.reportmcb.entity;

public class Liability {
	
	public String id;
	
	public String liab_no;
	
	public String liab_name;
	
	public String process_no;
	
	public String liab_branch;
	
	public String liab_ccy;
	
	public String util_amt;
	
	public String overall_limit;
	
	public String category;
	
	public String maker_id;
	
	public String checker_id;
	
	public String record_stat;
	
	public String auth_stat;
	
	public String status_rcd;
	
	public String user_define_status;
	
	public String status_oto;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLiab_no() {
		return liab_no;
	}

	public void setLiab_no(String liabNo) {
		liab_no = liabNo;
	}

	public String getLiab_name() {
		return liab_name;
	}

	public void setLiab_name(String liabName) {
		liab_name = liabName;
	}

	public String getProcess_no() {
		return process_no;
	}

	public void setProcess_no(String processNo) {
		process_no = processNo;
	}

	public String getLiab_branch() {
		return liab_branch;
	}

	public void setLiab_branch(String liabBranch) {
		liab_branch = liabBranch;
	}

	public String getLiab_ccy() {
		return liab_ccy;
	}

	public void setLiab_ccy(String liabCcy) {
		liab_ccy = liabCcy;
	}

	public String getUtil_amt() {
		return util_amt;
	}

	public void setUtil_amt(String utilAmt) {
		util_amt = utilAmt;
	}

	public String getOverall_limit() {
		return overall_limit;
	}

	public void setOverall_limit(String overallLimit) {
		overall_limit = overallLimit;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getMaker_id() {
		return maker_id;
	}

	public void setMaker_id(String makerId) {
		maker_id = makerId;
	}

	public String getChecker_id() {
		return checker_id;
	}

	public void setChecker_id(String checkerId) {
		checker_id = checkerId;
	}

	public String getRecord_stat() {
		return record_stat;
	}

	public void setRecord_stat(String recordStat) {
		record_stat = recordStat;
	}

	public String getAuth_stat() {
		return auth_stat;
	}

	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}

	public String getStatus_rcd() {
		return status_rcd;
	}

	public void setStatus_rcd(String statusRcd) {
		status_rcd = statusRcd;
	}

	public String getStatus_oto() {
		return status_oto;
	}

	public void setStatus_oto(String statusOto) {
		status_oto = statusOto;
	}

	public String getUser_define_status() {
		return user_define_status;
	}

	public void setUser_define_status(String userDefineStatus) {
		user_define_status = userDefineStatus;
	}
	
	
}
