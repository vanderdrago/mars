/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.entity;

/**
 *
 * @author Utis
 */
public class Listreport {
    private String idreport;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdreport() {
        return idreport;
    }

    public void setIdreport(String idreport) {
        this.idreport = idreport;
    }
    
}
