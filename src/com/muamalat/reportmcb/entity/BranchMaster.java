package com.muamalat.reportmcb.entity;

public class BranchMaster {
	
	private String brnch_cd;
	private String brnch_name;
	
	public String getBrnch_cd() {
		return brnch_cd;
	}
	public void setBrnch_cd(String brnch_cd) {
		this.brnch_cd = brnch_cd;
	}
	public String getBrnch_name() {
		return brnch_name;
	}
	public void setBrnch_name(String brnch_name) {
		this.brnch_name = brnch_name;
	}
}
