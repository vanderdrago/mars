package com.muamalat.reportmcb.entity;

public class Mars_report_desc {
	private String filename;
	private String desc;
	private boolean sts_display;
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public boolean isSts_display() {
		return sts_display;
	}
	public void setSts_display(boolean sts_display) {
		this.sts_display = sts_display;
	}
	
	
}
