package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class Gltb_avgbal {
	private String branch_code;
	private String gl_code;
	private String fin_year;
	private String period_code;
	private String ccy;
	private BigDecimal acy_bal;
	private BigDecimal lcy_bal;
	private Date from_date; 
	private String user_id;
	private BigDecimal ytodacy_bal;
	private BigDecimal ytodlcy_bal;
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getGl_code() {
		return gl_code;
	}
	public void setGl_code(String gl_code) {
		this.gl_code = gl_code;
	}
	public String getFin_year() {
		return fin_year;
	}
	public void setFin_year(String fin_year) {
		this.fin_year = fin_year;
	}
	public String getPeriod_code() {
		return period_code;
	}
	public void setPeriod_code(String period_code) {
		this.period_code = period_code;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAcy_bal() {
		return acy_bal;
	}
	public void setAcy_bal(BigDecimal acy_bal) {
		this.acy_bal = acy_bal;
	}
	public BigDecimal getLcy_bal() {
		return lcy_bal;
	}
	public void setLcy_bal(BigDecimal lcy_bal) {
		this.lcy_bal = lcy_bal;
	}
	public Date getFrom_date() {
		return from_date;
	}
	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public BigDecimal getYtodacy_bal() {
		return ytodacy_bal;
	}
	public void setYtodacy_bal(BigDecimal ytodacy_bal) {
		this.ytodacy_bal = ytodacy_bal;
	}
	public BigDecimal getYtodlcy_bal() {
		return ytodlcy_bal;
	}
	public void setYtodlcy_bal(BigDecimal ytodlcy_bal) {
		this.ytodlcy_bal = ytodlcy_bal;
	}
	
}
