package com.muamalat.reportmcb.entity;

import java.util.Comparator;

public class ReportNeraca {
	private String namefile;	
	private String description;

	public static class OrderByFilename implements Comparator<ReportNeraca> {

        @Override
        public int compare(ReportNeraca o1, ReportNeraca o2) {
            return o1.namefile.compareTo(o2.namefile);
        }
    }
	
	public String getNamefile() {
		return namefile;
	}

	public void setNamefile(String namefile) {
		this.namefile = namefile;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

		
}
