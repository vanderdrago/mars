package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class TRtiftsin {

	private String valuedate;
	
	private String relTRN;
	
	private String TRN;
	
	private String transcode;
	
	private String bor;
	
	private String osn;
	
	private String fromaccnum;
	
	private String fromaccname;
	
	private String toaccnum;
	
	private String toaccname;
	
	private String ultimatebeneacc;
	
	private String ultimatebenename;
	
	private String amount;
	
	private String payDetails;
	
	private String status;
	
	private String saktinum;
	
	private String frommember;
	
	private String tomember;
	
	private BigDecimal nominal;
	
	public TRtiftsin() {
    }

	public String getValuedate() {
		return valuedate;
	}

	public void setValuedate(String valuedate) {
		this.valuedate = valuedate;
	}

	public String getRelTRN() {
		return relTRN;
	}

	public void setRelTRN(String relTRN) {
		this.relTRN = relTRN;
	}

	public String getTRN() {
		return TRN;
	}

	public void setTRN(String tRN) {
		TRN = tRN;
	}

	public String getTranscode() {
		return transcode;
	}

	public void setTranscode(String transcode) {
		this.transcode = transcode;
	}

	public String getBor() {
		return bor;
	}

	public void setBor(String bor) {
		this.bor = bor;
	}

	public String getOsn() {
		return osn;
	}

	public void setOsn(String osn) {
		this.osn = osn;
	}

	public String getFromaccnum() {
		return fromaccnum;
	}

	public void setFromaccnum(String fromaccnum) {
		this.fromaccnum = fromaccnum;
	}

	public String getFromaccname() {
		return fromaccname;
	}

	public void setFromaccname(String fromaccname) {
		this.fromaccname = fromaccname;
	}
	
	public String getToaccnum() {
		return toaccnum;
	}

	public void setToaccnum(String toaccnum) {
		this.toaccnum = toaccnum;
	}

	public String getToaccname() {
		return toaccname;
	}

	public void setToaccname(String toaccname) {
		this.toaccname = toaccname;
	}

	public String getUltimatebeneacc() {
		return ultimatebeneacc;
	}

	public void setUltimatebeneacc(String ultimatebeneacc) {
		this.ultimatebeneacc = ultimatebeneacc;
	}

	public String getUltimatebenename() {
		return ultimatebenename;
	}

	public void setUltimatebenename(String ultimatebenename) {
		this.ultimatebenename = ultimatebenename;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSaktinum() {
		return saktinum;
	}

	public void setSaktinum(String saktinum) {
		this.saktinum = saktinum;
	}

	public String getFrommember() {
		return frommember;
	}

	public void setFrommember(String frommember) {
		this.frommember = frommember;
	}

	public String getTomember() {
		return tomember;
	}

	public void setTomember(String tomember) {
		this.tomember = tomember;
	}

	public String getPayDetails() {
		return payDetails;
	}

	public void setPayDetails(String payDetails) {
		this.payDetails = payDetails;
	}

	public BigDecimal getNominal() {
		return nominal;
	}

	public void setNominal(BigDecimal nominal) {
		this.nominal = nominal;
	}
	
}
