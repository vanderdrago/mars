package com.muamalat.reportmcb.entity;

import java.util.ArrayList;

public class RoleSubMenu {
	private String subMenu;
	private ArrayList detail;
	
	public String getSubMenu() {
		return subMenu;
	}
	public void setSubMenu(String subMenu) {
		this.subMenu = subMenu;
	}
	public ArrayList getDetail() {
		return detail;
	}
	public void setDetail(ArrayList detail) {
		this.detail = detail;
	}
}
