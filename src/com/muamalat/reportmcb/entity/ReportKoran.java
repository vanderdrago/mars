package com.muamalat.reportmcb.entity;

import java.util.Comparator;

public class ReportKoran {
	private String namefile;	
	private String description;

	public static class OrderByFilename implements Comparator<ReportKoran> {

        @Override
        public int compare(ReportKoran o1, ReportKoran o2) {
            return o1.namefile.compareTo(o2.namefile);
        }
    }
	
	public String getNamefile() {
		return namefile;
	}

	public void setNamefile(String namefile) {
		this.namefile = namefile;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

		
}
