/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

/**
 *
 * @author Utis
 */
public class Bukubesar {
    private Date trn_dt;
	private String batch_no;
    private String trn_ref_no;
    private String trn_code;
    private String event;
    private String maker_id;
    private String auth_id;
	private String ket;
    private String drcr;
    private String ac_ccy;
    private BigDecimal exh_rate;
    private BigDecimal amt_db;
    private BigDecimal amt_cr;
    private BigDecimal amt_db_eq;
    private BigDecimal amt_cr_eq;
    private BigDecimal fcy_amount;
    private BigDecimal lcy_amount;
    private Date value_dt;
    private String ac_no;
    private String ac_branch;
    private int totrow;
    private BigDecimal totdebet;
    private BigDecimal totkredit;
    private BigDecimal saldo;
    private String rcc_no;
    private String txn_dt_time;
    
    public Date getTrn_dt() {
		return trn_dt;
	}

	public void setTrn_dt(Date trn_dt) {
		this.trn_dt = trn_dt;
	}

	public String getTxn_dt_time() {
		return txn_dt_time;
	}

	public void setTxn_dt_time(String txn_dt_time) {
		this.txn_dt_time = txn_dt_time;
	}

	public String getRcc_no() {
		return rcc_no;
	}

	public void setRcc_no(String rcc_no) {
		this.rcc_no = rcc_no;
	}

	public BigDecimal getAmt_cr() {
        return amt_cr;
    }

    public void setAmt_cr(BigDecimal amt_cr) {
        this.amt_cr = amt_cr;
    }

    public BigDecimal getAmt_db() {
        return amt_db;
    }

    public void setAmt_db(BigDecimal amt_db) {
        this.amt_db = amt_db;
    }

    
    public String getAc_ccy() {
        return ac_ccy;
    }

    public void setAc_ccy(String ac_ccy) {
        this.ac_ccy = ac_ccy;
    }

    public BigDecimal getFcy_amount() {
        return fcy_amount;
    }

    public void setFcy_amount(BigDecimal fcy_amount) {
    	if(fcy_amount == null){
    		this.fcy_amount = new BigDecimal(0);
    	} else {
    		this.fcy_amount = fcy_amount;
    	}        
    }

    public String getBatch_no() {
        return batch_no;
    }

    public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getMaker_id() {
		return maker_id;
	}

	public void setMaker_id(String maker_id) {
		this.maker_id = maker_id;
	}

	public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public String getDrcr() {
        return drcr;
    }

    public void setDrcr(String drcr) {
        this.drcr = drcr;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public BigDecimal getLcy_amount() {
        return lcy_amount;
    }

    public void setLcy_amount(BigDecimal lcy_amount) {
    	if(lcy_amount == null){
    		this.lcy_amount = new BigDecimal(0);
    	} else {
    		this.lcy_amount = lcy_amount;
    	}
    }

    public String getTrn_code() {
        return trn_code;
    }

    public void setTrn_code(String trn_code) {
        this.trn_code = trn_code;
    }

//    public Date getTrn_dt() {
//        return trn_dt;
//    }
//
//    public void setTrn_dt(Date trn_dt) {
//        this.trn_dt = trn_dt;
//    }

    public String getTrn_ref_no() {
        return trn_ref_no;
    }

    public void setTrn_ref_no(String trn_ref_no) {
        this.trn_ref_no = trn_ref_no;
    }

	public Date getValue_dt() {
		return value_dt;
	}

	public void setValue_dt(Date value_dt) {
		this.value_dt = value_dt;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getAc_no() {
		return ac_no;
	}

	public void setAc_no(String acNo) {
		ac_no = acNo;
	}

	public int getTotrow() {
		return totrow;
	}

	public void setTotrow(int totrow) {
		this.totrow = totrow;
	}

	public BigDecimal getTotdebet() {
		return totdebet;
	}

	public void setTotdebet(BigDecimal totdebet) {
		this.totdebet = totdebet;
	}

	public BigDecimal getTotkredit() {
		return totkredit;
	}

	public void setTotkredit(BigDecimal totkredit) {
		this.totkredit = totkredit;
	}

	public String getAc_branch() {
		return this.ac_branch;
	}

	public void setAc_branch(String acBranch) {
		this.ac_branch = acBranch;
	}

	public BigDecimal getExh_rate() {
		return exh_rate;
	}

	public void setExh_rate(BigDecimal exhRate) {
		exh_rate = exhRate;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public BigDecimal getAmt_db_eq() {
		return amt_db_eq;
	}

	public void setAmt_db_eq(BigDecimal amt_db_eq) {
		this.amt_db_eq = amt_db_eq;
	}

	public BigDecimal getAmt_cr_eq() {
		return amt_cr_eq;
	}

	public void setAmt_cr_eq(BigDecimal amt_cr_eq) {
		this.amt_cr_eq = amt_cr_eq;
	}
	
	
    
}
