package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class kpr {
	private String nama;
	private BigDecimal plafond;
	private String tanggalSimulasi;
	private int jangka;
	private BigDecimal hargaJual;
	private BigDecimal totalMargin;
	private BigDecimal periode1;
	private BigDecimal periode2;
	private String rate;
	
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public BigDecimal getPlafond() {
		return plafond;
	}
	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}
	public String getTanggalSimulasi() {
		return tanggalSimulasi;
	}
	public void setTanggalSimulasi(String tanggalSimulasi) {
		this.tanggalSimulasi = tanggalSimulasi;
	}
	public int getJangka() {
		return jangka;
	}
	public void setJangka(int jangka) {
		this.jangka = jangka;
	}
	public BigDecimal getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(BigDecimal hargaJual) {
		this.hargaJual = hargaJual;
	}
	public BigDecimal getTotalMargin() {
		return totalMargin;
	}
	public void setTotalMargin(BigDecimal totalMargin) {
		this.totalMargin = totalMargin;
	}
	public BigDecimal getPeriode1() {
		return periode1;
	}
	public void setPeriode1(BigDecimal periode1) {
		this.periode1 = periode1;
	}
	public BigDecimal getPeriode2() {
		return periode2;
	}
	public void setPeriode2(BigDecimal periode2) {
		this.periode2 = periode2;
	}
	
	
}
