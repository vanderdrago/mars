package com.muamalat.reportmcb.entity;

public class account {
	
	private String cust_ac_no;
	private String ac_desc;
	private String cust_no;
	private String ccy;
	private String account_class;
	private String DATE_OF_BIRTH;
	private String UNIQUE_ID_VALUE;
	
	public String getCust_ac_no() {
		return cust_ac_no;
	}
	public void setCust_ac_no(String cust_ac_no) {
		this.cust_ac_no = cust_ac_no;
	}
	public String getAc_desc() {
		return ac_desc;
	}
	public void setAc_desc(String ac_desc) {
		this.ac_desc = ac_desc;
	}
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getAccount_class() {
		return account_class;
	}
	public void setAccount_class(String account_class) {
		this.account_class = account_class;
	}
	public String getDATE_OF_BIRTH() {
		return DATE_OF_BIRTH;
	}
	public void setDATE_OF_BIRTH(String dATE_OF_BIRTH) {
		DATE_OF_BIRTH = dATE_OF_BIRTH;
	}
	public String getUNIQUE_ID_VALUE() {
		return UNIQUE_ID_VALUE;
	}
	public void setUNIQUE_ID_VALUE(String uNIQUE_ID_VALUE) {
		UNIQUE_ID_VALUE = uNIQUE_ID_VALUE;
	}
	
	
}
