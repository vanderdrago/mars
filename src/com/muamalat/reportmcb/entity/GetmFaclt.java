package com.muamalat.reportmcb.entity;

public class GetmFaclt {
	
	public String id;
	
	public String liab_id;
	
	public String pool_id;
	
	public String liab_no; 
	
	public String liab_name;
	
	public String line_code;
	
	public String line_serial; 
	
	public String line_currency; 
	
	public String line_start_date; 
	
	public String line_expiry_date;
	
	public String limit_amount;
	
	public String collateral_contribution;
	
	public String available_amount;
	
	public String date_of_first_od; 
	
	public String date_of_last_od;
	
	public String amount_utilised_today;
	
	public String amount_reinstated_today;
	
	public String utilisation;
	
	public String brn;
	
	public String dsp_eff_line_amount;
	
	public String uncollected_amount;
	
	public String block_amount;
	
	public String tanked_util;
	
	public String netting_amount;
	
	public String description;
	
	public String revolving_amt;
	
	public String approved_amt;
	
	public String status_rcd;
	
	public String status_oto;
	
	public String transfer_amount;
	
	public String category;
	
	public String status_av;
	
	public String maker_id;
	
	public String checker_id;
	
	public String last_new_util_date;
	
	public String pool_code;
	
	public String pool_description;
	
	public String pool_ccy;
	
	public String pool_amount;
	
	public String pool_util;
	
	public String facility_branch_code;
	
	public String percentage_of_pool;
	
	public String facility_amount_pool_ccy;
	
	public String facility_currency;
	
	public String facility_amount;
	
	public String type;
	
	public String percentage_of_contract;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLiab_id() {
		return liab_id;
	}
	public void setLiab_id(String liabId) {
		liab_id = liabId;
	}
	public String getPool_id() {
		return pool_id;
	}
	public void setPool_id(String poolId) {
		pool_id = poolId;
	}
	public String getLiab_no() {
		return liab_no;
	}
	public void setLiab_no(String liabNo) {
		liab_no = liabNo;
	}
	public String getLiab_name() {
		return liab_name;
	}
	public void setLiab_name(String liabName) {
		liab_name = liabName;
	}
	public String getLine_code() {
		return line_code;
	}
	public void setLine_code(String lineCode) {
		line_code = lineCode;
	}
	public String getLine_serial() {
		return line_serial;
	}
	public void setLine_serial(String lineSerial) {
		line_serial = lineSerial;
	}
	public String getLine_currency() {
		return line_currency;
	}
	public void setLine_currency(String lineCurrency) {
		line_currency = lineCurrency;
	}
	public String getLine_start_date() {
		return line_start_date;
	}
	public void setLine_start_date(String lineStartDate) {
		line_start_date = lineStartDate;
	}
	public String getLine_expiry_date() {
		return line_expiry_date;
	}
	public void setLine_expiry_date(String lineExpiryDate) {
		line_expiry_date = lineExpiryDate;
	}
	public String getLimit_amount() {
		return limit_amount;
	}
	public void setLimit_amount(String limitAmount) {
		limit_amount = limitAmount;
	}
	public String getCollateral_contribution() {
		return collateral_contribution;
	}
	public void setCollateral_contribution(String collateralContribution) {
		collateral_contribution = collateralContribution;
	}
	public String getAvailable_amount() {
		return available_amount;
	}
	public void setAvailable_amount(String availableAmount) {
		available_amount = availableAmount;
	}
	public String getDate_of_first_od() {
		return date_of_first_od;
	}
	public void setDate_of_first_od(String dateOfFirstOd) {
		date_of_first_od = dateOfFirstOd;
	}
	public String getDate_of_last_od() {
		return date_of_last_od;
	}
	public void setDate_of_last_od(String dateOfLastOd) {
		date_of_last_od = dateOfLastOd;
	}
	public String getAmount_utilised_today() {
		return amount_utilised_today;
	}
	public void setAmount_utilised_today(String amountUtilisedToday) {
		amount_utilised_today = amountUtilisedToday;
	}
	public String getAmount_reinstated_today() {
		return amount_reinstated_today;
	}
	public void setAmount_reinstated_today(String amountReinstatedToday) {
		amount_reinstated_today = amountReinstatedToday;
	}
	public String getUtilisation() {
		return utilisation;
	}
	public void setUtilisation(String utilisation) {
		this.utilisation = utilisation;
	}
	public String getBrn() {
		return brn;
	}
	public void setBrn(String brn) {
		this.brn = brn;
	}
	public String getDsp_eff_line_amount() {
		return dsp_eff_line_amount;
	}
	public void setDsp_eff_line_amount(String dspEffLineAmount) {
		dsp_eff_line_amount = dspEffLineAmount;
	}
	public String getBlock_amount() {
		return block_amount;
	}
	public void setBlock_amount(String blockAmount) {
		block_amount = blockAmount;
	}
	public String getTanked_util() {
		return tanked_util;
	}
	public void setTanked_util(String tankedUtil) {
		tanked_util = tankedUtil;
	}
	public String getNetting_amount() {
		return netting_amount;
	}
	public void setNetting_amount(String nettingAmount) {
		netting_amount = nettingAmount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRevolving_amt() {
		return revolving_amt;
	}
	public void setRevolving_amt(String revolvingAmt) {
		revolving_amt = revolvingAmt;
	}
	public String getApproved_amt() {
		return approved_amt;
	}
	public void setApproved_amt(String approvedAmt) {
		approved_amt = approvedAmt;
	}
	public String getStatus_rcd() {
		return status_rcd;
	}
	public void setStatus_rcd(String statusRcd) {
		status_rcd = statusRcd;
	}
	public String getStatus_oto() {
		return status_oto;
	}
	public void setStatus_oto(String statusOto) {
		status_oto = statusOto;
	}
	public String getTransfer_amount() {
		return transfer_amount;
	}
	public void setTransfer_amount(String transferAmount) {
		transfer_amount = transferAmount;
	}
	public String getUncollected_amount() {
		return uncollected_amount;
	}
	public void setUncollected_amount(String uncollectedAmount) {
		uncollected_amount = uncollectedAmount;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getStatus_av() {
		return status_av;
	}
	public void setStatus_av(String statusAv) {
		status_av = statusAv;
	}
	public String getLast_new_util_date() {
		return last_new_util_date;
	}
	public void setLast_new_util_date(String lastNewUtilDate) {
		last_new_util_date = lastNewUtilDate;
	}
	public String getMaker_id() {
		return maker_id;
	}
	public void setMaker_id(String makerId) {
		maker_id = makerId;
	}
	public String getChecker_id() {
		return checker_id;
	}
	public void setChecker_id(String checkerId) {
		checker_id = checkerId;
	}
	public String getPool_code() {
		return pool_code;
	}
	public void setPool_code(String poolCode) {
		pool_code = poolCode;
	}
	public String getPool_description() {
		return pool_description;
	}
	public void setPool_description(String poolDescription) {
		pool_description = poolDescription;
	}
	public String getPool_ccy() {
		return pool_ccy;
	}
	public void setPool_ccy(String poolCcy) {
		pool_ccy = poolCcy;
	}
	public String getPool_amount() {
		return pool_amount;
	}
	public void setPool_amount(String poolAmount) {
		pool_amount = poolAmount;
	}
	public String getPool_util() {
		return pool_util;
	}
	public void setPool_util(String poolUtil) {
		pool_util = poolUtil;
	}
	public String getFacility_branch_code() {
		return facility_branch_code;
	}
	public void setFacility_branch_code(String facilityBranchCode) {
		facility_branch_code = facilityBranchCode;
	}
	public String getPercentage_of_pool() {
		return percentage_of_pool;
	}
	public void setPercentage_of_pool(String percentageOfPool) {
		percentage_of_pool = percentageOfPool;
	}
	public String getFacility_amount_pool_ccy() {
		return facility_amount_pool_ccy;
	}
	public void setFacility_amount_pool_ccy(String facilityAmountPoolCcy) {
		facility_amount_pool_ccy = facilityAmountPoolCcy;
	}
	public String getFacility_currency() {
		return facility_currency;
	}
	public void setFacility_currency(String facilityCurrency) {
		facility_currency = facilityCurrency;
	}
	public String getFacility_amount() {
		return facility_amount;
	}
	public void setFacility_amount(String facilityAmount) {
		facility_amount = facilityAmount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPercentage_of_contract() {
		return percentage_of_contract;
	}
	public void setPercentage_of_contract(String percentageOfContract) {
		percentage_of_contract = percentageOfContract;
	}
	
	
}
