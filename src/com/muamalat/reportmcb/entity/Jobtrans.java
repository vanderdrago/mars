package com.muamalat.reportmcb.entity;

public class Jobtrans {

	public String day;
	
	public String job_code;
	
	public String job_name;
	
	public String start_date;
	
	public String end_date;
	
	public String dur_h;
	
	public String status;
	
	public String description;

	public String duration;
	
	public String module;
	
	public String runjob_flag;
	
	
	public String getRunjob_flag() {
		return runjob_flag;
	}

	public void setRunjob_flag(String runjob_flag) {
		this.runjob_flag = runjob_flag;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getJob_code() {
		return job_code;
	}

	public void setJob_code(String jobCode) {
		job_code = jobCode;
	}

	public String getJob_name() {
		return job_name;
	}

	public void setJob_name(String jobName) {
		job_name = jobName;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String startDate) {
		start_date = startDate;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String endDate) {
		end_date = endDate;
	}

	public String getDur_h() {
		return dur_h;
	}

	public void setDur_h(String durH) {
		dur_h = durH;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
}
