package com.muamalat.reportmcb.entity;

public class SMTB_USER_ROLE {
	
	private String role_id;
	private String user_id;
	private String auth_stat;
	private String branch_code;
	public String getRole_id() {
		return role_id;
	}
	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAuth_stat() {
		return auth_stat;
	}
	public void setAuth_stat(String auth_stat) {
		this.auth_stat = auth_stat;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	
	
}
