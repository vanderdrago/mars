package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class JurnalTodayDetail {
	private String user_id;
	private String kode_cabang;
	private String kode_transaksi;
	private String ket_kode_transaksi;
	private String keterangan_transaksi;
	private BigDecimal debet;
	private BigDecimal kredit;
	private String user_approval;
	private String nomor_warkat;
	private String jam;
	private String tanggal;
	private String ac_no;
	private String keterangan;
	private String periksa;
	private String ac_desc;

	
	public String getAc_desc() {
		return ac_desc;
	}
	public void setAc_desc(String ac_desc) {
		this.ac_desc = ac_desc;
	}
	
	public String getPeriksa() {
		return periksa;
	}
	public void setPeriksa(String periksa) {
		this.periksa = periksa;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getAc_no() {
		return ac_no;
	}
	public void setAc_no(String ac_no) {
		this.ac_no = ac_no;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getNomor_warkat() {
		return nomor_warkat;
	}
	public void setNomor_warkat(String nomor_warkat) {
		this.nomor_warkat = nomor_warkat;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getKode_cabang() {
		return kode_cabang;
	}
	public void setKode_cabang(String kode_cabang) {
		this.kode_cabang = kode_cabang;
	}
	public String getKode_transaksi() {
		return kode_transaksi;
	}
	public void setKode_transaksi(String kode_transaksi) {
		this.kode_transaksi = kode_transaksi;
	}
	public String getKet_kode_transaksi() {
		return ket_kode_transaksi;
	}
	public void setKet_kode_transaksi(String ket_kode_transaksi) {
		this.ket_kode_transaksi = ket_kode_transaksi;
	}
	public String getKeterangan_transaksi() {
		return keterangan_transaksi;
	}
	public void setKeterangan_transaksi(String keterangan_transaksi) {
		this.keterangan_transaksi = keterangan_transaksi;
	}
	public BigDecimal getDebet() {
		return debet;
	}
	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}
	public BigDecimal getKredit() {
		return kredit;
	}
	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}
	public String getUser_approval() {
		return user_approval;
	}
	public void setUser_approval(String user_approval) {
		this.user_approval = user_approval;
	}
}
