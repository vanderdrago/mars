package com.muamalat.reportmcb.entity;

public class Altacct {
	
	public  String branch_code;
	public  String cust_ac_no; 
	public  String ac_desc ; 
	public  String cust_no; 
	public  String ccy; 
	public  String account_class; 
	public  String ac_open_date; 
	public  String alt_ac_no; 
	public  String record_stat; 
	public  String auth_stat; 
	public  String address1;
	public String TBSPACE_REGISTERED;  
	
	
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branchCode) {
		branch_code = branchCode;
	}
	public String getCust_ac_no() {
		return cust_ac_no;
	}
	public void setCust_ac_no(String custAcNo) {
		cust_ac_no = custAcNo;
	}
	public String getAc_desc() {
		return ac_desc;
	}
	public void setAc_desc(String acDesc) {
		ac_desc = acDesc;
	}
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String custNo) {
		cust_no = custNo;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getAccount_class() {
		return account_class;
	}
	public void setAccount_class(String accountClass) {
		account_class = accountClass;
	}
	public String getAc_open_date() {
		return ac_open_date;
	}
	public void setAc_open_date(String acOpenDate) {
		ac_open_date = acOpenDate;
	}
	public String getAlt_ac_no() {
		return alt_ac_no;
	}
	public void setAlt_ac_no(String altAcNo) {
		alt_ac_no = altAcNo;
	}
	public String getRecord_stat() {
		return record_stat;
	}
	public void setRecord_stat(String recordStat) {
		record_stat = recordStat;
	}
	public String getAuth_stat() {
		return auth_stat;
	}
	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getTBSPACE_REGISTERED() {
		return TBSPACE_REGISTERED;
	}
	public void setTBSPACE_REGISTERED(String tbspace_registered) {
		TBSPACE_REGISTERED = tbspace_registered;
	}
	
}
