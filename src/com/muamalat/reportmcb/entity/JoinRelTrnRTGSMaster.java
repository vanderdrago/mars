package com.muamalat.reportmcb.entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PCTB_CONTRACT_MASTER")
public class JoinRelTrnRTGSMaster implements Serializable {
	@Id
	@Column(name="CONTRACT_REF_NO")
	private String contractRefNo;

	@Column(name="PROD_REF_NO")
	private String prodRefNo;

	private String network;

	@Column(name="CUST_BANKCODE")
	private String custBankcode;

	@Column(name="CUST_AC_NO")
	private String custAcNo;
	
	@Column(name="CUST_NAME")
	private String custName;

	@Column(name="TXN_AMOUNT")
	private BigDecimal txnAmount;

	@Column(name="CPTY_AC_NO")
	private String cptyAcNo;

	@Column(name="CPTY_NAME")
	private String cptyName;

	@Column(name="BOOKING_DT")
	private Date bookingDt;

	@Column(name="EXCEPTION_QUEUE")
	private String exceptionQueue;

	@Column(name="MAKER_ID")
	private String makerId;

	@Column(name="CHECKER_ID")
	private String checkerId;

	@Column(name="DISPATCH_REF_NO")
	private String dispatchRefNo;

	@Column(name="UDF_2")
	private String udf_2;
	
	private static final long serialVersionUID = 1L;

	public String getContractRefNo() {
		return contractRefNo;
	}

	public void setContractRefNo(String contractRefNo) {
		this.contractRefNo = contractRefNo;
	}

	public String getProdRefNo() {
		return prodRefNo;
	}

	public void setProdRefNo(String prodRefNo) {
		this.prodRefNo = prodRefNo;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getCustBankcode() {
		return custBankcode;
	}

	public void setCustBankcode(String custBankcode) {
		this.custBankcode = custBankcode;
	}

	public String getCustAcNo() {
		return custAcNo;
	}

	public void setCustAcNo(String custAcNo) {
		this.custAcNo = custAcNo;
	}
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getCptyAcNo() {
		return cptyAcNo;
	}

	public void setCptyAcNo(String cptyAcNo) {
		this.cptyAcNo = cptyAcNo;
	}

	public String getCptyName() {
		return cptyName;
	}

	public void setCptyName(String cptyName) {
		this.cptyName = cptyName;
	}

	public Date getBookingDt() {
		return bookingDt;
	}

	public void setBookingDt(Date bookingDt) {
		this.bookingDt = bookingDt;
	}

	public String getExceptionQueue() {
		return exceptionQueue;
	}

	public void setExceptionQueue(String exceptionQueue) {
		this.exceptionQueue = exceptionQueue;
	}

	public String getMakerId() {
		return makerId;
	}

	public void setMakerId(String makerId) {
		this.makerId = makerId;
	}

	public String getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(String checkerId) {
		this.checkerId = checkerId;
	}

	public String getDispatchRefNo() {
		return dispatchRefNo;
	}

	public void setDispatchRefNo(String dispatchRefNo) {
		this.dispatchRefNo = dispatchRefNo;
	}

	public String getUdf_2() {
		return udf_2;
	}

	public void setUdf_2(String udf_2) {
		this.udf_2 = udf_2;
	}

	public JoinRelTrnRTGSMaster() {
		super();
	}

	
	@Column(name="BOR")
	private String bor;

	@Column(name="PayDetails")
	private String payDetails;

	@Column(name="RelTRN")
	private String relTRN;

	@Column(name="Status")
	private String status;

	@Column(name="TRN")
	private String trn;

	@Column(name="UltimateBeneAcc")
	private String ultimateBeneAcc;

	@Column(name="UltimateBeneName")
	private String ultimateBeneName;

	public String getBor() {
		return bor;
	}

	public void setBor(String bor) {
		this.bor = bor;
	}

	public String getPayDetails() {
		return payDetails;
	}

	public void setPayDetails(String payDetails) {
		this.payDetails = payDetails;
	}

	public String getRelTRN() {
		return relTRN;
	}

	public void setRelTRN(String relTRN) {
		this.relTRN = relTRN;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTrn() {
		return trn;
	}

	public void setTrn(String trn) {
		this.trn = trn;
	}

	public String getUltimateBeneAcc() {
		return ultimateBeneAcc;
	}

	public void setUltimateBeneAcc(String ultimateBeneAcc) {
		this.ultimateBeneAcc = ultimateBeneAcc;
	}

	public String getUltimateBeneName() {
		return ultimateBeneName;
	}

	public void setUltimateBeneName(String ultimateBeneName) {
		this.ultimateBeneName = ultimateBeneName;
	}

	
}
