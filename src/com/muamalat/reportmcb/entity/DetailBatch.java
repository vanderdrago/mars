package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class DetailBatch extends BatchMaster {
	private String trn_ref_no;
	private Date trn_dt;
	private String event;
	private String ac_entry_sr_no;
	private String ac_branch;
	private String ac_no;
	private String ac_ccy;
	private String drcr_ind;
	private String trn_code;
	private String trn_desc;
	private BigDecimal lcy_amount;
	private String related_customer;
	private String auth_id;
	private String remarks;
	private String gl_desc;
	
	
	
	public String getTrn_ref_no() {
		return trn_ref_no;
	}

	public void setTrn_ref_no(String trn_ref_no) {
		this.trn_ref_no = trn_ref_no;
	}

	public Date getTrn_dt() {
		return trn_dt;
	}

	public void setTrn_dt(Date trn_dt) {
		this.trn_dt = trn_dt;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getAc_entry_sr_no() {
		return ac_entry_sr_no;
	}

	public void setAc_entry_sr_no(String ac_entry_sr_no) {
		this.ac_entry_sr_no = ac_entry_sr_no;
	}

	public String getAc_branch() {
		return ac_branch;
	}

	public void setAc_branch(String ac_branch) {
		this.ac_branch = ac_branch;
	}

	public String getAc_no() {
		return ac_no;
	}

	public void setAc_no(String ac_no) {
		this.ac_no = ac_no;
	}

	public String getAc_ccy() {
		return ac_ccy;
	}

	public void setAc_ccy(String ac_ccy) {
		this.ac_ccy = ac_ccy;
	}

	public String getDrcr_ind() {
		return drcr_ind;
	}

	public void setDrcr_ind(String drcr_ind) {
		this.drcr_ind = drcr_ind;
	}

	public String getTrn_code() {
		return trn_code;
	}

	public void setTrn_code(String trn_code) {
		this.trn_code = trn_code;
	}

	public BigDecimal getLcy_amount() {
		return lcy_amount;
	}

	public void setLcy_amount(BigDecimal lcy_amount) {
		this.lcy_amount = lcy_amount;
	}

	public String getRelated_customer() {
		return related_customer;
	}

	public void setRelated_customer(String related_customer) {
		this.related_customer = related_customer;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getTrn_desc() {
		return trn_desc;
	}

	public void setTrn_desc(String trn_desc) {
		this.trn_desc = trn_desc;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getGl_desc() {
		return gl_desc;
	}

	public void setGl_desc(String glDesc) {
		gl_desc = glDesc;
	}
	
}
