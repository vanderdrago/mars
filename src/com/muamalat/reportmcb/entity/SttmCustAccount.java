	package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class SttmCustAccount {
	
	private String cust_ac_no;
	private String ac_desc;
	private String cust_no;
	private String ccy;
	private String account_class;
	private String branch_code;
	private String nationality;
	private String branch_name;
	private String CUST_AC_NO;
	private String acDesc;
	private String custNo;
	private String accountClass;
	private String desc_acc;
	private String address1;
	private String address2;
	private String address3;
	private String address_line1;
	private String address_line2;
	private String address_line3;
	
	private String DATE_OF_BIRTH;
	private String UNIQUE_ID_VALUE;
	private BigDecimal acyCurrBalance;
	private BigDecimal acyBlockedAmount;
	private BigDecimal acyAvlBal;
	private BigDecimal minBalance;
	private BigDecimal acy_bal;
	private BigDecimal amount;
	private String AC_OPEN_DATE;
	private String AC_CLOSE_DATE;
	private String record_stat;
	private String alt_ac_no;
	private String telephone;
	private String mobile_number;
	private String email;
	private String p_nationalid;
	private String CIF_CREATION_DATE;
	private String CUSTOMER_CATEGORY;
		
	private String fieldVal1;
	private String fieldVal2;
	private String fieldVal3;
	private String fieldVal4;
	private String fieldVal5;
	private String fieldVal6;
	private String fieldVal7;
	private String fieldVal8;
	private String fieldVal9;
	private String fieldVal10;
	private String fieldVal11;
	private String fieldVal12;	
	private String fieldVal13;	
	private String fieldVal14;
	private String fieldVal15;
	private String fieldVal16;
	private String fieldVal17;
	private String fieldVal18;
	private String fieldVal19;
	private String fieldVal20;
	private String fieldVal21;	
	private String fieldVal22;
	private String fieldVal23;
	private String fieldVal24;	
	private String kode_marketing;	
	private String function_id;
	private String tgl_kadaluarsa_id;
	private String sts_perkawinan;
	private String penghsl_ttp;
	private String penghsl_tdk_ttp;
	private String pengeluaran_ttp;
	private String rtrt_penghsl_tdk_ttp;
	private String info_mmprolh_hsl;
	private String customer_type;
	
	private String resident_status;
	private String ibu_kandung;
	private String fax;
	private String jenis_kelamin;
	private String agama;
	private String pekerjaan;
	private String bidang_pekerjaan;
	private String pendidikan;	
	private String sumber_dana;
	private String tujuan_buka_rek;
	
	private String jalan;
	private String rt_rw;
	private String kelurahan;
	private String kecamatan;
	private String kota_kabupaten;
	private String provinsi;
	private String kode_pos;
	private String tempat_lahir;
	
	private String expiryDate;
	private String remarks;
	private String effectiveDate;
	private String modNo;
	
	private String statDormant;
	private String statDebet;
	private String statCredit;
	private String statFrozen;
	private String handphone;
	private String marketing;
	private String accType;
	
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public String getMarketing() {
		return marketing;
	}
	public void setMarketing(String marketing) {
		this.marketing = marketing;
	}
	public String getHandphone() {
		return handphone;
	}
	public void setHandphone(String handphone) {
		this.handphone = handphone;
	}
	public String getStatDormant() {
		return statDormant;
	}
	public void setStatDormant(String statDormant) {
		this.statDormant = statDormant;
	}
	public String getStatDebet() {
		return statDebet;
	}
	public void setStatDebet(String statDebet) {
		this.statDebet = statDebet;
	}
	public String getStatCredit() {
		return statCredit;
	}
	public void setStatCredit(String statCredit) {
		this.statCredit = statCredit;
	}
	public String getStatFrozen() {
		return statFrozen;
	}
	public void setStatFrozen(String statFrozen) {
		this.statFrozen = statFrozen;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getModNo() {
		return modNo;
	}
	public void setModNo(String modNo) {
		this.modNo = modNo;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getTempat_lahir() {
		return tempat_lahir;
	}
	public void setTempat_lahir(String tempat_lahir) {
		this.tempat_lahir = tempat_lahir;
	}
	public String getJalan() {
		return jalan;
	}
	public void setJalan(String jalan) {
		this.jalan = jalan;
	}
	public String getRt_rw() {
		return rt_rw;
	}
	public void setRt_rw(String rt_rw) {
		this.rt_rw = rt_rw;
	}
	public String getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}
	public String getKecamatan() {
		return kecamatan;
	}
	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	public String getKota_kabupaten() {
		return kota_kabupaten;
	}
	public void setKota_kabupaten(String kota_kabupaten) {
		this.kota_kabupaten = kota_kabupaten;
	}
	public String getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}
	public String getKode_pos() {
		return kode_pos;
	}
	public void setKode_pos(String kode_pos) {
		this.kode_pos = kode_pos;
	}
	public String getResident_status() {
		return resident_status;
	}
	public void setResident_status(String resident_status) {
		this.resident_status = resident_status;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getCust_ac_no() {
		return cust_ac_no;
	}
	public void setCust_ac_no(String custAcNo) {
		cust_ac_no = custAcNo;
	}
	public String getAc_desc() {
		return ac_desc;
	}
	public void setAc_desc(String acDesc) {
		ac_desc = acDesc;
	}
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String custNo) {
		cust_no = custNo;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getAccount_class() {
		return account_class;
	}
	public void setAccount_class(String accountClass) {
		account_class = accountClass;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getCUST_AC_NO() {
		return CUST_AC_NO;
	}
	public void setCUST_AC_NO(String cust_ac_no) {
		CUST_AC_NO = cust_ac_no;
	}
	public String getAcDesc() {
		return acDesc;
	}
	public void setAcDesc(String acDesc) {
		this.acDesc = acDesc;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getAccountClass() {
		return accountClass;
	}
	public void setAccountClass(String accountClass) {
		this.accountClass = accountClass;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public BigDecimal getAcyCurrBalance() {
		return acyCurrBalance;
	}
	public void setAcyCurrBalance(BigDecimal acyCurrBalance) {
		this.acyCurrBalance = acyCurrBalance;
	}
	public BigDecimal getAcyBlockedAmount() {
		return acyBlockedAmount;
	}
	public void setAcyBlockedAmount(BigDecimal acyBlockedAmount) {
		this.acyBlockedAmount = acyBlockedAmount;
	}
	public BigDecimal getAcyAvlBal() {
		return acyAvlBal;
	}
	public void setAcyAvlBal(BigDecimal acyAvlBal) {
		this.acyAvlBal = acyAvlBal;
	}
	public BigDecimal getMinBalance() {
		return minBalance;
	}
	public void setMinBalance(BigDecimal minBalance) {
		this.minBalance = minBalance;
	}
	public BigDecimal getAcy_bal() {
		return acy_bal;
	}
	public void setAcy_bal(BigDecimal acy_bal) {
		this.acy_bal = acy_bal;
	}
	public String getFieldVal1() {
		return fieldVal1;
	}
	public void setFieldVal1(String fieldVal1) {
		this.fieldVal1 = fieldVal1;
	}
	public String getFieldVal2() {
		return fieldVal2;
	}
	public void setFieldVal2(String fieldVal2) {
		this.fieldVal2 = fieldVal2;
	}
	public String getFieldVal3() {
		return fieldVal3;
	}
	public void setFieldVal3(String fieldVal3) {
		this.fieldVal3 = fieldVal3;
	}
	public String getFieldVal4() {
		return fieldVal4;
	}
	public void setFieldVal4(String fieldVal4) {
		this.fieldVal4 = fieldVal4;
	}
	public String getFieldVal5() {
		return fieldVal5;
	}
	public void setFieldVal5(String fieldVal5) {
		this.fieldVal5 = fieldVal5;
	}
	public String getFieldVal6() {
		return fieldVal6;
	}
	public void setFieldVal6(String fieldVal6) {
		this.fieldVal6 = fieldVal6;
	}
	public String getFieldVal7() {
		return fieldVal7;
	}
	public void setFieldVal7(String fieldVal7) {
		this.fieldVal7 = fieldVal7;
	}
	public String getFieldVal8() {
		return fieldVal8;
	}
	public void setFieldVal8(String fieldVal8) {
		this.fieldVal8 = fieldVal8;
	}
	public String getFieldVal9() {
		return fieldVal9;
	}
	public void setFieldVal9(String fieldVal9) {
		this.fieldVal9 = fieldVal9;
	}
	public String getFieldVal10() {
		return fieldVal10;
	}
	public void setFieldVal10(String fieldVal10) {
		this.fieldVal10 = fieldVal10;
	}
	public String getFieldVal11() {
		return fieldVal11;
	}
	public void setFieldVal11(String fieldVal11) {
		this.fieldVal11 = fieldVal11;
	}
	public String getFieldVal12() {
		return fieldVal12;
	}
	public void setFieldVal12(String fieldVal12) {
		this.fieldVal12 = fieldVal12;
	}
	public String getFieldVal13() {
		return fieldVal13;
	}
	public void setFieldVal13(String fieldVal13) {
		this.fieldVal13 = fieldVal13;
	}
	public String getFieldVal14() {
		return fieldVal14;
	}
	public void setFieldVal14(String fieldVal14) {
		this.fieldVal14 = fieldVal14;
	}
	public String getFieldVal15() {
		return fieldVal15;
	}
	public void setFieldVal15(String fieldVal15) {
		this.fieldVal15 = fieldVal15;
	}
	public String getFieldVal16() {
		return fieldVal16;
	}
	public void setFieldVal16(String fieldVal16) {
		this.fieldVal16 = fieldVal16;
	}
	public String getFieldVal17() {
		return fieldVal17;
	}
	public void setFieldVal17(String fieldVal17) {
		this.fieldVal17 = fieldVal17;
	}
	public String getFieldVal18() {
		return fieldVal18;
	}
	public void setFieldVal18(String fieldVal18) {
		this.fieldVal18 = fieldVal18;
	}
	public String getFieldVal19() {
		return fieldVal19;
	}
	public void setFieldVal19(String fieldVal19) {
		this.fieldVal19 = fieldVal19;
	}
	public String getFieldVal20() {
		return fieldVal20;
	}
	public void setFieldVal20(String fieldVal20) {
		this.fieldVal20 = fieldVal20;
	}
	public String getFieldVal21() {
		return fieldVal21;
	}
	public void setFieldVal21(String fieldVal21) {
		this.fieldVal21 = fieldVal21;
	}
	public String getFieldVal22() {
		return fieldVal22;
	}
	public void setFieldVal22(String fieldVal22) {
		this.fieldVal22 = fieldVal22;
	}
	public String getFieldVal23() {
		return fieldVal23;
	}
	public void setFieldVal23(String fieldVal23) {
		this.fieldVal23 = fieldVal23;
	}
	public String getFieldVal24() {
		return fieldVal24;
	}
	public void setFieldVal24(String fieldVal24) {
		this.fieldVal24 = fieldVal24;
	}
	public String getDATE_OF_BIRTH() {
		return DATE_OF_BIRTH;
	}
	public void setDATE_OF_BIRTH(String dATEOFBIRTH) {
		this.DATE_OF_BIRTH = dATEOFBIRTH;
	}
	public String getUNIQUE_ID_VALUE() {
		return UNIQUE_ID_VALUE;
	}
	public void setUNIQUE_ID_VALUE(String uNIQUEIDVALUE) {
		this.UNIQUE_ID_VALUE = uNIQUEIDVALUE;
	}
	public String getDesc_acc() {
		return desc_acc;
	}
	public void setDesc_acc(String desc_acc) {
		this.desc_acc = desc_acc;
	}
	public String getAC_OPEN_DATE() {
		return AC_OPEN_DATE;
	}
	public void setAC_OPEN_DATE(String ac_open_date) {
		this.AC_OPEN_DATE= ac_open_date;
	}
	
	public void setCIF_CREATION_DATE(String cif_creation_date) {
		CIF_CREATION_DATE = cif_creation_date;
	}
	public String getCIF_CREATION_DATE() {
		return CIF_CREATION_DATE;
	}
	
	public String getRecord_stat() {
		return record_stat;
	}
	public void setRecord_stat(String record_stat) {
		this.record_stat = record_stat;
	}
	public String getAlt_ac_no() {
		return alt_ac_no;
	}
	public void setAlt_ac_no(String alt_ac_no) {
		this.alt_ac_no = alt_ac_no;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getFax() {
		return fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAC_CLOSE_DATE() {
		return AC_CLOSE_DATE;
	}
	public void setAC_CLOSE_DATE(String ac_close_date) {
		this.AC_CLOSE_DATE = ac_close_date;
	}
	public String getP_nationalid() {
		return p_nationalid;
	}
	public void setP_nationalid(String p_nationalid) {
		this.p_nationalid = p_nationalid;
	}
	public String getKode_marketing() {
		return kode_marketing;
	}
	public void setKode_marketing(String kode_marketing) {
		this.kode_marketing = kode_marketing;
	}
	public String getFunction_id() {
		return function_id;
	}
	public void setFunction_id(String function_id) {
		this.function_id = function_id;
	}
	public String getAddress_line1() {
		return address_line1;
	}
	public void setAddress_line1(String address_line1) {
		this.address_line1 = address_line1;
	}
	public String getAddress_line2() {
		return address_line2;
	}
	public void setAddress_line2(String address_line2) {
		this.address_line2 = address_line2;
	}
	public String getAddress_line3() {
		return address_line3;
	}
	public void setAddress_line3(String address_line3) {
		this.address_line3 = address_line3;
	}
	public String getTgl_kadaluarsa_id() {
		return tgl_kadaluarsa_id;
	}
	public void setTgl_kadaluarsa_id(String tgl_kadaluarsa_id) {
		this.tgl_kadaluarsa_id = tgl_kadaluarsa_id;
	}
	public String getSts_perkawinan() {
		return sts_perkawinan;
	}
	public void setSts_perkawinan(String sts_perkawinan) {
		this.sts_perkawinan = sts_perkawinan;
	}
	public String getPenghsl_ttp() {
		return penghsl_ttp;
	}
	public void setPenghsl_ttp(String penghsl_ttp) {
		this.penghsl_ttp = penghsl_ttp;
	}
	public String getPenghsl_tdk_ttp() {
		return penghsl_tdk_ttp;
	}
	public void setPenghsl_tdk_ttp(String penghsl_tdk_ttp) {
		this.penghsl_tdk_ttp = penghsl_tdk_ttp;
	}
	public String getPengeluaran_ttp() {
		return pengeluaran_ttp;
	}
	public void setPengeluaran_ttp(String pengeluaran_ttp) {
		this.pengeluaran_ttp = pengeluaran_ttp;
	}
	public String getRtrt_penghsl_tdk_ttp() {
		return rtrt_penghsl_tdk_ttp;
	}
	public void setRtrt_penghsl_tdk_ttp(String rtrt_penghsl_tdk_ttp) {
		this.rtrt_penghsl_tdk_ttp = rtrt_penghsl_tdk_ttp;
	}
	public String getInfo_mmprolh_hsl() {
		return info_mmprolh_hsl;
	}
	public void setInfo_mmprolh_hsl(String info_mmprolh_hsl) {
		this.info_mmprolh_hsl = info_mmprolh_hsl;
	}
	public String getCustomer_type() {
		return customer_type;
	}
	public void setCustomer_type(String customer_type) {
		this.customer_type = customer_type;
	}
	public void setIbu_kandung(String ibu_kandung) {
		this.ibu_kandung = ibu_kandung;
	}
	public String getIbu_kandung() {
		return ibu_kandung;
	}
	public void setCUSTOMER_CATEGORY(String customer_category) {
		CUSTOMER_CATEGORY = customer_category;
	}
	public String getCUSTOMER_CATEGORY() {
		return CUSTOMER_CATEGORY;
	}
	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}
	public String getJenis_kelamin() {
		return jenis_kelamin;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	public String getAgama() {
		return agama;
	}
	public void setPekerjaan(String pekerjaan) {
		this.pekerjaan = pekerjaan;
	}
	public String getPekerjaan() {
		return pekerjaan;
	}
	public void setBidang_pekerjaan(String bidang_pekerjaan) {
		this.bidang_pekerjaan = bidang_pekerjaan;
	}
	public String getBidang_pekerjaan() {
		return bidang_pekerjaan;
	}
	public void setPendidikan(String pendidikan) {
		this.pendidikan = pendidikan;
	}
	public String getPendidikan() {
		return pendidikan;
	}
	public void setSumber_dana(String sumber_dana) {
		this.sumber_dana = sumber_dana;
	}
	public String getSumber_dana() {
		return sumber_dana;
	}
	public void setTujuan_buka_rek(String tujuan_buka_rek) {
		this.tujuan_buka_rek = tujuan_buka_rek;
	}
	public String getTujuan_buka_rek() {
		return tujuan_buka_rek;
	}
	

	
}
