package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Pcode {
	private String pcd;
	private String sts;
	private int tot;
	private BigDecimal nom;
		
	public String getPcd() {
		return pcd;
	}
	public void setPcd(String pcd) {
		this.pcd = pcd;
	}
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
	public int getTot() {
		return tot;
	}
	public void setTot(int tot) {
		this.tot = tot;
	}
	public BigDecimal getNom() {
		return nom;
	}
	public void setNom(BigDecimal nom) {
		this.nom = nom;
	}
	
}
