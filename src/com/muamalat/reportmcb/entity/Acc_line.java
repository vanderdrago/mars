package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

//import sun.security.util.BigInt;

public class Acc_line {
	private BigDecimal id;
	private int no;
	private String account_no;
	private String branch;
	private String ccy;
	private BigDecimal ac_entry_sr_no;
	private int prev_line_no;
	private int prev_line_no_start;
	private int prev_page_no;
	private int total_trns;
	private String passbook_number;
	private Timestamp print_date;
	private String print_date_s;
	private Date last_date_trn;
	private String last_date_trn_s;
	private String user_print;
	private String user_spv_print;
	private String sts;
	private String notes;		
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getAccount_no() {
		return account_no;
	}
	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAc_entry_sr_no() {
		return ac_entry_sr_no;
	}
	public void setAc_entry_sr_no(BigDecimal ac_entry_sr_no) {
		this.ac_entry_sr_no = ac_entry_sr_no;
	}
	public int getPrev_line_no() {
		return prev_line_no;
	}
	public void setPrev_line_no(int prev_line_no) {
		this.prev_line_no = prev_line_no;
	}
	public int getPrev_line_no_start() {
		return prev_line_no_start;
	}
	public void setPrev_line_no_start(int prev_line_no_start) {
		this.prev_line_no_start = prev_line_no_start;
	}
	public int getPrev_page_no() {
		return prev_page_no;
	}
	public void setPrev_page_no(int prev_page_no) {
		this.prev_page_no = prev_page_no;
	}
	public int getTotal_trns() {
		return total_trns;
	}
	public void setTotal_trns(int total_trns) {
		this.total_trns = total_trns;
	}
	public String getPassbook_number() {
		return passbook_number;
	}
	public void setPassbook_number(String passbook_number) {
		this.passbook_number = passbook_number;
	}
	public Timestamp getPrint_date() {
		return print_date;
	}
	public void setPrint_date(Timestamp print_date) {
		this.print_date = print_date;
	}
	public String getPrint_date_s() {
		return print_date_s;
	}
	public void setPrint_date_s(String print_date_s) {
		this.print_date_s = print_date_s;
	}
	public Date getLast_date_trn() {
		return last_date_trn;
	}
	public void setLast_date_trn(Date last_date_trn) {
		this.last_date_trn = last_date_trn;
	}
	public String getLast_date_trn_s() {
		return last_date_trn_s;
	}
	public void setLast_date_trn_s(String last_date_trn_s) {
		this.last_date_trn_s = last_date_trn_s;
	}
	public String getUser_print() {
		return user_print;
	}
	public void setUser_print(String user_print) {
		this.user_print = user_print;
	}
	public String getUser_spv_print() {
		return user_spv_print;
	}
	public void setUser_spv_print(String user_spv_print) {
		this.user_spv_print = user_spv_print;
	}
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
}
