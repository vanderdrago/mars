package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class KonversiRekening {
	
	private String alt_rekening;
	private String alt_nm_rekening;
	private String rekening;
	private String nmrekening;
	private String status;
	private BigDecimal saldo;
	
	public String getAlt_rekening() {
		return alt_rekening;
	}
	public void setAlt_rekening(String alt_rekening) {
		this.alt_rekening = alt_rekening;
	}
	public String getAlt_nm_rekening() {
		return alt_nm_rekening;
	}
	public void setAlt_nm_rekening(String alt_nm_rekening) {
		this.alt_nm_rekening = alt_nm_rekening;
	}
	public String getRekening() {
		return rekening;
	}
	public void setRekening(String rekening) {
		this.rekening = rekening;
	}
	public String getNmrekening() {
		return nmrekening;
	}
	public void setNmrekening(String nmrekening) {
		this.nmrekening = nmrekening;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}	
	
}
