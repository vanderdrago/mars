package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class Event {

	public String event_sr_no;
    
	public String event_seq_no;
    
	public String ac_entry_sr_no;
    
	public String event_entry_sr_no;
    
	public String module;
	
	public String ac_no;
	
	public String branch_code;
	
	public String product_code;
	
	public String value_date;
	
	public String schedule_due_date;
	
	public String ccy;
	
	public String account_number;
	
	public String event_code;
	
	public BigDecimal amount;
    
	public BigDecimal amount_settled;
    
	public String amount_tag;
    
	public String from_status;
    
	public String to_status;
    
	public String component_name;
    
	public String drcr_ind;
    
	public String drtrnrefno;
    
	public String crtrnrefno;
	
	public String refno;
    
	public String dr_acc;
	
	public String cr_acc;
	
	public String acc;
    
	public String ac_desc;
	
	public String dr_trn_code;
	
	public String cr_trn_code;
	
	public String trn_code;
	
	public String trn_desc;
	
	public String dr_acc_role;
	
	public String cr_acc_role;
	
	public String acc_role;

	public String getEvent_sr_no() {
		return event_sr_no;
	}

	public void setEvent_sr_no(String eventSrNo) {
		event_sr_no = eventSrNo;
	}

	public String getEvent_seq_no() {
		return event_seq_no;
	}

	public void setEvent_seq_no(String eventSeqNo) {
		event_seq_no = eventSeqNo;
	}

	public String getAc_entry_sr_no() {
		return ac_entry_sr_no;
	}

	public void setAc_entry_sr_no(String acEntrySrNo) {
		ac_entry_sr_no = acEntrySrNo;
	}

	public String getEvent_entry_sr_no() {
		return event_entry_sr_no;
	}

	public void setEvent_entry_sr_no(String eventEntrySrNo) {
		event_entry_sr_no = eventEntrySrNo;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getAc_no() {
		return ac_no;
	}

	public void setAc_no(String acNo) {
		ac_no = acNo;
	}

	public String getBranch_code() {
		return branch_code;
	}

	public void setBranch_code(String branchCode) {
		branch_code = branchCode;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String productCode) {
		product_code = productCode;
	}

	public String getValue_date() {
		return value_date;
	}

	public void setValue_date(String valueDate) {
		value_date = valueDate;
	}

	public String getSchedule_due_date() {
		return schedule_due_date;
	}

	public void setSchedule_due_date(String scheduleDueDate) {
		schedule_due_date = scheduleDueDate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String accountNumber) {
		account_number = accountNumber;
	}

	public String getEvent_code() {
		return event_code;
	}

	public void setEvent_code(String eventCode) {
		event_code = eventCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		if(amount == null){
    		this.amount = new BigDecimal(0);
    	} else {
    		this.amount = amount;
    	}  
	}

	public BigDecimal getAmount_settled() {
		return amount_settled;
	}

	public void setAmount_settled(BigDecimal amountSettled) {
		if(amountSettled == null){
    		this.amount_settled = new BigDecimal(0);
    	} else {
    		amount_settled = amountSettled;
    	}  
	}

	public String getAmount_tag() {
		return amount_tag;
	}

	public void setAmount_tag(String amountTag) {
		amount_tag = amountTag;
	}

	public String getFrom_status() {
		return from_status;
	}

	public void setFrom_status(String fromStatus) {
		from_status = fromStatus;
	}

	public String getTo_status() {
		return to_status;
	}

	public void setTo_status(String toStatus) {
		to_status = toStatus;
	}

	public String getComponent_name() {
		return component_name;
	}

	public void setComponent_name(String componentName) {
		component_name = componentName;
	}

	public String getDrcr_ind() {
		return drcr_ind;
	}

	public void setDrcr_ind(String drcrInd) {
		drcr_ind = drcrInd;
	}

	public String getDrtrnrefno() {
		return drtrnrefno;
	}

	public void setDrtrnrefno(String drtrnrefno) {
		this.drtrnrefno = drtrnrefno;
	}

	public String getCrtrnrefno() {
		return crtrnrefno;
	}

	public void setCrtrnrefno(String crtrnrefno) {
		this.crtrnrefno = crtrnrefno;
	}

	public String getRefno() {
		return refno;
	}

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public String getDr_acc() {
		return dr_acc;
	}

	public void setDr_acc(String drAcc) {
		dr_acc = drAcc;
	}

	public String getCr_acc() {
		return cr_acc;
	}

	public void setCr_acc(String crAcc) {
		cr_acc = crAcc;
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getAc_desc() {
		return ac_desc;
	}

	public void setAc_desc(String acDesc) {
		ac_desc = acDesc;
	}

	public String getDr_trn_code() {
		return dr_trn_code;
	}

	public void setDr_trn_code(String drTrnCode) {
		dr_trn_code = drTrnCode;
	}

	public String getCr_trn_code() {
		return cr_trn_code;
	}

	public void setCr_trn_code(String crTrnCode) {
		cr_trn_code = crTrnCode;
	}

	public String getTrn_code() {
		return trn_code;
	}

	public void setTrn_code(String trnCode) {
		trn_code = trnCode;
	}

	public String getTrn_desc() {
		return trn_desc;
	}

	public void setTrn_desc(String trnDesc) {
		trn_desc = trnDesc;
	}

	public String getDr_acc_role() {
		return dr_acc_role;
	}

	public void setDr_acc_role(String drAccRole) {
		dr_acc_role = drAccRole;
	}

	public String getCr_acc_role() {
		return cr_acc_role;
	}

	public void setCr_acc_role(String crAccRole) {
		cr_acc_role = crAccRole;
	}

	public String getAcc_role() {
		return acc_role;
	}

	public void setAcc_role(String accRole) {
		acc_role = accRole;
	}
		
}
