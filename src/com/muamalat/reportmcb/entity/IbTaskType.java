package com.muamalat.reportmcb.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;


/**
 * The persistent class for the IB_TASK_TYPES database table.
 * 
 */
@Entity
@Table(name="IB_TASK_TYPES")
public class IbTaskType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TASK_TYPE")
	private String taskType;

	@Column(name="AUTHORIZE_FLAG")
	private BigDecimal authorizeFlag;

	@Column(name="CHARGE_AMOUNT")
	private BigDecimal chargeAmount;

	@Column(name="CHARGE_FLAG")
	private BigDecimal chargeFlag;

	@Column(name="INQUIRY_FLAG")
	private BigDecimal inquiryFlag;

	@Column(name="LIMIT_AMOUNT")
	private BigDecimal limitAmount;

	@Column(name="LIMIT_FLAG")
	private BigDecimal limitFlag;

	@Column(name="NAME_E")
	private String nameE;

	@Column(name="NAME_I")
	private String nameI;

	@Column(name="PARENT_TASK_TYPE")
	private String parentTaskType;

	@Column(name="REVIEW_FLAG")
	private BigDecimal reviewFlag;

	private BigDecimal status;

	@Column(name="TASK_TYPE_LEVEL")
	private BigDecimal taskTypeLevel;

	//bi-directional many-to-one association to IbTask
	@OneToMany(mappedBy="ibTaskType")
	private Set<IbTask> ibTasks;

    public IbTaskType() {
    }

	public String getTaskType() {
		return this.taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public BigDecimal getAuthorizeFlag() {
		return this.authorizeFlag;
	}

	public void setAuthorizeFlag(BigDecimal authorizeFlag) {
		this.authorizeFlag = authorizeFlag;
	}

	public BigDecimal getChargeAmount() {
		return this.chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public BigDecimal getChargeFlag() {
		return this.chargeFlag;
	}

	public void setChargeFlag(BigDecimal chargeFlag) {
		this.chargeFlag = chargeFlag;
	}

	public BigDecimal getInquiryFlag() {
		return this.inquiryFlag;
	}

	public void setInquiryFlag(BigDecimal inquiryFlag) {
		this.inquiryFlag = inquiryFlag;
	}

	public BigDecimal getLimitAmount() {
		return this.limitAmount;
	}

	public void setLimitAmount(BigDecimal limitAmount) {
		this.limitAmount = limitAmount;
	}

	public BigDecimal getLimitFlag() {
		return this.limitFlag;
	}

	public void setLimitFlag(BigDecimal limitFlag) {
		this.limitFlag = limitFlag;
	}

	public String getNameE() {
		return this.nameE;
	}

	public void setNameE(String nameE) {
		this.nameE = nameE;
	}

	public String getNameI() {
		return this.nameI;
	}

	public void setNameI(String nameI) {
		this.nameI = nameI;
	}

	public String getParentTaskType() {
		return this.parentTaskType;
	}

	public void setParentTaskType(String parentTaskType) {
		this.parentTaskType = parentTaskType;
	}

	public BigDecimal getReviewFlag() {
		return this.reviewFlag;
	}

	public void setReviewFlag(BigDecimal reviewFlag) {
		this.reviewFlag = reviewFlag;
	}

	public BigDecimal getStatus() {
		return this.status;
	}

	public void setStatus(BigDecimal status) {
		this.status = status;
	}

	public BigDecimal getTaskTypeLevel() {
		return this.taskTypeLevel;
	}

	public void setTaskTypeLevel(BigDecimal taskTypeLevel) {
		this.taskTypeLevel = taskTypeLevel;
	}

	public Set<IbTask> getIbTasks() {
		return this.ibTasks;
	}

	public void setIbTasks(Set<IbTask> ibTasks) {
		this.ibTasks = ibTasks;
	}
	
}