package com.muamalat.reportmcb.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the IB_TASKS database table.
 * 
 */
@Entity
@Table(name="IB_TASKS")
public class IbTask implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REF_ID")
	private long refId;

	@Column(name="ACCOUNT_NO")
	private String accountNo;

	private BigDecimal amount;

	@Column(name="AMOUNT_LOCAL")
	private BigDecimal amountLocal;

	@Column(name="APPROVE_COUNT")
	private BigDecimal approveCount;

	@Column(name="APPROVE_NEED")
	private BigDecimal approveNeed;

	@Column(name="BRANCH_CODE")
	private String branchCode;

	@Column(name="CURRENCY_CODE")
	private String currencyCode;

    @Temporal( TemporalType.DATE)
	@Column(name="DATE_CREATE")
	private Date dateCreate;

    @Temporal( TemporalType.DATE)
	@Column(name="DATE_EXPIRE")
	private Date dateExpire;

    @Temporal( TemporalType.DATE)
	@Column(name="DATE_TRX")
	private Date dateTrx;

	@Column(name="EXECUTION_TYPE")
	private BigDecimal executionType;

    @Temporal( TemporalType.DATE)
	@Column(name="REC_DATE_EXPIRE")
	private Date recDateExpire;

	@Column(name="REC_MODE")
	private BigDecimal recMode;

	@Column(name="REC_PARAM")
	private BigDecimal recParam;

	@Column(name="REC_SESSION")
	private BigDecimal recSession;

	@Column(name="REC_STATUS")
	private BigDecimal recStatus;

	@Column(name="REF_ID_TARGET")
	private BigDecimal refIdTarget;

	@Column(name="REF_NO")
	private String refNo;

	@Column(name="REVIEW_COUNT")
	private BigDecimal reviewCount;

	@Column(name="REVIEW_NEED")
	private BigDecimal reviewNeed;

	@Column(name="STATUS_DESCRIPTION_E")
	private String statusDescriptionE;

	@Column(name="STATUS_DESCRIPTION_I")
	private String statusDescriptionI;

	@Column(name="STATUS_TF")
	private BigDecimal statusTf;

	private String subject;

	@Column(name="TRX_COUNT_ALL")
	private BigDecimal trxCountAll;

	@Column(name="TRX_COUNT_FAIL")
	private BigDecimal trxCountFail;

	@Column(name="TRX_COUNT_SUCCESS")
	private BigDecimal trxCountSuccess;

	@Column(name="TRX_TOTAL")
	private BigDecimal trxTotal;

	@Column(name="TRX_TYPE")
	private BigDecimal trxType;

	@Column(name="USER_ID")
	private BigDecimal userId;

	//bi-directional many-to-one association to IbTaskType
    @ManyToOne
	@JoinColumn(name="TASK_TYPE")
	private IbTaskType ibTaskType;

    public IbTask() {
    }

	public long getRefId() {
		return this.refId;
	}

	public void setRefId(long refId) {
		this.refId = refId;
	}

	public String getAccountNo() {
		return this.accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountLocal() {
		return this.amountLocal;
	}

	public void setAmountLocal(BigDecimal amountLocal) {
		this.amountLocal = amountLocal;
	}

	public BigDecimal getApproveCount() {
		return this.approveCount;
	}

	public void setApproveCount(BigDecimal approveCount) {
		this.approveCount = approveCount;
	}

	public BigDecimal getApproveNeed() {
		return this.approveNeed;
	}

	public void setApproveNeed(BigDecimal approveNeed) {
		this.approveNeed = approveNeed;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Date getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Date getDateExpire() {
		return this.dateExpire;
	}

	public void setDateExpire(Date dateExpire) {
		this.dateExpire = dateExpire;
	}

	public Date getDateTrx() {
		return this.dateTrx;
	}

	public void setDateTrx(Date dateTrx) {
		this.dateTrx = dateTrx;
	}

	public BigDecimal getExecutionType() {
		return this.executionType;
	}

	public void setExecutionType(BigDecimal executionType) {
		this.executionType = executionType;
	}

	public Date getRecDateExpire() {
		return this.recDateExpire;
	}

	public void setRecDateExpire(Date recDateExpire) {
		this.recDateExpire = recDateExpire;
	}

	public BigDecimal getRecMode() {
		return this.recMode;
	}

	public void setRecMode(BigDecimal recMode) {
		this.recMode = recMode;
	}

	public BigDecimal getRecParam() {
		return this.recParam;
	}

	public void setRecParam(BigDecimal recParam) {
		this.recParam = recParam;
	}

	public BigDecimal getRecSession() {
		return this.recSession;
	}

	public void setRecSession(BigDecimal recSession) {
		this.recSession = recSession;
	}

	public BigDecimal getRecStatus() {
		return this.recStatus;
	}

	public void setRecStatus(BigDecimal recStatus) {
		this.recStatus = recStatus;
	}

	public BigDecimal getRefIdTarget() {
		return this.refIdTarget;
	}

	public void setRefIdTarget(BigDecimal refIdTarget) {
		this.refIdTarget = refIdTarget;
	}

	public String getRefNo() {
		return this.refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public BigDecimal getReviewCount() {
		return this.reviewCount;
	}

	public void setReviewCount(BigDecimal reviewCount) {
		this.reviewCount = reviewCount;
	}

	public BigDecimal getReviewNeed() {
		return this.reviewNeed;
	}

	public void setReviewNeed(BigDecimal reviewNeed) {
		this.reviewNeed = reviewNeed;
	}

	public String getStatusDescriptionE() {
		return this.statusDescriptionE;
	}

	public void setStatusDescriptionE(String statusDescriptionE) {
		this.statusDescriptionE = statusDescriptionE;
	}

	public String getStatusDescriptionI() {
		return this.statusDescriptionI;
	}

	public void setStatusDescriptionI(String statusDescriptionI) {
		this.statusDescriptionI = statusDescriptionI;
	}

	public BigDecimal getStatusTf() {
		return this.statusTf;
	}

	public void setStatusTf(BigDecimal statusTf) {
		this.statusTf = statusTf;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public BigDecimal getTrxCountAll() {
		return this.trxCountAll;
	}

	public void setTrxCountAll(BigDecimal trxCountAll) {
		this.trxCountAll = trxCountAll;
	}

	public BigDecimal getTrxCountFail() {
		return this.trxCountFail;
	}

	public void setTrxCountFail(BigDecimal trxCountFail) {
		this.trxCountFail = trxCountFail;
	}

	public BigDecimal getTrxCountSuccess() {
		return this.trxCountSuccess;
	}

	public void setTrxCountSuccess(BigDecimal trxCountSuccess) {
		this.trxCountSuccess = trxCountSuccess;
	}

	public BigDecimal getTrxTotal() {
		return this.trxTotal;
	}

	public void setTrxTotal(BigDecimal trxTotal) {
		this.trxTotal = trxTotal;
	}

	public BigDecimal getTrxType() {
		return this.trxType;
	}

	public void setTrxType(BigDecimal trxType) {
		this.trxType = trxType;
	}

	public BigDecimal getUserId() {
		return this.userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public IbTaskType getIbTaskType() {
		return this.ibTaskType;
	}

	public void setIbTaskType(IbTaskType ibTaskType) {
		this.ibTaskType = ibTaskType;
	}
	
}