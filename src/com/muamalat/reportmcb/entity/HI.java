package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class HI {

	private BigDecimal idr;
	private BigDecimal usd;
	private String date;
	private String lccyCode;
	private String fccyCode;
	private String glClass;
	
	public String getGlClass() {
		return glClass;
	}
	public void setGlClass(String glClass) {
		this.glClass = glClass;
	}
	public String getLccyCode() {
		return lccyCode;
	}
	public void setLccyCode(String lccyCode) {
		this.lccyCode = lccyCode;
	}
	public String getFccyCode() {
		return fccyCode;
	}
	public void setFccyCode(String fccyCode) {
		this.fccyCode = fccyCode;
	}
	public BigDecimal getIdr() {
		return idr;
	}
	public void setIdr(BigDecimal idr) {
		this.idr = idr;
	}
	public BigDecimal getUsd() {
		return usd;
	}
	public void setUsd(BigDecimal usd) {
		this.usd = usd;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
