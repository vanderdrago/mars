package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class fbti {
	
	private int tenor;
	
	private BigDecimal plafond;
	private BigDecimal marginAwal;
	private BigDecimal hargaJual;
	private BigDecimal profitMargin;
	
	private String InstallmentDate;
	private BigDecimal amountPrincipal;
	private BigDecimal marginAmount;
	private BigDecimal principalRepayment;
	private BigDecimal totalRepayment;
	private String installmentNo;
	
	
	private String tanggal;
	
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	private double rate;
	
	public String getInstallmentNo() {
		return installmentNo;
	}
	public void setInstallmentNo(String installmentNo) {
		this.installmentNo = installmentNo;
	}
	public int getTenor() {
		return tenor;
	}
	public void setTenor(int tenor) {
		this.tenor = tenor;
	}
	
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public BigDecimal getPlafond() {
		return plafond;
	}
	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}
	public BigDecimal getMarginAwal() {
		return marginAwal;
	}
	public void setMarginAwal(BigDecimal marginAwal) {
		this.marginAwal = marginAwal;
	}
	public BigDecimal getHargaJual() {
		return hargaJual;
	}
	public void setHargaJual(BigDecimal hargaJual) {
		this.hargaJual = hargaJual;
	}
	public BigDecimal getProfitMargin() {
		return profitMargin;
	}
	public void setProfitMargin(BigDecimal profitMargin) {
		this.profitMargin = profitMargin;
	}
	public String getInstallmentDate() {
		return InstallmentDate;
	}
	public void setInstallmentDate(String installmentDate) {
		InstallmentDate = installmentDate;
	}
	public BigDecimal getAmountPrincipal() {
		return amountPrincipal;
	}
	public void setAmountPrincipal(BigDecimal amountPrincipal) {
		this.amountPrincipal = amountPrincipal;
	}
	public BigDecimal getMarginAmount() {
		return marginAmount;
	}
	public void setMarginAmount(BigDecimal marginAmount) {
		this.marginAmount = marginAmount;
	}
	public BigDecimal getPrincipalRepayment() {
		return principalRepayment;
	}
	public void setPrincipalRepayment(BigDecimal principalRepayment) {
		this.principalRepayment = principalRepayment;
	}
	public BigDecimal getTotalRepayment() {
		return totalRepayment;
	}
	public void setTotalRepayment(BigDecimal totalRepayment) {
		this.totalRepayment = totalRepayment;
	}
	
	
	
	
}
