package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class salamMuamalat {
	private String nama_nasabah;
	private String lahir;
	private String no_kartu;
	private String no_rek_angsuran;
	private String no_cif;
	private String produk_kode;
	private String produk_kode_type;
	private String produk_kategori;
	private String nama_produk;
	private String tanggal_pencairan;
	private String tanggal_jatuh_tempo;
	private String tanggal_jatuh_tempo_angsuran;
	private String tanggal_angsuran_terakhir;
	private String tanggal_angsur_selanjutnya;
	private String status_aktif;
	
	private BigDecimal piutang;
	private BigDecimal pembiayaan;
	private BigDecimal denda;
	private BigDecimal tunggakan_denda;
	private String jangka_waktu;
	private BigDecimal angsuran_terakhir;
	private BigDecimal tunggakan_angsuran;
	private String sisa_angsuran;
	
	private String branch;
	private String alamat;
	private String telp;
	
	private String kode_marketing;
	private String nama_marketing;
	
	private String currency;
	private String tanggalTerakhir;
	private String tanggalSelanjutnya;
	
	
	
	public String getTanggalTerakhir() {
		return tanggalTerakhir;
	}
	public void setTanggalTerakhir(String tanggalTerakhir) {
		this.tanggalTerakhir = tanggalTerakhir;
	}
	public String getTanggalSelanjutnya() {
		return tanggalSelanjutnya;
	}
	public void setTanggalSelanjutnya(String tanggalSelanjutnya) {
		this.tanggalSelanjutnya = tanggalSelanjutnya;
	}
	public String getTanggal_angsuran_terakhir() {
		return tanggal_angsuran_terakhir;
	}
	public void setTanggal_angsuran_terakhir(String tanggal_angsuran_terakhir) {
		this.tanggal_angsuran_terakhir = tanggal_angsuran_terakhir;
	}
	public String getTanggal_angsur_selanjutnya() {
		return tanggal_angsur_selanjutnya;
	}
	public void setTanggal_angsur_selanjutnya(String tanggal_angsur_selanjutnya) {
		this.tanggal_angsur_selanjutnya = tanggal_angsur_selanjutnya;
	}
	public String getSisa_angsuran() {
		return sisa_angsuran;
	}
	public void setSisa_angsuran(String sisa_angsuran) {
		this.sisa_angsuran = sisa_angsuran;
	}
	public BigDecimal getAngsuran_terakhir() {
		return angsuran_terakhir;
	}
	public void setAngsuran_terakhir(BigDecimal angsuran_terakhir) {
		this.angsuran_terakhir = angsuran_terakhir;
	}
	public BigDecimal getTunggakan_angsuran() {
		return tunggakan_angsuran;
	}
	public void setTunggakan_angsuran(BigDecimal tunggakan_angsuran) {
		this.tunggakan_angsuran = tunggakan_angsuran;
	}
	public String getTanggal_jatuh_tempo_angsuran() {
		return tanggal_jatuh_tempo_angsuran;
	}
	public void setTanggal_jatuh_tempo_angsuran(String tanggal_jatuh_tempo_angsuran) {
		this.tanggal_jatuh_tempo_angsuran = tanggal_jatuh_tempo_angsuran;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	private BigDecimal total_angsuran;
	
	public BigDecimal getTotal_angsuran() {
		return total_angsuran;
	}
	public void setTotal_angsuran(BigDecimal total_angsuran) {
		this.total_angsuran = total_angsuran;
	}
	public String getKode_marketing() {
		return kode_marketing;
	}
	public void setKode_marketing(String kode_marketing) {
		this.kode_marketing = kode_marketing;
	}
	public String getNama_marketing() {
		return nama_marketing;
	}
	public void setNama_marketing(String nama_marketing) {
		this.nama_marketing = nama_marketing;
	}
	public String getProduk_kode_type() {
		return produk_kode_type;
	}
	public void setProduk_kode_type(String produk_kode_type) {
		this.produk_kode_type = produk_kode_type;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telp) {
		this.telp = telp;
	}
	
	public String getTanggal_pencairan() {
		return tanggal_pencairan;
	}
	public void setTanggal_pencairan(String tanggal_pencairan) {
		this.tanggal_pencairan = tanggal_pencairan;
	}
	
	public BigDecimal getDenda() {
		return denda;
	}
	public void setDenda(BigDecimal denda) {
		this.denda = denda;
	}
	public BigDecimal getTunggakan_denda() {
		return tunggakan_denda;
	}
	public void setTunggakan_denda(BigDecimal tunggakan_denda) {
		this.tunggakan_denda = tunggakan_denda;
	}
	public String getJangka_waktu() {
		return jangka_waktu;
	}
	public void setJangka_waktu(String jangka_waktu) {
		this.jangka_waktu = jangka_waktu;
	}
	public BigDecimal getPembiayaan() {
		return pembiayaan;
	}
	public void setPembiayaan(BigDecimal pembiayaan) {
		this.pembiayaan = pembiayaan;
	}
	public BigDecimal getPiutang() {
		return piutang;
	}
	public void setPiutang(BigDecimal piutang) {
		this.piutang = piutang;
	}
	public String getNama_nasabah() {
		return nama_nasabah;
	}
	public void setNama_nasabah(String nama_nasabah) {
		this.nama_nasabah = nama_nasabah;
	}
	public String getLahir() {
		return lahir;
	}
	public void setLahir(String lahir) {
		this.lahir = lahir;
	}
	public String getNo_kartu() {
		return no_kartu;
	}
	public void setNo_kartu(String no_kartu) {
		this.no_kartu = no_kartu;
	}
	public String getNo_rek_angsuran() {
		return no_rek_angsuran;
	}
	public void setNo_rek_angsuran(String no_rek_angsuran) {
		this.no_rek_angsuran = no_rek_angsuran;
	}
	public String getNo_cif() {
		return no_cif;
	}
	public void setNo_cif(String no_cif) {
		this.no_cif = no_cif;
	}
	public String getProduk_kode() {
		return produk_kode;
	}
	public void setProduk_kode(String produk_kode) {
		this.produk_kode = produk_kode;
	}
	public String getProduk_kategori() {
		return produk_kategori;
	}
	public void setProduk_kategori(String produk_kategori) {
		this.produk_kategori = produk_kategori;
	}
	public String getNama_produk() {
		return nama_produk;
	}
	public void setNama_produk(String nama_produk) {
		this.nama_produk = nama_produk;
	}
	public String getTanggal_jatuh_tempo() {
		return tanggal_jatuh_tempo;
	}
	public void setTanggal_jatuh_tempo(String tanggal_jatuh_tempo) {
		this.tanggal_jatuh_tempo = tanggal_jatuh_tempo;
	}
	public String getStatus_aktif() {
		return status_aktif;
	}
	public void setStatus_aktif(String status_aktif) {
		this.status_aktif = status_aktif;
	}
}
