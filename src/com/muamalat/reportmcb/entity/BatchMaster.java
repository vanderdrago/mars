package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class BatchMaster {
	 private String batch_no;
	 private String description;
	 private String user_id;
	 private BigDecimal dr_ent_total;
	 private BigDecimal cr_ent_total;
	 private Date last_oper_dt_stamp;
	 private String branch_code;
	 private String  auth_stat;
	 private String module;
	 private String trn_ref_no;
	 private String ac_ccy;
	 private String auth_id;
	 private String trn_code;
	 private String drcr_ind;
	 private Date trn_dt;
	 private String ac_no;
     private String last_oper_id;
	 private String BATCH_MODE;
	 private String RELATED_ACCOUNT;
	 private Date EXEC_DATE ;
	 private int totrow;
	 private BigDecimal totdebet;
	 private BigDecimal totkredit;
	 
	 
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branchCode) {
		branch_code = branchCode;
	}
	public String getBatch_no() {
		return batch_no;
	}
	public void setBatch_no(String batch_no) {
		this.batch_no = batch_no;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getDr_ent_total() {
		return dr_ent_total;
	}
	public void setDr_ent_total(BigDecimal dr_ent_total) {
		this.dr_ent_total = dr_ent_total;
	}
	public BigDecimal getCr_ent_total() {
		return cr_ent_total;
	}
	public void setCr_ent_total(BigDecimal cr_ent_total) {
		this.cr_ent_total = cr_ent_total;
	}
	public Date getLast_oper_dt_stamp() {
		return last_oper_dt_stamp;
	}
	public void setLast_oper_dt_stamp(Date last_oper_dt_stamp) {
		this.last_oper_dt_stamp = last_oper_dt_stamp;
	}
	public String getAuth_stat() {
		return auth_stat;
	}
	public void setAuth_stat(String authStat) {
		auth_stat = authStat;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getTrn_ref_no() {
		return trn_ref_no;
	}
	public void setTrn_ref_no(String trn_ref_no) {
		this.trn_ref_no = trn_ref_no;
	}
	public String getAc_ccy() {
		return ac_ccy;
	}
	public void setAc_ccy(String ac_ccy) {
		this.ac_ccy = ac_ccy;
	}
	public String getAuth_id() {
		return auth_id;
	}
	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}
	public String getTrn_code() {
		return trn_code;
	}
	public void setTrn_code(String trn_code) {
		this.trn_code = trn_code;
	}
	public String getDrcr_ind() {
		return drcr_ind;
	}
	public void setDrcr_ind(String drcr_ind) {
		this.drcr_ind = drcr_ind;
	}
	public Date getTrn_dt() {
		return trn_dt;
	}
	public void setTrn_dt(Date trn_dt) {
		this.trn_dt = trn_dt;
	}
	public String getAc_no() {
		return ac_no;
	}
	public void setAc_no(String ac_no) {
		this.ac_no = ac_no;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getLast_oper_id() {
		return last_oper_id;
	}
	public void setLast_oper_id(String last_oper_id) {
		this.last_oper_id = last_oper_id;
	}
	
	public String getBATCH_MODE() {
		return BATCH_MODE;
	}
	public void setBATCH_MODE(String batch_mode) {
		BATCH_MODE = batch_mode;
	}
	public Date getEXEC_DATE() {
		return EXEC_DATE;
	}
	public void setEXEC_DATE(Date exec_date) {
		EXEC_DATE = exec_date;
	}
	public int getTotrow() {
		return totrow;
	}
	public void setTotrow(int totrow) {
		this.totrow = totrow;
	}
	public BigDecimal getTotdebet() {
		return totdebet;
	}
	public void setTotdebet(BigDecimal totdebet) {
		this.totdebet = totdebet;
	}
	public BigDecimal getTotkredit() {
		return totkredit;
	}
	public void setTotkredit(BigDecimal totkredit) {
		this.totkredit = totkredit;
	}
	public String getRELATED_ACCOUNT() {
		return RELATED_ACCOUNT;
	}
	public void setRELATED_ACCOUNT(String rELATEDACCOUNT) {
		RELATED_ACCOUNT = rELATEDACCOUNT;
	}

	 
	 
}

