package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class RtgsSknMcb {
	private int inc;
	private String bookdt;
	private String init;
	private String activ;
	private String maker_id;
	private String checker_id;
	private String dispatch_ref_no;
	private String network;
	private String contract_ref_no;
	private String exception_queue;
	private BigDecimal txn_amount;
	private String branch_code;
	private String cust_name;
	private String ac_entry_ref_no;
	private String cpty_bankcode;
	private String cpty_ac_no;
	private String cpty_name;
	private String remarks;
	private String cust_ac_no;
	private String trn_code;
	private String trn_desc;
	private String eventdesc;
	
	public int getInc() {
		return inc;
	}
	public void setInc(int inc) {
		this.inc = inc;
	}
	public String getBookdt() {
		return bookdt;
	}
	public void setBookdt(String bookdt) {
		this.bookdt = bookdt;
	}
	public String getInit() {
		return init;
	}
	public void setInit(String init) {
		this.init = init;
	}
	public String getActiv() {
		return activ;
	}
	public void setActiv(String activ) {
		this.activ = activ;
	}
	public String getMaker_id() {
		return maker_id;
	}
	public void setMaker_id(String maker_id) {
		this.maker_id = maker_id;
	}
	public String getChecker_id() {
		return checker_id;
	}
	public void setChecker_id(String checker_id) {
		this.checker_id = checker_id;
	}
	public String getDispatch_ref_no() {
		return dispatch_ref_no;
	}
	public void setDispatch_ref_no(String dispatch_ref_no) {
		this.dispatch_ref_no = dispatch_ref_no;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getContract_ref_no() {
		return contract_ref_no;
	}
	public void setContract_ref_no(String contract_ref_no) {
		this.contract_ref_no = contract_ref_no;
	}
	public String getException_queue() {
		return exception_queue;
	}
	public void setException_queue(String exception_queue) {
		this.exception_queue = exception_queue;
	}
	public BigDecimal getTxn_amount() {
		return txn_amount;
	}
	public void setTxn_amount(BigDecimal txn_amount) {
		this.txn_amount = txn_amount;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getAc_entry_ref_no() {
		return ac_entry_ref_no;
	}
	public void setAc_entry_ref_no(String ac_entry_ref_no) {
		this.ac_entry_ref_no = ac_entry_ref_no;
	}
	public String getCpty_bankcode() {
		return cpty_bankcode;
	}
	public void setCpty_bankcode(String cpty_bankcode) {
		this.cpty_bankcode = cpty_bankcode;
	}
	public String getCpty_ac_no() {
		return cpty_ac_no;
	}
	public void setCpty_ac_no(String cpty_ac_no) {
		this.cpty_ac_no = cpty_ac_no;
	}
	public String getCpty_name() {
		return cpty_name;
	}
	public void setCpty_name(String cpty_name) {
		this.cpty_name = cpty_name;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCust_ac_no() {
		return cust_ac_no;
	}
	public void setCust_ac_no(String cust_ac_no) {
		this.cust_ac_no = cust_ac_no;
	}
	public String getTrn_code() {
		return trn_code;
	}
	public void setTrn_code(String trn_code) {
		this.trn_code = trn_code;
	}
	public String getTrn_desc() {
		return trn_desc;
	}
	public void setTrn_desc(String trn_desc) {
		this.trn_desc = trn_desc;
	}
	public String getEventdesc() {
		return eventdesc;
	}
	public void setEventdesc(String eventdesc) {
		this.eventdesc = eventdesc;
	}
	
	
}
