package com.muamalat.reportmcb.entity;

public class LapTes {
	private String norek;
	private String fastpath;
	private String datalama;
	private String databaru;
	private String modif_ke;
	private String keterangan;
	
	public String getNorek() {
		return norek;
	}
	public void setNorek(String norek) {
		this.norek = norek;
	}
	public String getFastpath() {
		return fastpath;
	}
	public void setFastpath(String fastpath) {
		this.fastpath = fastpath;
	}
	public String getDatalama() {
		return datalama;
	}
	public void setDatalama(String datalama) {
		this.datalama = datalama;
	}
	public String getDatabaru() {
		return databaru;
	}
	public void setDatabaru(String databaru) {
		this.databaru = databaru;
	}
	public String getModif_ke() {
		return modif_ke;
	}
	public void setModif_ke(String modif_ke) {
		this.modif_ke = modif_ke;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
}
