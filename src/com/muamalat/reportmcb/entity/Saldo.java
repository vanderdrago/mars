/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

/**
 *
 * @author Utis
 */
public class Saldo {
    private String branch_code;
    private String account;
    private String ac_name;
    private String acc_ccy;
    private BigDecimal acy_closing_bal;
    private BigDecimal lcy_closing_bal;
    private BigDecimal acy_opening_bal;
    private BigDecimal lcy_opening_bal;
    private BigDecimal ekivalen;
    private BigDecimal saldo_awal;
    
    public BigDecimal getSaldo_awal() {
		return saldo_awal;
	}

	public void setSaldo_awal(BigDecimal saldo_awal) {
		this.saldo_awal = saldo_awal;
	}

	public String getAcc_ccy() {
        return acc_ccy;
    }

    public void setAcc_ccy(String acc_ccy) {
        this.acc_ccy = acc_ccy;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAc_name() {
		return ac_name;
	}

	public void setAc_name(String acName) {
		ac_name = acName;
	}

	public BigDecimal getAcy_closing_bal() {
        return acy_closing_bal;
    }

    public void setAcy_closing_bal(BigDecimal acy_closing_bal) {
        this.acy_closing_bal = acy_closing_bal;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public BigDecimal getLcy_closing_bal() {
        return lcy_closing_bal;
    }

    public void setLcy_closing_bal(BigDecimal lcy_closing_bal) {
    	if(lcy_closing_bal == null){
    		this.lcy_closing_bal = new BigDecimal(0);
    	} else {
    		this.lcy_closing_bal = lcy_closing_bal;
    	}
    }

	public BigDecimal getAcy_opening_bal() {
		return acy_opening_bal;
	}

	public void setAcy_opening_bal(BigDecimal acy_opening_bal) {
		this.acy_opening_bal = acy_opening_bal;
	}

	public BigDecimal getLcy_opening_bal() {
		return lcy_opening_bal;
	}

	public void setLcy_opening_bal(BigDecimal lcy_opening_bal) {
		this.lcy_opening_bal = lcy_opening_bal;
	}

	public BigDecimal getEkivalen() {
		return ekivalen;
	}

	public void setEkivalen(BigDecimal ekivalen) {
		this.ekivalen = ekivalen;
	}
	
}

