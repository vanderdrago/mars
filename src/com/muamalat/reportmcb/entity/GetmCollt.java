package com.muamalat.reportmcb.entity;

public class GetmCollt {
	
	private String id;
	
	private String liab_id;
	
	private String collateral_code;
	
	private String collateral_description;
	
	private String category_id;
	
	private String collateral_currency;
	
	private String collateral_value;
	
	private String limit_contribution;
	
	private String start_date;
	
	private String end_date;
	
	private String available_amount;
	
	private String haircut;
	
	private String lendable_margin;
	
	private String liab_branch;
	
	private String branch_code;
	
	private String liab_no;
	
	private String liab_name;
	
	private String pool_id;
	
	private String status_rcd;
	
	private String status_oto;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLiab_id() {
		return liab_id;
	}

	public void setLiab_id(String liabId) {
		liab_id = liabId;
	}

	public String getCollateral_code() {
		return collateral_code;
	}

	public void setCollateral_code(String collateralCode) {
		collateral_code = collateralCode;
	}

	public String getCollateral_description() {
		return collateral_description;
	}

	public void setCollateral_description(String collateralDescription) {
		collateral_description = collateralDescription;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String categoryId) {
		category_id = categoryId;
	}

	public String getCollateral_currency() {
		return collateral_currency;
	}

	public void setCollateral_currency(String collateralCurrency) {
		collateral_currency = collateralCurrency;
	}

	public String getCollateral_value() {
		return collateral_value;
	}

	public void setCollateral_value(String collateralValue) {
		collateral_value = collateralValue;
	}

	public String getLimit_contribution() {
		return limit_contribution;
	}

	public void setLimit_contribution(String limitContribution) {
		limit_contribution = limitContribution;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String startDate) {
		start_date = startDate;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String endDate) {
		end_date = endDate;
	}

	public String getAvailable_amount() {
		return available_amount;
	}

	public void setAvailable_amount(String availableAmount) {
		available_amount = availableAmount;
	}

	public String getHaircut() {
		return haircut;
	}

	public void setHaircut(String haircut) {
		this.haircut = haircut;
	}

	public String getLendable_margin() {
		return lendable_margin;
	}

	public void setLendable_margin(String lendableMargin) {
		lendable_margin = lendableMargin;
	}

	public String getLiab_branch() {
		return liab_branch;
	}

	public void setLiab_branch(String liabBranch) {
		liab_branch = liabBranch;
	}

	public String getBranch_code() {
		return branch_code;
	}

	public void setBranch_code(String branchCode) {
		branch_code = branchCode;
	}

	public String getLiab_no() {
		return liab_no;
	}

	public void setLiab_no(String liabNo) {
		liab_no = liabNo;
	}

	public String getPool_id() {
		return pool_id;
	}

	public void setPool_id(String poolId) {
		pool_id = poolId;
	}

	public String getStatus_rcd() {
		return status_rcd;
	}

	public void setStatus_rcd(String statusRcd) {
		status_rcd = statusRcd;
	}

	public String getStatus_oto() {
		return status_oto;
	}

	public void setStatus_oto(String statusOto) {
		status_oto = statusOto;
	}

	public String getLiab_name() {
		return liab_name;
	}

	public void setLiab_name(String liabName) {
		liab_name = liabName;
	}
	
	
}
