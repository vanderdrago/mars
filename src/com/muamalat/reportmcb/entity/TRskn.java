package com.muamalat.reportmcb.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


/**
 * The persistent class for the T_RTIFTSOUT database table.
 * 
 */
@Entity
@Table(name="master")
public class TRskn implements Serializable {
	private static final long serialVersionUID = 1L;
	 
	@Column(name="Od_id") 
	private String od_id;
	
	@Column(name="Od_ob_id") 
	private String od_ob_id;
	
	@Column(name="Od_seq") 
	private String od_seq;
	
	@Column(name="Od_br_id") 
	private String od_br_id;
	
	@Column(name="Od_ob_num") 
	private String od_ob_num;
	
	@Column(name="Od_ref_no") 
	private String od_ref_no;
	
	@Column(name="Od_op_name") 
	private String od_op_name;
	
	@Column(name="Od_cu_name") 
	private String od_cu_name;
	
	@Column(name="Od_accno")
	private String od_accno;
	
	@Column(name="Od_desc") 
	private String od_desc;
	
	@Column(name="Od_amount") 
//	private String od_amount;
	private BigDecimal od_amount;
	
	@Column(name="Od_bankno") 
	private String od_bankno;
	
	@Column(name="Od_bank_type")
	private String od_bank_type;
	
	@Column(name="Od_tc") 
	private String od_tc;
	
	@Column(name="Od_cu_resident") 
	private String od_cu_resident;
	
	@Column(name="Od_cu_citizen") 
	 private String od_cu_citizen;
	
	@Column(name="Od_prov_code") 
	private String od_prov_code;
	
	@Column(name="Od_city_code") 
	private String od_city_code;
	
	@Column(name="Od_chno") 
	private String od_chno;
	
	@Column(name="Od_op_ac_num") 
	 private String od_op_ac_num;
	
	@Column(name="Od_status_desc") 
	private String od_status_desc;
	
	@Column(name="Od_status1") 
	 private String od_status1;
	
	@Column(name="Od_status2") 
	private String od_status2;
	
	@Column(name="Od_status3") 
	private String od_status3;
	
	@Column(name="Od_flag1") 
	private String od_flag1;
	
	@Column(name="Od_flag2") 
	private String od_flag2;
	
	@Column(name="Od_flag3") 
	private String od_flag3;
	
	@Column(name="Od_ect_status1") 
	private String od_ect_status1;
	
	@Column(name="Od_ect_status2")
	private String od_ect_status2;
	
	@Column(name="Od_ect_status3") 
	private String od_ect_status3;
	
	@Column(name="Od_ect_sor") 
	private String od_ect_sor;
	
	@Column(name="Od_corrected") 
	private String od_corrected;
	
	@Column(name="Od_cetak") 
	private String od_cetak;
	
	@Column(name="Od_prn_times")
	private String od_prn_times;
	
	@Column(name="Od_legacy_no") 
	private String od_legacy_no;
	
	@Column(name="Od_bank_desc") 
	private String od_bank_desc;
	
	@Column(name="Od_chtext") 
	private String od_chtext;
	 
	public TRskn() {
	 }

	public String getOd_id() {
		return od_id;
	}

	public void setOd_id(String odId) {
		od_id = odId;
	}

	public String getOd_ob_id() {
		return od_ob_id;
	}

	public void setOd_ob_id(String odObId) {
		od_ob_id = odObId;
	}

	public String getOd_seq() {
		return od_seq;
	}

	public void setOd_seq(String odSeq) {
		od_seq = odSeq;
	}

	public String getOd_br_id() {
		return od_br_id;
	}

	public void setOd_br_id(String odBrId) {
		od_br_id = odBrId;
	}

	public String getOd_ob_num() {
		return od_ob_num;
	}

	public void setOd_ob_num(String odObNum) {
		od_ob_num = odObNum;
	}

	public String getOd_ref_no() {
		return od_ref_no;
	}

	public void setOd_ref_no(String odRefNo) {
		od_ref_no = odRefNo;
	}

	public String getOd_op_name() {
		return od_op_name;
	}

	public void setOd_op_name(String odOpName) {
		od_op_name = odOpName;
	}

	public String getOd_cu_name() {
		return od_cu_name;
	}

	public void setOd_cu_name(String odCuName) {
		od_cu_name = odCuName;
	}

	public String getOd_accno() {
		return od_accno;
	}

	public void setOd_accno(String odAccno) {
		od_accno = odAccno;
	}

	public String getOd_desc() {
		return od_desc;
	}

	public void setOd_desc(String odDesc) {
		od_desc = odDesc;
	}

	public BigDecimal getOd_amount() {
		return od_amount;
	}

	public void setOd_amount(BigDecimal odAmount) {
		od_amount = odAmount;
	}

	public String getOd_bankno() {
		return od_bankno;
	}

	public void setOd_bankno(String odBankno) {
		od_bankno = odBankno;
	}

	public String getOd_bank_type() {
		return od_bank_type;
	}

	public void setOd_bank_type(String odBankType) {
		od_bank_type = odBankType;
	}

	public String getOd_tc() {
		return od_tc;
	}

	public void setOd_tc(String odTc) {
		od_tc = odTc;
	}

	public String getOd_cu_resident() {
		return od_cu_resident;
	}

	public void setOd_cu_resident(String odCuResident) {
		od_cu_resident = odCuResident;
	}

	public String getOd_cu_citizen() {
		return od_cu_citizen;
	}

	public void setOd_cu_citizen(String odCuCitizen) {
		od_cu_citizen = odCuCitizen;
	}

	public String getOd_prov_code() {
		return od_prov_code;
	}

	public void setOd_prov_code(String odProvCode) {
		od_prov_code = odProvCode;
	}

	public String getOd_city_code() {
		return od_city_code;
	}

	public void setOd_city_code(String odCityCode) {
		od_city_code = odCityCode;
	}

	public String getOd_chno() {
		return od_chno;
	}

	public void setOd_chno(String odChno) {
		od_chno = odChno;
	}

	public String getOd_op_ac_num() {
		return od_op_ac_num;
	}

	public void setOd_op_ac_num(String odOpAcNum) {
		od_op_ac_num = odOpAcNum;
	}

	public String getOd_status_desc() {
		return od_status_desc;
	}

	public void setOd_status_desc(String odStatusDesc) {
		od_status_desc = odStatusDesc;
	}

	public String getOd_status1() {
		return od_status1;
	}

	public void setOd_status1(String odStatus1) {
		od_status1 = odStatus1;
	}

	public String getOd_status2() {
		return od_status2;
	}

	public void setOd_status2(String odStatus2) {
		od_status2 = odStatus2;
	}

	public String getOd_status3() {
		return od_status3;
	}

	public void setOd_status3(String odStatus3) {
		od_status3 = odStatus3;
	}

	public String getOd_flag1() {
		return od_flag1;
	}

	public void setOd_flag1(String odFlag1) {
		od_flag1 = odFlag1;
	}

	public String getOd_flag2() {
		return od_flag2;
	}

	public void setOd_flag2(String odFlag2) {
		od_flag2 = odFlag2;
	}

	public String getOd_flag3() {
		return od_flag3;
	}

	public void setOd_flag3(String odFlag3) {
		od_flag3 = odFlag3;
	}

	public String getOd_ect_status1() {
		return od_ect_status1;
	}

	public void setOd_ect_status1(String odEctStatus1) {
		od_ect_status1 = odEctStatus1;
	}

	public String getOd_ect_status2() {
		return od_ect_status2;
	}

	public void setOd_ect_status2(String odEctStatus2) {
		od_ect_status2 = odEctStatus2;
	}

	public String getOd_ect_status3() {
		return od_ect_status3;
	}

	public void setOd_ect_status3(String odEctStatus3) {
		od_ect_status3 = odEctStatus3;
	}

	public String getOd_ect_sor() {
		return od_ect_sor;
	}

	public void setOd_ect_sor(String odEctSor) {
		od_ect_sor = odEctSor;
	}

	public String getOd_corrected() {
		return od_corrected;
	}

	public void setOd_corrected(String odCorrected) {
		od_corrected = odCorrected;
	}

	public String getOd_cetak() {
		return od_cetak;
	}

	public void setOd_cetak(String odCetak) {
		od_cetak = odCetak;
	}

	public String getOd_prn_times() {
		return od_prn_times;
	}

	public void setOd_prn_times(String odPrnTimes) {
		od_prn_times = odPrnTimes;
	}

	public String getOd_legacy_no() {
		return od_legacy_no;
	}

	public void setOd_legacy_no(String odLegacyNo) {
		od_legacy_no = odLegacyNo;
	}

	public String getOd_bank_desc() {
		return od_bank_desc;
	}

	public void setOd_bank_desc(String odBankDesc) {
		od_bank_desc = odBankDesc;
	}

	public String getOd_chtext() {
		return od_chtext;
	}

	public void setOd_chtext(String odChtext) {
		od_chtext = odChtext;
	}
	
}