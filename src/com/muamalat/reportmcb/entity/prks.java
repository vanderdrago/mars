package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class prks {

	private String acc;
	private String periode;
	private BigDecimal rbh;
	private BigDecimal pbh;
	private BigDecimal os;
	private BigDecimal fcu;
	private int rate;
	private String brn;
	private String status;
	private String AccClass;
	private String custName;
	private String accBaghas;
	private String startDt;
	private String expiryDt;
	private BigDecimal plafond;
	private String bulanTagihan;
	private String bulanBayar;
	private BigDecimal saldoRata;
	
	public BigDecimal getSaldoRata() {
		return saldoRata;
	}
	public void setSaldoRata(BigDecimal saldoRata) {
		this.saldoRata = saldoRata;
	}
	public String getBulanTagihan() {
		return bulanTagihan;
	}
	public void setBulanTagihan(String bulanTagihan) {
		this.bulanTagihan = bulanTagihan;
	}
	public String getBulanBayar() {
		return bulanBayar;
	}
	public void setBulanBayar(String bulanBayar) {
		this.bulanBayar = bulanBayar;
	}
	public BigDecimal getPlafond() {
		return plafond;
	}
	public void setPlafond(BigDecimal plafond) {
		this.plafond = plafond;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getExpiryDt() {
		return expiryDt;
	}
	public void setExpiryDt(String expiryDt) {
		this.expiryDt = expiryDt;
	}
	public String getAccBaghas() {
		return accBaghas;
	}
	public void setAccBaghas(String accBaghas) {
		this.accBaghas = accBaghas;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getAccClass() {
		return AccClass;
	}
	public void setAccClass(String accClass) {
		AccClass = accClass;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBrn() {
		return brn;
	}
	public void setBrn(String brn) {
		this.brn = brn;
	}
	public String getAcc() {
		return acc;
	}
	public void setAcc(String acc) {
		this.acc = acc;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public Number getRbh() {
		return rbh;
	}
	public void setRbh(BigDecimal rbh) {
		this.rbh = rbh;
	}
	public Number getPbh() {
		return pbh;
	}
	public void setPbh(BigDecimal pbh) {
		this.pbh = pbh;
	}
	public BigDecimal getOs() {
		return os;
	}
	public void setOs(BigDecimal os) {
		this.os = os;
	}
	public BigDecimal getFcu() {
		return fcu;
	}
	public void setFcu(BigDecimal fcu) {
		this.fcu = fcu;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	
	
}
