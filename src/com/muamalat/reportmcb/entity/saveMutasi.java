package com.muamalat.reportmcb.entity;

import java.math.BigDecimal;

public class saveMutasi {
	private String userId;
	private String branch;
	private String tanggal;
	private String jam;
	private String trnCode;
	private String accNo;
	private String noWarkat;
	private String accDesc;
	private BigDecimal debet;
	private BigDecimal kredit;
	private String userApproval;
	private String periksa;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getJam() {
		return jam;
	}
	public void setJam(String jam) {
		this.jam = jam;
	}
	public String getTrnCode() {
		return trnCode;
	}
	public void setTrnCode(String trnCode) {
		this.trnCode = trnCode;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getNoWarkat() {
		return noWarkat;
	}
	public void setNoWarkat(String noWarkat) {
		this.noWarkat = noWarkat;
	}
	public String getAccDesc() {
		return accDesc;
	}
	public void setAccDesc(String accDesc) {
		this.accDesc = accDesc;
	}
	public BigDecimal getDebet() {
		return debet;
	}
	public void setDebet(BigDecimal debet) {
		this.debet = debet;
	}
	public BigDecimal getKredit() {
		return kredit;
	}
	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}
	public String getUserApproval() {
		return userApproval;
	}
	public void setUserApproval(String userApproval) {
		this.userApproval = userApproval;
	}
	public String getPeriksa() {
		return periksa;
	}
	public void setPeriksa(String periksa) {
		this.periksa = periksa;
	}
	
	
}
