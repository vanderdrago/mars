package com.muamalat.reportmcb.entity;

import java.util.ArrayList;

public class RoleMenu {
	private String HeaderName;
	private ArrayList subSubMenu;
	
	public ArrayList getSubSubMenu() {
		return subSubMenu;
	}
	public void setSubSubMenu(ArrayList subSubMenu) {
		this.subSubMenu = subSubMenu;
	}
	public String getHeaderName() {
		return HeaderName;
	}
	public void setHeaderName(String headerName) {
		HeaderName = headerName;
	}
	
}
