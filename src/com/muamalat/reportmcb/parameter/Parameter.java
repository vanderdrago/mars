package com.muamalat.reportmcb.parameter;

public class Parameter {
	public static String W = "Menunggu Otorisasi";
	public static String H = "Hold";
	public static String O = "Active";
	public static String C = "Closed";
	public static String R = "Reject";
	
	public static String r003 = "R003";

//	public static String READFILE = "C://konversirekening/";
	public static String READFILE = "/data/konversirekening/";
//	public static String READFILE_KARTU = "/data/konversikartu/upload";
//	public static String DOWNLOADFILE_KARTU = "/data/konversikartu/download";
//	public static String READFILE_KARTU = "D://konversikartu/upload/";
//	public static String DOWNLOADFILE_KARTU = "D://konversikartu/download/";
	

	public static String getMonth(int n){
		String m = "";
		switch (n) {
		case 1:
			m = "Januari";
			break;
		case 2:
			m = "Februari";
			break;
		case 3:
			m = "Maret";
			break;
		case 4:
			m = "April";
			break;
		case 5:
			m = "Mei";
			break;
		case 6:
			m = "Juni";
			break;
		case 7:
			m = "Juli";
			break;
		case 8:
			m = "Agustus";
			break;
		case 9:
			m = "September";
			break;
		case 10:
			m = "Oktober";
			break;
		case 11:
			m = "November";
			break;
		case 12:
			m = "Desember";
			break;
		default:
			m = "";
			break;
		}
		return m;
	}
	
		
	public static String getKonversiKartuPathUpload(String statusServer){
		if (statusServer.equals("dev")){
			return "D://konversikartu/upload/";
		}else if (statusServer.equals("uat")){
			return "/data/konversikartu/upload";
		}else if (statusServer.equals("prod")){
			return "/data/konversikartu/upload";			
		}	
		return null;
	}
	public static String getKonversiKartuPathDownload(String statusServer){
		if (statusServer.equals("dev")){
			return "D://konversikartu/download/";
		}else if (statusServer.equals("uat")){
			return "/data/konversikartu/download";
		}else if (statusServer.equals("prod")){
			return "/data/konversikartu/download";			
		}	
		return null;
	}
}
