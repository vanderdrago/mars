package com.muamalat.reportmcb.parameter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class tesConnection {
	
	//conn oracle
//	private static String url = "jdbc:oracle:thin:@10.81.100.12:1521/bmiuatcr";
//	private static String driverName = "oracle.jdbc.driver.OracleDriver";
//	private static String username = "FCC114";   
//    private static String password = "fcc1141";
//    private static Connection con;
    
    //conn postgres
//    private static String url = "jdbc:postgresql://10.55.109.50:5432/DBMarsRep";
//	private static String driverName = "org.postgresql.Driver";
//	private static String username = "postgres";   
//    private static String password = "postgres";
//    private static Connection con;
//
//    public static Connection getConnection() {
//    	try {
//            Class.forName(driverName);
//            try {
//                con = DriverManager.getConnection(url, username, password);
//            } catch (SQLException ex) {
//                // log an exception. fro example:
//                System.out.println("Failed to create the database connection."); 
//            }
//        } catch (ClassNotFoundException ex) {
//            // log an exception. for example:
//            System.out.println("Driver not found."); 
//        }
//        return con;
//    
//		
//	}
	
	public static Connection getOracleConnection() throws Exception {
		String url = "jdbc:oracle:thin:@10.81.100.12:1521/bmiuatcr";
		String driverName = "oracle.jdbc.driver.OracleDriver";
		String username = "FCC114";   
	    String password = "fcc1141";
	    Class.forName(driverName);
	    Connection conn = DriverManager.getConnection(url, username, password);
	    return conn;
	}
	
	public static Connection getPostgresConnection() throws Exception {
		String url = "jdbc:postgresql://10.55.109.50:5432/DBMarsRep";
		String driverName = "org.postgresql.Driver";
		String username = "postgres";   
	    String password = "postgres";
	    Class.forName(driverName);
	    Connection conn = DriverManager.getConnection(url, username, password);
	    return conn;
	}
	
}
