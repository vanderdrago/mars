package com.muamalat.reportmcb.parameter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.muamalat.reportmcb.entity.SttmCustAccount;
import com.muamalat.reportmcb.entity.salamMuamalat;
import com.muamalat.reportmcb.entity.saldoHold;
import com.muamalat.singleton.DatasourceEntry;


public class sql {
	private static Logger log = Logger.getLogger(sql.class);
	public void closeConnDb (Connection conn, PreparedStatement stat, ResultSet rs){
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				log.error("closeConnDb 1 : " + e.getMessage()); 
			}
		}
		if (rs != null){
			try {
				rs.close();
				rs = null;
			} catch (Exception e) {
				log.error("closeConnDb 2 : " + e.getMessage()); 
			}
		}
		if (stat != null){
			try {
				stat.close();
				stat = null;
			} catch (Exception e) {
				log.error("closeConnDb 3 : " + e.getMessage()); 
			}
		}
	}
	
	public List getTes (String norek){
		int total = 0;
		List bb = new ArrayList();
		saldoHold saldo = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String tes = "";
		
		try {
//			conn = DatasourceEntry.getInstance().getMcb2DS().getConnection();
			conn = tesConnection.getOracleConnection();
			sql = " SELECT ACCOUNT,AMOUNT,TO_CHAR (EXPIRY_DATE, 'dd-mm-yyyy') AS EXPIRY_DATE,REMARKS, "
					+ " TO_CHAR (EFFECTIVE_DATE, 'dd-mm-yyyy') AS EFFECTIVE_DATE, MOD_NO, RECORD_STAT"
					+ " FROM CATM_AMOUNT_BLOCKS WHERE ACCOUNT = ? AND RECORD_STAT = 'O' ";
			System.out.println("sql:" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, norek);
			rs = stat.executeQuery();
			while (rs.next()){
//				total = rs.getInt("HASIL");
//				System.out.println("total:" + total);
//				System.out.println("5");
				saldo = new saldoHold();
				saldo.setAcc(rs.getString("ACCOUNT"));
				System.out.println("ACCOUNT :" + rs.getString("ACCOUNT"));
//				if (rs.getBigDecimal("AMOUNT") == null){
//					bb.setAmount(new BigDecimal(0));
//				} else {
//					bb.setAmount(rs.getBigDecimal("AMOUNT"));
//				}
//				System.out.println("amount :" + rs.getBigDecimal("AMOUNT"));
//				bb.setExpiryDate(rs.getString("EXPIRY_DATE"));
//				System.out.println("expiry:" + rs.getString("EXPIRY_DATE"));
//				bb.setRemarks(rs.getString("REMARKS"));
//				System.out.println("ket:" + rs.getString("REMARKS"));
//				bb.setEffectiveDate(rs.getString("EFFECTIVE_DATE"));
//				System.out.println("efektif:" + rs.getString("EFFECTIVE_DATE"));
////				bb.setModNo(rs.getString("MOD_NO"));
				bb.add(saldo);
			}
		} catch (Exception e) {
			log.error("getSalamMuamalat :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return bb;
	}
	
	public List getTes1 (String norek){
		System.out.println("norek sql:" + norek);
		List lbb = new ArrayList();
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		String tes = "";
		String tes1 = "";
		try {
			conn = tesConnection.getPostgresConnection();
			sql = " SELECT * FROM HISTORY_REKENING WHERE CUST_AC_NO = ?";
			System.out.println("sql:" + sql);
			stat = conn.prepareStatement(sql);
			stat.setString(1, norek);
			rs = stat.executeQuery();
			while (rs.next()){
				tes = rs.getString("CUST_AC_NO");
				tes1 = rs.getString("BRANCH_CODE");
				System.out.println(tes + tes1);
				
//				System.out.println("expiry:" + rs.getString("EXPIRY_DATE"));
//				bb.setRemarks(rs.getString("REMARKS"));
//				System.out.println("ket:" + rs.getString("REMARKS"));
//				bb.setEffectiveDate(rs.getString("EFFECTIVE_DATE"));
//				System.out.println("efektif:" + rs.getString("EFFECTIVE_DATE"));
////				bb.setModNo(rs.getString("MOD_NO"));
			}
		} catch (Exception e) {
			log.error("getSalamMuamalat :" + e.getMessage());
		} finally {
			closeConnDb(conn, stat, rs);
		}
		return lbb;
	}
	
	
}
