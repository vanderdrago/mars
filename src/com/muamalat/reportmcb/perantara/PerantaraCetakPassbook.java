package com.muamalat.reportmcb.perantara;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jboss.logging.Logger;

import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.CetakPassbook;
import com.muamalat.reportmcb.entity.LmtTrnsPssbk;
import com.muamalat.reportmcb.function.CetakPassbookFunction;
import com.muamalat.reportmcb.function.DataFunction;

public class PerantaraCetakPassbook {
	private CetakPassbookFunction cpf = new CetakPassbookFunction();
	private Logger log = Logger.getLogger(PerantaraCetakPassbook.class);
	
	public List getListTrans(Acc_line acc, BigDecimal saldoawal, String prnt, LmtTrnsPssbk lmt_trns){
		List lbb = null;
		CetakPassbook cp = null;
		DecimalFormat myFormatter = new DecimalFormat("###,##0.00");
		
		Acc_line aclne_hist = cpf.getAcc_line(acc.getAccount_no(), "print");
		
		int start_line = acc.getPrev_line_no() + 1;
		if(start_line > lmt_trns.getLimit_per_page()){
			start_line = 1;
		}
		if (start_line <= lmt_trns.getLimit_per_page()){
			lbb = new ArrayList();			
			List trans = cpf.getListTrns(acc, aclne_hist, saldoawal, prnt);	
			if (trans.size() > 0){
				CetakPassbook latesttrns = null;
				int v = 1;
				int startnotrans = 1;
				Calendar cal = Calendar.getInstance();
				DateFormat formatter = null;
		        Date convertedDate = null;

		        try {
		            int currentDay = cal.get(Calendar.DATE);
		            int currentMonth = cal.get(Calendar.MONTH) + 1;
		            int currentYear = cal.get(Calendar.YEAR);
		            DataFunction df = new DataFunction();
		            String yyyyMMdd = "" + currentYear + "" + df.converttostr(currentMonth) + "" + df.converttostr(currentDay);
		            formatter = new SimpleDateFormat("yyyyMMdd");
		            convertedDate = (Date) formatter.parse(yyyyMMdd);
		        } catch (Exception e) {
		        	log.info("Error : " + e.getMessage());
		        }
//		        int tday = (int)((convertedDate.getTime() - cpf.getMinDate().getTime()) / (1000 * 60 * 60 * 24));
		        int tday = (int)((convertedDate.getTime() - acc.getPrint_date().getTime()) / (1000 * 60 * 60 * 24));
		        BigDecimal tot_mutasi = new BigDecimal(0);
				for (Iterator<CetakPassbook> i = trans.iterator(); i.hasNext();) {
					CetakPassbook pbook = i.next();
					if(pbook.getNo() >= startnotrans){						
						if (pbook.getNo() <= cpf.getStart_no_compress()){
							if ("C".equals(pbook.getDrcr_ind())){
								tot_mutasi = tot_mutasi.add(pbook.getMutasi());
							} else {
								tot_mutasi = tot_mutasi.subtract(pbook.getMutasi());
							}
						} 
						if (tday <= lmt_trns.getLimit_max_time() && trans.size() < lmt_trns.getLimit_trans()){
							cp = new CetakPassbook();
							cp.setNo(v++);
							cp.setNo_s(cp.getNo() + "");
							cp.setTrn_dt(pbook.getTrn_dt());
							cp.setTrn_dt_s(pbook.getTrn_dt_s());
							cp.setTrn_code(pbook.getTrn_code());
							cp.setDrcr_ind(pbook.getDrcr_ind());
							cp.setMutasi_s(myFormatter.format(pbook.getMutasi()));
							cp.setDetail(pbook.getDetail());
							cp.setMutasi_s(myFormatter.format(pbook.getMutasi()));
							cp.setSaldo_s(myFormatter.format(pbook.getSaldo()));
							cp.setAuth_id(pbook.getAuth_id());
							cp.setAc_entry_sr_no(pbook.getAc_entry_sr_no());
							lbb.add(cp);
							latesttrns = cp;
						} else {
							if (pbook.getNo() >= cpf.getStart_no_compress()){
								if (pbook.getNo() == cpf.getStart_no_compress()){
									cp = new CetakPassbook();
									cp.setNo(v++);
									cp.setNo_s("&nbsp;");
									cp.setTrn_dt(pbook.getTrn_dt());
									cp.setTrn_dt_s(pbook.getTrn_dt_s());
									cp.setTrn_code(" ");
									int r = tot_mutasi.compareTo(new BigDecimal(0));
									if(r == -1){
										cp.setDrcr_ind("D");								
										cp.setMutasi_s(myFormatter.format(tot_mutasi.multiply(new BigDecimal(-1))));
										
									} else {
										cp.setDrcr_ind("C");								
										cp.setMutasi_s(myFormatter.format(tot_mutasi));
									}
									
									cp.setDetail("Nett Compression (total credit � total debet)");
									cp.setSaldo_s(myFormatter.format(pbook.getSaldo()));
									cp.setAc_entry_sr_no(pbook.getAc_entry_sr_no());
									cp.setAuth_id("SYSTEM");
									lbb.add(cp);
									latesttrns = cp;
								} else if (pbook.getNo() > cpf.getStart_no_compress()) {
									cp = new CetakPassbook();
									cp.setNo(v++);
									cp.setNo_s(cp.getNo() + "");
									cp.setTrn_dt(pbook.getTrn_dt());
									cp.setTrn_dt_s(pbook.getTrn_dt_s());
									cp.setTrn_code(pbook.getTrn_code());
									cp.setDrcr_ind(pbook.getDrcr_ind());
									cp.setMutasi_s(myFormatter.format(pbook.getMutasi()));
									cp.setDetail(pbook.getDetail());
									cp.setMutasi_s(myFormatter.format(pbook.getMutasi()));
									cp.setSaldo_s(myFormatter.format(pbook.getSaldo()));
									cp.setAuth_id(pbook.getAuth_id());
									cp.setAc_entry_sr_no(pbook.getAc_entry_sr_no());
									lbb.add(cp);
									latesttrns = cp;
								}
							}
							/*
							if (tday > lmt_trns.getLimit_max_time() || trans.size() >= lmt_trns.getLimit_trans()){
								if (trans.size() == cpf.getNomor2()){
									if (pbook.getNo() == trans.size()){
										System.out.println("masuk 0");
										cp = new CetakPassbook();
										cp.setNo(v++);
										cp.setNo_s("&nbsp;");
										cp.setTrn_dt(pbook.getTrn_dt());
										cp.setTrn_dt_s(pbook.getTrn_dt_s());
										cp.setTrn_code(" ");
										int r = tot_mutasi.compareTo(new BigDecimal(0));
										if(r == -1){
											cp.setDrcr_ind("D");								
											cp.setMutasi_s(myFormatter.format(tot_mutasi.multiply(new BigDecimal(-1))));
											
										} else {
											cp.setDrcr_ind("C");								
											cp.setMutasi_s(myFormatter.format(tot_mutasi));
										}
										
										cp.setDetail("Nett Compression (total credit � total debet)");
										cp.setSaldo_s(myFormatter.format(pbook.getSaldo()));
										cp.setAc_entry_sr_no(pbook.getAc_entry_sr_no());
										cp.setAuth_id("SYSTEM");
										lbb.add(cp);
										latesttrns = cp;	
									}
								} else {
									if (pbook.getNo() == cpf.getStart_no_compress()){
										System.out.println("masuk 1");
										cp = new CetakPassbook();
										cp.setNo(v++);
										cp.setNo_s("&nbsp;");
										cp.setTrn_dt(pbook.getTrn_dt());
										cp.setTrn_dt_s(pbook.getTrn_dt_s());
										cp.setTrn_code(" ");
										int r = tot_mutasi.compareTo(new BigDecimal(0));
										if(r == -1){
											cp.setDrcr_ind("D");								
											cp.setMutasi_s(myFormatter.format(tot_mutasi.multiply(new BigDecimal(-1))));
											
										} else {
											cp.setDrcr_ind("C");								
											cp.setMutasi_s(myFormatter.format(tot_mutasi));
										}
										
										cp.setDetail("Nett Compression (total credit � total debet)");
										cp.setSaldo_s(myFormatter.format(pbook.getSaldo()));
										cp.setAc_entry_sr_no(pbook.getAc_entry_sr_no());
										cp.setAuth_id("SYSTEM");
										lbb.add(cp);
										latesttrns = cp;
									} else if (pbook.getNo() > cpf.getStart_no_compress()) {
										System.out.println("masuk 2");
										cp = new CetakPassbook();
										cp.setNo(v++);
										cp.setNo_s(cp.getNo() + "");
										cp.setTrn_dt(pbook.getTrn_dt());
										cp.setTrn_dt_s(pbook.getTrn_dt_s());
										cp.setTrn_code(pbook.getTrn_code());
										cp.setDrcr_ind(pbook.getDrcr_ind());
										cp.setMutasi_s(myFormatter.format(pbook.getMutasi()));
										cp.setDetail(pbook.getDetail());
										cp.setMutasi_s(myFormatter.format(pbook.getMutasi()));
										cp.setSaldo_s(myFormatter.format(pbook.getSaldo()));
										cp.setAuth_id(pbook.getAuth_id());
										cp.setAc_entry_sr_no(pbook.getAc_entry_sr_no());
										lbb.add(cp);
										latesttrns = cp;
									}
								}
							} 
							*/
						} 
					} 
				}
			} else {
				lbb = null;
			}
		}
		return lbb;
	}
	
	public CetakPassbook getCPBKosong(int no){
		CetakPassbook cp = new CetakPassbook();
		cp.setNo(no);
		cp.setTrn_dt_s("");
		cp.setTrn_code("");
		cp.setDrcr_ind("");
		cp.setMutasi_s("");
		cp.setDetail("");
		cp.setMutasi_s("");
		cp.setSaldo_s("");
		cp.setAuth_id("");
		return cp;
	}
}
