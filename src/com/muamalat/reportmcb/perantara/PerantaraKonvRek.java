package com.muamalat.reportmcb.perantara;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.muamalat.reportmcb.entity.KonversiRekening;
import com.muamalat.reportmcb.entity.Reklist;
import com.muamalat.reportmcb.function.AccountFunction;

public class PerantaraKonvRek {
	
	public List ListKonvRek(List arrData){
		List lsKonvRek = new ArrayList();
		AccountFunction af = new AccountFunction();
		KonversiRekening krnew = null;
		for (Iterator<KonversiRekening> i = arrData.iterator(); i.hasNext();) {
			KonversiRekening kr = i.next();
			krnew = new KonversiRekening();
			krnew.setAlt_nm_rekening(kr.getAlt_nm_rekening());
			Reklist rek = af.getAccMCB(kr.getAlt_rekening().trim());
			if (rek != null){
				krnew.setAlt_rekening(rek.getAltnomrek());
				krnew.setRekening(rek.getNomrek());
				krnew.setNmrekening(rek.getRekname());
				if("1".equals(rek.getReksts().trim())){
					krnew.setStatus("Active account");  					
				} else if("3".equals(rek.getReksts().trim())){
					krnew.setStatus("Closed Account");
				} else if("2".equals(rek.getReksts().trim())){
					krnew.setStatus("Invalid Account");
				} else {
					krnew.setStatus("Account_not_found");
				}				
				krnew.setSaldo(af.getSaldoRek(rek.getNomrek()));		
			} else {
				krnew.setAlt_rekening(kr.getAlt_rekening());
				krnew.setStatus("Account_not_found");				
			}

			lsKonvRek.add(krnew);
		}
		return lsKonvRek;
	}
}
