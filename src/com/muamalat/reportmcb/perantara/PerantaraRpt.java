package com.muamalat.reportmcb.perantara;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jpos.tpl.RowMap;

import com.muamalat.reportmcb.entity.Acc_line;
import com.muamalat.reportmcb.entity.HistTransRek;
import com.muamalat.reportmcb.entity.JurnalTodayDetail;
import com.muamalat.reportmcb.entity.KonversiRekening;
import com.muamalat.reportmcb.entity.LaporanSPVTeller;
import com.muamalat.reportmcb.entity.LaporanSpvCs;
import com.muamalat.reportmcb.entity.SknIn;
import com.muamalat.reportmcb.entity.pinpad;
import com.muamalat.reportmcb.entity.quickCif;

public class PerantaraRpt {
	public ArrayList moveListToHashMap(List lhist) {
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        int inc = 1;
        for (Iterator<HistTransRek> i = lhist.iterator(); i.hasNext();) {
        	HistTransRek hist = i.next();
            rowMap = new HashMap();
            rowMap.put("account", hist.getAccount());
            rowMap.put("id", inc++);
            rowMap.put("noref", hist.getTrn_ref_no());
            rowMap.put("trn_dt", hist.getTrn_dt());
            rowMap.put("value_dt", hist.getVal_dt());
            rowMap.put("trn_cd", hist.getTrn_code());
            rowMap.put("desc", hist.getDesc());
            rowMap.put("no_warkat", hist.getInstrument_code());
            rowMap.put("drcr", hist.getDrcr());
            rowMap.put("nominal", hist.getNominal());
            rowMap.put("saldo", hist.getSaldo());
            detail.add(rowMap);
        }
        return detail;
    }
	
	public ArrayList moveListToHashMapCif (List lph) {
		HashMap rowMap = null;
		ArrayList detail = new ArrayList();
		int inc = 1;
		for (Iterator<quickCif> i = lph.iterator(); i.hasNext();) {
			quickCif hist = i.next();
			rowMap = new HashMap();
//			rowMap.put("tglLaporan", hist.getTglLaporan());
			rowMap.put("customerNo", hist.getCustomerNo());
			rowMap.put("customerName", hist.getCustomerName());
//			rowMap.put("custAcNo", hist.getCustAcNo());
//			System.out.println("custAcNo :" + hist.getCustAcNo());
			rowMap.put("branchCode", hist.getBranchCode());
			rowMap.put("BRANCH_NAME", hist.getBranchName());
			rowMap.put("functionId", hist.getFunctionId());
			rowMap.put("openCifDate", hist.getCifCreationDate());
			rowMap.put("makerId", hist.getFirstMakerId());
			rowMap.put("lastMakerId", hist.getLastMakerId());
			rowMap.put("checkerId", hist.getLastOtoId());
			rowMap.put("tglUpdateCif", hist.getLastUpdate());
			rowMap.put("status", hist.getStatus());
			detail.add(rowMap);
		}
		return detail;
	}
	
	public ArrayList moveListToHashMapUser(List lph) {
		HashMap rowMap = null;
		ArrayList detail = new ArrayList();
		int inc = 1;
		 System.out.println("movehashmap");
		for (Iterator<JurnalTodayDetail> i = lph.iterator(); i.hasNext();) {
			JurnalTodayDetail hist = i.next();
			rowMap = new HashMap();
			
			//format number
			DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
			DecimalFormatSymbols format = new DecimalFormatSymbols();
			format.setMonetaryDecimalSeparator(',');
			format.setGroupingSeparator('.');
			df.setDecimalFormatSymbols(format);
			
//			rowMap.put("user_id", hist.getUser_id());
//			rowMap.put("kode_cabang", hist.getKode_cabang());
			rowMap.put("jam", hist.getJam());
			rowMap.put("kode_transaksi", hist.getKode_transaksi());
			rowMap.put("ket_kode_transaksi", hist.getKet_kode_transaksi());
			rowMap.put("nomor_warkat", hist.getNomor_warkat());
			rowMap.put("debet", hist.getDebet());
			rowMap.put("kredit", hist.getKredit());
			rowMap.put("user_approval", hist.getUser_approval());
			rowMap.put("ac_no", hist.getAc_no());
			rowMap.put("ac_desc", hist.getAc_desc());
			detail.add(rowMap);
		}
		 System.out.println("detail"+detail);
		return detail;
	}
	
	public ArrayList moveListToHasMapTeller(List lphTlr) {
		HashMap rowMap = null;
		ArrayList detail = new ArrayList();
		int inc = 1;
		for (Iterator<LaporanSPVTeller> i = lphTlr.iterator(); i.hasNext();) {
			LaporanSPVTeller hist = i.next();
			rowMap = new HashMap();
			rowMap.put("branch", hist.getBranch());
			rowMap.put("jam", hist.getJam());
			rowMap.put("trn_code", hist.getTrn_code());
			rowMap.put("ac_no", hist.getAc_no());
			rowMap.put("no_warkat", hist.getNo_warkat());
			rowMap.put("trn_ref_no", hist.getTrn_ref_no());
			rowMap.put("function_desc", hist.getFunction_desc());
			rowMap.put("drcr_ind", hist.getDrcr_ind());
			rowMap.put("nominal", hist.getNominal()); 
			rowMap.put("user_id", hist.getUser_id()); 
			rowMap.put("auth_id", hist.getAuth_id()); 
			detail.add(rowMap);
		}
		return detail;
	}
	
	public ArrayList moveListToHashMapPinpad (List download){
		HashMap rowMap = null;
		ArrayList detail = new ArrayList();
		int inc = 1;
		
		for (Iterator<pinpad> i = download.iterator(); i.hasNext();) {
			pinpad hist = i.next();
			rowMap = new HashMap();
			DateFormat dateFormattt = new SimpleDateFormat("dd-MMM-yyyy");
    	    Date dateee = new Date();
			rowMap.put("tgl", hist.getTrnDt());
			rowMap.put("termId", hist.getTermId());
			rowMap.put("mercId", hist.getIdMerc());
			rowMap.put("kartu", hist.getAccNo());
			rowMap.put("acNo", hist.getCustAcNo());
			rowMap.put("trnRef", hist.getTrnRefNo());
			rowMap.put("status", hist.getTrnType());
			rowMap.put("branch", hist.getBranch());
			rowMap.put("mercName", hist.getMercName());
			rowMap.put("tglCetak", dateFormattt.format(dateee));			
			detail.add(rowMap);
		}
		return detail;
	}
	
	public ArrayList moveListToHashMapUserCs(List lphCs) {
		HashMap rowMap = null;
		ArrayList detail = new ArrayList();
		int inc = 1;
		System.out.println("Masuk HashMap");
		
		for (Iterator<LaporanSpvCs> i = lphCs.iterator(); i.hasNext();) {
			LaporanSpvCs hist = i.next();
			rowMap = new HashMap();

			rowMap.put("branch_code", hist.getBranch());
			rowMap.put("branch_name", hist.getBranch_name());
			rowMap.put("tanggal", hist.getTanggal());
			rowMap.put("jam", hist.getJam());
			rowMap.put("key_id", hist.getKey_id());
			rowMap.put("maker_id", hist.getMaker_id());
			rowMap.put("checker_id", hist.getChecker_id());
			rowMap.put("function_id", hist.getFunction_id());
			rowMap.put("modifikasi_ke", hist.getModifikasi_ke());
			detail.add(rowMap);
		}
		return detail;
	}
	
	public ArrayList moveListToHashMapRepSknIn(List lhist) {
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        int inc = 1;
        for (Iterator<SknIn> i = lhist.iterator(); i.hasNext();) {
        	SknIn hist = i.next();
            rowMap = new HashMap();
            rowMap.put("no", inc++);
            rowMap.put("noref", hist.getNoref());
            rowMap.put("sor", hist.getSor());
            rowMap.put("sbp", hist.getSbp());
            rowMap.put("nmpngirim", hist.getNm_pengirim());
            rowMap.put("nmpenerima", hist.getNm_penerima());
            rowMap.put("nmpenerimabmi", hist.getNm_penerima_core());
            rowMap.put("rektujuan", hist.getRek_tujuan());
            rowMap.put("nominal", hist.getNominal());
            rowMap.put("ket", hist.getKet());
            rowMap.put("sts", hist.getSts());
            detail.add(rowMap);
        }
        return detail;
    }
	
	public List getSknInRepRetTtpn(List lhist, int a, int b) {
		List lttpn = new ArrayList();
        int inc = 1;
        for (Iterator<SknIn> i = lhist.iterator(); i.hasNext();) {
        	SknIn hist = i.next();
        	if (hist.getNo() >= a && hist.getNo() <= b){
            	lttpn.add(hist);        		
        	}
        }
        return lttpn;
    }
	
	public List getAcc_lineHist(List lhist, int a, int b) {
		List lttpn = new ArrayList();
        int inc = 1;
        for (Iterator<Acc_line> i = lhist.iterator(); i.hasNext();) {
        	Acc_line hist = i.next();
        	if (hist.getNo() >= a && hist.getNo() <= b){
            	lttpn.add(hist);        		
        	}
        }
        return lttpn;
    }
	
	public ArrayList moveListToHashMapKonvRek(List lhist) {
        HashMap rowMap = null;
        ArrayList detail = new ArrayList();
        int inc = 1;
        for (Iterator<KonversiRekening> i = lhist.iterator(); i.hasNext();) {
        	KonversiRekening hist = i.next();
            rowMap = new HashMap();
            rowMap.put("no", inc++);
            rowMap.put("altnmrek", hist.getAlt_nm_rekening());
            rowMap.put("altrek", hist.getAlt_rekening());
            rowMap.put("rek", hist.getRekening());
            rowMap.put("nmrek", hist.getNmrekening());
            rowMap.put("sts", hist.getStatus());
            rowMap.put("saldo", hist.getSaldo());
            detail.add(rowMap);
        }
        return detail;
    }
}
